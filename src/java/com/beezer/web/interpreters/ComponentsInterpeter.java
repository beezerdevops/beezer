/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.interpreters;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;

/**
 *
 * @author badry
 */
public class ComponentsInterpeter extends BeanFramework {

    private String advancedComponentId;
    private RecordModel componentConfiguration;

    public ComponentsInterpeter() {
    }

    @PostConstruct
    public void init() {
        if (super.getModuleId() != null) {
            super.setModuleId(super.getModuleId());
            this.loadComponentConfiguration();
            this.loadRecordsList();
        }
    }

    public void loadRecordsList() {
        super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", null, false, LazyRecordModel.RECORD, super.getModuleId()));
    }

    public void loadComponentConfiguration() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule("24");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("id", "=", this.advancedComponentId));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.componentConfiguration = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ComponentsInterpeter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getAdvancedComponentId() {
        return advancedComponentId;
    }

    public void setAdvancedComponentId(String advancedComponentId) {
        this.advancedComponentId = advancedComponentId;
    }

}
