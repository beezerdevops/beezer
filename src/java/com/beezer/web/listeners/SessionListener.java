/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.listeners;

import com.beezer.web.commons.Defines;
import com.crm.models.global.UserModel;
import java.util.HashMap;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Ahmed El Badry Nov 7, 2022
 */
public class SessionListener implements HttpSessionBindingListener {

    private static HashMap<SessionListener, HttpSession> loginLedger = new HashMap<SessionListener, HttpSession>();
    private UserModel userModel;
    private PrimeFaces pf;

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        Defines.LOGIN_LEDGER.put(this.getUserModel().getUserId(), event.getSession());
        HttpSession session = loginLedger.remove(this);
        if (session != null) {
            session.invalidate();
        }
        loginLedger.put(this, event.getSession());
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        Defines.LOGIN_LEDGER.remove(this.getUserModel().getUserId());
//        this.getPf().executeScript("PF('doubleLoginNotificationDialog').show()");
        loginLedger.remove(this);
    }

    public static HashMap<SessionListener, HttpSession> getLoginLedger() {
        return loginLedger;
    }

    public static void setLoginLedger(HashMap<SessionListener, HttpSession> loginLedger) {
        SessionListener.loginLedger = loginLedger;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public PrimeFaces getPf() {
        return pf;
    }

    public void setPf(PrimeFaces pf) {
        this.pf = pf;
    }

}
