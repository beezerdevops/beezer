/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.comparators;

import com.crm.models.internal.ModuleFieldModel;
import java.util.Comparator;

/**
 *
 * @author badry
 */
public class ModuleFieldByModuleIdComparator implements Comparator<ModuleFieldModel> {
    @Override
    public int compare(ModuleFieldModel o1, ModuleFieldModel o2) {
        return o1.getModuleId()- o2.getModuleId();
    }
}
