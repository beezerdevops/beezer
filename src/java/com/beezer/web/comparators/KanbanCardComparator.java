/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.comparators;

import com.crm.models.internal.dataView.KanbanCard;
import java.util.Comparator;

/**
 *
 * @author badry
 */
public class KanbanCardComparator implements Comparator<KanbanCard> {
    @Override
    public int compare(KanbanCard o1, KanbanCard o2) {
        return o1.getOrder()- o2.getOrder();
    }
}
