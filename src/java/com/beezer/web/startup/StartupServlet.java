/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.startup;

import com.beezer.web.commons.Defines;
import com.beezer.web.utils.GeneralUtils;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Ahmed Elbadry
 */
public class StartupServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        Defines.APP_ROOT = sc.getRealPath("");
        try {
            loadLoggers();
            GeneralUtils.loadGeneralConfigurations(Defines.APP_ROOT);
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void loadLoggers() {
        String log4jfile = getInitParameter("log4j-init-file");

        if (log4jfile != null) {
            String propfile = getServletContext().getRealPath(log4jfile);
            PropertyConfigurator.configure(propfile);
            Defines.DEBUG_LOGGER = Logger.getLogger("debugLogger");
            Defines.ERROR_LOGGER = Logger.getLogger("errorLogger");
        }
    }
}
