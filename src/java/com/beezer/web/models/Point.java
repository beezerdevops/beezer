/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

/**
 *
 * @author Ahmed El Badry Feb 6, 2021
 */
public class Point {

    private int x;
    private int y;

    public Point(float x, float y) {
        this.x = Math.round(x);
        this.y = Math.round(y);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    
}
