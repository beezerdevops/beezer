/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.RequestHandler;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;

/**
 *
 * @author badry
 */
public class LazyCustomTableRecord extends LazyDataModel<RecordModel> {

    public static final int RECORD = 1;
    public static final int DEAL = 2;

    private List<RecordModel> data;
    private String filterClause;
    private final int type;
    private String moduleId;
    private ResponseModel response;
    private String currentDataCount;
    private BeanFramework beanFramework;
    private FieldsLayoutModel fieldsLayoutModel;
    private ArrayList<ModuleFieldModel> tableFieldSet;

    public LazyCustomTableRecord(String moduleId, String filterClause) {
        this.moduleId = moduleId;
        this.filterClause = filterClause;
        this.type = 0;
    }

    public LazyCustomTableRecord(ArrayList<RecordModel> records) {
        this.data = records;
        this.type = 1;
    }

    public LazyCustomTableRecord(BeanFramework beanFramework, FieldsLayoutModel fieldsLayoutModel) {
        this.type = 2;
        this.beanFramework = beanFramework;
        this.fieldsLayoutModel = fieldsLayoutModel;
        this.tableFieldSet = this.beanFramework.getFieldSetService(null, fieldsLayoutModel.getTableComponentConfiguration().getBindingModelId(), true);
    }

    @Override
    public List<RecordModel> load(int first, int pageSize, Map<String, SortMeta> sortMeta, Map<String, FilterMeta> filterMeta) {
        switch (type) {
            case 0:
                this.setRowCount(data.size());
                break;
            case 1:
                List<RecordModel> currPageList = new ArrayList<>();
                if (this.data != null) {
                    ArrayList<RecordModel> pagedList = new ArrayList<>(data);
                    int pSize = pageSize + first;
                    if (pSize > data.size()) {
                        pSize = data.size();
                    }
                    currPageList = pagedList.subList(first, pSize);
                }
//                data = this.queryData(filterClause, pageSize, first, moduleId, "", "");
                this.setRowCount(data.size());
//                this.setRowCount(response.getTotalRecordCount());
                return currPageList;
            case 2:
                data = this.queryData(filterClause, pageSize, first, moduleId, "", "");
                this.setRowCount(response.getTotalRecordCount());
                for (RecordModel rm : data) {
                    this.beanFramework.buildTableComponentRecord(fieldsLayoutModel, rm, tableFieldSet);
                }
                break;
        }

        this.currentDataCount = String.valueOf(data.size());
        return data;
    }

    @Override
    public String getRowKey(RecordModel record) {
        for (Map.Entry<String, ModuleFieldModel> entry : record.getFieldValueMap().entrySet()) {
            if (entry.getValue().isIsKey()) {
                return entry.getValue().getCurrentValue();
            }
        }
        return null;
    }

    @Override
    public RecordModel getRowData(String rowKey) {
        for (RecordModel record : data) {
            for (Map.Entry<String, ModuleFieldModel> entry : record.getFieldValueMap().entrySet()) {
                if (entry.getValue().isIsKey()) {
                    if (entry.getValue().getCurrentValue().equals(rowKey)) {
                        return record;
                    }
                }
            }
        }
        return null;
    }

    public ArrayList<RecordModel> getData() {
        return (ArrayList<RecordModel>) this.data;
    }

    private ArrayList<RecordModel> queryData(String filterClause, int limit, int offset, String moduleId, String sortField, String sortType) {
        try {
            RequestModel request = new RequestModel();
            request.setRequestActionType("0");
            request.setLimit(limit);
            request.setOffset(offset);
            request.setRequestingModule(this.fieldsLayoutModel.getTableComponentConfiguration().getBindingModelId());
            ModuleModel mm = this.beanFramework.loadModule(this.fieldsLayoutModel.getTableComponentConfiguration().getBindingModelId());
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType(mm.getModuleBaseTable() + "." + this.fieldsLayoutModel.getTableComponentConfiguration().getKeyFieldName(), Integer.valueOf(this.beanFramework.getRequestParams().get("recordId"))));
            request.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
//            ResponseModel response = requestHandler.executeRequest(request);;
            response = requestHandler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
//                this.response = response;
                return response.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyCustomTableRecord.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getCurrentDataCount() {
        return currentDataCount;
    }

    @Override
    public int count(Map<String, FilterMeta> map) {
        return data.size();
    }

}
