/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

import com.beezer.web.beans.templates.DataBean;
import com.beezer.web.handler.RequestHandler;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.SortConfiguration;
import com.crm.models.internal.dataView.KanbanViewSettings;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

/**
 *
 * @author badry
 */
public class LazyKanbanModel extends LazyDataModel<RecordModel> {

    public static final int RECORD = 1;
    public static final int DEAL = 2;
    public static final int REPORT = 3;

    private String apiName;
    private ArrayList<RecordModel> data;
    private final String filterClause;
    private final boolean fullTextSearch;
    private ResponseModel response;
    private int type;
    private String moduleId;
    private String currentDataCount;
    private String extraClause;
    private ArrayList<SortConfiguration> sortConfigurations;

    private ArrayList<KanbanRecordModel> recordsGroupsList;
    private KanbanViewSettings kanbanViewSettings;
    private ModuleFieldModel boardField;
    private DataBean dataBean;
    private String targetTagId;

    private boolean cardMoved;

    public LazyKanbanModel(String apiName, String filterClause, boolean fullTextSearch, int type, String moduleId, KanbanViewSettings kanbanViewSettings, DataBean dataBean, String targetTagId) {
        this.moduleId = moduleId;
        this.apiName = apiName;
        this.filterClause = filterClause;
        this.fullTextSearch = fullTextSearch;
        this.type = type;
        this.kanbanViewSettings = kanbanViewSettings;
        this.dataBean = dataBean;
        this.targetTagId = targetTagId;
    }

    public LazyKanbanModel(String apiName, String filterClause, boolean fullTextSearch, int type, String moduleId, ArrayList<SortConfiguration> sortConfigurations) {
        this.moduleId = moduleId;
        this.apiName = apiName;
        this.filterClause = filterClause;
        this.fullTextSearch = fullTextSearch;
        this.type = type;
        this.sortConfigurations = sortConfigurations;
    }

//    public LazyRecordModel(String apiName, String filterClause, boolean fullTextSearch, int type) {
//        this.apiName = apiName;
//        this.filterClause = filterClause;
//        this.fullTextSearch = fullTextSearch;
//        this.type = type;
//    }
    public LazyKanbanModel(String filterClause, boolean fullTextSearch, int type) {
        this.filterClause = filterClause;
        this.fullTextSearch = fullTextSearch;
        this.type = type;
    }

    public void loadRecords(int first, int pageSize, Map<String, SortMeta> sortMeta, Map<String, FilterMeta> filterMeta) {
        FilterManager fm = new FilterManager();
        for (ModuleFieldModel mfm : this.dataBean.getFieldsList()) {
            if (kanbanViewSettings.getFieldId() == mfm.getFieldId()) {
                boardField = mfm;
                break;
            }
        }
        String moduleBaseName = dataBean.getActiveModule().getModuleBaseTable();
        fm.setFilter("AND", new FilterType(moduleBaseName + "." + boardField.getFieldName(), "=", targetTagId));
        switch (type) {
            case RECORD:
                if (sortMeta != null && !sortMeta.isEmpty()) {
                    String sortField = "";
                    String sortOrder = "";
                    for (Map.Entry<String, SortMeta> entry : sortMeta.entrySet()) {
                        sortField = entry.getKey();
                        sortOrder = entry.getValue().getOrder().name();
                    }
                    data = this.queryData(fm.getClause(), pageSize, first, moduleId, sortField, sortOrder);
                }
                else {
                    data = this.queryData(fm.getClause(), pageSize, first, moduleId, "", "");
                }
                break;
        }
    }

    @Override
    public List<RecordModel> load(int first, int pageSize, Map<String, SortMeta> sortMeta, Map<String, FilterMeta> filterMeta) {
        FilterManager fm = new FilterManager();
        if (filterClause == null || filterClause.isEmpty()) {
            if (this.data == null || this.data.isEmpty()) {
                this.loadRecords(first, pageSize, sortMeta, filterMeta);
            }
            else {
                if (cardMoved) {
                    cardMoved = false;
                    if (data.size() < pageSize && response.getTotalRecordCount() > data.size()) {
                        this.loadRecords(first, pageSize, sortMeta, filterMeta);
                    }
                }
                else {
                    this.loadRecords(first, pageSize, sortMeta, filterMeta);
                }

            }
        }
        else {
            switch (type) {
                case RECORD:
                    fm = new FilterManager();
                    fm.skipDefaultHeader(true);
                    for (ModuleFieldModel mfm : this.dataBean.getFieldsList()) {
                        if (kanbanViewSettings.getFieldId() == mfm.getFieldId()) {
                            boardField = mfm;
                            break;
                        }
                    }
                    String moduleBaseName = dataBean.getActiveModule().getModuleBaseTable();
                    fm.setFilter("AND", new FilterType(moduleBaseName + "." + boardField.getFieldName(), "=", targetTagId));
                    this.setExtraClause(fm.getClause());

                    if (sortMeta != null && !sortMeta.isEmpty()) {
                        String sortField = "";
                        String sortOrder = "";
                        for (Map.Entry<String, SortMeta> entry : sortMeta.entrySet()) {
                            sortField = entry.getKey();
                            sortOrder = entry.getValue().getOrder().name();
                        }
                        data = this.queryData(filterClause, pageSize, first, moduleId, sortField, sortOrder);
                    }
                    else {
                        data = this.queryData(filterClause, pageSize, first, moduleId, "", "");
                    }
                    break;
            }
        }
//        this.setRowCount(response.getTotalRecordCount());
        if (data != null) {
            this.currentDataCount = String.valueOf(data.size());
        }
        else {
            this.currentDataCount = "0";
        }
        return data;
    }

    @Override
    public String getRowKey(RecordModel record) {
        for (Map.Entry<String, ModuleFieldModel> entry : record.getFieldValueMap().entrySet()) {
            if (entry.getValue().isIsKey()) {
                return entry.getValue().getCurrentValue();
            }
        }
        return null;
    }

    @Override
    public RecordModel getRowData(String rowKey) {
        for (RecordModel record : data) {
            for (Map.Entry<String, ModuleFieldModel> entry : record.getFieldValueMap().entrySet()) {
                if (entry.getValue().isIsKey()) {
                    if (entry.getValue().getCurrentValue().equals(rowKey)) {
                        return record;
                    }
                }
            }
        }
        return null;
    }

    public ArrayList<RecordModel> getData() {
        return (ArrayList<RecordModel>) this.data;
    }

    private ArrayList<RecordModel> queryData(String filterClause, int limit, int offset, String moduleId, String sortField, String sortType) {
        try {
            RequestModel request = new RequestModel();
            request.setClause(filterClause);
            request.setRequestingModule(moduleId);
            request.setFullTextSearch(fullTextSearch);
            request.setLimit(limit);
            request.setOffset(offset);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            request.setExtraClause(extraClause);
            if (kanbanViewSettings.isEnableCustomSort()) {
                ArrayList<SortConfiguration> sortConfigurations = new ArrayList<>();
                sortConfigurations.add(this.kanbanViewSettings.getSortConfiguration());
                request.setSortConfigurations(sortConfigurations);
            }
            request.setRequestContext("DATAVIEW");
            RequestHandler requestHandler = new RequestHandler();
            response = requestHandler.executeRequest(request, this.apiName);
            if (response.getErrorCode() == 1000) {
                this.data = response.getRecordList();
                this.setRowCount(response.getTotalRecordCount());
                return response.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyKanbanModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

//    public ArrayList<RecordModel> groupData(ArrayList<RecordModel> recordModels) {
//        ArrayList<RecordModel> groupedDataRecords = new ArrayList<>();
//        if (recordModels != null) {
//            Collections.sort(kanbanViewSettings.getCardSettings(), new KanbanCardComparator());
//            this.recordsGroupsList = new ArrayList<>();
//            ArrayList<LookupModel> lookupList = null;
//            if (this.dataBean.getFieldsList() != null && !this.dataBean.getFieldsList().isEmpty()) {
//                String lookupTableId = null;
//                for (ModuleFieldModel mfm : this.dataBean.getFieldsList()) {
//                    if (kanbanViewSettings.getFieldId() == mfm.getFieldId()) {
//                        lookupTableId = mfm.getFieldValues();
//                        boardField = mfm;
//                        break;
//                    }
//                }
//
//                LookupHandler lh = new LookupHandler();
//                LookupRequest request = new LookupRequest();
//                request.setLookupId(lookupTableId);
//                request.setActionType("0");
//                LookupResponse response = lh.getLookups(request);
//                if (response.getErrorCode() == 1000) {
//                    lookupList = response.getReturnList();
//                }
//
//                if (lookupList != null) {
//
//                    for (RecordModel rm : recordModels) {
//                        if (rm.getFieldValueMap().get(boardField.getFieldName()).getCurrentValue().equals(String.valueOf(targetTagId))) {
//                            groupedDataRecords.add(rm);
//                        }
//                    }
//                }
//            }
//        }
//        return groupedDataRecords;
//    }
    public String getCurrentDataCount() {
        return currentDataCount;
    }

    public String getExtraClause() {
        return extraClause;
    }

    public void setExtraClause(String extraClause) {
        this.extraClause = extraClause;
    }

    public ArrayList<KanbanRecordModel> getRecordsGroupsList() {
        return recordsGroupsList;
    }

    public void setRecordsGroupsList(ArrayList<KanbanRecordModel> recordsGroupsList) {
        this.recordsGroupsList = recordsGroupsList;
    }

    public KanbanViewSettings getKanbanViewSettings() {
        return kanbanViewSettings;
    }

    public void setKanbanViewSettings(KanbanViewSettings kanbanViewSettings) {
        this.kanbanViewSettings = kanbanViewSettings;
    }

    public ModuleFieldModel getBoardField() {
        return boardField;
    }

    public void setBoardField(ModuleFieldModel boardField) {
        this.boardField = boardField;
    }

    public DataBean getDataBean() {
        return dataBean;
    }

    public void setDataBean(DataBean dataBean) {
        this.dataBean = dataBean;
    }

    public boolean isCardMoved() {
        return cardMoved;
    }

    public void setCardMoved(boolean cardMoved) {
        this.cardMoved = cardMoved;
    }

    public void setData(ArrayList<RecordModel> data) {
        this.data = data;
    }

    @Override
    public int count(Map<String, FilterMeta> map) {
        try {
            RequestModel request = new RequestModel();
            request.setClause(filterClause);
            request.setRequestingModule(moduleId);
            request.setFullTextSearch(fullTextSearch);
            request.setRequestActionType("4");
            request.setSkipContent(true);
            request.setExtraClause(extraClause);
            request.setRequestContext("DATAVIEW");
            RequestHandler requestHandler = new RequestHandler();
            response = requestHandler.executeRequest(request, this.apiName);
            if (response.getErrorCode() == 1000) {
                this.data = response.getRecordList();
                this.setRowCount(response.getTotalRecordCount());
                return response.getTotalRecordCount();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyKanbanModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

}
