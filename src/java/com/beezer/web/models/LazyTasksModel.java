/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

import com.beezer.web.handler.TaskHandler;
import com.crm.models.internal.TaskManagerModel;
import com.crm.models.requests.managers.TasksRequest;
import com.crm.models.responses.managers.TasksResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

/**
 *
 * @author Ahmed El Badry
 */
public class LazyTasksModel extends LazyDataModel<TaskManagerModel> {

    private List<TaskManagerModel> datasource;
    private final String filterClause;
    private final boolean fullTextSearch;
    private TasksResponse tasksResponse;
    private boolean showTeamTasks;
    private int currentPage = 1;

    public LazyTasksModel(String filterClause, boolean fullTextSearch, boolean showTeamTasks) {
        this.filterClause = filterClause;
        this.fullTextSearch = fullTextSearch;
        this.showTeamTasks = showTeamTasks;
    }

    @Override
    public TaskManagerModel getRowData(String rowKey) {
        for (TaskManagerModel task : datasource) {
            if (task.getTaskId() == Integer.valueOf(rowKey)) {
                return task;
            }
        }
        return null;
    }

    @Override
    public String getRowKey(TaskManagerModel task) {
        return String.valueOf(task.getTaskId());
    }

    @Override
    public List<TaskManagerModel> load(int first, int pageSize, Map<String, SortMeta> sortMeta, Map<String, FilterMeta> filterMeta) {
//        this.datasource = this.queryData(filterClause, pageSize, first, sortField, sortOrder.name());
        float page = first / pageSize;
        currentPage = (int) Math.ceil(page);
        this.datasource = this.queryData(filterClause, pageSize, first, "", "");
        this.setRowCount(tasksResponse.getTotalRecordCount());
        return datasource;
    }

    private ArrayList<TaskManagerModel> queryData(String filterClause, int limit, int offset, String sortField, String sortType) {
        try {
            TasksRequest request = new TasksRequest();
            request.setClause(filterClause);
            request.setFullTextSearch(fullTextSearch);
            request.setLimit(limit);
            request.setOffset(offset);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            request.setOrderByFieldName(sortField);
            request.setOrderType(sortType);
            request.setShowTeamTasks(showTeamTasks);
            TaskHandler requestHandler = new TaskHandler();
            tasksResponse = requestHandler.execute(request);
            if (tasksResponse.getErrorCode() == 1000) {
                return tasksResponse.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyTasksModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int count(Map<String, FilterMeta> map) {
        try {
            TasksRequest request = new TasksRequest();
            request.setClause(filterClause);
            request.setFullTextSearch(fullTextSearch);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            TaskHandler requestHandler = new TaskHandler();
            tasksResponse = requestHandler.execute(request);
            if (tasksResponse.getErrorCode() == 1000) {
                return tasksResponse.getTotalRecordCount();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyTasksModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

}
