/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

import com.crm.models.internal.RecordModel;
import java.util.ArrayList;

/**
 *
 * @author badry
 */
public class AttributeVariantModel {

    private String attributeId;
    private String attributeName;
    private ArrayList<String> attributeValues;
    private RecordModel entityModel;

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public ArrayList<String> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(ArrayList<String> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public RecordModel getEntityModel() {
        return entityModel;
    }

    public void setEntityModel(RecordModel entityModel) {
        this.entityModel = entityModel;
    }

}
