/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models.cube;

import java.util.LinkedHashMap;

/**
 *
 * @author Ahmed El Badry Aug 15, 2022
 */
public class ProcessingPartition {

    private String partitionId;
    private LinkedHashMap<String, String> processingConfig;
    private LinkedHashMap<String, String> processedValues;
    private boolean complete;
    private String status;
    private String message;

    public String getPartitionId() {
        return partitionId;
    }

    public void setPartitionId(String partitionId) {
        this.partitionId = partitionId;
    }

    public LinkedHashMap<String, String> getProcessingConfig() {
        return processingConfig;
    }

    public void setProcessingConfig(LinkedHashMap<String, String> processingConfig) {
        this.processingConfig = processingConfig;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LinkedHashMap<String, String> getProcessedValues() {
        return processedValues;
    }

    public void setProcessedValues(LinkedHashMap<String, String> processedValues) {
        this.processedValues = processedValues;
    }

}
