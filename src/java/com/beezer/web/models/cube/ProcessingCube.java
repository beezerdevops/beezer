/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models.cube;

import java.util.LinkedHashMap;

/**
 *
 * @author Ahmed El Badry Aug 15, 2022
 */
public class ProcessingCube {

    private int numberOfPartitions;
    private LinkedHashMap<String, ProcessingPartition> partitionMap;
    private boolean cubeComplete;
    private String startTime;
    private String endTime;

    public int getNumberOfPartitions() {
        return numberOfPartitions;
    }

    public void setNumberOfPartitions(int numberOfPartitions) {
        this.numberOfPartitions = numberOfPartitions;
    }

    public LinkedHashMap<String, ProcessingPartition> getPartitionMap() {
        return partitionMap;
    }

    public void setPartitionMap(LinkedHashMap<String, ProcessingPartition> partitionMap) {
        this.partitionMap = partitionMap;
    }

    public boolean isCubeComplete() {
        return cubeComplete;
    }

    public void setCubeComplete(boolean cubeComplete) {
        this.cubeComplete = cubeComplete;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

}
