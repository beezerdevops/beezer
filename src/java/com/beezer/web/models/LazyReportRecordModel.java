/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

import com.beezer.web.handler.ReportHandler;
import com.crm.models.global.reports.ReportModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.SortConfiguration;
import com.crm.models.requests.ReportRequest;
import com.crm.models.responses.ReportResponse;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

/**
 *
 * @author badry
 */
public class LazyReportRecordModel extends LazyDataModel<RecordModel> {

    public static final int RECORD = 1;
    public static final int DEAL = 2;
    public static final int REPORT = 3;

    private String apiName;
    private List<RecordModel> data;
    private String filterClause;
    private boolean fullTextSearch;
    private ResponseModel response;
    private int type;
    private String moduleId;
    private String currentDataCount;
    private String extraClause;
    private ArrayList<SortConfiguration> sortConfigurations;
    private ReportModel reportModel;

    public LazyReportRecordModel(String apiName, String filterClause, boolean fullTextSearch, int type, String moduleId) {
        this.moduleId = moduleId;
        this.apiName = apiName;
        this.filterClause = filterClause;
        this.fullTextSearch = fullTextSearch;
        this.type = type;
    }

    public LazyReportRecordModel(String apiName, String filterClause, boolean fullTextSearch, int type, String moduleId, ArrayList<SortConfiguration> sortConfigurations) {
        this.moduleId = moduleId;
        this.apiName = apiName;
        this.filterClause = filterClause;
        this.fullTextSearch = fullTextSearch;
        this.type = type;
        this.sortConfigurations = sortConfigurations;
    }

//    public LazyRecordModel(String apiName, String filterClause, boolean fullTextSearch, int type) {
//        this.apiName = apiName;
//        this.filterClause = filterClause;
//        this.fullTextSearch = fullTextSearch;
//        this.type = type;
//    }
    public LazyReportRecordModel(ReportModel reportModel) {
        this.reportModel = reportModel;
    }

    @Override
    public List<RecordModel> load(int first, int pageSize, Map<String, SortMeta> sortMeta, Map<String, FilterMeta> filterMeta) {
        data = this.queryData(pageSize, first);
        this.setRowCount(response.getTotalRecordCount());
        if (data != null) {
            this.currentDataCount = String.valueOf(data.size());
        }
        else {
            this.currentDataCount = "0";
        }
        return data;
    }

    @Override
    public String getRowKey(RecordModel record) {
        for (Map.Entry<String, ModuleFieldModel> entry : record.getFieldValueMap().entrySet()) {
            if (entry.getValue().isIsKey()) {
                return entry.getValue().getCurrentValue();
            }
        }
        return null;
    }

    @Override
    public RecordModel getRowData(String rowKey) {
        for (RecordModel record : data) {
            for (Map.Entry<String, ModuleFieldModel> entry : record.getFieldValueMap().entrySet()) {
                if (entry.getValue().isIsKey()) {
                    if (entry.getValue().getCurrentValue().equals(rowKey)) {
                        return record;
                    }
                }
            }
        }
        return null;
    }

    public ArrayList<RecordModel> getData() {
        return (ArrayList<RecordModel>) this.data;
    }

    private ArrayList<RecordModel> queryData(int limit, int offset) {
        try {
            ReportRequest request = new ReportRequest();
            request.setReportModel(reportModel);
            request.setLimit(limit);
            request.setOffset(offset);
            request.setRequestActionType("0");
            request.setGetData(true);
            ReportHandler reportHandler = new ReportHandler();
            ReportResponse reportResponse = reportHandler.execute(request);
            if (reportResponse.getErrorCode() == 1000) {
                response = new ResponseModel();
                response.setErrorCode(1000);
                response.setRecordList(reportResponse.getDataRecords());
                response.setTotalRecordCount(reportResponse.getTotalRecordCount());
                return reportResponse.getDataRecords();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyReportRecordModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getCurrentDataCount() {
        return currentDataCount;
    }

    public String getExtraClause() {
        return extraClause;
    }

    public void setExtraClause(String extraClause) {
        this.extraClause = extraClause;
    }

    @Override
    public int count(Map<String, FilterMeta> map) {
        try {
            ReportRequest request = new ReportRequest();
            request.setReportModel(reportModel);
            request.setRequestActionType("4");
            request.setGetData(true);
            ReportHandler reportHandler = new ReportHandler();
            ReportResponse reportResponse = reportHandler.execute(request);
            if (reportResponse.getErrorCode() == 1000) {
                response = new ResponseModel();
                response.setErrorCode(1000);
                response.setRecordList(reportResponse.getDataRecords());
                response.setTotalRecordCount(reportResponse.getTotalRecordCount());
                return reportResponse.getTotalRecordCount();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyReportRecordModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

}
