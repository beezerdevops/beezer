/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

import com.crm.models.internal.ActionLogModel;
import com.crm.models.internal.ModuleFieldModel;
import java.util.LinkedHashMap;

/**
 *
 * @author Ahmed El Badry Apr 9, 2022
 */
public class ProxyRecordModel {

    private LinkedHashMap<String, ModuleFieldModel> fieldValueMap;
    private ActionLogModel actionLogModel;
    private String viewField;

    public LinkedHashMap<String, ModuleFieldModel> getFieldValueMap() {
        return fieldValueMap;
    }

    public void setFieldValueMap(LinkedHashMap<String, ModuleFieldModel> fieldValueMap) {
        this.fieldValueMap = fieldValueMap;
    }

    public ActionLogModel getActionLogModel() {
        return actionLogModel;
    }

    public void setActionLogModel(ActionLogModel actionLogModel) {
        this.actionLogModel = actionLogModel;
    }

    public String getViewField() {
        return viewField;
    }

    public void setViewField(String viewField) {
        this.viewField = viewField;
    }

    @Override
    public String toString() {
        return this.getFieldValueMap().get(viewField).getCurrentValue();
    }

}
