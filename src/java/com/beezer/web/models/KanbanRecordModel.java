/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

import com.crm.models.internal.RecordModel;
import java.util.ArrayList;

/**
 *
 * @author badry
 */
public class KanbanRecordModel {

    private String tagId;
    private String groupTagName;
    private ArrayList<RecordModel> groupData;
    private LazyKanbanModel lazyRecordModel;

    public String getGroupTagName() {
        return groupTagName;
    }

    public void setGroupTagName(String groupTagName) {
        this.groupTagName = groupTagName;
    }

    public ArrayList<RecordModel> getGroupData() {
        return groupData;
    }

    public void setGroupData(ArrayList<RecordModel> groupData) {
        this.groupData = groupData;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public LazyKanbanModel getLazyRecordModel() {
        return lazyRecordModel;
    }

    public void setLazyRecordModel(LazyKanbanModel lazyRecordModel) {
        this.lazyRecordModel = lazyRecordModel;
    }

}
