/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

import com.beezer.web.handler.DealHandler;
import com.beezer.web.handler.ReportHandler;
import com.beezer.web.handler.RequestHandler;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.SortConfiguration;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.requests.DealRequest;
import com.crm.models.requests.ReportRequest;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;

/**
 *
 * @author badry
 */
public class LazyRecordModel extends LazyDataModel<RecordModel> {

    public static final int RECORD = 1;
    public static final int DEAL = 2;
    public static final int REPORT = 3;

    private String apiName;
    private List<RecordModel> data;
    private final String filterClause;
    private final boolean fullTextSearch;
    private ResponseModel response;
    private int type;
    private String moduleId;
    private String currentDataCount;
    private String extraClause;
    private ArrayList<SortConfiguration> sortConfigurations;

    public LazyRecordModel(String apiName, String filterClause, boolean fullTextSearch, int type, String moduleId) {
        this.moduleId = moduleId;
        this.apiName = apiName;
        this.filterClause = filterClause;
        this.fullTextSearch = fullTextSearch;
        this.type = type;
    }

    public LazyRecordModel(String apiName, String filterClause, boolean fullTextSearch, int type, String moduleId, ArrayList<SortConfiguration> sortConfigurations) {
        this.moduleId = moduleId;
        this.apiName = apiName;
        this.filterClause = filterClause;
        this.fullTextSearch = fullTextSearch;
        this.type = type;
        this.sortConfigurations = sortConfigurations;
    }

//    public LazyRecordModel(String apiName, String filterClause, boolean fullTextSearch, int type) {
//        this.apiName = apiName;
//        this.filterClause = filterClause;
//        this.fullTextSearch = fullTextSearch;
//        this.type = type;
//    }
    public LazyRecordModel(String filterClause, boolean fullTextSearch, int type) {
        this.filterClause = filterClause;
        this.fullTextSearch = fullTextSearch;
        this.type = type;
    }

    @Override
    public List<RecordModel> load(int first, int pageSize, Map<String, SortMeta> sortMeta, Map<String, FilterMeta> filterMeta) {
        FilterManager fm = new FilterManager();
        if (filterClause == null || filterClause.isEmpty()) {
            switch (type) {
                case RECORD:
                    if (sortMeta != null && !sortMeta.isEmpty()) {
                        String sortField = "";
                        String sortOrder = "";
                        for (Map.Entry<String, SortMeta> entry : sortMeta.entrySet()) {
                            sortField = entry.getKey();
                            sortOrder = entry.getValue().getOrder().name();
                        }
                        data = this.queryData(fm.getClause(), pageSize, first, moduleId, sortField, sortOrder);
                    }
                    else {
                        data = this.queryData(fm.getClause(), pageSize, first, moduleId, "", "");
                    }
                    break;
                case DEAL:
                    data = this.queryDealData(fm.getClause(), pageSize, first, "", "");
                    break;
                case REPORT:
                    data = this.queryReportData(fm.getClause(), pageSize, first, "", "");
                    break;
            }
        }
        else {
            switch (type) {
                case RECORD:
                    if (sortMeta != null && !sortMeta.isEmpty()) {
                        String sortField = "";
                        String sortOrder = "";
                        for (Map.Entry<String, SortMeta> entry : sortMeta.entrySet()) {
                            sortField = entry.getKey();
                            sortOrder = entry.getValue().getOrder().name();
                        }
                        data = this.queryData(filterClause, pageSize, first, moduleId, sortField, sortOrder);
                    }
                    else {
                        data = this.queryData(filterClause, pageSize, first, moduleId, "", "");
                    }
                    break;
                case DEAL:
                    data = this.queryDealData(filterClause, pageSize, first, "", "");
                    break;
                case REPORT:
                    data = this.queryReportData(filterClause, pageSize, first, "", "");
                    break;
            }
        }
        this.setRowCount(response.getTotalRecordCount());
        if (data != null) {
            this.currentDataCount = String.valueOf(data.size());
        }
        else {
            this.currentDataCount = "0";
        }
        return data;
    }

    @Override
    public String getRowKey(RecordModel record) {
        for (Map.Entry<String, ModuleFieldModel> entry : record.getFieldValueMap().entrySet()) {
            if (entry.getValue().isIsKey()) {
                return entry.getValue().getCurrentValue();
            }
        }
        return null;
    }

    @Override
    public RecordModel getRowData(String rowKey) {
        for (RecordModel record : data) {
            for (Map.Entry<String, ModuleFieldModel> entry : record.getFieldValueMap().entrySet()) {
                if (entry.getValue().isIsKey()) {
                    if (entry.getValue().getCurrentValue().equals(rowKey)) {
                        return record;
                    }
                }
            }
        }
        return null;
    }

    public ArrayList<RecordModel> getData() {
        return (ArrayList<RecordModel>) this.data;
    }

    private ArrayList<RecordModel> queryData(String filterClause, int limit, int offset, String moduleId, String sortField, String sortType) {
        try {
            RequestModel request = new RequestModel();
            request.setClause(filterClause);
            request.setRequestingModule(moduleId);
            request.setFullTextSearch(fullTextSearch);
            request.setLimit(limit);
            request.setOffset(offset);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            request.setExtraClause(extraClause);
            request.setOrderByFieldName(sortField);
            request.setOrderType(sortType);
            request.setRequestContext("DATAVIEW");
            request.setSortConfigurations(sortConfigurations);
            RequestHandler requestHandler = new RequestHandler();
            response = requestHandler.executeRequest(request, this.apiName);
            if (response.getErrorCode() == 1000) {
                return response.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyRecordModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private ArrayList<RecordModel> queryDealData(String filterClause, int limit, int offset, String sortField, String sortType) {
        try {
            DealHandler dealHandler = new DealHandler();
            DealRequest request = new DealRequest();
            request.setClause(filterClause);
            request.setFullTextSearch(fullTextSearch);
            request.setLimit(limit);
            request.setOffset(offset);
            request.setExtraClause(extraClause);
            request.setActionType("0");
            request.setOrderByFieldName(sortField);
            request.setOrderType(sortType);
            response = dealHandler.dealExecutor(request);
            if (response.getErrorCode() == 1000) {
                return response.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyRecordModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private ArrayList<RecordModel> queryReportData(String filterClause, int limit, int offset, String sortField, String sortType) {
        try {
            ReportHandler reportHandler = new ReportHandler();
            ReportRequest request = new ReportRequest();
            request.setClause(filterClause);
            request.setFullTextSearch(fullTextSearch);
            request.setLimit(limit);
            request.setOffset(offset);
            request.setExtraClause(extraClause);
            request.setRequestActionType("0");
            request.setOrderByFieldName(sortField);
            request.setOrderType(sortType);
            response = reportHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                return response.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyRecordModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getCurrentDataCount() {
        return currentDataCount;
    }

    public String getExtraClause() {
        return extraClause;
    }

    public void setExtraClause(String extraClause) {
        this.extraClause = extraClause;
    }

    @Override
    public int count(Map<String, FilterMeta> map) {
        try {
            RequestModel request = new RequestModel();
            if (filterClause != null && !filterClause.isEmpty()) {
                request.setClause(filterClause);
            }
            request.setRequestingModule(moduleId);
            request.setFullTextSearch(fullTextSearch);
            request.setRequestActionType("4");
            request.setSkipContent(true);
            request.setExtraClause(extraClause);
            request.setRequestContext("DATAVIEW");
            request.setSortConfigurations(sortConfigurations);
            RequestHandler requestHandler = new RequestHandler();
            response = requestHandler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
                return response.getTotalRecordCount();
            }
            else {
                return 0;
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LazyRecordModel.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

}
