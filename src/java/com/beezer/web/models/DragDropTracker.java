/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

/**
 *
 * @author badry
 */
public class DragDropTracker {
    private int sourceBlock;
    private int destinationBlock;
    private Object sourceObject;
    private Object destinationObject;

    public int getSourceBlock() {
        return sourceBlock;
    }

    public void setSourceBlock(int sourceBlock) {
        this.sourceBlock = sourceBlock;
    }

    public int getDestinationBlock() {
        return destinationBlock;
    }

    public void setDestinationBlock(int destinationBlock) {
        this.destinationBlock = destinationBlock;
    }

    public Object getSourceObject() {
        return sourceObject;
    }

    public void setSourceObject(Object sourceObject) {
        this.sourceObject = sourceObject;
    }

    public Object getDestinationObject() {
        return destinationObject;
    }

    public void setDestinationObject(Object destinationObject) {
        this.destinationObject = destinationObject;
    }
    
    
}
