/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.models;

import com.crm.models.global.UnstructuredModel;
import com.crm.models.internal.RecordModel;
import java.io.InputStream;
import java.util.ArrayList;
import org.primefaces.model.file.UploadedFile;
import org.primefaces.model.file.UploadedFiles;

/**
 *
 * @author badry
 */
public class SelectionModel {

    private String fieldName;
    private String selectedValue;
    private String[] multiSelectedValues;
    private ArrayList<String> multiRelationSelectedValues;
    private ArrayList<String> multiRelationSelectedValueViews;
    private ArrayList<RecordModel> selectedRecords;
    private UploadedFile selectedContent;
    private UploadedFiles selectedFiles;
    private InputStream contentStream;
    private UnstructuredModel contentModel;
    private ArrayList<UnstructuredModel> contentList;
    private ArrayList<String> contentUrlList;

    public SelectionModel() {
    }

    public SelectionModel(String[] multiSelectedValues) {
        this.multiSelectedValues = multiSelectedValues;
    }

    public SelectionModel(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public SelectionModel(UploadedFile selectedContent) {
        this.selectedContent = selectedContent;
    }

    public SelectionModel(InputStream contentStream) {
        this.contentStream = contentStream;
    }

    public SelectionModel(ArrayList<String> multiRelationSelectedValues, ArrayList<String> multiRelationSelectedValueViews) {
        this.multiRelationSelectedValues = multiRelationSelectedValues;
        this.multiRelationSelectedValueViews = multiRelationSelectedValueViews;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public UploadedFile getSelectedContent() {
        return selectedContent;
    }

    public void setSelectedContent(UploadedFile selectedContent) {
        this.selectedContent = selectedContent;
    }

    public InputStream getContentStream() {
        return contentStream;
    }

    public void setContentStream(InputStream contentStream) {
        this.contentStream = contentStream;
    }

    public UnstructuredModel getContentModel() {
        return contentModel;
    }

    public void setContentModel(UnstructuredModel contentModel) {
        this.contentModel = contentModel;
    }

    public String[] getMultiSelectedValues() {
        return multiSelectedValues;
    }

    public void setMultiSelectedValues(String[] multiSelectedValues) {
        this.multiSelectedValues = multiSelectedValues;
    }

    public ArrayList<String> getMultiRelationSelectedValues() {
        return multiRelationSelectedValues;
    }

    public void setMultiRelationSelectedValues(ArrayList<String> multiRelationSelectedValues) {
        this.multiRelationSelectedValues = multiRelationSelectedValues;
    }

    public ArrayList<String> getMultiRelationSelectedValueViews() {
        return multiRelationSelectedValueViews;
    }

    public void setMultiRelationSelectedValueViews(ArrayList<String> multiRelationSelectedValueViews) {
        this.multiRelationSelectedValueViews = multiRelationSelectedValueViews;
    }

    public ArrayList<RecordModel> getSelectedRecords() {
        return selectedRecords;
    }

    public void setSelectedRecords(ArrayList<RecordModel> selectedRecords) {
        this.selectedRecords = selectedRecords;
    }

    public ArrayList<UnstructuredModel> getContentList() {
        return contentList;
    }

    public void setContentList(ArrayList<UnstructuredModel> contentList) {
        this.contentList = contentList;
    }

    public ArrayList<String> getContentUrlList() {
        return contentUrlList;
    }

    public void setContentUrlList(ArrayList<String> contentUrlList) {
        this.contentUrlList = contentUrlList;
    }

    public UploadedFiles getSelectedFiles() {
        return selectedFiles;
    }

    public void setSelectedFiles(UploadedFiles selectedFiles) {
        this.selectedFiles = selectedFiles;
    }

}
