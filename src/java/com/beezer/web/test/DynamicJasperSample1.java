/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.test;

import com.beezer.web.handler.RequestHandler;
import com.crm.models.global.reports.ReportModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JRDesignBand;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import net.sf.jasperreports.engine.design.JRDesignField;
import net.sf.jasperreports.engine.design.JRDesignSection;
import net.sf.jasperreports.engine.design.JRDesignStaticText;
import net.sf.jasperreports.engine.design.JRDesignStyle;
import net.sf.jasperreports.engine.design.JRDesignTextField;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;

/**
 *
 * @author badry
 */
public class DynamicJasperSample1 {

    private ReportModel reportModel;

    public void run(ReportModel reportModel) throws Exception {
        this.reportModel = reportModel;
        JasperReport jasperReport;
        DynamicJasperSample1 dynamicJasperSample = new DynamicJasperSample1();
        JasperDesign design = dynamicJasperSample.createDesign();

        /*Create jrxml file if you need to see file in iReport or jasper studio.*/
//        jasperReport = JasperCompileManager.compileReport(design);
//        DynamicJasperHelper.generateJRXML(jasperReport, "UTF-8", "report/samplejrxml.jrxml");
        List<Person> personList = new ArrayList<>();
        List<ModuleFieldModel> fieldList = dynamicJasperSample.getTestData(null);
        jasperReport = JasperCompileManager.compileReport(design);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<String, Object>(), new JRBeanCollectionDataSource(fieldList));
        JasperExportManager.exportReportToPdfFile(jasperPrint, "C:\\Users\\badry\\Desktop\\Hybris\\sample.pdf");

    }

    public ArrayList<ModuleFieldModel> getTestData(ReportModel reportModel) {
        try {
            RequestModel request = new RequestModel();
            request.setLimit(1000);
            request.setOffset(0);
            request.setRequestActionType("0");
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "GetCustomersWS");
            if (response.getErrorCode() == 1000) {
                ArrayList<ModuleFieldModel> data = new ArrayList<>();
                for (RecordModel rm : response.getRecordList()) {
                    for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                        data.add(entry.getValue());
                    }
                }

                return data;
            }
        }
        catch (Exception ex) {
            Logger.getLogger(DynamicJasperSample1.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public JasperDesign createDesign() throws JRException {
        JasperDesign jasperDesign = new JasperDesign();
        /*Set basic design of page.*/
        jasperDesign.setName("sampleDynamicJasperDesign");
        jasperDesign.setPageWidth(595); // page width
        jasperDesign.setPageHeight(842); // page height
        jasperDesign.setColumnWidth(515);   // column width of page
        jasperDesign.setColumnSpacing(0);
        jasperDesign.setLeftMargin(40);
        jasperDesign.setRightMargin(40);
        jasperDesign.setTopMargin(20);
        jasperDesign.setBottomMargin(20);

        JRDesignExpression expression = new JRDesignExpression();

        //Set style of page.
        JRDesignStyle normalStyle = new JRDesignStyle();
        normalStyle.setName("Sans_Normal");
        normalStyle.setDefault(true);
        normalStyle.setFontSize(12);
        normalStyle.setPdfFontName("Helvetica");
        normalStyle.setPdfEncoding("Cp1252");
        normalStyle.setPdfEmbedded(false);
        jasperDesign.addStyle(normalStyle);


        /*
         * Generate field dynamically
         * */
//        for(ModuleFieldModel mfm : reportModel.getFieldList()){}
        JRDesignField field = new JRDesignField();
        field.setName("fieldName");
        field.setValueClass(String.class);
        jasperDesign.addField(field);

        JRDesignBand band = new JRDesignBand();

        //Title Band
        band = new JRDesignBand();
        band.setHeight(30);

        JRDesignStaticText staticText = new JRDesignStaticText();
        staticText.setText("Person's Specification");
        staticText.setX(0);
        staticText.setY(0);
        staticText.setHeight(20);
        staticText.setWidth(515);
        staticText.setHorizontalAlignment(HorizontalAlignEnum.CENTER);
        band.addElement(staticText);
        jasperDesign.setTitle(band);

//        Detail Band
        band = new JRDesignBand(); // New band
        band.setHeight(20); // Set band height

        /*Create text field dynamically*/
        JRDesignTextField textField = new JRDesignTextField();
        textField.setX(0);  // x position of text field.
        textField.setY(0);  // y position of text field.
        textField.setWidth(160);    // set width of text field.
        textField.setHeight(20);    // set height of text field.
        JRDesignExpression jrExpression = new JRDesignExpression(); // new instanse of expression. We need create new instance always when need to set expression.
        jrExpression.setText("\"" + "First Name: " + "\"" + "+" + "$F{currentValue}"); //  Added String before field in expression.
        textField.setExpression(jrExpression);  // set expression value in textfield.
        band.addElement(textField); // Added element in textfield.

        textField = new JRDesignTextField();
        textField.setX(160);
        textField.setY(0);
        textField.setWidth(160);
        textField.setHeight(20);
        jrExpression = new JRDesignExpression();
        jrExpression.setText("$F{currentValue}" + "+" + "\"" + " :Last Name" + "\""); // Added string after field value
        textField.setExpression(jrExpression);
        band.addElement(textField);

        textField = new JRDesignTextField();
        textField.setX(320);
        textField.setY(0);
        textField.setWidth(160);
        textField.setHeight(20);
        jrExpression = new JRDesignExpression();
        String age = "\"" + "<html><font color=" + "\\" + "\"" + "#66FF33" + "\\" + "\"" + ">" + "\"" + "+" + "\"" + "Age is: " + "\"" + "+" + "\"" + "</font><font color=" + "\\" + "\"" + "#6600FF" + "\\" + "\"" + ">" + "\"" + "+" + "$F{currentValue}" + "+" + "\"" + "</font></html>" + "\"";  // added html in text field with different color.
        jrExpression.setText(age);
        textField.setExpression(jrExpression);
        textField.setMarkup("html"); // By Default markup is none, We need to set it as html if we set expression as html.
        band.addElement(textField);
        ((JRDesignSection) jasperDesign.getDetailSection()).addBand(band);

        return jasperDesign;
    }
}
