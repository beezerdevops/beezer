/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 *
 * @author badry
 */
public class TestInterperter {

    public static void main(String[] args) throws Exception {
        String so = "order,hoo";
        ArrayList<String> customFields = new ArrayList<>(Arrays.asList(so.split(",")));
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        String defaultDateFrom = format1.format(cal.getTime());

        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        String defaultDateTo = format1.format(cal.getTime());

        Pattern p = Pattern.compile("\\$(\\w.*?)\\$.\\$(\\w.*?)\\$");
        String s = "Hello $everybody$.$soso$ How $are$ you $all$ $";

        Matcher m = p.matcher(s);
        while (m.find()) {
            System.out.println(m.group(1));
            System.out.println(m.group(2));
        }

        Pattern p2 = Pattern.compile("\\$(test?)\\$");
        String s2 = "Hello $test$.event jhgjgj";

        Matcher m2 = p2.matcher(s2);
        while (m2.find()) {
            System.out.println(m2.group(1));
//            System.out.println(m2.group(2));
        }

        String t = "if($this$.event === 'getProductsEvent'){"
                + "'PRODUCT'"
                + "}else{"
                + "'SERVICE'"
                + "}";
        if (t.contains("$test$.event")) {
            String o = t.replaceAll("\\$(test?)\\$.event", "hamada");
            System.out.println(o);
        }

        Pattern pex = Pattern.compile("\\$\\[(\\w.*)\\]");
        String sexp = "$[Hello $[yasalam $[3ala el nesting]]] $everybody$.$soso$ $[How $are$ you] $all$";
        Matcher moexp = pex.matcher(sexp);
        while (moexp.find()) {
            System.out.println(moexp.group(1));
        }
        System.out.println(executeExpressions(sexp));

        Pattern po = Pattern.compile("\\$(\\w.*?)\\$");
        Matcher mo = po.matcher(s2);
        while (mo.find()) {
            System.out.println(mo.group(1));
        }

        String script = "var f = {\n"
                + "  value: 0,\n"
                + "  add: function(n) {\n"
                + "    this.value += n;\n"
                + "    return this.value;\n"
                + "  }\n"
                + "};\n"
                + "f;";
        String script2 = "var str1 = \"Hello \";\n"
                + "var str2 = \"world!\";\n"
                + "var v = { zz :  'hamada '.concat('yel3ab')}; v;";

        String script3 = "zz : if('hamada' === 'x') {'hamada'.concat(' ').concat('yel3ab')}else{'hamada'.concat(' ').concat('yenot')};";

        String script4 = "load(\"nashorn:mozilla_compat.js\");"
                + "importPackage(com.beezer.web.test);"
                + "print('Welocme to java world\\n');"
                + "var ss = TestInterperter.sayHi();"
                + "ss";

        String scriptStyle = "load('nashorn:mozilla_compat.js');"
                + "importPackage(com.beezer.web.utils);"
                + "importPackage(com.crm.models.internal);"
                + "var ss = new StylingManager();"
                + "ss.setStylingModel(new TableStylingModel());"
                + "ss";

        ScriptEngine engine = new ScriptEngineManager().getEngineByName("js");

        String script5 = "var v = {z:if('hamada' === 'x') {fontColor=#000}else{fontColor=#fff}};v;";
//        ScriptObjectMirror obj5 = (ScriptObjectMirror) engine.eval(script5);
//        System.out.println(obj5.getMember("fontColor"));

        Object objx = (Object) engine.eval(script3);
        System.out.println("res = " + objx.toString());

//        ScriptObjectMirror obj = (ScriptObjectMirror) engine.eval(s2);
//        System.out.println("obj.value = " + obj.getMember("value"));
//        System.out.println("obj.add(5): " + obj.callMember("add", 5));
//        System.out.println("obj.add(-3): " + obj.callMember("add", -3));
//        System.out.println("obj.value = " + obj.getMember("value"));

        String result = null;
        engine.put("result", result);

    }

    public static Object sayHi() {
        return 0;
    }

    public static String executeExpressions(String script) {
        String replacedScript = script;
        Pattern pex = Pattern.compile("\\$\\[(\\w.*?)\\]");
        Matcher moexp = pex.matcher(script);
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("js");
        while (moexp.find()) {
            String exp = moexp.group(1);
            //Supporting nesting expressions
            exp = executeExpressions(exp);
            Object objx = null;
            try {
                objx = engine.eval(exp);
            }
            catch (ScriptException ex) {
                Logger.getLogger(TestInterperter.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (objx != null) {
                replacedScript = script.replace("\\$\\[" + exp + "\\]", objx.toString());
            }
        }
        return replacedScript;
    }
}
