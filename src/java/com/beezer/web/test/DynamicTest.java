/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.test;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.core.layout.LayoutManager;
import ar.com.fdvs.dj.domain.DJCalculation;
import ar.com.fdvs.dj.domain.DJValueFormatter;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.builders.FastReportBuilder;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author badry
 */
public class DynamicTest {

    public static void main(String[] args) {
        try {
            build();
        }
        catch (Exception ex) {
            Logger.getLogger(DynamicTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void build() throws Exception {
        FastReportBuilder drb = new FastReportBuilder();
        drb.addColumn("State", "state", String.class.getName(), 30)
                .addColumn("Branch", "branch", String.class.getName(), 30)
                .addColumn("Product Line", "productLine", String.class.getName(), 50)
                .addColumn("Item", "item", String.class.getName(), 50)
                .addColumn("Item Code", "id", Long.class.getName(), 30, true)
                .addColumn("Quantity", "quantity", Long.class.getName(), 60, true)
                .addColumn("Amount", "amount", Float.class.getName(), 70, true)
                .addGroups(1)
                .setTitle("November " + "2017" + " sales report")
                .setSubtitle("This report was generated at " + new Date())
                .setPrintBackgroundOnOddRows(true)
                .setUseFullPageWidth(true);

        drb.addGlobalFooterVariable(drb.getColumn(4), DJCalculation.COUNT, null, new DJValueFormatter() {

            @Override
            public String getClassName() {
                return String.class.getName();
            }

            @Override
            public Object evaluate(Object value, Map fields, Map variables, Map parameters) {
                return (value == null ? "0" : value.toString()) + " Clients";
            }
        });

        DynamicReport dr = drb.build();
        JRDataSource ds = getDataSource();
        JasperPrint jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), ds);
        JasperViewer.viewReport(jp);  
    }
    
    protected LayoutManager getLayoutManager() {
        return new ClassicLayoutManager();
    }

    protected void exportReport() throws Exception {
//        ReportExporter.exportReport(jp, System.getProperty("user.dir") + "/target/reports/" + this.getClass().getName() + ".pdf");
//        exportToJRXML();
    }

    protected void exportToJRXML() throws Exception {
//        if (this.jr != null) {
//            DynamicJasperHelper.generateJRXML(this.jr, "UTF-8", System.getProperty("user.dir") + "/target/reports/" + this.getClass().getName() + ".jrxml");
//
//        }
//        else {
//            DynamicJasperHelper.generateJRXML(this.dr, this.getLayoutManager(), this.params, "UTF-8", System.getProperty("user.dir") + "/target/reports/" + this.getClass().getName() + ".jrxml");
//        }
    }

    protected void exportToHTML() throws Exception {
//        ReportExporter.exportReportHtml(this.jp, System.getProperty("user.dir") + "/target/reports/" + this.getClass().getName() + ".html");
    }

    /**
     * @return JRDataSource
     */
    protected static JRDataSource getDataSource() {
        Collection dummyCollection = TestRepositoryProducts.getDummyCollection();

        JRDataSource ds = new JRBeanCollectionDataSource(dummyCollection);		//Create a JRDataSource, the Collection used
        //here contains dummy hardcoded objects...
        return ds;
    }

    public int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
     }
}
