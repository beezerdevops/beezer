/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.utils.cube;

import com.beezer.web.models.cube.ProcessingCube;
import com.beezer.web.models.cube.ProcessingPartition;
import com.beezer.web.utils.GeneralUtils;
import com.beezer.web.utils.RandomUtils;
import com.crm.models.global.UserModel;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Ahmed El Badry Aug 15, 2022
 */
public class CubeProcessor {

    private LinkedHashMap<String, String> toProcessMap;
    private int partitionSize = 1;
    private int currentPartition = 1;
    private ProcessingCube processingCube;
    private UserModel requestingUser;

    public CubeProcessor(LinkedHashMap<String, String> toProcessMap, int partitionSize) {
        this.toProcessMap = toProcessMap;
        this.partitionSize = partitionSize;
        this.requestingUser = GeneralUtils.getLoggedUser();
        this.initializePartions();
    }

    private void initializePartions() {
        if (this.toProcessMap != null && !this.toProcessMap.isEmpty()) {
            this.processingCube = new ProcessingCube();
            int numberOfPartions = (int) Math.ceil(this.toProcessMap.size() / partitionSize);
            this.processingCube.setNumberOfPartitions(numberOfPartions);
            this.processingCube.setPartitionMap(new LinkedHashMap<>());

            this.partitionMap(toProcessMap);
        }
    }

    public LinkedHashMap<String, String> execute() {
        this.parallelCompute();
        LinkedHashMap<String, String> bvm = new LinkedHashMap<>();
        for (Map.Entry<String, ProcessingPartition> entry : this.processingCube.getPartitionMap().entrySet()) {
            bvm.putAll(entry.getValue().getProcessedValues());
        }
        return bvm;
    }

    private boolean parallelCompute() {
        boolean cubeNotCompleted = true;
        for (Map.Entry<String, ProcessingPartition> entry : this.processingCube.getPartitionMap().entrySet()) {
            CubeProcessingThread cubeProcessingThread = new CubeProcessingThread(this, entry.getValue());
            cubeProcessingThread.start();
        }

        while (cubeNotCompleted) {
            cubeNotCompleted = false;
            for (Map.Entry<String, ProcessingPartition> entry : this.processingCube.getPartitionMap().entrySet()) {
                if (!entry.getValue().isComplete()) {
                    cubeNotCompleted = true;
                    break;
                }
            }
        }

        return true;
    }

    private void partitionMap(LinkedHashMap<String, String> inProcessMap) {
        ProcessingPartition partition = new ProcessingPartition();
        partition.setPartitionId(RandomUtils.randomAlphanumeric(7));
        partition.setProcessingConfig(new LinkedHashMap<>());
        LinkedHashMap<String, String> updatedProcessMap = new LinkedHashMap<>(inProcessMap);
        int index = 1;
        for (Map.Entry<String, String> entry : inProcessMap.entrySet()) {
            if (index <= this.partitionSize) {
                partition.getProcessingConfig().put(entry.getKey(), entry.getValue());
                updatedProcessMap.remove(entry.getKey());
                index++;
            }
            else {
                break;
            }
        }
        this.processingCube.getPartitionMap().put(partition.getPartitionId(), partition);
        if (!updatedProcessMap.isEmpty()) {
            this.partitionMap(updatedProcessMap);
        }
    }

    public LinkedHashMap<String, String> getToProcessMap() {
        return toProcessMap;
    }

    public void setToProcessMap(LinkedHashMap<String, String> toProcessMap) {
        this.toProcessMap = toProcessMap;
    }

    public ProcessingCube getProcessingCube() {
        return processingCube;
    }

    public void setProcessingCube(ProcessingCube processingCube) {
        this.processingCube = processingCube;
    }

    public UserModel getRequestingUser() {
        return requestingUser;
    }

    public void setRequestingUser(UserModel requestingUser) {
        this.requestingUser = requestingUser;
    }

}
