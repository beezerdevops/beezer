/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.utils.cube;

import com.beezer.web.commons.Defines;
import com.beezer.web.models.cube.ProcessingPartition;
import com.beezer.web.utils.GeneralUtils;
import com.beezl.interpreter.BEEZLInterperter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ahmed El Badry Aug 15, 2022
 */
public class CubeProcessingThread extends Thread {

    private CubeProcessor cubeProcessor;
    private ProcessingPartition processingPartition;

    public CubeProcessingThread(CubeProcessor cubeProcessor, ProcessingPartition processingPartition) {
        this.cubeProcessor = cubeProcessor;
        this.processingPartition = processingPartition;
    }

    @Override
    public void run() {
        if (this.processingPartition != null && this.processingPartition.getProcessingConfig() != null) {
            BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
            beezlInterperter.setRequestingUser(cubeProcessor.getRequestingUser());
            LinkedHashMap<String, String> bvm = null;
            for (Map.Entry<String, String> entry : processingPartition.getProcessingConfig().entrySet()) {
                beezlInterperter.addDynamicField(entry.getKey(), entry.getValue());
            }

            try {
                bvm = beezlInterperter.evaluate();
                processingPartition.setProcessedValues(new LinkedHashMap<>(bvm));
                processingPartition.setComplete(true);
                this.cubeProcessor.getProcessingCube().getPartitionMap().put(processingPartition.getPartitionId(), processingPartition);
            }
            catch (Exception ex) {
                Logger.getLogger(CubeProcessingThread.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
