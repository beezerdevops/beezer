/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.utils;

import com.beezer.web.beans.AccessBean;
import com.beezer.web.beans.IntegrationAccessBean;
import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.handler.UnstructuredHandler;
import com.beezer.web.models.SelectionModel;
import com.crm.models.global.CompanyModel;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.global.UserModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.application.AppModuleConfigModel;
import com.crm.models.internal.application.ApplicationModel;
import com.crm.models.internal.components.EditableTableComponent;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.filter.dynamic.FilterConfig;
import com.crm.models.internal.filter.dynamic.FilterHolder;
import com.crm.models.internal.forms.FieldBlockModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.forms.FormModel;
import com.crm.models.requests.UnstructuredRequest;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.responses.UnstructuredResponse;
import com.crm.models.responses.managers.ModuleFieldResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class GeneralUtils {

    public static String getDateFormat(String date) {
        String returnFormat = "";
        String[] formats = {"MMM dd, yyyy", "yyyy-MM-dd", "dd/MM/yyyy", "dd/MM/yyyy HH:mm:ss", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss", "EEE MMM dd hh:mm:ss zzz yyyy", "E MMM dd HH:mm:ss Z yyyy"};
        if (date != null) {
            for (String parse : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(parse);
                try {
                    sdf.parse(date);
                    returnFormat = parse;
                }
                catch (ParseException e) {
                }
            }
        }
        return returnFormat;
    }

    public static String getDateFromMillis(String millis) {
        if (millis == null || millis.isEmpty()) {
            millis = "0";
        }
        Date date = new Date(Long.parseLong(millis));
//        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return sdf.format(date);
    }

    public static String getDateFromMillisDBFormat(String millis) {
        if (millis == null || millis.isEmpty()) {
            millis = "0";
        }
        Date date = new Date(Long.parseLong(millis));
//        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

    public static String getDateNoTimeFromMillisDBFormat(String millis) {
        if (millis == null || millis.isEmpty()) {
            millis = "0";
        }
        Date date = new Date(Long.parseLong(millis));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static String getDateNoTimeFromMillis(String millis) {
        if (millis == null || millis.isEmpty()) {
            millis = "0";
        }
        Date date = new Date(Long.parseLong(millis));
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return sdf.format(date);
    }

    public static String getMillisFromDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat sdfPF = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy");
        if (date == null || date.isEmpty()) {
            Date dateD = new Date();
            date = sdfPF.format(dateD);
        }

        try {
            Date dateD = sdfPF.parse(date);
            return String.valueOf(dateD.getTime());
        }
        catch (ParseException ex) {
            Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String getMillisFromSimpleDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//        SimpleDateFormat sdfPF = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy");
        if (date == null || date.isEmpty()) {
            Date dateD = new Date();
            date = sdf.format(dateD);
        }

        try {
            Date dateD = sdf.parse(date);
            return String.valueOf(dateD.getTime());
        }
        catch (ParseException ex) {
            Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static LocalDateTime getLocalDateTimeFromDateString(String dateString) {
        if (dateString != null && !dateString.isEmpty()) {
            String dateFormat = getDateFormat(dateString);
            try {
                Date date = new SimpleDateFormat(dateFormat).parse(dateString);
                return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            }
            catch (ParseException ex) {
                Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return LocalDateTime.now();
    }

    public static String convertPFDateFormate(Date dateIn) {
        DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = destDf.format(dateIn);
        return dateStr;
    }

    public static String convertPFDateFormate(String dateIn) {
        DateFormat destDf = new SimpleDateFormat("dd/MM/yyyy");
        String ogFormat = getDateFormat(dateIn);
        DateFormat ogDF = new SimpleDateFormat(ogFormat);
        try {
            Date ogDate = ogDF.parse(dateIn);
            String dateStr = destDf.format(ogDate);
            return dateStr;
        }
        catch (ParseException ex) {
            Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    public static String serializeRequest(Object responseObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(responseObject);
        }
        catch (IOException ex) {
            return null;
        }
    }

    public String trimTrailingZeros(String input) {
        return input.replaceAll("[0]*$", "").replaceAll(".$", "");
    }
    
    public static void setActiveViewBean(BeanFramework beanFramework){
//        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
//        AccessBean accessBean = (AccessBean) sessionMap.get("accessBean");
//        Defines.ACCESS_LEDGER.get(GeneralUtils.getLoggedUser().getUserId()).setActiveView(beanFramework);
//        if (accessBean != null) {
//            accessBean.setActiveView(beanFramework);
//        }
    }

    public static UserModel getLoggedUser() {
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        AccessBean accessBean = (AccessBean) sessionMap.get("accessBean");
        if (accessBean != null) {
            UserModel user = accessBean.getUserModel();
            if (user == null) {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml?faces-redirect=true");
                    FacesContext.getCurrentInstance().responseComplete();
                    return null;
                }
                catch (IOException ex) {
                    Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return accessBean.getUserModel();
        }
        else {
            IntegrationAccessBean integrationAccessBean = (IntegrationAccessBean) sessionMap.get("integrationAccessBean");
            if (integrationAccessBean != null) {
                UserModel user = integrationAccessBean.getUserModel();
                if (user == null) {
                    try {
                        FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
                        return null;
                    }
                    catch (IOException ex) {
                        Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return integrationAccessBean.getUserModel();
            }
        }
        return null;
    }
    
    public static String getSessionTimeZone() {
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        AccessBean accessBean = (AccessBean) sessionMap.get("accessBean");
        if (accessBean != null) {
            String timezone = accessBean.getCompanyTimezone();
            if (timezone == null) {
//                try {
//                    FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml?faces-redirect=true");
//                    FacesContext.getCurrentInstance().responseComplete();
//                    return null;
//                }
//                catch (IOException ex) {
//                    Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }
            return timezone;
        }
        return null;
    }

    public static ApplicationModel getActiveApplication() {
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        AccessBean accessBean = (AccessBean) sessionMap.get("accessBean");
        ApplicationModel am = accessBean.getActiveApplication();
        if (am == null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
                return null;
            }
            catch (IOException ex) {
                Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return accessBean.getActiveApplication();
    }

    public static void refreshMenuIconStyles() {
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        AccessBean accessBean = (AccessBean) sessionMap.get("accessBean");
        if (accessBean != null) {
            accessBean.loadIconStyles();
        }
    }

    public static CompanyModel getCompany() {
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        AccessBean accessBean = (AccessBean) sessionMap.get("accessBean");
        CompanyModel company = accessBean.getCompanyModel();
        if (company == null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
                return null;
            }
            catch (IOException ex) {
                Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return company;
    }

    public static String getFileExtenstion(String fileName) {
        String ext = fileName.substring(fileName.lastIndexOf('.'), fileName.length());
        return ext;
    }

    public static ModuleFieldModel getFieldFromRecord(RecordModel rm, String fieldName) {
        if (rm != null) {
            for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                if (entry.getValue().getFieldName().equals(fieldName)) {
                    return entry.getValue();
                }
            }
        }
        return null;
    }

    public static ModuleFieldModel getKeyFieldFromRecord(RecordModel rm) {
        if (rm != null) {
            for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                if (entry.getValue().isIsKey()) {
                    return entry.getValue();
                }
            }
        }
        return null;
    }

    public static RecordModel setFieldValueInRecord(RecordModel rm, String fieldName, String value) {
        if (rm != null) {
            for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                if (entry.getValue().getFieldName().equals(fieldName)) {
                    entry.getValue().setCurrentValue(value);
                    return rm;
                }
            }
        }
        return null;
    }

    public static ModuleFieldModel getFieldType(String fieldName, int moduleId) {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("module_configuration.module_id", moduleId));
            fm.setFilter("AND", new FilterType("field_name", fieldName));
            request.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ModuleFieldResponse response = requestHandler.getFieldSet(request);
            if (response.getErrorCode() == 1000) {
                return response.getReturnList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static ModuleFieldModel getKeyFieldType(int moduleId) {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("module_configuration.module_id", moduleId));
            fm.setFilter("AND", new FilterType("is_key", true));
            request.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ModuleFieldResponse response = requestHandler.getFieldSet(request);
            if (response.getErrorCode() == 1000) {
                return response.getReturnList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(GeneralUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String getApiName(String moduleId) {
        String apiName = null;
        switch (moduleId) {
            case "1":
                apiName = "GenericMasterAPI";
                break;
            case "2":
                apiName = "GenericMasterAPI";
                break;
            case "3":
                apiName = "GenericMasterAPI";
                break;
            case "4":
                apiName = "GetDealsWS";
                break;
            case "5":
                apiName = "GetDealDetailsWS";
                break;
            case "6":
                apiName = "GenericMasterAPI";
                break;
            case "7":
                apiName = "GenericMasterAPI";
                break;
            case "8":
                apiName = "GetCategoriesWS";
                break;
            case "9":
                apiName = "GenericMasterAPI";
                break;
            default:
                apiName = "GenericMasterAPI";
                break;
        }
        return apiName;
    }

    public static String getBean(String moduleId) {
        String bean = null;
        switch (moduleId) {
            case "1":
                bean = "GetAccountsWS";
                break;
            case "2":
                bean = "GetContactsWS";
                break;
            case "3":
                bean = "GetProductsWS";
                break;
            case "4":
                bean = "GetDealsWS";
                break;
            case "5":
                bean = "GetDealDetailsWS";
                break;
            case "6":
                bean = "customersBean";
                break;
            case "7":
                bean = "DealMasterWS";
                break;
            case "8":
                bean = "GetCategoriesWS";
                break;
            case "9":
                bean = "GetSuppliersWS";
                break;
        }
        return bean;
    }

    public static EditableTableComponent deserializeETC(String config) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        EditableTableComponent reqObject = mapper.readValue(config, EditableTableComponent.class);
        return reqObject;
    }

    public static LinkedHashMap<String, String> deserializeKVMap(String kvContent) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        LinkedHashMap<String, String> reqObject = mapper.readValue(kvContent, LinkedHashMap.class);
        return reqObject;
    }

    public static RecordModel deepCopyRecord(RecordModel recordModel) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String str = mapper.writeValueAsString(recordModel);
            RecordModel rm = mapper.readValue(str, RecordModel.class);
            return rm;
        }
        catch (IOException ex) {
            return null;
        }
    }

    public static int generateRandomId() {
        Random random = new Random();
        return random.nextInt(999999) + 100000;
    }

    public static void loadGeneralConfigurations(String appPath) throws Exception {
        Properties properties;
        appPath = appPath.replaceAll("%20", " ");//Remove empty spaces if available
        appPath = appPath + File.separatorChar + "WEB-INF" + File.separatorChar + "configurations" + File.separatorChar + "General_Configurations.properties";
        properties = new Properties();
        try {
            properties.load(new FileInputStream(appPath));
        }
        catch (IOException ex) {
            System.err.println("Failed to general configuration file. EX: " + ex.getMessage());
            Defines.GENERAL_CONFIG_MAP = null;
            throw new Exception("Failed to general configuration file. EX: " + ex.getMessage());
        }

        Defines.GENERAL_CONFIG_MAP = new HashMap<>();
        for (String key : properties.stringPropertyNames()) {
            String value = properties.getProperty(key);
            Defines.GENERAL_CONFIG_MAP.put(key, value);
        }
    }

    public static String generateDynamicResourceURL(String resource) {
        if (Defines.GENERAL_CONFIG_MAP.get("content.source") != null && Defines.GENERAL_CONFIG_MAP.get("content.source").equals("amazons3")) {
            UnstructuredRequest unstructuredRequest = new UnstructuredRequest();
            unstructuredRequest.setRequestorUserModel(GeneralUtils.getLoggedUser());
            unstructuredRequest.setUnstructuredId(resource);
            unstructuredRequest.setContext("CONFIG");
            unstructuredRequest.setObjectId(0);
            UnstructuredHandler unstructuredHandler = new UnstructuredHandler();
            UnstructuredResponse unstructuredResponse = unstructuredHandler.getImage(unstructuredRequest);
            if (unstructuredResponse.getErrorCode() == 1000 && unstructuredResponse.getReturnList() != null
                    && !unstructuredResponse.getReturnList().isEmpty()) {
                return unstructuredResponse.getReturnList().get(0).getLink();
            }
        }
        else {
            String url = "";
            if (Defines.GENERAL_CONFIG_MAP.get("SERVER.NAME") != null && !Defines.GENERAL_CONFIG_MAP.get("SERVER.NAME").isEmpty()) {
                url = url + Defines.GENERAL_CONFIG_MAP.get("SERVER.NAME") + "/" + "content" + "/" + resource;
            }
            return url;
        }

        return null;
    }

    public static HashMap<String, String> generateBulkDynamicResourceURL(String clause, ArrayList<AppModuleConfigModel> completeModuleList) {
        if (Defines.GENERAL_CONFIG_MAP.get("content.source") != null && Defines.GENERAL_CONFIG_MAP.get("content.source").equals("amazons3")) {
            UnstructuredRequest unstructuredRequest = new UnstructuredRequest();
            unstructuredRequest.setRequestorUserModel(GeneralUtils.getLoggedUser());
            unstructuredRequest.setRequestActionType("5");
            unstructuredRequest.setClause(clause);
            unstructuredRequest.setContext("CONFIG");
            unstructuredRequest.setObjectId(0);
            UnstructuredHandler unstructuredHandler = new UnstructuredHandler();
            UnstructuredResponse unstructuredResponse = unstructuredHandler.executeForManager(unstructuredRequest);
            if (unstructuredResponse.getErrorCode() == 1000 && unstructuredResponse.getReturnList() != null
                    && !unstructuredResponse.getReturnList().isEmpty()) {
                HashMap<String, String> mapOfUrls = new HashMap<>();
                for (UnstructuredModel um : unstructuredResponse.getReturnList()) {
                    for (AppModuleConfigModel amcm : completeModuleList) {
                        if (amcm.getDynamicIcon() != null && um.getUnstructuredId() != null) {
                            if (amcm.getDynamicIcon().equals(um.getUnstructuredId())) {
                                mapOfUrls.put(amcm.getModuleDefaultName().replaceAll("\\s+", "_").toLowerCase(), um.getLink());
                                break;
                            }
                        }
                    }
                }
                return mapOfUrls;
            }
        }
        return null;
    }

    public static int generateIntegerId() {
        int x = Integer.parseInt(RandomUtils.randomNumeric(5));
        return Integer.parseInt(RandomUtils.randomNumeric(8));
    }

    public static String getFieldValueFromForm(FormModel form, String fieldName, HashMap<String, SelectionModel> selectionMap) {
        if (form != null && fieldName != null && !fieldName.isEmpty()) {
            for (FieldBlockModel fbm : form.getBlockList()) {
                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                    if (flm.getComponentType().equals("NORM")) {
                        if (flm.getFieldObject().getFieldName().equals(fieldName)) {
                            if (flm.getFieldObject().getFieldType().equals("REL")) {
                                return selectionMap.get(fieldName).getSelectedValue();
                            }
                            else {
                                return flm.getFieldObject().getCurrentValue();
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
    
    public static String convertHolderToClause(FilterHolder filterHolder) {
        if (filterHolder != null) {
            FilterManager filterManager = new FilterManager();
            int index = 0;
            if (filterHolder.getANDFilterList() != null || filterHolder.getORFilterList() != null) {
                filterManager.openContainerBraces("AND");
            }
            else {
                return filterManager.getClause();
            }

            if (filterHolder.getANDFilterList() != null && !filterHolder.getANDFilterList().isEmpty()) {
                for (FilterConfig filterConfig : filterHolder.getANDFilterList()) {
                    FilterType filterType;
                    String qualifiedFieldName = "";
                    switch (filterConfig.getFieldModel().getFieldType()) {
                        case "BOOLEAN":
                            qualifiedFieldName = filterConfig.getFieldModel().getTableName() + "." + filterConfig.getFieldModel().getFieldName();
                            filterType = new FilterType(qualifiedFieldName, filterConfig.getOperator(), Boolean.valueOf(filterConfig.getFieldModel().getCurrentValue()));
                            break;
                        default:
                            qualifiedFieldName = filterConfig.getFieldModel().getTableName() + "." + filterConfig.getFieldModel().getFieldName();
                            filterType = new FilterType(qualifiedFieldName, filterConfig.getOperator(), filterConfig.getFieldModel().getCurrentValue());
                    }

                    if (index == 0) {
                        filterManager.setFilter(filterType);
                    }
                    else {
                        filterManager.setFilter("AND", filterType);
                    }
                    index++;
                }
                filterManager.closeContainerBraces();
            }

            index = 0;
            if (filterHolder.getORFilterList() != null && !filterHolder.getORFilterList().isEmpty()) {
                filterManager.openContainerBraces("OR");
                String qualifiedFieldName = "";
                for (FilterConfig filterConfig : filterHolder.getORFilterList()) {
                    FilterType filterType;
                    switch (filterConfig.getFieldModel().getFieldType()) {
                        case "BOOLEAN":
                            qualifiedFieldName = filterConfig.getFieldModel().getTableName() + "." + filterConfig.getFieldModel().getFieldName();
                            filterType = new FilterType(qualifiedFieldName, filterConfig.getOperator(), Boolean.valueOf(filterConfig.getFieldModel().getCurrentValue()));
                            break;
                        default:
                            qualifiedFieldName = filterConfig.getFieldModel().getTableName() + "." + filterConfig.getFieldModel().getFieldName();
                            filterType = new FilterType(qualifiedFieldName, filterConfig.getOperator(), filterConfig.getFieldModel().getCurrentValue());
                    }

                    if (index == 0) {
                        filterManager.setFilter(filterType);
                    }
                    else {
                        filterManager.setFilter("OR", filterType);
                    }
                    index++;
                }
                filterManager.closeContainerBraces();
            }
            return filterManager.getClause();
        }
        else {
            return null;
        }
    }

}
