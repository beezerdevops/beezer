/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.utils;

import com.crm.models.internal.RecordModel;
import java.util.ArrayList;

/**
 *
 * @author badry
 */
public class RecordAggregateFunctions {

    public static float sumOfField(ArrayList<RecordModel> recordList, String field) {
        float sumValue = 0;
        if (recordList != null && field != null) {
            for (RecordModel rm : recordList) {
                if (rm.getFieldValueMap().get(field) != null && rm.getFieldValueMap().get(field).getCurrentValue() != null) {
                    float val = 0;
                    if(rm.getFieldValueMap().get(field).getCurrentValue() != null && !rm.getFieldValueMap().get(field).getCurrentValue().isEmpty()){
                        String valClean = rm.getFieldValueMap().get(field).getCurrentValue().replace(",", "");
                        val = Float.valueOf(valClean);
                    }
                    sumValue = sumValue + val;
                }
            }
            return sumValue;
        }
        return 0;
    }

    public static float conditionalSumOfField(ArrayList<RecordModel> recordList, String field, String conditionalField, String conditionalValue, String secondConditionalField, String secondConditionalValue) {
        float sumValue = 0;
        if (recordList != null && field != null) {
            for (RecordModel rm : recordList) {
                if (rm.getFieldValueMap().get(conditionalField) != null && rm.getFieldValueMap().get(conditionalField).getCurrentValue() != null
                        && rm.getFieldValueMap().get(conditionalField).getCurrentValue().equals(conditionalValue) 
                        && rm.getFieldValueMap().get(secondConditionalField) != null 
                        && rm.getFieldValueMap().get(secondConditionalField).getCurrentValue() != null
                        && rm.getFieldValueMap().get(secondConditionalField).getCurrentValue().equals(secondConditionalValue)) {
                    if (rm.getFieldValueMap().get(field) != null && rm.getFieldValueMap().get(field).getCurrentValue() != null && !rm.getFieldValueMap().get(field).getCurrentValue().isEmpty()) {
                        sumValue = sumValue + Float.valueOf(rm.getFieldValueMap().get(field).getCurrentValue());
                    }
                    else {
                        sumValue = sumValue + 0;
                    }
                }
            }
            return sumValue;
        }
        return 0;
    }
}
