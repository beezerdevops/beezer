/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler.apps;

import com.beezer.web.handler.*;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.ApplicationRequest;
import com.crm.models.responses.managers.ApplicationResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class ApplicationsHandler {

    public ApplicationResponse execute(ApplicationRequest request)  {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("AppsMasterAPI");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(ApplicationsHandler.class.getName()).log(Level.SEVERE, null, ex);
            ApplicationResponse model = new ApplicationResponse();
            model.setErrorCode(2056);
            model.setErrorMessage("Error in deserialization");
            return model;
        }
    }

    protected ApplicationResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ApplicationResponse response = mapper.readValue(responseJson, ApplicationResponse.class);
        return response;
    }
}
