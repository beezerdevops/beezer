/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler.apps.sales;

import com.beezer.web.handler.*;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.apps.sales.InsightsRequest;
import com.crm.models.responses.apps.sales.InsightsResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class InsightsHandler {

    public InsightsResponse execute(InsightsRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("GetSalesInsightsWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected InsightsResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        InsightsResponse response = mapper.readValue(responseJson, InsightsResponse.class);
        return response;
    }
}
