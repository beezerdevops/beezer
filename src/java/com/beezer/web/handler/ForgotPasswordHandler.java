/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class ForgotPasswordHandler {

    public ResponseModel executeForManager(RequestModel request) {
//        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("ForgotPasswordMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(ForgotPasswordHandler.class.getName()).log(Level.SEVERE, null, ex);
            ResponseModel response = new ResponseModel();
            response.setErrorCode(2056);
            response.setErrorMessage("Technical Error - EX: " + ex.getMessage());
            return response;
        }
    }

    protected ResponseModel deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ResponseModel response = mapper.readValue(responseJson, ResponseModel.class);
        return response;
    }
}
