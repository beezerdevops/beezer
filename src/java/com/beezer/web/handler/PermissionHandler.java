/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.PermissionRequest;
import com.crm.models.responses.managers.PermissionsResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class PermissionHandler {

    public PermissionsResponse execute(PermissionRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if(request.getLimit() == 0){
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("PermissionsMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected PermissionsResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        PermissionsResponse response = mapper.readValue(responseJson, PermissionsResponse.class);
        return response;
    }
}
