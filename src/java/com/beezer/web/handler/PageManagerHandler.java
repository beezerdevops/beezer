/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.PageRequest;
import com.crm.models.responses.managers.PageResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class PageManagerHandler {

    public PageResponse executeRequest(PageRequest request) {
        try {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
            if (request.getLimit() == 0) {
                request.setLimit(10);
            }
            String reqJson = this.serializeRequest(request);
            ApiHandler apiHandler = new ApiHandler("PageManagerMasterWS");
            String responseJson = apiHandler.execute(reqJson);
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(PageManagerHandler.class.getName()).log(Level.SEVERE, null, ex);
            PageResponse model = new PageResponse();
            model.setErrorCode(2056);
            model.setErrorMessage("Error in deserialization");
            return model;
        }
    }

    protected PageResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        PageResponse response = mapper.readValue(responseJson, PageResponse.class);
        return response;
    }

    protected String serializeRequest(Object responseObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(responseObject);
        }
        catch (IOException ex) {
            return null;
        }
    }

}
