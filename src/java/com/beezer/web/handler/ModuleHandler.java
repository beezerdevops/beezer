/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.serviceModels.ResponseModel;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class ModuleHandler {
    
    public ModuleManagerResponse managerExecutor(ModuleManagerRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
//        request.setAppId(GeneralUtils.getActiveApplication().getAppId());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("ModuleManagerMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeManagerResponse(responseJson);
    }
    
    public ModuleFieldResponse fieldExecutor(ModuleFieldRequest request){
//        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getApiKey() != null && !request.getApiKey().isEmpty()) {
            request.setRequestorUserModel(null);
        }
        else {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        }
//        request.setAppId(GeneralUtils.getActiveApplication().getAppId());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("ModuleFieldsMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeFieldResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(ModuleHandler.class.getName()).log(Level.SEVERE, null, ex);
            ModuleFieldResponse model = new ModuleFieldResponse();
            model.setErrorCode(2056);
            model.setErrorMessage("Error in deserialization | Ex: " + ex.getMessage());
            return model;
        }
    }

    protected ModuleManagerResponse deserializeManagerResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ModuleManagerResponse response = mapper.readValue(responseJson, ModuleManagerResponse.class);
        return response;
    }
    
    protected ModuleFieldResponse deserializeFieldResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ModuleFieldResponse response = mapper.readValue(responseJson, ModuleFieldResponse.class);
        return response;
    }
}
