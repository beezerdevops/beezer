/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class RequestHandler {

    public ResponseModel executeRequest(RequestModel request, String apiName) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if(request.getKeyObjectMap() == null){
            request.setKeyObjectMap(new HashMap<>());
        }
        
        request.getKeyObjectMap().put("timezone", GeneralUtils.getSessionTimeZone());
        
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
//        request.setAppId(GeneralUtils.getActiveApplication().getAppId());
        String reqJson = this.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler(apiName);
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    public ResponseModel executeRequest(RequestModel request) {
        try {
//            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
            if (request.getApiKey() != null && !request.getApiKey().isEmpty()) {
                request.setRequestorUserModel(null);
            }
            else {
                request.setRequestorUserModel(GeneralUtils.getLoggedUser());
            }

            if (request.getLimit() == 0) {
                request.setLimit(10);
            }
            else if (request.getLimit() == 999) {
                request.setLimit(0);
            }

//            request.setAppId(GeneralUtils.getActiveApplication().getAppId());
            String reqJson = this.serializeRequest(request);
            ApiHandler apiHandler = new ApiHandler("GenericMasterAPI");
            String responseJson = apiHandler.execute(reqJson);
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(RequestHandler.class.getName()).log(Level.SEVERE, null, ex);
            ResponseModel model = new ResponseModel();
            model.setErrorCode(2056);
            model.setErrorMessage("Error in deserialization");
            return model;
        }
    }

    public ResponseModel executeRequest(RequestModel request, String apiName, boolean limitless) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (limitless) {
            request.setLimit(0);
        }
        else {
            if (request.getLimit() == 0) {
                request.setLimit(10);
            }
        }
//        request.setAppId(GeneralUtils.getActiveApplication().getAppId());
        String reqJson = this.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler(apiName);
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    public ResponseModel executeGetRequest(RequestModel request) throws Exception {
        String apiName = null;
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        switch (request.getRequestingModule()) {
            default:
                apiName = "GenericMasterAPI";
                break;
        }
//        request.setAppId(GeneralUtils.getActiveApplication().getAppId());
        String reqJson = this.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler(apiName);
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    public ModuleFieldResponse getFieldSet(ModuleFieldRequest request) throws Exception {
//        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getApiKey() != null && !request.getApiKey().isEmpty()) {
            request.setRequestorUserModel(null);
        }
        else {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        }
//        request.setAppId(GeneralUtils.getActiveApplication().getAppId());
        String reqJson = this.serializeRequest(request);
        ObjectMapper mapper = new ObjectMapper();
        ApiHandler apiHandler = new ApiHandler("GetModuleFieldsWS");
        String responseJson = apiHandler.execute(reqJson);
        ModuleFieldResponse response;
        response = mapper.readValue(responseJson, ModuleFieldResponse.class);
        return response;
    }

    protected ResponseModel deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ResponseModel response = mapper.readValue(responseJson, ResponseModel.class);
        return response;
    }

    protected String serializeRequest(Object responseObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(responseObject);
        }
        catch (IOException ex) {
            return null;
        }
    }

}
