/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.LabelPrinterRequest;
import com.crm.models.responses.managers.LabelPrinterResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class LabelPrinterHandler {

    public LabelPrinterResponse executeRequest(LabelPrinterRequest request) {
        try {
            if (request.getRequestorUserModel() == null) {
                request.setRequestorUserModel(GeneralUtils.getLoggedUser());
            }
            if (request.getLimit() == 0) {
                request.setLimit(10);
            }
            String reqJson = this.serializeRequest(request);
            ApiHandler apiHandler = new ApiHandler("LabelPrinterMasterWS");
            String responseJson = apiHandler.execute(reqJson);
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(LabelPrinterHandler.class.getName()).log(Level.SEVERE, null, ex);
            LabelPrinterResponse model = new LabelPrinterResponse();
            model.setErrorCode(2056);
            model.setErrorMessage("Error in deserialization");
            return model;
        }
    }

    protected LabelPrinterResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        LabelPrinterResponse response = mapper.readValue(responseJson, LabelPrinterResponse.class);
        return response;
    }

    protected String serializeRequest(Object responseObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(responseObject);
        }
        catch (IOException ex) {
            return null;
        }
    }

}
