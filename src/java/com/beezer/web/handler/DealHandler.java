/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.DealRequest;
import com.crm.models.responses.DealResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class DealHandler {

    public DealResponse dealExecutor(DealRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if(request.getLimit() == 0){
            request.setLimit(10);
        }
        request.setAppId(GeneralUtils.getActiveApplication().getAppId());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("DealMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }
    
    public DealResponse dealDetailExecutor(DealRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if(request.getLimit() == 0){
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("DealDetailsMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected DealResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        DealResponse response = mapper.readValue(responseJson, DealResponse.class);
        return response;
    }
}
