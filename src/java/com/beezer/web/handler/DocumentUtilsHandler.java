/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.pdf.DocumentUtilRequest;
import com.crm.models.responses.pdf.DocumentUtilResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class DocumentUtilsHandler {

    public DocumentUtilResponse executeRequest(DocumentUtilRequest request) {
        try {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
            if (request.getLimit() == 0) {
                request.setLimit(10);
            }
            String reqJson = this.serializeRequest(request);
            ApiHandler apiHandler = new ApiHandler("DocumentUtilsMasterWS");
            String responseJson = apiHandler.execute(reqJson);
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(DocumentUtilsHandler.class.getName()).log(Level.SEVERE, null, ex);
            DocumentUtilResponse model = new DocumentUtilResponse();
            model.setErrorCode(2056);
            model.setErrorMessage("Error in deserialization");
            return model;
        }
    }

    protected DocumentUtilResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        DocumentUtilResponse response = mapper.readValue(responseJson, DocumentUtilResponse.class);
        return response;
    }

    protected String serializeRequest(Object responseObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(responseObject);
        }
        catch (IOException ex) {
            return null;
        }
    }

}
