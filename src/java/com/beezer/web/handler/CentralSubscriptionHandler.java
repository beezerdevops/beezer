/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.SubscriptionRequest;
import com.crm.models.responses.managers.SubscriptionResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class CentralSubscriptionHandler {

    public SubscriptionResponse execute(SubscriptionRequest request)  {
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("CentralSubscriptionMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(CentralSubscriptionHandler.class.getName()).log(Level.SEVERE, null, ex);
            SubscriptionResponse model = new SubscriptionResponse();
            model.setErrorCode(2056);
            model.setErrorMessage("We are facing an error at the moment | Ex: " + ex.getMessage());
            return model;
        }
    }

    protected SubscriptionResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        SubscriptionResponse response = mapper.readValue(responseJson, SubscriptionResponse.class);
        return response;
    }
}
