/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.DocTemplateRequest;
import com.crm.models.responses.DocTemplateResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class DocTemplateHandler {

    public DocTemplateResponse executeRequest(DocTemplateRequest request) {
        try {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
            if (request.getLimit() == 0) {
                request.setLimit(10);
            }
            String reqJson = this.serializeRequest(request);
            ApiHandler apiHandler = new ApiHandler("DocTemplatesMasterAPI");
            String responseJson = apiHandler.execute(reqJson);
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(DocTemplateHandler.class.getName()).log(Level.SEVERE, null, ex);
            DocTemplateResponse model = new DocTemplateResponse();
            model.setErrorCode(2056);
            model.setErrorMessage("Error in deserialization");
            return model;
        }
    }

    protected DocTemplateResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        DocTemplateResponse response = mapper.readValue(responseJson, DocTemplateResponse.class);
        return response;
    }

    protected String serializeRequest(Object responseObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(responseObject);
        }
        catch (IOException ex) {
            return null;
        }
    }

}
