/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.RolesRequest;
import com.crm.models.responses.managers.RoleResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class RoleHandler {

    public RoleResponse execute(RolesRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if(request.getLimit() == 0){
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("RolesMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected RoleResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        RoleResponse response = mapper.readValue(responseJson, RoleResponse.class);
        return response;
    }
}
