/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.managers.RelationResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class RelationHandler {

    public RelationResponse getRelation(RelationRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("GetRelationWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    public RelationResponse relationExecutor(RelationRequest request) {
        try {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
            String reqJson = GeneralUtils.serializeRequest(request);
            ApiHandler apiHandler = new ApiHandler("RelationManagerMasterWS");
            String responseJson = apiHandler.execute(reqJson);
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(RelationHandler.class.getName()).log(Level.SEVERE, null, ex);
            RelationResponse relationResponse = new RelationResponse();
            relationResponse.setErrorCode(2056);
            relationResponse.setErrorMessage("Error in deserialization");
            return relationResponse;
        }
    }

    protected RelationResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        RelationResponse response = mapper.readValue(responseJson, RelationResponse.class);
        return response;
    }
}
