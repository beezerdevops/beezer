/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.MailServerRequest;
import com.crm.models.responses.managers.MailServerResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class MailServerHandler {

    public MailServerResponse execute(MailServerRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if(request.getLimit() == 0){
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("MailServerMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected MailServerResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        MailServerResponse response = mapper.readValue(responseJson, MailServerResponse.class);
        return response;
    }
}
