/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.DataTableRequest;
import com.crm.models.responses.DataTableResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class TableSettingsHandler {

    public DataTableResponse execute(DataTableRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
//        request.setAppId(GeneralUtils.getActiveApplication().getAppId());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("TableSettingsMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected DataTableResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        DataTableResponse response = mapper.readValue(responseJson, DataTableResponse.class);
        return response;
    }
}
