/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.ReportRequest;
import com.crm.models.responses.ReportResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class ReportHandler {

    public ReportResponse execute(ReportRequest request) {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("ReportsMasterAPI");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(ReportHandler.class.getName()).log(Level.SEVERE, null, ex);
            ReportResponse reportResponse = new ReportResponse();
            reportResponse.setErrorCode(2056);
            reportResponse.setErrorMessage(ex.getMessage());
            return reportResponse;
        }
    }

    protected ReportResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ReportResponse response = mapper.readValue(responseJson, ReportResponse.class);
        return response;
    }
}
