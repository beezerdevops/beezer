/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.TasksRequest;
import com.crm.models.responses.managers.TasksResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class TaskHandler {

    public TasksResponse execute(TasksRequest request) {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("TasksMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(TaskHandler.class.getName()).log(Level.SEVERE, null, ex);
            TasksResponse tasksResponse = new TasksResponse();
            tasksResponse.setErrorCode(2056);
            tasksResponse.setErrorMessage("Error in deserialization from handler");
            return tasksResponse;
        }
    }

    protected TasksResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        TasksResponse response = mapper.readValue(responseJson, TasksResponse.class);
        return response;
    }
}
