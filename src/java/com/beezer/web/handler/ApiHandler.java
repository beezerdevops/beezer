/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.commons.Defines;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

/**
 * Jersey REST client generated for REST resource:AddContactWS
 * [AddContactWS]<br>
 * USAGE:
 * <pre>
 *        AddContactApi client = new AddContactApi();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author badry
 */
public class ApiHandler {

    private final WebTarget webTarget;
    private final Client client;
    private static String BASE_URI;
//    private static final String BASE_URI = "http://localhost:8080/BeezerServiceMaster/generic";
//     private static final String BASE_URI = "http://beezerlabs.eu-central-1.elasticbeanstalk.com/generic";

    public ApiHandler(String apiName) {
        BASE_URI = Defines.GENERAL_CONFIG_MAP.get("services.server.path");
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path(apiName);
    }

    public String execute(Object requestEntity) throws ClientErrorException {
        return webTarget.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).post(javax.ws.rs.client.Entity.entity(requestEntity, javax.ws.rs.core.MediaType.APPLICATION_JSON), String.class);
    }

    public String getJson() throws ClientErrorException {
        WebTarget resource = webTarget;
        return resource.request(javax.ws.rs.core.MediaType.APPLICATION_JSON).get(String.class);
    }

    public void close() {
        client.close();
    }

}
