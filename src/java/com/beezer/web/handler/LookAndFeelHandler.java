/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.LookAndFeelRequest;
import com.crm.models.responses.LookAndFeelResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class LookAndFeelHandler {

    public LookAndFeelResponse executeRequest(LookAndFeelRequest request) {
        try {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
            request.setTenantId(GeneralUtils.getLoggedUser().getTenantId());
            if (request.getLimit() == 0) {
                request.setLimit(10);
            }
            String reqJson = this.serializeRequest(request);
            ApiHandler apiHandler = new ApiHandler("LookAndFeelMasterWS");
            String responseJson = apiHandler.execute(reqJson);
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(LookAndFeelHandler.class.getName()).log(Level.SEVERE, null, ex);
            LookAndFeelResponse model = new LookAndFeelResponse();
            model.setErrorCode(2056);
            model.setErrorMessage("Error in deserialization");
            return model;
        }
    }

    protected LookAndFeelResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        LookAndFeelResponse response = mapper.readValue(responseJson, LookAndFeelResponse.class);
        return response;
    }

    protected String serializeRequest(Object responseObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(responseObject);
        }
        catch (IOException ex) {
            return null;
        }
    }

}
