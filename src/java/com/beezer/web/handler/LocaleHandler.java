/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.LocaleRequest;
import com.crm.models.responses.managers.LocaleResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class LocaleHandler {

    public LocaleResponse execute(LocaleRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("LocaleMasterAPI");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected LocaleResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        LocaleResponse response = mapper.readValue(responseJson, LocaleResponse.class);
        return response;
    }
}
