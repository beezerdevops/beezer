/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.CompanyRequest;
import com.crm.models.responses.CompanyResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class CompaniesHandler {

    public CompanyResponse execute(CompanyRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("CompaniesMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected CompanyResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        CompanyResponse response = mapper.readValue(responseJson, CompanyResponse.class);
        return response;
    }
}
