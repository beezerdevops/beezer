/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class ProcessingHandler {

    public ResponseModel executeRequest(RequestModel request) {
        try {
            if (request.getRequestorUserModel() == null) {
                request.setRequestorUserModel(GeneralUtils.getLoggedUser());
            }
            if (request.getLimit() == 0) {
                request.setLimit(10);
            }
            String reqJson = this.serializeRequest(request);
            ApiHandler apiHandler = new ApiHandler("PreProcessAPI");
            String responseJson = apiHandler.execute(reqJson);
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(ProcessingHandler.class.getName()).log(Level.SEVERE, null, ex);
            ResponseModel serverResponse = new ResponseModel();
            serverResponse.setErrorCode(2056);
            serverResponse.setErrorMessage("Error in deserialization.");
            return serverResponse;
        }
    }

    protected ResponseModel deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ResponseModel response = mapper.readValue(responseJson, ResponseModel.class);
        return response;
    }

    protected String serializeRequest(Object responseObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(responseObject);
        }
        catch (IOException ex) {
            return null;
        }
    }

}
