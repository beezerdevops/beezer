/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.process.handler;

import com.beezer.web.handler.ApiHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.WorkflowRequest;
import com.crm.models.responses.managers.WorkflowResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class WorkflowHandler {

    public WorkflowResponse executeForManager(WorkflowRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("WorkflowMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    public WorkflowResponse executeForConfiguration(WorkflowRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("WorkflowConfigMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected WorkflowResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        WorkflowResponse response = mapper.readValue(responseJson, WorkflowResponse.class);
        return response;
    }
}
