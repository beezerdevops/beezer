/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.FormRequest;
import com.crm.models.responses.managers.FormResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class FormsHandler {

    public FormResponse formExecutor(FormRequest request) throws Exception {
//        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getApiKey() != null && !request.getApiKey().isEmpty()) {
            request.setRequestorUserModel(null);
        }
        else {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        }
//        request.setAppId(GeneralUtils.getActiveApplication().getAppId());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("FormManagerMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected FormResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        FormResponse response = mapper.readValue(responseJson, FormResponse.class);
        return response;
    }
}
