/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.AccessRequest;
import com.crm.models.responses.managers.AccessResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class AccessHandler {

    public AccessResponse accessExcecutor(AccessRequest request) throws Exception {
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("AuthenticationAPI");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected AccessResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        AccessResponse response = mapper.readValue(responseJson, AccessResponse.class);
        return response;
    }
}
