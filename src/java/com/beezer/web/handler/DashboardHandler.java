/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.DashboardRequest;
import com.crm.models.responses.managers.DashboardResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class DashboardHandler {

    public DashboardResponse executeRequest(DashboardRequest request) {
        try {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
            if (request.getLimit() == 0) {
                request.setLimit(10);
            }
            request.setAppId(GeneralUtils.getActiveApplication().getAppId());
            String reqJson = this.serializeRequest(request);
            ApiHandler apiHandler = new ApiHandler("DashboardWidgetsMasterWS");
            String responseJson = apiHandler.execute(reqJson);
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(DashboardHandler.class.getName()).log(Level.SEVERE, null, ex);
            DashboardResponse model = new DashboardResponse();
            model.setErrorCode(2056);
            model.setErrorMessage("Error in deserialization");
            return model;
        }
    }

    protected DashboardResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        DashboardResponse response = mapper.readValue(responseJson, DashboardResponse.class);
        return response;
    }

    protected String serializeRequest(Object responseObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(responseObject);
        }
        catch (IOException ex) {
            return null;
        }
    }

}
