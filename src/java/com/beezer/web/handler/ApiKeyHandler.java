/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.crm.models.requests.managers.APIManagerRequest;
import com.crm.models.responses.managers.APIManagerResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class ApiKeyHandler {

    public APIManagerResponse executeRequest(APIManagerRequest request) {
        try {
            if (request.getLimit() == 0) {
                request.setLimit(10);
            }
            String reqJson = this.serializeRequest(request);
            ApiHandler apiHandler = new ApiHandler("ApiKeysMasterWS");
            String responseJson = apiHandler.execute(reqJson);
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(ApiKeyHandler.class.getName()).log(Level.SEVERE, null, ex);
            APIManagerResponse serverResponse = new APIManagerResponse();
            serverResponse.setErrorCode(2056);
            serverResponse.setErrorMessage("Error in deserialization.");
            return serverResponse;
        }
    }

    protected APIManagerResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        APIManagerResponse response = mapper.readValue(responseJson, APIManagerResponse.class);
        return response;
    }

    protected String serializeRequest(Object responseObject) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(responseObject);
        }
        catch (IOException ex) {
            return null;
        }
    }

}
