/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.LookupRequest;
import com.crm.models.responses.managers.LookupResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class LookupHandler {

    public LookupResponse getLookups(LookupRequest request) {
        if (request.getApiKey() != null && !request.getApiKey().isEmpty()) {
            request.setRequestorUserModel(null);
        }
        else {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("GetLookupsWS");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(LookupHandler.class.getName()).log(Level.SEVERE, null, ex);
            LookupResponse lookupResponse = new LookupResponse();
            lookupResponse.setErrorCode(2056);
            lookupResponse.setErrorMessage("Error in client deseralization");
            return lookupResponse;
        }
    }

    public LookupResponse getManagers(LookupRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("GetLookupManagersWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    public LookupResponse managerExecutor(LookupRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("LookupManagersMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    public LookupResponse valueExecutor(LookupRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("LookupValuesMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected LookupResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        LookupResponse response = mapper.readValue(responseJson, LookupResponse.class);
        return response;
    }
}
