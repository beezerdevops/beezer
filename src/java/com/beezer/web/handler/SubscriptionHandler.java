/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.SubscriptionRequest;
import com.crm.models.responses.managers.SubscriptionResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class SubscriptionHandler {

    public SubscriptionResponse execute(SubscriptionRequest request) throws Exception {
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("SubscriptionMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected SubscriptionResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        SubscriptionResponse response = mapper.readValue(responseJson, SubscriptionResponse.class);
        return response;
    }
}
