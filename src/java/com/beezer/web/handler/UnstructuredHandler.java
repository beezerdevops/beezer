/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.UnstructuredRequest;
import com.crm.models.responses.UnstructuredResponse;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.HttpClientBuilder;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class UnstructuredHandler {

    public UnstructuredResponse executeForManager(UnstructuredRequest request) {
//        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getApiKey() != null && !request.getApiKey().isEmpty()) {
            request.setRequestorUserModel(null);
        }
        else {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        }
        if (request.getLimit() == 0) {
            request.setLimit(100);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("UnstructuredMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(UnstructuredHandler.class.getName()).log(Level.SEVERE, null, ex);
            UnstructuredResponse unstructuredResponse = new UnstructuredResponse();
            unstructuredResponse.setErrorCode(2056);
            unstructuredResponse.setErrorMessage("Error in handler deserilaization");
            return unstructuredResponse;
        }
    }

    public UnstructuredResponse getImage(UnstructuredRequest request) {
        if (request.getApiKey() == null || request.getApiKey().isEmpty()) {
            request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("GetImageAPI");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(UnstructuredHandler.class.getName()).log(Level.SEVERE, null, ex);
            UnstructuredResponse unstructuredResponse = new UnstructuredResponse();
            unstructuredResponse.setErrorCode(2056);
            unstructuredResponse.setErrorMessage("Error in handler deserilaization");
            return unstructuredResponse;
        }
    }

    public UnstructuredResponse upload(UnstructuredRequest request, InputStream is) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        String reqJson = GeneralUtils.serializeRequest(request);
        String BASE_URI = "http://beezerlabs.eu-central-1.elasticbeanstalk.com/generic";
        String result = "";
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(BASE_URI + "/CreateUnstructuredAPI");
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        ContentBody cbFile = new InputStreamBody(is, "image/jpeg");
        try {
            builder.addPart("file", cbFile);
            builder.addTextBody("request", reqJson);
            HttpEntity mpEntity = builder.build();
            httpPost.setEntity(mpEntity);
            System.out.println("executing request " + httpPost.getRequestLine());
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity resEntity = response.getEntity();
            result = resEntity.toString();
        }
        catch (Exception e) {
            e.getMessage();
        }
        return this.deserializeResponse(result);
    }

    protected UnstructuredResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        UnstructuredResponse response = mapper.readValue(responseJson, UnstructuredResponse.class);
        return response;
    }
}
