/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.PublishedRegistryRequest;
import com.crm.models.responses.managers.PublishedRegistryResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class PublishedComponentsHandler {

    public PublishedRegistryResponse executeForManager(PublishedRegistryRequest request) {
//        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("PublishedComponentsMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(PublishedComponentsHandler.class.getName()).log(Level.SEVERE, null, ex);
            PublishedRegistryResponse response = new PublishedRegistryResponse();
            response.setErrorCode(2056);
            response.setErrorMessage("Technical Error - EX: " + ex.getMessage());
            return response;
        }
    }

    protected PublishedRegistryResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        PublishedRegistryResponse response = mapper.readValue(responseJson, PublishedRegistryResponse.class);
        return response;
    }
}
