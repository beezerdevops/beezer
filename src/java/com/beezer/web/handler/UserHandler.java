/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.UserRequest;
import com.crm.models.responses.UserResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class UserHandler {

    public UserResponse execute(UserRequest request) {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("UserBasicsMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(UserHandler.class.getName()).log(Level.SEVERE, null, ex);
            UserResponse response = new UserResponse();
            response.setErrorCode(2056);
            response.setErrorMessage("Error in execution, due to: " + ex.getMessage());
            return response;
        }
    }
    
    public UserResponse executeForActivation(UserRequest request) {
//        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
        if (request.getLimit() == 0) {
            request.setLimit(10);
        }
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("UserBasicsMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        try {
            return this.deserializeResponse(responseJson);
        }
        catch (Exception ex) {
            Logger.getLogger(UserHandler.class.getName()).log(Level.SEVERE, null, ex);
            UserResponse response = new UserResponse();
            response.setErrorCode(2056);
            response.setErrorMessage("Error in execution, due to: " + ex.getMessage());
            return response;
        }
    }

    protected UserResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        UserResponse response = mapper.readValue(responseJson, UserResponse.class);
        return response;
    }
}
