/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.handler;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.requests.managers.CategoryRequest;
import com.crm.models.responses.managers.CategoryResponse;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
public class CategoriesHandler {

    public CategoryResponse execute(CategoryRequest request) throws Exception {
        request.setRequestorUserModel(GeneralUtils.getLoggedUser());
//        request.setAppId(GeneralUtils.getActiveApplication().getAppId());
        String reqJson = GeneralUtils.serializeRequest(request);
        ApiHandler apiHandler = new ApiHandler("CategoryMasterWS");
        String responseJson = apiHandler.execute(reqJson);
        return this.deserializeResponse(responseJson);
    }

    protected CategoryResponse deserializeResponse(String responseJson) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        CategoryResponse response = mapper.readValue(responseJson, CategoryResponse.class);
        return response;
    }
}
