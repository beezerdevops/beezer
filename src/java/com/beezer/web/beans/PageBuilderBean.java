/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.beans.modules.reports.AddReportBean_new;
import com.beezer.web.beans.modules.reports.ReportViewer;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.PageManagerHandler;
import com.beezer.web.utils.GeneralUtils;
import com.beezl.interpreter.BEEZLInterperter;
import com.crm.models.global.reports.ReportModel;
import com.crm.models.internal.pageDesigner.PageDesignModel;
import com.crm.models.internal.pageDesigner.PageWidgets;
import com.crm.models.internal.workflow.activities.DocTemplateParameter;
import com.crm.models.requests.managers.PageRequest;
import com.crm.models.responses.managers.PageResponse;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.codehaus.jackson.map.ObjectMapper;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Ahmed El Badry Nov 10, 2021
 */
@ManagedBean(name = "pageBuilderBean")
@ViewScoped
public class PageBuilderBean {

    private PageDesignModel pageDesignModel;
    private LinkedHashMap<Integer, ReportViewer> reportViewerMap;
    private LinkedHashMap<Integer, String> customViewMap;
    private String customLinkStyling;
    private String customCssStyling;
    private boolean emptyDashboard = true;
    private AddReportBean_new addReportBean;

    private boolean showFilters;

    private HashMap<Integer, ReportModel> reportModelMap;

    @PostConstruct
    public void init() {
        this.reportModelMap = new HashMap<>();
        this.customLinkStyling = "";
        this.customCssStyling = "";
        GeneralUtils.refreshMenuIconStyles();
        if (GeneralUtils.getActiveApplication() != null) {
            this.reportViewerMap = new LinkedHashMap<>();
            this.customViewMap = new LinkedHashMap<>();
            this.loadPage();
//            this.initialize();
            PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
        }
    }

    public void loadPage() {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        PageRequest pageRequest = new PageRequest();
        pageRequest.setRequestActionType("5");
        pageRequest.setAppId(GeneralUtils.getActiveApplication().getAppId());
        PageManagerHandler pageManagerHandler = new PageManagerHandler();
        PageResponse pageResponse = pageManagerHandler.executeRequest(pageRequest);
        if (pageResponse.getErrorCode() == 1000) {
            if (pageResponse.getPageList() != null && !pageResponse.getPageList().isEmpty()) {
                emptyDashboard = false;
                this.pageDesignModel = pageResponse.getPageList().get(0);
            }
            else {
                emptyDashboard = true;
            }
        }
    }

    public void initialize() {
        if (pageDesignModel != null && pageDesignModel.getPageConfig() != null && pageDesignModel.getPageConfig().getPageWidgets() != null) {
            this.initializeWidgets(pageDesignModel.getPageConfig().getPageWidgets());
        }
        if (GeneralUtils.getLoggedUser().isShowWelcomeFlow()) {
            PrimeFaces.current().executeScript("PF('firstTimeWelcomeDialog').show()");
        }
    }

    public void initializeWidgets(ArrayList<PageWidgets> widgetList) {
        for (PageWidgets widget : widgetList) {
            switch (widget.getType()) {
                case "REPORT":
                    ReportViewer reportViewer = new ReportViewer();
                    reportViewer.setReportId(widget.getReportWidgetConfig().getReportId());
                    reportViewer.init();
                    this.reportViewerMap.put(widget.getWidgetId(), reportViewer);
                    break;
                case "CUSTOM":
                    this.evaluateCustomWidgets(widget);
                    break;
                case "SECTION":
                    if (widget.getSectionWidgetConfig() != null && widget.getSectionWidgetConfig().getLayoutWidgets() != null) {
                        this.initializeWidgets(widget.getSectionWidgetConfig().getLayoutWidgets());
                    }
                    break;
                case "INFO_PANEL":
                    break;
            }
        }
    }

    public void openReportFilterConfiguration(PageWidgets widget) {
        this.showFilters = true;
        ReportModel reportModel;
        if (this.reportModelMap.get(widget.getWidgetId()) != null) {
            reportModel = this.reportModelMap.get(widget.getWidgetId());
        }
        else {
            reportModel = this.getReportViewerMap().get(widget.getWidgetId()).getReportModel();
        }
        if (reportModel != null) {
            this.addReportBean = new AddReportBean_new();
            this.addReportBean.initializeLocally(reportModel);
            PrimeFaces.current().ajax().update("reportDashboardFilterDialog");
            PrimeFaces.current().executeScript("PF('reportDashboardFilterDialog').show()");
        }
        int x = 0;
    }

    public void applyReportChanges(PageWidgets widget) {
        ReportViewer reportViewer = new ReportViewer();
        reportViewer.initializeForPreview(this.addReportBean.getReportModel());
        String serializedModel = GeneralUtils.serializeRequest(this.addReportBean.getReportModel());

        ObjectMapper mapper = new ObjectMapper();
        try {
            ReportModel reportModelCopy = mapper.readValue(serializedModel, ReportModel.class);
            this.reportModelMap.put(widget.getWidgetId(), reportModelCopy);
        }
        catch (IOException ex) {
            Logger.getLogger(PageBuilderBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.reportViewerMap.put(widget.getWidgetId(), reportViewer);
    }

    public void evaluateCustomWidgets(PageWidgets widget) {
        if (widget.getCustomWidgetConfig() != null) {
            if (widget.getCustomWidgetConfig().getHtmlContent() != null
                    && !widget.getCustomWidgetConfig().getHtmlContent().isEmpty()
                    && widget.getCustomWidgetConfig().getTemplateParameters() != null
                    && !widget.getCustomWidgetConfig().getTemplateParameters().isEmpty()) {

                if (widget.getCustomWidgetConfig().getLinkStyling() != null) {
                    this.customLinkStyling = this.customLinkStyling + "\n" + widget.getCustomWidgetConfig().getLinkStyling();
                }

                if (widget.getCustomWidgetConfig().getCssStyling() != null) {
                    this.customCssStyling = this.customCssStyling + "\n" + widget.getCustomWidgetConfig().getCssStyling();
                }

                BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                LinkedHashMap<String, String> bvm = null;

                Map<String, Object> configs = new HashMap<>();
                for (DocTemplateParameter dtp : widget.getCustomWidgetConfig().getTemplateParameters()) {
                    switch (dtp.getType()) {
                        case "STRING":
                            beezlInterperter.addDynamicField(dtp.getKeyName(), dtp.getKeyValue());
                            break;
                        case "LIST":
                            break;
                    }
                }

                try {
                    bvm = beezlInterperter.evaluate();
                }
                catch (Exception ex) {
                    Logger.getLogger(DashboardV2Bean.class.getName()).log(Level.SEVERE, null, ex);
                }

                for (DocTemplateParameter dtp : widget.getCustomWidgetConfig().getTemplateParameters()) {
                    if (bvm != null) {
                        if (!dtp.getType().equals("LIST")) {
                            configs.put(dtp.getKeyName(), bvm.get(dtp.getKeyName()));
                        }
                    }
                    else {
                        return;
                    }
                }

                StringWriter out = new StringWriter();
                Configuration cfg = new Configuration();
                cfg.setDefaultEncoding("UTF-8");
                Template t;
                try {
                    t = new Template("name", new StringReader(widget.getCustomWidgetConfig().getHtmlContent()), cfg);
                    t.process(configs, out);
                }
                catch (Exception ex) {
                    Logger.getLogger(DashboardV2Bean.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
                String processedWidget = out.getBuffer().toString();

                this.customViewMap.put(widget.getWidgetId(), processedWidget);
            }
            else if (widget.getCustomWidgetConfig().getHtmlContent() != null
                    && !widget.getCustomWidgetConfig().getHtmlContent().isEmpty()) {
                this.customViewMap.put(widget.getWidgetId(), widget.getCustomWidgetConfig().getHtmlContent());
            }
        }
    }
    
    public String loadImageBase64(PageWidgets pageWidgets){
        String imageString= new String(Base64.getEncoder().encodeToString(pageWidgets.getInfoPanelWidgetConfig().getIcon().getContentStream()));
        return imageString;
    }

    public StreamedContent loadInfoPanelImage(PageWidgets pageWidgets) {
        FacesContext context = FacesContext.getCurrentInstance();

        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the view. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            // So, browser is requesting the image. Get ID value from actual request param.
            return DefaultStreamedContent.builder()
                    .name("infoPanelImage")
                    .stream(() -> new ByteArrayInputStream(pageWidgets.getInfoPanelWidgetConfig().getIcon().getContentStream()))
                    .build();
        }
    }

    public String evaluateValue(String script) {
        if (script != null && !script.isEmpty()) {
            try {
                BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                beezlInterperter.addDynamicField("value", script);
                LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                script = dynamicFieldMap.get("value");
            }
            catch (Exception ex) {
                Logger.getLogger(PageBuilderBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return script;
    }

    public PageDesignModel getPageDesignModel() {
        return pageDesignModel;
    }

    public void setPageDesignModel(PageDesignModel pageDesignModel) {
        this.pageDesignModel = pageDesignModel;
    }

    public LinkedHashMap<Integer, ReportViewer> getReportViewerMap() {
        return reportViewerMap;
    }

    public void setReportViewerMap(LinkedHashMap<Integer, ReportViewer> reportViewerMap) {
        this.reportViewerMap = reportViewerMap;
    }

    public LinkedHashMap<Integer, String> getCustomViewMap() {
        return customViewMap;
    }

    public void setCustomViewMap(LinkedHashMap<Integer, String> customViewMap) {
        this.customViewMap = customViewMap;
    }

    public String getCustomLinkStyling() {
        return customLinkStyling;
    }

    public void setCustomLinkStyling(String customLinkStyling) {
        this.customLinkStyling = customLinkStyling;
    }

    public String getCustomCssStyling() {
        return customCssStyling;
    }

    public void setCustomCssStyling(String customCssStyling) {
        this.customCssStyling = customCssStyling;
    }

    public boolean isEmptyDashboard() {
        return emptyDashboard;
    }

    public void setEmptyDashboard(boolean emptyDashboard) {
        this.emptyDashboard = emptyDashboard;
    }

    public AddReportBean_new getAddReportBean() {
        return addReportBean;
    }

    public void setAddReportBean(AddReportBean_new addReportBean) {
        this.addReportBean = addReportBean;
    }

    public boolean isShowFilters() {
        return showFilters;
    }

    public void setShowFilters(boolean showFilters) {
        this.showFilters = showFilters;
    }

}
