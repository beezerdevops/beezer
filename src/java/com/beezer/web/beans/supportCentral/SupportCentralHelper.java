/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.supportCentral;

import com.beezer.web.beans.AccessBean;
import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.application.AppModuleConfigModel;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Ahmed El Badry Nov 8, 2022
 */
public class SupportCentralHelper {

    private AccessBean accessBean;
    private ArrayList<AppModuleConfigModel> activAppModuleList;
    private String moduleNameForTicket;
    private String issueDescription;
    private UnstructuredModel attachment;

    public SupportCentralHelper(AccessBean accessBean) {
        this.accessBean = accessBean;
        this.populateModuleList();
    }

    private void populateModuleList() {
        if (accessBean != null && accessBean.getActiveApplication() != null) {
            this.attachment = null;
            activAppModuleList = new ArrayList<>();
            for (Map.Entry<String, ArrayList<AppModuleConfigModel>> entrySet : accessBean.getActiveApplication().getModuleConfigMap().entrySet()) {
                activAppModuleList.addAll(entrySet.getValue());
            }
        }
    }

    public void onContentSelect(FileUploadEvent event) {
        try {
            this.attachment = new UnstructuredModel();
            this.attachment.setFileExtenstion(GeneralUtils.getFileExtenstion(event.getFile().getFileName()));
            this.attachment.setFileName(event.getFile().getFileName());
            this.attachment.setFileSize(event.getFile().getSize());
            this.attachment.setContentStream(event.getFile().getContent());
        }
        catch (Exception ex) {
            Logger.getLogger(SupportCentralHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void submitTicket() {
        String centralApiKey = "c8246de6de5940b2994cde8defce0f0d";
        String supportTicketModuleId = "775863312";
        ModuleFieldRequest moduleFieldRequest = new ModuleFieldRequest();
        moduleFieldRequest.setModuleId(supportTicketModuleId);
        moduleFieldRequest.setApiKey(centralApiKey);
        moduleFieldRequest.setActionType("0");
        ModuleHandler moduleHandler = new ModuleHandler();
        ModuleFieldResponse moduleFieldResponse = moduleHandler.fieldExecutor(moduleFieldRequest);
        if (moduleFieldResponse.getErrorCode() == 1000 && moduleFieldResponse.getReturnList() != null) {
            RecordModel recordModel = new RecordModel();
            LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
            for (ModuleFieldModel mfm : moduleFieldResponse.getReturnList()) {
                switch (mfm.getFieldName()) {
                    case "company_id":
                        mfm.setCurrentValue(this.accessBean.getCompanyModel().getCompanyId());
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "tenant_id":
                        mfm.setCurrentValue(this.accessBean.getUserModel().getTenantId());
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "ticket_scope":
                        mfm.setCurrentValue("1");
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "ticket_status":
                        mfm.setCurrentValue("1");
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "ticket_issuer":
                        mfm.setCurrentValue(this.accessBean.getUserModel().getName());
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "ticket_type":
                        mfm.setCurrentValue("3");
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "module_name":
                        mfm.setCurrentValue(this.moduleNameForTicket);
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "application_name":
                        mfm.setCurrentValue(this.accessBean.getActiveApplication().getAppName());
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "issue_description":
                        mfm.setCurrentValue(this.issueDescription);
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "issuer_id":
                        mfm.setCurrentValue(String.valueOf(this.accessBean.getUserModel().getUserId()));
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "issuer_email":
                        mfm.setCurrentValue(this.accessBean.getUserModel().getEmail());
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "ticket_attachment":
                        mfm.setContent(attachment);
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                    case "open_date":
                        Date date = new Date();
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        String strDate = formatter.format(date);
                        mfm.setCurrentValue(strDate);
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                }
            }
            recordModel.setFieldValueMap(fieldValueMap);

            RequestModel requestModel = new RequestModel();
            requestModel.setRecordModel(recordModel);
            requestModel.setRequestActionType("1");
            requestModel.setApiKey(centralApiKey);
            requestModel.setRequestingModule(supportTicketModuleId);
            RequestHandler handler = new RequestHandler();
            ResponseModel responseModel = handler.executeRequest(requestModel);
            if (responseModel.getErrorCode() == 1000) {
                PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
                PrimeFaces.current().executeScript("PF('supportCentralDialog').hide()");
                PrimeFaces.current().executeScript("PF('ticketSubmittedConfirmationDialog').show()");
            }
        }
    }

    public ArrayList<AppModuleConfigModel> getActivAppModuleList() {
        return activAppModuleList;
    }

    public void setActivAppModuleList(ArrayList<AppModuleConfigModel> activAppModuleList) {
        this.activAppModuleList = activAppModuleList;
    }

    public AccessBean getAccessBean() {
        return accessBean;
    }

    public void setAccessBean(AccessBean accessBean) {
        this.accessBean = accessBean;
    }

    public String getModuleNameForTicket() {
        return moduleNameForTicket;
    }

    public void setModuleNameForTicket(String moduleNameForTicket) {
        this.moduleNameForTicket = moduleNameForTicket;
    }

    public String getIssueDescription() {
        return issueDescription;
    }

    public void setIssueDescription(String issueDescription) {
        this.issueDescription = issueDescription;
    }

}
