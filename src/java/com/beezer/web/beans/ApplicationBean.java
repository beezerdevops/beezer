/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.bills.BillGenerator;
import com.beezer.web.beans.modules.deals.InvoiceGenerator;
import com.beezer.web.beans.modules.reports.AddReportBean_new;
import com.beezer.web.handler.DocumentUtilsHandler;
import com.beezer.web.handler.ReportHandler;
import com.beezer.web.handler.UnstructuredHandler;
import com.beezer.web.reportEngine.ReportGenerator;
import com.beezer.web.reportEngine.SalesReport;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.reports.ReportModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.ReportRequest;
import com.crm.models.requests.UnstructuredRequest;
import com.crm.models.requests.pdf.DocumentUtilRequest;
import com.crm.models.responses.ReportResponse;
import com.crm.models.responses.UnstructuredResponse;
import com.crm.models.responses.pdf.DocumentUtilResponse;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author badry
 */
@ManagedBean(name = "applicationBean")
@ApplicationScoped
public class ApplicationBean {

    public StreamedContent getImage() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            try {
                String id = context.getExternalContext().getRequestParameterMap().get("contentId");
                String imageContext = context.getExternalContext().getRequestParameterMap().get("context");
                String moduleId = context.getExternalContext().getRequestParameterMap().get("moduleId");
                if (imageContext == null) {
                    imageContext = "GALLERY";
                }
                if (moduleId == null) {
                    moduleId = "0";
                }
                UnstructuredRequest unstructuredRequest = new UnstructuredRequest();
                unstructuredRequest.setRequestorUserModel(GeneralUtils.getLoggedUser());
                unstructuredRequest.setUnstructuredId(id);
                unstructuredRequest.setContext(imageContext);
                unstructuredRequest.setObjectId(Integer.valueOf(moduleId));
                UnstructuredHandler unstructuredHandler = new UnstructuredHandler();
                UnstructuredResponse unstructuredResponse = unstructuredHandler.getImage(unstructuredRequest);
                if (unstructuredResponse != null && unstructuredResponse.getErrorCode() == 1000) {
                    return DefaultStreamedContent.builder()
                            .stream(() -> new ByteArrayInputStream(
                            unstructuredResponse.getReturnList().get(0).getContentStream()))
                            .build();
//                    return new DefaultStreamedContent(new ByteArrayInputStream(unstructuredResponse.getReturnList().get(0).getContentStream()));
                }
            }
            catch (Exception ex) {
                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
    }

    public StreamedContent getRecordDetailModuleImage() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            try {
                String id = context.getExternalContext().getRequestParameterMap().get("contentId");
                String imageContext = context.getExternalContext().getRequestParameterMap().get("context");
//                String moduleId = context.getExternalContext().getRequestParameterMap().get("moduleId");
                if (imageContext == null) {
                    imageContext = "GALLERY";
                }
//                if (moduleId == null) {
//                    moduleId = "0";
//                }
                UnstructuredRequest unstructuredRequest = new UnstructuredRequest();
                unstructuredRequest.setRequestorUserModel(GeneralUtils.getLoggedUser());
                unstructuredRequest.setUnstructuredId(id);
                unstructuredRequest.setContext(imageContext);
                unstructuredRequest.setObjectId(0);
                UnstructuredHandler unstructuredHandler = new UnstructuredHandler();
                UnstructuredResponse unstructuredResponse = unstructuredHandler.getImage(unstructuredRequest);
                if (unstructuredResponse != null && unstructuredResponse.getErrorCode() == 1000) {
                    return DefaultStreamedContent.builder()
                            .stream(() -> new ByteArrayInputStream(
                            unstructuredResponse.getReturnList().get(0).getContentStream()))
                            .build();
//                    return new DefaultStreamedContent(new ByteArrayInputStream(unstructuredResponse.getReturnList().get(0).getContentStream()));
                }
            }
            catch (Exception ex) {
                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
    }

    public StreamedContent getReportPdf() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            String id = context.getExternalContext().getRequestParameterMap().get("reportId");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("id", "=", id));
            ReportRequest reportRequest = new ReportRequest();
            reportRequest.setRequestActionType("0");
            reportRequest.setClause(fm.getClause());
            ReportResponse reportResponse = null;
            ReportHandler reportHandler = new ReportHandler();
            try {
                reportResponse = reportHandler.execute(reportRequest);
            }
            catch (Exception ex) {
                Logger.getLogger(ApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            ReportModel reportModel = null;
            if (reportResponse != null && reportResponse.getErrorCode() == 1000) {
                reportModel = reportResponse.getReturnList().get(0);
            }

            if (reportModel.getPrimaryModule().getModuleId() == 5) {
                SalesReport salesReport = new SalesReport();
                try {
                    byte[] byteReport = salesReport.generate(reportModel);
                    return DefaultStreamedContent.builder()
                            .contentType("application/pdf")
                            .stream(() -> new ByteArrayInputStream(
                            byteReport))
                            .build();
//                    return new DefaultStreamedContent(new ByteArrayInputStream(byteReport), "application/pdf");
                }
                catch (Exception ex) {
                    Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
            else {
                ReportGenerator gr = new ReportGenerator();
                try {
                    byte[] byteReport = gr.generate(reportModel);
                    return DefaultStreamedContent.builder()
                            .contentType("application/pdf")
                            .stream(() -> new ByteArrayInputStream(
                            byteReport))
                            .build();
//                    return new DefaultStreamedContent(new ByteArrayInputStream(byteReport), "application/pdf");
                }
                catch (Exception ex) {
                    Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }

        }
    }

    public StreamedContent getInvoicePdf() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            String invoiceId = context.getExternalContext().getRequestParameterMap().get("invoiceId");
            String id = context.getExternalContext().getRequestParameterMap().get("dealId");
            try {
                InvoiceGenerator invoiceGenerator = new InvoiceGenerator(id, invoiceId);
                String invoiceHtml = invoiceGenerator.generateHtmlInvoice();
                ITextRenderer renderer = new ITextRenderer();
                renderer.setDocumentFromString(invoiceHtml);
                renderer.layout();
                ByteArrayOutputStream fos = new ByteArrayOutputStream(invoiceHtml.length());
                renderer.createPDF(fos);
                byte[] pdfBytes = fos.toByteArray();
                return DefaultStreamedContent.builder()
                        .contentType("application/pdf")
                        .stream(() -> new ByteArrayInputStream(
                        pdfBytes))
                        .build();
//                return new DefaultStreamedContent(new ByteArrayInputStream(pdfBytes), "application/pdf");
            }
            catch (Exception ex) {
                Logger.getLogger(ApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
    }

    public StreamedContent getPOBillPdf() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            try {
                String billId = context.getExternalContext().getRequestParameterMap().get("billId");
                BillGenerator billGenerator = new BillGenerator(billId);
                String invoiceHtml = billGenerator.generateHtmlInvoice();
                ITextRenderer renderer = new ITextRenderer();
                try {
                    renderer.getFontResolver().addFont("../../fonts/NotoNaskhArabic-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                    renderer.getFontResolver().addFont("../../fonts/arialuni.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                    renderer.getFontResolver().addFont("../../fonts/arial.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                    renderer.getFontResolver().addFont("../../fonts/ArialBold.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

                }
                catch (IOException ex) {
                    Logger.getLogger(ApplicationBean.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
                renderer.setDocumentFromString(invoiceHtml);
                renderer.layout();
                ByteArrayOutputStream fos = new ByteArrayOutputStream(invoiceHtml.length());
                renderer.createPDF(fos);
                renderer.finishPDF();
                byte[] pdfBytes = fos.toByteArray();
                return DefaultStreamedContent.builder()
                        .contentType("application/pdf")
                        .stream(() -> new ByteArrayInputStream(
                        pdfBytes))
                        .build();
//                return new DefaultStreamedContent(new ByteArrayInputStream(pdfBytes), "application/pdf");

            }
            catch (DocumentException ex) {
                Logger.getLogger(ApplicationBean.class
                        .getName()).log(Level.SEVERE, null, ex);

                return null;
            }
        }
    }

    public StreamedContent getRecordDynamicImage() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            try {
                String id = context.getExternalContext().getRequestParameterMap().get("contentId");
                String imageContext = context.getExternalContext().getRequestParameterMap().get("context");
                String moduleId = context.getExternalContext().getRequestParameterMap().get("moduleId");
                if (imageContext == null) {
                    imageContext = "GALLERY";
                }
                if (moduleId == null) {
                    moduleId = "0";
                }

                if (id.isEmpty()) {
                    return null;
                }
                UnstructuredRequest unstructuredRequest = new UnstructuredRequest();
                unstructuredRequest.setRequestorUserModel(GeneralUtils.getLoggedUser());
                unstructuredRequest.setUnstructuredId(id);
                unstructuredRequest.setContext(imageContext);
                UnstructuredHandler unstructuredHandler = new UnstructuredHandler();
                UnstructuredResponse unstructuredResponse = unstructuredHandler.getImage(unstructuredRequest);
                if (unstructuredResponse != null && unstructuredResponse.getErrorCode() == 1000) {
                    return DefaultStreamedContent.builder()
                            .stream(() -> new ByteArrayInputStream(
                            unstructuredResponse.getReturnList().get(0).getContentStream()))
                            .build();
//                    return new DefaultStreamedContent(new ByteArrayInputStream(unstructuredResponse.getReturnList().get(0).getContentStream()));
                }
            }
            catch (Exception ex) {
                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
    }

    public StreamedContent getGalleriaDynamicImage() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            try {
                String id = context.getExternalContext().getRequestParameterMap().get("contentId");
                String imageContext = context.getExternalContext().getRequestParameterMap().get("context");
                String moduleId = context.getExternalContext().getRequestParameterMap().get("moduleId");
                if (imageContext == null) {
                    imageContext = "GALLERY";
                }
                if (moduleId == null) {
                    moduleId = "0";
                }

                if (id.isEmpty()) {
                    return null;
                }
                UnstructuredRequest unstructuredRequest = new UnstructuredRequest();
                unstructuredRequest.setRequestorUserModel(GeneralUtils.getLoggedUser());
                unstructuredRequest.setUnstructuredId(id);
                unstructuredRequest.setContext(imageContext);
                UnstructuredHandler unstructuredHandler = new UnstructuredHandler();
                UnstructuredResponse unstructuredResponse = unstructuredHandler.getImage(unstructuredRequest);
                if (unstructuredResponse != null && unstructuredResponse.getErrorCode() == 1000) {
                    return DefaultStreamedContent.builder()
                            .stream(() -> new ByteArrayInputStream(
                            unstructuredResponse.getReturnList().get(0).getContentStream()))
                            .build();
//                    return new DefaultStreamedContent(new ByteArrayInputStream(unstructuredResponse.getReturnList().get(0).getContentStream()));
                }
            }
            catch (Exception ex) {
                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
        else {
            try {
                String id = context.getExternalContext().getRequestParameterMap().get("contentId");
                String imageContext = context.getExternalContext().getRequestParameterMap().get("context");
                String moduleId = context.getExternalContext().getRequestParameterMap().get("moduleId");
                if (imageContext == null) {
                    imageContext = "GALLERY";
                }
                if (moduleId == null) {
                    moduleId = "0";
                }

                if (id.isEmpty()) {
                    return null;
                }
                UnstructuredRequest unstructuredRequest = new UnstructuredRequest();
                unstructuredRequest.setRequestorUserModel(GeneralUtils.getLoggedUser());
                unstructuredRequest.setUnstructuredId(id);
                unstructuredRequest.setContext(imageContext);
                UnstructuredHandler unstructuredHandler = new UnstructuredHandler();
                UnstructuredResponse unstructuredResponse = unstructuredHandler.getImage(unstructuredRequest);
                if (unstructuredResponse != null && unstructuredResponse.getErrorCode() == 1000) {
                    return DefaultStreamedContent.builder()
                            .stream(() -> new ByteArrayInputStream(
                            unstructuredResponse.getReturnList().get(0).getContentStream()))
                            .build();
//                    return new DefaultStreamedContent(new ByteArrayInputStream(unstructuredResponse.getReturnList().get(0).getContentStream()));
                }
            }
            catch (Exception ex) {
                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
    }

    public StreamedContent getCustomReportAsPdf() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            return new DefaultStreamedContent();
        }
        else {
            String id = context.getExternalContext().getRequestParameterMap().get("reportId");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("id", "=", id));
            ReportRequest reportRequest = new ReportRequest();
            reportRequest.setRequestActionType("0");
            reportRequest.setClause(fm.getClause());
            ReportResponse reportResponse = null;
            ReportHandler reportHandler = new ReportHandler();
            try {
                reportResponse = reportHandler.execute(reportRequest);
            }
            catch (Exception ex) {
                Logger.getLogger(ApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            ReportModel reportModel = null;
            if (reportResponse != null && reportResponse.getErrorCode() == 1000) {
                reportModel = reportResponse.getReturnList().get(0);
            }

            if (reportModel.getCustomReportConfig() != null) {
                DocumentUtilsHandler documentUtilsHandler = new DocumentUtilsHandler();
                DocumentUtilRequest documentUtilRequest = new DocumentUtilRequest();
                documentUtilRequest.setRequestActionType("0");
                documentUtilRequest.setHtmlContent(reportModel.getCustomReportConfig().getHtmlContent());
                DocumentUtilResponse documentUtilResponse = documentUtilsHandler.executeRequest(documentUtilRequest);
                if (documentUtilResponse.getErrorCode() == 1000 && documentUtilResponse.getConvertedDocument() != null) {
                    return DefaultStreamedContent.builder()
                            .name(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue() + ".pdf")
                            .contentType("application/pdf")
                            .stream(() -> new ByteArrayInputStream(
                            documentUtilResponse.getConvertedDocument().getContentStream()))
                            .build();
                }
            }
        }
        return null;
    }

    public void showIdleDialog() {
//        RequestContext context = RequestContext.getCurrentInstance();
//        context.execute("PF('idleDialog').show()");
        PrimeFaces.current().executeScript("PF('idleDialog').show()");
    }
}
