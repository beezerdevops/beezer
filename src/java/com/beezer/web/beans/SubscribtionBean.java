/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.handler.SubscriptionHandler;
import com.crm.models.requests.managers.SubscriptionRequest;
import com.crm.models.responses.managers.SubscriptionResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author badry
 */
@ManagedBean(name = "subscribtionBean")
@ViewScoped
public class SubscribtionBean {

    private boolean authenticationFailed = false;
    private String userName;
    private String userEmail;
    private String userPassword;
    private String userMobile;
    private String country;
    private String city;
    private String companyName;
    private String errorMessage;

    public void signUp() {
        try {
            SubscriptionHandler subscriptionHandler = new SubscriptionHandler();
            SubscriptionRequest subscriptionRequest = new SubscriptionRequest();
            subscriptionRequest.setRequestActionType("1");
            subscriptionRequest.setUserName(userName);
            subscriptionRequest.setUserEmail(userEmail);
            subscriptionRequest.setUserPassword(userPassword);
            subscriptionRequest.setUserMobile(userMobile);
            subscriptionRequest.setCountry(country);
            subscriptionRequest.setCity(city);
            subscriptionRequest.setCompanyName(companyName);
            SubscriptionResponse response = subscriptionHandler.execute(subscriptionRequest);
            if (response.getErrorCode() == 1000) {
                this.authenticationFailed = false;
                FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
            }
            else {
                this.errorMessage = response.getErrorMessage();
                this.authenticationFailed = true;
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isAuthenticationFailed() {
        return authenticationFailed;
    }

    public void setAuthenticationFailed(boolean authenticationFailed) {
        this.authenticationFailed = authenticationFailed;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
