/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.registeration;

import com.beezer.web.handler.CentralSubscriptionHandler;
import com.crm.models.requests.managers.SubscriptionRequest;
import com.crm.models.responses.managers.SubscriptionResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Ahmed El Badry Jan 3, 2023
 */
@ManagedBean(name = "registerationBean")
@ViewScoped
public class RegisterationBean {

    private Map<String, String> countries = new LinkedHashMap<>();
    private SubscriptionRequest subscriptionRequest;
    private String validationResponse;
    private boolean subscriptionComplete;

    @PostConstruct
    public void init() {
        this.subscriptionRequest = new SubscriptionRequest();
        this.initializeCountries();
    }

    private void initializeCountries() {
        countries = new LinkedHashMap<>();
        String countryNames = "Afghanistan,Albania,Algeria,Andorra,Angola,Antigua and Barbuda,Argentina,Armenia,Austria,Azerbaijan,Bahrain,Bangladesh,Barbados,Belarus,Belgium,Belize,Benin,Bhutan,Bolivia,Bosnia and Herzegovina,Botswana,Brazil,Brunei,Bulgaria,Burkina Faso,Burundi,Cabo Verde,Cambodia,Cameroon,Canada,Central African Republic,Chad,Channel Islands,Chile,China,Colombia,Comoros,Congo,Costa Rica,Côte d'Ivoire,Croatia,Cuba,Cyprus,Czech Republic,Denmark,Djibouti,Dominica,Dominican Republic,DR Congo,Ecuador,Egypt,El Salvador,Equatorial Guinea,Eritrea,Estonia,Eswatini,Ethiopia,Faeroe Islands,Finland,France,French Guiana,Gabon,Gambia,Georgia,Germany,Ghana,Gibraltar,Greece,Grenada,Guatemala,Guinea,Guinea-Bissau,Guyana,Haiti,Holy See,Honduras,Hong Kong,Hungary,Iceland,India,Indonesia,Iran,Iraq,Ireland,Isle of Man,Israel,Italy,Jamaica,Japan,Jordan,Kazakhstan,Kenya,Kuwait,Kyrgyzstan,Laos,Latvia,Lebanon,Lesotho,Liberia,Libya,Liechtenstein,Lithuania,Luxembourg,Macao,Madagascar,Malawi,Malaysia,Maldives,Mali,Malta,Mauritania,Mauritius,Mayotte,Mexico,Moldova,Monaco,Mongolia,Montenegro,Morocco,Mozambique,Myanmar,Namibia,Nepal,Netherlands,Nicaragua,Niger,Nigeria,North Korea,North Macedonia,Norway,Oman,Pakistan,Panama,Paraguay,Peru,Philippines,Poland,Portugal,Qatar,Réunion,Romania,Russia,Rwanda,Saint Helena,Saint Kitts and Nevis,Saint Lucia,Saint Vincent and the Grenadines,San Marino,Sao Tome & Principe,Saudi Arabia,Senegal,Serbia,Seychelles,Sierra Leone,Singapore,Slovakia,Slovenia,Somalia,South Africa,South Korea,South Sudan,Spain,Sri Lanka,State of Palestine,Sudan,Suriname,Sweden,Switzerland,Syria,Taiwan,Tajikistan,Tanzania,Thailand,The Bahamas,Timor-Leste,Togo,Trinidad and Tobago,Tunisia,Turkey,Turkmenistan,Uganda,Ukraine,United Arab Emirates,United Kingdom,United States,Uruguay,Uzbekistan,Venezuela,Vietnam,Western Sahara,Yemen,Zambia,Zimbabwe";
        List<String> countryList = Arrays.asList(countryNames.split("\\s*,\\s*"));
        for (String country : countryList) {
            countries.put(country, country);
        }
    }

    public void subscribe() {
        boolean validationStatus = this.validateSubscriptionRequestInput();
        if (!validationStatus) {
            PrimeFaces.current().ajax().update("somethingWentWrongWarningDialog");
            PrimeFaces.current().executeScript("PF('buildingEnvWaitingDialog').hide()");
            PrimeFaces.current().executeScript("PF('somethingWentWrongWarningDialog').show()");
            return;
        }

        this.subscriptionRequest.setRequestActionType("1");
        this.subscriptionRequest.setKeyObjectMap(new HashMap<>());
        this.subscriptionRequest.getKeyObjectMap().put("planCode", "FREE_TIER");
        this.subscriptionRequest.setUserPassword("dumdum");

        CentralSubscriptionHandler centralSubscriptionHandler = new CentralSubscriptionHandler();
        SubscriptionResponse subscriptionResponse = centralSubscriptionHandler.execute(this.subscriptionRequest);
        switch (subscriptionResponse.getErrorCode()) {
            case 1000:
                this.subscriptionComplete = true;
                PrimeFaces.current().executeScript("PF('buildingEnvWaitingDialog').hide()");
                PrimeFaces.current().ajax().update("mainForm");
                break;
            case 1001:
                this.validationResponse = subscriptionResponse.getErrorMessage();
                PrimeFaces.current().ajax().update("somethingWentWrongWarningDialog");
                PrimeFaces.current().executeScript("PF('buildingEnvWaitingDialog').hide()");
                PrimeFaces.current().executeScript("PF('somethingWentWrongWarningDialog').show()");
                break;
            default:
                this.validationResponse = "We're sorry, something went wrong. Here is the issue: " + subscriptionResponse.getErrorMessage();
                PrimeFaces.current().ajax().update("somethingWentWrongWarningDialog");
                PrimeFaces.current().executeScript("PF('buildingEnvWaitingDialog').hide()");
                PrimeFaces.current().executeScript("PF('somethingWentWrongWarningDialog').show()");
                break;
        }
    }

    private boolean validateSubscriptionRequestInput() {
        if (this.subscriptionRequest != null) {
            if (subscriptionRequest.getUserName() == null || subscriptionRequest.getUserName().trim().isEmpty()) {
                this.validationResponse = "Name is mandatory for the subscription";
                return false;
            }

            if (subscriptionRequest.getUserEmail() == null || subscriptionRequest.getUserEmail().trim().isEmpty()) {
                this.validationResponse = "Email is mandatory for the subscription";
                return false;
            }

            if (subscriptionRequest.getUserMobile() == null || subscriptionRequest.getUserMobile().trim().isEmpty()) {
                this.validationResponse = "Mobile number is mandatory for the subscription";
                return false;
            }

            if (subscriptionRequest.getCountry() == null || subscriptionRequest.getCountry().trim().isEmpty()) {
                this.validationResponse = "Country is mandatory for the subscription";
                return false;
            }

            if (subscriptionRequest.getCity() == null || subscriptionRequest.getCity().trim().isEmpty()) {
                this.validationResponse = "City is mandatory for the subscription";
                return false;
            }
        }
        this.validationResponse = "";
        return true;
    }

    public Map<String, String> getCountries() {
        return countries;
    }

    public void setCountries(Map<String, String> countries) {
        this.countries = countries;
    }

    public SubscriptionRequest getSubscriptionRequest() {
        return subscriptionRequest;
    }

    public void setSubscriptionRequest(SubscriptionRequest subscriptionRequest) {
        this.subscriptionRequest = subscriptionRequest;
    }

    public String getValidationResponse() {
        return validationResponse;
    }

    public void setValidationResponse(String validationResponse) {
        this.validationResponse = validationResponse;
    }

    public boolean isSubscriptionComplete() {
        return subscriptionComplete;
    }

    public void setSubscriptionComplete(boolean subscriptionComplete) {
        this.subscriptionComplete = subscriptionComplete;
    }

}
