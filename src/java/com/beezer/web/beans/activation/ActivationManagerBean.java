/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.activation;

import com.beezer.web.handler.ActivationsHandler;
import com.beezer.web.handler.UserHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UserModel;
import com.crm.models.requests.UserRequest;
import com.crm.models.responses.UserResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Ahmed El Badry Dec 16, 2022
 */
@ManagedBean(name = "activationManagerBean")
@ViewScoped
public class ActivationManagerBean {

    private UserModel userModel;
    private String userEmail;
    private String userPassword;
    private String activationKey;
    private boolean activationKeyMissing;
    private boolean invalidActivationKey;
    private String validationResponse;

    @PostConstruct
    public void init() {
        this.getUrlParams();
        this.validateActivationSecret();
    }

    private void getUrlParams() {
        Map<String, String> params = new HashMap<>(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        if (params.get("activation_key") != null) {
            this.activationKey = params.get("activation_key");
        }

        if (activationKey == null || activationKey.isEmpty()) {
            this.activationKeyMissing = true;
        }
    }

    private void validateActivationSecret() {
        if (!this.activationKeyMissing) {
            RequestModel requestModel = new RequestModel();
            requestModel.setRequestActionType("1");
            requestModel.setKeyObjectMap(new HashMap<>());
            requestModel.getKeyObjectMap().put("activationSecret", this.activationKey);
            ActivationsHandler activationsHandler = new ActivationsHandler();
            ResponseModel responseModel = activationsHandler.executeForManager(requestModel);
            if (responseModel.getErrorCode() == 1000) {
                this.invalidActivationKey = false;
                String serializedUser = (String) responseModel.getKeyObjectMap().get("userModel");
                ObjectMapper mapper = new ObjectMapper();
                try {
                    userModel = mapper.readValue(serializedUser, UserModel.class);
                }
                catch (IOException ex) {
                    Logger.getLogger(ActivationManagerBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else {
                this.invalidActivationKey = true;
                this.validationResponse = responseModel.getErrorMessage();
            }
        }
    }

    public void updateUserPassword() {
        UserRequest userRequest = new UserRequest();
        userRequest.setActionType("4");
        userRequest.setUserModel(userModel);
        userRequest.setOveridePasswordVerification(true);

        UserHandler handler = new UserHandler();
        UserResponse response = handler.executeForActivation(userRequest);
        if (response.getErrorCode() == 1000) {
            RequestModel requestModel = new RequestModel();
            requestModel.setRequestActionType("3");
            requestModel.setKeyObjectMap(new HashMap<>());
            requestModel.getKeyObjectMap().put("userModel", GeneralUtils.serializeRequest(this.userModel));
            ActivationsHandler activationsHandler = new ActivationsHandler();
            ResponseModel responseModel = activationsHandler.executeForManager(requestModel);
            if (responseModel.getErrorCode() == 1000) {
                PrimeFaces.current().executeScript("PF('activationCompletedDialog').show()");
            }
            else {
                PrimeFaces.current().ajax().update("somethingWentWrongWarningDialog");
                PrimeFaces.current().executeScript("PF('somethingWentWrongWarningDialog').show()");
            }
        }

    }

    public void redirectToSpace(String type) {
        switch (type) {
            case "APP":
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("https://app.beezr.io/");
                }
                catch (IOException ex) {
                    Logger.getLogger(ActivationManagerBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "DESIGNER":
                 try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("https://designer.beezr.io/");
                }
                catch (IOException ex) {
                    Logger.getLogger(ActivationManagerBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public boolean isActivationKeyMissing() {
        return activationKeyMissing;
    }

    public void setActivationKeyMissing(boolean activationKeyMissing) {
        this.activationKeyMissing = activationKeyMissing;
    }

    public boolean isInvalidActivationKey() {
        return invalidActivationKey;
    }

    public void setInvalidActivationKey(boolean invalidActivationKey) {
        this.invalidActivationKey = invalidActivationKey;
    }

    public String getValidationResponse() {
        return validationResponse;
    }

    public void setValidationResponse(String validationResponse) {
        this.validationResponse = validationResponse;
    }

}
