/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.invoices;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.sales.DealModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldBlockModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "addInvoiceBean")
@ViewScoped
public class AddInvoiceBean extends BeanFramework {

    private DealModel dealModel;
    private RecordModel customerModel;

    public AddInvoiceBean() {
    }

    @PostConstruct
    public void init() {
        super.setModuleId("25");
        super.loadForms(super.getModuleId(),0);
        super.setMode();
        super.setMasterPageName("invoices.xhtml");
        if (super.getRequestMode().equals(Defines.REQUEST_MODE_ADD)) {
            this.loadDeal();
            this.loadCustomer();
            this.initializeInvoiceBasicData();
        }
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(this.getKeyField(), Integer.valueOf(super.getRequestParams().get("recordId"))));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.recordModel = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddInvoiceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadDeal() {
        try {
            RequestHandler dealHandler = new RequestHandler();
            RequestModel request = new RequestModel();
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("deals_b2c_base.deal_id", super.getRequestParams().get("deal")));
            request.setRequestActionType("0");
            request.setRequestingModule("7");
            request.setClause(fm.getClause());
            ResponseModel response = dealHandler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
                this.dealModel = new DealModel();
                this.dealModel.setDealDetails(response.getRecordList().get(0));
                fm = new FilterManager();
                fm.setFilter("AND", new FilterType("deal_details.deal_id", super.getRequestParams().get("deal")));
                request.setRequestingModule("5");
                request.setClause(fm.getClause());
                ResponseModel ordersResponse = dealHandler.executeRequest(request);
                if (ordersResponse.getErrorCode() == 1000) {
                    this.dealModel.setOrderList(ordersResponse.getRecordList());
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddInvoiceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadCustomer() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule("6");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("customer_base.cust_id", Integer.valueOf(super.getRequestParams().get("cust"))));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.customerModel = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddInvoiceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initializeInvoiceBasicData() {
        if (this.getDefaultForm() != null) {
            for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                    ModuleFieldModel mfm;
                    switch (flm.getFieldName()) {
                        case "cust_id":
                            mfm = GeneralUtils.getFieldFromRecord(customerModel, "cust_id");
                            flm.getFieldObject().setCurrentValue(mfm.getCurrentValue());
                            this.loadRelationRecord("cust_id", mfm.getCurrentValue());
                            break;
                        case "deal_id":
                            mfm = GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "deal_id");
                            flm.getFieldObject().setCurrentValue(mfm.getCurrentValue());
                            this.loadRelationRecord("deal_id", mfm.getCurrentValue());
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void save() {
        if (!super.validateFields()) {
            return;
        }
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                this.edit();
                break;
            case Defines.REQUEST_MODE_ADD:
                super.populateDataFromForm();
                RequestModel request = new RequestModel();
                request.setRequestingModule(super.getModuleId());
                request.setRecordModel(super.recordModel);
                request.setRequestActionType("1");
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel response;
                try {
                    response = requestHandler.executeRequest(request, "GenericMasterAPI");
//                    this.updateMessage(response.getErrorCode(), response.getErrorMessage());
                    this.updateMessage(response.getErrorCode(), response.getErrorMessage(), "invoiceDetails.xhtml?recordId="
                            + response.getObjectId() + "&" + "moduleId=" + super.getModuleId());
                }
                catch (Exception ex) {
                    Logger.getLogger(AddInvoiceBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
    }

    @Override
    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("invoices.xhtml");
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddInvoiceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddInvoiceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddInvoiceBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public DealModel getDealModel() {
        return dealModel;
    }

    public void setDealModel(DealModel dealModel) {
        this.dealModel = dealModel;
    }

    public RecordModel getCustomerModel() {
        return customerModel;
    }

    public void setCustomerModel(RecordModel customerModel) {
        this.customerModel = customerModel;
    }

}
