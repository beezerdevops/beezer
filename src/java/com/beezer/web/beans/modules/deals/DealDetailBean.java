/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.deals;

import com.beezer.web.beans.modules.customers.CustomersBean;
import com.beezer.web.beans.base.RecordDetailManager;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.sales.DealModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "dealDetailBean")
@ViewScoped
public class DealDetailBean extends RecordDetailManager {

    private DealModel dealModel;
//    private ArrayList<DealModel> dealsList;
    private float totalValue;

    @ManagedProperty(value = "#{customersBean}")
    private CustomersBean customerBean;

    public DealDetailBean() {
    }

    @PostConstruct
    public void init() {
        super.setModuleId("7");
        this.setSelectedRecord();
        super.loadFieldSet(super.getModuleId(), null);
        this.loadRecord();
        this.loadRelationalModules();
        super.fillRecordDetails();
    }

    @Override
    public void setSelectedRecord() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        requestedId = params.get("recordId");
    }

    public void generateInvoice() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("addInvoice.xhtml?deal=" + requestedId
                    + "&" + "cust=" + dealModel.getDealDetails().getFieldValueMap().get("cust_id").getCurrentValue());
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(DealDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void voidDeal() {
        dealModel.getDealDetails().getFieldValueMap().get("deal_status").setCurrentValue("VOID");
        RequestModel request = new RequestModel();
        request.setRecordModel(this.dealModel.getDealDetails());
        request.setRequestActionType("3");
        request.setRequestingModule("7");
        RequestHandler requestHandler = new RequestHandler();
        try {
            ResponseModel response = requestHandler.executeRequest(request);
            super.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(DealDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            ResponseModel response = new ResponseModel();
            response.setErrorCode(2056);
            response.setErrorMessage("Failed to add active orders");
            super.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public void confirmDeal() {
    }

    public void createReturnRequest() {
//        try {
//            RequestContext context = RequestContext.getCurrentInstance();
//            context.execute("PF('invoiceDialog').show()");
//        }
//        catch (Exception ex) {
//            Logger.getLogger(DealDetailBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("addReturnOrder.xhtml?deal=" + requestedId
                    + "&" + "cust=" + dealModel.getDealDetails().getFieldValueMap().get("cust_id").getCurrentValue());
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(DealDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void loadRecord() {
        if (requestedId != null && !requestedId.isEmpty()) {
            try {
                RequestHandler dealHandler = new RequestHandler();
                RequestModel request = new RequestModel();
                FilterManager fm = new FilterManager();
                fm.setFilter("AND", new FilterType("deals_b2c_base.deal_id", Integer.valueOf(requestedId)));
                request.setRequestActionType("0");
                request.setClause(fm.getClause());
                request.setRequestingModule("7");
                ResponseModel response = dealHandler.executeRequest(request);
                if (response.getErrorCode() == 1000) {
                    this.dealModel = new DealModel();
                    this.dealModel.setDealDetails(response.getRecordList().get(0));
                    super.setRecordModel(response.getRecordList().get(0));
                    fm = new FilterManager();
                    fm.setFilter("AND", new FilterType("deal_details.deal_id", Integer.valueOf(requestedId)));
                    request.setClause(fm.getClause());
                    request.setRequestingModule("5");
                    ResponseModel ordersResponse = dealHandler.executeRequest(request);
                    if (ordersResponse.getErrorCode() == 1000) {
                        this.dealModel.setOrderList(ordersResponse.getRecordList());
                    }
                    customerBean.setCustId(dealModel.getDealDetails().getFieldValueMap().get("cust_id").getCurrentValue());
                }
            }
            catch (Exception ex) {
                Logger.getLogger(DealDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String trimTrailingZeros(String input) {
        return input.replaceAll("[0]*$", "").replaceAll(".$", "");
    }

    public void calaculateOrderValue() {
        totalValue = 0;
        for (RecordModel rm : this.dealModel.getOrderList()) {
            totalValue = totalValue
                    + Integer.valueOf(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "goods_selling_price").getCurrentValue());
        }
        calaculateGrandValue();
//        totalValue = 0;
//        for (OrderDetailsModel odm : orderList) {
//            totalValue = totalValue + odm.getQuantity() * Float.valueOf(odm.getSellable().getFieldValueMap().get("selling_price").getCurrentValue());
//        }
//        calaculateGrandValue();
    }

    public void calaculateGrandValue() {
        totalValue = 0;
        for (RecordModel rm : this.dealModel.getOrderList()) {
            totalValue = totalValue
                    + Integer.valueOf(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "goods_selling_price").getCurrentValue());
        }
        if (super.recordModel != null) {
            float costSalesTax = totalValue * (Float.valueOf(super.recordModel.getFieldValueMap().get("sales_tax").getCurrentValue()) / 100);
            float costVatTax = totalValue * (Float.valueOf(super.recordModel.getFieldValueMap().get("vat_tax").getCurrentValue()) / 100);
            float shippingCost = Float.valueOf(super.recordModel.getFieldValueMap().get("shipping_cost").getCurrentValue());
            float grandTotal = totalValue + costSalesTax + costVatTax + shippingCost;
            super.recordModel.getFieldValueMap().get("final_price").setCurrentValue(String.valueOf(grandTotal));
        }
    }

//    public void onTabChange(TabChangeEvent event) {
//        String tabId = event.getTab().getId();
//        switch (tabId) {
//            case "detailsTab":
////                this.loadProduct();
////                this.fillFieldDetails();
//                break;
//            case "customerTab":
//                customerBean.loadRelatedCustomer();
//                break;
//        }
//    }
    @Override
    public void onRowSelect(SelectEvent event) {
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", super.getRecordModel());
            FacesContext.getCurrentInstance().getExternalContext().redirect("addDeal.xhtml?recordId="
                    + super.getRecordModel().getFieldValueMap().get("deal_id").getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT);
        }
        catch (Exception ex) {
            Logger.getLogger(DealDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        RequestModel requestModel = new RequestModel();
        requestModel.setRequestActionType("2");
        requestModel.setRecordModel(recordModel);
        requestModel.setRequestingModule("7");
        RequestHandler handler = new RequestHandler();
        ResponseModel responseModel = handler.executeRequest(requestModel);
        if (responseModel.getErrorCode() == 1000) {
            requestModel.setBulkRecords(dealModel.getOrderList());
            requestModel.setRequestingModule("5");
            ResponseModel ordersResponse = handler.executeRequest(requestModel);
        }

        super.updateMessage(responseModel.getErrorCode(), responseModel.getErrorMessage());
    }

    public DealModel getDealModel() {
        return dealModel;
    }

    public void setDealModel(DealModel dealModel) {
        this.dealModel = dealModel;
    }

    public float getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(float totalValue) {
        this.totalValue = totalValue;
    }

    public CustomersBean getCustomerBean() {
        return customerBean;
    }

    public void setCustomerBean(CustomersBean customerBean) {
        this.customerBean = customerBean;
    }

}
