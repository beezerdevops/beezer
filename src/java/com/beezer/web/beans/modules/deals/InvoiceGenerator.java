/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.deals;

import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.sales.DealModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author badry
 */
public class InvoiceGenerator {

    private DealModel dealModel;
    private RecordModel invoiceDetails;
    private float subtotal;
    private final String invoiceId;
    private final String dealId;

    public InvoiceGenerator(String dealId, String invoiceId) {
        this.dealId = dealId;
        this.invoiceId = invoiceId;
        this.loadInvoiceDetails();
        this.loadDealModel();
    }

    public String generateHtmlInvoice() {
        String template = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<html>"
                + "   <head>"
                + "  <meta charset=\"utf-8\" />"
                + "  <title>Invoice</title>"
                + ""
                + setHtmlStyle()
                + "</head>"
                + ""
                + "<body>"
                + "  <div class=\"invoice-box\">"
                + "    <div class=\"row invoice-header\">"
                + "      <div class=\"col-xs-8\">"
                + "        <h1><strong>" + "INVOICE" + "</strong></h1>"
                + "      </div>"
                + "      <div class=\"media-left logo\">"
                + "        <img class=\"media-object logo\" src=\"" + GeneralUtils.getCompany().getCompanyLogo() + "\" />"
                + "      </div>"
                + "    </div>"
                + ""
                + "    <div class=\"row\">"
                + "      <div class=\"col-xs-6\">"
                + "        <h4><strong>CUSTOMER</strong></h4>"
                + "        <ul class=\"media-body list-unstyled details-list\">"
                + "          <li><strong>Name: </strong>" + GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "fullname").getCurrentValue() + "</li>"
                + "          <li><strong>Mobile: </strong>" + GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "cust_mobile").getCurrentValue() + "</li>"
                + "          <li><strong>Email: </strong>" + GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "cust_email").getCurrentValue() + "</li>"
                + "          <li><strong>Address: </strong>" + GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "cust_address").getCurrentValue() + "</li>"
                + "        </ul>"
                + "      </div>"
                + ""
                + ""
                + "      <div class=\"col-xs-6 pull-right invoice-details\">"
                + "        <h4><strong>INVOICE DETAILS</strong></h4>"
                + "        <ul class=\"media-body list-unstyled details-list\">"
                + generateInvoiceDetails()
                + "        </ul>"
                + "      </div>"
                + "    </div>"
                + ""
                + "    <div class=\"row\">"
                + "      <table cellpadding=\"0\" cellspacing=\"0\">"
                + "        <tr class=\"heading\">"
                + "          <td>Item</td>"
                + "          <td>Quantity</td>"
                + "          <td>Unit Price</td>"
                + "          <td>Total Price</td>"
                + "        </tr>"
                + generateItemizedBill()
                + "      </table>"
                + "    </div>"
                + "	"
                + "	<div class=\"row\">"
                + "	<div class=\"col-xs-6 pull-right invoice-details subtotal\">"
                + "        <table>"
                + "		<tbody>"
                + generateTotal()
                + "		</tbody>"
                + "		</table>"
                + "      </div>"
                + "	</div>"
                + "	"
                + "  </div>"
                + "</body>"
                + "</html>";
        return template;
    }

    private String setHtmlStyle() {
        return "<style> "
                + "        body { "
                + "            margin: 0; "
                + "        } "
                + "           "
                + "          .h1, h1 { "
                + "            font-size: 36px; "
                + "        	    margin-top: 20px; "
                + "            margin-bottom: 10px; "
                + "        	    margin: .67em 0; "
                + "        } "
                + "         "
                + "        .h4, h4 { "
                + "            font-size: 18px; "
                + "        	    margin-top: 10px; "
                + "            margin-bottom: 10px; "
                + "        } "
                + "         "
                + "        .row { "
                + "            margin-right: -15px; "
                + "            margin-left: -15px; "
                + "        } "
                + "           "
                + "          .row:after { "
                + "            clear: both; "
                + "        } "
                + "         "
                + "        :after, :before { "
                + "            -webkit-box-sizing: border-box; "
                + "            -moz-box-sizing: border-box; "
                + "            box-sizing: border-box; "
                + "        } "
                + "         "
                + "        .row:after, .row:before { "
                + "            display: table; "
                + "            content: \" \"; "
                + "        } "
                + "         "
                + "        .col-xs-8 { "
                + "            width: 66.66666667%; "
                + "        	float: left; "
                + "        	    position: relative; "
                + "            min-height: 1px; "
                + "            padding-right: 15px; "
                + "            padding-left: 15px; "
                + "        } "
                + "         "
                + "        .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{ "
                + "        	font-family: inherit; "
                + "            font-weight: 500; "
                + "            line-height: 1.1; "
                + "            color: inherit; "
                + "        	    margin-top: 20px; "
                + "            margin-bottom: 10px; "
                + "        } "
                + "         "
                + "        .col-xs-6 { "
                + "            width: 50%; "
                + "            float: left; "
                + "            position: relative; "
                + "            min-height: 1px; "
                + "            padding-right: 15px; "
                + "            padding-left: 15px; "
                + "        } "
                + "         "
                + "        .media-body { "
                + "            width: 10000px; "
                + "            overflow: hidden; "
                + "            zoom: 1; "
                + "            display: table-cell; "
                + "            vertical-align: top; "
                + "        } "
                + "         "
                + "        .list-unstyled { "
                + "            padding-left: 0; "
                + "            list-style: none; "
                + "        } "
                + "         "
                + "        * { "
                + "            -webkit-box-sizing: border-box; "
                + "            -moz-box-sizing: border-box; "
                + "            box-sizing: border-box; "
                + "        } "
                + "         "
                + "        .pull-right{ "
                + "        float:right; "
                + "        } "
                + "           "
                + "            .invoice-box { "
                + "              max-width: 800px; "
                + "              margin: auto; "
                + "              padding: 30px; "
                + "              padding-top: 0; "
                + "              border: 0px solid #eee; "
                + "              box-shadow: 0 0 10px rgba(0, 0, 0, .15); "
                + "              font-size: 16px; "
                + "              line-height: 24px; "
                + "              font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; "
                + "              color: #555555 !important; "
                + "            } "
                + "             "
                + "            .invoice-header { "
                + "              background-color: #fdffb7; "
                + "              margin-right: -30px; "
                + "              margin-left: -30px; "
                + "            } "
                + "             "
                + "            .invoice-box .row:nth-child(1) { "
                + "              background-color: #fdffb7; "
                + "              margin-right: -30px; "
                + "              margin-left: -30px; "
                + "            } "
                + "             "
                + "            .logo { "
                + "              float: right; "
                + "        	  padding: 6px; "
                + "            } "
                + "             "
                + "            .invoice-box table { "
                + "              width: 100%; "
                + "              line-height: inherit; "
                + "              text-align: left; "
                + "              color: #6e6e6e; "
                + "              font-size: 14px; "
                + "            } "
                + "             "
                + "            .invoice-details { "
                + "              width: 30%; "
                + "            } "
                + "             "
                + "            .details-list { "
                + "              font-size: 121px; "
                + "            } "
                + "             "
                + "            .heading { "
                + "              border-top: 2px solid #ff5858; "
                + "              border-bottom: 2px solid #ff5858; "
                + "              background-color: #fff; "
                + "            } "
                + "             "
                + "            .invoice-box table td { "
                + "              padding: 5px; "
                + "              vertical-align: top; "
                + "            } "
                + "             "
                + "            .invoice-box table tr td:nth-child(2) { "
                + "              text-align: center; "
                + "            } "
                + "             "
                + "            .invoice-box table tr td:nth-child(3) { "
                + "              text-align: center; "
                + "            } "
                + "             "
                + "            .invoice-box table tr td:nth-child(4) { "
                + "              text-align: center; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.top table td { "
                + "              padding-bottom: 20px; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.top table td.title { "
                + "              font-size: 45px; "
                + "              line-height: 45px; "
                + "              color: #333; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.information table td { "
                + "              padding-bottom: 40px; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.heading td { "
                + "              border-top: 2px solid #ff5858; "
                + "              border-bottom: 2px solid #ff5858; "
                + "              font-weight: bold; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.details td { "
                + "              padding-bottom: 20px; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.item td { "
                + "              border-bottom: 1px solid #eee; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.item.last td { "
                + "              border-bottom: none; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.total td:nth-child(4) { "
                + "              border-top: 2px solid #eee; "
                + "              font-weight: bold; "
                + "            } "
                + "             "
                + "            @media only screen and (max-width: 600px) { "
                + "              .invoice-box table tr.top table td { "
                + "                width: 100%; "
                + "                display: block; "
                + "                text-align: center; "
                + "              } "
                + "              .invoice-box table tr.information table td { "
                + "                width: 100%; "
                + "                display: block; "
                + "                text-align: center; "
                + "              } "
                + "            } "
                + "             "
                + "            .rtl { "
                + "              direction: rtl; "
                + "              font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; "
                + "            } "
                + "             "
                + "            .rtl table { "
                + "              text-align: right; "
                + "            } "
                + "             "
                + "            .rtl table tr td:nth-child(2) { "
                + "              text-align: left; "
                + "            } "
                + "        	 "
                + "        	.subtotal table tbody tr td:nth-child(1){ "
                + "        	text-align:right !important; "
                + "        	} "
                + "        	 "
                + "        	.subtotal{ "
                + "        	margin-right: 2em; "
                + "        	} "
                + "        	 "
                + "        	.subtotal table td { "
                + "            padding: 1px; "
                + "        } "
                + "    </style>";
    }

    private String generateInvoiceDetails() {
        return "<li><strong>Number: </strong>" + (GeneralUtils.getFieldFromRecord(this.invoiceDetails, "invoice_number") != null ? GeneralUtils.getFieldFromRecord(this.invoiceDetails, "invoice_number").getCurrentValue() : "") + "</li>"
                + "<li><strong>Date: </strong>" + (GeneralUtils.getFieldFromRecord(this.invoiceDetails, "added_date") != null ? GeneralUtils.getFieldFromRecord(this.invoiceDetails, "added_date").getCurrentValue() : "") + "</li>"
                + "<li><strong>Payment: </strong>" + (GeneralUtils.getFieldFromRecord(this.invoiceDetails, "payment_type.name") != null ? GeneralUtils.getFieldFromRecord(this.invoiceDetails, "payment_type.name").getCurrentValue() : "") + "</li>";
//        return "<li><strong>Number: </strong>1256398</li>" +
//"          <li><strong>Date: </strong>2018-01-13</li>" +
//"          <li><strong>Payment: </strong>CASH</li>";
    }

    private String generateMyCompanyInfo() {
        String myCompanyInfo = GeneralUtils.getCompany().getName() + "<br/>";
        myCompanyInfo = myCompanyInfo + GeneralUtils.getCompany().getAddress() + "<br/>";
        myCompanyInfo = myCompanyInfo + GeneralUtils.getCompany().getCity()
                + ", " + GeneralUtils.getCompany().getCountry() + "<br/>";
        return myCompanyInfo;
    }

    private String generateRecepientDetails() {
        String recepientDetails = "";
        boolean contactFound = false;
        for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
            if (entry.getValue().getFieldName().equals("contact_name")) {
                if (entry.getValue().getCurrentValue() != null) {
                    contactFound = true;
                    recepientDetails = recepientDetails + "Recepient: " + entry.getValue().getCurrentValue() + "<br/>";
                    break;
                }
            }
        }

        if (contactFound) {
            for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
                if (entry.getValue().getFieldName().equals("account_name")) {
                    recepientDetails = recepientDetails + "Company: " + entry.getValue().getCurrentValue() + "<br/>";
                    break;
                }
            }
        }
        else {
            for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
                if (entry.getValue().getFieldName().equals("cust_firstname")) {
                    recepientDetails = recepientDetails + "Receiver: " + entry.getValue().getCurrentValue() + "";
                    break;
                }
            }

            for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
                if (entry.getValue().getFieldName().equals("cust_lastname")) {
                    recepientDetails = recepientDetails + " " + entry.getValue().getCurrentValue() + "";
                    break;
                }
            }
        }

        return recepientDetails;
    }

    private String generateItemizedBill() {
        String itemizedBill = "";
        subtotal = 0;
        if (dealModel.getOrderList() != null) {
            for (RecordModel rm : dealModel.getOrderList()) {
                itemizedBill = itemizedBill + "<tr class=\"item\">"
                        + "<td>" + GeneralUtils.getFieldFromRecord(rm, "item_name").getCurrentValue() + "</td>"
                        + "<td>" + GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue() + "</td>"
                        + "<td>" + GeneralUtils.getFieldFromRecord(rm, "goods_selling_price").getCurrentValue() + "</td>"
                        + "<td>" + GeneralUtils.getFieldFromRecord(rm, "net_value").getCurrentValue() + "</td>"
                        + "</tr>";
                subtotal = subtotal + Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "net_value").getCurrentValue());
            }
        }
        return itemizedBill;
    }

    private String generateTotal() {
        String totalAmount = "";
        float vat = subtotal * ((GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "vat_tax").getCurrentValue() != null && !GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "vat_tax").getCurrentValue().isEmpty()) ? Float.valueOf(GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "vat_tax").getCurrentValue()) : 0);
        float salesTax = subtotal * ((GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "sales_tax").getCurrentValue() != null && !GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "sales_tax").getCurrentValue().isEmpty()) ? Float.valueOf(GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "sales_tax").getCurrentValue()) : 0);
        float shipmentCost = (GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "shipping_cost").getCurrentValue() != null && !GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "shipping_cost").getCurrentValue().isEmpty()) ? Float.valueOf(GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "shipping_cost").getCurrentValue()) : 0;
        totalAmount = "<tr>"
                + "		<td><strong>Subtotal: </strong></td>"
                + "		<td>" + subtotal + "</td>"
                + "		</tr>"
                + "		<tr>"
                + "		<td><strong>VAT: </strong></td>"
                + "		<td>" + vat + "</td>"
                + "		</tr>"
                + "		<tr>"
                + "		<td><strong>Tax: </strong></td>"
                + "		<td>" + salesTax + "</td>"
                + "		</tr>"
                + "		<tr>"
                + "		<td><strong>Shipment: </strong></td>"
                + "		<td>" + shipmentCost + "</td>"
                + "		</tr>"
                + "		<tr>"
                + "		<td><strong>Total: </strong></td>"
                + "		<td>" + (subtotal + vat + salesTax + shipmentCost) + "</td>"
                + "		</tr>";
        return totalAmount;
    }

    private void loadInvoiceDetails() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule("25");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("invoice_id", invoiceId));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                invoiceDetails = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(InvoiceGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadDealModel() {
        RequestModel requestModel = new RequestModel();
        requestModel.setRequestActionType("0");
        requestModel.setRequestingModule("7");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("deals_b2c_base.deal_id", Integer.valueOf(dealId)));
        requestModel.setClause(fm.getClause());
        RequestHandler handler = new RequestHandler();
        ResponseModel responseModel = handler.executeRequest(requestModel);
        if(responseModel.getErrorCode() == 1000){
            this.dealModel = new DealModel();
            this.dealModel.setDealDetails(responseModel.getRecordList().get(0));
            this.loadOrderDetails();
        }
    }

    private void loadOrderDetails() {
        RequestModel requestModel = new RequestModel();
        requestModel.setRequestActionType("0");
        requestModel.setRequestingModule("5");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("deal_details.deal_id", Integer.valueOf(dealId)));
        requestModel.setClause(fm.getClause());
        RequestHandler handler = new RequestHandler();
        ResponseModel responseModel = handler.executeRequest(requestModel);
        if (responseModel.getErrorCode() == 1000) {
            this.dealModel.setOrderList(responseModel.getRecordList());
        }
    }
}
