/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.deals;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.customers.CustomersBean;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.AttributeVariantModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.comparators.FormFieldOrderComparator;
import com.crm.models.global.sales.DealModel;
import com.crm.models.global.sales.OrderDetailsModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldBlockModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

/**
 *
 * @author badry
 */
@ManagedBean(name = "addDealBean")
@ViewScoped
public class AddDealBean extends BeanFramework {

    private RecordModel selectedCustomer;
    private ArrayList<ModuleFieldModel> customerFieldList;
    private ArrayList<ModuleFieldModel> orderFieldList;
    private ArrayList<RecordModel> selectedProducts;
    private ArrayList<RecordModel> selectedServices;
    private DealModel dealModel;
    private ArrayList<OrderDetailsModel> orderList;
    private ArrayList<RecordModel> orderRecordList;
    private ArrayList<OrderDetailsModel> orderProductList;
    private ArrayList<RecordModel> orderRecordProductList;
    private ArrayList<OrderDetailsModel> orderServiceList;
    private ArrayList<RecordModel> orderRecordServiceList;
    private float totalValue;

    private ArrayList<AttributeVariantModel> attributeVariantList;
    private LinkedHashMap<String, AttributeVariantModel> selectedAttributes;
    private LinkedHashMap<String, LinkedHashMap<String, AttributeVariantModel>> selectedVariantAttributes;

    private boolean activeRecordsAvailable = false;

    public AddDealBean() {
    }

    @PostConstruct
    public void init() {
        super.setModuleId("7");
        this.selectedVariantAttributes = new LinkedHashMap<>();
        this.attributeVariantList = new ArrayList<>();
        this.selectedAttributes = new LinkedHashMap<>();
        this.orderList = new ArrayList<>();
        this.orderFieldList = new ArrayList<>();
        this.orderProductList = new ArrayList<>();
        this.orderServiceList = new ArrayList<>();
        this.orderRecordList = new ArrayList<>();
        this.orderRecordProductList = new ArrayList<>();
        this.orderRecordServiceList = new ArrayList<>();
        this.dealModel = new DealModel();
        super.loadFieldSet("7", null);
        this.loadOrderFieldList();
        super.loadForms("7",0);
        super.buildRecordModel();
        super.setMode();
//        this.setDefaultDealName();
    }

    public void setDefaultDealName() {
        if (super.getDefaultForm() != null) {
            for (FieldBlockModel fbm : super.getDefaultForm().getBlockList()) {
                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                    if (flm.getFieldObject().getFieldName().equals("deal_name")) {
                        SimpleDateFormat dt1 = new SimpleDateFormat("ddMMYY.HHmmss");
//                        System.out.println(dt1.format(new Date()));
                        flm.getFieldObject().setCurrentValue("DEAL" + dt1.format(new Date()));
                        break;
                    }
                }
            }
        }
    }

    public void loadOrderFieldList() {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId("5");
            request.setLayoutId(null);
            RequestHandler requestHandler = new RequestHandler();
            ModuleFieldResponse response = requestHandler.getFieldSet(request);
            if (response.getErrorCode() == 1000) {
                this.orderFieldList = response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRadioValueChange(String s1, String s2) {
        AttributeVariantModel avm = new AttributeVariantModel();
        avm.setAttributeName(s2);
        selectedAttributes.put(s1, avm);
    }

    public void onVariantProductExpand(ToggleEvent event) {
        if (event.getVisibility().equals(Visibility.VISIBLE)) {
            try {
                RecordModel rm = (RecordModel) event.getData();
                if (selectedVariantAttributes.get(rm.getFieldValueMap().get("product_id").getCurrentValue()) != null
                        && !selectedVariantAttributes.get(rm.getFieldValueMap().get("product_id").getCurrentValue()).isEmpty()) {
                }
                else {
                    selectedAttributes = new LinkedHashMap<>();
                    super.loadLookup("attribute", rm.getFieldValueMap().get("attribute").getFieldValues());
                    LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(GeneralUtils.getFieldFromRecord(rm, "attr_map").getCurrentValue());
                    this.attributeVariantList = new ArrayList<>();
                    for (Map.Entry<String, String> entry : kvmap.entrySet()) {
                        AttributeVariantModel avm = new AttributeVariantModel();
                        avm.setAttributeId(entry.getKey());
                        avm.setAttributeValues(new ArrayList<>(Arrays.asList(entry.getValue().split("\\s*,\\s*"))));
                        attributeVariantList.add(avm);
                        selectedAttributes.put(entry.getKey(), new AttributeVariantModel());
                        this.onAttributeSelect(avm.getAttributeId());
                    }
                    selectedVariantAttributes.put(rm.getFieldValueMap().get("product_id").getCurrentValue(), selectedAttributes);
//                    RecordModel vRec = this.interpretVariant(rm);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void onAttributeSelect(String selection) {
        String attrKeyName = null;
        TreeMap<String, String> attMap = super.getLookupDictionary().get("attribute");
        for (Map.Entry<String, String> entry : attMap.entrySet()) {
            if (entry.getValue().equals(selection)) {
                attrKeyName = entry.getKey();
                break;
            }
        }

        for (AttributeVariantModel avm : attributeVariantList) {
            if (avm.getAttributeId().equals(selection)) {
                avm.setAttributeName(attrKeyName);
                break;
            }
        }
    }

    public ResponseModel saveActiveDeal(String actionType) {
        ArrayList<RecordModel> activeOrders = new ArrayList<>();
        for (RecordModel rm : orderRecordList) {
            if (rm.getFieldValueMap().get("item_status").getCurrentValue() == null
                    || rm.getFieldValueMap().get("item_status").getCurrentValue().equals("COMPLETE")) {
                activeOrders.add(rm);
            }
        }

        if (!activeOrders.isEmpty()) {
            recordModel.getFieldValueMap().get("deal_status").setCurrentValue("NEW");
            String netValue = this.calaculateRecordGrandValue(recordModel, activeOrders);
            recordModel.getFieldValueMap().get("final_price").setCurrentValue(netValue);
            this.dealModel.setDealDetails(super.recordModel);
            this.dealModel.setOrderList(activeOrders);

            RequestModel requestModel = new RequestModel();
            requestModel.setRecordModel(super.recordModel);
            requestModel.setRequestActionType(actionType);
            requestModel.setRequestingModule("7");
            RequestHandler handler = new RequestHandler();
            try {
                ResponseModel responseModel = handler.executeRequest(requestModel);
                if (responseModel.getErrorCode() == 1000) {
                    for (RecordModel rm : activeOrders) {
                        rm.getFieldValueMap().get("deal_id").setCurrentValue(String.valueOf(responseModel.getObjectId()));
                    }
                    ResponseModel orderResponse = this.saveOrderDetails(actionType, activeOrders);
                    if (orderResponse.getErrorCode() == 1000) {
                        this.activeRecordsAvailable = true;
                        this.dealModel.getDealDetails().getFieldValueMap().get("deal_id").setCurrentValue(String.valueOf(responseModel.getObjectId()));
                    }
                    return orderResponse;
                }
                return responseModel;
            }
            catch (Exception ex) {
                Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
                ResponseModel response = new ResponseModel();
                response.setErrorCode(2056);
                response.setErrorMessage("Failed to add active orders");
                return response;
            }

//            DealRequest request = new DealRequest();
//            request.setDealModel(this.dealModel);
//            request.setActionType(actionType);
//            DealHandler requestHandler = new DealHandler();
//            try {
//                DealResponse response = requestHandler.dealExecutor(request);
//                if (response.getErrorCode() == 1000) {
//                    this.activeRecordsAvailable = true;
//                    this.dealModel.getDealDetails().getFieldValueMap().get("deal_id").setCurrentValue(String.valueOf(response.getObjectId()));
//                }
//                return response;
//            }
//            catch (Exception ex) {
//                Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
//                ResponseModel response = new ResponseModel();
//                response.setErrorCode(2056);
//                response.setErrorMessage("Failed to add active orders");
//                return response;
//            }
        }
        else {
            this.activeRecordsAvailable = false;
            ResponseModel response = new ResponseModel();
            response.setErrorCode(1000);
            response.setErrorMessage("No active orders");
            return response;
        }
    }

    public ResponseModel saveBackOrderDeal(String actionType) {
        ArrayList<RecordModel> backOrders = new ArrayList<>();
        for (RecordModel rm : orderRecordList) {
            if (rm.getFieldValueMap().get("item_status").getCurrentValue() != null
                    && rm.getFieldValueMap().get("item_status").getCurrentValue().equals("ONHOLD")) {
                backOrders.add(rm);
            }
        }

        if (!backOrders.isEmpty()) {
            RecordModel dealDetails = GeneralUtils.deepCopyRecord(recordModel);
            dealDetails.getFieldValueMap().get("deal_id").setCurrentValue(null);
            dealDetails.getFieldValueMap().get("deal_status").setCurrentValue("ONHOLD");
            String netValue = this.calaculateRecordGrandValue(dealDetails, backOrders);
            dealDetails.getFieldValueMap().get("final_price").setCurrentValue(netValue);
            DealModel dealRecord = new DealModel();
            dealRecord.setDealDetails(dealDetails);
            dealRecord.setOrderList(backOrders);

            RequestModel requestModel = new RequestModel();
            requestModel.setRecordModel(dealDetails);
            requestModel.setRequestActionType(actionType);
            requestModel.setRequestingModule("7");
            RequestHandler handler = new RequestHandler();
            try {
                ResponseModel response = handler.executeRequest(requestModel);
                if (response.getErrorCode() == 1000) {
                    dealRecord.getDealDetails().getFieldValueMap().get("deal_id").setCurrentValue(String.valueOf(response.getObjectId()));
                    for (RecordModel rm : backOrders) {
                        rm.getFieldValueMap().get("deal_id").setCurrentValue(String.valueOf(response.getObjectId()));
                    }
                    ResponseModel orderResponse = this.saveOrderDetails(actionType, backOrders);
                }

                if (!this.activeRecordsAvailable) {
                    this.dealModel = dealRecord;
                }
                return response;
            }
            catch (Exception ex) {
                Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
                ResponseModel response = new ResponseModel();
                response.setErrorCode(2056);
                response.setErrorMessage("Failed to add on hold orders");
                return response;
            }
        }
        else {
            ResponseModel response = new ResponseModel();
            response.setErrorCode(1000);
            response.setErrorMessage("No on hold orders");
            return response;
        }
    }

    public ResponseModel saveOrderDetails(String actionType, ArrayList<RecordModel> orderList) {
        try {
            RequestModel requestModel = new RequestModel();
            requestModel.setBulkRecords(orderList);
            requestModel.setRequestActionType(actionType);
            requestModel.setRequestingModule("5");
            RequestHandler handler = new RequestHandler();
            ResponseModel responseModel = handler.executeRequest(requestModel);
            return responseModel;
        }
        catch (Exception ex) {
            Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
            ResponseModel responseModel = new ResponseModel();
            responseModel.setErrorCode(2056);
            responseModel.setErrorMessage("Failed to add order list");
            return responseModel;
        }
    }

    @Override
    public void loadRecord() {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        try {
            RequestHandler dealHandler = new RequestHandler();
            RequestModel request = new RequestModel();
            request.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("deals_b2c_base.deal_id", Integer.valueOf(super.getRequestParams().get("recordId"))));
            request.setClause(fm.getClause());
            request.setRequestingModule("7");
            ResponseModel response = dealHandler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
                super.setRecordModel(response.getRecordList().get(0));
                RequestModel requestForDetails = new RequestModel();
                requestForDetails.setRequestActionType("0");
                requestForDetails.setRequestingModule("5");
                FilterManager fmForDetails = new FilterManager();
                fmForDetails.setFilter("AND", new FilterType("deal_details.deal_id", super.getRecordModel().getFieldValueMap().get("deal_id").getCurrentValue()));
                requestForDetails.setClause(fmForDetails.getClause());
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel responseForDetails;
                try {
                    responseForDetails = requestHandler.executeRequest(requestForDetails, "GenericMasterAPI");
                    if (responseForDetails.getErrorCode() == 1000) {
                        this.setOrderRecordList(responseForDetails.getRecordList());
                    }
                }
                catch (Exception ex) {
                    Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (this.getOrderRecordList() != null) {
                    for (RecordModel rm : this.orderRecordList) {
                        if (GeneralUtils.getFieldFromRecord(rm, "goods_type").getCurrentValue().equals("PRODUCT")) {
                            this.orderRecordProductList.add(rm);
                        }
                        else {
                            this.orderRecordServiceList.add(rm);
                        }
                    }
                }

                CustomersBean customersBean = new CustomersBean();
                this.selectedCustomer = customersBean.loadRelatedCustomers(super.getRecordModel().getFieldValueMap().get("cust_id").getCurrentValue()).get(0);
                this.customerFieldList = new ArrayList<>();
                for (Map.Entry<String, ModuleFieldModel> entry : selectedCustomer.getFieldValueMap().entrySet()) {
                    this.customerFieldList.add(entry.getValue());
                }
                Collections.sort(this.customerFieldList, new FormFieldOrderComparator());
                this.calaculateOrderValue();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    @Override
    public void save() {
        String actionType = "1";
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                actionType = "3";
                break;
            case Defines.REQUEST_MODE_ADD:
                actionType = "1";
                break;
        }
        if (super.validateFields()) {
            super.populateFromAllFields();
            if (selectedCustomer != null) {
                super.recordModel.getFieldValueMap().get("cust_id").setCurrentValue(selectedCustomer.getFieldValueMap().get("cust_id").getCurrentValue());
            }
            ResponseModel response = this.saveActiveDeal(actionType);
            if (response.getErrorCode() == 1000) {
                response = this.saveBackOrderDeal(actionType);
            }
            this.updateMessage(response.getErrorCode(), response.getErrorMessage(), "dealDetails.xhtml?recordId="
                    + this.dealModel.getDealDetails().getFieldValueMap().get("deal_id").getCurrentValue());
        }
    }

    @Override
    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("deals.xhtml");
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createOrderForDeal() {
        try {
            RequestModel request = new RequestModel();
            LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
            ModuleFieldRequest moduleFieldRequest = new ModuleFieldRequest();
            moduleFieldRequest.setModuleId("13");
            moduleFieldRequest.setLayoutId(null);
            RequestHandler requestHandler = new RequestHandler();
            ModuleFieldResponse moduleFieldResponse = requestHandler.getFieldSet(moduleFieldRequest);
            if (moduleFieldResponse.getErrorCode() == 1000) {
                for (ModuleFieldModel mfm : moduleFieldResponse.getReturnList()) {
                    switch (mfm.getFieldName()) {
                        case "deal_id":
                            mfm.setCurrentValue(this.dealModel.getDealDetails().getFieldValueMap().get("deal_id").getCurrentValue());
                            fieldValueMap.put(mfm.getFieldName(), mfm);
                            break;
                        case "cust_id":
                            mfm.setCurrentValue(this.dealModel.getDealDetails().getFieldValueMap().get("cust_id").getCurrentValue());
                            fieldValueMap.put(mfm.getFieldName(), mfm);
                            break;
                        case "order_status":
                            mfm.setCurrentValue("1");
                            fieldValueMap.put(mfm.getFieldName(), mfm);
                            break;
                    }
                }
                RecordModel orderRecord = new RecordModel();
                orderRecord.setFieldValueMap(fieldValueMap);
                request.setRecordModel(orderRecord);
                request.setRequestActionType("1");
                RequestHandler orderRequestHandler = new RequestHandler();
                ResponseModel orderResponse = orderRequestHandler.executeRequest(request, "OrdersMasterAPI");
                if (orderResponse.getErrorCode() == 1000) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("deals.xhtml");
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onCustomerSelect(SelectEvent event) {
        this.selectedCustomer = (RecordModel) event.getObject();
        this.customerFieldList = new ArrayList<>();
        for (Map.Entry<String, ModuleFieldModel> entry : selectedCustomer.getFieldValueMap().entrySet()) {
            this.customerFieldList.add(entry.getValue());
        }
        Collections.sort(this.customerFieldList, new FormFieldOrderComparator());
    }

    public RecordModel constructOrderRecord() {
        RecordModel orderRecord = new RecordModel();
        orderRecord.setFieldValueMap(new LinkedHashMap<>());
        ArrayList<ModuleFieldModel> tempFieldList = (ArrayList<ModuleFieldModel>) orderFieldList.clone();
        for (ModuleFieldModel mfm : tempFieldList) {
            orderRecord.getFieldValueMap().put(mfm.getFieldName(), new ModuleFieldModel(mfm));
        }
        return orderRecord;
    }

    public RecordModel interpretVariant(RecordModel recordModel) {
        String parentProduct = recordModel.getFieldValueMap().get("product_id").getCurrentValue();
        LinkedHashMap<String, AttributeVariantModel> kavm = selectedVariantAttributes.get(parentProduct);
        LinkedHashMap<String, String> kvmap = new LinkedHashMap<>();
        for (Map.Entry<String, AttributeVariantModel> entry : kavm.entrySet()) {
            kvmap.put(entry.getKey(), entry.getValue().getAttributeName());
        }
        RequestModel request = new RequestModel();
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("products_base.parent_product", "=", parentProduct));
        fm.setFilter("AND", new FilterType("products_base.attr_map", "=", GeneralUtils.serializeRequest(kvmap)));
        request.setClause(fm.getClause());
        request.setRequestingModule("3");
        request.setRequestActionType("0");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000 && !response.getRecordList().isEmpty()) {
                return response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addProductToOrderList() {
        RecordModel orderProduct;
        this.orderRecordList.clear();
//        this.orderRecordProductList.clear();
        for (RecordModel pr : this.selectedProducts) {
            orderProduct = this.constructOrderRecord();
            if (pr.getFieldValueMap().get("has_variant").getCurrentValue().equals("true")) {
                RecordModel vpr = this.interpretVariant(pr);
                if (vpr != null) {
                    orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_id", vpr.getFieldValueMap().get("product_id").getCurrentValue());
                    orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_type", "PRODUCT");
                    orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "quantity", "1");
                    orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_raw_price", vpr.getFieldValueMap().get("raw_price").getCurrentValue());
                    orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_selling_price", vpr.getFieldValueMap().get("selling_price").getCurrentValue());
                    orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "product_code", vpr.getFieldValueMap().get("product_code").getCurrentValue());
                    ModuleFieldModel mfmStock = vpr.getFieldValueMap().get("current_stock");
                    if (mfmStock.getCurrentValue() != null && !mfmStock.getCurrentValue().isEmpty()) {
                        if (Float.valueOf(mfmStock.getCurrentValue()) <= 0) {
                            orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "item_status", "ONHOLD");
                        }
                    }
                    orderProduct.getFieldValueMap().put("current_dummy_stock", new ModuleFieldModel(mfmStock));
                    float updatedStock = Float.valueOf(orderProduct.getFieldValueMap().get("current_dummy_stock").getCurrentValue()) - 1;
                    orderProduct.getFieldValueMap().get("current_dummy_stock").setCurrentValue(String.valueOf(updatedStock));
                    orderProduct.getFieldValueMap().put("current_stock", new ModuleFieldModel(mfmStock));
                    String prName = "";
                    try {
                        LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(vpr.getFieldValueMap().get("attr_map").getCurrentValue());
                        for (Map.Entry<String, String> entry : kvmap.entrySet()) {
                            prName = prName + "/" + entry.getValue();
                        }
                    }
                    catch (Exception ex) {
                        Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "product_name", vpr.getFieldValueMap().get("product_name").getCurrentValue() + prName);
                }
                else {
                    return;
                }
            }
            else {
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_id", pr.getFieldValueMap().get("product_id").getCurrentValue());
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_type", "PRODUCT");
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "quantity", "1");
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_raw_price", pr.getFieldValueMap().get("raw_price").getCurrentValue());
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_selling_price", pr.getFieldValueMap().get("selling_price").getCurrentValue());
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "product_code", pr.getFieldValueMap().get("product_code").getCurrentValue());
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "product_name", pr.getFieldValueMap().get("product_name").getCurrentValue());
                ModuleFieldModel mfmStock = pr.getFieldValueMap().get("current_stock");
                if (mfmStock.getCurrentValue() != null && !mfmStock.getCurrentValue().isEmpty()) {
                    if (Float.valueOf(mfmStock.getCurrentValue()) <= 0) {
                        orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "item_status", "ONHOLD");
                    }
                }
                orderProduct.getFieldValueMap().put("current_stock", new ModuleFieldModel(mfmStock));
                orderProduct.getFieldValueMap().put("current_dummy_stock", new ModuleFieldModel(mfmStock));
                float updatedStock = Float.valueOf(orderProduct.getFieldValueMap().get("current_dummy_stock").getCurrentValue()) - 1;
                orderProduct.getFieldValueMap().get("current_dummy_stock").setCurrentValue(String.valueOf(updatedStock));
            }
            this.orderRecordProductList.add(orderProduct);
        }

        this.selectedProducts.clear();
        this.orderRecordList.addAll(orderRecordServiceList);
        this.orderRecordList.addAll(orderRecordProductList);
        calaculateOrderValue();
    }

    public void addServiceToOrderList() {
        RecordModel orderService;
        this.orderRecordList.clear();
        this.orderRecordServiceList.clear();
        for (RecordModel sr : this.selectedServices) {
            orderService = this.constructOrderRecord();
            orderService = GeneralUtils.setFieldValueInRecord(orderService, "goods_id", sr.getFieldValueMap().get("service_id").getCurrentValue());
            orderService = GeneralUtils.setFieldValueInRecord(orderService, "goods_type", "SERVICE");
            orderService = GeneralUtils.setFieldValueInRecord(orderService, "goods_raw_price", sr.getFieldValueMap().get("raw_price").getCurrentValue());
            orderService = GeneralUtils.setFieldValueInRecord(orderService, "goods_selling_price", sr.getFieldValueMap().get("selling_price").getCurrentValue());
            orderService = GeneralUtils.setFieldValueInRecord(orderService, "quantity", "1");
            orderService = GeneralUtils.setFieldValueInRecord(orderService, "code", sr.getFieldValueMap().get("code").getCurrentValue());
            orderService = GeneralUtils.setFieldValueInRecord(orderService, "name", sr.getFieldValueMap().get("name").getCurrentValue());
//            this.orderRecordList.add(orderService);
            this.orderRecordServiceList.add(orderService);
//            OrderDetailsModel odm = new OrderDetailsModel();
//            odm.setGoodsType("SERVICE");
//            odm.setSellable(sr);
//            odm.setGoodsId(Integer.parseInt(sr.getFieldValueMap().get("service_id").getCurrentValue()));
//            this.orderServiceList.add(odm);
//            this.orderList.add(odm);

        }
        this.orderRecordList.addAll(orderRecordServiceList);
        this.orderRecordList.addAll(orderRecordProductList);
        calaculateOrderValue();
    }

    public void modifyQuantity(RecordModel rm) {
        String curStock = rm.getFieldValueMap().get("current_stock").getCurrentValue();
        float newStock = Float.valueOf(curStock) - Float.valueOf(rm.getFieldValueMap().get("quantity").getCurrentValue());
        rm.getFieldValueMap().get("current_dummy_stock").setCurrentValue(String.valueOf(newStock));
        this.calaculateOrderValue();
    }

    public void calaculateOrderValue() {
        totalValue = 0;
        for (RecordModel rm : orderRecordList) {
            totalValue = totalValue
                    + Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "goods_selling_price").getCurrentValue());
        }
        calaculateGrandValue();
    }

    public void calaculateGrandValue() {
//        totalValue = 0;
//        for (OrderDetailsModel odm : orderList) {
//            totalValue = totalValue + odm.getQuantity() * Float.valueOf(odm.getSellable().getFieldValueMap().get("selling_price").getCurrentValue());
//        }
        totalValue = 0;
        for (RecordModel rm : orderRecordList) {
            totalValue = totalValue
                    + Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "goods_selling_price").getCurrentValue());
        }
        if (super.recordModel != null) {
            if (super.recordModel.getFieldValueMap().get("sales_tax").getCurrentValue() == null) {
                super.recordModel.getFieldValueMap().get("sales_tax").setCurrentValue("0");
            }
            if (super.recordModel.getFieldValueMap().get("vat_tax").getCurrentValue() == null) {
                super.recordModel.getFieldValueMap().get("vat_tax").setCurrentValue("0");
            }
            if (super.recordModel.getFieldValueMap().get("shipping_cost").getCurrentValue() == null) {
                super.recordModel.getFieldValueMap().get("shipping_cost").setCurrentValue("0");
            }
            float costSalesTax = totalValue * (Float.valueOf(super.recordModel.getFieldValueMap().get("sales_tax").getCurrentValue()) / 100);
            float costVatTax = totalValue * (Float.valueOf(super.recordModel.getFieldValueMap().get("vat_tax").getCurrentValue()) / 100);
            float shippingCost = Float.valueOf(super.recordModel.getFieldValueMap().get("shipping_cost").getCurrentValue());
            float grandTotal = totalValue + costSalesTax + costVatTax + shippingCost;
            super.recordModel.getFieldValueMap().get("final_price").setCurrentValue(String.valueOf(grandTotal));
        }
    }

    public String calaculateRecordGrandValue(RecordModel master, ArrayList<RecordModel> orderDetails) {
        totalValue = 0;
        for (RecordModel rm : orderDetails) {
            totalValue = totalValue
                    + Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "goods_selling_price").getCurrentValue());
        }
        if (master != null) {
            if (master.getFieldValueMap().get("sales_tax").getCurrentValue() == null) {
                master.getFieldValueMap().get("sales_tax").setCurrentValue("0");
            }
            if (master.getFieldValueMap().get("vat_tax").getCurrentValue() == null) {
                master.getFieldValueMap().get("vat_tax").setCurrentValue("0");
            }
            if (master.getFieldValueMap().get("shipping_cost").getCurrentValue() == null) {
                master.getFieldValueMap().get("shipping_cost").setCurrentValue("0");
            }
            float costSalesTax = totalValue * (Float.valueOf(master.getFieldValueMap().get("sales_tax").getCurrentValue()) / 100);
            float costVatTax = totalValue * (Float.valueOf(master.getFieldValueMap().get("vat_tax").getCurrentValue()) / 100);
            float shippingCost = Float.valueOf(master.getFieldValueMap().get("shipping_cost").getCurrentValue());
            float grandTotal = totalValue + costSalesTax + costVatTax + shippingCost;
//            float grandTotal = totalValue;
            return String.valueOf(grandTotal);
        }
        return "0";
    }

    public String trimTrailingZeros(String input) {
        return input.replaceAll("[0]*$", "").replaceAll(".$", "");
    }

    public void deleteSelectedProduct(RecordModel orderDetailsModel) {
//        if (this.orderList != null && this.orderProductList != null) {
//            this.orderProductList.remove(orderDetailsModel);
//            orderList.remove(orderDetailsModel);
//        }
        if (this.orderRecordList != null && this.orderRecordProductList != null) {
            this.orderRecordProductList.remove(orderDetailsModel);
            orderRecordList.remove(orderDetailsModel);
        }
    }

    public void deleteSelectedService(RecordModel orderDetailsModel) {
//        if (this.orderList != null && this.orderServiceList != null) {
//            this.orderServiceList.remove(orderDetailsModel);
//            orderList.remove(orderDetailsModel);
//        }
        if (this.orderRecordList != null && this.orderRecordServiceList != null) {
            this.orderRecordServiceList.remove(orderDetailsModel);
            orderRecordList.remove(orderDetailsModel);
        }
    }

    public RecordModel getSelectedCustomer() {
        return selectedCustomer;
    }

    public void setSelectedCustomer(RecordModel selectedCustomer) {
        this.selectedCustomer = selectedCustomer;
    }

    public ArrayList<ModuleFieldModel> getCustomerFieldList() {
        return customerFieldList;
    }

    public void setCustomerFieldList(ArrayList<ModuleFieldModel> customerFieldList) {
        this.customerFieldList = customerFieldList;
    }

    public ArrayList<RecordModel> getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedProducts(ArrayList<RecordModel> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    public ArrayList<RecordModel> getSelectedServices() {
        return selectedServices;
    }

    public void setSelectedServices(ArrayList<RecordModel> selectedServices) {
        this.selectedServices = selectedServices;
    }

    public ArrayList<OrderDetailsModel> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<OrderDetailsModel> orderList) {
        this.orderList = orderList;
    }

    public ArrayList<OrderDetailsModel> getOrderProductList() {
        return orderProductList;
    }

    public void setOrderProductList(ArrayList<OrderDetailsModel> orderProductList) {
        this.orderProductList = orderProductList;
    }

    public ArrayList<OrderDetailsModel> getOrderServiceList() {
        return orderServiceList;
    }

    public void setOrderServiceList(ArrayList<OrderDetailsModel> orderServiceList) {
        this.orderServiceList = orderServiceList;
    }

    public float getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(float totalValue) {
        this.totalValue = totalValue;
    }

    public ArrayList<ModuleFieldModel> getOrderFieldList() {
        return orderFieldList;
    }

    public void setOrderFieldList(ArrayList<ModuleFieldModel> orderFieldList) {
        this.orderFieldList = orderFieldList;
    }

    public ArrayList<RecordModel> getOrderRecordList() {
        return orderRecordList;
    }

    public void setOrderRecordList(ArrayList<RecordModel> orderRecordList) {
        this.orderRecordList = orderRecordList;
    }

    public ArrayList<RecordModel> getOrderRecordProductList() {
        return orderRecordProductList;
    }

    public void setOrderRecordProductList(ArrayList<RecordModel> orderRecordProductList) {
        this.orderRecordProductList = orderRecordProductList;
    }

    public ArrayList<RecordModel> getOrderRecordServiceList() {
        return orderRecordServiceList;
    }

    public void setOrderRecordServiceList(ArrayList<RecordModel> orderRecordServiceList) {
        this.orderRecordServiceList = orderRecordServiceList;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<AttributeVariantModel> getAttributeVariantList() {
        return attributeVariantList;
    }

    public void setAttributeVariantList(ArrayList<AttributeVariantModel> attributeVariantList) {
        this.attributeVariantList = attributeVariantList;
    }

    public LinkedHashMap<String, AttributeVariantModel> getSelectedAttributes() {
        return selectedAttributes;
    }

    public void setSelectedAttributes(LinkedHashMap<String, AttributeVariantModel> selectedAttributes) {
        this.selectedAttributes = selectedAttributes;
    }

    public LinkedHashMap<String, LinkedHashMap<String, AttributeVariantModel>> getSelectedVariantAttributes() {
        return selectedVariantAttributes;
    }

    public void setSelectedVariantAttributes(LinkedHashMap<String, LinkedHashMap<String, AttributeVariantModel>> selectedVariantAttributes) {
        this.selectedVariantAttributes = selectedVariantAttributes;
    }

}
