/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.deals;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.sales.DealModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import java.util.Map;

/**
 *
 * @author badry
 */
public class InvoiceGeneratorOld {

    private final DealModel dealModel;

    public InvoiceGeneratorOld(DealModel dealModel) {
        this.dealModel = dealModel;
    }

    public String generateHtmlInvoice() {
//        String template = ""
//                + "<html>"
//                + "<head>"
//                + "    <meta charset=\"utf-8\"/>"
//                + "    <title>A simple, clean, and responsive HTML invoice template</title>"
//                + setHtmlStyle()
//                + "</head>"
//                + "<body>"
//                + "    <div class=\"invoice-box\">"
//                + "        <table cellpadding=\"0\" cellspacing=\"0\">"
//                + "            <tr class=\"top\">"
//                + "                <td colspan=\"4\">"
//                + "                    <table>"
//                + "                        <tr>"
//                + "                            <td class=\"title\">"
//                + "                                <img src=\"" + GeneralUtils.getCompany().getCompanyLogo() + "\" style=\"margin: -30px;width: 60%;margin-left: 0px;\"/>"
//                + "                            </td>"
//                + "                            "
//                + "                            <td>"
//                + generateInvoiceDetails()
//                + "                            </td>"
//                + "                        </tr>"
//                + "                    </table>"
//                + "                </td>"
//                + "            </tr>"
//                + "            "
//                + "            <tr class=\"information\">"
//                + "                <td colspan=\"4\">"
//                + "                    <table>"
//                + "                        <tr>"
//                + "                            <td>"
//                + generateMyCompanyInfo()
//                + "                            </td>"
//                + "                            "
//                + "                            <td>"
//                + generateRecepientDetails()
//                + "                            </td>"
//                + "                        </tr>"
//                + "                    </table>"
//                + "                </td>"
//                + "            </tr>"
//                + "            <tr class=\"heading\">"
//                + "                <td>"
//                + "                    Item"
//                + "                </td>"
//                + "                <td>"
//                + "                    Quantity"
//                + "                </td>"
//                + "                <td>"
//                + "                    Unit Price"
//                + "                </td>"
//                + "                <td>"
//                + "                    Total Price"
//                + "                </td>"
//                + "            </tr>"
//                + "            "
//                + generateItemizedBill()
//                + generateTotal()
//                + "        </table>"
//                + "    </div>"
//                + "</body>"
//                + "</html>";
        String template ="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
"<html>\n" +
"   <head>\n" +
"  <meta charset=\"utf-8\" />\n" +
"  <title>A simple, clean, and responsive HTML invoice template</title>\n" +
"  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" />\n" +
"\n" +
"  <style>\n" +
"    .invoice-box {\n" +
"      max-width: 800px;\n" +
"      margin: auto;\n" +
"      padding: 30px;\n" +
"      padding-top: 0;\n" +
"      border: 0px solid #eee;\n" +
"      box-shadow: 0 0 10px rgba(0, 0, 0, .15);\n" +
"      font-size: 16px;\n" +
"      line-height: 24px;\n" +
"      font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;\n" +
"      color: #555555 !important;\n" +
"    }\n" +
"    \n" +
"    .invoice-header {\n" +
"      background-color: #fdffb7;\n" +
"      margin-right: -30px;\n" +
"      margin-left: -30px;\n" +
"    }\n" +
"    \n" +
"    .invoice-box .row:nth-child(1) {\n" +
"      background-color: #fdffb7;\n" +
"      margin-right: -30px;\n" +
"      margin-left: -30px;\n" +
"    }\n" +
"    \n" +
"    .logo {\n" +
"      float: right;\n" +
"	  padding: 6px;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table {\n" +
"      width: 100%;\n" +
"      line-height: inherit;\n" +
"      text-align: left;\n" +
"      color: #6e6e6e;\n" +
"      font-size: 14px;\n" +
"    }\n" +
"    \n" +
"    .invoice-details {\n" +
"      width: 30%;\n" +
"    }\n" +
"    \n" +
"    .details-list {\n" +
"      font-size: 121px;\n" +
"    }\n" +
"    \n" +
"    .heading {\n" +
"      border-top: 2px solid #ff5858;\n" +
"      border-bottom: 2px solid #ff5858;\n" +
"      background-color: #fff;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table td {\n" +
"      padding: 5px;\n" +
"      vertical-align: top;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr td:nth-child(2) {\n" +
"      text-align: center;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr td:nth-child(3) {\n" +
"      text-align: center;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr td:nth-child(4) {\n" +
"      text-align: center;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr.top table td {\n" +
"      padding-bottom: 20px;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr.top table td.title {\n" +
"      font-size: 45px;\n" +
"      line-height: 45px;\n" +
"      color: #333;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr.information table td {\n" +
"      padding-bottom: 40px;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr.heading td {\n" +
"      border-top: 2px solid #ff5858;\n" +
"      border-bottom: 2px solid #ff5858;\n" +
"      font-weight: bold;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr.details td {\n" +
"      padding-bottom: 20px;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr.item td {\n" +
"      border-bottom: 1px solid #eee;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr.item.last td {\n" +
"      border-bottom: none;\n" +
"    }\n" +
"    \n" +
"    .invoice-box table tr.total td:nth-child(4) {\n" +
"      border-top: 2px solid #eee;\n" +
"      font-weight: bold;\n" +
"    }\n" +
"    \n" +
"    @media only screen and (max-width: 600px) {\n" +
"      .invoice-box table tr.top table td {\n" +
"        width: 100%;\n" +
"        display: block;\n" +
"        text-align: center;\n" +
"      }\n" +
"      .invoice-box table tr.information table td {\n" +
"        width: 100%;\n" +
"        display: block;\n" +
"        text-align: center;\n" +
"      }\n" +
"    }\n" +
"    \n" +
"    .rtl {\n" +
"      direction: rtl;\n" +
"      font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;\n" +
"    }\n" +
"    \n" +
"    .rtl table {\n" +
"      text-align: right;\n" +
"    }\n" +
"    \n" +
"    .rtl table tr td:nth-child(2) {\n" +
"      text-align: left;\n" +
"    }\n" +
"	\n" +
"	.subtotal table tbody tr td:nth-child(1){\n" +
"	text-align:right !important;\n" +
"	}\n" +
"	\n" +
"	.subtotal{\n" +
"	margin-right: 2em;\n" +
"	}\n" +
"\n" +
"  </style>\n" +
"</head>\n" +
"\n" +
"<body>\n" +
"  <div class=\"invoice-box\">\n" +
"    <div class=\"row invoice-header\">\n" +
"      <div class=\"col-xs-8\">\n" +
"        <h1><strong>INVOICE</strong></h1>\n" +
"      </div>\n" +
"      <div class=\"media-left logo\">\n" +
"        <img class=\"media-object logo\" src=\"https://s3.eu-central-1.amazonaws.com/123sys-bucket/Company/beezer_logo_new_small_black.png\" />\n" +
"      </div>\n" +
"    </div>\n" +
"\n" +
"    <div class=\"row\">\n" +
"      <div class=\"col-xs-6\">\n" +
"        <h4><strong>CUSTOMER</strong></h4>\n" +
"        <ul class=\"media-body list-unstyled details-list\">\n" +
"          <li><strong>Name: </strong> Ola Shawky</li>\n" +
"          <li><strong>Mobile: </strong>0122589635</li>\n" +
"          <li><strong>Email: </strong>ola@gmail.com</li>\n" +
"          <li><strong>Address: </strong>Rehab City, Building 8, group 180</li>\n" +
"        </ul>\n" +
"      </div>\n" +
"\n" +
"\n" +
"      <div class=\"col-xs-6 pull-right invoice-details\">\n" +
"        <h4><strong>INVOICE DETAILS</strong></h4>\n" +
"        <ul class=\"media-body list-unstyled details-list\">\n" +
"          <li><strong>Number: </strong>1256398</li>\n" +
"          <li><strong>Date: </strong>2018-01-13</li>\n" +
"          <li><strong>Payment: </strong>CASH</li>\n" +
"        </ul>\n" +
"      </div>\n" +
"    </div>\n" +
"\n" +
"    <div class=\"row\">\n" +
"      <table cellpadding=\"0\" cellspacing=\"0\">\n" +
"        <tr class=\"heading\">\n" +
"          <td>Item</td>\n" +
"          <td>Quantity</td>\n" +
"          <td>Unit Price</td>\n" +
"          <td>Total Price</td>\n" +
"        </tr>\n" +
"        <tr class=\"item\">\n" +
"          <td>Horse Statue</td>\n" +
"          <td>1.0</td>\n" +
"          <td>1000.0</td>\n" +
"          <td>1000.0</td>\n" +
"        </tr>\n" +
"        <tr class=\"item\">\n" +
"          <td>Wooden Chair Model 10045</td>\n" +
"          <td>1.0</td>\n" +
"          <td>1250.0</td>\n" +
"          <td>1250.0</td>\n" +
"        </tr>\n" +
"      </table>\n" +
"    </div>\n" +
"	\n" +
"	<div class=\"row\">\n" +
"	<div class=\"col-xs-6 pull-right invoice-details subtotal\">\n" +
"        <table>\n" +
"		<tbody>\n" +
"		<tr>\n" +
"		<td><strong>Subtotal: </strong></td>\n" +
"		<td>3260</td>\n" +
"		</tr>\n" +
"		<tr>\n" +
"		<td><strong>VAT: </strong></td>\n" +
"		<td>14%</td>\n" +
"		</tr>\n" +
"		<tr>\n" +
"		<td><strong>Tax: </strong></td>\n" +
"		<td>0%</td>\n" +
"		</tr>\n" +
"		<tr>\n" +
"		<td><strong>Shipment: </strong></td>\n" +
"		<td>60</td>\n" +
"		</tr>\n" +
"		<tr>\n" +
"		<td><strong>Total: </strong></td>\n" +
"		<td>3856</td>\n" +
"		</tr>\n" +
"		</tbody>\n" +
"		</table>\n" +
"      </div>\n" +
"	</div>\n" +
"	\n" +
"  </div>\n" +
"</body>\n" +
"</html>";
        return template;
    }

    private String setHtmlStyle() {
        return "    <style>"
                + "    .invoice-box{"
                + "        max-width:800px;"
                + "        margin:auto;"
                + "        padding:30px;"
                + "        border:1px solid #eee;"
                + "        box-shadow:0 0 10px rgba(0, 0, 0, .15);"
                + "        font-size:16px;"
                + "        line-height:24px;"
                + "        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;"
                + "        color:#555;"
                + "    }"
                + ".invoice-box img{"
                + "margin:-30px; "
                + "width:60%;"
                + "margin-left:0px;"
                + "}"
                + "    .invoice-box table{"
                + "        width:100%;"
                + "        line-height:inherit;"
                + "        text-align:left;"
                + "    }"
                + "    .invoice-box table td{"
                + "        padding:5px;"
                + "        vertical-align:top;"
                + "    }"
                + "    .invoice-box table tr td:nth-child(2){"
                + "        text-align:right;"
                + "    }"
                + "    .invoice-box table tr.top table td{"
                + "        padding-bottom:20px;"
                + "    }"
                + "    .invoice-box table tr.top table td.title{"
                + "        font-size:45px;"
                + "        line-height:45px;"
                + "        color:#333;"
                + "max-width:130px;"
                + "    }"
                + ".invoice-box table tr td.title img { " +
"    max-width: 130px; " +
"}"
                + "    .invoice-box table tr.information table td{"
                + "        padding-bottom:40px;"
                + "    }"
                + "    .invoice-box table tr.heading td{"
                + "        background:#eee;"
                + "        border-bottom:1px solid #ddd;"
                + "        font-weight:bold;"
                + "    }"
                + "    .invoice-box table tr.details td{"
                + "        padding-bottom:20px;"
                + "    }"
                + "    .invoice-box table tr.item td{"
                + "        border-bottom:1px solid #eee;"
                + "    }"
                + "    .invoice-box table tr.item.last td{"
                + "        border-bottom:none;"
                + "    }"
                + "    .invoice-box table tr.total td:nth-child(4){"
                + "        border-top:2px solid #eee;"
                + "        font-weight:bold;"
                + "    }"
                + "    @media only screen and (max-width: 600px) {"
                + "        .invoice-box table tr.top table td{"
                + "            width:100%;"
                + "            display:block;"
                + "            text-align:center;"
                + "        }"
                + "        .invoice-box table tr.information table td{"
                + "            width:100%;"
                + "            display:block;"
                + "            text-align:center;"
                + "        }"
                + "    }"
                + "    .rtl {"
                + "        direction: rtl;"
                + "        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;"
                + "    }"
                + "    "
                + "    .rtl table {"
                + "        text-align: right;"
                + "    }"
                + "    "
                + "    .rtl table tr td:nth-child(2) {"
                + "        text-align: left;"
                + "    }"
                + "    </style>";
    }

    private String generateInvoiceDetails() {
        String invoiceDetails = "";
        for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
            if (entry.getValue().getFieldName().equals("invoice_number")) {
                invoiceDetails = invoiceDetails + "Invoice #: " + entry.getValue().getCurrentValue() + "<br/>";
                break;
            }
        }

        for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
            if (entry.getValue().getFieldName().equals("added_date")) {
                invoiceDetails = invoiceDetails + "Created On: " + entry.getValue().getCurrentValue() + "<br/>";
                break;
            }
        }

        for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
            if (entry.getValue().getFieldName().equals("invoice_due_date")) {
                invoiceDetails = invoiceDetails + "Due On: " + entry.getValue().getCurrentValue() + "<br/>";
                break;
            }
        }

        return invoiceDetails;
    }

    private String generateMyCompanyInfo() {
        String myCompanyInfo = GeneralUtils.getCompany().getName() + "<br/>";
        myCompanyInfo = myCompanyInfo + GeneralUtils.getCompany().getAddress() + "<br/>";
        myCompanyInfo = myCompanyInfo + GeneralUtils.getCompany().getCity()
                + ", " + GeneralUtils.getCompany().getCountry() + "<br/>";
        return myCompanyInfo;
    }

    private String generateRecepientDetails() {
        String recepientDetails = "";
        boolean contactFound = false;
        for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
            if (entry.getValue().getFieldName().equals("contact_name")) {
                if (entry.getValue().getCurrentValue() != null) {
                    contactFound = true;
                    recepientDetails = recepientDetails + "Recepient: " + entry.getValue().getCurrentValue() + "<br/>";
                    break;
                }
            }
        }

        if (contactFound) {
            for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
                if (entry.getValue().getFieldName().equals("account_name")) {
                    recepientDetails = recepientDetails + "Company: " + entry.getValue().getCurrentValue() + "<br/>";
                    break;
                }
            }
        }
        else {
            for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
                if (entry.getValue().getFieldName().equals("cust_firstname")) {
                    recepientDetails = recepientDetails + "Receiver: " + entry.getValue().getCurrentValue() + "";
                    break;
                }
            }

            for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
                if (entry.getValue().getFieldName().equals("cust_lastname")) {
                    recepientDetails = recepientDetails + " " + entry.getValue().getCurrentValue() + "";
                    break;
                }
            }
        }

        return recepientDetails;
    }

    private String generateItemizedBill() {
        String itemizedBill = "";
        if (dealModel.getOrderList()!= null) {
            for (RecordModel rm : dealModel.getOrderList()) {
                switch (GeneralUtils.getFieldFromRecord(rm, "goods_type").getCurrentValue()) {
                        case "PRODUCT": {
                            float itemPrice = 0;
                            for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                                if (entry.getValue().getFieldName().equals("goods_selling_price")) {
                                    if (entry.getValue().getCurrentValue() != null) {
                                        itemPrice = Float.valueOf(entry.getValue().getCurrentValue());
                                        break;
                                    }
                                }
                            }
                            for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                                if (entry.getValue().getFieldName().equals("product_name")) {
                                    if (entry.getValue().getCurrentValue() != null) {
                                        itemizedBill = itemizedBill + "<tr class=\"item\">"
                                                + "                <td>"
                                                + entry.getValue().getCurrentValue()
                                                + "                </td>"
                                                + "                <td>"
                                                + GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()
                                                + "                </td>"
                                                + "                <td>"
                                                + itemPrice
                                                + "                </td>"
                                                + "                <td>"
                                                + Integer.parseInt(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * itemPrice
                                                + "                </td>"
                                                + "            </tr>";
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                        case "SERVICE": {
                            float itemPrice = 0;
                            for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                                if (entry.getValue().getFieldName().equals("good_selling_price")) {
                                    if (entry.getValue().getCurrentValue() != null) {
                                        itemPrice = Float.valueOf(entry.getValue().getCurrentValue());
                                        break;
                                    }
                                }
                            }
                            for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                                if (entry.getValue().getFieldName().equals("name")) {
                                    if (entry.getValue().getCurrentValue() != null) {
                                        itemizedBill = itemizedBill + "<tr class=\"item\">"
                                                + "                <td>"
                                                + entry.getValue().getCurrentValue()
                                                + "                </td>"
                                                + "                <td>"
                                                + GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()
                                                + "                </td>"
                                                + "                <td>"
                                                + itemPrice
                                                + "                </td>"
                                                + "                <td>"
                                                + Integer.parseInt(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * itemPrice
                                                + "                </td>"
                                                + "            </tr>";
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
            }
        }

        return itemizedBill;
    }

    
//    private String generateItemizedBill() {
//        String itemizedBill = "";
//        if (dealModel.getOrderDetailsList() != null) {
//            for (OrderDetailsModel odm : dealModel.getOrderDetailsList()) {
//                if (odm.getSellable() != null) {
//                    switch (odm.getGoodsType()) {
//                        case "PRODUCT": {
//                            float itemPrice = 0;
//                            for (Map.Entry<String, ModuleFieldModel> entry : odm.getSellable().getFieldValueMap().entrySet()) {
//                                if (entry.getValue().getFieldName().equals("selling_price")) {
//                                    if (entry.getValue().getCurrentValue() != null) {
//                                        itemPrice = Float.valueOf(entry.getValue().getCurrentValue());
//                                        break;
//                                    }
//                                }
//                            }
//                            for (Map.Entry<String, ModuleFieldModel> entry : odm.getSellable().getFieldValueMap().entrySet()) {
//                                if (entry.getValue().getFieldName().equals("product_name")) {
//                                    if (entry.getValue().getCurrentValue() != null) {
//                                        itemizedBill = itemizedBill + "<tr class=\"item\">"
//                                                + "                <td>"
//                                                + entry.getValue().getCurrentValue()
//                                                + "                </td>"
//                                                + "                <td>"
//                                                + odm.getQuantity()
//                                                + "                </td>"
//                                                + "                <td>"
//                                                + itemPrice
//                                                + "                </td>"
//                                                + "                <td>"
//                                                + odm.getQuantity() * itemPrice
//                                                + "                </td>"
//                                                + "            </tr>";
//                                    }
//                                    break;
//                                }
//                            }
//                            break;
//                        }
//                        case "SERVICE": {
//                            float itemPrice = 0;
//                            for (Map.Entry<String, ModuleFieldModel> entry : odm.getSellable().getFieldValueMap().entrySet()) {
//                                if (entry.getValue().getFieldName().equals("selling_price")) {
//                                    if (entry.getValue().getCurrentValue() != null) {
//                                        itemPrice = Float.valueOf(entry.getValue().getCurrentValue());
//                                        break;
//                                    }
//                                }
//                            }
//                            for (Map.Entry<String, ModuleFieldModel> entry : odm.getSellable().getFieldValueMap().entrySet()) {
//                                if (entry.getValue().getFieldName().equals("name")) {
//                                    if (entry.getValue().getCurrentValue() != null) {
//                                        itemizedBill = itemizedBill + "<tr class=\"item\">"
//                                                + "                <td>"
//                                                + entry.getValue().getCurrentValue()
//                                                + "                </td>"
//                                                + "                <td>"
//                                                + odm.getQuantity()
//                                                + "                </td>"
//                                                + "                <td>"
//                                                + itemPrice
//                                                + "                </td>"
//                                                + "                <td>"
//                                                + odm.getQuantity() * itemPrice
//                                                + "                </td>"
//                                                + "            </tr>";
//                                    }
//                                    break;
//                                }
//                            }
//                            break;
//                        }
//                    }
//
//                }
//            }
//        }
//
//        return itemizedBill;
//    }

    private String generateTotal() {
        String totalAmount = "";
        for (Map.Entry<String, ModuleFieldModel> entry : dealModel.getDealDetails().getFieldValueMap().entrySet()) {
            if (entry.getValue().getFieldName().equals("final_price")) {
                totalAmount = "<tr class=\"total\">"
                        + "                <td></td>"
                        + "                <td></td>"
                        + "                <td></td>"
                        + "                <td>"
                        + "                   Total: " + entry.getValue().getCurrentValue()
                        + "                </td>"
                        + "            </tr>";
                break;
            }
        }
        return totalAmount;
    }
}
