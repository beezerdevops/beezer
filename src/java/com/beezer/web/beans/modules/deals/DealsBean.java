/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.deals;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "dealsBean")
@ViewScoped
public class DealsBean extends BeanFramework {

    private String custId;

    public DealsBean() {
    }

    @PostConstruct
    public void init() {
        super.setModuleId("7");
        super.loadFilters("7");
        this.loadRecordsList();
    }

    private void loadRecordsList() {
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("deal_status", "=", "NEW"));
        fm.setFilter("OR", new FilterType("deal_status", "=", "ADJ"));
        fm.setFilter("OR", new FilterType("deal_status", "=", "REPLACE"));
        super.setLazyRecordModel(new LazyRecordModel(fm.getClause(), false, LazyRecordModel.DEAL));
    }

    @Override
    public void search(String api) {
        if (this.getFTSKeyword() != null && !this.getFTSKeyword().isEmpty()) {
            super.setLazyRecordModel(new LazyRecordModel(this.getFTSKeyword(), true, LazyRecordModel.DEAL));
            this.setFTSKeyword(null);
        }
        else {
            FilterManager fm = new FilterManager();
            this.populateFormData();
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                if (mfm.getCurrentValue() != null && !mfm.getCurrentValue().isEmpty()) {
                    if (super.relationRecordsCache.get(mfm.getFieldName()) != null) {
                        String tableName = super.relationRecordsCache.get(mfm.getFieldName()).getRelationManager().getMasterTable();
                        fm.setFilter("AND", new FilterType(tableName + "." + mfm.getFieldName(), "IN", mfm.getCurrentValue()));
                    }
                    else {
                        switch (mfm.getFieldType()) {
                            case "LOOKUP":
                                fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", mfm.getCurrentValue()));
                                break;
                            case "BOOLEAN":
                                if (mfm.getCurrentValue() == null || mfm.getCurrentValue().isEmpty() || mfm.getCurrentValue().equals("0")) {
                                }
                                else if (mfm.getCurrentValue().equals("1")) {
                                    fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", true));
                                }
                                else if (mfm.getCurrentValue().equals("2")) {
                                    fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", false));
                                }
                                break;
                            default:
                                fm.setFilter("AND", new FilterType(mfm.getFieldName(), "LIKE", mfm.getCurrentValue()));
                                break;
                        }
                    }
                }
            }
            super.loadTableSettings("7");
            super.setLazyRecordModel(new LazyRecordModel(fm.getClause(), false, LazyRecordModel.DEAL));
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    @Override
    public void onRowSelect(SelectEvent event) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", super.getSelectedRecords().get(0));
            FacesContext.getCurrentInstance().getExternalContext().redirect("dealDetails.xhtml?recordId="
                    + super.getSelectedRecords().get(0).getFieldValueMap().get("deal_id").getCurrentValue());
        }
        catch (Exception ex) {
            Logger.getLogger(DealsBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void delete() {
        if (super.getSelectedRecords() != null) {
            RequestModel requestModel = new RequestModel();
            requestModel.setBulkRecords(new ArrayList<>(this.getSelectedRecords()));
            requestModel.setRequestActionType("2");
            requestModel.setRequestingModule("7");
            RequestHandler handler = new RequestHandler();
            ResponseModel responseModel = handler.executeRequest(requestModel);
            if (responseModel.getErrorCode() == 1000) {
                FilterManager fm = new FilterManager();
                boolean first = true;
                for (RecordModel rm : this.getSelectedRecords()) {
                    if (first) {
                        fm.setFilter("AND", new FilterType("deal_details.deal_id", "=", rm.getFieldValueMap().get("deal_id").getCurrentValue()));
                        first = false;
                    }
                    else {
                        fm.setFilter("OR", new FilterType("deal_details.deal_id", "=", rm.getFieldValueMap().get("deal_id").getCurrentValue()));
                    }
                }

                requestModel.setClause(fm.getClause());
                requestModel.setBulkRecords(null);
                requestModel.setRecordModel(null);
                requestModel.setRequestingModule("5");
                ResponseModel orderResponse = handler.executeRequest(requestModel);
            }
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
