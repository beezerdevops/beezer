/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.customers;

import com.beezer.web.beans.modules.deals.DealsBean;
import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "customerDetailBean")
@ViewScoped
public class CustomerDetailBean extends BeanFramework {

    private String requestedId;
    
    @ManagedProperty(value="#{dealsBean}")
    private DealsBean dealsBean;

    public CustomerDetailBean() {
    }

    @PostConstruct
    public void init() {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        this.setSelectedRecord();
        super.loadFieldSet("6", null);
        this.loadRecord();
        super.fillRecordDetails();
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    private void setSelectedRecord() {
        requestedId = super.getRequestParams().get("customer");
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        FilterManager fm = new FilterManager();
        FilterType fStatus = new FilterType("cust_id", Integer.valueOf(requestedId));
        fm.setFilter("AND", fStatus);
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GetCustomersWS");
            if (response.getErrorCode() == 1000) {
                this.recordModel = response.getRecordList().get(0);
                dealsBean.setCustId(this.recordModel.getFieldValueMap().get("cust_id").getCurrentValue());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(CustomerDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onTabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        switch (tabId) {
            case "detailsTab":
//                this.loadProduct();
//                this.fillFieldDetails();
                break;
            case "dealsTab":
//                dealsBean.loadRelatedDeal();
                break;
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", super.getRecordModel());
            FacesContext.getCurrentInstance().getExternalContext().redirect("addCustomer.xhtml?customer="
                    + super.getRecordModel().getFieldValueMap().get("cust_id").getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT);
        }
        catch (Exception ex) {
            Logger.getLogger(CustomerDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "DeleteCustomerWS");
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Customer deleted Susscefully."));
                FacesContext.getCurrentInstance().getExternalContext().redirect("customers.xhtml");
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete Customer."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(CustomerDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public DealsBean getDealsBean() {
        return dealsBean;
    }

    public void setDealsBean(DealsBean dealsBean) {
        this.dealsBean = dealsBean;
    }

    public String getRequestedId() {
        return requestedId;
    }

    public void setRequestedId(String requestedId) {
        this.requestedId = requestedId;
    }

}
