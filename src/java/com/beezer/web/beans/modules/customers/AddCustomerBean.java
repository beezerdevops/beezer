/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.customers;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "addCustomerBean")
@ViewScoped
public class AddCustomerBean extends BeanFramework {

    public AddCustomerBean() {
    }

    @PostConstruct
    public void init() {
        super.loadForms("6",0);
        super.setMode();
        super.setMasterPageName("customers.xhtml");
    }

    @Override
    public void loadRecord() {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        RequestModel request = new RequestModel();
        FilterManager fm = new FilterManager();
        FilterType fStatus = new FilterType("cust_id", Integer.valueOf(super.getRequestParams().get("customer")));
        fm.setFilter("AND", fStatus);
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GetCustomersWS");
            if (response.getErrorCode() == 1000) {
                super.recordModel = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddCustomerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    @Override
    public void save() {
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                this.edit();
                break;
            case Defines.REQUEST_MODE_ADD:
                super.populateDataFromForm();
                RequestModel request = new RequestModel();
                request.setRecordModel(super.recordModel);
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel response;
                try {
                    response = requestHandler.executeRequest(request, "AddCustomerWS");
                    this.updateMessage(response.getErrorCode(), response.getErrorMessage());
                }
                catch (Exception ex) {
                    Logger.getLogger(AddCustomerBean.class.getName()).log(Level.SEVERE, null, ex);
                }   break;
        }
    }

    @Override
    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("customers.xhtml");
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddCustomerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRecordModel(super.recordModel);
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "UpdateCustomerWS");
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "updated Added Susscefully."));
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to update customer."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddCustomerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "DeleteCustomerWS");
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Customer deleted Susscefully."));
                FacesContext.getCurrentInstance().getExternalContext().redirect("customers.xhtml");
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete Customer."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddCustomerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
}
