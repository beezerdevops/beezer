/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.customers;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "customersBean")
@ViewScoped
public class CustomersBean extends BeanFramework {

    private String custId;
    

    public CustomersBean() {
    }

    @PostConstruct
    public void init() {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        super.setModuleId("6");
        super.loadFilters("6");
        this.loadRecordsList();
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public void loadRecordsList() {
        super.setLazyRecordModel(new LazyRecordModel("GetCustomersWS", null, false,LazyRecordModel.RECORD,"6"));
    }

    public void loadRelatedCustomer() {
        try {
            RequestModel request = new RequestModel();
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("cust_id", custId));
            request.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "GetCustomersWS");
            if (response.getErrorCode() == 1000) {
                super.setRecordsList(response.getRecordList());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(CustomersBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<RecordModel> loadRelatedCustomers(String custId) {
        try {
            RequestModel request = new RequestModel();
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("cust_id", custId));
            request.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "GetCustomersWS");
            if (response.getErrorCode() == 1000) {
                return response.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(CustomersBean.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    @Override
    public void search(String api) {
        super.search("GetCustomersWS");
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        request.setBulkRecords(new ArrayList<>(super.getSelectedRecords()));
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "DeleteCustomerWS");
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Customers deleted Susscefully."));
                FacesContext.getCurrentInstance().getExternalContext().redirect("customers.xhtml");
                super.getSelectedRecords().clear();
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete Customers."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(CustomersBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", super.getSelectedRecords().get(0));
            FacesContext.getCurrentInstance().getExternalContext().redirect("customerDetails.xhtml?customer="
                    + super.getSelectedRecords().get(0).getFieldValueMap().get("cust_id").getCurrentValue());
        }
        catch (Exception ex) {
            Logger.getLogger(CustomersBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
