/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.products;

import com.beezer.web.beans.base.RecordDetailManager;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.AttributeVariantModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "productDetailBean")
@ViewScoped
public class ProductDetailBean extends RecordDetailManager {

    private ArrayList<RecordModel> productVariantList;
    private ArrayList<AttributeVariantModel> attributeVariantList;
    private RecordModel selectedVariant;

    public ProductDetailBean() {
    }

    @PostConstruct
    public void init() {
        productVariantList = new ArrayList<>();
        attributeVariantList = new ArrayList<>();
        super.setModuleId("3");
        this.setSelectedRecord();
        super.loadFieldSet("3", null);
        this.loadRecord();
        this.loadVariantList();
        this.loadRelationalModules();
        super.fillRecordDetails();
    }

    @Override
    public void setSelectedRecord() {
        requestedId = super.getRequestParams().get("product");
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        FilterManager fm = new FilterManager();
        FilterType fStatus = new FilterType("products_base.product_id", requestedId);
        fm.setFilter("AND", fStatus);
        request.setClause(fm.getClause());
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.recordModel = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ProductDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onTabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        if (!tabId.equalsIgnoreCase("variantsTab")) {
            super.onTabChange(event);
        }
    }

    public void loadVariantList() {
        if (super.recordModel != null) {
            if (GeneralUtils.getFieldFromRecord(recordModel, "has_variant") != null
                    && GeneralUtils.getFieldFromRecord(recordModel, "has_variant").getCurrentValue().equals("true")) {
                super.loadLookup("attribute", recordModel.getFieldValueMap().get("attribute").getFieldValues());
                try {
                    RequestModel request = new RequestModel();
                    FilterManager fm = new FilterManager();
                    fm.setFilter("AND", new FilterType("products_base.parent_product", "=", recordModel.getFieldValueMap().get("product_id").getCurrentValue()));
                    request.setClause(fm.getClause());
                    request.setRequestingModule(super.getModuleId());
                    request.setRequestActionType("0");
                    request.setSkipContent(true);
                    RequestHandler requestHandler = new RequestHandler();
                    ResponseModel response = requestHandler.executeRequest(request, "GenericMasterAPI");
                    if (response.getErrorCode() == 1000) {
                        this.productVariantList = response.getRecordList();
                        for (RecordModel rm : productVariantList) {
                            LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(GeneralUtils.getFieldFromRecord(rm, "attr_map").getCurrentValue());
                            for (Map.Entry<String, String> entry : kvmap.entrySet()) {
                                String attrKeyName = null;
                                TreeMap<String, String> attMap = super.getLookupDictionary().get("attribute");
                                for (Map.Entry<String, String> entryAttName : attMap.entrySet()) {
                                    if (entryAttName.getValue().equals(entry.getKey())) {
                                        attrKeyName = entryAttName.getKey();
                                        break;
                                    }
                                }

                                ModuleFieldModel mfm = new ModuleFieldModel();
                                mfm.setFieldName(attrKeyName);
                                mfm.setCurrentValue(entry.getValue());
                                mfm.setFieldType("TEXT");
                                rm.getFieldValueMap().put(mfm.getFieldName(), mfm);
                            }
                        }

                        LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(GeneralUtils.getFieldFromRecord(recordModel, "attr_map").getCurrentValue());
                        for (Map.Entry<String, String> entry : kvmap.entrySet()) {
                            AttributeVariantModel avm = new AttributeVariantModel();
                            avm.setAttributeId(entry.getKey());
                            avm.setAttributeValues(new ArrayList<>(Arrays.asList(entry.getValue().split("\\s*,\\s*"))));
                            attributeVariantList.add(avm);
                            this.onAttributeSelect(avm.getAttributeId());
                        }
                    }
                }
                catch (Exception ex) {
                    Logger.getLogger(ProductDetailBean.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }

    public void onAttributeSelect(String selection) {
        String attrKeyName = null;
        TreeMap<String, String> attMap = super.getLookupDictionary().get("attribute");
        for (Map.Entry<String, String> entry : attMap.entrySet()) {
            if (entry.getValue().equals(selection)) {
                attrKeyName = entry.getKey();
                break;
            }
        }

        for (AttributeVariantModel avm : attributeVariantList) {
            if (avm.getAttributeId().equals(selection)) {
                avm.setAttributeName(attrKeyName);
                break;
            }
        }
    }

    public void onVariantSelect(SelectEvent event) {
        try {
            RecordModel selectedModel = (RecordModel) event.getObject();
            FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                    + selectedModel.getFieldValueMap().get("product_id").getCurrentValue() + "&" + "moduleId=" + super.getModuleId());
        }
        catch (IOException ex) {
            Logger.getLogger(ProductDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveAsProfilePic() {
        String link = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("imageFileName");
        super.getRecordModel().getFieldValueMap().get("img_path").setCurrentValue(link);
        RequestModel request = new RequestModel();
        request.setRecordModel(super.recordModel);
        request.setRequestingModule(super.getModuleId());
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record deleted Susscefully."));
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete record."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ProductDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", super.getRecordModel());
            FacesContext.getCurrentInstance().getExternalContext().redirect("addProduct.xhtml?product="
                    + super.getRecordModel().getFieldValueMap().get("product_id").getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT);
        }
        catch (Exception ex) {
            Logger.getLogger(ProductDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        super.getRecordModel().getFieldValueMap().get("product_status").setCurrentValue("DELETED");
        request.setRecordModel(super.getRecordModel());
        request.setRequestingModule(super.getModuleId());
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                if (super.getRecordModel().getFieldValueMap().get("has_variant").getCurrentValue().equals("true")) {
                    this.deleteProductVariants(super.getRecordModel().getFieldValueMap().get("product_id").getCurrentValue());
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "products deleted Susscefully."));
                FacesContext.getCurrentInstance().getExternalContext().redirect("products.xhtml");
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete products."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ProductDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void deleteProductVariants(String productId) {
        try {
            RequestModel requestVariants = new RequestModel();
            requestVariants.setRequestingModule(super.getModuleId());
            requestVariants.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("products_base.parent_product", "=", productId));
            requestVariants.setClause(fm.getClause());
            RequestHandler rvHandler = new RequestHandler();
            ResponseModel rvResponse = rvHandler.executeRequest(requestVariants, "GenericMasterAPI", true);
            if (rvResponse.getErrorCode() == 1000) {
                if (rvResponse.getRecordList() != null) {
                    for (RecordModel rm : rvResponse.getRecordList()) {
                        rm.getFieldValueMap().get("product_status").setCurrentValue("DELETED");
                    }
                }
                requestVariants = new RequestModel();
                requestVariants.setRequestingModule(super.getModuleId());
                requestVariants.setRequestActionType("3");
                requestVariants.setBulkRecords(rvResponse.getRecordList());
                rvHandler = new RequestHandler();
                rvResponse = rvHandler.executeRequest(requestVariants, "GenericMasterAPI", true);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ProductsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<RecordModel> getProductVariantList() {
        return productVariantList;
    }

    public void setProductVariantList(ArrayList<RecordModel> productVariantList) {
        this.productVariantList = productVariantList;
    }

    public ArrayList<AttributeVariantModel> getAttributeVariantList() {
        return attributeVariantList;
    }

    public void setAttributeVariantList(ArrayList<AttributeVariantModel> attributeVariantList) {
        this.attributeVariantList = attributeVariantList;
    }

    public RecordModel getSelectedVariant() {
        return selectedVariant;
    }

    public void setSelectedVariant(RecordModel selectedVariant) {
        this.selectedVariant = selectedVariant;
    }

}
