/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.products;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.AttributeVariantModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldBlockModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "addProductBean")
@ViewScoped
public class AddProductBean extends BeanFramework {

    private String requestingCategory;
    private ArrayList<AttributeVariantModel> attributeVariantList;
    private ArrayList<RecordModel> variantProductList;
    private ArrayList<RecordModel> originalVariantList;

    public AddProductBean() {
    }

    @PostConstruct
    public void init() {
        attributeVariantList = new ArrayList<>();
        variantProductList = new ArrayList<>();
        super.setModuleId("3");
        super.setMasterPageName("products.xhtml");
        super.loadFieldSet("3", null);
        super.buildRecordModel();
        this.loadForms("3",0);
        this.getRequestingCategory();
        super.setMode();
    }

    private void getRequestingCategory() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        requestingCategory = params.get("catalog");
        if (requestingCategory != null) {
//            this.selectionMap.put("category_id", new SelectionModel(requestingCategory));
            PrimeFaces.current().executeScript("PF('category_id').selectValue(" + "'" + requestingCategory + "'" + ")");
        }
    }

    public void dumdum() {
        int x = 0;
    }

    public void onAttributeSelect(String selection) {
        String attrKeyName = null;
        TreeMap<String, String> attMap = super.getLookupDictionary().get("attribute");
        for (Map.Entry<String, String> entry : attMap.entrySet()) {
            if (entry.getValue().equals(selection)) {
                attrKeyName = entry.getKey();
                break;
            }
        }

        for (AttributeVariantModel avm : attributeVariantList) {
            if (avm.getAttributeId().equals(selection)) {
                avm.setAttributeName(attrKeyName);
                break;
            }
        }
    }

    public void addNewAttribute() {
        AttributeVariantModel avm = new AttributeVariantModel();
        avm.setAttributeValues(new ArrayList<>());
        attributeVariantList.add(avm);
    }

    public void deleteAttribute(AttributeVariantModel selectedAVM) {
        ArrayList<String> atrrValues = selectedAVM.getAttributeValues();
        for (String attrValue : atrrValues) {
            selectedAVM.getAttributeValues().remove(attrValue);
            this.removeVariantAttribute(attrValue, selectedAVM.getAttributeId(), selectedAVM.getAttributeName());
        }
        attributeVariantList.remove(selectedAVM);
    }

    public void onAddNewAttributeValue(SelectEvent event) {
        String attrValue = (String) event.getObject();
        String attrKeyId = (String) event.getComponent().getAttributes().get("attributeId");
        String attrKeyName = null;
        TreeMap<String, String> attMap = super.getLookupDictionary().get("attribute");
        for (Map.Entry<String, String> entry : attMap.entrySet()) {
            if (entry.getValue().equals(attrKeyId)) {
                attrKeyName = entry.getKey();
                break;
            }
        }
        if (variantProductList.isEmpty()) {
            variantProductList.add(this.createVariantProduct(attrKeyId, attrValue, attrKeyName));
        }
        else {
            ArrayList<RecordModel> newVariants = new ArrayList<>();
            for (RecordModel rm : variantProductList) {
                boolean merge = true;
                boolean duplicate = false;
                for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                    if (entry.getValue().getFieldName().equals(attrKeyName)) {
                        RecordModel newVariant = this.createVariantProductByRecord(attrKeyId, attrValue, attrKeyName, rm, newVariants);
                        if (newVariant != null) {
                            newVariants.add(newVariant);
                        }
                        merge = false;
                        break;
                    }
                }

                if (merge) {
                    this.mergeVariantAttribute(attrKeyId, attrValue, attrKeyName, rm);
                }
            }
            variantProductList.addAll(newVariants);
        }
    }

    public RecordModel createVariantProduct(String attrKeyId, String attrValue, String attrKeyName) {
        RecordModel vProduct = new RecordModel();
        LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
        for (ModuleFieldModel mfm : super.getFieldsList()) {
            ModuleFieldModel mfmv = new ModuleFieldModel(mfm);
            if (mfm.getFieldName().equals("attribute")) {
                mfmv.setCurrentValue(attrKeyId);

                ModuleFieldModel mfmvL = new ModuleFieldModel();
                mfmvL.setFieldName(attrKeyName);
                mfmvL.setCurrentValue(attrValue);
                fieldValueMap.put(attrKeyName, mfmvL);
            }
            if (mfm.getFieldName().equals("attr_map")) {
                if (mfm.getCurrentValue() != null) {
                    try {
                        LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(mfm.getCurrentValue());
                        if (kvmap.get(attrKeyId) != null) {
                            return null;
                        }
                        else {
                            kvmap.put(attrKeyId, attrValue);
                            mfmv.setCurrentValue(GeneralUtils.serializeRequest(kvmap));
                        }
                    }
                    catch (Exception ex) {
                        Logger.getLogger(AddProductBean.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else {
                    LinkedHashMap<String, String> kvmap = new LinkedHashMap<>();
                    kvmap.put(attrKeyId, attrValue);
                    mfmv.setCurrentValue(GeneralUtils.serializeRequest(kvmap));
                }
            }

            if (mfm.getFieldName().equals("is_variant")) {
                mfmv.setCurrentValue("true");
            }
            fieldValueMap.put(mfm.getFieldName(), mfmv);
        }
        vProduct.setFieldValueMap(fieldValueMap);
//        variantProductList.add(vProduct);
        return vProduct;
    }

    public RecordModel createVariantProductByRecord(String attrKeyId, String attrValue, String attrKeyName, RecordModel recordModel, ArrayList<RecordModel> variantList) {
        RecordModel vProduct = new RecordModel();
        LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
        for (Map.Entry<String, ModuleFieldModel> entry : recordModel.getFieldValueMap().entrySet()) {
            ModuleFieldModel mfm = entry.getValue();
            ModuleFieldModel mfmv = new ModuleFieldModel(mfm);
            if (mfm.getFieldName().equals("attribute")) {
                mfmv.setCurrentValue(attrKeyId);
            }
            if (mfm.getFieldName().equals(attrKeyName)) {
                mfmv.setFieldName(attrKeyName);
                mfmv.setCurrentValue(attrValue);
            }
            if (mfm.getFieldName().equals("attr_map")) {
                if (mfm.getCurrentValue() != null) {
                    try {
                        LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(mfm.getCurrentValue());
                        if (kvmap.get(attrKeyId) != null) {
                            if (kvmap.get(attrKeyId).equals(attrValue)) {
                                return null;
                            }
                            else {
                                kvmap.put(attrKeyId, attrValue);
                                String serializedKVMap = GeneralUtils.serializeRequest(kvmap);
                                boolean duplicateFound = false;
                                if (variantList != null && !variantList.isEmpty()) {
                                    for (RecordModel rm : variantList) {
                                        if (rm.getFieldValueMap().get("attr_map").getCurrentValue().equals(serializedKVMap)) {
                                            duplicateFound = true;
                                            break;
                                        }
                                    }
                                }
                                if (!duplicateFound) {
                                    mfmv.setCurrentValue(GeneralUtils.serializeRequest(kvmap));
                                }
                                else {
                                    return null;
                                }
                            }
                        }
                        else {
                            kvmap.put(attrKeyId, attrValue);
                            mfmv.setCurrentValue(GeneralUtils.serializeRequest(kvmap));
                        }
                    }
                    catch (Exception ex) {
                        Logger.getLogger(AddProductBean.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else {
                    LinkedHashMap<String, String> kvmap = new LinkedHashMap<>();
                    kvmap.put(attrKeyId, attrValue);
                    mfmv.setCurrentValue(GeneralUtils.serializeRequest(kvmap));
                }
            }
//            if (mfm.getFieldName().equals("attribute_value")) {
//                mfmv.setCurrentValue(attrValue);
//            }
            if (mfm.getFieldName().equals("is_variant")) {
                mfmv.setCurrentValue("true");
            }
            fieldValueMap.put(mfmv.getFieldName(), mfmv);
        }
        vProduct.setFieldValueMap(fieldValueMap);
//        variantProductList.add(vProduct);
        return vProduct;
    }

    public void mergeVariantAttribute(String attrKeyId, String attrValue, String attrKeyName, RecordModel recordModel) {
        ModuleFieldModel mfmvL = new ModuleFieldModel();
        mfmvL.setFieldName(attrKeyName);
        mfmvL.setCurrentValue(attrValue);
        mfmvL.setFieldType("TEXT");
        recordModel.getFieldValueMap().put(attrKeyName, mfmvL);

        try {
            LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(GeneralUtils.getFieldFromRecord(recordModel, "attr_map").getCurrentValue());
            kvmap.put(attrKeyId, attrValue);
            recordModel = GeneralUtils.setFieldValueInRecord(recordModel, "attr_map", GeneralUtils.serializeRequest(kvmap));
        }
        catch (Exception ex) {
            Logger.getLogger(AddProductBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onDeleteAttributeValue(UnselectEvent event) {
        String attrValue = (String) event.getObject();
        String attrKeyId = (String) event.getComponent().getAttributes().get("attributeId");
        String attrKeyName = null;
        TreeMap<String, String> attMap = super.getLookupDictionary().get("attribute");
        for (Map.Entry<String, String> entry : attMap.entrySet()) {
            if (entry.getValue().equals(attrKeyId)) {
                attrKeyName = entry.getKey();
                break;
            }
        }

        this.removeVariantAttribute(attrValue, attrKeyId, attrKeyName);

    }

    public void removeVariantAttribute(String attrValue, String attrKeyId, String attrKeyName) {
        ArrayList<RecordModel> updatedVariants = new ArrayList<>();
        for (RecordModel rm : variantProductList) {
            ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, attrKeyName);
            if (!mfm.getCurrentValue().equals(attrValue)) {
                updatedVariants.add(rm);
            }
        }

        boolean allAttributesEmpty = true;
        for (AttributeVariantModel avm : attributeVariantList) {
            if (avm.getAttributeValues() != null && !avm.getAttributeValues().isEmpty()) {
                allAttributesEmpty = false;
                break;
            }
        }

        if (allAttributesEmpty) {
            variantProductList.clear();
            return;
        }

        boolean updateVariantList = true;
        for (AttributeVariantModel avm : attributeVariantList) {
            if (avm.getAttributeId().equals(attrKeyId)) {
                if (avm.getAttributeValues() == null || avm.getAttributeValues().isEmpty()) {
                    updateVariantList = false;
                    for (RecordModel rm : variantProductList) {
                        rm.getFieldValueMap().remove(attrKeyName);
                    }
                    break;
                }
            }
        }

        if (updateVariantList) {
            variantProductList.clear();
            variantProductList.addAll(updatedVariants);
        }
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        FilterManager fm = new FilterManager();
        FilterType fStatus = new FilterType("products_base.product_id", Integer.valueOf(super.getRequestParams().get("product")));
        fm.setFilter("AND", fStatus);
        request.setClause(fm.getClause());
        request.setRequestingModule(super.getModuleId());
        request.setRequestActionType("0");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.recordModel = response.getRecordList().get(0);
                if (GeneralUtils.getFieldFromRecord(recordModel, "has_variant").getCurrentValue().equals("true")) {
                    fm = new FilterManager();
                    fm.setFilter("AND", new FilterType("products_base.parent_product", super.getRequestParams().get("product")));
                    request.setClause(fm.getClause());
                    response = requestHandler.executeRequest(request, "GenericMasterAPI");
                    if (response.getErrorCode() == 1000) {
                        originalVariantList = response.getRecordList();
                        variantProductList = response.getRecordList();
                        for (RecordModel rm : variantProductList) {
                            LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(GeneralUtils.getFieldFromRecord(rm, "attr_map").getCurrentValue());
                            for (Map.Entry<String, String> entry : kvmap.entrySet()) {
                                String attrKeyName = null;
                                TreeMap<String, String> attMap = super.getLookupDictionary().get("attribute");
                                for (Map.Entry<String, String> entryAttName : attMap.entrySet()) {
                                    if (entryAttName.getValue().equals(entry.getKey())) {
                                        attrKeyName = entryAttName.getKey();
                                        break;
                                    }
                                }

                                ModuleFieldModel mfm = new ModuleFieldModel();
                                mfm.setFieldName(attrKeyName);
                                mfm.setCurrentValue(entry.getValue());
                                mfm.setFieldType("TEXT");
                                rm.getFieldValueMap().put(mfm.getFieldName(), mfm);
                            }
                        }
                    }

                    LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(GeneralUtils.getFieldFromRecord(recordModel, "attr_map").getCurrentValue());
                    for (Map.Entry<String, String> entry : kvmap.entrySet()) {
                        AttributeVariantModel avm = new AttributeVariantModel();
                        avm.setAttributeId(entry.getKey());
                        avm.setAttributeValues(new ArrayList<>(Arrays.asList(entry.getValue().split("\\s*,\\s*"))));
                        attributeVariantList.add(avm);
                        this.onAttributeSelect(avm.getAttributeId());
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddProductBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void save() {
        if (!super.validateFields()) {
            return;
        }
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                this.edit();
                break;
            case Defines.REQUEST_MODE_ADD:
                this.populateDataFromForm();
                if (attributeVariantList != null && !attributeVariantList.isEmpty()) {
                    LinkedHashMap<String, String> kvmap = new LinkedHashMap<>();
                    for (AttributeVariantModel avm : attributeVariantList) {
                        kvmap.put(avm.getAttributeId(), String.join(",", avm.getAttributeValues()));
                    }
                    recordModel = GeneralUtils.setFieldValueInRecord(recordModel, "attr_map", GeneralUtils.serializeRequest(kvmap));
                }
                RequestModel request = new RequestModel();
                request.setRecordModel(super.recordModel);
                request.setRequestingModule(super.getModuleId());
                request.setRequestActionType("1");
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel response;
                try {
                    response = requestHandler.executeRequest(request, "GenericMasterAPI");
                    if (response.getErrorCode() == 1000) {
                        ModuleFieldModel mfm = new ModuleFieldModel();
                        mfm.setFieldName("product_id");
                        mfm.setIsKey(true);
                        mfm.setFieldType("LONG");
                        mfm.setCurrentValue(String.valueOf(response.getObjectId()));
                        super.recordModel.getFieldValueMap().put(mfm.getFieldName(), mfm);
                        if (GeneralUtils.getFieldFromRecord(super.recordModel, "has_variant") != null
                                && GeneralUtils.getFieldFromRecord(super.recordModel, "has_variant").getCurrentValue() != null
                                && GeneralUtils.getFieldFromRecord(super.recordModel, "has_variant").getCurrentValue().equals("true")) {
                            for (RecordModel rm : variantProductList) {
                                for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                                    ModuleFieldModel mfmVariant = entry.getValue();
                                    if (mfmVariant.getCurrentValue() == null || mfmVariant.getCurrentValue().isEmpty()) {
                                        rm = GeneralUtils.setFieldValueInRecord(rm, mfmVariant.getFieldName(), GeneralUtils.getFieldFromRecord(recordModel, entry.getKey()).getCurrentValue());
                                    }

                                    if (mfmVariant.getFieldName().equals("parent_product")) {
                                        rm = GeneralUtils.setFieldValueInRecord(rm, mfmVariant.getFieldName(), String.valueOf(response.getObjectId()));
                                    }

                                    if (mfmVariant.getFieldName().equals("product_id")) {
                                        rm = GeneralUtils.setFieldValueInRecord(rm, mfmVariant.getFieldName(), null);
                                    }

                                    if (mfmVariant.getFieldName().equals("has_variant")) {
                                        rm = GeneralUtils.setFieldValueInRecord(rm, mfmVariant.getFieldName(), "false");
                                    }
                                }
                            }

                            request.setBulkRecords(variantProductList);
                            request.setRecordModel(null);
                            response = requestHandler.executeRequest(request, "GenericMasterAPI");
                            if (response.getErrorCode() != 1000) {
                                this.delete();
                            }
                        }
                    }
                    super.updateMessage(response.getErrorCode(), response.getErrorMessage());
                }
                catch (Exception ex) {
                    Logger.getLogger(AddProductBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
    }

    @Override
    public void populateFormData() {
        recordModel = new RecordModel();
        LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
        for (FieldBlockModel fbm : super.getDefaultForm().getBlockList()) {
            for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                if (flm.getComponentType().equals("NORM")) {
                    ModuleFieldModel mfm = new ModuleFieldModel();
                    if (selectedCache != null && selectedCache.get(flm.getFieldObject().getFieldName()) != null) {
                        flm.getFieldObject().setCurrentValue(selectedCache.get(flm.getFieldObject().getFieldName()));
                    }
                    if (requestingCategory != null) {
                        if (flm.getFieldObject().getFieldName().equals("category_id")) {
                            flm.getFieldObject().setCurrentValue(requestingCategory);
                        }
                    }
                    fieldValueMap.put(flm.getFieldObject().getFieldName(), flm.getFieldObject());
                }
            }
        }
        recordModel.setFieldValueMap(fieldValueMap);
    }

    @Override
    public void cancel() {
        if (requestingCategory != null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("catalog.xhtml?faces-redirect=true&catalog="
                        + requestingCategory);
            }
            catch (IOException ex) {
                Logger.getLogger(AddProductBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("products.xhtml");
                FacesContext.getCurrentInstance().responseComplete();
            }
            catch (IOException ex) {
                Logger.getLogger(AddProductBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    @Override
    public void edit() {
        this.populateDataFromForm();
        if (attributeVariantList != null && !attributeVariantList.isEmpty()) {
            LinkedHashMap<String, String> kvmap = new LinkedHashMap<>();
            for (AttributeVariantModel avm : attributeVariantList) {
                kvmap.put(avm.getAttributeId(), String.join(",", avm.getAttributeValues()));
            }
            recordModel = GeneralUtils.setFieldValueInRecord(recordModel, "attr_map", GeneralUtils.serializeRequest(kvmap));
        }
        RequestModel request = new RequestModel();
        request.setRecordModel(super.recordModel);
        request.setRequestingModule(super.getModuleId());
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                boolean addVariants = true;
                if (originalVariantList != null) {
                    request.setRequestActionType("2");
                    request.setRecordModel(null);
                    request.setBulkRecords(originalVariantList);
                    response = requestHandler.executeRequest(request, "GenericMasterAPI");
                    if (response.getErrorCode() != 1000) {
                        addVariants = false;
                    }
                }

                if (addVariants) {
                    if (GeneralUtils.getFieldFromRecord(super.recordModel, "has_variant") != null
                            && GeneralUtils.getFieldFromRecord(super.recordModel, "has_variant").getCurrentValue() != null
                            && GeneralUtils.getFieldFromRecord(super.recordModel, "has_variant").getCurrentValue().equals("true")) {
                        for (RecordModel rm : variantProductList) {
                            for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                                ModuleFieldModel mfmVariant = entry.getValue();
                                if (mfmVariant.getFieldName().equals("parent_product")) {
                                    rm = GeneralUtils.setFieldValueInRecord(rm, mfmVariant.getFieldName(), recordModel.getFieldValueMap().get("product_id").getCurrentValue());
                                }
                                else if (mfmVariant.getFieldName().equals("product_id")) {
                                    rm = GeneralUtils.setFieldValueInRecord(rm, mfmVariant.getFieldName(), null);
                                }
                                else if (mfmVariant.getFieldName().equals("has_variant")) {
                                    rm = GeneralUtils.setFieldValueInRecord(rm, mfmVariant.getFieldName(), "false");
                                }
                                else if (mfmVariant.getCurrentValue() == null || mfmVariant.getCurrentValue().isEmpty()) {
                                    rm = GeneralUtils.setFieldValueInRecord(rm, mfmVariant.getFieldName(), GeneralUtils.getFieldFromRecord(recordModel, entry.getKey()).getCurrentValue());
                                }
                            }
                        }

                        request.setBulkRecords(variantProductList);
                        request.setRecordModel(null);
                        request.setRequestActionType("1");
                        response = requestHandler.executeRequest(request, "GenericMasterAPI");
                        if (response.getErrorCode() != 1000) {
                            this.delete();
                        }
                    }
                }
            }
            super.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddProductBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddProductBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<AttributeVariantModel> getAttributeVariantList() {
        return attributeVariantList;
    }

    public void setAttributeVariantList(ArrayList<AttributeVariantModel> attributeVariantList) {
        this.attributeVariantList = attributeVariantList;
    }

    public ArrayList<RecordModel> getVariantProductList() {
        return variantProductList;
    }

    public void setVariantProductList(ArrayList<RecordModel> variantProductList) {
        this.variantProductList = variantProductList;
    }

}
