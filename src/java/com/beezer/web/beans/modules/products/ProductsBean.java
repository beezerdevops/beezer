/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.products;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.crm.models.comparators.TableSettingsOrderComparator;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "productsBean")
@ViewScoped
public class ProductsBean extends BeanFramework {

    public ProductsBean() {
        super.setModuleId("3");
    }

    @PostConstruct
    public void init() {
        super.setModuleId("3");
        super.loadFilters("3");
        this.loadRecordsList();
    }

    private void loadRecordsList() {
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("products_base.is_variant", "=", "false"));
        fm.setFilter("AND", new FilterType("products_base.product_status", "=", "ACTIVE"));
        super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", fm.getClause(), false, LazyRecordModel.RECORD, super.getModuleId()));
//        super.setLazyRecordModel(new LazyRecordModel("GetProductsWS", null, false,LazyRecordModel.RECORD,"13"));
    }

    public void loadRecordsListBySupplier(String supplierId) {
        try {
            RequestModel request = new RequestModel();
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("products_base.supplier_id", supplierId));
            fm.setFilter("AND", new FilterType("products_base.product_status", "ACTIVE"));
            request.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "GetProductsWS");
            if (response.getErrorCode() == 1000) {
                super.setSettingsList(response.getTabularSettings());
                Collections.sort(super.getSettingsList(), new TableSettingsOrderComparator());
                super.setRecordsList(response.getRecordList());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ProductsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void search(String api) {
        FilterManager fm = new FilterManager();
        fm.skipDefaultHeader(true);
        fm.setFilter("AND", new FilterType("products_base.is_variant", "=", false));
        fm.setFilter("AND", new FilterType("products_base.product_status", "=", "ACTIVE"));
        super.search("GenericMasterAPI", fm.getClause());
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        for (RecordModel rm : super.getSelectedRecords()) {
            rm.getFieldValueMap().get("product_status").setCurrentValue("DELETED");
        }
        request.setRequestingModule(super.getModuleId());
        request.setBulkRecords(new ArrayList<>(super.getSelectedRecords()));
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                for (RecordModel rm : super.getSelectedRecords()) {
                    if (rm.getFieldValueMap().get("has_variant").getCurrentValue().equals("true")) {
                        this.deleteProductVariants(rm.getFieldValueMap().get("product_id").getCurrentValue());
                    }
                }
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "products deleted Susscefully."));
//                FacesContext.getCurrentInstance().getExternalContext().redirect("products.xhtml");
                super.getSelectedRecords().clear();
                this.loadRecordsList();
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete products."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ProductsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void deleteProductVariants(String productId) {
        try {
            RequestModel requestVariants = new RequestModel();
            requestVariants.setRequestingModule(super.getModuleId());
            requestVariants.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("products_base.parent_product", "=", productId));
            requestVariants.setClause(fm.getClause());
            RequestHandler rvHandler = new RequestHandler();
            ResponseModel rvResponse = rvHandler.executeRequest(requestVariants, "GenericMasterAPI", true);
            if (rvResponse.getErrorCode() == 1000) {
                if (rvResponse.getRecordList() != null) {
                    for (RecordModel rm : rvResponse.getRecordList()) {
                        rm.getFieldValueMap().get("product_status").setCurrentValue("DELETED");
                    }
                }
                requestVariants = new RequestModel();
                requestVariants.setRequestingModule(super.getModuleId());
                requestVariants.setRequestActionType("3");
                requestVariants.setBulkRecords(rvResponse.getRecordList());
                rvHandler = new RequestHandler();
                rvResponse = rvHandler.executeRequest(requestVariants, "GenericMasterAPI", true);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ProductsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", super.getSelectedRecords().get(0));
            FacesContext.getCurrentInstance().getExternalContext().redirect("productDetails.xhtml?&product="
                    + super.getSelectedRecords().get(0).getFieldValueMap().get("product_id").getCurrentValue());
//            FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
//                        + super.getSelectedRecords().get(0).getFieldValueMap().get("product_id").getCurrentValue() + "&" + "moduleId=" + super.getModuleId());
        }
        catch (Exception ex) {
            Logger.getLogger(ProductsBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
