/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.purchasing;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.AttributeVariantModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.Visibility;

/**
 *
 * @author badry
 */
@ManagedBean(name = "addPurchaseOrderBean")
@ViewScoped
public class AddPurchaseOrderBean extends BeanFramework {

    private ArrayList<RecordModel> orderRecordList;
    private ArrayList<ModuleFieldModel> orderFieldList;
    private ArrayList<RecordModel> selectedProducts;
    private ArrayList<RecordModel> orderRecordProductList;
    private float totalValue;

    private ArrayList<AttributeVariantModel> attributeVariantList;
    private LinkedHashMap<String, AttributeVariantModel> selectedAttributes;
    private LinkedHashMap<String, LinkedHashMap<String, AttributeVariantModel>> selectedVariantAttributes;

    public AddPurchaseOrderBean() {
    }

    @PostConstruct
    public void init() {
        super.setModuleId("27");
        this.selectedVariantAttributes = new LinkedHashMap<>();
        this.attributeVariantList = new ArrayList<>();
        this.selectedAttributes = new LinkedHashMap<>();
        this.orderFieldList = new ArrayList<>();
        this.orderRecordList = new ArrayList<>();
        this.orderRecordProductList = new ArrayList<>();
        super.loadFieldSet(super.getModuleId(), null);
        this.loadOrderFieldList();
        super.loadForms(super.getModuleId(),0);
        super.buildRecordModel();
        super.setMode();
    }

    public void loadOrderFieldList() {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId("28");
            request.setLayoutId(null);
            RequestHandler requestHandler = new RequestHandler();
            ModuleFieldResponse response = requestHandler.getFieldSet(request);
            if (response.getErrorCode() == 1000) {
                this.orderFieldList = response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRadioValueChange(String s1, String s2) {
        AttributeVariantModel avm = new AttributeVariantModel();
        avm.setAttributeName(s2);
        selectedAttributes.put(s1, avm);
    }

    public void onVariantProductExpand(ToggleEvent event) {
        if (event.getVisibility().equals(Visibility.VISIBLE)) {
            try {
                RecordModel rm = (RecordModel) event.getData();
                if (selectedVariantAttributes.get(rm.getFieldValueMap().get("product_id").getCurrentValue()) != null
                        && !selectedVariantAttributes.get(rm.getFieldValueMap().get("product_id").getCurrentValue()).isEmpty()) {
                }
                else {
                    selectedAttributes = new LinkedHashMap<>();
                    super.loadLookup("attribute", rm.getFieldValueMap().get("attribute").getFieldValues());
                    LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(GeneralUtils.getFieldFromRecord(rm, "attr_map").getCurrentValue());
                    this.attributeVariantList = new ArrayList<>();
                    for (Map.Entry<String, String> entry : kvmap.entrySet()) {
                        AttributeVariantModel avm = new AttributeVariantModel();
                        avm.setAttributeId(entry.getKey());
                        avm.setAttributeValues(new ArrayList<>(Arrays.asList(entry.getValue().split("\\s*,\\s*"))));
                        attributeVariantList.add(avm);
                        selectedAttributes.put(entry.getKey(), new AttributeVariantModel());
                        this.onAttributeSelect(avm.getAttributeId());
                    }
                    selectedVariantAttributes.put(rm.getFieldValueMap().get("product_id").getCurrentValue(), selectedAttributes);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void onAttributeSelect(String selection) {
        String attrKeyName = null;
        TreeMap<String, String> attMap = super.getLookupDictionary().get("attribute");
        for (Map.Entry<String, String> entry : attMap.entrySet()) {
            if (entry.getValue().equals(selection)) {
                attrKeyName = entry.getKey();
                break;
            }
        }

        for (AttributeVariantModel avm : attributeVariantList) {
            if (avm.getAttributeId().equals(selection)) {
                avm.setAttributeName(attrKeyName);
                break;
            }
        }
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        ModuleModel mm = super.loadModule(super.getModuleId());
        String moduleName = "";
        if (mm != null) {
            moduleName = mm.getModuleBaseTable() + ".";
        }
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(moduleName + this.getKeyField(), Integer.valueOf(super.getRequestParams().get("recordId"))));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.recordModel = response.getRecordList().get(0);
                this.loadOrderDetails();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadOrderDetails() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule("28");
        ModuleModel mm = super.loadModule("28");
        String moduleName = "";
        if (mm != null) {
            moduleName = mm.getModuleBaseTable() + ".";
        }
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(moduleName + this.getKeyField(), Integer.valueOf(super.getRequestParams().get("recordId"))));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.orderRecordList = response.getRecordList();
                this.orderRecordProductList = response.getRecordList();
                this.calaculateOrderValue();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void save() {
        if(!super.validateFields()){
            return;
        }
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                this.edit();
                break;
            case Defines.REQUEST_MODE_ADD:
                super.populateFromAllFields();
                RequestModel request = new RequestModel();
                request.setRequestingModule(super.getModuleId());
                request.setRecordModel(this.recordModel);
                request.setRequestActionType("1");
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel response;
                try {
                    response = requestHandler.executeRequest(request, "GenericMasterAPI");
                    if (response.getErrorCode() == 1000) {
                        ModuleFieldModel mfm = new ModuleFieldModel();
                        mfm.setFieldName("po_id");
                        mfm.setIsKey(true);
                        mfm.setCurrentValue(String.valueOf(response.getObjectId()));
                        super.recordModel.getFieldValueMap().put(mfm.getFieldName(), mfm);
                        ResponseModel detailsResponse = createDetailsForPO();
                        if (detailsResponse.getErrorCode() == 1000) {
                            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
                        }
                        else {
                            this.delete();
                            this.updateMessage(2056, "Failed to create order details");
                        }
                    }
                }
                catch (Exception ex) {
                    Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
    }

    @Override
    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("deals.xhtml");
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ResponseModel createDetailsForPO() {
        this.attachPOToDetails();
        RequestModel request = new RequestModel();
        request.setRequestingModule("28");
        request.setBulkRecords(this.orderRecordList);
        request.setRequestActionType("1");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = null;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
        }
        catch (Exception ex) {
            Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    public ResponseModel updateDetailsForPO() {
        this.attachPOToDetails();
        RequestModel request = new RequestModel();
        request.setRequestingModule("28");
        request.setBulkRecords(this.orderRecordList);
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = null;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
        }
        catch (Exception ex) {
            Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    public void attachPOToDetails() {
        for (RecordModel rm : this.orderRecordList) {
            rm = GeneralUtils.setFieldValueInRecord(rm, "po_id", super.recordModel.getFieldValueMap().get("po_id").getCurrentValue());
        }
    }

    public RecordModel constructOrderRecord() {
        RecordModel orderRecord = new RecordModel();
        orderRecord.setFieldValueMap(new LinkedHashMap<>());
        ArrayList<ModuleFieldModel> tempFieldList = (ArrayList<ModuleFieldModel>) orderFieldList.clone();
        for (ModuleFieldModel mfm : tempFieldList) {
            orderRecord.getFieldValueMap().put(mfm.getFieldName(), new ModuleFieldModel(mfm));
        }
        return orderRecord;
    }

    public RecordModel interpretVariant(RecordModel recordModel) {
        String parentProduct = recordModel.getFieldValueMap().get("product_id").getCurrentValue();
        LinkedHashMap<String, AttributeVariantModel> kavm = selectedVariantAttributes.get(parentProduct);
        LinkedHashMap<String, String> kvmap = new LinkedHashMap<>();
        for (Map.Entry<String, AttributeVariantModel> entry : kavm.entrySet()) {
            kvmap.put(entry.getKey(), entry.getValue().getAttributeName());
        }
        RequestModel request = new RequestModel();
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("products_base.parent_product", "=", parentProduct));
        fm.setFilter("AND", new FilterType("products_base.attr_map", "=", GeneralUtils.serializeRequest(kvmap)));
        request.setClause(fm.getClause());
        request.setRequestingModule("3");
        request.setRequestActionType("0");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                return response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void addProductToOrderList() {
        RecordModel orderProduct;
        this.orderRecordList.clear();
//        this.orderRecordProductList.clear();
        for (RecordModel pr : this.selectedProducts) {
            orderProduct = this.constructOrderRecord();
            if (pr.getFieldValueMap().get("has_variant").getCurrentValue().equals("true")) {
                RecordModel vpr = this.interpretVariant(pr);
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_id", vpr.getFieldValueMap().get("product_id").getCurrentValue());
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_type", "PRODUCT");
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "quantity", "1");
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_raw_price", vpr.getFieldValueMap().get("raw_price").getCurrentValue());
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "product_code", vpr.getFieldValueMap().get("product_code").getCurrentValue());
                String prName = "";
                try {
                    LinkedHashMap<String, String> kvmap = GeneralUtils.deserializeKVMap(vpr.getFieldValueMap().get("attr_map").getCurrentValue());
                    for (Map.Entry<String, String> entry : kvmap.entrySet()) {
                        prName = prName + "/" + entry.getValue();
                    }
                }
                catch (Exception ex) {
                    Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "product_name", vpr.getFieldValueMap().get("product_name").getCurrentValue() + prName);
            }
            else {
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_id", pr.getFieldValueMap().get("product_id").getCurrentValue());
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_type", "PRODUCT");
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "quantity", "1");
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_raw_price", pr.getFieldValueMap().get("raw_price").getCurrentValue());
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "product_code", pr.getFieldValueMap().get("product_code").getCurrentValue());
                orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "product_name", pr.getFieldValueMap().get("product_name").getCurrentValue());

            }
            this.orderRecordProductList.add(orderProduct);
        }
        this.selectedProducts.clear();
        this.orderRecordList.addAll(orderRecordProductList);
        calaculateOrderValue();
    }

    public void calaculateOrderValue() {
        totalValue = 0;
        for (RecordModel rm : orderRecordList) {
            totalValue = totalValue
                    + Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "goods_raw_price").getCurrentValue());
        }
        calaculateGrandValue();
    }

    public void calaculateGrandValue() {
        totalValue = 0;
        for (RecordModel rm : orderRecordList) {
            totalValue = totalValue
                    + Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "goods_raw_price").getCurrentValue());
        }
        if (super.recordModel != null) {
            if (super.recordModel.getFieldValueMap().get("sales_tax").getCurrentValue() == null) {
                super.recordModel.getFieldValueMap().get("sales_tax").setCurrentValue("0");
            }
            if (super.recordModel.getFieldValueMap().get("vat_tax").getCurrentValue() == null) {
                super.recordModel.getFieldValueMap().get("vat_tax").setCurrentValue("0");
            }
            if (super.recordModel.getFieldValueMap().get("shipping_cost").getCurrentValue() == null) {
                super.recordModel.getFieldValueMap().get("shipping_cost").setCurrentValue("0");
            }
            float costSalesTax = totalValue * (Float.valueOf(super.recordModel.getFieldValueMap().get("sales_tax").getCurrentValue()) / 100);
            float costVatTax = totalValue * (Float.valueOf(super.recordModel.getFieldValueMap().get("vat_tax").getCurrentValue()) / 100);
            float shippingCost = Float.valueOf(super.recordModel.getFieldValueMap().get("shipping_cost").getCurrentValue());
            float grandTotal = totalValue + costSalesTax + costVatTax + shippingCost;
//            float grandTotal = totalValue;
            super.recordModel.getFieldValueMap().get("po_net_value").setCurrentValue(String.valueOf(grandTotal));
        }
    }

    public String trimTrailingZeros(String input) {
        return input.replaceAll("[0]*$", "").replaceAll(".$", "");
    }

    public void deleteSelectedProduct(RecordModel orderDetailsModel) {
        if (this.orderRecordList != null && this.orderRecordProductList != null) {
            this.orderRecordProductList.remove(orderDetailsModel);
            orderRecordList.remove(orderDetailsModel);
        }
    }

    public ArrayList<RecordModel> getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedProducts(ArrayList<RecordModel> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    public float getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(float totalValue) {
        this.totalValue = totalValue;
    }

    public ArrayList<ModuleFieldModel> getOrderFieldList() {
        return orderFieldList;
    }

    public void setOrderFieldList(ArrayList<ModuleFieldModel> orderFieldList) {
        this.orderFieldList = orderFieldList;
    }

    public ArrayList<RecordModel> getOrderRecordList() {
        return orderRecordList;
    }

    public void setOrderRecordList(ArrayList<RecordModel> orderRecordList) {
        this.orderRecordList = orderRecordList;
    }

    public ArrayList<RecordModel> getOrderRecordProductList() {
        return orderRecordProductList;
    }

    public void setOrderRecordProductList(ArrayList<RecordModel> orderRecordProductList) {
        this.orderRecordProductList = orderRecordProductList;
    }

    @Override
    public void edit() {
        super.populateFromAllFields();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(this.recordModel);
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                ResponseModel detailsResponse = updateDetailsForPO();
                if (detailsResponse.getErrorCode() == 1000) {
                    this.updateMessage(response.getErrorCode(), response.getErrorMessage());
                }
                else {
                    this.updateMessage(2056, "Failed to update order details");
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
        }
        catch (Exception ex) {
            Logger.getLogger(AddPurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<AttributeVariantModel> getAttributeVariantList() {
        return attributeVariantList;
    }

    public void setAttributeVariantList(ArrayList<AttributeVariantModel> attributeVariantList) {
        this.attributeVariantList = attributeVariantList;
    }

    public LinkedHashMap<String, AttributeVariantModel> getSelectedAttributes() {
        return selectedAttributes;
    }

    public void setSelectedAttributes(LinkedHashMap<String, AttributeVariantModel> selectedAttributes) {
        this.selectedAttributes = selectedAttributes;
    }

    public LinkedHashMap<String, LinkedHashMap<String, AttributeVariantModel>> getSelectedVariantAttributes() {
        return selectedVariantAttributes;
    }

    public void setSelectedVariantAttributes(LinkedHashMap<String, LinkedHashMap<String, AttributeVariantModel>> selectedVariantAttributes) {
        this.selectedVariantAttributes = selectedVariantAttributes;
    }

}
