/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.returns;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.sales.DealModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldBlockModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author badry
 */
@ManagedBean(name = "addReturnOrderBean")
@ViewScoped
public class AddReturnOrderBean extends BeanFramework {

    private ArrayList<RecordModel> orderRecordList;
    private ArrayList<ModuleFieldModel> orderFieldList;
    private ArrayList<RecordModel> selectedProducts;
    private ArrayList<RecordModel> orderRecordProductList;
    private ArrayList<RecordModel> summaryRecordList;
    private float totalValue;

    private DealModel dealModel;

    public AddReturnOrderBean() {
    }

    @PostConstruct
    public void init() {
        super.setModuleId("31");
        this.summaryRecordList = new ArrayList<>();
        this.orderFieldList = new ArrayList<>();
        this.orderRecordList = new ArrayList<>();
        this.orderRecordProductList = new ArrayList<>();
        super.loadFieldSet(super.getModuleId(), null);
        this.loadOrderFieldList();
        super.loadForms(super.getModuleId(),0);
        super.loadLookup("item_action", "18");
        super.buildRecordModel();
        super.setMode();
        if (super.getRequestParams().get("deal") != null) {
            this.loadDeal();
            this.initializeBasicData();
        }
    }

    public void loadOrderFieldList() {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId("32");
            request.setLayoutId(null);
            RequestHandler requestHandler = new RequestHandler();
            ModuleFieldResponse response = requestHandler.getFieldSet(request);
            if (response.getErrorCode() == 1000) {
                this.orderFieldList = response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        ModuleModel mm = super.loadModule(super.getModuleId());
        String moduleName = "";
        if (mm != null) {
            moduleName = mm.getModuleBaseTable() + ".";
        }
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(moduleName + this.getKeyField(), Integer.valueOf(super.getRequestParams().get("recordId"))));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.recordModel = response.getRecordList().get(0);
                this.loadOrderDetails();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadDeal() {
        try {
            RequestHandler dealHandler = new RequestHandler();
            RequestModel request = new RequestModel();
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("deals_b2c_base.deal_id", super.getRequestParams().get("deal")));
            request.setRequestActionType("0");
            request.setRequestingModule("7");
            request.setClause(fm.getClause());
            ResponseModel response = dealHandler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
                this.dealModel = new DealModel();
                this.dealModel.setDealDetails(response.getRecordList().get(0));
                fm = new FilterManager();
                fm.setFilter("AND", new FilterType("deal_details.deal_id", super.getRequestParams().get("deal")));
                request.setClause(fm.getClause());
                request.setRequestingModule("5");
                ResponseModel ordersResponse = dealHandler.executeRequest(request);
                if (ordersResponse.getErrorCode() == 1000) {
                    this.dealModel.setOrderList(ordersResponse.getRecordList());
                }
                this.constructOrderDetailsFromDeal();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadOrderDetails() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule("32");
        ModuleModel mm = super.loadModule("32");
        String moduleName = "";
        if (mm != null) {
            moduleName = mm.getModuleBaseTable() + ".";
        }
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(moduleName + this.getKeyField(), Integer.valueOf(super.getRequestParams().get("recordId"))));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
//                this.orderRecordList = response.getRecordList();
                this.orderRecordProductList = response.getRecordList();
//                this.calaculateOrderValue();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    public void constructOrderDetailsFromDeal() {
//        if (dealModel != null && dealModel.getOrderList() != null) {
//            this.orderRecordList = new ArrayList<>();
////            this.orderRecordList = dealModel.getOrderList();
//            for (RecordModel rm : dealModel.getOrderList()) {
//                for (ModuleFieldModel mfm : orderFieldList) {
//                    if (GeneralUtils.getFieldFromRecord(rm, mfm.getFieldName()) == null) {
//                        if (mfm.getFieldName().equals("item_action")) {
//                            mfm.setCurrentValue("3");//set By default to no action
//                            
//                            rm.getFieldValueMap().put(mfm.getFieldName(), new ModuleFieldModel(mfm));
//                        }
//                        else {
//                            rm.getFieldValueMap().put(mfm.getFieldName(), new ModuleFieldModel(mfm));
//                        }
//                    }
//                }
//            }
//            calaculateOrderValue();
//        }
//    }
    public void constructOrderDetailsFromDeal() {
        if (dealModel != null && dealModel.getOrderList() != null) {
            this.orderRecordList = new ArrayList<>();
            ModuleFieldModel itemActionField = null;
            ModuleFieldModel roIdField = null;
            ModuleFieldModel rodIdField = null;
            for (ModuleFieldModel mfm : orderFieldList) {
                switch (mfm.getFieldName()) {
                    case "item_action":
                        mfm.setCurrentValue("3");
                        itemActionField = new ModuleFieldModel(mfm);
                        break;
                    case "ro_id":
                        roIdField = new ModuleFieldModel(mfm);
                        break;
                    case "rod_id":
                        rodIdField = new ModuleFieldModel(mfm);
                        break;
                }
            }
            for (RecordModel rm : dealModel.getOrderList()) {
                RecordModel rec = new RecordModel();
                rec.setFieldValueMap(new LinkedHashMap<>());
                rec.getFieldValueMap().put(itemActionField.getFieldName(), new ModuleFieldModel(itemActionField));
                rec.getFieldValueMap().put(roIdField.getFieldName(), new ModuleFieldModel(roIdField));
                rec.getFieldValueMap().put(rodIdField.getFieldName(), new ModuleFieldModel(rodIdField));
                for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                    rec.getFieldValueMap().put(entry.getValue().getFieldName(), new ModuleFieldModel(rm.getFieldValueMap().get(entry.getValue().getFieldName())));
                }
                orderRecordList.add(rec);
            }
            calaculateOrderValue();
        }
    }

    private void initializeBasicData() {
        if (this.getDefaultForm() != null) {
            for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                    ModuleFieldModel mfm;
                    switch (flm.getFieldName()) {
                        case "deal_id":
                            flm.getFieldObject().setCurrentValue(super.getRequestParams().get("deal"));
                            this.loadRelationRecord("deal_id", super.getRequestParams().get("deal"));
                            break;
                    }
                }
            }
        }
    }

    public void onItemActionSelect(RecordModel recordModel) {
        summaryRecordList.clear();
        for (RecordModel rm : this.orderRecordList) {
            if (rm.getFieldValueMap().get("item_action").getCurrentValue().equals("1")) {
                summaryRecordList.add(rm);
            }
        }
        calaculateOrderValue();
    }

    @Override
    public void save() {
        if (!super.validateFields()) {
            return;
        }
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                this.edit();
                break;
            case Defines.REQUEST_MODE_ADD:
                super.populateFromAllFields();
                this.refundedItemsHandler();
                this.replacedItemsHandler();
//                RequestModel request = new RequestModel();
//                request.setRequestingModule(super.getModuleId());
//                request.setRecordModel(this.recordModel);
//                request.setRequestActionType("1");
//                RequestHandler requestHandler = new RequestHandler();
//                ResponseModel response;
//                try {
//                    response = requestHandler.executeRequest(request, "GenericMasterAPI");
//                    if (response.getErrorCode() == 1000) {
//                        ModuleFieldModel mfm = new ModuleFieldModel();
//                        mfm.setFieldName("ro_id");
//                        mfm.setIsKey(true);
//                        mfm.setCurrentValue(String.valueOf(response.getObjectId()));
//                        super.recordModel.getFieldValueMap().put(mfm.getFieldName(), mfm);
//                        ResponseModel detailsResponse = createDetailsForRO();
//                        if (detailsResponse.getErrorCode() == 1000) {
//                            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
//                        }
//                        else {
//                            this.delete();
//                            this.updateMessage(2056, "Failed to create order details");
//                        }
//                    }
//                }
//                catch (Exception ex) {
//                    Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
//                }
                break;
        }
    }

    @Override
    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("deals.xhtml");
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ResponseModel createDetailsForRO(ArrayList<RecordModel> itemList) {
//        this.attachROToDetails();
        for (RecordModel rm : itemList) {
            rm.getFieldValueMap().get("dd_id").setIsKey(false);
            rm.getFieldValueMap().get("rod_id").setCurrentValue(null);
            rm.getFieldValueMap().get("ro_id").setCurrentValue(super.recordModel.getFieldValueMap().get("ro_id").getCurrentValue());
        }
        RequestModel request = new RequestModel();
        request.setRequestingModule("32");
        request.setBulkRecords(itemList);
        request.setRequestActionType("1");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = null;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    public ResponseModel updateDetailsForRO() {
        this.attachROToDetails();
        RequestModel request = new RequestModel();
        request.setRequestingModule("32");
        request.setBulkRecords(this.orderRecordList);
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = null;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    public void attachROToDetails() {
        for (RecordModel rm : this.orderRecordList) {
            rm = GeneralUtils.setFieldValueInRecord(rm, "ro_id", super.recordModel.getFieldValueMap().get("ro_id").getCurrentValue());
        }
    }

    public RecordModel constructOrderRecord() {
        RecordModel orderRecord = new RecordModel();
        orderRecord.setFieldValueMap(new LinkedHashMap<>());
        ArrayList<ModuleFieldModel> tempFieldList = (ArrayList<ModuleFieldModel>) orderFieldList.clone();
        for (ModuleFieldModel mfm : tempFieldList) {
            orderRecord.getFieldValueMap().put(mfm.getFieldName(), new ModuleFieldModel(mfm));
        }
        return orderRecord;
    }

    public void addProductToOrderList() {
        RecordModel orderProduct;
        this.orderRecordList.clear();
        this.orderRecordProductList.clear();
        for (RecordModel pr : this.selectedProducts) {
            orderProduct = this.constructOrderRecord();
            orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_id", pr.getFieldValueMap().get("product_id").getCurrentValue());
            orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_type", "PRODUCT");
            orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "quantity", "1");
            orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "goods_raw_price", pr.getFieldValueMap().get("raw_price").getCurrentValue());
            orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "product_code", pr.getFieldValueMap().get("product_code").getCurrentValue());
            orderProduct = GeneralUtils.setFieldValueInRecord(orderProduct, "product_name", pr.getFieldValueMap().get("product_name").getCurrentValue());
            this.orderRecordProductList.add(orderProduct);
        }
        this.orderRecordList.addAll(orderRecordProductList);
        calaculateOrderValue();
    }

    public void calaculateOrderValue() {
        totalValue = 0;
        for (RecordModel rm : summaryRecordList) {
            totalValue = totalValue
                    + Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "goods_selling_price").getCurrentValue());
        }
        calaculateGrandValue();
    }

    public void calaculateGrandValue() {
        totalValue = 0;
        for (RecordModel rm : summaryRecordList) {
            totalValue = totalValue
                    + Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "goods_selling_price").getCurrentValue());
        }
        if (dealModel.getDealDetails() != null) {
            if (dealModel.getDealDetails().getFieldValueMap().get("sales_tax") == null || dealModel.getDealDetails().getFieldValueMap().get("sales_tax").getCurrentValue() == null) {
                dealModel.getDealDetails().getFieldValueMap().get("sales_tax").setCurrentValue("0");
            }
            if (dealModel.getDealDetails().getFieldValueMap().get("vat_tax") == null || dealModel.getDealDetails().getFieldValueMap().get("vat_tax").getCurrentValue() == null) {
                dealModel.getDealDetails().getFieldValueMap().get("vat_tax").setCurrentValue("0");
            }
            if (recordModel.getFieldValueMap().get("shipping_cost") == null || recordModel.getFieldValueMap().get("shipping_cost").getCurrentValue() == null) {
                recordModel.getFieldValueMap().get("shipping_cost").setCurrentValue("0");
            }
            float costSalesTax = totalValue * (Float.valueOf(dealModel.getDealDetails().getFieldValueMap().get("sales_tax").getCurrentValue()) / 100);
            float costVatTax = totalValue * (Float.valueOf(dealModel.getDealDetails().getFieldValueMap().get("vat_tax").getCurrentValue()) / 100);
            float shippingCost = Float.valueOf(recordModel.getFieldValueMap().get("shipping_cost").getCurrentValue());
            float grandTotal = totalValue + costSalesTax + costVatTax + shippingCost;
//            float grandTotal = totalValue;
            super.recordModel.getFieldValueMap().get("ro_net_value").setCurrentValue(String.valueOf(grandTotal));
        }
    }

    public String calaculateRecordGrandValue(RecordModel master, ArrayList<RecordModel> orderDetails) {
        totalValue = 0;
        for (RecordModel rm : orderDetails) {
            totalValue = totalValue
                    + Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue()) * Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "goods_selling_price").getCurrentValue());
        }
        if (master != null) {
            if (master.getFieldValueMap().get("sales_tax").getCurrentValue() == null) {
                master.getFieldValueMap().get("sales_tax").setCurrentValue("0");
            }
            if (master.getFieldValueMap().get("vat_tax").getCurrentValue() == null) {
                master.getFieldValueMap().get("vat_tax").setCurrentValue("0");
            }
            if (master.getFieldValueMap().get("shipping_cost").getCurrentValue() == null) {
                master.getFieldValueMap().get("shipping_cost").setCurrentValue("0");
            }
            float costSalesTax = totalValue * (Float.valueOf(master.getFieldValueMap().get("sales_tax").getCurrentValue()) / 100);
            float costVatTax = totalValue * (Float.valueOf(master.getFieldValueMap().get("vat_tax").getCurrentValue()) / 100);
            float shippingCost = Float.valueOf(master.getFieldValueMap().get("shipping_cost").getCurrentValue());
            float grandTotal = totalValue + costSalesTax + costVatTax + shippingCost;
//            float grandTotal = totalValue;
            return String.valueOf(grandTotal);
        }
        return "0";
    }

    public String trimTrailingZeros(String input) {
        return input.replaceAll("[0]*$", "").replaceAll(".$", "");
    }

    public void createReturnOrder(String returnType, ArrayList<RecordModel> itemList) {
        super.populateFromAllFields();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        this.recordModel.getFieldValueMap().get("ro_order_type").setCurrentValue(returnType);
        this.recordModel.getFieldValueMap().get("cust_id").setCurrentValue(dealModel.getDealDetails().getFieldValueMap().get("cust_id").getCurrentValue());
        request.setRecordModel(this.recordModel);
        request.setRequestActionType("1");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                ModuleFieldModel mfm = new ModuleFieldModel();
                mfm.setFieldName("ro_id");
                mfm.setIsKey(true);
                mfm.setCurrentValue(String.valueOf(response.getObjectId()));
                super.recordModel.getFieldValueMap().put(mfm.getFieldName(), mfm);
                ResponseModel detailsResponse = createDetailsForRO(itemList);
                if (detailsResponse.getErrorCode() == 1000) {
                    this.updateMessage(response.getErrorCode(), response.getErrorMessage());
                }
                else {
                    this.delete();
                    this.updateMessage(2056, "Failed to create order details");
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void refundedItemsHandler() {
        ArrayList<RecordModel> refundedItems = new ArrayList<>();
        for (RecordModel rm : this.orderRecordList) {
            if (rm.getFieldValueMap().get("item_action").getCurrentValue().equals("1")) {
                rm.getFieldValueMap().get("item_status").setCurrentValue("REFUND");
                refundedItems.add(rm);
            }
        }

        if (!refundedItems.isEmpty()) {
            String netRefundedValue = this.calaculateRecordGrandValue(dealModel.getDealDetails(), refundedItems);
            DealModel dm = this.deepCopyDeal();
            dm.getDealDetails().getFieldValueMap().get("deal_id").setCurrentValue(null);
            dm.getDealDetails().getFieldValueMap().get("final_price").setCurrentValue("-" + netRefundedValue);
            dm.getDealDetails().getFieldValueMap().get("deal_status").setCurrentValue("REFUND");
            dm.getDealDetails().getFieldValueMap().get("deal_name").setCurrentValue(null);
            RequestModel request = new RequestModel();
            request.setRequestingModule("7");
            request.setRecordModel(dm.getDealDetails());
            request.setRequestActionType("1");
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response;
            try {
                response = requestHandler.executeRequest(request, "GenericMasterAPI");
                if (response.getErrorCode() == 1000) {
                    this.createReturnOrder("REFUND", refundedItems);
                    this.updateDealDetails(refundedItems);
                    String netDealValue = this.calaculateRecordGrandValue(dealModel.getDealDetails(), dealModel.getOrderList());
                    this.updateDeal(netDealValue);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void replacedItemsHandler() {
        ArrayList<RecordModel> replacedItems = new ArrayList<>();
        for (RecordModel rm : this.orderRecordList) {
            if (rm.getFieldValueMap().get("item_action").getCurrentValue().equals("2")) {
                rm.getFieldValueMap().get("item_status").setCurrentValue("REPLACE");
                replacedItems.add(rm);
            }
        }

        if (!replacedItems.isEmpty()) {
            DealModel dm = this.deepCopyDeal();
            dm.getDealDetails().getFieldValueMap().get("deal_id").setCurrentValue(null);
            dm.getDealDetails().getFieldValueMap().get("final_price").setCurrentValue("0");
            dm.getDealDetails().getFieldValueMap().get("deal_status").setCurrentValue("REPLACE");
            dm.getDealDetails().getFieldValueMap().get("deal_name").setCurrentValue(null);
            RequestModel request = new RequestModel();
            request.setRequestingModule("7");
            request.setRecordModel(dm.getDealDetails());
            request.setRequestActionType("1");
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response;
            try {
                response = requestHandler.executeRequest(request, "GenericMasterAPI");
                if (response.getErrorCode() == 1000) {
                    this.createReturnOrder("REPLACE", replacedItems);
                    this.updateDealDetails(replacedItems);
                    ArrayList<RecordModel> dealDetails = new ArrayList<>();
                    dealDetails.addAll(orderRecordList);
                    for (RecordModel rm : replacedItems) {
                        rm.getFieldValueMap().get("deal_id").setCurrentValue(String.valueOf(response.getObjectId()));
                    }
                    this.createDealDetails(replacedItems);
                    this.updateDeal(null);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private DealModel deepCopyDeal() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String str = mapper.writeValueAsString(dealModel);
            DealModel dm = mapper.readValue(str, DealModel.class);
            return dm;
        }
        catch (IOException ex) {
            return null;
        }
    }

    //In case of replacments only beacause a return order can be create on them by themselfs
    public void createDeal() {
    }

    public void updateDeal(String newTotalValue) {
        DealModel dm = this.deepCopyDeal();
        dm.getDealDetails().getFieldValueMap().get("deal_status").setCurrentValue("ADJ");
        if (newTotalValue != null) {
            dm.getDealDetails().getFieldValueMap().get("final_price").setCurrentValue(newTotalValue);
        }
        RequestModel request = new RequestModel();
        request.setRequestingModule("7");
        request.setRecordModel(dm.getDealDetails());
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createDealDetails(ArrayList<RecordModel> orderRecords) {
        for (RecordModel rm : orderRecords) {
            ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, "dd_id");
            mfm.setIsKey(true);
            mfm.setCurrentValue(null);
        }
        RequestModel request = new RequestModel();
        request.setRequestingModule("5");
        request.setBulkRecords(orderRecords);
        request.setRequestActionType("1");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = null;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateDealDetails(ArrayList<RecordModel> orderRecords) {
        for (RecordModel rm : orderRecords) {
            ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, "dd_id");
            mfm.setIsKey(true);
            rm.getFieldValueMap().get("rod_id").setIsKey(false);
            for (RecordModel dmrm : dealModel.getOrderList()) {
                if (rm.getFieldValueMap().get("goods_id").getCurrentValue().equals(dmrm.getFieldValueMap().get("goods_id").getCurrentValue())) {
                    float updatedQuant = Float.valueOf(dmrm.getFieldValueMap().get("quantity").getCurrentValue()) - Float.valueOf(rm.getFieldValueMap().get("quantity").getCurrentValue());
                    rm.getFieldValueMap().get("quantity").setCurrentValue(String.valueOf(updatedQuant));
                    dmrm.getFieldValueMap().get("quantity").setCurrentValue(String.valueOf(updatedQuant));
                }
            }
        }
        RequestModel request = new RequestModel();
        request.setRequestingModule("5");
        request.setBulkRecords(orderRecords);
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = null;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteSelectedProduct(RecordModel orderDetailsModel) {
        if (this.orderRecordList != null && this.orderRecordProductList != null) {
            this.orderRecordProductList.remove(orderDetailsModel);
            orderRecordList.remove(orderDetailsModel);
        }
    }

    public ArrayList<RecordModel> getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedProducts(ArrayList<RecordModel> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    public float getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(float totalValue) {
        this.totalValue = totalValue;
    }

    public ArrayList<ModuleFieldModel> getOrderFieldList() {
        return orderFieldList;
    }

    public void setOrderFieldList(ArrayList<ModuleFieldModel> orderFieldList) {
        this.orderFieldList = orderFieldList;
    }

    public ArrayList<RecordModel> getOrderRecordList() {
        return orderRecordList;
    }

    public void setOrderRecordList(ArrayList<RecordModel> orderRecordList) {
        this.orderRecordList = orderRecordList;
    }

    public ArrayList<RecordModel> getOrderRecordProductList() {
        return orderRecordProductList;
    }

    public void setOrderRecordProductList(ArrayList<RecordModel> orderRecordProductList) {
        this.orderRecordProductList = orderRecordProductList;
    }

    @Override
    public void edit() {
        super.populateFromAllFields();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(this.recordModel);
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                ResponseModel detailsResponse = updateDetailsForRO();
                if (detailsResponse.getErrorCode() == 1000) {
                    this.updateMessage(response.getErrorCode(), response.getErrorMessage());
                }
                else {
                    this.updateMessage(2056, "Failed to update order details");
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
        }
        catch (Exception ex) {
            Logger.getLogger(AddReturnOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public DealModel getDealModel() {
        return dealModel;
    }

    public void setDealModel(DealModel dealModel) {
        this.dealModel = dealModel;
    }

    public ArrayList<RecordModel> getSummaryRecordList() {
        return summaryRecordList;
    }

    public void setSummaryRecordList(ArrayList<RecordModel> summaryRecordList) {
        this.summaryRecordList = summaryRecordList;
    }

}
