/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.returns;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "returnOrdersBean")
@ViewScoped
public class ReturnOrdersBean extends BeanFramework {

    private String recordId;

    public ReturnOrdersBean() {
    }

    @PostConstruct
    public void init() {
        super.setModuleId("31");
        super.loadFilters(super.getModuleId());
        this.loadRecordsList();
    }

    public void loadRecordsList() {
        super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", null, false, LazyRecordModel.RECORD, super.getModuleId()));
    }

    public void loadRelatedRecord(String keyName, String keyValue) {
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(keyName, keyValue));
        super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", fm.getClause(), false, LazyRecordModel.RECORD, super.getModuleId()));
    }

    public void addNewRecord() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("addReturnOrder.xhtml?moduleId=" + super.getModuleId());
        }
        catch (IOException ex) {
            Logger.getLogger(ReturnOrdersBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void search(String api) {
        super.search("GenericMasterAPI");
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        request.setBulkRecords(new ArrayList<>(super.getSelectedRecords()));
        request.setRequestActionType("2");
        request.setRequestingModule(super.getModuleId());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.getSelectedRecords().clear();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record deleted Susscefully."));
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete record."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ReturnOrdersBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", super.getSelectedRecords().get(0));
            RecordModel selectedModel = (RecordModel) event.getObject();
            for (Map.Entry<String, ModuleFieldModel> entry : selectedModel.getFieldValueMap().entrySet()) {
                if (entry.getValue().isIsKey()) {
                    this.recordId = entry.getValue().getCurrentValue();
                    break;
                }
            }
            if (super.getModuleId().equals("7")) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("dealDetails.xhtml?recordId="
                        + this.recordId);
            }
            else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("returnOrderDetails.xhtml?recordId="
                        + this.recordId + "&" + "moduleId=" + super.getModuleId());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ReturnOrdersBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
