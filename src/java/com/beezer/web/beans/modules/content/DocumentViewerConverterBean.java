/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.content;

import com.beezer.web.handler.UnstructuredHandler;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.requests.UnstructuredRequest;
import com.crm.models.responses.UnstructuredResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Ahmed El Badry Aug 12, 2022
 */
@ManagedBean(name = "documentViewerConverterBean")
@RequestScoped
public class DocumentViewerConverterBean {

    private StreamedContent readyDocument;
    private String idCache;
    private String requestedObjectId;

    public DocumentViewerConverterBean() {
//        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            readyDocument = new DefaultStreamedContent();
        }
        else {
            String id = context.getExternalContext().getRequestParameterMap().get("documentId");
            requestedObjectId = context.getExternalContext().getRequestParameterMap().get("objectId");
            if (id != null) {
                idCache = id;
            }
            UnstructuredModel requestedContentModel = null;
            UnstructuredRequest request = new UnstructuredRequest();
            request.setRequestActionType("0");
            request.setContext("ATTACHMENTS");
            request.setObjectId(Integer.valueOf(requestedObjectId));
            request.setUnstructuredId(idCache);
            UnstructuredHandler handler = new UnstructuredHandler();
            UnstructuredResponse response = handler.executeForManager(request);
            if (response.getErrorCode() == 1000) {
                requestedContentModel = response.getReturnList().get(0);
            }

            if (requestedContentModel != null && requestedContentModel.getContentStream() != null) {
                InputStream targetStream = new ByteArrayInputStream(requestedContentModel.getContentStream());
                readyDocument = DefaultStreamedContent.builder()
                        .name(requestedContentModel.getFileName())
                        .contentType("application/pdf")
                        .stream(() -> targetStream)
                        .build();
                FacesContext.getCurrentInstance().responseComplete();
            }
        }
    }

//    public void generatePdf(String reportId) {
//        FacesContext context = FacesContext.getCurrentInstance();
//        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
//            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
//            readyDocument = new DefaultStreamedContent();
//        }
//        else {
//            FilterManager fm = new FilterManager();
//            fm.setFilter("AND", new FilterType("id", "=", reportId));
//            ReportRequest reportRequest = new ReportRequest();
//            reportRequest.setRequestActionType("0");
//            reportRequest.setClause(fm.getClause());
//            ReportResponse reportResponse = null;
//            ReportHandler reportHandler = new ReportHandler();
//            try {
//                reportResponse = reportHandler.execute(reportRequest);
//            }
//            catch (Exception ex) {
//                Logger.getLogger(ApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            ReportModel reportModel = null;
//            if (reportResponse != null && reportResponse.getErrorCode() == 1000) {
//                reportModel = reportResponse.getReturnList().get(0);
//            }
//
//            if (reportModel.getCustomReportConfig() != null) {
//                DocumentUtilsHandler documentUtilsHandler = new DocumentUtilsHandler();
//                DocumentUtilRequest documentUtilRequest = new DocumentUtilRequest();
//                documentUtilRequest.setRequestActionType("0");
//                documentUtilRequest.setHtmlContent(reportModel.getCustomReportConfig().getHtmlContent());
//                DocumentUtilResponse documentUtilResponse = documentUtilsHandler.executeRequest(documentUtilRequest);
//                if (documentUtilResponse.getErrorCode() == 1000 && documentUtilResponse.getConvertedDocument() != null) {
//                    convertedReport = DefaultStreamedContent.builder()
//                            .name(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue() + ".pdf")
//                            .contentType("application/pdf")
//                            .stream(() -> new ByteArrayInputStream(
//                            documentUtilResponse.getConvertedDocument().getContentStream()))
//                            .build();
//                }
//            }
//        }
//    }

    public boolean isAjaxRequest() {
        boolean val = FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest();
        return val;
    }

    public StreamedContent getReadyDocument() {
        return readyDocument;
    }

}
