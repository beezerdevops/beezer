/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.content;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.UnstructuredHandler;
import com.beezer.web.models.BreadCrumbHolder;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UnstructuredFolder;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.UnstructuredRequest;
import com.crm.models.responses.UnstructuredResponse;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Ahmed El Badry
 */
@ManagedBean(name = "contentLibraryBean")
@ViewScoped
public class ContentLibraryBean extends BeanFramework {

    private String requestedObjectId;
    private String requestedModule;
    private ArrayList<UnstructuredModel> contentList;
    private ArrayList<UnstructuredFolder> unstructuredFolders;
    private ArrayList<UnstructuredModel> directoryContentList;
    private ArrayList<UnstructuredFolder> directoryFolders;
    private UnstructuredFolder newFolder;
    private UnstructuredFolder selectedFolder;

    private StreamedContent pdfToView;
    private String selectedUnstructId;

    private int currentFolderTreeId;

    private ArrayList<BreadCrumbHolder> breadCrumbHolder;
    private List<File> selectedFiles = new ArrayList<>();

    @PostConstruct
    public void init() {
        this.currentFolderTreeId = -1;
        this.loadFolderList();
        this.loadContentList();
    }

    public void loadContentList() {
        if (requestedObjectId != null) {
            UnstructuredRequest request = new UnstructuredRequest();
            request.setRequestActionType("0");
            request.setContext("ATTACHMENTS");
            request.setObjectId(Integer.valueOf(requestedObjectId));
            UnstructuredHandler handler = new UnstructuredHandler();
            UnstructuredResponse response = handler.executeForManager(request);
            if (response.getErrorCode() == 1000) {
                this.contentList = response.getReturnList();
            }
            this.loadDirectoryContentList();
        }
    }

    public void loadDirectoryContentList() {
        this.directoryContentList = new ArrayList<>();
        if (this.contentList != null && !this.contentList.isEmpty()) {
            for (UnstructuredModel um : contentList) {
                if (um.getParentFolder() == 0) {
                    um.setParentFolder(-1);
                }
                if (um.getParentFolder() == this.currentFolderTreeId) {
                    this.directoryContentList.add(um);
                }
            }
        }
    }

    public ArrayList<UnstructuredModel> loadDirectoryContentList(int folderId) {
        ArrayList<UnstructuredModel> returnList = new ArrayList<>();
        if (this.contentList != null && !this.contentList.isEmpty()) {
            for (UnstructuredModel um : contentList) {
                if (um.getParentFolder() == folderId) {
                    returnList.add(um);
                }
            }
        }
        return returnList;
    }

    public void loadFolderList() {
        if (requestedObjectId != null) {
            UnstructuredRequest request = new UnstructuredRequest();
            request.setRequestActionType("7");
            FilterManager filterManager = new FilterManager();
            filterManager.setFilter("AND", new FilterType("module_id", "=", requestedModule));
            filterManager.setFilter("AND", new FilterType("record_id", "=", requestedObjectId));
            request.setClause(filterManager.getClause());
            UnstructuredHandler handler = new UnstructuredHandler();
            UnstructuredResponse response = handler.executeForManager(request);
            if (response.getErrorCode() == 1000) {
                this.unstructuredFolders = response.getFolderList();
            }
            this.loadDirectoryFolders();
            this.buildBreadCrumb();
        }
    }

    public void loadDirectoryFolders() {
        this.directoryFolders = new ArrayList<>();
        if (this.unstructuredFolders != null && !this.unstructuredFolders.isEmpty()) {
            for (UnstructuredFolder folder : unstructuredFolders) {
                if (folder.getParentFolder() == this.currentFolderTreeId) {
                    this.directoryFolders.add(folder);
                }
            }
        }
    }

    private void buildBreadCrumb() {
        this.breadCrumbHolder = new ArrayList<>();
        BreadCrumbHolder bch = new BreadCrumbHolder();
        if (this.unstructuredFolders != null && !this.unstructuredFolders.isEmpty()) {
            bch = new BreadCrumbHolder();
            bch.setName("Root");
            bch.setFolderId(-1);
            bch.setParentFolderId(-1);
            breadCrumbHolder.add(bch);
//            for (UnstructuredFolder folder : unstructuredFolders) {
//                bch = new BreadCrumbHolder();
//                bch.setName(folder.getFolderName());
//                bch.setFolderId(folder.getFolderId());
//                bch.setParentFolderId(folder.getParentFolder());
//                breadCrumbHolder.add(bch);
//            }
        }
        else {
            bch = new BreadCrumbHolder();
            bch.setName("Root");
            bch.setFolderId(-1);
            bch.setParentFolderId(-1);
            breadCrumbHolder.add(bch);
        }
    }

    public void onFolderClick(UnstructuredFolder folder) {
        this.currentFolderTreeId = folder.getFolderId();
        this.loadDirectoryFolders();
        this.loadDirectoryContentList();
//        this.buildBreadCrumb();
        BreadCrumbHolder bch = new BreadCrumbHolder();
        bch.setName(folder.getFolderName());
        bch.setFolderId(folder.getFolderId());
        bch.setParentFolderId(folder.getParentFolder());
        breadCrumbHolder.add(bch);
    }

    public void folderPredeleteCheck(UnstructuredFolder folder) {
        this.selectedFolder = folder;
        ArrayList<UnstructuredModel> returnList = this.loadDirectoryContentList(folder.getFolderId());
        if (returnList != null && !returnList.isEmpty()) {
            PrimeFaces.current().executeScript("PF('folderContainsFilesWarning').show()");
        }
        else {
            this.deleteFolder();
            this.selectedFolder = new UnstructuredFolder();
            PrimeFaces.current().ajax().update("attachementsContentHolder");
        }
    }

    public void deleteFolder() {
        UnstructuredRequest request = new UnstructuredRequest();
        request.setRequestActionType("9");
        request.setUnstructuredFolder(selectedFolder);
        UnstructuredHandler handler = new UnstructuredHandler();
        UnstructuredResponse response = handler.executeForManager(request);
        if (response.getErrorCode() == 1000) {
            this.unstructuredFolders.remove(selectedFolder);
            this.loadDirectoryFolders();
        }
    }

    public void onBreadCrumbClick(BreadCrumbHolder crumbHolder) {
        this.currentFolderTreeId = crumbHolder.getFolderId();
        this.loadDirectoryFolders();
        this.loadDirectoryContentList();

        int index = breadCrumbHolder.indexOf(crumbHolder);
        if (index != -1) {
            breadCrumbHolder.subList(index + 1, breadCrumbHolder.size()).clear(); // clear the sublist starting from the next element after the specified object
        }
//        this.buildBreadCrumb();
    }

    public void handleAttchmentsUpload(FileUploadEvent event) {
        UnstructuredModel um = new UnstructuredModel();
        um.setFileExtenstion(GeneralUtils.getFileExtenstion(event.getFile().getFileName()));
        um.setFileName(event.getFile().getFileName());
        um.setFileSize(event.getFile().getSize());
        um.setContentStream(event.getFile().getContent());

        UnstructuredRequest unstructuredRequest = new UnstructuredRequest();
        um.setObjectId(Integer.valueOf(requestedObjectId));
        um.setModule(super.loadModule(this.requestedModule).getModuleName());
        um.setContext("ATTACHMENTS");
        um.setParentFolder(currentFolderTreeId);
        unstructuredRequest.setUnstructuredModel(um);
        unstructuredRequest.setRequestActionType("1");

        UnstructuredHandler handler = new UnstructuredHandler();
        UnstructuredResponse response = handler.executeForManager(unstructuredRequest);
        if (response.getErrorCode() == 1000) {
            this.loadContentList();
        }
    }

    public void openNewFolderDialog() {
        this.newFolder = new UnstructuredFolder();
        PrimeFaces.current().resetInputs("addAttachmentFolderDialog");
        PrimeFaces.current().ajax().update("addAttachmentFolderDialog");
        PrimeFaces.current().executeScript("PF('addAttachmentFolderDialog').show()");
    }

    public void openUploadFolderDialog() {
        this.newFolder = new UnstructuredFolder();
        PrimeFaces.current().resetInputs("uploadAttachmentFolderDialog");
        PrimeFaces.current().ajax().update("uploadAttachmentFolderDialog");
        PrimeFaces.current().executeScript("PF('uploadAttachmentFolderDialog').show()");
    }

    public void uploadFolder() {
        List<File> files = new ArrayList<>();
        try {
            for (Part part : ((HttpServletRequest) (FacesContext.getCurrentInstance().getExternalContext().getRequest())).getParts()) {
                String filename = getFilename(part);
                if (filename != null) {
                    File file = new File(filename);
                    try {
                        Files.copy(part.getInputStream(), file.toPath());
                        files.add(file);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        catch (IOException ex) {
            Logger.getLogger(ContentLibraryBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (ServletException ex) {
            Logger.getLogger(ContentLibraryBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        // Store the selected files in the bean
        selectedFiles = new ArrayList<>();
        for (File file : files) {
            if (file.isDirectory()) {
                selectedFiles.addAll(getFilesInDirectory(file));
            }
            else {
                selectedFiles.add(file);
            }
        }

        // Do something with the selected files, e.g. print their names
        for (File file : selectedFiles) {
            System.out.println(file.getName());
        }

    }

    private String getFilename(Part part) {
        String contentDisposition = part.getHeader("content-disposition");
        String[] tokens = contentDisposition.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2, token.length() - 1);
            }
        }
        return null;
    }

    private List<File> getFilesInDirectory(File directory) {
        List<File> files = new ArrayList<>();
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                files.addAll(getFilesInDirectory(file));
            }
            else {
                files.add(file);
            }
        }
        return files;
    }

    public void createNewFolder() {
        UnstructuredRequest request = new UnstructuredRequest();
        request.setRequestActionType("6");
        this.newFolder.setParentFolder(currentFolderTreeId);
        this.newFolder.setModuleId(Integer.valueOf(requestedModule));
        this.newFolder.setRecordId(Integer.valueOf(requestedObjectId));
        request.setUnstructuredFolder(newFolder);
        UnstructuredHandler handler = new UnstructuredHandler();
        UnstructuredResponse response = handler.executeForManager(request);
        if (response.getErrorCode() == 1000) {
            if (this.unstructuredFolders == null) {
                this.unstructuredFolders = new ArrayList<>();
            }
            UnstructuredFolder folder = new UnstructuredFolder();
            folder.setFolderId(response.getObjectId());
            folder.setFolderName(newFolder.getFolderName());
            folder.setModuleId(newFolder.getModuleId());
            folder.setRecordId(newFolder.getRecordId());
            folder.setParentFolder(newFolder.getParentFolder());
            this.unstructuredFolders.add(folder);
            this.loadDirectoryFolders();
        }
    }

    public StreamedContent downloadAttachment(UnstructuredModel model) {
        UnstructuredModel requestedContentModel = null;
        if (model != null) {
            UnstructuredRequest request = new UnstructuredRequest();
            request.setRequestActionType("0");
            request.setContext("ATTACHMENTS");
            request.setObjectId(Integer.valueOf(requestedObjectId));
            request.setUnstructuredId(model.getUnstructuredId());
            UnstructuredHandler handler = new UnstructuredHandler();
            UnstructuredResponse response = handler.executeForManager(request);
            if (response.getErrorCode() == 1000) {
                requestedContentModel = response.getReturnList().get(0);
            }
        }

        if (requestedContentModel != null && requestedContentModel.getContentStream() != null) {
            InputStream targetStream = new ByteArrayInputStream(requestedContentModel.getContentStream());
            switch (requestedContentModel.getFileExtenstion().toLowerCase()) {
                case "docx":
                    return DefaultStreamedContent.builder()
                            .name(requestedContentModel.getFileName())
                            .contentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document")
                            .stream(() -> targetStream)
                            .build();
                case "pdf":
                    return DefaultStreamedContent.builder()
                            .name(requestedContentModel.getFileName())
                            .contentType("application/pdf")
                            .stream(() -> targetStream)
                            .build();
                case "png":
                    return DefaultStreamedContent.builder()
                            .name(requestedContentModel.getFileName())
                            .contentType("image/png")
                            .stream(() -> targetStream)
                            .build();
                default:
                    return DefaultStreamedContent.builder()
                            .name(requestedContentModel.getFileName())
                            .stream(() -> targetStream)
                            .build();
            }
//            return new DefaultStreamedContent(targetStream, "image/jpg", requestedContentModel.getFileName());
        }
        return null;
    }

    public StreamedContent downloadAttachment(UnstructuredModel model, String objectId) {
        UnstructuredModel requestedContentModel = null;
        if (model != null) {
            UnstructuredRequest request = new UnstructuredRequest();
            request.setRequestActionType("0");
            request.setContext("ATTACHMENTS");
            request.setObjectId(Integer.valueOf(objectId));
            request.setUnstructuredId(model.getUnstructuredId());
            UnstructuredHandler handler = new UnstructuredHandler();
            UnstructuredResponse response = handler.executeForManager(request);
            if (response.getErrorCode() == 1000) {
                requestedContentModel = response.getReturnList().get(0);
            }
        }

        if (requestedContentModel != null && requestedContentModel.getContentStream() != null) {
            InputStream targetStream = new ByteArrayInputStream(requestedContentModel.getContentStream());
            return DefaultStreamedContent.builder()
                    .name(requestedContentModel.getFileName())
                    .contentType("image/jpg")
                    .stream(() -> targetStream)
                    .build();
//            return new DefaultStreamedContent(targetStream, "image/jpg", requestedContentModel.getFileName());
        }
        return null;
    }

    public void openPdf(UnstructuredModel model) {
//        this.pdfToView = this.downloadAttachment(model);
        this.selectedUnstructId = model.getUnstructuredId();
        PrimeFaces.current().ajax().update("attachmentPdfViewerDialog");
        PrimeFaces.current().executeScript("PF('attachmentPdfViewerDialog').show()");
    }

    public void openImage(UnstructuredModel model) {
//        this.pdfToView = this.downloadAttachment(model);
        this.selectedUnstructId = model.getUnstructuredId();
        PrimeFaces.current().ajax().update("fullScreenImageAttachment");
        PrimeFaces.current().executeScript("PF('fullScreenImageAttachment').show()");
    }

    public void deleteAttachment(UnstructuredModel model) {
        if (model != null) {
            UnstructuredRequest request = new UnstructuredRequest();
            request.setRequestActionType("2");
            request.setContext("ATTACHMENTS");
            request.setUnstructuredModel(model);
            request.setObjectId(Integer.valueOf(requestedObjectId));
            request.setUnstructuredId(model.getUnstructuredId());
            UnstructuredHandler handler = new UnstructuredHandler();
            UnstructuredResponse response = handler.executeForManager(request);
            if (response.getErrorCode() == 1000) {
                this.loadContentList();
            }
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getRequestedObjectId() {
        return requestedObjectId;
    }

    public void setRequestedObjectId(String requestedObjectId) {
        this.requestedObjectId = requestedObjectId;
    }

    public ArrayList<UnstructuredModel> getContentList() {
        return contentList;
    }

    public void setContentList(ArrayList<UnstructuredModel> contentList) {
        this.contentList = contentList;
    }

    public String getRequestedModule() {
        return requestedModule;
    }

    public void setRequestedModule(String requestedModule) {
        this.requestedModule = requestedModule;
    }

    public StreamedContent getPdfToView() {
        return pdfToView;
    }

    public void setPdfToView(StreamedContent pdfToView) {
        this.pdfToView = pdfToView;
    }

    public String getSelectedUnstructId() {
        return selectedUnstructId;
    }

    public void setSelectedUnstructId(String selectedUnstructId) {
        this.selectedUnstructId = selectedUnstructId;
    }

    public ArrayList<UnstructuredFolder> getUnstructuredFolders() {
        return unstructuredFolders;
    }

    public void setUnstructuredFolders(ArrayList<UnstructuredFolder> unstructuredFolders) {
        this.unstructuredFolders = unstructuredFolders;
    }

    public UnstructuredFolder getNewFolder() {
        return newFolder;
    }

    public void setNewFolder(UnstructuredFolder newFolder) {
        this.newFolder = newFolder;
    }

    public int getCurrentFolderTreeId() {
        return currentFolderTreeId;
    }

    public void setCurrentFolderTreeId(int currentFolderTreeId) {
        this.currentFolderTreeId = currentFolderTreeId;
    }

    public ArrayList<UnstructuredModel> getDirectoryContentList() {
        return directoryContentList;
    }

    public void setDirectoryContentList(ArrayList<UnstructuredModel> directoryContentList) {
        this.directoryContentList = directoryContentList;
    }

    public ArrayList<BreadCrumbHolder> getBreadCrumbHolder() {
        return breadCrumbHolder;
    }

    public void setBreadCrumbHolder(ArrayList<BreadCrumbHolder> breadCrumbHolder) {
        this.breadCrumbHolder = breadCrumbHolder;
    }

    public ArrayList<UnstructuredFolder> getDirectoryFolders() {
        return directoryFolders;
    }

    public void setDirectoryFolders(ArrayList<UnstructuredFolder> directoryFolders) {
        this.directoryFolders = directoryFolders;
    }

}
