/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.users;

import com.beezer.web.beans.AccessBean;
import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.base.RecordDetailManager;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.handler.UserHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UserModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.UserRequest;
import com.crm.models.responses.UserResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author badry
 */
@ManagedBean(name = "userProfileBean")
@ViewScoped
public class UserProfileBean extends RecordDetailManager {

    private UserModel userModel;
    private String oldPassword;
    private boolean adminConsole;

    @PostConstruct
    public void init() {
        if (super.getRequestParams().get("ADMIN") != null) {
            this.adminConsole = Boolean.valueOf(super.getRequestParams().get("ADMIN"));
        }
        super.setModuleId("11");
        super.loadForms("11", 0);
        super.setMode();
        if (super.getRequestMode().equals(Defines.REQUEST_MODE_ADD)) {
            this.loadBasicUserData();
        }
        else if (super.getRequestMode().equals(Defines.REQUEST_MODE_EDIT)) {
            super.initialize();
        }
    }

    @Override
    public void loadRecord() {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        RequestModel request = new RequestModel();
        FilterManager fm = new FilterManager();
        FilterType fStatus = new FilterType("user_id", super.getRequestParams().get("user"));
        fm.setFilter("AND", fStatus);
        request.setClause(fm.getClause());
        request.setRequestActionType("0");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "UsersMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.recordModel = response.getRecordList().get(0);
                this.loadBasicUserData();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UserProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public void loadBasicUserData() {
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("user_id", "=", super.getRequestParams().get("user")));
            userRequest.setClause(fm.getClause());
            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
                this.userModel = response.getReturnList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UserProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changePassword() {
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("4");
            userRequest.setOldPassword(oldPassword);
            userRequest.setUserModel(userModel);
            if (!GeneralUtils.getLoggedUser().isSuperUser()) {
                if (GeneralUtils.getLoggedUser().getPermissionSet() != null) {
                    if (GeneralUtils.getLoggedUser().getPermissionSet().isAdmin()) {
                        userRequest.setOveridePasswordVerification(true);
                    }
                }
                else {
                    userRequest.setOveridePasswordVerification(false);
                }
            }
            else {
                userRequest.setOveridePasswordVerification(true);
            }

            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Operation successful."));
                PrimeFaces.current().executeScript("PF('changePasswordDialog').hide()");
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Reason: " + response.getErrorMessage()));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UserProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changeEmail() {
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("5");
            userRequest.setUserModel(userModel);
            if (GeneralUtils.getLoggedUser().getPermissionSet().isAdmin()) {
                userRequest.setOveridePasswordVerification(true);
            }
            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
                PrimeFaces.current().executeScript("PF('changeEmailDialog').hide()");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UserProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onLanguageChange(String selectedLang) {
        if (selectedLang.equals("ar")) {
            userModel.setRtl(true);
        }
        else {
            userModel.setRtl(false);
        }
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("8");
            userRequest.setUserModel(userModel);
            if (GeneralUtils.getLoggedUser().isSuperUser()) {
                userRequest.setOveridePasswordVerification(true);
            }
            else {
                if (GeneralUtils.getLoggedUser().getPermissionSet().isAdmin()) {
                    userRequest.setOveridePasswordVerification(true);
                }
            }

            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
//                this.reloadApp();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UserProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void reloadApp() {
        AccessBean ab = new AccessBean();
        ab.setEmail(userModel.getEmail());
        ab.setPassword(userModel.getPassword());
        ab.signIn();
    }

    @Override
    public void save() {
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                this.edit();
                break;
            case Defines.REQUEST_MODE_ADD:
                super.populateDataFromForm();
                PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
                try {
                    UserRequest userRequest = new UserRequest();
                    userRequest.setActionType("7");
                    userRequest.setUserModel(userModel);
                    userRequest.setRecordModel(super.getRecordModel());
                    UserHandler handler = new UserHandler();
                    UserResponse response = handler.execute(userRequest);
                    if (response.getErrorCode() == 1000) {
                        FacesContext.getCurrentInstance().getExternalContext().redirect("users.xhtml");
                    }
                }
                catch (Exception ex) {
                    Logger.getLogger(UserProfileBean.class.getName()).log(Level.SEVERE, null, ex);
                }
//                RequestModel request = new RequestModel();
//                request.setRecordModel(super.getRecordModel());
//                request.setRequestActionType("1");
//                RequestHandler requestHandler = new RequestHandler();
//                ResponseModel response;
//                try {
//                    response = requestHandler.executeRequest(request, "UsersMasterAPI");
//                    if (response.getErrorCode() == 1000) {
//                        super.recordModel = response.getRecordList().get(0);
//                        this.loadBasicUserData();
//                    }
//                }
//                catch (Exception ex) {
//                    Logger.getLogger(UserProfileBean.class.getName()).log(Level.SEVERE, null, ex);
//                }
                PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
                break;
        }

    }

    @Override
    public void edit() {
        super.populateDataFromForm();
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        RequestModel request = new RequestModel();
        request.setRecordModel(super.getRecordModel());
        request.setRequestActionType("3");
        request.setRequestingModule("11");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "UsersMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.loadBasicUserData();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UserProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public boolean isAdminConsole() {
        return adminConsole;
    }

    public void setAdminConsole(boolean adminConsole) {
        this.adminConsole = adminConsole;
    }

}
