/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.orders;

import com.beezer.web.beans.admin.appBuilder.lookups.LookupManagerBean;
import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.deals.DealDetailBean;
import com.beezer.web.handler.LookupHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.beezer.web.models.KanbanRecordModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.lookup.LookupModel;
import com.crm.models.requests.managers.LookupRequest;
import com.crm.models.responses.managers.LookupResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "ordersKanbanBean")
@ViewScoped
public class OrdersKanbanBean extends BeanFramework {

    private String orderId;
    private ArrayList<KanbanRecordModel> orderGroupsList;
    
    @ManagedProperty(value = "#{dealDetailBean}")
    private DealDetailBean dealDetailBean;

    public OrdersKanbanBean() {
    }

    @PostConstruct
    public void init() {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        super.loadFilters("13");
//        this.loadRecordsList();
        this.loadRecordsListForKanban();
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public void loadRecordsList() {
        super.setLazyRecordModel(new LazyRecordModel("OrdersMasterAPI", null, false, LazyRecordModel.RECORD, "13"));
    }

    public void loadRecordsListForKanban() {
        try {
            RequestModel request = new RequestModel();
            request.setLimit(100);
            request.setOffset(0);
            request.setRequestActionType("0");
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "OrdersMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.groupData(response.getRecordList());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(OrdersKanbanBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadRelatedOrder() {
        try {
            RequestModel request = new RequestModel();
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("order_id", this.orderId));
            request.setRequestActionType("0");
            request.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "OrdersMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.setRecordsList(response.getRecordList());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(OrdersKanbanBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void groupData(ArrayList<RecordModel> recordModels) {
        if (recordModels != null) {
            this.orderGroupsList = new ArrayList<>();
            ArrayList<LookupModel> lookupList = null;
            try {
                LookupHandler lh = new LookupHandler();
                LookupRequest request = new LookupRequest();
                request.setLookupId("7");
                request.setActionType("0");
                LookupResponse response = lh.getLookups(request);
                if (response.getErrorCode() == 1000) {
                    lookupList = response.getReturnList();
                }
            }
            catch (Exception ex) {
                Logger.getLogger(LookupManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (lookupList != null) {
                KanbanRecordModel ogm;
                for (LookupModel lm : lookupList) {
                    ogm = new KanbanRecordModel();
                    ogm.setGroupTagName(lm.getName());
                    ogm.setTagId(String.valueOf(lm.getId()));
                    ArrayList<RecordModel> groupData = new ArrayList<>();
                    for (RecordModel rm : recordModels) {
                        if (rm.getFieldValueMap().get("order_status").getCurrentValue().equals(String.valueOf(lm.getId()))) {
                            groupData.add(rm);
                        }
                    }
                    ogm.setGroupData(groupData);
                    this.orderGroupsList.add(ogm);
                }
            }
        }
    }

    public void onCardMove() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String orderDraggedId = params.get("orderId");
        String sourceListId = params.get("sourceListId");
        String targetListId = params.get("targetListId");
        RecordModel modifiedRM = null;
        for (KanbanRecordModel krm : this.orderGroupsList) {
            if (krm.getTagId().equals(sourceListId)) {
                for (RecordModel rm : krm.getGroupData()) {
                    if (rm.getFieldValueMap().get("order_id").getCurrentValue().equals(orderDraggedId)) {
                        modifiedRM = rm;
                        break;
                    }
                }
                if (modifiedRM != null) {
                    krm.getGroupData().remove(modifiedRM);
                }
            }
        }
        if (modifiedRM != null) {
            for (KanbanRecordModel krm : this.orderGroupsList) {
                if (krm.getTagId().equals(targetListId)) {
                    if (krm.getGroupData() != null) {
                        krm.getGroupData().add(modifiedRM);
                        modifiedRM.getFieldValueMap().get("order_status").setCurrentValue(targetListId);
                        break;
                    }
                    else {
                        krm.setGroupData(new ArrayList<>());
                        krm.getGroupData().add(modifiedRM);
                        break;
                    }
                }
            }
            super.setRecordModel(modifiedRM);
            this.edit();
        }
    }
    
    public void onDetailView(RecordModel recordModel){
        dealDetailBean.setRequestedId(recordModel.getFieldValueMap().get("deal_id").getCurrentValue());
        dealDetailBean.loadRecord();
    }

    @Override
    public void search(String api) {
        super.search("OrdersMasterAPI");
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        request.setBulkRecords(new ArrayList<>(super.getSelectedRecords()));
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "OrdersMasterAPI");
            updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(OrdersKanbanBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {

    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        try {
            RequestModel request = new RequestModel();
            request.setLimit(100);
            request.setOffset(0);
            request.setRequestActionType("3");
            request.setRecordModel(super.getRecordModel());
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "OrdersMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.groupData(response.getRecordList());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(OrdersKanbanBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<KanbanRecordModel> getOrderGroupsList() {
        return orderGroupsList;
    }

    public void setOrderGroupsList(ArrayList<KanbanRecordModel> orderGroupsList) {
        this.orderGroupsList = orderGroupsList;
    }

    public DealDetailBean getDealDetailBean() {
        return dealDetailBean;
    }

    public void setDealDetailBean(DealDetailBean dealDetailBean) {
        this.dealDetailBean = dealDetailBean;
    }

    

}
