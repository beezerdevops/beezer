/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.bills;

import com.beezer.web.beans.templates.*;
import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.bills.payments.AddBillPaymentBean;
import com.beezer.web.beans.modules.notes.NotesBean;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.managers.RelationResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "billDetailBean")
@ViewScoped
public class BillDetailBean extends BeanFramework {

    private String requestedId;
    private LinkedHashMap<Integer, RelationManagerModel> relationalMap;
    private LinkedHashMap<Integer, DataBean> relationalBeanMap;
    private LinkedHashMap<Integer, String> relationalKeyMap;
    private LinkedHashMap<Integer, String> relationalTableMap;
    private AddBillPaymentBean addBillPaymentBean;
    private String actionTypeName;

    @ManagedProperty(value = "#{notesBean}")
    private NotesBean notesBean;

    public BillDetailBean() {
    }

    @PostConstruct
    public void init() {
        this.setSelectedRecord();
        super.loadFieldSet(super.getModuleId(), null);
        this.loadRecord();
        this.loadRelationalModules();
        super.fillRecordDetails();
    }

    private void setSelectedRecord() {
        requestedId = super.getRequestParams().get("recordId");
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        ModuleModel mm = super.loadModule(super.getModuleId());
        String moduleName = "";
        if (mm != null) {
            moduleName = mm.getModuleBaseTable() + ".";
        }
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(moduleName + this.getKeyField(), Integer.valueOf(requestedId)));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.recordModel = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BillDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadRelationalModules() {
        RelationRequest relationRequest = new RelationRequest();
        relationRequest.setRelationId(super.getModuleId());
        relationRequest.setActionType("5");
        RelationHandler handler = new RelationHandler();
        RelationResponse relationResponse;
        try {
            relationResponse = handler.relationExecutor(relationRequest);
            if (relationResponse.getErrorCode() == 1000) {
                this.relationalMap = new LinkedHashMap<>();
                this.relationalBeanMap = new LinkedHashMap<>();
                this.relationalKeyMap = new LinkedHashMap<>();
                this.relationalTableMap = new LinkedHashMap<>();
                DataBean db;
                for (RelationManagerModel rmm : relationResponse.getRelationManagerList()) {
                    relationalMap.put(rmm.getModuleId(), rmm);
                    db = new DataBean();
                    db.setModuleId(String.valueOf(rmm.getModuleId()));
                    db.init();
                    relationalBeanMap.put(rmm.getModuleId(), db);
                    relationalKeyMap.put(rmm.getModuleId(), rmm.getMasterColumn());
                    relationalTableMap.put(rmm.getModuleId(), rmm.getMasterTable());
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BillDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addPayment() {
        this.addBillPaymentBean = new AddBillPaymentBean();
        this.addBillPaymentBean.setBillId(requestedId);
        this.addBillPaymentBean.setRequestParams(new HashMap<>());
        this.addBillPaymentBean.getRequestParams().put(Defines.REQUEST_MODE_KEY, Defines.REQUEST_MODE_ADD);
        this.addBillPaymentBean.init();
        this.actionTypeName = "Add";
//        RequestContext.getCurrentInstance().update("addPaymentSidebar");
//        RequestContext.getCurrentInstance().execute("PF('addPaymentSidebar').show()");
        PrimeFaces.current().ajax().update("addPaymentSidebar");
        PrimeFaces.current().executeScript("PF('addPaymentSidebar').hide()");
    }

    public void editPayment() {
        this.addBillPaymentBean = new AddBillPaymentBean();
        this.addBillPaymentBean.setBillId(requestedId);
        this.addBillPaymentBean.setRequestParams(new HashMap<>());
        this.addBillPaymentBean.getRequestParams().put(Defines.REQUEST_MODE_KEY, Defines.REQUEST_MODE_EDIT);
        if (this.relationalBeanMap.get(30).getSelectedRecords() != null && this.relationalBeanMap.get(30).getSelectedRecords().size() == 1) {
            this.addBillPaymentBean.setPaymentId(this.relationalBeanMap.get(30).getSelectedRecords().get(0).getFieldValueMap().get("payment_id").getCurrentValue());
            this.addBillPaymentBean.init();
            this.actionTypeName = "Edit";
//            RequestContext.getCurrentInstance().update("addPaymentSidebar");
//            RequestContext.getCurrentInstance().execute("PF('addPaymentSidebar').show()");
            PrimeFaces.current().ajax().update("addPaymentSidebar");
            PrimeFaces.current().executeScript("PF('addPaymentSidebar').hide()");
        }
    }

    public void onTabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        if (!tabId.equalsIgnoreCase("detailsTab") && !tabId.equalsIgnoreCase("notesTab") && !tabId.equalsIgnoreCase("billTab") && !tabId.equalsIgnoreCase("paymentsTab")) {
            int id = Integer.parseInt(tabId.replaceAll("id", ""));
            DataBean temp = this.relationalBeanMap.get(id);
            String qualifiedColumnName = this.relationalTableMap.get(id) + "." + this.relationalKeyMap.get(id);
            String keyValue = GeneralUtils.getFieldFromRecord(recordModel, this.relationalKeyMap.get(id)).getCurrentValue();
            temp.loadRelatedRecord(qualifiedColumnName, keyValue);
            this.relationalBeanMap.put(id, temp);
//            RequestContext.getCurrentInstance().update("detailsForm:tabHolder");
            PrimeFaces.current().ajax().update("detailsForm:tabHolder");
        }
        else if (tabId.equalsIgnoreCase("notesTab")) {
            notesBean.setRequestedModuleId(super.getModuleId());
            notesBean.setRequestedObjectId(this.requestedId);
            notesBean.init();
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("addBill.xhtml?recordId="
                    + super.getRecordModel().getFieldValueMap().get(super.getKeyField()).getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT + "&moduleId=" + super.getModuleId());
        }
        catch (Exception ex) {
            Logger.getLogger(BillDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public LinkedHashMap<Integer, RelationManagerModel> getRelationalMap() {
        return relationalMap;
    }

    public void setRelationalMap(LinkedHashMap<Integer, RelationManagerModel> relationalMap) {
        this.relationalMap = relationalMap;
    }

    public LinkedHashMap<Integer, DataBean> getRelationalBeanMap() {
        return relationalBeanMap;
    }

    public void setRelationalBeanMap(LinkedHashMap<Integer, DataBean> relationalBeanMap) {
        this.relationalBeanMap = relationalBeanMap;
    }

    public LinkedHashMap<Integer, String> getRelationalKeyMap() {
        return relationalKeyMap;
    }

    public void setRelationalKeyMap(LinkedHashMap<Integer, String> relationalKeyMap) {
        this.relationalKeyMap = relationalKeyMap;
    }

    public LinkedHashMap<Integer, String> getRelationalTableMap() {
        return relationalTableMap;
    }

    public void setRelationalTableMap(LinkedHashMap<Integer, String> relationalTableMap) {
        this.relationalTableMap = relationalTableMap;
    }

    public String getRequestedId() {
        return requestedId;
    }

    public void setRequestedId(String requestedId) {
        this.requestedId = requestedId;
    }

    public NotesBean getNotesBean() {
        return notesBean;
    }

    public void setNotesBean(NotesBean notesBean) {
        this.notesBean = notesBean;
    }

    public AddBillPaymentBean getAddBillPaymentBean() {
        return addBillPaymentBean;
    }

    public void setAddBillPaymentBean(AddBillPaymentBean addBillPaymentBean) {
        this.addBillPaymentBean = addBillPaymentBean;
    }

    public String getActionTypeName() {
        return actionTypeName;
    }

    public void setActionTypeName(String actionTypeName) {
        this.actionTypeName = actionTypeName;
    }

}
