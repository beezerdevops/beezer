/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.bills;

import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author badry
 */
public class BillGenerator {

    private final String billId;
    private RecordModel purchaseOrder;
    private RecordModel billDetails;
    private RecordModel supplierInfo;
    private ArrayList<RecordModel> poDetails;
    private float subtotal;

    public BillGenerator(String billId) {
        this.billId = billId;
        this.getBillDetails();
        this.getPurchaseOrder();
        this.getPODetails();
        this.getSupplierInfo();
    }

    public String generateHtmlInvoice() {
        String template = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<html>"
                + "   <head>"
                + "  <meta charset=\"utf-8\" />"
                + "  <title>Invoice</title>"
                + ""
                + setHtmlStyle()
                + "</head>"
                + ""
                + "<body>"
                + "  <div class=\"invoice-box\">"
                + "    <div class=\"row invoice-header\" style=\"background: #fdffb7\">"
                + "      <div class=\"col-xs-8\">"
                + "        <h1><strong>INVOICE</strong></h1>"
                + "      </div>"
                + "      <div class=\"media-left logo\">"
                + "        <img class=\"media-object logo\" src=\"" + GeneralUtils.getCompany().getCompanyLogo() + "\" />"
                + "      </div>"
                + "    </div>"
                + ""
                + "    <div class=\"row\">"
                + "      <div class=\"col-xs-6\">"
                + "        <h4><strong>SUPPLIER</strong></h4>"
                + "        <ul class=\"media-body list-unstyled details-list\">"
                + "          <li><strong>Company: </strong>" + GeneralUtils.getFieldFromRecord(supplierInfo, "supplier_company").getCurrentValue() + "</li>"
                + "          <li><strong>Phone: </strong>" + GeneralUtils.getFieldFromRecord(supplierInfo, "supplier_phone").getCurrentValue() + "</li>"
                + "          <li><strong>Email: </strong>" + GeneralUtils.getFieldFromRecord(supplierInfo, "supplier_email").getCurrentValue() + "</li>"
                + "          <li><strong>Address: </strong>" + GeneralUtils.getFieldFromRecord(supplierInfo, "supplier_address").getCurrentValue() + "</li>"
                + "        </ul>"
                + "      </div>"
                + ""
                + ""
                + "      <div class=\"col-xs-6 pull-right invoice-details\">"
                + "        <h4><strong>BILL DETAILS</strong></h4>"
                + "        <ul class=\"media-body list-unstyled details-list\">"
                + generateBillDetails()
                + "        </ul>"
                + "      </div>"
                + "    </div>"
                + ""
                + "    <div class=\"row\">"
                + "      <table cellpadding=\"0\" cellspacing=\"0\">"
                + "        <tr class=\"heading\">"
                + "          <td>Item</td>"
                + "          <td>Quantity</td>"
                + "          <td>Unit Price</td>"
                + "          <td>Total Price</td>"
                + "        </tr>"
                + generateItemizedBill()
                + "      </table>"
                + "    </div>"
                + "	"
                + "	<div class=\"row\">"
                + "	<div class=\"col-xs-6 pull-right invoice-details subtotal\">"
                + "        <table>"
                + "		<tbody>"
                + generateTotal()
                + "		</tbody>"
                + "		</table>"
                + "      </div>"
                + "	</div>"
                + "	"
                + "  </div>"
                + "</body>"
                + "</html>";
        return template;
    }

    private String setHtmlStyle() {
        return "<style> "
                + "        body { "
                + "            margin: 0; "
                + "        } "
                + "           "
                + "          .h1, h1 { "
                + "            font-size: 36px; "
                + "        	    margin-top: 20px; "
                + "            margin-bottom: 10px; "
                + "        	    margin: .67em 0; "
                + "        } "
                + "         "
                + "        .h4, h4 { "
                + "            font-size: 18px; "
                + "        	    margin-top: 10px; "
                + "            margin-bottom: 10px; "
                + "        } "
                + "         "
                + "        .row { "
                + "            margin-right: -15px; "
                + "            margin-left: -15px; "
                + "        } "
                + "           "
                + "          .row:after { "
                + "            clear: both; "
                + "        } "
                + "         "
                + "        :after, :before { "
                + "            -webkit-box-sizing: border-box; "
                + "            -moz-box-sizing: border-box; "
                + "            box-sizing: border-box; "
                + "        } "
                + "         "
                + "        .row:after, .row:before { "
                + "            display: table; "
                + "            content: \" \"; "
                + "        } "
                + "         "
                + "        .col-xs-8 { "
                + "            width: 66.66666667%; "
                + "        	float: left; "
                + "        	    position: relative; "
                + "            min-height: 1px; "
                + "            padding-right: 15px; "
                + "            padding-left: 15px; "
                + "        } "
                + "         "
                + "        .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6{ "
                + "        	font-family: inherit; "
                + "            font-weight: 500; "
                + "            line-height: 1.1; "
                + "            color: inherit; "
                + "        	    margin-top: 20px; "
                + "            margin-bottom: 10px; "
                + "        } "
                + "         "
                + "        .col-xs-6 { "
                + "            width: 50%; "
                + "            float: left; "
                + "            position: relative; "
                + "            min-height: 1px; "
                + "            padding-right: 15px; "
                + "            padding-left: 15px; "
                + "        } "
                + "         "
                + "        .media-body { "
                + "            width: 10000px; "
                + "            overflow: hidden; "
                + "            zoom: 1; "
                + "            display: table-cell; "
                + "            vertical-align: top; "
                + "        } "
                + "         "
                + "        .list-unstyled { "
                + "            padding-left: 0; "
                + "            list-style: none; "
                + "        } "
                + "         "
                + "        * { "
                + "            -webkit-box-sizing: border-box; "
                + "            -moz-box-sizing: border-box; "
                + "            box-sizing: border-box; "
                + "        } "
                + "         "
                + "        .pull-right{ "
                + "        float:right; "
                + "        } "
                + "           "
                + "            .invoice-box { "
                + "              max-width: 800px; "
                + "              margin: auto; "
                + "              padding: 30px; "
                + "              padding-top: 0; "
                + "              border: 0px solid #eee; "
                + "              box-shadow: 0 0 10px rgba(0, 0, 0, .15); "
                + "              font-size: 16px; "
                + "              line-height: 24px; "
                + "              font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; "
                + "              color: #555555 !important; "
                + "            } "
                + "             "
                + "            .invoice-header { "
                + "              background-color: #fdffb7; "
                + "              margin-right: -30px; "
                + "              margin-left: -30px; "
                + "            } "
                + "             "
                + "            .invoice-box .row:nth-child(1) { "
                + "              background-color: #fdffb7; "
                + "              margin-right: -30px; "
                + "              margin-left: -30px; "
                + "            } "
                + "             "
                + "            .logo { "
                + "              float: right; "
                + "        	  padding: 6px; "
                + "            } "
                + "             "
                + "            .invoice-box table { "
                + "              width: 100%; "
                + "              line-height: inherit; "
                + "              text-align: left; "
                + "              color: #6e6e6e; "
                + "              font-size: 14px; "
                + "            } "
                + "             "
                + "            .invoice-details { "
                + "              width: 30%; "
                + "            } "
                + "             "
                + "            .details-list { "
                + "              font-size: 121px; "
                + "            } "
                + "             "
                + "            .heading { "
                + "              border-top: 2px solid #ff5858; "
                + "              border-bottom: 2px solid #ff5858; "
                + "              background-color: #fff; "
                + "            } "
                + "             "
                + "            .invoice-box table td { "
                + "              padding: 5px; "
                + "              vertical-align: top; "
                + "            } "
                + "             "
                + "            .invoice-box table tr td:nth-child(2) { "
                + "              text-align: center; "
                + "            } "
                + "             "
                + "            .invoice-box table tr td:nth-child(3) { "
                + "              text-align: center; "
                + "            } "
                + "             "
                + "            .invoice-box table tr td:nth-child(4) { "
                + "              text-align: center; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.top table td { "
                + "              padding-bottom: 20px; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.top table td.title { "
                + "              font-size: 45px; "
                + "              line-height: 45px; "
                + "              color: #333; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.information table td { "
                + "              padding-bottom: 40px; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.heading td { "
                + "              border-top: 2px solid #ff5858; "
                + "              border-bottom: 2px solid #ff5858; "
                + "              font-weight: bold; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.details td { "
                + "              padding-bottom: 20px; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.item td { "
                + "              border-bottom: 1px solid #eee; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.item.last td { "
                + "              border-bottom: none; "
                + "            } "
                + "             "
                + "            .invoice-box table tr.total td:nth-child(4) { "
                + "              border-top: 2px solid #eee; "
                + "              font-weight: bold; "
                + "            } "
                + "             "
                + "            @media only screen and (max-width: 600px) { "
                + "              .invoice-box table tr.top table td { "
                + "                width: 100%; "
                + "                display: block; "
                + "                text-align: center; "
                + "              } "
                + "              .invoice-box table tr.information table td { "
                + "                width: 100%; "
                + "                display: block; "
                + "                text-align: center; "
                + "              } "
                + "            } "
                + "             "
                + "            .rtl { "
                + "              direction: rtl; "
                + "              font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; "
                + "            } "
                + "             "
                + "            .rtl table { "
                + "              text-align: right; "
                + "            } "
                + "             "
                + "            .rtl table tr td:nth-child(2) { "
                + "              text-align: left; "
                + "            } "
                + "        	 "
                + "        	.subtotal table tbody tr td:nth-child(1){ "
                + "        	text-align:right !important; "
                + "        	} "
                + "        	 "
                + "        	.subtotal{ "
                + "        	margin-right: 2em; "
                + "        	} "
                + "        	 "
                + "        	.subtotal table td { "
                + "            padding: 1px; "
                + "        } "
                + "    </style>";
    }

    private String generateBillDetails() {
        return "<li><strong>Number: </strong>" + (GeneralUtils.getFieldFromRecord(this.billDetails, "bill_number") != null ? GeneralUtils.getFieldFromRecord(this.billDetails, "bill_number").getCurrentValue() : "") + "</li>"
                + "<li><strong>Date: </strong>" + (GeneralUtils.getFieldFromRecord(this.billDetails, "added_date") != null ? GeneralUtils.getFieldFromRecord(this.billDetails, "added_date").getCurrentValue() : "") + "</li>"
                + "<li><strong>Payment: </strong>" + (GeneralUtils.getFieldFromRecord(this.billDetails, "payment_type.name") != null ? GeneralUtils.getFieldFromRecord(this.billDetails, "payment_type.name").getCurrentValue() : "") + "</li>";
    }

    private String generateMyCompanyInfo() {
        String myCompanyInfo = GeneralUtils.getCompany().getName() + "<br/>";
        myCompanyInfo = myCompanyInfo + GeneralUtils.getCompany().getAddress() + "<br/>";
        myCompanyInfo = myCompanyInfo + GeneralUtils.getCompany().getCity()
                + ", " + GeneralUtils.getCompany().getCountry() + "<br/>";
        return myCompanyInfo;
    }

    private String generateItemizedBill() {
        String itemizedBill = "";
        subtotal = 0;
        if (poDetails != null) {
            for (RecordModel rm : poDetails) {
                itemizedBill = itemizedBill + "<tr class=\"item\">"
                        + "<td>" + GeneralUtils.getFieldFromRecord(rm, "product_name").getCurrentValue() + "</td>"
                        + "<td>" + GeneralUtils.getFieldFromRecord(rm, "quantity").getCurrentValue() + "</td>"
                        + "<td>" + GeneralUtils.getFieldFromRecord(rm, "goods_raw_price").getCurrentValue() + "</td>"
                        + "<td>" + GeneralUtils.getFieldFromRecord(rm, "net_value").getCurrentValue() + "</td>"
                        + "</tr>";
                subtotal = subtotal + Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "net_value").getCurrentValue());
            }
        }
        return itemizedBill;
    }

    private String generateTotal() {
        String totalAmount = "";
        float vat = subtotal * ((GeneralUtils.getFieldFromRecord(purchaseOrder, "vat_tax").getCurrentValue() != null && !GeneralUtils.getFieldFromRecord(purchaseOrder, "vat_tax").getCurrentValue().isEmpty()) ? Float.valueOf(GeneralUtils.getFieldFromRecord(purchaseOrder, "vat_tax").getCurrentValue()) : 0);
        float salesTax = subtotal * ((GeneralUtils.getFieldFromRecord(purchaseOrder, "sales_tax").getCurrentValue() != null && !GeneralUtils.getFieldFromRecord(purchaseOrder, "sales_tax").getCurrentValue().isEmpty()) ? Float.valueOf(GeneralUtils.getFieldFromRecord(purchaseOrder, "sales_tax").getCurrentValue()) : 0);
        float shipmentCost = (GeneralUtils.getFieldFromRecord(purchaseOrder, "shipping_cost").getCurrentValue() != null && !GeneralUtils.getFieldFromRecord(purchaseOrder, "shipping_cost").getCurrentValue().isEmpty()) ? Float.valueOf(GeneralUtils.getFieldFromRecord(purchaseOrder, "shipping_cost").getCurrentValue()) : 0;
        totalAmount = "<tr>"
                + "		<td><strong>Subtotal: </strong></td>"
                + "		<td>" + subtotal + "</td>"
                + "		</tr>"
                + "		<tr>"
                + "		<td><strong>VAT: </strong></td>"
                + "		<td>" + vat + "</td>"
                + "		</tr>"
                + "		<tr>"
                + "		<td><strong>Tax: </strong></td>"
                + "		<td>" + salesTax + "</td>"
                + "		</tr>"
                + "		<tr>"
                + "		<td><strong>Shipment: </strong></td>"
                + "		<td>" + shipmentCost + "</td>"
                + "		</tr>"
                + "		<tr>"
                + "		<td><strong>Total: </strong></td>"
                + "		<td>" + (subtotal + vat + salesTax + shipmentCost) + "</td>"
                + "		</tr>";
        return totalAmount;
    }

    private void getBillDetails() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule("29");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("bills_base.bill_id", billId));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.billDetails = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BillDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void getPurchaseOrder() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule("27");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("purchase_orders_base.po_id", billDetails.getFieldValueMap().get("po_id").getCurrentValue()));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.purchaseOrder = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BillDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void getPODetails() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule("28");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("purchase_order_details.po_id", billDetails.getFieldValueMap().get("po_id").getCurrentValue()));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.poDetails = response.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BillDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void getSupplierInfo() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule("9");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("suppliers_base.supplier_id", purchaseOrder.getFieldValueMap().get("supplier_id").getCurrentValue()));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.supplierInfo = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BillDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
