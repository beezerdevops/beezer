/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.sales.forecast;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UserModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author badry
 */
@ManagedBean(name = "forecastBean")
@ViewScoped
public class ForecastBean extends BeanFramework {

    private ArrayList<RecordModel> originalForecastData;
    private ArrayList<RecordModel> forecastData;
    private TreeMap<String,String> userMap;
    private ArrayList<String> selectedUsers;
    
    @ManagedProperty(value = "#{salesOrganizationBean.organizationUsers}")
    private ArrayList<UserModel> organizationUsers;

    @PostConstruct
    public void init() {
        initializeFilter();
        this.loadForecast();
    }

    public void loadForecast() {
        try {
            RequestModel request = new RequestModel();
            request.setRequestActionType("0");
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "GetForecastWS");
            if (response.getErrorCode() == 1000) {
                this.forecastData = response.getRecordList();
                this.originalForecastData = response.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ForecastBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void initializeFilter(){
        if(organizationUsers != null){
            userMap = new TreeMap<>();
            selectedUsers = new ArrayList<>();
            for(UserModel um : organizationUsers){
                userMap.put(um.getName(), String.valueOf(um.getUserId()));
                selectedUsers.add(String.valueOf(um.getUserId()));
            }
        }
    }
    
    public void applyFilters(){
        forecastData = new ArrayList<>();
        for(String s : selectedUsers){
            for(RecordModel rm : originalForecastData){
                ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, "user_id");
                if(mfm.getCurrentValue().equals(s)){
                    forecastData.add(rm);
                }
            }
        }
    }

    public float transform(String s) {
        if (s != null && !s.isEmpty()) {
            return Float.valueOf(s);
        }
        return 0;
    }

    public float calculateTotal(String s1, String s2, String s3, String s4) {
        return transform(s1) + transform(s2) + transform(s3) + transform(s4);
    }

    @Override
    public void save() {
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<RecordModel> getForecastData() {
        return forecastData;
    }

    public void setForecastData(ArrayList<RecordModel> forecastData) {
        this.forecastData = forecastData;
    }

    public ArrayList<UserModel> getOrganizationUsers() {
        return organizationUsers;
    }

    public void setOrganizationUsers(ArrayList<UserModel> organizationUsers) {
        this.organizationUsers = organizationUsers;
    }

    public TreeMap<String, String> getUserMap() {
        return userMap;
    }

    public void setUserMap(TreeMap<String, String> userMap) {
        this.userMap = userMap;
    }

    public ArrayList<String> getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(ArrayList<String> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

}
