/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.sales.quota;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.sales.forecast.ForecastConfigurationBean;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author badry
 */
@ManagedBean(name = "qoutaConfigurationBean")
@ViewScoped
public class QoutaConfigurationBean extends BeanFramework {

    private ArrayList<RecordModel> salesPersonnel;
    private String quotaType;
    private ArrayList<RecordModel> quotaConfiguration;

    @ManagedProperty(value = "#{forecastConfigurationBean}")
    private ForecastConfigurationBean forecastConfigurationBean;

    @PostConstruct
    public void init() {
        this.interpretQuotaType();
        this.loadQuotaConfiguration();
    }

    public void interpretQuotaType() {
        if (forecastConfigurationBean != null && forecastConfigurationBean.getForecastBasicConfigurationBean() != null) {
            ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(forecastConfigurationBean.getForecastBasicConfigurationBean().getRecordModel(), "period_type");
            if (mfm != null) {
                switch (mfm.getCurrentValue()) {
                    case "2":
                        this.quotaType = "Q";
                        super.setModuleId("20");
                        break;
                    case "3":
                        this.quotaType = "Y";
                        super.setModuleId("21");
                        break;
                }
            }
        }
    }

    public void loadQuotaConfiguration() {
        try {
            RequestModel request = new RequestModel();
            request.setRequestingModule(super.getModuleId());
            request.setFullTextSearch(false);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.quotaConfiguration = response.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(QoutaConfigurationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void save() {
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setBulkRecords(quotaConfiguration);
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(QoutaConfigurationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<RecordModel> getSalesPersonnel() {
        return salesPersonnel;
    }

    public void setSalesPersonnel(ArrayList<RecordModel> salesPersonnel) {
        this.salesPersonnel = salesPersonnel;
    }

    public String getQuotaType() {
        return quotaType;
    }

    public void setQuotaType(String quotaType) {
        this.quotaType = quotaType;
    }

    public ArrayList<RecordModel> getQuotaConfiguration() {
        return quotaConfiguration;
    }

    public void setQuotaConfiguration(ArrayList<RecordModel> quotaConfiguration) {
        this.quotaConfiguration = quotaConfiguration;
    }

    public ForecastConfigurationBean getForecastConfigurationBean() {
        return forecastConfigurationBean;
    }

    public void setForecastConfigurationBean(ForecastConfigurationBean forecastConfigurationBean) {
        this.forecastConfigurationBean = forecastConfigurationBean;
    }

}
