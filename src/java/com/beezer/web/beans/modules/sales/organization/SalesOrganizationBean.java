/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.sales.organization;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.handler.UserHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UserModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.UserRequest;
import com.crm.models.responses.UserResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.TreeDragDropEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author badry
 */
@ManagedBean(name = "salesOrganizationBean")
@ViewScoped
public class SalesOrganizationBean extends BeanFramework {

    private String requestedModuleId;
    private String requestedObjectId;

    @ManagedProperty(value = "#{usersBean.usersList}")
    private ArrayList<UserModel> usersList;

    @ManagedProperty(value = "#{organizationUsers}") 
    private ArrayList<UserModel> organizationUsers;

    private ArrayList<UserModel> selectedUserList;

    private TreeNode usersTree;
    private TreeNode SalesOrgTree;
    private TreeNode selectedUsers = new DefaultTreeNode("Users", null);
    private TreeNode selectedUsersInSalesOrg = new DefaultTreeNode("Users", null);
    private HashMap<Long, TreeNode> treeMap;

    @PostConstruct
    public void init() {
        this.treeMap = new HashMap<>();
        this.selectedUserList = new ArrayList<>();
        selectedUsers = new DefaultTreeNode("Users", null);
        selectedUsersInSalesOrg = new DefaultTreeNode("Users", null);
        this.loadOrganization();
        initializeUsersTree();
        initializeSalesOrgTree();
//        super.setModuleId("15");
//        super.loadFieldSet("15", null);
//        super.buildRecordModel();
//        this.getRequestorIds();
//        this.loadRecordsList();
    }

    public void loadOrganization() {
        try {
            RequestModel request = new RequestModel();
            request.setRequestingModule("17");
            request.setFullTextSearch(false);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                if (response.getRecordList() != null && !response.getRecordList().isEmpty()) {
                    this.organizationUsers = new ArrayList<>();
                    for (RecordModel rm : response.getRecordList()) {
                        UserModel um = new UserModel();
                        ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, "user_id");
                        um.setUserId(Long.valueOf(mfm.getCurrentValue()));
                        mfm = GeneralUtils.getFieldFromRecord(rm, "user_name");
                        um.setName(mfm.getCurrentValue());
                        organizationUsers.add(um);
                    }
                }
                else {
                    this.organizationUsers = new ArrayList<>();
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(SalesOrganizationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initializeUsersTree() {
        this.usersTree = new DefaultTreeNode("Users", null);
        TreeNode nodeRoot = new DefaultTreeNode("user", "Users", usersTree);
        nodeRoot.setExpanded(true);
        if (organizationUsers != null) {
            boolean valid = true;
            UserModel umRemove = new UserModel();
            for (UserModel umo : organizationUsers) {
                if (usersList != null) {
                    for (UserModel um : usersList) {
                        if (umo.getUserId() == um.getUserId()) {
                            valid = false;
                            umRemove = um;
                            break;
                        }
                    }
                    if (!valid) {
                        usersList.remove(umRemove);
                    }
                }
            }
        }

        for (UserModel um : usersList) {
            TreeNode node = new DefaultTreeNode("user", um, nodeRoot);
        }

    }

    public void initializeSalesOrgTree() {
        this.SalesOrgTree = new DefaultTreeNode("org", null);
        TreeNode nodeRoot = new DefaultTreeNode("user", "Organization", SalesOrgTree);
        nodeRoot.setExpanded(true);
        if (organizationUsers != null) {
            for (UserModel um : organizationUsers) {
                TreeNode node = new DefaultTreeNode("user", um, nodeRoot);
            }
        }

    }

    public ArrayList<RecordModel> constructRecord() {
        if (selectedUserList != null) {
            super.loadFieldSet("17", null);
            RecordModel rm;
            LinkedHashMap<String, ModuleFieldModel> fieldValueMap;
            ArrayList<RecordModel> recordList = new ArrayList<>();
            for (UserModel um : selectedUserList) {
                rm = new RecordModel();
                fieldValueMap = new LinkedHashMap<>();
                for (ModuleFieldModel mfm : super.getFieldsList()) {
                    ModuleFieldModel fModel = new ModuleFieldModel(mfm);
                    switch (mfm.getFieldName()) {
                        case "user_id":
                            fModel.setCurrentValue(String.valueOf(um.getUserId()));
                            break;
                        case "reports_to":
                            fModel.setCurrentValue(String.valueOf(um.getReportsToId()));
                            break;
                    }
                    fieldValueMap.put(fModel.getFieldName(), fModel);
                }
                rm.setFieldValueMap(fieldValueMap);
                recordList.add(rm);
            }
            return recordList;
        }
        return null;
    }

    @Override
    public void save() {
        RequestModel request = new RequestModel();
        request.setRequestingModule("17");
        request.setBulkRecords(this.constructRecord());
        request.setRequestActionType("1");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(SalesOrganizationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void onDragDrop(TreeDragDropEvent event) {
        TreeNode dragNode = event.getDragNode();
        TreeNode dropNode = event.getDropNode();

        UserModel sUser = (UserModel) dragNode.getData();
        if (dropNode.getData() == null || dropNode.getData().toString().equals("Organization")) {
            sUser.setReportsToId(0);
        }
        else {
            sUser.setReportsToId(((UserModel) dropNode.getData()).getUserId());
        }
        selectedUserList.add(sUser);
    }

    public ArrayList<UserModel> getUsersList() {
        return usersList;
    }

    public void setUsersList(ArrayList<UserModel> usersList) {
        this.usersList = usersList;
    }

    public TreeNode getUsersTree() {
        return usersTree;
    }

    public void setUsersTree(TreeNode usersTree) {
        this.usersTree = usersTree;
    }

    public TreeNode getSalesOrgTree() {
        return SalesOrgTree;
    }

    public void setSalesOrgTree(TreeNode SalesOrgTree) {
        this.SalesOrgTree = SalesOrgTree;
    }

    public TreeNode getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(TreeNode selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

    public TreeNode getSelectedUsersInSalesOrg() {
        return selectedUsersInSalesOrg;
    }

    public void setSelectedUsersInSalesOrg(TreeNode selectedUsersInSalesOrg) {
        this.selectedUsersInSalesOrg = selectedUsersInSalesOrg;
    }

    public ArrayList<UserModel> getOrganizationUsers() {
        return organizationUsers;
    }

    public void setOrganizationUsers(ArrayList<UserModel> organizationUsers) {
        this.organizationUsers = organizationUsers;
    }

}
