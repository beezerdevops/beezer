/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.sales.insights;

import com.beezer.web.handler.apps.sales.InsightsHandler;
import com.beezer.web.models.apps.sales.InsightsOptionsModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.requests.apps.sales.InsightsRequest;
import com.crm.models.responses.apps.sales.InsightsResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
//import org.chartistjsf.model.chart.AspectRatio;
//import org.chartistjsf.model.chart.Axis;
//import org.chartistjsf.model.chart.AxisType;
//import org.chartistjsf.model.chart.BarChartModel;
//import org.chartistjsf.model.chart.BarChartSeries;
//import org.chartistjsf.model.chart.PieChartModel;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.bar.BarChartModel;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartModel;

/**
 *
 * @author badry
 */
@ManagedBean(name = "salesInsightsBean")
@ViewScoped
public class SalesInsightsBean {

    private String defaultDateFrom;
    private String defaultDateTo;
    private String defaultDateType;
    private Date filterFromDate;
    private Date filterToDate;
    private InsightsResponse insights;
    private ArrayList<InsightsOptionsModel> insightsOptionsModelList;

    private BarChartModel spaAnalysis;
    private PieChartModel pwlAnalysis;
    private PieChartModel spRevenueAnalysis;
    private String funnelScript;
    private BarChartModel lraAnalysis;

    private boolean overviewScreen = true;
    private boolean spaScreen;
    private boolean funnelScreen;
    private boolean lraScreen;

    @PostConstruct
    public void init() {
        this.generateOptionsMenu();
        this.generateDefaultParameters();
        this.loadInsights();
    }

    public void generateOptionsMenu() {
        this.insightsOptionsModelList = new ArrayList<>();
        InsightsOptionsModel iom = new InsightsOptionsModel();
        iom.setId(1);
        iom.setName("Overview");
        iom.setIcon("insights_ov_ic.png");
        insightsOptionsModelList.add(iom);
        iom = new InsightsOptionsModel();
        iom.setId(2);
        iom.setName("Sales Personnel Effectivnes");
        iom.setIcon("insights_spa_ic.png");
        insightsOptionsModelList.add(iom);
        iom = new InsightsOptionsModel();
        iom.setId(3);
        iom.setName("Funnel Analysis");
        iom.setIcon("insights_funnel_ic.png");
        insightsOptionsModelList.add(iom);
        iom = new InsightsOptionsModel();
        iom.setId(4);
        iom.setName("Loss Reason Analysis");
        iom.setIcon("insights_lra_ic.png");
        insightsOptionsModelList.add(iom);
    }

    public void generateDefaultParameters() {
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        defaultDateFrom = format1.format(cal.getTime());

        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        defaultDateTo = format1.format(cal.getTime());

        defaultDateType = "CREATED";
    }

    public void loadInsights() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            if (filterFromDate != null) {
                defaultDateFrom = format.format(filterFromDate);
            }
            else {
                filterFromDate = format.parse(defaultDateFrom);
            }

            if (filterToDate != null) {
                defaultDateTo = format.format(filterToDate);
            }
            else {
                filterToDate = format.parse(defaultDateTo);
            }
            InsightsRequest insightsRequest = new InsightsRequest();
            insightsRequest.setRequestActionType("0");
            insightsRequest.setDateFrom(defaultDateFrom);
            insightsRequest.setDateTo(defaultDateTo);
            insightsRequest.setDateType(defaultDateType);
            InsightsHandler handler = new InsightsHandler();
            insights = handler.execute(insightsRequest);
            this.buildGraphs();
        }
        catch (Exception ex) {
            Logger.getLogger(SalesInsightsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void buildGraphs() {
        this.buildPWLGraph();
//        this.buildSPAGraph();
//        this.buildSPRGraph();
        this.buildFunnelGraph();
//        this.buildLRAGraph();
    }

    public void buildPWLGraph() {
        pwlAnalysis = new PieChartModel();
        ModuleFieldModel pipelineValue = GeneralUtils.getFieldFromRecord(insights.getPwlAnalysis().get(0), "pipeline_value");
        ModuleFieldModel won = GeneralUtils.getFieldFromRecord(insights.getPwlAnalysis().get(0), "won_value");
        ModuleFieldModel loss = GeneralUtils.getFieldFromRecord(insights.getPwlAnalysis().get(0), "lost_value");

        ChartData data = new ChartData();
        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        List<String> bgColors = new ArrayList<>();
        Random randomGenerator = new Random();
        int R, G, B;

        labels.add("Pipeline Value");
        values.add(Integer.parseInt(pipelineValue.getCurrentValue()));
        R = randomGenerator.nextInt(255) + 1;
        G = randomGenerator.nextInt(255) + 1;
        B = randomGenerator.nextInt(255) + 1;
        bgColors.add("rgb(" + R + "," + G + "," + B + ")");

        labels.add("Won Value");
        values.add(Integer.parseInt(won.getCurrentValue()));
        R = randomGenerator.nextInt(255) + 1;
        G = randomGenerator.nextInt(255) + 1;
        B = randomGenerator.nextInt(255) + 1;
        bgColors.add("rgb(" + R + "," + G + "," + B + ")");

        labels.add("Lost Value");
        values.add(Integer.parseInt(loss.getCurrentValue()));
        R = randomGenerator.nextInt(255) + 1;
        G = randomGenerator.nextInt(255) + 1;
        B = randomGenerator.nextInt(255) + 1;
        bgColors.add("rgb(" + R + "," + G + "," + B + ")");

        dataSet.setData(values);
        dataSet.setBackgroundColor(bgColors);
        data.addChartDataSet(dataSet);
        data.setLabels(labels);
        pwlAnalysis.setData(data);
    }
//

//    public void buildSPAGraph() {
//        spaAnalysis = new BarChartModel();
//        ChartData data = new ChartData();
//
//        BarChartDataSet barDataSet = new BarChartDataSet();
//        barDataSet.setLabel("SPA Report");
//
//        List<Number> values = new ArrayList<>();
//        List<String> labels = new ArrayList<>();
//        
//        BarChartSeries series1 = new BarChartSeries();
//        series1.setName("Pipeline");
//        BarChartSeries series2 = new BarChartSeries();
//        series2.setName("Won");
//        BarChartSeries series3 = new BarChartSeries();
//        series3.setName("Lost");
//
//        if (insights.getSalesKPI() != null) {
//            for (RecordModel rm : insights.getSalesKPI()) {
//                spaAnalysis.addLabel(GeneralUtils.getFieldFromRecord(rm, "user_name").getCurrentValue());
//                if (GeneralUtils.getFieldFromRecord(rm, "pipeline") != null) {
//                    series1.set(Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "pipeline").getCurrentValue()));
//                }
//                else {
//                    series1.set(0);
//                }
//
//                if (GeneralUtils.getFieldFromRecord(rm, "won") != null) {
//                    series2.set(Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "won").getCurrentValue()));
//                }
//                else {
//                    series2.set(0);
//                }
//
//                if (GeneralUtils.getFieldFromRecord(rm, "lost") != null) {
//                    series3.set(Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "lost").getCurrentValue()));
//                }
//                else {
//                    series3.set(0);
//                }
//            }
//
//            Axis xAxis = spaAnalysis.getAxis(AxisType.X);
//            xAxis.setShowGrid(false);
//
//            spaAnalysis.addSeries(series1);
//            spaAnalysis.addSeries(series2);
//            spaAnalysis.addSeries(series3);
//            spaAnalysis.setShowTooltip(true);
//            spaAnalysis.setAnimateAdvanced(true);
//        }
//    }
//
//    public void buildSPRGraph() {
//        spRevenueAnalysis = new PieChartModel();
//        spRevenueAnalysis.setAspectRatio(AspectRatio.GOLDEN_SECTION);
//        if (insights.getSalesPersonnelRevenueAnalysis() != null) {
//            for (RecordModel rm : insights.getSalesPersonnelRevenueAnalysis()) {
//                spRevenueAnalysis.addLabel(GeneralUtils.getFieldFromRecord(rm, "user_name").getCurrentValue());
//                spRevenueAnalysis.set(Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "total_revenue").getCurrentValue()));
//            }
//
//            spRevenueAnalysis.setShowTooltip(true);
//            spRevenueAnalysis.setAnimateAdvanced(true);
//            spRevenueAnalysis.setShowLabel(true);
//        }
//    }
//
    public void buildFunnelGraph() {
        if (insights.getFunnelAnalysis() != null) {
            StringBuilder builder = new StringBuilder();
            builder.append("series: [{name: 'Number of Opportunities', data: [");
            int index = 0;
            for (RecordModel rm : insights.getFunnelAnalysis()) {
                if (index != 0) {
                    builder.append(",");
                }
                builder.append("['");
                builder.append(GeneralUtils.getFieldFromRecord(rm, "stage_name").getCurrentValue());
                builder.append("'");
                builder.append(",");
                builder.append(GeneralUtils.getFieldFromRecord(rm, "stage_count").getCurrentValue());
                builder.append("]");
                index++;
            }
            builder.append("]"); // belongs to closing data
            builder.append("}]");// belongs to closing series
            funnelScript = builder.toString();
        }

    }
//
//    public void buildLRAGraph() {
//        lraAnalysis = new BarChartModel();
//        lraAnalysis.setAspectRatio(AspectRatio.GOLDEN_SECTION);
//        BarChartSeries series1 = new BarChartSeries();
//        series1.setName("Lost Opportunities");
//        if (insights.getLostReasonAnalysis() != null) {
//            for (RecordModel rm : insights.getLostReasonAnalysis()) {
//                lraAnalysis.addLabel(GeneralUtils.getFieldFromRecord(rm, "loss_reason").getCurrentValue());
//                series1.set(Float.valueOf(GeneralUtils.getFieldFromRecord(rm, "lost_count").getCurrentValue()));
//            }
//        }
//        Axis xAxis = lraAnalysis.getAxis(AxisType.X);
//        xAxis.setShowGrid(false);
//
//        lraAnalysis.addSeries(series1);
//        lraAnalysis.setShowTooltip(true);
//        lraAnalysis.setAnimateAdvanced(true);
//    }

    public void onOptionSelect(SelectEvent event) {
        InsightsOptionsModel iom = (InsightsOptionsModel) event.getObject();
        switch (iom.getId()) {
            case 1:
                overviewScreen = true;
                spaScreen = false;
                funnelScreen = false;
                lraScreen = false;
                break;
            case 2:
                overviewScreen = false;
                spaScreen = true;
                funnelScreen = false;
                lraScreen = false;
                break;
            case 3:
                overviewScreen = false;
                spaScreen = false;
                funnelScreen = true;
                lraScreen = false;
                break;
            case 4:
                overviewScreen = false;
                spaScreen = false;
                funnelScreen = false;
                lraScreen = true;
                break;
        }
    }

    public String getDefaultDateFrom() {
        return defaultDateFrom;
    }

    public void setDefaultDateFrom(String defaultDateFrom) {
        this.defaultDateFrom = defaultDateFrom;
    }

    public String getDefaultDateTo() {
        return defaultDateTo;
    }

    public void setDefaultDateTo(String defaultDateTo) {
        this.defaultDateTo = defaultDateTo;
    }

    public String getDefaultDateType() {
        return defaultDateType;
    }

    public void setDefaultDateType(String defaultDateType) {
        this.defaultDateType = defaultDateType;
    }

    public InsightsResponse getInsights() {
        return insights;
    }

    public void setInsights(InsightsResponse insights) {
        this.insights = insights;
    }

    public BarChartModel getSpaAnalysis() {
        return spaAnalysis;
    }

    public void setSpaAnalysis(BarChartModel spaAnalysis) {
        this.spaAnalysis = spaAnalysis;
    }

    public PieChartModel getPwlAnalysis() {
        return pwlAnalysis;
    }

    public void setPwlAnalysis(PieChartModel pwlAnalysis) {
        this.pwlAnalysis = pwlAnalysis;
    }

    public ArrayList<InsightsOptionsModel> getInsightsOptionsModelList() {
        return insightsOptionsModelList;
    }

    public void setInsightsOptionsModelList(ArrayList<InsightsOptionsModel> insightsOptionsModelList) {
        this.insightsOptionsModelList = insightsOptionsModelList;
    }

    public boolean isOverviewScreen() {
        return overviewScreen;
    }

    public void setOverviewScreen(boolean overviewScreen) {
        this.overviewScreen = overviewScreen;
    }

    public boolean isSpaScreen() {
        return spaScreen;
    }

    public void setSpaScreen(boolean spaScreen) {
        this.spaScreen = spaScreen;
    }

    public boolean isFunnelScreen() {
        return funnelScreen;
    }

    public void setFunnelScreen(boolean funnelScreen) {
        this.funnelScreen = funnelScreen;
    }

    public boolean isLraScreen() {
        return lraScreen;
    }

    public void setLraScreen(boolean lraScreen) {
        this.lraScreen = lraScreen;
    }

    public PieChartModel getSpRevenueAnalysis() {
        return spRevenueAnalysis;
    }

    public void setSpRevenueAnalysis(PieChartModel spRevenueAnalysis) {
        this.spRevenueAnalysis = spRevenueAnalysis;
    }

    public String getFunnelScript() {
        return funnelScript;
    }

    public void setFunnelScript(String funnelScript) {
        this.funnelScript = funnelScript;
    }

    public BarChartModel getLraAnalysis() {
        return lraAnalysis;
    }

    public void setLraAnalysis(BarChartModel lraAnalysis) {
        this.lraAnalysis = lraAnalysis;
    }

    public Date getFilterFromDate() {
        return filterFromDate;
    }

    public void setFilterFromDate(Date filterFromDate) {
        this.filterFromDate = filterFromDate;
    }

    public Date getFilterToDate() {
        return filterToDate;
    }

    public void setFilterToDate(Date filterToDate) {
        this.filterToDate = filterToDate;
    }

}
