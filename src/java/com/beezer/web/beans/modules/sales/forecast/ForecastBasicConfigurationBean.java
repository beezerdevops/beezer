/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.sales.forecast;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.templates.AddDataBean;
import com.beezer.web.beans.templates.RecordDetailBean;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author badry
 */
@ManagedBean(name = "forecastBasicConfigurationBean")
@ViewScoped
public class ForecastBasicConfigurationBean extends BeanFramework {

    @PostConstruct
    public void init() {
        super.setModuleId("19");
        super.loadForms("19",0);
        this.loadRecord();
        super.fillFormDetails();
//        super.loadFieldSet("19", null);
//        super.buildRecordModel();
//        this.getRequestorIds();
//        this.loadRecordsList();
    }

    public void loadRecordsList() {
        super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", null, false, LazyRecordModel.RECORD, super.getModuleId()));
    }

    public void constructionCategoryStageMapper() {
    }

    @Override
    public void save() {
        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(ForecastBasicConfigurationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(this.getKeyField(), 1));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.recordModel = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ForecastBasicConfigurationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
