/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.sales.forecast;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.beezer.web.models.SelectionModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author badry
 */
@ManagedBean(name = "forecastConfigurationBean")
@ViewScoped
public class ForecastConfigurationBean extends BeanFramework {

    @ManagedProperty(value = "#{forecastBasicConfigurationBean}")
    private ForecastBasicConfigurationBean forecastBasicConfigurationBean;

    private HashMap<String, SelectionModel> tableSelectionMap;

    @PostConstruct
    public void init() {
        super.setModuleId("18");
        super.loadFieldSet("18", null);
//        super.buildRecordModel();
//        this.getRequestorIds();
        this.buildSelectionMap();
        this.loadRecordsList();
    }

    public void loadRecordsList() {
//        super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", null, false, LazyRecordModel.RECORD, super.getModuleId()));
        try {
            RequestModel request = new RequestModel();
            request.setRequestingModule(super.getModuleId());
            request.setFullTextSearch(false);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.setRecordsList(response.getRecordList());
                for (RecordModel rm : super.getRecordsList()) {
                    ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, "id");
                    ModuleFieldModel mfmSS = GeneralUtils.getFieldFromRecord(rm, "sales_stages");
                    if (mfmSS.getCurrentValue() != null && !mfmSS.getCurrentValue().isEmpty()) {
                        tableSelectionMap.put(mfm.getCurrentValue(), new SelectionModel(mfmSS.getCurrentValue().split(",")));
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ForecastConfigurationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void buildSelectionMap() {
        this.tableSelectionMap = new HashMap<>();
        tableSelectionMap.put("1", new SelectionModel());
        tableSelectionMap.put("2", new SelectionModel());
        tableSelectionMap.put("3", new SelectionModel());
        tableSelectionMap.put("4", new SelectionModel());
    }

    public void populateSalesStageSelection() {
        for (RecordModel rm : super.getLazyRecordModel().getData()) {
            ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, "id");
            GeneralUtils.setFieldValueInRecord(rm, "sales_stages", String.join(",", tableSelectionMap.get(mfm.getCurrentValue()).getMultiSelectedValues()));
        }
    }

    @Override
    public void onMultiSelect(String fieldName, String selection) {
    }

    @Override
    public void save() {
        forecastBasicConfigurationBean.save();
//        super.populateDataFromForm();
        this.populateSalesStageSelection();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
//        request.setRecordModel(super.recordModel);
        request.setBulkRecords(super.getLazyRecordModel().getData());
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(ForecastConfigurationBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ForecastBasicConfigurationBean getForecastBasicConfigurationBean() {
        return forecastBasicConfigurationBean;
    }

    public void setForecastBasicConfigurationBean(ForecastBasicConfigurationBean forecastBasicConfigurationBean) {
        this.forecastBasicConfigurationBean = forecastBasicConfigurationBean;
    }

    public HashMap<String, SelectionModel> getTableSelectionMap() {
        return tableSelectionMap;
    }

    public void setTableSelectionMap(HashMap<String, SelectionModel> tableSelectionMap) {
        this.tableSelectionMap = tableSelectionMap;
    }

}
