/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.tasks;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.TaskHandler;
import com.beezer.web.models.LazyRecordModel;
import com.beezer.web.models.LazyTasksModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UserModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.TaskManagerModel;
import com.crm.models.internal.TaskMemberModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.security.RoleModel;
import com.crm.models.requests.managers.TasksRequest;
import com.crm.models.responses.managers.TasksResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Ahmed El Badry
 */
@ManagedBean(name = "tasksBean")
@ViewScoped
public class TasksBean extends BeanFramework {

    private ArrayList<TaskManagerModel> taskList;
    private TaskManagerModel taskModel;
    private TaskManagerModel selectedTask;

    @ManagedProperty(value = "#{rolesBean.rolesList}")
    private ArrayList<RoleModel> rolesList;

    @ManagedProperty(value = "#{usersBean.usersList}")
    private ArrayList<UserModel> users;
    private ArrayList<UserModel> selectedUsers;

    private LazyTasksModel lazyTasksModel;

    private int openTasksCount;
    private int completedTasksCount;
    private int delayedTasksCount;
    private int inProgressTasks;
    private int totalTaskCount;

    private UserModel reassignUser;

    private boolean myInboxEnabled = true;
    private boolean allInboxEnabled = false;
    private int currentPage = 1;

    @PostConstruct
    public void init() {
        this.taskModel = new TaskManagerModel();
        super.setModuleId("34");
        super.loadFilters("34");
//        super.initializeTableColumnDesigner();
//        this.loadTasksList();
        this.loadTasks();
    }

    public void loadTasksList() {
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("member_id", "=", GeneralUtils.getLoggedUser().getUserId()));
        super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", fm.getClause(), false, LazyRecordModel.RECORD, "34"));
    }

    public boolean isTaskOverdue(String dueDate) {
        if (dueDate != null && !dueDate.isEmpty()) {
            Date enteredDate = null;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(GeneralUtils.getDateFormat(dueDate));
                enteredDate = sdf.parse(dueDate);
            }
            catch (Exception ex) {
                // enteredDate will be null if date="287686";
            }
            Date currentDate = new Date();
            if (enteredDate.after(currentDate)) {
                return false;
            }
            else {
                return true;
            }
        }
        return false;
    }

    public void loadTasks() {
        try {
            //Load Lazy Data
            if (super.getRequestParams().get("origin_page") != null) {
                int originPage = Integer.valueOf(super.getRequestParams().get("origin_page")) * 12;
                currentPage = originPage;
            }

            this.lazyTasksModel = new LazyTasksModel(null, false, false);

            //Load Summary Counters
            TasksRequest tasksRequest = new TasksRequest();
            tasksRequest.setRequestActionType("6");
            TaskHandler handler = new TaskHandler();
            TasksResponse response = handler.execute(tasksRequest);
            if (response.getErrorCode() == 1000) {
                this.completedTasksCount = response.getCompletedTasksCount();
                this.openTasksCount = response.getOpenTasksCount();
                this.delayedTasksCount = response.getDelayedTasksCount();
                this.inProgressTasks = response.getInProgressTasks();
                this.totalTaskCount = response.getTotalRecordCount();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(TasksBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadTeamTasks() {
        try {
            //Load Lazy Data
            this.lazyTasksModel = new LazyTasksModel(null, false, true);

            //Load Summary Counters
            TasksRequest tasksRequest = new TasksRequest();
            tasksRequest.setRequestActionType("6");
            TaskHandler handler = new TaskHandler();
            TasksResponse response = handler.execute(tasksRequest);
            if (response.getErrorCode() == 1000) {
                this.completedTasksCount = response.getCompletedTasksCount();
                this.openTasksCount = response.getOpenTasksCount();
                this.delayedTasksCount = response.getDelayedTasksCount();
                this.inProgressTasks = response.getInProgressTasks();
                this.totalTaskCount = response.getTotalRecordCount();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(TasksBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void search(String api) {
        //Load Lazy Data
//        super.search("GenericMasterAPI"); 
        this.lazyTasksModel = new LazyTasksModel(super.getFTSKeyword(), true, this.allInboxEnabled);
    }

    public void loadCompletedTasks() {
        //Load Lazy Data
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("task_status", "=", "DONE"));
        this.lazyTasksModel = new LazyTasksModel(fm.getClause(), false, this.allInboxEnabled);
        PrimeFaces.current().ajax().update("allTasksList");
    }

    public void loadOpenTasks() {
        //Load Lazy Data
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("task_status", "=", "NSTART"));
        this.lazyTasksModel = new LazyTasksModel(fm.getClause(), false, this.allInboxEnabled);
        PrimeFaces.current().ajax().update("allTasksList");
    }

    public void loadProgressTasks() {
        //Load Lazy Data
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("task_status", "=", "START"));
        this.lazyTasksModel = new LazyTasksModel(fm.getClause(), false, this.allInboxEnabled);
        PrimeFaces.current().ajax().update("allTasksList");
    }

    public void loadDelayedTasks() {
        //Load Lazy Data
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("task_status", "=", "NSTART"));
        Date now = new Date();
        fm.setFilter("AND", new FilterType("due_date", "<", GeneralUtils.getDateFromMillisDBFormat(String.valueOf(now.getTime()))));
        this.lazyTasksModel = new LazyTasksModel(fm.getClause(), false, this.allInboxEnabled);
        PrimeFaces.current().ajax().update("allTasksList");
    }

    public void showTaskConfig() {
        this.taskModel = new TaskManagerModel();
        PrimeFaces.current().ajax().update("addTaskSideBar");
        PrimeFaces.current().executeScript("PF('addTaskSideBar').show()");
    }

    public void saveTask() {
        if (selectedUsers != null) {
            this.taskModel.setTaskMembers(new ArrayList<>());
            for (UserModel um : selectedUsers) {
                TaskMemberModel memberModel = new TaskMemberModel();
                memberModel.setMemberId(um.getUserId());
                memberModel.setMemberType("USER");
                this.taskModel.getTaskMembers().add(memberModel);
            }
        }

        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("1");
        tasksRequest.setTaskManagerModel(taskModel);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        super.updateMessage(tasksResponse.getErrorCode(), tasksResponse.getErrorMessage());
        this.loadTasks();
    }

    public void openRequest(TaskManagerModel task) {
        try {
//            FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
//                    + task.getRequestId()
//                    + "&" + "moduleId=" + task.getRequestModuleId()
//                    + "&" + "RequestMode=" + Defines.REQUEST_MODE_APPROVE
//                    + "&" + "TaskId=" + task.getTaskId()
//                    + "&" + "origin_page=" + this.lazyTasksModel.getCurrentPage());

            FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?"
                    + "recordId=" + task.getRequestId()
                    + "&" + Defines.REQUEST_MODE_KEY + "=" + Defines.REQUEST_MODE_EDIT
                    + "&" + "moduleId=" + task.getRequestModuleId()
                    + "&" + "TaskId=" + task.getTaskId()
                    + "&" + "origin_page=" + this.lazyTasksModel.getCurrentPage());
        }
        catch (IOException ex) {
            Logger.getLogger(TaskDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void claimTaskToUser(TaskManagerModel task) {
        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("7");
        task.setClaimerId(GeneralUtils.getLoggedUser().getUserId());
        tasksRequest.setTaskManagerModel(task);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Task Claimed"));
//            this.manageFlags();
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Failed", "Could not claim task"));
        }
    }

    public boolean shouldDisable(String option, TaskManagerModel task) {
        boolean disableClaim = false;
        boolean disableStart = false;
        boolean disableComplete = false;
        boolean disableGoToRequest = false;
        if (task != null) {
            if (task.getAssignedToType().equalsIgnoreCase("USER")) {
                disableClaim = true;
                if (task.getStatus().equals("NSTART")) {
                    disableComplete = true;
                    disableStart = false;
                }

                if (task.getStatus().equals("START")) {
                    disableComplete = false;
                    disableStart = true;
                }

                if (!task.getStatus().equalsIgnoreCase("NSTART") && task.getStatus().equalsIgnoreCase("DONE")) {
                    disableComplete = true;
                    disableStart = true;
                }
            }

            if (task.getAssignedToType().equalsIgnoreCase("ROLE")) {
                disableClaim = false;
                if (task.getClaimerId() != 0) {
                    if (task.getClaimerId() == GeneralUtils.getLoggedUser().getUserId()) {
                        disableStart = false;
                    }
                    else {
                        disableGoToRequest = true;
                        disableStart = true;
                    }
                    disableClaim = true;
                }
                else {
                    disableClaim = false;
                    disableStart = true;
                }

                if (task.getStatus().equals("NSTART")) {
                    disableComplete = true;
                    if (task.getClaimerId() != 0) {
                        if (task.getClaimerId() == GeneralUtils.getLoggedUser().getUserId()) {
                            disableStart = false;
                        }
                        else {
                            disableGoToRequest = true;
                            disableStart = true;
                        }
                    }
                    else {
                        disableStart = true;
                    }
                }

                if (task.getStatus().equals("START")) {
                    disableComplete = false;
                    disableStart = true;
                }

                if (!task.getStatus().equalsIgnoreCase("NSTART") && task.getStatus().equalsIgnoreCase("DONE")) {
                    disableComplete = true;
                    disableStart = true;
                }
            }
        }

        switch (option) {
            case "GO_TO_REQUEST":
                return disableGoToRequest;
            case "CLAIM":
                return disableClaim;
            case "COMPLETE":
                return disableComplete;
            case "START":
                return disableStart;
        }
        return true;
    }

    public void openReassignTaskDialog(TaskManagerModel task) {
        this.selectedTask = task;
        PrimeFaces.current().ajax().update("reassignTaskToUserDialog");
        PrimeFaces.current().executeScript("PF('reassignTaskToUserDialog').show();");
    }

    public void reassignTask() {
        TaskMemberModel memberModel = new TaskMemberModel();
        memberModel.setMemberId(this.reassignUser.getUserId());
        memberModel.setMemberType("USER");
        this.selectedTask.setTaskMembers(new ArrayList<>());
        this.selectedTask.getTaskMembers().add(memberModel);

        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("10");
        tasksRequest.setTaskManagerModel(selectedTask);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        super.updateMessage(tasksResponse.getErrorCode(), tasksResponse.getErrorMessage());
        if (this.myInboxEnabled) {
            this.loadTasks();
        }
        else {
            this.loadTeamTasks();
        }
    }

    public void switchToMyInbox() {
        this.myInboxEnabled = true;
        this.allInboxEnabled = false;
        this.loadTasks();
    }

    public void switchToAllInbox() {
        this.myInboxEnabled = false;
        this.allInboxEnabled = true;
        this.loadTeamTasks();
    }

    @Override
    public void onRowSelect(SelectEvent event) {
        try {
//            String recordId = "0";
//            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", super.getSelectedRecords().get(0));
//            RecordModel selectedModel = (RecordModel) event.getObject();
//            for (Map.Entry<String, ModuleFieldModel> entry : selectedModel.getFieldValueMap().entrySet()) {
//                if (entry.getValue().isIsKey()) {
//                    recordId = entry.getValue().getCurrentValue();
//                    break;
//                }
//            }
//            FacesContext.getCurrentInstance().getExternalContext().redirect("taskDetails.xhtml?recordId="
//                    + recordId + "&" + "moduleId=" + super.getModuleId());

            TaskManagerModel selectedModel = (TaskManagerModel) event.getObject();
            FacesContext.getCurrentInstance().getExternalContext().redirect("taskDetails.xhtml?recordId="
                    + selectedModel.getTaskId() + "&" + "moduleId=" + super.getModuleId());
        }
        catch (Exception ex) {
            Logger.getLogger(TasksBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void transformRecordToTask(RecordModel rm) {
        if (rm != null) {
            this.taskModel = new TaskManagerModel();
            for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                switch (entry.getValue().getFieldName()) {
                }
            }
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<TaskManagerModel> getTaskList() {
        return taskList;
    }

    public void setTaskList(ArrayList<TaskManagerModel> taskList) {
        this.taskList = taskList;
    }

    public TaskManagerModel getTaskModel() {
        return taskModel;
    }

    public void setTaskModel(TaskManagerModel taskModel) {
        this.taskModel = taskModel;
    }

    public ArrayList<RoleModel> getRolesList() {
        return rolesList;
    }

    public void setRolesList(ArrayList<RoleModel> rolesList) {
        this.rolesList = rolesList;
    }

    public ArrayList<UserModel> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<UserModel> users) {
        this.users = users;
    }

    public ArrayList<UserModel> getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(ArrayList<UserModel> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

    public int getOpenTasksCount() {
        return openTasksCount;
    }

    public void setOpenTasksCount(int openTasksCount) {
        this.openTasksCount = openTasksCount;
    }

    public int getCompletedTasksCount() {
        return completedTasksCount;
    }

    public void setCompletedTasksCount(int completedTasksCount) {
        this.completedTasksCount = completedTasksCount;
    }

    public int getDelayedTasksCount() {
        return delayedTasksCount;
    }

    public void setDelayedTasksCount(int delayedTasksCount) {
        this.delayedTasksCount = delayedTasksCount;
    }

    public int getTotalTaskCount() {
        return totalTaskCount;
    }

    public void setTotalTaskCount(int totalTaskCount) {
        this.totalTaskCount = totalTaskCount;
    }

    public LazyTasksModel getLazyTasksModel() {
        return lazyTasksModel;
    }

    public void setLazyTasksModel(LazyTasksModel lazyTasksModel) {
        this.lazyTasksModel = lazyTasksModel;
    }

    public int getInProgressTasks() {
        return inProgressTasks;
    }

    public void setInProgressTasks(int inProgressTasks) {
        this.inProgressTasks = inProgressTasks;
    }

    public UserModel getReassignUser() {
        return reassignUser;
    }

    public void setReassignUser(UserModel reassignUser) {
        this.reassignUser = reassignUser;
    }

    public boolean isMyInboxEnabled() {
        return myInboxEnabled;
    }

    public void setMyInboxEnabled(boolean myInboxEnabled) {
        this.myInboxEnabled = myInboxEnabled;
    }

    public boolean isAllInboxEnabled() {
        return allInboxEnabled;
    }

    public void setAllInboxEnabled(boolean allInboxEnabled) {
        this.allInboxEnabled = allInboxEnabled;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

}
