/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.tasks;

import com.beezer.web.beans.templates.*;
import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.content.ContentLibraryBean;
import com.beezer.web.beans.modules.notes.NotesBean;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.handler.SmartViewHandler;
import com.beezer.web.handler.TaskHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.TaskManagerModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.internal.smartView.SmartViewModel;
import com.crm.models.internal.smartView.TabsConfig;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.requests.managers.SmartViewRequest;
import com.crm.models.requests.managers.TasksRequest;
import com.crm.models.responses.managers.RelationResponse;
import com.crm.models.responses.managers.SmartViewResponse;
import com.crm.models.responses.managers.TasksResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "taskDetailBean")
@ViewScoped
public class TaskDetailBean extends BeanFramework {

    private String requestedId;
    private LinkedHashMap<Integer, RelationManagerModel> relationalMap;
    private LinkedHashMap<Integer, DataBean> relationalBeanMap;
    private LinkedHashMap<Integer, String> relationalKeyMap;
    private LinkedHashMap<Integer, String> relationalTableMap;

    private SmartViewModel smartViewModel;

    @ManagedProperty(value = "#{notesBean}")
    private NotesBean notesBean;

    @ManagedProperty(value = "#{contentLibraryBean}")
    private ContentLibraryBean contentLibraryBean;

    private boolean disableGoToRequest = true;
    private TaskManagerModel taskModel;

    private boolean disableClaim;
    private boolean disableStart;
    private boolean disableComplete;
    
    private String chosenRichTextField;

    public TaskDetailBean() {
    }

    @PostConstruct
    public void init() {
        this.setSelectedRecord();
        super.loadFieldSet(super.getModuleId(), null);
        this.loadRecord();
        this.disableRequestButton();
        this.loadTaskManager();
        this.loadSmartViewConfigurations();
        this.loadRelationalModules();
        super.fillRecordDetails();
        super.fillAdvancentComponentsValuesForSmartView(smartViewModel);

    }

    private void setSelectedRecord() {
        requestedId = super.getRequestParams().get("recordId");
    }

    public void loadTaskManager() {
        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("0");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("tasks_manager_base.task_id", "=", requestedId));
        tasksRequest.setClause(fm.getClause());
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            if (tasksResponse.getReturnList() != null) {
                this.taskModel = tasksResponse.getReturnList().get(0);
                this.manageFlags();
            }
        }
    }

    public void manageFlags() {
        if (taskModel != null) {
            if (taskModel.getAssignedToType().equalsIgnoreCase("USER")) {
                this.disableClaim = true;
                if (taskModel.getStatus().equals("NSTART")) {
                    this.disableComplete = true;
                    this.disableStart = false;
                }

                if (taskModel.getStatus().equals("START")) {
                    this.disableComplete = false;
                    this.disableStart = true;
                }

                if (!taskModel.getStatus().equalsIgnoreCase("NSTART") && taskModel.getStatus().equalsIgnoreCase("DONE")) {
                    this.disableComplete = true;
                    this.disableStart = true;
                }
            }

            if (taskModel.getAssignedToType().equalsIgnoreCase("ROLE")) {
                this.disableClaim = false;
                if (taskModel.getClaimerId() != 0) {
                    if (taskModel.getClaimerId() == GeneralUtils.getLoggedUser().getUserId()) {
                        this.disableStart = false;
                    }
                    else {
                        this.disableGoToRequest = true;
                        this.disableStart = true;
                    }
                    this.disableClaim = true;
                }
                else {
                    this.disableClaim = false;
                    this.disableStart = true;
                }

                if (taskModel.getStatus().equals("NSTART")) {
                    this.disableComplete = true;
                    if (taskModel.getClaimerId() != 0) {
                        if (taskModel.getClaimerId() == GeneralUtils.getLoggedUser().getUserId()) {
                            this.disableStart = false;
                        }
                        else {
                            this.disableGoToRequest = true;
                            this.disableStart = true;
                        }
                    }
                    else {
                        this.disableStart = true;
                    }
                }

                if (taskModel.getStatus().equals("START")) {
                    this.disableComplete = false;
                    this.disableStart = true;
                }

                if (!taskModel.getStatus().equalsIgnoreCase("NSTART") && taskModel.getStatus().equalsIgnoreCase("DONE")) {
                    this.disableComplete = true;
                    this.disableStart = true;
                }
            }
        }
    }

    public void claimTaskToUser() {
        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("7");
        this.taskModel.setClaimerId(GeneralUtils.getLoggedUser().getUserId());
        tasksRequest.setTaskManagerModel(taskModel);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Task Claimed"));
            this.manageFlags();
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Failed", "Could not claim task"));
        }
    }

    public void unclaimTask() {
        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("7");
        this.taskModel.setClaimerId(0);
        tasksRequest.setTaskManagerModel(taskModel);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Task Unclaimed"));
            this.manageFlags();
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Failed", "Could not unclaim task"));
        }
    }

    public void startTask() {
        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("3");
        taskModel.setStatus("START");
        tasksRequest.setTaskManagerModel(taskModel);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Task Started"));
            this.manageFlags();
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Failed", "Could not start task"));
        }
    }

    public void closeTask() {
        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("3");
        taskModel.setStatus("DONE");
        tasksRequest.setTaskManagerModel(taskModel);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Task Completed"));
            this.manageFlags();
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Failed", "Could not update task"));
        }
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        ModuleModel mm = super.loadModule(super.getModuleId());
        String moduleName = "";
        if (mm != null) {
            moduleName = mm.getModuleBaseTable() + ".";
        }
        FilterManager fm = new FilterManager();
        if (requestedId != null) {
            fm.setFilter("AND", new FilterType(moduleName + this.getKeyField(), Integer.valueOf(requestedId)));
            request.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response;
            try {
                response = requestHandler.executeRequest(request, "GenericMasterAPI");
                if (response.getErrorCode() == 1000) {
                    super.recordModel = response.getRecordList().get(0);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(TaskDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void loadRelationalModules() {
        RelationRequest relationRequest = new RelationRequest();
        relationRequest.setRelationId(super.getModuleId());
        relationRequest.setActionType("5");
        RelationHandler handler = new RelationHandler();
        RelationResponse relationResponse;
        try {
            relationResponse = handler.relationExecutor(relationRequest);
            if (relationResponse.getErrorCode() == 1000) {
                this.relationalMap = new LinkedHashMap<>();
                this.relationalBeanMap = new LinkedHashMap<>();
                this.relationalKeyMap = new LinkedHashMap<>();
                this.relationalTableMap = new LinkedHashMap<>();
                DataBean db;
                for (RelationManagerModel rmm : relationResponse.getRelationManagerList()) {
                    relationalMap.put(rmm.getModuleId(), rmm);
                    db = new DataBean();
                    db.setModuleId(String.valueOf(rmm.getModuleId()));
                    db.setRelationView(true);
                    db.init();
                    relationalBeanMap.put(rmm.getModuleId(), db);
                    relationalKeyMap.put(rmm.getModuleId(), rmm.getMasterColumn());
                    relationalTableMap.put(rmm.getModuleId(), rmm.getMasterTable());
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(TaskDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadSmartViewConfigurations() {
        SmartViewHandler smartViewHandler = new SmartViewHandler();
        SmartViewRequest request = new SmartViewRequest();
        request.setRequestActionType("0");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("module_id", "=", super.getModuleId()));
        request.setClause(fm.getClause());
        SmartViewResponse response = smartViewHandler.executeRequest(request);
        if (response.getErrorCode() == 1000) {
            if (response.getReturnList() != null && !response.getReturnList().isEmpty()) {
                smartViewModel = response.getReturnList().get(0);
                if (smartViewModel != null) {
                    if (smartViewModel.getTabConfigHolder() != null && smartViewModel.getTabConfigHolder().getTabsConfig() != null) {
                        for (TabsConfig tc : smartViewModel.getTabConfigHolder().getTabsConfig()) {
                            if (tc.getFieldsLayoutList() != null) {
                                for (FieldsLayoutModel flm : tc.getFieldsLayoutList()) {
                                    super.buildAdvancedComponentsMap(flm);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void onTabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        if (!tabId.equalsIgnoreCase("detailsTab") && !tabId.equalsIgnoreCase("notesTab") && !tabId.equalsIgnoreCase("attachmentsTab")) {
            if (!tabId.contains("smartTab_")) {
                int id = Integer.parseInt(tabId.replaceAll("id", ""));
                DataBean temp = this.relationalBeanMap.get(id);
                String qualifiedColumnName = this.relationalTableMap.get(id) + "." + this.relationalKeyMap.get(id);
                String keyValue = GeneralUtils.getFieldFromRecord(recordModel, this.relationalMap.get(id).getDetailColumn()).getCurrentValue();
                temp.loadRelatedRecord(qualifiedColumnName, keyValue);
                temp.loadFilters(String.valueOf(id));
                temp.setKeyName(qualifiedColumnName);
                temp.setKeyValue(keyValue);
                this.relationalBeanMap.put(id, temp);
//                RequestContext.getCurrentInstance().update("detailsForm:tabHolder");
                PrimeFaces.current().ajax().update("detailsForm:tabHolder");
            }
            else {
//                RequestContext.getCurrentInstance().update("detailsForm:tabHolder");
                PrimeFaces.current().ajax().update("detailsForm:tabHolder");
            }
        }
        else if (tabId.equalsIgnoreCase("notesTab")) {
            notesBean.setRequestedModuleId(super.getModuleId());
            notesBean.setRequestedObjectId(this.requestedId);
            notesBean.init();
        }
        else if (tabId.equalsIgnoreCase("attachmentsTab")) {
            contentLibraryBean.setRequestedModule(super.getModuleId());
            contentLibraryBean.setRequestedObjectId(this.requestedId);
            contentLibraryBean.init();
        }
    }

    public void disableRequestButton() {
        if (GeneralUtils.getFieldFromRecord(recordModel, "request_id") != null
                && GeneralUtils.getFieldFromRecord(recordModel, "request_id").getCurrentValue() != null
                && GeneralUtils.getFieldFromRecord(recordModel, "request_module_id") != null
                && GeneralUtils.getFieldFromRecord(recordModel, "request_module_id").getCurrentValue() != null
                && !GeneralUtils.getFieldFromRecord(recordModel, "request_id").getCurrentValue().equalsIgnoreCase("0")
                && !GeneralUtils.getFieldFromRecord(recordModel, "request_module_id").getCurrentValue().equalsIgnoreCase("0")) {
            disableGoToRequest = false;
        }
    }

    public void openRequest() {
        if (GeneralUtils.getFieldFromRecord(recordModel, "request_id") != null
                && GeneralUtils.getFieldFromRecord(recordModel, "request_id").getCurrentValue() != null
                && GeneralUtils.getFieldFromRecord(recordModel, "request_module_id") != null
                && GeneralUtils.getFieldFromRecord(recordModel, "request_module_id").getCurrentValue() != null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                        + GeneralUtils.getFieldFromRecord(recordModel, "request_id").getCurrentValue()
                        + "&" + "moduleId=" + GeneralUtils.getFieldFromRecord(recordModel, "request_module_id").getCurrentValue()
                        + "&" + "RequestMode=" + Defines.REQUEST_MODE_APPROVE
                        + "&" + "TaskId=" + GeneralUtils.getFieldFromRecord(recordModel, "task_id").getCurrentValue());
            }
            catch (IOException ex) {
                Logger.getLogger(TaskDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?recordId="
                    + super.getRecordModel().getFieldValueMap().get(super.getKeyField()).getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT + "&moduleId=" + super.getModuleId());
        }
        catch (Exception ex) {
            Logger.getLogger(TaskDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public LinkedHashMap<Integer, RelationManagerModel> getRelationalMap() {
        return relationalMap;
    }

    public void setRelationalMap(LinkedHashMap<Integer, RelationManagerModel> relationalMap) {
        this.relationalMap = relationalMap;
    }

    public LinkedHashMap<Integer, DataBean> getRelationalBeanMap() {
        return relationalBeanMap;
    }

    public void setRelationalBeanMap(LinkedHashMap<Integer, DataBean> relationalBeanMap) {
        this.relationalBeanMap = relationalBeanMap;
    }

    public LinkedHashMap<Integer, String> getRelationalKeyMap() {
        return relationalKeyMap;
    }

    public void setRelationalKeyMap(LinkedHashMap<Integer, String> relationalKeyMap) {
        this.relationalKeyMap = relationalKeyMap;
    }

    public LinkedHashMap<Integer, String> getRelationalTableMap() {
        return relationalTableMap;
    }

    public void setRelationalTableMap(LinkedHashMap<Integer, String> relationalTableMap) {
        this.relationalTableMap = relationalTableMap;
    }

    public String getRequestedId() {
        return requestedId;
    }

    public void setRequestedId(String requestedId) {
        this.requestedId = requestedId;
    }

    public NotesBean getNotesBean() {
        return notesBean;
    }

    public void setNotesBean(NotesBean notesBean) {
        this.notesBean = notesBean;
    }

    public SmartViewModel getSmartViewModel() {
        return smartViewModel;
    }

    public void setSmartViewModel(SmartViewModel smartViewModel) {
        this.smartViewModel = smartViewModel;
    }

    public ContentLibraryBean getContentLibraryBean() {
        return contentLibraryBean;
    }

    public void setContentLibraryBean(ContentLibraryBean contentLibraryBean) {
        this.contentLibraryBean = contentLibraryBean;
    }

    public boolean isDisableGoToRequest() {
        return disableGoToRequest;
    }

    public void setDisableGoToRequest(boolean disableGoToRequest) {
        this.disableGoToRequest = disableGoToRequest;
    }

    public TaskManagerModel getTaskModel() {
        return taskModel;
    }

    public void setTaskModel(TaskManagerModel taskModel) {
        this.taskModel = taskModel;
    }

    public boolean isDisableClaim() {
        return disableClaim;
    }

    public void setDisableClaim(boolean disableClaim) {
        this.disableClaim = disableClaim;
    }

    public boolean isDisableStart() {
        return disableStart;
    }

    public void setDisableStart(boolean disableStart) {
        this.disableStart = disableStart;
    }

    public boolean isDisableComplete() {
        return disableComplete;
    }

    public void setDisableComplete(boolean disableComplete) {
        this.disableComplete = disableComplete;
    }

    public String getChosenRichTextField() {
        return chosenRichTextField;
    }

    public void setChosenRichTextField(String chosenRichTextField) {
        this.chosenRichTextField = chosenRichTextField;
    }

}
