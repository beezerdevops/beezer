/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.reports;

import com.beezer.web.handler.DocTemplateHandler;
import com.crm.models.internal.docTemplates.DocTemplateModel;
import com.crm.models.internal.docTemplates.TemplateFieldObject;
import com.crm.models.internal.workflow.activities.DocTemplateParameter;
import com.crm.models.requests.DocTemplateRequest;
import com.crm.models.responses.DocTemplateResponse;
import freemarker.core.TemplateElement;
import freemarker.ext.beans.StringModel;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ahmed El Badry Sep 10, 2020
 */
public class DocGenComponentManager {

    public ArrayList<DocTemplateParameter> loadTemplateVariables(int templateId) {
        if (templateId != 0) {
            ArrayList<DocTemplateParameter> paramList = new ArrayList<>();
            DocTemplateRequest request = new DocTemplateRequest();
            request.setRequestActionType("4");
            request.setTemplateId(templateId);
            DocTemplateHandler handler = new DocTemplateHandler();
            DocTemplateResponse response = handler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
                if (response.getReturnList() != null) {
                    DocTemplateModel templateModel = response.getReturnList().get(0);
                    if (templateModel != null && templateModel.getTemplateConfig() != null && !templateModel.getTemplateConfig().isEmpty()) {
                        Configuration cfg = new Configuration();
                        cfg.setDefaultEncoding("UTF-8");
                        try {
                            Template t = new Template("name", new StringReader(templateModel.getTemplateConfig()), cfg);
                            Set<TemplateFieldObject> result = this.referenceSet(t);
                            if (result != null) {
                                for (TemplateFieldObject param : result) {
                                    DocTemplateParameter dtp = new DocTemplateParameter();
                                    dtp.setKeyName(param.getName());
                                    dtp.setKeyValue("");
                                    dtp.setType(param.getType());
                                    paramList.add(dtp);
                                }
                                return paramList;
                            }
                        }
                        catch (Exception ex) {
                            Logger.getLogger(DocGenComponentManager.class.getName()).log(Level.SEVERE, null, ex);
                            return null;
                        }
                    }
                }
            }
            else {
                return null;
            }
        }
        return null;
    }

    public ArrayList<DocTemplateParameter> loadTemplateParametersList(String template) {
        ArrayList<DocTemplateParameter> paramList = new ArrayList<>();
        Configuration cfg = new Configuration();
        cfg.setDefaultEncoding("UTF-8");
        try {
            template = template.replace("<!--", "<");
            template = template.replace("-->", ">");
            template = template.replace("&#39;", "'");
            Template t = new Template("name", new StringReader(template), cfg);
            Set<TemplateFieldObject> result = this.referenceSet(t);
            if (result != null) {
                for (TemplateFieldObject param : result) {
                    DocTemplateParameter dtp = new DocTemplateParameter();
                    dtp.setKeyName(param.getName());
                    dtp.setKeyValue("");
                    dtp.setType(param.getType());
                    paramList.add(dtp);
                }
                return paramList;
            }
        }
        catch (Exception ex) {
            Logger.getLogger(DocGenComponentManager.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    public ArrayList<DocTemplateParameter> loadVariablesForDocxt(int templateId) {
        DocTemplateRequest request = new DocTemplateRequest();
        request.setRequestActionType("5");
        request.setTemplateId(templateId);
        DocTemplateHandler handler = new DocTemplateHandler();
        DocTemplateResponse response = handler.executeRequest(request);
        if (response.getErrorCode() == 1000 && response.getReturnList() != null && !response.getReturnList().isEmpty()) {
            ArrayList<DocTemplateParameter> paramList = response.getReturnList().get(0).getParameterList();
            return paramList;
        }
        else {
            return null;
        }
    }

    private Set<TemplateFieldObject> referenceSet(Template template) throws TemplateModelException {
        Set<TemplateFieldObject> result = new HashSet<>();
        TemplateElement rootTreeNode = template.getRootTreeNode();
        for (int i = 0; i < rootTreeNode.getChildCount(); i++) {
            TemplateModel templateModel = rootTreeNode.getChildNodes().get(i);
            if (!(templateModel instanceof StringModel)) {
                continue;
            }
            Object wrappedObject = ((StringModel) templateModel).getWrappedObject();
            if ("IteratorBlock".equals(wrappedObject.getClass().getSimpleName())) {
                try {
                    Object expression = getInternalState(wrappedObject, "listedExp");
                    TemplateFieldObject to = new TemplateFieldObject();
                    to.setName(getInternalState(expression, "name").toString());
                    to.setType("LIST");
                    result.add(to);
                }
                catch (Exception ignored) {
                    int y = 0;
                }
            }
            else if ("DollarVariable".equals(wrappedObject.getClass().getSimpleName())) {
                try {
                    Object expression = getInternalState(wrappedObject, "expression");
                    switch (expression.getClass().getSimpleName()) {
                        case "Identifier":
                            TemplateFieldObject toIdentifier = new TemplateFieldObject();
                            toIdentifier.setName(getInternalState(expression, "name").toString());
                            toIdentifier.setType("STRING");
                            result.add(toIdentifier);
                            break;
                        case "DefaultToExpression":
                            TemplateFieldObject toDTE = new TemplateFieldObject();
                            toDTE.setName(getInternalState(expression, "lho").toString());
                            toDTE.setType("STRING");
                            result.add(toDTE);
                            break;
                        case "BuiltinVariable":
                            break;
                        default:
                            throw new IllegalStateException("Unable to introspect variable");
                    }
                }
                catch (NoSuchFieldException | IllegalAccessException e) {
                    throw new TemplateModelException("Unable to reflect template model");
                }
            }
        }
        return result;
    }

    private static Object getInternalState(Object o, String fieldName) throws NoSuchFieldException, IllegalAccessException {
        java.lang.reflect.Field field = o.getClass().getDeclaredField(fieldName);
        boolean wasAccessible = field.isAccessible();
        try {
            field.setAccessible(true);
            return field.get(o);
        }
        finally {
            field.setAccessible(wasAccessible);
        }
    }
}
