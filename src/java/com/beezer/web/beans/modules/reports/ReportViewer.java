/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.reports;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.DocumentUtilsHandler;
import com.beezer.web.handler.ReportHandler;
import com.beezer.web.models.LazyReportRecordModel;
import com.beezer.web.utils.GeneralUtils;
import com.beezer.web.utils.RecordAggregateFunctions;
import com.beezer.web.utils.cube.CubeProcessor;
import com.beezl.interpreter.BEEZLInterperter;
import com.crm.models.global.reports.AggregatModel;
import com.crm.models.global.reports.ComparitiveDatasetResult;
import com.crm.models.global.reports.CustomReportConfig;
import com.crm.models.global.reports.ReportModel;
import com.crm.models.internal.GlobalVariable;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.docTemplates.DynamicTemplateReference;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.workflow.activities.DocTemplateParameter;
import com.crm.models.requests.ReportRequest;
import com.crm.models.requests.pdf.DocumentUtilRequest;
import com.crm.models.responses.ReportResponse;
import com.crm.models.responses.pdf.DocumentUtilResponse;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.lang3.math.NumberUtils;
import org.primefaces.component.export.ExcelOptions;
import org.primefaces.component.export.PDFOptions;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.axes.cartesian.CartesianScales;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearAxes;
import org.primefaces.model.charts.axes.cartesian.linear.CartesianLinearTicks;
import org.primefaces.model.charts.bar.BarChartDataSet;
import org.primefaces.model.charts.bar.BarChartModel;
import org.primefaces.model.charts.bar.BarChartOptions;
import org.primefaces.model.charts.donut.DonutChartDataSet;
import org.primefaces.model.charts.donut.DonutChartModel;
import org.primefaces.model.charts.line.LineChartDataSet;
import org.primefaces.model.charts.line.LineChartModel;
import org.primefaces.model.charts.line.LineChartOptions;
import org.primefaces.model.charts.optionconfig.legend.Legend;
import org.primefaces.model.charts.optionconfig.legend.LegendLabel;
import org.primefaces.model.charts.optionconfig.title.Title;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartModel;

/**
 *
 * @author badry
 */
@ManagedBean(name = "reportViewer")
@ViewScoped
public class ReportViewer extends BeanFramework {

    private String reportId;
    private ReportModel reportModel;
    private ArrayList<RecordModel> reportData;
    private ExcelOptions excelOpt;
    private PDFOptions pdfOpt;

    private BarChartModel barChartModel;
    private PieChartModel pieChartModel;
    private LineChartModel lineModel;
    private DonutChartModel donutModel;

    private String barChartConfig;
    private ArrayList<String> pivotHeaderValues;
    private ArrayList<String> pivotRowValues;
    private ArrayList<RecordModel> pivotReportData;

    private ArrayList<ComparitiveDatasetResult> comparativeDatasetResults;
    private String customLinkStyling;
    private String customCssStyling;

    private StreamedContent customPdfReport;

    private LazyReportRecordModel lazyReportRecordModel;

    private HashMap<String, ArrayList<String>> colorMap;
    private boolean fromSmartView;

    @PostConstruct
    public void init() {
        this.colorMap = new HashMap<>();
        this.buildColorMap();
        this.pivotReportData = new ArrayList<>();
        this.pivotHeaderValues = new ArrayList<>();
        this.pivotRowValues = new ArrayList<>();
        this.loadRecord();
        this.getRecordData(false);
        this.customizationOptions();
    }

    public void initializeForPreview(ReportModel reportModel) {
        this.colorMap = new HashMap<>();
        this.buildColorMap();
        this.pivotReportData = new ArrayList<>();
        this.pivotHeaderValues = new ArrayList<>();
        this.pivotRowValues = new ArrayList<>();
        this.initializeRecord(reportModel);
        this.getRecordData(true);
        this.customizationOptions();
    }

    public void initializeRecord(ReportModel reportModel) {
        this.reportModel = reportModel;
        if (reportModel != null) {
            if (reportModel.getChartColorPallete() == null || reportModel.getChartColorPallete().isEmpty()) {
                reportModel.setChartColorPallete("JUST_BLUE");
            }
            if (reportModel.getAggregatesList() != null) {
                for (AggregatModel aggregatModel : reportModel.getAggregatesList()) {
                    ModuleFieldModel model;
                    model = new ModuleFieldModel();
                    model.setFieldName(aggregatModel.getAlias());
                    model.setFieldLabel(aggregatModel.getAlias());
                    model.setFieldType("TEXT");
                    model.setFieldValues(null);
                    model.setIsCustom(false);
                    model.setIsKey(false);
                    model.setIsMandatory(false);
                    model.setIsVisible(true);
                    if (reportModel.getFieldList() != null) {
                        reportModel.getFieldList().add(model);
                    }
                    else {
                        reportModel.setFieldList(new ArrayList<>());
                        reportModel.getFieldList().add(model);
                    }
                }
            }

            if (reportModel.getCustomFields() != null && !reportModel.getCustomFields().isEmpty()) {
                for (String colName : reportModel.getCustomFields()) {
                    ModuleFieldModel cmfm = new ModuleFieldModel();
                    cmfm.setFieldName(colName);
                    cmfm.setFieldLabel(colName);
                    cmfm.setFieldType("TEXT");
                    cmfm.setVirtual(true);
                    reportModel.getFieldList().add(cmfm);

                }
            }
        }
    }

    private void buildColorMap() {
        ArrayList<String> allBlueList = new ArrayList<>(Arrays.asList(Defines.allBlue.split("\\s*,\\s*")));
        ArrayList<String> retroList = new ArrayList<>(Arrays.asList(Defines.retro.split("\\s*,\\s*")));
        ArrayList<String> blueYellowList = new ArrayList<>(Arrays.asList(Defines.blueYellow.split("\\s*,\\s*")));
        ArrayList<String> orangePurpleList = new ArrayList<>(Arrays.asList(Defines.orangePurple.split("\\s*,\\s*")));
        ArrayList<String> aquaList = new ArrayList<>(Arrays.asList(Defines.aqua.split("\\s*,\\s*")));
        ArrayList<String> blackPinkList = new ArrayList<>(Arrays.asList(Defines.blackPink.split("\\s*,\\s*")));

        this.colorMap.put("JUST_BLUE", allBlueList);
        this.colorMap.put("RETRO", retroList);
        this.colorMap.put("BLUE_YELLOW", blueYellowList);
        this.colorMap.put("ORANGE_PURPLE", orangePurpleList);
        this.colorMap.put("BLACK_PINK", blackPinkList);
        this.colorMap.put("AQUA", aquaList);

    }

    public void customizationOptions() {
        excelOpt = new ExcelOptions();
        excelOpt.setFacetBgColor("#2F3C45");
        excelOpt.setFacetFontSize("12");
        excelOpt.setFacetFontColor("#FFFFFF");
        excelOpt.setFacetFontStyle("BOLD");
        excelOpt.setCellFontColor("#000000");
        excelOpt.setCellFontSize("8");

        pdfOpt = new PDFOptions();
        pdfOpt.setFacetBgColor("#2F3C45");
        pdfOpt.setFacetFontColor("#FFFFFF");
        pdfOpt.setFacetFontStyle("BOLD");
        pdfOpt.setFacetFontSize("10");
        pdfOpt.setCellFontSize("8");
        pdfOpt.setCellFontColor("#000000");
    }

    public void getRecordData(boolean preview) {
        if (reportModel.getReportDetails().getFieldValueMap().get("report_type").getCurrentValue().equals("3")) {
            if (this.reportModel.getCustomReportConfig() != null) {
                if (!preview) {
                    if (!fromSmartView) {
                        this.reportModel.getCustomReportConfig().setHtmlContent(this.evaluateCustomReport(this.reportModel.getCustomReportConfig()));
                    }
                }
            }
        }
        else {
            try {
                if (reportModel.getReportDetails().getFieldValueMap().get("report_type").getCurrentValue().equals("1")) {
                    if (reportModel.isEnableCustomReport()) {
                        this.reportModel.getCustomReportConfig().setHtmlContent(this.evaluateDetailedReportWithCustomization(this.reportModel.getCustomReportConfig()));
                        return;
                    }
                    this.lazyReportRecordModel = new LazyReportRecordModel(reportModel);
                    if (reportModel.isEnablePivotView() && reportModel.getPivotConfiguration() != null) {
                        if (reportModel.getPivotConfiguration().getHeaderColumnFieldName() != null
                                && !reportModel.getPivotConfiguration().getHeaderColumnFieldName().isEmpty()) {
                            for (RecordModel rm : reportData) {
                                ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, reportModel.getPivotConfiguration().getHeaderColumnFieldName());
                                ModuleFieldModel mfmRow = GeneralUtils.getFieldFromRecord(rm, reportModel.getPivotConfiguration().getRowConfiguration());

                                if (mfm != null) {
                                    if (!this.pivotHeaderValues.contains(mfm.getCurrentValue())) {
                                        this.pivotHeaderValues.add(mfm.getCurrentValue());
                                    }
                                }

                                if (mfmRow != null) {
                                    if (!this.pivotRowValues.contains(mfmRow.getCurrentValue())) {
                                        this.pivotRowValues.add(mfmRow.getCurrentValue());
                                    }
                                }

                                if (mfmRow != null) {
                                    if (!pivotReportData.isEmpty()) {
                                        for (RecordModel pivRecord : pivotReportData) {
                                            ModuleFieldModel mfmPivRow = GeneralUtils.getFieldFromRecord(pivRecord, reportModel.getPivotConfiguration().getRowConfiguration());
                                            if (mfmPivRow != null) {
                                                if (mfmPivRow.getCurrentValue() != null && !mfmPivRow.getCurrentValue().equals(mfmRow.getCurrentValue())) {
                                                    pivotReportData.add(rm);
                                                }
                                            }
                                            else {
                                                pivotReportData.add(rm);
                                            }
                                        }
                                    }
                                    else {
                                        pivotReportData.add(rm);
                                    }
                                }
                            }
                        }
                    }
                }
                else {
                    ReportRequest request = new ReportRequest();
                    request.setReportModel(reportModel);
                    request.setLimit(1000);
                    request.setOffset(0);
                    request.setRequestActionType("0");
                    request.setGetData(true);
                    ReportHandler reportHandler = new ReportHandler();
                    ReportResponse response = reportHandler.execute(request);
                    if (response.getErrorCode() == 1000) {
                        if (!reportModel.isEnableComparitiveDataset()) {
                            reportData = response.getDataRecords();
                        }
                        else {
                            reportData = new ArrayList<>();
                            for (ComparitiveDatasetResult cdr : response.getComparitiveDatasetResults()) {
                                reportData.addAll(cdr.getRecordList());
                            }
                            comparativeDatasetResults = response.getComparitiveDatasetResults();
                        }

                        if (reportModel.getReportDetails().getFieldValueMap().get("report_type").getCurrentValue().equals("2")) {
                            switch (reportModel.getReportDetails().getFieldValueMap().get("graph_type").getCurrentValue()) {
                                case "1":
                                    if (!reportModel.isEnableComparitiveDataset()) {
                                        this.buildPieChart();
                                    }
                                    break;
                                case "3":
                                    if (!reportModel.isEnableComparitiveDataset()) {
                                        this.buildLineChart();
                                    }
                                    break;
                                case "2":
                                    if (!reportModel.isEnableComparitiveDataset()) {
                                        this.buildBarChart();
                                    }
                                    else {
                                        this.buildBarChartForComparitiveAnalysis();
                                    }
                                    break;
                                case "4":
                                    if (!reportModel.isEnableComparitiveDataset()) {
                                        this.buildDonutChart();
                                    }
                                    break;
                            }
                        }
                    }
                }

            }
            catch (Exception ex) {
                Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private List<String> fillColorList(int dataSize, List<String> colorPallete) {
        if (colorPallete.size() < dataSize) {
            colorPallete.addAll(colorPallete);
            this.fillColorList(dataSize, colorPallete);
        }
        return colorPallete;
    }

    public void buildLineChart() {
        lineModel = new LineChartModel();
        ChartData data = new ChartData();
        LineChartDataSet dataSet = new LineChartDataSet();
        List<Object> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        dataSet.setLabel(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue());
        dataSet.setTension(0.3);

        String labelFieldName = reportModel.getGroupByList().get(0).getFieldName();
        ArrayList<String> valueFieldList = new ArrayList<>();
        for (AggregatModel aggregatModel : reportModel.getAggregatesList()) {
            valueFieldList.add(aggregatModel.getAlias());
            break;
        }

        ModuleModel mm = super.loadModule(reportModel.getReportDetails().getFieldValueMap().get("primary_module").getCurrentValue());
        if (mm != null) {
            dataSet.setLabel(mm.getModuleName());
        }

        if (reportData != null) {
            for (String valueField : valueFieldList) {
//                LineChartSeries series = new LineChartSeries();
                for (RecordModel rm : reportData) {
                    labels.add(rm.getFieldValueMap().get(labelFieldName).getCurrentValue());
//                    lineModel.addLabel(rm.getFieldValueMap().get(labelFieldName).getCurrentValue());
                    values.add(Float.valueOf(rm.getFieldValueMap().get(valueField).getCurrentValue()));
//                    series.set(Float.valueOf(rm.getFieldValueMap().get(valueField).getCurrentValue()));
                }
//                lineModel.addSeries(series);
                dataSet.setData(values);
                dataSet.setFill(false);
//                dataSet.setLabel("My First Dataset");
                dataSet.setBorderColor("#1789fc");
//                dataSet.setLineTension(0.1);
                data.addChartDataSet(dataSet);
            }
            data.setLabels(labels);

            LineChartOptions options = new LineChartOptions();
            Title title = new Title();
            title.setDisplay(true);
            title.setText(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue());
            options.setTitle(title);

            lineModel.setOptions(options);
            lineModel.setData(data);
        }

//        lineModel.setAspectRatio(AspectRatio.GOLDEN_SECTION);
//        String labelFieldName = reportModel.getGroupByList().get(0).getFieldName();
//        ArrayList<String> valueFieldList = new ArrayList<>();
//        for (AggregatModel aggregatModel : reportModel.getAggregatesList()) {
//            valueFieldList.add(aggregatModel.getAlias());
//            break;
//        }
//
//        if (reportData != null) {
//            for (String valueField : valueFieldList) {
//                LineChartSeries series = new LineChartSeries();
//                for (RecordModel rm : reportData) {
//                    lineModel.addLabel(rm.getFieldValueMap().get(labelFieldName).getCurrentValue());
//                    series.set(Float.valueOf(rm.getFieldValueMap().get(valueField).getCurrentValue()));
//                }
//                lineModel.addSeries(series);
//            }
//
//            Axis xAxis = lineModel.getAxis(AxisType.X);
//            xAxis.setShowGrid(false);
//            lineModel.setShowTooltip(true);
//        }
    }

    public void buildPieChart() {
        pieChartModel = new PieChartModel();
        String labelName = "";
        String value = "";
        List<String> bgColorsPallete = new ArrayList<>(this.colorMap.get(reportModel.getChartColorPallete()));

        if (reportModel.getFieldList() != null) {
            labelName = reportModel.getFieldList().get(0).getFieldName();
        }

        if (reportModel.getAggregatesList() != null && !reportModel.getAggregatesList().isEmpty()) {
            value = reportModel.getAggregatesList().get(0).getAlias();
        }
        else {
            if (reportModel.getCustomFields() != null && !reportModel.getCustomFields().isEmpty()) {
                value = reportModel.getCustomFields().get(0);
            }
        }

        ChartData data = new ChartData();
        PieChartDataSet dataSet = new PieChartDataSet();
        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        List<String> bgColors = this.fillColorList(reportData.size(), bgColorsPallete);
        Random randomGenerator = new Random();
        int R, G, B;

        for (RecordModel rm : reportData) {
            labels.add(rm.getFieldValueMap().get(labelName).getCurrentValue());
            values.add(Float.valueOf(rm.getFieldValueMap().get(value).getCurrentValue()));
//            R = randomGenerator.nextInt(255) + 1;
//            G = randomGenerator.nextInt(255) + 1;
//            B = randomGenerator.nextInt(255) + 1;
//            bgColors.add("rgb(" + R + "," + G + "," + B + ")");
        }
        dataSet.setData(values);
        dataSet.setBackgroundColor(bgColors);
        data.addChartDataSet(dataSet);
        data.setLabels(labels);
        pieChartModel.setData(data);

    }

    public void buildDonutChart() {
        donutModel = new DonutChartModel();
        String labelName = "";
        String value = "";
        List<String> bgColorsPallete = new ArrayList<>(this.colorMap.get(reportModel.getChartColorPallete()));

        if (reportModel.getFieldList() != null) {
            labelName = reportModel.getFieldList().get(0).getFieldName();
        }

        if (reportModel.getAggregatesList() != null && !reportModel.getAggregatesList().isEmpty()) {
            value = reportModel.getAggregatesList().get(0).getAlias();
        }
        else {
            if (reportModel.getCustomFields() != null && !reportModel.getCustomFields().isEmpty()) {
                value = reportModel.getCustomFields().get(0);
            }
        }

        ChartData data = new ChartData();
        DonutChartDataSet dataSet = new DonutChartDataSet();
        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        List<String> bgColors = this.fillColorList(reportData.size(), bgColorsPallete);
        Random randomGenerator = new Random();
        int R, G, B;

        for (RecordModel rm : reportData) {
            labels.add(rm.getFieldValueMap().get(labelName).getCurrentValue());
            values.add(Float.valueOf(rm.getFieldValueMap().get(value).getCurrentValue()));
//            R = randomGenerator.nextInt(255) + 1;
//            G = randomGenerator.nextInt(255) + 1;
//            B = randomGenerator.nextInt(255) + 1;
//            bgColors.add("rgb(" + R + "," + G + "," + B + ")");
        }
        dataSet.setData(values);
        dataSet.setBackgroundColor(bgColors);
        data.addChartDataSet(dataSet);
        data.setLabels(labels);
        donutModel.setData(data);
    }

    public void buildBarChart() {
        //support legacy
        if (this.reportModel.getReportChart() != null && reportModel.getReportChart().getAxisX().getFieldName() != null) {
            barChartModel = new BarChartModel();
            ChartData data = new ChartData();
            List<String> bgColors = new ArrayList<>(this.colorMap.get(reportModel.getChartColorPallete()));

            BarChartDataSet barDataSet = new BarChartDataSet();
            barDataSet.setLabel(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue());

            List<Number> values = new ArrayList<>();//y-axis
            List<String> labels = new ArrayList<>();//x-axis
//            String labelFieldName = reportModel.getGroupByList().get(0).getFieldName();
            String labelFieldName = reportModel.getReportChart().getAxisX().getFieldName();
            ArrayList<String> valueFieldList = new ArrayList<>();

            if (reportModel.getAggregatesList() != null && !reportModel.getAggregatesList().isEmpty()) {
                for (AggregatModel aggregatModel : reportModel.getAggregatesList()) {
                    valueFieldList.add(aggregatModel.getAlias());
                    break;
                }
            }
            else {
                if (reportModel.getCustomFields() != null && !reportModel.getCustomFields().isEmpty()) {
                    for (String customField : reportModel.getCustomFields()) {
                        valueFieldList.add(customField);
                        break;
                    }
                }
            }

            if (reportData != null) {
                List<String> bgBarColor = this.fillColorList(reportData.size(), bgColors);
                for (String valueField : valueFieldList) {
                    LineChartDataSet series = new LineChartDataSet();
                    int index = 0;
                    for (RecordModel rm : reportData) {
                        labels.add(rm.getFieldValueMap().get(labelFieldName).getCurrentValue());
                        values.add(Float.valueOf(rm.getFieldValueMap().get(valueField).getCurrentValue()));
//                    bgColor.add("rgb(75, 192, 192)");
//                    bgColor.add("#1789fc");
                    }
                    barDataSet.setData(values);
                }

                barDataSet.setBackgroundColor(bgBarColor);

                data.addChartDataSet(barDataSet);
                data.setLabels(labels);
                barChartModel.setData(data);

                BarChartOptions options = new BarChartOptions();
                CartesianScales cScales = new CartesianScales();
                CartesianLinearAxes linearAxes = new CartesianLinearAxes();
                CartesianLinearTicks ticks = new CartesianLinearTicks();
//            ticks.setBeginAtZero(true);
                linearAxes.setTicks(ticks);
                cScales.addYAxesData(linearAxes);
                options.setScales(cScales);

                Title title = new Title();
                title.setDisplay(true);
                title.setText(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue());
                options.setTitle(title);

//            Legend legend = new Legend();
//            legend.setDisplay(true);
//            legend.setPosition("top");
//            LegendLabel legendLabels = new LegendLabel();
//            legendLabels.setFontStyle("bold");
//            legendLabels.setFontColor("#2980B9");
//            legendLabels.setFontSize(24);
//            legend.setLabels(legendLabels);
//            options.setLegend(legend);
                barChartModel.setOptions(options);
            }
        }
        else {
            barChartModel = new BarChartModel();
            ChartData data = new ChartData();
            List<String> bgColors = new ArrayList<>(this.colorMap.get(reportModel.getChartColorPallete()));

            BarChartDataSet barDataSet = new BarChartDataSet();
            barDataSet.setLabel(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue());

            List<Number> values = new ArrayList<>();//y-axis
            List<String> labels = new ArrayList<>();//x-axis
            String labelFieldName = reportModel.getGroupByList().get(0).getFieldName();
            ArrayList<String> valueFieldList = new ArrayList<>();

            if (reportModel.getAggregatesList() != null && !reportModel.getAggregatesList().isEmpty()) {
                for (AggregatModel aggregatModel : reportModel.getAggregatesList()) {
                    valueFieldList.add(aggregatModel.getAlias());
                    break;
                }
            }
            else {
                if (reportModel.getCustomFields() != null && !reportModel.getCustomFields().isEmpty()) {
                    for (String customField : reportModel.getCustomFields()) {
                        valueFieldList.add(customField);
                        break;
                    }
                }
            }

            List<String> bgBarColor = this.fillColorList(reportData.size(), bgColors);

            if (reportData != null) {
                for (String valueField : valueFieldList) {
                    LineChartDataSet series = new LineChartDataSet();
                    int index = 0;
                    for (RecordModel rm : reportData) {
                        labels.add(rm.getFieldValueMap().get(labelFieldName).getCurrentValue());
                        values.add(Float.valueOf(rm.getFieldValueMap().get(valueField).getCurrentValue()));
//                    bgColor.add("rgb(75, 192, 192)");
//                    bgColor.add("#1789fc");
                    }
                    barDataSet.setData(values);
                }

                barDataSet.setBackgroundColor(bgBarColor);

                data.addChartDataSet(barDataSet);
                data.setLabels(labels);
                barChartModel.setData(data);

                BarChartOptions options = new BarChartOptions();
                CartesianScales cScales = new CartesianScales();
                CartesianLinearAxes linearAxes = new CartesianLinearAxes();
                CartesianLinearTicks ticks = new CartesianLinearTicks();
//            ticks.setBeginAtZero(true);
                linearAxes.setTicks(ticks);
                cScales.addYAxesData(linearAxes);
                options.setScales(cScales);

                Title title = new Title();
                title.setDisplay(true);
                title.setText(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue());
                options.setTitle(title);

//            Legend legend = new Legend();
//            legend.setDisplay(true);
//            legend.setPosition("top");
//            LegendLabel legendLabels = new LegendLabel();
//            legendLabels.setFontStyle("bold");
//            legendLabels.setFontColor("#2980B9");
//            legendLabels.setFontSize(24);
//            legend.setLabels(legendLabels);
//            options.setLegend(legend);
                barChartModel.setOptions(options);
            }
        }

    }

    public void buildBarChartForComparitiveAnalysis() {
        barChartModel = new BarChartModel();
        ChartData data = new ChartData();

        List<Number> values = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        String labelFieldName = "";
        for (ModuleFieldModel groupFields : reportModel.getGroupByList()) {
            for (ModuleFieldModel selectedFields : reportModel.getFieldList()) {
                if (selectedFields.getFieldName().equals(groupFields.getFieldName())) {
                    labelFieldName = groupFields.getFieldName();
                }
            }
        }
//        String labelFieldName = reportModel.getGroupByList().get(0).getFieldName();
        ArrayList<String> valueFieldList = new ArrayList<>();
        for (AggregatModel aggregatModel : reportModel.getAggregatesList()) {
            valueFieldList.add(aggregatModel.getAlias());
            break;
        }

        ///////////////////////////////////////////////////////////
        // 2. manage data
        ArrayList< LinkedHashMap<String, Number>> valueMapList = null;
        if (this.comparativeDatasetResults != null) {
            valueMapList = new ArrayList<>();
            for (ComparitiveDatasetResult cdr : comparativeDatasetResults) {
                LinkedHashMap<String, Number> valueMap = new LinkedHashMap<>();
                if (cdr.getRecordList() != null) {
                    for (String valueField : valueFieldList) {
                        for (RecordModel rm : cdr.getRecordList()) {
                            valueMap.put(rm.getFieldValueMap().get(labelFieldName).getCurrentValue(), Float.valueOf(rm.getFieldValueMap().get(valueField).getCurrentValue()));
                        }
                    }
                }
                valueMapList.add(valueMap);
            }
        }

        ArrayList<String> unifiedLabels = new ArrayList<>();
        if (valueMapList != null) {
            for (LinkedHashMap<String, Number> valueMap : valueMapList) {
                for (Map.Entry<String, Number> entry : valueMap.entrySet()) {
                    if (!unifiedLabels.contains(entry.getKey())) {
                        unifiedLabels.add(entry.getKey());
                    }
                }
            }

            boolean numericLabels = true;
            for (String label : unifiedLabels) {
                if (!NumberUtils.isCreatable(label)) {
                    numericLabels = false;
                    break;
                }
            }

            if (numericLabels) {
                Collections.sort(unifiedLabels, new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return (int) Math.ceil(Float.valueOf(o1)) - (int) Math.ceil(Float.valueOf(o2));
                    }
                });
            }
            else {
                Collections.sort(unifiedLabels, new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return o1.compareToIgnoreCase(o2);
                    }
                });
            }

            ArrayList< LinkedHashMap<String, Number>> cleanValueMapList = new ArrayList<>();
            for (LinkedHashMap<String, Number> valueMap : valueMapList) {
                LinkedHashMap<String, Number> cleanValueMap = new LinkedHashMap<>();
                for (String unifiedLabel : unifiedLabels) {
                    if (valueMap.get(unifiedLabel) != null) {
                        cleanValueMap.put(unifiedLabel, valueMap.get(unifiedLabel));
                    }
                    else {
                        cleanValueMap.put(unifiedLabel, 0);
                    }
                }
                cleanValueMapList.add(cleanValueMap);
            }

            int index = 0;
            for (ComparitiveDatasetResult cdr : comparativeDatasetResults) {
                BarChartDataSet barDataSet = new BarChartDataSet();
                barDataSet.setLabel(cdr.getComparitiveDatasetConfig().getDatasetLabel());
                barDataSet.setStack("Stack " + index);
                values = new ArrayList<>();

                List<String> bgColor = new ArrayList<>();

                LinkedHashMap<String, Number> targetValueMap = cleanValueMapList.get(index);
                for (Map.Entry<String, Number> entry : targetValueMap.entrySet()) {
                    values.add(entry.getValue());
                    bgColor.add("#" + cdr.getComparitiveDatasetConfig().getColor());
                }
                barDataSet.setData(values);
                barDataSet.setBackgroundColor(bgColor);
                data.addChartDataSet(barDataSet);
                index++;
            }

            data.setLabels(unifiedLabels);
            barChartModel.setData(data);

            BarChartOptions options = new BarChartOptions();
            CartesianScales cScales = new CartesianScales();
            CartesianLinearAxes linearAxes = new CartesianLinearAxes();
            CartesianLinearTicks ticks = new CartesianLinearTicks();
//            ticks.setBeginAtZero(true);
            linearAxes.setTicks(ticks);
            cScales.addYAxesData(linearAxes);
            options.setScales(cScales);

            Title title = new Title();
            title.setDisplay(true);
            title.setText(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue());
            options.setTitle(title);

            Legend legend = new Legend();
            legend.setDisplay(false);
            legend.setPosition("top");
            LegendLabel legendLabels = new LegendLabel();
            legendLabels.setFontStyle("bold");
            legendLabels.setFontColor("#2980B9");
            legendLabels.setFontSize(24);
            legend.setLabels(legendLabels);
            options.setLegend(legend);

            barChartModel.setOptions(options);
        }
    }

    public void fillMissingComparitiveSeries(String key, ArrayList< LinkedHashMap<String, Number>> valueMapList) {
        if (valueMapList != null) {
            for (LinkedHashMap<String, Number> valueMap : valueMapList) {
                if (valueMap.get(key) == null) {
                    valueMap.put(key, 0);
                }
            }
        }
    }

    public String evaluatePivotRowValue(String columnValue, String rowValue) {
        return String.valueOf(RecordAggregateFunctions.conditionalSumOfField(reportData, reportModel.getPivotConfiguration().getValueColumn(), reportModel.getPivotConfiguration().getHeaderColumnFieldName(), columnValue, reportModel.getPivotConfiguration().getRowConfiguration(), rowValue));
    }

    public void evaluateGlobalVariables(RecordModel transientRecordModel, ArrayList<GlobalVariable> globalVariables) {
        if (reportModel.getReportDetails().getFieldValueMap().get("report_type").getCurrentValue().equals("3")) {
            if (this.reportModel.getCustomReportConfig() != null) {

                if (globalVariables != null && !globalVariables.isEmpty()) {
                    BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                    beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                    beezlInterperter.setRecodeModel(transientRecordModel);
                    for (GlobalVariable gv : globalVariables) {
                        beezlInterperter.addDynamicField(gv.getVariableName(), gv.getVariableValue());
                    }

                    try {
                        LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                        CustomReportConfig customReportConfig = this.getReportModel().getCustomReportConfig();
                        if (customReportConfig.getHtmlContent() != null
                                && !customReportConfig.getHtmlContent().isEmpty()
                                && customReportConfig.getTemplateParameters() != null
                                && !customReportConfig.getTemplateParameters().isEmpty()) {
                            for (DocTemplateParameter dtp : customReportConfig.getTemplateParameters()) {
                                for (Map.Entry<String, String> entry : dynamicFieldMap.entrySet()) {
                                    dtp.setKeyValue(dtp.getKeyValue().replace("$" + entry.getKey() + "$", entry.getValue()));
                                }
                            }
                        }
                    }
                    catch (Exception ex) {
                        Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                this.reportModel.getCustomReportConfig().setHtmlContent(this.evaluateCustomReport(this.reportModel.getCustomReportConfig()));
            }
        }
    }

    public String evaluateCustomReport(CustomReportConfig customReportConfig) {
        if (customReportConfig != null) {
            if (customReportConfig.getHtmlContent() != null
                    && !customReportConfig.getHtmlContent().isEmpty()) {
                
                if (customReportConfig.getLinkStyling() != null) {
                        this.customLinkStyling = this.customLinkStyling + "\n" + customReportConfig.getLinkStyling();
                    }

                    if (customReportConfig.getCssStyling() != null) {
                        this.customCssStyling = this.customCssStyling + "\n" + customReportConfig.getCssStyling();
                    }

                if (customReportConfig.getTemplateParameters() != null
                        && !customReportConfig.getTemplateParameters().isEmpty()) {

//                    if (customReportConfig.getLinkStyling() != null) {
//                        this.customLinkStyling = this.customLinkStyling + "\n" + customReportConfig.getLinkStyling();
//                    }
//
//                    if (customReportConfig.getCssStyling() != null) {
//                        this.customCssStyling = this.customCssStyling + "\n" + customReportConfig.getCssStyling();
//                    }

                    BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                    beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                    LinkedHashMap<String, String> bvm = null;
                    LinkedHashMap<String, String> dynamicFieldsMap = new LinkedHashMap<>();

                    Map<String, Object> configs = new HashMap<>();
                    for (DocTemplateParameter dtp : customReportConfig.getTemplateParameters()) {
                        switch (dtp.getType()) {
                            case "STRING":
                                beezlInterperter.addDynamicField(dtp.getKeyName(), dtp.getKeyValue());
                                dynamicFieldsMap.put(dtp.getKeyName(), dtp.getKeyValue());
                                break;
                            case "LIST":
                                break;
                        }
                    }

                    CubeProcessor cubeProcessor = new CubeProcessor(dynamicFieldsMap, 10);
                    bvm = cubeProcessor.execute();

                    try {
//                    bvm = beezlInterperter.evaluate();
                    }
                    catch (Exception ex) {
                        Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                        return "Something went wrong :(  we encountered this error " + ex.getMessage();
                    }

                    for (DocTemplateParameter dtp : customReportConfig.getTemplateParameters()) {
                        if (bvm != null) {
                            if (!dtp.getType().equals("LIST")) {
                                configs.put(dtp.getKeyName(), bvm.get(dtp.getKeyName()));
                            }
                        }
                        else {
                            return "";
                        }
                    }

                    StringWriter out = new StringWriter();
                    Configuration cfg = new Configuration();
                    cfg.setDefaultEncoding("UTF-8");
                    Template t;
                    try {
                        t = new Template("name", new StringReader(customReportConfig.getHtmlContent()), cfg);
                        t.process(configs, out);
                    }
                    catch (Exception ex) {
                        Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                        return "Something went wrong :(  we encountered this error " + ex.getMessage();
                    }
                    String processedWidget = out.getBuffer().toString();

                    return processedWidget;
                }
                else {
                    return customReportConfig.getHtmlContent();
                }
            }
        }
        return "";
    }

    public String evaluateDetailedReportWithCustomization(CustomReportConfig customReportConfig) {
        if (customReportConfig != null) {
            if (customReportConfig.getHtmlContent() != null
                    && !customReportConfig.getHtmlContent().isEmpty()) {

                ArrayList<RecordModel> detailReportData = null;
                ReportRequest request = new ReportRequest();
                request.setReportModel(reportModel);
                request.setLimit(1000);
                request.setOffset(0);
                request.setRequestActionType("0");
                request.setGetData(true);
                ReportHandler reportHandler = new ReportHandler();
                ReportResponse reportResponse = reportHandler.execute(request);
                if (reportResponse.getErrorCode() == 1000 && reportResponse.getDataRecords() != null && !reportResponse.getDataRecords().isEmpty()) {
                    detailReportData = new ArrayList<>(reportResponse.getDataRecords());
                }

                if (customReportConfig.getLinkStyling() != null) {
                    this.customLinkStyling = this.customLinkStyling + "\n" + customReportConfig.getLinkStyling();
                }

                if (customReportConfig.getCssStyling() != null) {
                    this.customCssStyling = this.customCssStyling + "\n" + customReportConfig.getCssStyling();
                }

                BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                LinkedHashMap<String, String> bvm = null;
                LinkedHashMap<String, String> dynamicFieldsMap = new LinkedHashMap<>();

                Map<String, Object> configs = new HashMap<>();
                for (DocTemplateParameter dtp : customReportConfig.getTemplateParameters()) {
                    switch (dtp.getType()) {
                        case "STRING":
                            beezlInterperter.addDynamicField(dtp.getKeyName(), dtp.getKeyValue());
                            dynamicFieldsMap.put(dtp.getKeyName(), dtp.getKeyValue());
                            break;
                        case "LIST":
                            ArrayList<RecordModel> contextRecordsList;
                            contextRecordsList = detailReportData;
                            ArrayList<DynamicTemplateReference> dynamicObjects = new ArrayList<>();
                            HashMap<String, String> propertValueMap;
                            if (contextRecordsList != null && !contextRecordsList.isEmpty()) {
                                for (RecordModel rm : contextRecordsList) {
                                    propertValueMap = new HashMap<>();
                                    for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                                        String value = "";
                                        if (entry.getValue().getCurrentValue() != null) {
                                            value = entry.getValue().getCurrentValue();
                                        }
                                        propertValueMap.put(entry.getKey(), value);
                                    }
                                    dynamicObjects.add(new DynamicTemplateReference(propertValueMap));
                                }
                            }
                            configs.put(dtp.getKeyName(), dynamicObjects);
                            break;
                    }
                }

//                CubeProcessor cubeProcessor = new CubeProcessor(dynamicFieldsMap, 10);
//                bvm = cubeProcessor.execute();
                try {
                    bvm = beezlInterperter.evaluate();
                }
                catch (Exception ex) {
                    Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                    return "Something went wrong :(  we encountered this error " + ex.getMessage();
                }

                for (DocTemplateParameter dtp : customReportConfig.getTemplateParameters()) {
                    if (bvm != null) {
                        if (!dtp.getType().equals("LIST")) {
                            configs.put(dtp.getKeyName(), bvm.get(dtp.getKeyName()));
                        }
                    }
                    else {
//                        return "";
                    }
                }

                StringWriter out = new StringWriter();
                Configuration cfg = new Configuration();
                cfg.setDefaultEncoding("UTF-8");
                Template t;
                try {
                    String modifiedTemplate = customReportConfig.getHtmlContent();
                    modifiedTemplate = modifiedTemplate.replace("<!--", "<");
                    modifiedTemplate = modifiedTemplate.replace("-->", ">");
                    modifiedTemplate = modifiedTemplate.replace("&#39;", "'");
                    t = new Template("name", new StringReader(modifiedTemplate), cfg);
                    t.process(configs, out);
                }
                catch (Exception ex) {
                    Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                }
                String processedWidget = out.getBuffer().toString();

                return processedWidget;
            }
        }
        return "";
    }

    public void convertCustomReportToPdf() {
        if (this.reportModel.getCustomReportConfig() != null) {
            DocumentUtilsHandler documentUtilsHandler = new DocumentUtilsHandler();
            DocumentUtilRequest documentUtilRequest = new DocumentUtilRequest();
            documentUtilRequest.setRequestActionType("0");
            documentUtilRequest.setHtmlContent(this.reportModel.getCustomReportConfig().getHtmlContent());
            DocumentUtilResponse documentUtilResponse = documentUtilsHandler.executeRequest(documentUtilRequest);
            if (documentUtilResponse.getErrorCode() == 1000 && documentUtilResponse.getConvertedDocument() != null) {
                this.customPdfReport = DefaultStreamedContent.builder()
                        .name(this.reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue() + ".pdf")
                        .contentType("application/pdf")
                        .stream(() -> new ByteArrayInputStream(
                        documentUtilResponse.getConvertedDocument().getContentStream()))
                        .build();
            }
        }
    }

    public void editReport() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("addReport_new.xhtml?report="
                    + super.getRequestParams().get("report") + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT);
        }
        catch (Exception ex) {
            Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ReportModel getReportModel() {
        return reportModel;
    }

    public void setReportModel(ReportModel reportModel) {
        this.reportModel = reportModel;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        FilterManager fm = new FilterManager();
        if (super.getRequestParams().get("report") != null) {
            fm.setFilter("AND", new FilterType("id", "=", super.getRequestParams().get("report")));
        }
        else if (reportId != null) {
            fm.setFilter("AND", new FilterType("id", "=", reportId));
        }
        else {
            return;
        }
        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setRequestActionType("0");
        reportRequest.setClause(fm.getClause());
        ReportResponse reportResponse;
        ReportHandler reportHandler = new ReportHandler();
        try {
            reportResponse = reportHandler.execute(reportRequest);
            if (reportResponse != null && reportResponse.getErrorCode() == 1000 && reportResponse.getReturnList() != null
                    && !reportResponse.getReturnList().isEmpty()) {
                this.reportModel = reportResponse.getReturnList().get(0);
                this.lazyReportRecordModel = new LazyReportRecordModel(reportModel);
                if (reportModel != null) {
                    if (reportModel.getChartColorPallete() == null || reportModel.getChartColorPallete().isEmpty()) {
                        reportModel.setChartColorPallete("JUST_BLUE");
                    }
                    if (reportModel.getAggregatesList() != null) {
                        for (AggregatModel aggregatModel : reportModel.getAggregatesList()) {
                            ModuleFieldModel model;
                            model = new ModuleFieldModel();
                            model.setFieldName(aggregatModel.getAlias());
                            model.setFieldLabel(aggregatModel.getAlias());
                            model.setFieldType("TEXT");
                            model.setFieldValues(null);
                            model.setIsCustom(false);
                            model.setIsKey(false);
                            model.setIsMandatory(false);
                            model.setIsVisible(true);
                            if (reportModel.getFieldList() != null) {
                                reportModel.getFieldList().add(model);
                            }
                            else {
                                reportModel.setFieldList(new ArrayList<>());
                                reportModel.getFieldList().add(model);
                            }
                        }
                    }

                    if (reportModel.getCustomFields() != null && !reportModel.getCustomFields().isEmpty()) {
                        for (String colName : reportModel.getCustomFields()) {
                            ModuleFieldModel cmfm = new ModuleFieldModel();
                            cmfm.setFieldName(colName);
                            cmfm.setFieldLabel(colName);
                            cmfm.setFieldType("TEXT");
                            cmfm.setVirtual(true);
                            reportModel.getFieldList().add(cmfm);

                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ReportViewer.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<RecordModel> getReportData() {
        return reportData;
    }

    public void setReportData(ArrayList<RecordModel> reportData) {
        this.reportData = reportData;
    }

    public ExcelOptions getExcelOpt() {
        return excelOpt;
    }

    public void setExcelOpt(ExcelOptions excelOpt) {
        this.excelOpt = excelOpt;
    }

    public PDFOptions getPdfOpt() {
        return pdfOpt;
    }

    public void setPdfOpt(PDFOptions pdfOpt) {
        this.pdfOpt = pdfOpt;
    }

    public BarChartModel getBarChartModel() {
        return barChartModel;
    }

    public void setBarChartModel(BarChartModel barChartModel) {
        this.barChartModel = barChartModel;
    }

    public PieChartModel getPieChartModel() {
        return pieChartModel;
    }

    public void setPieChartModel(PieChartModel pieChartModel) {
        this.pieChartModel = pieChartModel;
    }

    public LineChartModel getLineModel() {
        return lineModel;
    }

    public void setLineModel(LineChartModel lineModel) {
        this.lineModel = lineModel;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public ArrayList<String> getPivotHeaderValues() {
        return pivotHeaderValues;
    }

    public void setPivotHeaderValues(ArrayList<String> pivotHeaderValues) {
        this.pivotHeaderValues = pivotHeaderValues;
    }

    public ArrayList<String> getPivotRowValues() {
        return pivotRowValues;
    }

    public void setPivotRowValues(ArrayList<String> pivotRowValues) {
        this.pivotRowValues = pivotRowValues;
    }

    public ArrayList<RecordModel> getPivotReportData() {
        return pivotReportData;
    }

    public void setPivotReportData(ArrayList<RecordModel> pivotReportData) {
        this.pivotReportData = pivotReportData;
    }

    public String getCustomLinkStyling() {
        return customLinkStyling;
    }

    public void setCustomLinkStyling(String customLinkStyling) {
        this.customLinkStyling = customLinkStyling;
    }

    public String getCustomCssStyling() {
        return customCssStyling;
    }

    public void setCustomCssStyling(String customCssStyling) {
        this.customCssStyling = customCssStyling;
    }

    public StreamedContent getCustomPdfReport() {
        return customPdfReport;
    }

    public void setCustomPdfReport(StreamedContent customPdfReport) {
        this.customPdfReport = customPdfReport;
    }

    public LazyReportRecordModel getLazyReportRecordModel() {
        return lazyReportRecordModel;
    }

    public void setLazyReportRecordModel(LazyReportRecordModel lazyReportRecordModel) {
        this.lazyReportRecordModel = lazyReportRecordModel;
    }

    public DonutChartModel getDonutModel() {
        return donutModel;
    }

    public void setDonutModel(DonutChartModel donutModel) {
        this.donutModel = donutModel;
    }

    public boolean isFromSmartView() {
        return fromSmartView;
    }

    public void setFromSmartView(boolean fromSmartView) {
        this.fromSmartView = fromSmartView;
    }

}
