/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.reports;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.deals.DealsBean;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.ReportHandler;
import com.beezer.web.models.SelectionModel;
import com.beezer.web.utils.BeezlCodeHelper;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.reports.AggregatModel;
import com.crm.models.global.reports.ComparitiveDatasetConfig;
import com.crm.models.global.reports.CustomReportConfig;
import com.crm.models.global.reports.JoinModel;
import com.crm.models.global.reports.OrderByConfig;
import com.crm.models.global.reports.PivotConfiguration;
import com.crm.models.global.reports.ReportFilterModel;
import com.crm.models.global.reports.ReportModel;
import com.crm.models.global.reports.ViewStatement;
import com.crm.models.global.reports.charts.ChartAxisX;
import com.crm.models.global.reports.charts.ChartAxisY;
import com.crm.models.global.reports.charts.CountChartValues;
import com.crm.models.global.reports.charts.FieldSummaryValues;
import com.crm.models.global.reports.charts.ReportChart;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldBlockModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.internal.workflow.activities.DocTemplateParameter;
import com.crm.models.requests.ReportRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.ReportResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.responses.managers.RelationResponse;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.extensions.model.monacoeditor.ELanguage;
import org.primefaces.extensions.model.monacoeditor.ETheme;
import org.primefaces.extensions.model.monacoeditor.EWordWrap;
import org.primefaces.extensions.model.monacoeditor.EditorOptions;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "addReportBean_new")
@ViewScoped
public class AddReportBean_new extends BeanFramework {

    private ArrayList<ModuleModel> relatedModules;
    private ArrayList<ModuleFieldModel> selectedColumnList;
    private ArrayList<ModuleFieldModel> availableColumnList;
    private ArrayList<ModuleFieldModel> availableColumnListForAggregates;
    private ArrayList<ModuleFieldModel> currentFieldsForJoin;
    private ReportModel reportModel;
    private ArrayList<ModuleModel> moduleList;

    private String filterColumn;
    private List<SelectItem> filterColumnList;
    private List<SelectItem> filterColumnSimpleList;
    private StreamedContent reportPdfContent;
    private boolean enableReportGeneration;

    private ArrayList<String> customColumns;
    private HashMap<String, ArrayList<ModuleFieldModel>> moduleFieldMap;

    private boolean skipPattenRecognition;
    private EditorOptions editorOptions;
    private boolean skip;
    private DocGenComponentManager docGenComponentManager;
    private BeezlCodeHelper beezlCodeHelper;

    private ResponseModel validationResponse;

    private boolean showChartColors;
    private String selectedChartColorPallete;
    private HashMap<String, ArrayList<String>> colorMap;

    private ReportViewer reportViewer;

    private RecordModel reportBasicRecord;
    private int currentStep;
    private String showChartFiltering = "false";
    private ComparitiveDatasetConfig selectedComparitiveDatasetConfig;

    private LinkedHashMap<ModuleModel, ArrayList<ModuleModel>> relationHeirarchyMap;

    @PostConstruct
    public void init() {
        this.currentStep = 1;
//        this.initializeReportRecord();
        this.colorMap = new HashMap<>();
        this.buildColorMap();
        this.relationHeirarchyMap = new LinkedHashMap<>();
        this.reportModel = new ReportModel();
        this.reportModel.setANDFilterList(new ArrayList<>());
        this.reportModel.setORFilterList(new ArrayList<>());
        this.reportModel.setGroupByList(new ArrayList<>());
        this.reportModel.setOrderByList(new ArrayList<>());
        this.reportModel.setAggregatesList(new ArrayList<>());
        this.reportModel.setCustomFields(new ArrayList<>());
        this.reportModel.setJoinConfiguration(new ArrayList<>());
        this.reportModel.setPivotConfiguration(new PivotConfiguration());
        this.reportModel.setCustomReportConfig(new CustomReportConfig());
        this.reportModel.getCustomReportConfig().setGlobalVariables(new ArrayList<>());
        this.moduleList = this.getModuleService();
        this.availableColumnList = new ArrayList<>();
        this.availableColumnListForAggregates = new ArrayList<>();
        this.relatedModules = new ArrayList<>();
        this.currentFieldsForJoin = new ArrayList<>();
        this.moduleFieldMap = new HashMap<>();
        this.beezlCodeHelper = new BeezlCodeHelper();
//        super.loadForms("12", 0);
//        super.setMode();
        this.initializeReportRecord();
        if (super.getRequestMode().equalsIgnoreCase(Defines.REQUEST_MODE_EDIT)) {
            this.enableReportGeneration = true;
            this.hideGraphTypes("edit");
        }
        else {
            this.hideGraphTypes("new");
        }
        this.editorOptions = new EditorOptions();
        this.editorOptions.setTheme(ETheme.VS);
        this.editorOptions.setFontSize(12);
        this.editorOptions.setLanguage(ELanguage.SQL);
        this.editorOptions.setWordWrap(EWordWrap.BOUNDED);
    }

    public void initializeReportRecord() {
        if (super.getRequestParams().get(Defines.REQUEST_MODE_KEY) != null
                && super.getRequestParams().get(Defines.REQUEST_MODE_KEY).equals(Defines.REQUEST_MODE_EDIT)) {
            this.loadRecord();
//            this.reportBasicRecord = this.reportModel.getReportDetails();

        }
        else {
            this.reportBasicRecord = new RecordModel();
            this.reportBasicRecord.setFieldValueMap(new LinkedHashMap<>());
            if (super.getFieldsList() == null) {
                this.loadFieldSet("12", null);
            }

            if (super.getFieldsList() != null) {
                for (ModuleFieldModel mfm : super.getFieldsList()) {
                    this.reportBasicRecord.getFieldValueMap().put(mfm.getFieldName(), mfm);
                }
            }
        }

    }

    public void onReportTypeChoose(String type, String graphType) {
        switch (type) {
            case "1":
                this.reportBasicRecord.getFieldValueMap().get("report_type").setCurrentValue(type);
                break;
            case "2":
                this.reportBasicRecord.getFieldValueMap().get("report_type").setCurrentValue(type);
                this.reportBasicRecord.getFieldValueMap().get("graph_type").setCurrentValue(graphType);
                this.reportModel.setReportChart(new ReportChart());
                this.reportModel.getReportChart().setAxisX(new ChartAxisX());
                this.reportModel.getReportChart().setAxisY(new ChartAxisY());
                this.reportModel.getReportChart().getAxisY().setCountChartValues(new CountChartValues());
                this.reportModel.getReportChart().getAxisY().setFieldSummaryValues(new FieldSummaryValues());
                this.reportModel.getReportChart().getAxisY().setType("RECORD_COUNT");
                break;
            case "3":
                this.reportBasicRecord.getFieldValueMap().get("report_type").setCurrentValue(type);
                break;
        }
        PrimeFaces.current().ajax().update("reportBuilderStepOne");
//        this.updateCurrentStep("UP");
    }

    public boolean checkSelectedTypeForView(String type, String graphType) {
        if (this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue() == null) {
            return false;
        }

        if (type.equals("1")) {
            if (type.equals(this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue())) {
                return true;
            }
            else {
                return false;
            }
        }
        else if (type.equals("3")) {
            if (type.equals(this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue())) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            if (type.equals(this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue())
                    && graphType.equals(this.reportBasicRecord.getFieldValueMap().get("graph_type").getCurrentValue())) {
                return true;
            }
            else {
                return false;
            }
        }

    }

    public void updateCurrentStep(String direction) {
        switch (direction) {
            case "UP":
                if (currentStep == 1) {
                    if (this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue() == null
                            || this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue().isEmpty()) {
//                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Missing step", "Please choose the type of the report"));
//                        PrimeFaces.current().ajax().update("messages");
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Missing Report Type", " Please choose the type of the report you need.");
                        PrimeFaces.current().dialog().showMessageDynamic(message);
                    }
                    else {
                        this.currentStep++;
                    }
                }
                break;
            case "DOWN":
                this.currentStep--;
                break;
        }
        PrimeFaces.current().ajax().update("reportBuilderContainer");
    }

    public void previewReport(String... previewType) {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        if (reportModel.getChartColorPallete() == null || reportModel.getChartColorPallete().isEmpty()) {
            reportModel.setChartColorPallete("JUST_BLUE");
        }
//        super.populateDataFromForm();
        if (!this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue().equals("3")) {
            if (this.reportModel.getCustomColumnScript() != null) {
                if (!this.reportModel.isSkipPatternRecognition()) {
                    String patternString = "" + reportModel.getPrimaryModule().getModuleBaseTable() + "" + "\\W+(\\w+)";
                    Pattern pattern = Pattern.compile(patternString);
                    Matcher matcher = pattern.matcher(reportModel.getCustomColumnScript());
                    while (matcher.find()) {
                        for (ModuleFieldModel mfm : this.availableColumnList) {
                            if (mfm.getModuleId() == reportModel.getPrimaryModule().getModuleId()) {
                                if (mfm.getFieldName().equals(matcher.group(1).replaceAll("\\.", ""))) {
                                    boolean exists = false;
                                    for (ModuleFieldModel mfmR : reportModel.getFieldList()) {
                                        if (mfmR.getModuleId() == mfm.getModuleId() && mfmR.getFieldName().equals(mfm.getFieldName())) {
                                            exists = true;
                                            break;
                                        }
                                    }
                                    if (!exists) {
                                        reportModel.getFieldList().add(mfm);
                                    }
                                }
                            }
                        }
                    }

                    if (reportModel.getRelatedModulesList() != null) {
                        for (ModuleModel mm : reportModel.getRelatedModulesList()) {
                            String patternStringRelated = "" + mm.getModuleBaseTable() + "" + "\\W+(\\w+)";
                            Pattern patternRelated = Pattern.compile(patternStringRelated);
                            Matcher matcherRelated = patternRelated.matcher(reportModel.getCustomColumnScript());
                            while (matcherRelated.find()) {
                                for (ModuleFieldModel mfm : this.availableColumnList) {
                                    if (mfm.getModuleId() == mm.getModuleId()) {
                                        if (mfm.getFieldName().equals(matcherRelated.group(1).replaceAll("\\.", ""))) {
                                            boolean exists = false;
                                            for (ModuleFieldModel mfmR : reportModel.getFieldList()) {
                                                if (mfmR.getModuleId() == mfm.getModuleId() && mfmR.getFieldName().equals(mfm.getFieldName())) {
                                                    exists = true;
                                                    break;
                                                }
                                            }
                                            if (!exists) {
                                                reportModel.getFieldList().add(mfm);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        boolean skipMapping = false;
        if (previewType.length > 0) {
            if (previewType[0] != null) {
                if (previewType[0].equals("RELOAD")) {
                    skipMapping = true;
                }
            }
        }

        if (!skipMapping) {
            if (this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue().equals("2")) {
                this.reportModel.setFieldList(new ArrayList<>());
                for (ModuleFieldModel mfm : this.availableColumnList) {
                    if (mfm.getFieldName().equals(reportModel.getReportChart().getAxisX().getFieldName())
                            && mfm.getModuleId() == reportModel.getPrimaryModule().getModuleId()) {
                        this.reportModel.getFieldList().add(mfm);
                        break;
                    }
                }

                AggregatModel aggregatModel;
                this.reportModel.setGroupByList(new ArrayList<>());
                for (ModuleFieldModel mfm : this.availableColumnList) {
//                    if (mfm.getFieldName().equals(reportModel.getReportChart().getAxisX().getFieldName())
//                            && mfm.getModuleId() == reportModel.getPrimaryModule().getModuleId()) {
//                        this.reportModel.getGroupByList().add(mfm);
//                        break;
//                    }
                    if (mfm.getFieldName().equals(reportModel.getReportChart().getAxisX().getFieldName())) {
                        this.reportModel.getGroupByList().add(mfm);
                        break;
                    }
                }

                switch (this.reportModel.getReportChart().getAxisY().getType()) {
                    case "RECORD_COUNT":
                        this.reportModel.setAggregatesList(new ArrayList<>());
                        aggregatModel = new AggregatModel();
                        ModuleFieldModel randomField = null;
                        for (ModuleFieldModel mfm : this.availableColumnList) {
                            if (mfm.getModuleId() == reportModel.getPrimaryModule().getModuleId()) {
                                randomField = mfm;
                                break;
                            }
                        }

                        if (randomField != null) {
                            aggregatModel.setModel(randomField);
                            aggregatModel.setFunction("COUNT");
                            aggregatModel.setAlias(randomField.getFieldName() + "_func");
                            this.reportModel.getAggregatesList().add(aggregatModel);
                        }

                        break;
                    case "FIELD_SUMMARY":
                        this.reportModel.setAggregatesList(new ArrayList<>());
                        aggregatModel = new AggregatModel();
                        ModuleFieldModel targetField = null;
                        for (ModuleFieldModel mfm : this.availableColumnList) {
                            if (mfm.getModuleId() == reportModel.getPrimaryModule().getModuleId()
                                    && mfm.getFieldName().equals(this.reportModel.getReportChart().getAxisY().getFieldSummaryValues().getFieldName())) {
                                targetField = mfm;
                                break;
                            }
                        }

                        if (targetField != null) {
                            aggregatModel.setModel(targetField);
                            aggregatModel.setFunction(this.reportModel.getReportChart().getAxisY().getFieldSummaryValues().getAggrgateFunction());
                            aggregatModel.setAlias(targetField.getFieldName() + "_func");
                            this.reportModel.getAggregatesList().add(aggregatModel);
                        }
                        break;
                }
            }
        }

        this.reportModel.setReportDetails(this.reportBasicRecord);

        this.reportViewer = new ReportViewer();
        this.reportViewer.initializeForPreview(reportModel);

        if (previewType.length > 0) {
            if (previewType[0] != null) {
                if (previewType[0].equals("CHANGE_CHART")) {
                    PrimeFaces.current().ajax().update("reportDesignerPreviewPanel", "reportDesignerConfigPanel");
                }
            }
        }
        PrimeFaces.current().ajax().update("reportDesignerPreviewPanel");
//        PrimeFaces.current().ajax().update("reportDesignerPreviewPanel","reportDesignerConfigPanel");
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
//        PrimeFaces.current().executeScript("PF('reportPreviewDialog').show()");
    }

    public void onBaseModuleSelect() {
        ModuleModel mm = null;
        if (moduleList == null || moduleList.isEmpty()) {
            this.moduleList = this.getModuleService();
        }
        if (this.reportBasicRecord.getFieldValueMap().get("primary_module").getCurrentValue() != null) {
            for (ModuleModel moduleModel : moduleList) {
                if (moduleModel.getModuleId() == Integer.valueOf(this.reportBasicRecord.getFieldValueMap().get("primary_module").getCurrentValue())) {
                    mm = moduleModel;
                    break;
                }
            }
        }

        this.relatedModules = new ArrayList<>();
        if (mm != null) {
            this.reportModel.setPrimaryModule(mm);
            this.getPrimaryModuleRelations(this.reportModel.getPrimaryModule().getModuleId(), mm);
            this.loadAvailableColumns();
        }

    }

    public void onConfigurationChange() {
        this.previewReport();
    }

    public void onReportTypeChange(String graphType) {
        this.reportBasicRecord.getFieldValueMap().get("graph_type").setCurrentValue(graphType);
        this.previewReport("CHANGE_CHART");
    }

    public void showComparitiveCriteriaSettings(ComparitiveDatasetConfig cdc) {
        this.selectedComparitiveDatasetConfig = cdc;
        PrimeFaces.current().ajax().update("comparitiveCriteriaSettingsDialog");
        PrimeFaces.current().executeScript("PF('comparitiveCriteriaSettingsDialog').show()");
    }

    public void initializeLocally(ReportModel reportModelIn) {
        this.colorMap = new HashMap<>();
        this.buildColorMap();
        this.reportModel = new ReportModel();
        this.reportModel.setANDFilterList(new ArrayList<>());
        this.reportModel.setORFilterList(new ArrayList<>());
        this.reportModel.setGroupByList(new ArrayList<>());
        this.reportModel.setOrderByList(new ArrayList<>());
        this.reportModel.setAggregatesList(new ArrayList<>());
        this.reportModel.setCustomFields(new ArrayList<>());
        this.reportModel.setJoinConfiguration(new ArrayList<>());
        this.reportModel.setPivotConfiguration(new PivotConfiguration());
        this.moduleList = this.getModuleService();
        this.availableColumnList = new ArrayList<>();
        this.availableColumnListForAggregates = new ArrayList<>();
        this.relatedModules = new ArrayList<>();
        this.currentFieldsForJoin = new ArrayList<>();
        this.moduleFieldMap = new HashMap<>();
        this.beezlCodeHelper = new BeezlCodeHelper();
        this.editorOptions = new EditorOptions();
        this.editorOptions.setTheme(ETheme.VS);
        this.editorOptions.setFontSize(12);
        this.editorOptions.setLanguage(ELanguage.SQL);
        this.editorOptions.setWordWrap(EWordWrap.BOUNDED);
        this.loadReportRecordLocally(reportModelIn);
    }

    public void loadReportRecordLocally(ReportModel reportModelIn) {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        this.reportModel = reportModelIn;
        if (reportModel.getChartColorPallete() == null || reportModel.getChartColorPallete().isEmpty()) {
            reportModel.setChartColorPallete("JUST_BLUE");
        }
        this.loadAvailableColumns();
        super.setRecordModel(reportModel.getReportDetails());
        if (reportModel.getJoinConfiguration() != null && !reportModel.getJoinConfiguration().isEmpty()) {
            for (JoinModel jm : reportModel.getJoinConfiguration()) {
                this.generateColumnsForJoin(jm);
            }
        }

        if (reportModel.getPivotConfiguration() == null) {
            reportModel.setPivotConfiguration(new PivotConfiguration());
        }

        if (reportModel.getCustomReportConfig() == null) {
            reportModel.setCustomReportConfig(new CustomReportConfig());
        }

        if (reportModel.getANDFilterList() != null && !reportModel.getANDFilterList().isEmpty()) {
            for (ReportFilterModel rfm : reportModel.getANDFilterList()) {
                if (rfm.getFieldModel().getFieldType().equals("LOOKUP")) {
                    this.loadLookupForFilter(rfm.getFieldModel());
                }
            }
        }

        if (reportModel.getORFilterList() != null && !reportModel.getORFilterList().isEmpty()) {
            for (ReportFilterModel rfm : reportModel.getORFilterList()) {
                if (rfm.getFieldModel().getFieldType().equals("LOOKUP")) {
                    this.loadLookupForFilter(rfm.getFieldModel());
                }
            }
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public void hideGraphTypes(String type) {
        if (type.equals("new")) {
            if (this.getDefaultForm() != null) {
                for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
                    for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                        if (flm.getFieldObject().getFieldName().equals("graph_type")) {
                            flm.setIsVisible(false);
                            PrimeFaces.current().ajax().update("reportControlsMenuHolder");
                            PrimeFaces.current().ajax().update("tabHolder:containerPanel");
                            break;
                        }
                    }
                }
            }
        }
        else {
            if (this.getDefaultForm() != null) {
                for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
                    for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                        if (flm.getFieldObject().getFieldName().equals("graph_type")) {
                            if (this.recordModel.getFieldValueMap().get("report_type").getCurrentValue() != null
                                    && this.recordModel.getFieldValueMap().get("report_type").getCurrentValue().equals("2")) {
                                flm.setIsVisible(true);
                                this.showChartColors = true;
                                PrimeFaces.current().ajax().update("reportControlsMenuHolder");
                                PrimeFaces.current().ajax().update("tabHolder:containerPanel");
                            }
                            else {
                                this.showChartColors = false;
                                flm.setIsVisible(false);
                                PrimeFaces.current().ajax().update("reportControlsMenuHolder");
                                PrimeFaces.current().ajax().update("tabHolder:containerPanel");
                            }

                            break;
                        }
                    }
                }
            }
        }

    }

    private void buildColorMap() {
        ArrayList<String> allBlueList = new ArrayList<>(Arrays.asList(Defines.allBlue.split("\\s*,\\s*")));
        ArrayList<String> retroList = new ArrayList<>(Arrays.asList(Defines.retro.split("\\s*,\\s*")));
        ArrayList<String> blueYellowList = new ArrayList<>(Arrays.asList(Defines.blueYellow.split("\\s*,\\s*")));
        ArrayList<String> orangePurpleList = new ArrayList<>(Arrays.asList(Defines.orangePurple.split("\\s*,\\s*")));
        ArrayList<String> aquaList = new ArrayList<>(Arrays.asList(Defines.aqua.split("\\s*,\\s*")));
        ArrayList<String> blackPinkList = new ArrayList<>(Arrays.asList(Defines.blackPink.split("\\s*,\\s*")));

        this.colorMap.put("JUST_BLUE", allBlueList);
        this.colorMap.put("RETRO", retroList);
        this.colorMap.put("BLUE_YELLOW", blueYellowList);
        this.colorMap.put("ORANGE_PURPLE", orangePurpleList);
        this.colorMap.put("BLACK_PINK", blackPinkList);
        this.colorMap.put("AQUA", aquaList);

    }

    public void openGlobalVariablesDialog() {
        PrimeFaces.current().ajax().update("globalVariablesDialog");
        PrimeFaces.current().executeScript("PF('globalVariablesDialog').show()");
    }

    public void addNewGlobalVariable() {
        if (this.reportModel.getCustomReportConfig().getGlobalVariables() == null) {
            this.reportModel.getCustomReportConfig().setGlobalVariables(new ArrayList<>());
        }
        this.reportModel.getCustomReportConfig().getGlobalVariables().add("");
    }

    public void deleteGlobalVariable(String variableName) {
        this.reportModel.getCustomReportConfig().getGlobalVariables().remove(variableName);
    }

    @Override
    public void loadRecord() {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("id", "=", Integer.valueOf(super.getRequestParams().get("report"))));
        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setRequestActionType("0");
        reportRequest.setClause(fm.getClause());
        ReportResponse reportResponse = null;
        ReportHandler reportHandler = new ReportHandler();
        try {
            reportResponse = reportHandler.execute(reportRequest);
        }
        catch (Exception ex) {
            Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (reportResponse != null && reportResponse.getErrorCode() == 1000) {
            reportModel = reportResponse.getReturnList().get(0);
            this.reportBasicRecord = this.reportModel.getReportDetails();
            //legacy handling
            if (this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue().equals("2")
                    && reportModel.getReportChart() == null) {
                this.reportModel.setReportChart(new ReportChart());
                this.reportModel.getReportChart().setAxisX(new ChartAxisX());
                this.reportModel.getReportChart().setAxisY(new ChartAxisY());
                this.reportModel.getReportChart().getAxisY().setCountChartValues(new CountChartValues());
                this.reportModel.getReportChart().getAxisY().setFieldSummaryValues(new FieldSummaryValues());
                this.reportModel.getReportChart().getAxisY().setType("RECORD_COUNT");
            }

            if (this.reportModel.getANDFilterList() != null || !this.reportModel.getANDFilterList().isEmpty()
                    || this.reportModel.getORFilterList() != null || !this.reportModel.getORFilterList().isEmpty()) {
                this.showChartFiltering = "true";
            }

            if (reportModel.getChartColorPallete() == null || reportModel.getChartColorPallete().isEmpty()) {
                reportModel.setChartColorPallete("JUST_BLUE");
            }
//            this.loadAvailableColumns();
            this.onBaseModuleSelect();
            super.setRecordModel(reportModel.getReportDetails());
            if (reportModel.getJoinConfiguration() != null && !reportModel.getJoinConfiguration().isEmpty()) {
                for (JoinModel jm : reportModel.getJoinConfiguration()) {
                    this.generateColumnsForJoin(jm);
                }
            }

            if (reportModel.getPivotConfiguration() == null) {
                reportModel.setPivotConfiguration(new PivotConfiguration());
            }

            if (reportModel.getCustomReportConfig() == null) {
                reportModel.setCustomReportConfig(new CustomReportConfig());
            }

            if (reportModel.getANDFilterList() != null && !reportModel.getANDFilterList().isEmpty()) {
                for (ReportFilterModel rfm : reportModel.getANDFilterList()) {
                    if (rfm.getFieldModel() != null) {
                        if (rfm.getFieldModel().getFieldType().equals("LOOKUP")) {
                            this.loadLookupForFilter(rfm.getFieldModel());
                        }
                    }
                }
            }

            if (reportModel.getORFilterList() != null && !reportModel.getORFilterList().isEmpty()) {
                for (ReportFilterModel rfm : reportModel.getORFilterList()) {
                    if (rfm.getFieldModel() != null) {
                        if (rfm.getFieldModel().getFieldType().equals("LOOKUP")) {
                            this.loadLookupForFilter(rfm.getFieldModel());
                        }
                    }
                }
            }

        }
        this.currentStep = 2;
        this.previewReport("RELOAD");
        PrimeFaces.current().ajax().update("reportBuilderStepThree");
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public void openLegacyBuilder() {
        try {
            if (super.getRequestParams().get(Defines.REQUEST_MODE_KEY) != null
                    && super.getRequestParams().get(Defines.REQUEST_MODE_KEY).equals(Defines.REQUEST_MODE_EDIT)) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("addReport.xhtml?report="
                        + super.getRequestParams().get("report") + "&" + Defines.REQUEST_MODE_KEY + "="
                        + Defines.REQUEST_MODE_EDIT);
            }
            else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("addReport.xhtml");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(DealsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void fillFormDetails() {
        super.fillFormDetails();
        if (reportModel != null) {
            PrimeFaces.current().executeScript("PF('" + "primary_module" + "').selectValue(" + reportModel.getPrimaryModule().getModuleId() + ")");
        }
    }

    public void onTabChange(TabChangeEvent event) {
        switch (event.getTab().getId()) {
            case "selectFilters":
                this.generateColumnGroups();
                break;
            case "selectGroups":
                this.generateColumnGroups();
                break;
            case "selectOrders":
                this.generateColumnGroups();
                break;
            case "selectColumns":
                this.getPrimaryModuleRelations(reportModel.getPrimaryModule().getModuleId(), reportModel.getPrimaryModule());
                this.generateColumnGroups();
                break;
            case "configureComparitiveAnalysisTab":
                if (reportModel.isEnableComparitiveDataset() && reportModel.getComparitiveDatasetConfigs() != null) {
                    for (ComparitiveDatasetConfig cdc : reportModel.getComparitiveDatasetConfigs()) {
                        if (cdc.getComparitiveDatasetANDFilterList() != null) {
                            for (ReportFilterModel rfm : cdc.getComparitiveDatasetANDFilterList()) {
                                if (rfm.getFieldModel().getFieldType().equals("LOOKUP")) {
                                    this.loadLookupForFilter(rfm.getFieldModel());
                                }
                            }
                        }

                        if (cdc.getComparitiveDatasetORFilterList() != null) {
                            for (ReportFilterModel rfm : cdc.getComparitiveDatasetORFilterList()) {
                                if (rfm.getFieldModel().getFieldType().equals("LOOKUP")) {
                                    this.loadLookupForFilter(rfm.getFieldModel());
                                }
                            }
                        }
                    }
                }
                break;
        }
    }

    public void getPrimaryModuleRelations(int moduleId, ModuleModel parentModule) {
        try {
            this.relationHeirarchyMap.put(parentModule, new ArrayList<>());
            RelationRequest request = new RelationRequest();
            request.setActionType("4");
            request.setModuleId(String.valueOf(moduleId));
            RelationHandler relationHandler = new RelationHandler();
            RelationResponse response = relationHandler.relationExecutor(request);
            if (response.getErrorCode() == 1000) {
                for (RelationManagerModel rmm : response.getRelationManagerList()) {
                    for (ModuleModel mm : this.moduleList) {
                        if (mm.getModuleId() == rmm.getDetailModuleId()) {
                            if (!relatedModules.contains(mm)) {
                                for (Map.Entry<ModuleModel, ArrayList<ModuleModel>> entry : this.relationHeirarchyMap.entrySet()) {
                                    if (entry.getKey().getModuleId() == parentModule.getModuleId()) {
                                        if (entry.getValue() == null) {
                                            entry.setValue(new ArrayList<>());
                                        }

                                        entry.getValue().add(mm);
                                    }
                                }
                                this.relatedModules.add(mm);
                                this.getPrimaryModuleRelations(mm.getModuleId(), mm);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onSelect(String fieldName, String selection) {
        if (selection != null) {
            switch (fieldName) {
                case "primary_module":
                    for (ModuleModel mm : this.moduleList) {
                        if (Integer.valueOf(selection) == mm.getModuleId()) {
                            this.reportModel.setPrimaryModule(mm);
                            this.selectedCache.put(fieldName, selectionMap.get(fieldName).getSelectedValue());
                            this.loadAvailableColumns();
                            break;
                        }
                    }
                    break;
                case "report_type":
                    super.onSelect(fieldName, selection);
                    if (selection.equals("2")) {
                        if (this.getDefaultForm() != null) {
                            for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
                                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                                    if (flm.getFieldObject().getFieldName().equals("graph_type")) {
                                        flm.setIsVisible(true);
                                        this.showChartColors = true;
                                        PrimeFaces.current().ajax().update("reportControlsMenuHolder");
                                        PrimeFaces.current().ajax().update("tabHolder:containerPanel");
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else if (selection.equals("3")) {
                        if (this.reportModel.getCustomReportConfig() == null) {
                            this.reportModel.setCustomReportConfig(new CustomReportConfig());
                            this.reportModel.getCustomReportConfig().setTemplateParameters(new ArrayList<>());
                        }

                        if (this.getDefaultForm() != null) {
                            for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
                                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                                    if (flm.getFieldObject().getFieldName().equals("graph_type")) {
                                        this.showChartColors = false;
                                        flm.setIsVisible(false);
                                        PrimeFaces.current().ajax().update("reportControlsMenuHolder");
                                        PrimeFaces.current().ajax().update("tabHolder:containerPanel");
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else {
                        if (this.getDefaultForm() != null) {
                            for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
                                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                                    if (flm.getFieldObject().getFieldName().equals("graph_type")) {
                                        this.showChartColors = false;
                                        flm.setIsVisible(false);
                                        PrimeFaces.current().ajax().update("reportControlsMenuHolder");
                                        PrimeFaces.current().ajax().update("tabHolder:containerPanel");
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                default:
                    super.onSelect(fieldName, selection);
                    break;

            }
        }
    }

    @Override
    protected void loadLookup(String fieldName, String lookupId, String... apiKey) {
        if (fieldName.equals("primary_module")) {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response;
            try {
                response = moduleHandler.managerExecutor(request);
                if (response.getErrorCode() == 1000) {
                    super.selectedCache.put(fieldName, null);
                    TreeMap<String, String> temp = new TreeMap<>();
                    for (ModuleModel mm : response.getReturnList()) {
                        temp.put(mm.getModuleName(), String.valueOf(mm.getModuleId()));
                        super.selectionMap.put(fieldName, new SelectionModel());
                    }
                    super.lookupDictionary.put(fieldName, temp);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            super.loadLookup(fieldName, lookupId);
        }
    }

    public void loadLookupForFilter(ModuleFieldModel moduleFieldModel) {
        if (moduleFieldModel.getFieldType().equals("LOOKUP")) {
            super.loadLookup(moduleFieldModel.getFieldName(), moduleFieldModel.getFieldValues());
        }
    }

    public void generateColumnGroups() {
        if (reportModel != null) {
            filterColumnList = new ArrayList<>();
            filterColumnSimpleList = new ArrayList<>();
            SelectItemGroup primaryColumnsGroup = null;
            SelectItemGroup primaryColumnsGroupSimple = null;
            if (reportModel.getPrimaryModule() != null) {
                primaryColumnsGroup = new SelectItemGroup(reportModel.getPrimaryModule().getModuleName());
                primaryColumnsGroupSimple = new SelectItemGroup(reportModel.getPrimaryModule().getModuleName());
                ArrayList<SelectItem> columItemList = new ArrayList<>();
                ArrayList<SelectItem> columItemListSimple = new ArrayList<>();
                for (ModuleFieldModel mfm : this.availableColumnList) {
                    if (mfm.getModuleId() == this.reportModel.getPrimaryModule().getModuleId()) {
                        columItemList.add(new SelectItem(mfm, mfm.getFieldLabel()));
                        columItemListSimple.add(new SelectItem(mfm.getFieldName(), mfm.getFieldLabel()));
                    }
                }
                primaryColumnsGroup.setSelectItems(columItemList.toArray(new SelectItem[columItemList.size()]));
                primaryColumnsGroupSimple.setSelectItems(columItemListSimple.toArray(new SelectItem[columItemListSimple.size()]));
            }
            filterColumnList.add(primaryColumnsGroup);
            filterColumnSimpleList.add(primaryColumnsGroupSimple);

            if (this.relatedModules != null && !this.relatedModules.isEmpty() && reportModel.getRelatedModulesList() != null && reportModel.getRelatedModulesList().isEmpty()) {
                for (ModuleModel relMod : relatedModules) {
                    reportModel.getRelatedModulesList().add(relMod);
                }
            }
            if (reportModel.getRelatedModulesList() != null) {
                for (ModuleModel mm : reportModel.getRelatedModulesList()) {
                    SelectItemGroup columnsGroup = new SelectItemGroup(mm.getModuleName());
                    ArrayList<SelectItem> columnItemList = new ArrayList<>();
                    SelectItemGroup columnsGroupSimple = new SelectItemGroup(mm.getModuleName());
                    ArrayList<SelectItem> columnItemListSimple = new ArrayList<>();
                    for (ModuleFieldModel mfm : this.availableColumnList) {
                        if (mfm.getModuleId() == mm.getModuleId()) {
                            columnItemList.add(new SelectItem(mfm, mfm.getFieldLabel()));
                            columnItemListSimple.add(new SelectItem(mfm.getFieldName(), mfm.getFieldLabel()));
                        }
                    }
                    columnsGroup.setSelectItems(columnItemList.toArray(new SelectItem[columnItemList.size()]));
                    columnsGroupSimple.setSelectItems(columnItemListSimple.toArray(new SelectItem[columnItemListSimple.size()]));
                    filterColumnList.add(columnsGroup);
                    filterColumnSimpleList.add(columnsGroupSimple);
                }
            }

            if (reportModel.getAggregatesList() != null && !reportModel.getAggregatesList().isEmpty()) {
                SelectItemGroup columnsGroup = new SelectItemGroup("Aggreagte Fields");
                ArrayList<SelectItem> columnItemList = new ArrayList<>();

                SelectItemGroup columnsGroupSimple = new SelectItemGroup("Aggreagte Fields");
                ArrayList<SelectItem> columnItemListSimple = new ArrayList<>();
                for (ModuleFieldModel mfm : this.availableColumnList) {
                    if (mfm.getFieldType().equals("AGGREGATE")) {
                        columnItemList.add(new SelectItem(mfm, mfm.getFieldName()));
                        columnItemListSimple.add(new SelectItem(mfm.getFieldName(), mfm.getFieldName()));
                    }
                }
                columnsGroup.setSelectItems(columnItemList.toArray(new SelectItem[columnItemList.size()]));
                columnsGroupSimple.setSelectItems(columnItemListSimple.toArray(new SelectItem[columnItemListSimple.size()]));
                filterColumnList.add(columnsGroup);
                filterColumnSimpleList.add(columnsGroupSimple);
            }

            if (reportModel.getCustomFields() != null && !reportModel.getCustomFields().isEmpty()) {
                SelectItemGroup columnsGroup = new SelectItemGroup("Custom Fields");
                ArrayList<SelectItem> columnItemList = new ArrayList<>();

                SelectItemGroup columnsGroupSimple = new SelectItemGroup("Custom Fields");
                ArrayList<SelectItem> columnItemListSimple = new ArrayList<>();
                for (ModuleFieldModel mfm : this.availableColumnList) {
                    if (mfm.getFieldType().equals("CUSTOM")) {
                        columnItemList.add(new SelectItem(mfm, mfm.getFieldName()));
                        columnItemListSimple.add(new SelectItem(mfm.getFieldName(), mfm.getFieldName()));
                    }
                }
                columnsGroup.setSelectItems(columnItemList.toArray(new SelectItem[columnItemList.size()]));
                columnsGroupSimple.setSelectItems(columnItemListSimple.toArray(new SelectItem[columnItemListSimple.size()]));
                filterColumnList.add(columnsGroup);
                filterColumnSimpleList.add(columnsGroupSimple);
            }
        }
    }

    public void addJoinStatement() {
        if (this.reportModel.getJoinConfiguration() != null) {
            this.reportModel.getJoinConfiguration().add(new JoinModel());
        }
        else {
            this.reportModel.setJoinConfiguration(new ArrayList<>());
            this.reportModel.getJoinConfiguration().add(new JoinModel());
        }
    }

    public void onCustomColumnAdd(SelectEvent event) {
        String attrValue = (String) event.getObject();
        ModuleFieldModel mfm = new ModuleFieldModel();
        mfm.setFieldId(GeneralUtils.generateRandomId());
        mfm.setFieldName(attrValue);
        mfm.setFieldType("CUSTOM");
        this.availableColumnList.add(mfm);
        this.generateColumnGroups();
    }

    public void removeJoinStatement(JoinModel joinModel) {
        this.reportModel.getJoinConfiguration().remove(joinModel);
    }

    public void addNewANDCondition() {
        if (this.reportModel.getANDFilterList() != null) {
            this.reportModel.getANDFilterList().add(new ReportFilterModel());
        }
        else {
            this.reportModel.setANDFilterList(new ArrayList<>());
            this.reportModel.getANDFilterList().add(new ReportFilterModel());
        }
    }

    public void addNewORCondition() {
        if (this.reportModel.getORFilterList() != null) {
            this.reportModel.getORFilterList().add(new ReportFilterModel());
        }
        else {
            this.reportModel.setORFilterList(new ArrayList<>());
            this.reportModel.getORFilterList().add(new ReportFilterModel());
        }
    }

    public void addNewDataset() {
        if (this.reportModel.getComparitiveDatasetConfigs() == null) {
            this.reportModel.setComparitiveDatasetConfigs(new ArrayList<>());
        }

        ComparitiveDatasetConfig cdc = new ComparitiveDatasetConfig();
        cdc.setSequenceId(GeneralUtils.generateIntegerId());
        cdc.setComparitiveDatasetANDFilterList(new ArrayList<>());
        cdc.setComparitiveDatasetORFilterList(new ArrayList<>());
        this.reportModel.getComparitiveDatasetConfigs().add(cdc);
    }

    public void removeDataset(ComparitiveDatasetConfig comparitiveDatasetConfig) {
    }

    public void addAndConditionForDataset(ComparitiveDatasetConfig comparitiveDatasetConfig) {
        if (reportModel.getComparitiveDatasetConfigs() != null) {
            for (ComparitiveDatasetConfig cdc : reportModel.getComparitiveDatasetConfigs()) {
                if (cdc.getSequenceId() == comparitiveDatasetConfig.getSequenceId()) {
                    cdc.getComparitiveDatasetANDFilterList().add(new ReportFilterModel());
                    break;
                }
            }
        }
    }

    public void removeANDConditionForDataset(ComparitiveDatasetConfig comparitiveDatasetConfig, ReportFilterModel rfm) {
    }

    public void addOrConditionForDataset(ComparitiveDatasetConfig comparitiveDatasetConfig) {
        if (reportModel.getComparitiveDatasetConfigs() != null) {
            for (ComparitiveDatasetConfig cdc : reportModel.getComparitiveDatasetConfigs()) {
                if (cdc.getSequenceId() == comparitiveDatasetConfig.getSequenceId()) {
                    cdc.getComparitiveDatasetORFilterList().add(new ReportFilterModel());
                    break;
                }
            }
        }
    }

    public void removeOrConditionForDataset(ComparitiveDatasetConfig comparitiveDatasetConfig, ReportFilterModel rfm) {
    }

    public void addNewAggregateFunction() {
        AggregatModel aggregatModel = new AggregatModel();
        aggregatModel.setRuntimeId(GeneralUtils.generateRandomId());
        if (this.reportModel.getAggregatesList() != null) {
            this.reportModel.getAggregatesList().add(aggregatModel);
        }
        else {
            this.reportModel.setAggregatesList(new ArrayList<>());
            this.reportModel.getAggregatesList().add(aggregatModel);
        }

        ModuleFieldModel mfm = new ModuleFieldModel();
        if (aggregatModel.getRuntimeId() != 0) {
            mfm.setFieldId(aggregatModel.getRuntimeId());
        }
        else {
            aggregatModel.setRuntimeId(GeneralUtils.generateRandomId());
            mfm.setFieldId(aggregatModel.getRuntimeId());
        }
        mfm.setFieldName(aggregatModel.getAlias());
        mfm.setFieldType("AGGREGATE");
        this.availableColumnList.add(mfm);
    }

    public void removeAggregateFunction(AggregatModel aggregatModel) {
        ModuleFieldModel aggField = new ModuleFieldModel();
        for (ModuleFieldModel mfm : this.availableColumnList) {
            if (mfm.getFieldId() == aggregatModel.getRuntimeId()) {
                aggField = mfm;
                break;
            }
        }
        this.availableColumnList.remove(aggField);
        this.reportModel.getAggregatesList().remove(aggregatModel);
    }

    public void addNewGroup() {
        if (this.reportModel.getGroupByList() != null) {
            this.reportModel.getGroupByList().add(new ModuleFieldModel());
        }
        else {
            this.reportModel.setGroupByList(new ArrayList<>());
            this.reportModel.getGroupByList().add(new ModuleFieldModel());
        }
    }

    public void addNewOrderBy() {
        if (this.reportModel.getOrderByList() != null) {
            this.reportModel.getOrderByList().add(new OrderByConfig());
        }
        else {
            this.reportModel.setOrderByList(new ArrayList<>());
            this.reportModel.getOrderByList().add(new OrderByConfig());
        }
    }

    public void removeCondition(String conditionType, ReportFilterModel rfm) {
        if (conditionType.equals("AND")) {
            this.reportModel.getANDFilterList().remove(rfm);
        }
        else {
            this.reportModel.getORFilterList().remove(rfm);
        }
    }

    public void removeANDCondition(ReportFilterModel rfm) {
        this.reportModel.getANDFilterList().remove(rfm);
    }

    public void removeORCondition(ReportFilterModel rfm) {
        this.reportModel.getORFilterList().remove(rfm);
    }

    public void removeGroup(ModuleFieldModel mfm) {
        this.reportModel.getGroupByList().remove(mfm);
        this.previewReport();
    }

    public void removeOrderBy(OrderByConfig mfm) {
        this.reportModel.getOrderByList().remove(mfm);
        this.previewReport();
    }

    public void clearAllFilters() {
        if (this.showChartFiltering.equals("false")) {
            this.reportModel.setANDFilterList(new ArrayList<>());
            this.reportModel.setORFilterList(new ArrayList<>());
            this.previewReport();
        }
    }

    public void removeComparitiveDataset(ComparitiveDatasetConfig datasetConfig) {
        this.reportModel.getComparitiveDatasetConfigs().remove(datasetConfig);
    }

    public void removeComparitiveDatasetAndFilter(ComparitiveDatasetConfig datasetConfig, ReportFilterModel filterModel) {
        datasetConfig.getComparitiveDatasetANDFilterList().remove(filterModel);
    }

    public void removeComparitiveDatasetOrFilter(ComparitiveDatasetConfig datasetConfig, ReportFilterModel filterModel) {
        datasetConfig.getComparitiveDatasetORFilterList().remove(filterModel);
    }

    public List<ModuleModel> completeModuleList(String query) {
//        List<ModuleModel> allModules = this.getRelatedModules();
        List<ModuleModel> filteredModules = new ArrayList<>();
        for (ModuleModel module : moduleList) {
            if (module.getModuleName().toLowerCase().startsWith(query)) {
                filteredModules.add(module);
            }
        }
        return filteredModules;
    }

    public List<ModuleFieldModel> completeFieldList(String query) {
        List<ModuleFieldModel> allFields = this.getAvailableColumnList();
        List<ModuleFieldModel> filtiredFields = new ArrayList<>();
        for (ModuleFieldModel mfm : allFields) {
            if (mfm.getFieldLabel() != null) {
                if (mfm.getFieldLabel().toLowerCase().contains(query)) {
                    filtiredFields.add(mfm);
                }
            }
            else {
                filtiredFields.add(mfm);
            }
        }
        Collections.sort(filtiredFields);
        return filtiredFields;
    }

    private ArrayList<ModuleModel> getModuleService() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response;
            response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                return response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void onModuleSelect(SelectEvent event) {
//        if (this.reportModel.getRelatedModulesList() == null) {
//            this.reportModel.setRelatedModulesList(new ArrayList<>());
//        }
//        this.reportModel.getRelatedModulesList().add((ModuleModel) event.getObject());
        this.loadAvailableColumns();
    }

    public void onColumnSelect(SelectEvent event) {
//        if(this.reportModel.getFieldList() == null){
//            this.reportModel.setFieldList(new ArrayList<>());
//        }
//        this.reportModel.getFieldList().add((ModuleFieldModel) event.getObject());
//        this.getAvailableColumnList().remove((ModuleFieldModel) event.getObject());
        if (this.reportModel.getFieldList() != null) {
            for (ModuleFieldModel mfm : this.reportModel.getFieldList()) {
                if (mfm.getModuleId() != this.reportModel.getPrimaryModule().getModuleId()) {
                    for (ModuleModel mm : this.relatedModules) {
                        if (mfm.getModuleId() == mm.getModuleId()) {
                            if (reportModel.getRelatedModulesList() == null) {
                                reportModel.setRelatedModulesList(new ArrayList<>());
                            }

                            for (Map.Entry<ModuleModel, ArrayList<ModuleModel>> entry : this.relationHeirarchyMap.entrySet()) {
                                for (ModuleModel relModel : entry.getValue()) {
                                    if (relModel.getModuleId() == mfm.getModuleId()) {
                                        if (!reportModel.getRelatedModulesList().contains(entry.getKey())) {
                                            reportModel.getRelatedModulesList().add(entry.getKey());
                                        }
                                        if (!reportModel.getRelatedModulesList().contains(relModel)) {
                                            reportModel.getRelatedModulesList().add(relModel);
                                        }
                                    }
                                }
                            }

                            this.loadAvailableColumns();

//                            if (!reportModel.getRelatedModulesList().contains(mm)) {
//                                reportModel.getRelatedModulesList().add(mm);
//                            }
                        }
                    }
                }
            }
        }
        this.previewReport();
    }

    public void onSelectFromViewToggle() {
        this.reportModel.setViewStatement(new ViewStatement());
    }

    public void onColumnUnselect(UnselectEvent event) {
        ModuleFieldModel moduleFieldModel = (ModuleFieldModel) event.getObject();
        this.reportModel.getFieldList().remove(moduleFieldModel);
        this.reportModel.setRelatedModulesList(new ArrayList<>());
        if (this.reportModel.getFieldList() != null) {
            for (ModuleFieldModel mfm : this.reportModel.getFieldList()) {
                if (mfm.getModuleId() != this.reportModel.getPrimaryModule().getModuleId()) {
                    for (ModuleModel mm : this.relatedModules) {
                        if (mfm.getModuleId() == mm.getModuleId()) {
                            if (reportModel.getRelatedModulesList() == null) {
                                reportModel.setRelatedModulesList(new ArrayList<>());
                            }
//                            if (!reportModel.getRelatedModulesList().contains(mm)) {
//                                reportModel.getRelatedModulesList().add(mm);
//                            }

                            for (Map.Entry<ModuleModel, ArrayList<ModuleModel>> entry : this.relationHeirarchyMap.entrySet()) {
                                for (ModuleModel relModel : entry.getValue()) {
                                    if (relModel.getModuleId() == mfm.getModuleId()) {
                                        if (!reportModel.getRelatedModulesList().contains(entry.getKey())) {
                                            reportModel.getRelatedModulesList().add(entry.getKey());
                                        }
                                        if (!reportModel.getRelatedModulesList().contains(relModel)) {
                                            reportModel.getRelatedModulesList().add(relModel);
                                        }
                                    }
                                }
                            }

                            this.loadAvailableColumns();
                        }
                    }
                }
            }
        }
        this.previewReport();
    }

    public void loadAvailableColumns() {
        if (this.reportModel.getPrimaryModule() != null) {
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("modules_field_configuration.module_id", "=", this.reportModel.getPrimaryModule().getModuleId()));
            if (this.relatedModules != null) {
                for (ModuleModel mm : this.relatedModules) {
                    fm.setFilter("OR", new FilterType("modules_field_configuration.module_id", "=", mm.getModuleId()));
                }
            }

            if (reportModel.getRelatedModulesList() != null) {
                for (ModuleModel mm : this.reportModel.getRelatedModulesList()) {
                    fm.setFilter("OR", new FilterType("modules_field_configuration.module_id", "=", mm.getModuleId()));
                }
            }

            try {
//                this.availableColumnList = super.getFieldSetService(fm.getClause(), String.valueOf(this.reportModel.getPrimaryModule().getModuleId()));

                ArrayList<ModuleFieldModel> customDateFields = new ArrayList<>();
                this.availableColumnList = super.getFieldSetService(fm.getClause(), null, false);

                for (ModuleFieldModel mfm : availableColumnList) {
                    if (mfm.getFieldType().equals("DATE")) {
                        ModuleFieldModel functionDayField = new ModuleFieldModel(mfm);
                        int newLastIdNumber = Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(String.valueOf(mfm.getFieldId()).length() - 1)) + 1;
                        if (newLastIdNumber > 9) {
                            functionDayField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 2) + newLastIdNumber));
                        }
                        else {
                            functionDayField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        }
//                        functionDayField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        functionDayField.setFieldLabel("Day of " + mfm.getFieldLabel());
                        functionDayField.setFieldType("DATE_FUNCTION");
                        functionDayField.setFieldValues("DAY");
                        customDateFields.add(functionDayField);

                        ModuleFieldModel functionMonthField = new ModuleFieldModel(mfm);
                        newLastIdNumber = Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(String.valueOf(mfm.getFieldId()).length() - 1)) + 2;
                        if (newLastIdNumber > 9) {
                            functionMonthField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 2) + newLastIdNumber));
                        }
                        else {
                            functionMonthField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        }
//                        functionMonthField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        functionMonthField.setFieldLabel("Month of " + mfm.getFieldLabel());
                        functionMonthField.setFieldType("DATE_FUNCTION");
                        functionMonthField.setFieldValues("MONTH");
                        customDateFields.add(functionMonthField);

                        ModuleFieldModel functionYearField = new ModuleFieldModel(mfm);
                        newLastIdNumber = Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(String.valueOf(mfm.getFieldId()).length() - 1)) + 3;
                        if (newLastIdNumber > 9) {
                            functionYearField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 2) + newLastIdNumber));
                        }
                        else {
                            functionYearField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        }
//                        functionYearField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        functionYearField.setFieldLabel("Year of " + mfm.getFieldLabel());
                        functionYearField.setFieldType("DATE_FUNCTION");
                        functionYearField.setFieldValues("YEAR");
                        customDateFields.add(functionYearField);

                        ModuleFieldModel functionMonthNameField = new ModuleFieldModel(mfm);
                        newLastIdNumber = Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(String.valueOf(mfm.getFieldId()).length() - 1)) + 4;
                        if (newLastIdNumber > 9) {
                            functionMonthNameField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 2) + newLastIdNumber));
                        }
                        else {
                            functionMonthNameField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        }
//                        functionMonthNameField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        functionMonthNameField.setFieldLabel("Month Name of " + mfm.getFieldLabel());
                        functionMonthNameField.setFieldType("DATE_FUNCTION");
                        functionMonthNameField.setFieldValues("MONTHNAME");
                        customDateFields.add(functionMonthNameField);
                    }
                    else if (mfm.getFieldType().equals("INT") || mfm.getFieldType().equals("DECIMAL") || mfm.getFieldType().equals("CURRENCY")) {
                        ModuleFieldModel functionCountField = new ModuleFieldModel(mfm);
                        int newLastIdNumber = Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(String.valueOf(mfm.getFieldId()).length() - 1)) + 5;
                        if (newLastIdNumber > 9) {
                            functionCountField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 2) + newLastIdNumber));
                        }
                        else {
                            functionCountField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        }
                        functionCountField.setFieldLabel("Count of " + mfm.getFieldLabel());
                        functionCountField.setFieldType("AGG_FUNCTION");
                        functionCountField.setFieldValues("COUNT");
                        customDateFields.add(functionCountField);

                        ModuleFieldModel functionCountDistinctField = new ModuleFieldModel(mfm);
                        newLastIdNumber = Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(String.valueOf(mfm.getFieldId()).length() - 1)) + 6;
                        if (newLastIdNumber > 9) {
                            functionCountDistinctField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 2) + newLastIdNumber));
                        }
                        else {
                            functionCountDistinctField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        }
//                        functionCountDistinctField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        functionCountDistinctField.setFieldLabel("Unique Count of " + mfm.getFieldLabel());
                        functionCountDistinctField.setFieldType("AGG_FUNCTION");
                        functionCountDistinctField.setFieldValues("COUNT_DISTINCT");
                        customDateFields.add(functionCountDistinctField);

                        ModuleFieldModel functionSumField = new ModuleFieldModel(mfm);
                        newLastIdNumber = Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(String.valueOf(mfm.getFieldId()).length() - 1)) + 7;
                        if (newLastIdNumber > 9) {
                            functionSumField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 2) + newLastIdNumber));
                        }
                        else {
                            functionSumField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        }
//                        functionSumField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        functionSumField.setFieldLabel("Sum of " + mfm.getFieldLabel());
                        functionSumField.setFieldType("AGG_FUNCTION");
                        functionSumField.setFieldValues("SUM");
                        customDateFields.add(functionSumField);

                        ModuleFieldModel functionAverageField = new ModuleFieldModel(mfm);
                        newLastIdNumber = Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(String.valueOf(mfm.getFieldId()).length() - 1)) + 8;
                        if (newLastIdNumber > 9) {
                            functionAverageField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 2) + newLastIdNumber));
                        }
                        else {
                            functionAverageField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        }
//                        functionAverageField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
                        functionAverageField.setFieldLabel("Average of " + mfm.getFieldLabel());
                        functionAverageField.setFieldType("AGG_FUNCTION");
                        functionAverageField.setFieldValues("AVG");
                        customDateFields.add(functionAverageField);
                    }
//                    else if (mfm.getFieldType().equals("USER")) {
//                        ModuleFieldModel userNameField = new ModuleFieldModel(mfm);
//                        int newLastIdNumber = Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(String.valueOf(mfm.getFieldId()).length() - 1)) + 8;
//                        if (newLastIdNumber > 9) {
//                            userNameField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 2) + newLastIdNumber));
//                        }
//                        else {
//                            userNameField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + newLastIdNumber));
//                        }
//
//                        userNameField = new ModuleFieldModel();
//                        userNameField.setFieldName(mfm.getFieldName() + ".name");
//                        userNameField.setFieldLabel(mfm.getFieldLabel() + " Name");
//                        userNameField.setFieldType("USER_NAME");
//                        userNameField.setFieldValues(null);
//                        userNameField.setIsCustom(false);
//                        userNameField.setIsKey(false);
//                        userNameField.setIsMandatory(false);
//                        userNameField.setIsVisible(true);
//                        userNameField.setCurrentValue("");
//                        userNameField.setModuleId(mfm.getModuleId());
//                        userNameField.setTableName(mfm.getTableName());
//                        
//                        this.availableColumnList.add(userNameField);
//                    }
                }

//                for (ModuleFieldModel mfm : availableColumnList) {
//                    if (mfm.getFieldType().equals("DATE")) {
//                        ModuleFieldModel functionDayField = new ModuleFieldModel(mfm);
//                        functionDayField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + "0"));
//                        functionDayField.setFieldLabel("Day of " + mfm.getFieldLabel());
//                        functionDayField.setFieldType("DATE_FUNCTION");
//                        functionDayField.setFieldValues("DAY");
//                        functionDayField.setFieldName(mfm.getFieldName() + ".agg_func.day");
//                        customDateFields.add(functionDayField);
//
//                        ModuleFieldModel functionMonthField = new ModuleFieldModel(mfm);
//                        functionMonthField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + "1"));
//                        functionMonthField.setFieldLabel("Month of " + mfm.getFieldLabel());
//                        functionMonthField.setFieldType("DATE_FUNCTION");
//                        functionMonthField.setFieldValues("MONTH");
//                        functionMonthField.setFieldName(mfm.getFieldName() + ".agg_func.month");
//                        customDateFields.add(functionMonthField);
//
//                        ModuleFieldModel functionYearField = new ModuleFieldModel(mfm);
//                        functionYearField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + "2"));
//                        functionYearField.setFieldLabel("Year of " + mfm.getFieldLabel());
//                        functionYearField.setFieldType("DATE_FUNCTION");
//                        functionYearField.setFieldValues("YEAR");
//                        functionYearField.setFieldName(mfm.getFieldName() + ".agg_func.year");
//                        customDateFields.add(functionYearField);
//
//                        ModuleFieldModel functionMonthNameField = new ModuleFieldModel(mfm);
//                        functionMonthNameField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + "3"));
//                        functionMonthNameField.setFieldLabel("Month Name of " + mfm.getFieldLabel());
//                        functionMonthNameField.setFieldType("DATE_FUNCTION");
//                        functionMonthNameField.setFieldValues("MONTHNAME");
//                        functionMonthNameField.setFieldName(mfm.getFieldName() + ".agg_func.monthname");
//                        customDateFields.add(functionMonthNameField);
//                    }
//                    else if (mfm.getFieldType().equals("INT") || mfm.getFieldType().equals("DECIMAL") || mfm.getFieldType().equals("CURRENCY")) {
//                        ModuleFieldModel functionCountField = new ModuleFieldModel(mfm);
//                        functionCountField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + "4"));
//                        functionCountField.setFieldLabel("Count of " + mfm.getFieldLabel());
//                        functionCountField.setFieldType("AGG_FUNCTION");
//                        functionCountField.setFieldValues("COUNT");
//                        functionCountField.setFieldName(mfm.getFieldName() + ".agg_func");
//                        customDateFields.add(functionCountField);
//
//                        ModuleFieldModel functionCountDistinctField = new ModuleFieldModel(mfm);
//                        functionCountDistinctField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + "5"));
//                        functionCountDistinctField.setFieldLabel("Unique Count of " + mfm.getFieldLabel());
//                        functionCountDistinctField.setFieldType("AGG_FUNCTION");
//                        functionCountDistinctField.setFieldValues("COUNT_DISTINCT");
//                        functionCountDistinctField.setFieldName(mfm.getFieldName() + ".agg_func");
//                        customDateFields.add(functionCountDistinctField);
//
//                        ModuleFieldModel functionSumField = new ModuleFieldModel(mfm);
//                        functionSumField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + "6"));
//                        functionSumField.setFieldLabel("Sum of " + mfm.getFieldLabel());
//                        functionSumField.setFieldType("AGG_FUNCTION");
//                        functionSumField.setFieldValues("SUM");
//                        functionSumField.setFieldName(mfm.getFieldName() + ".agg_func");
//                        customDateFields.add(functionSumField);
//
//                        ModuleFieldModel functionAverageField = new ModuleFieldModel(mfm);
//                        functionAverageField.setFieldId(Integer.valueOf(String.valueOf(mfm.getFieldId()).substring(0, String.valueOf(mfm.getFieldId()).length() - 1) + "7"));
//                        functionAverageField.setFieldLabel("Average of " + mfm.getFieldLabel());
//                        functionAverageField.setFieldType("AGG_FUNCTION");
//                        functionAverageField.setFieldValues("AVG");
//                        functionAverageField.setFieldName(mfm.getFieldName() + ".agg_func");
//                        customDateFields.add(functionAverageField);
//                    }
//                }
                this.availableColumnList.addAll(customDateFields);

                if (reportModel.getAggregatesList() != null && !reportModel.getAggregatesList().isEmpty()) {
                    for (AggregatModel aggregatModel : reportModel.getAggregatesList()) {
                        ModuleFieldModel mfm = new ModuleFieldModel();
                        if (aggregatModel.getRuntimeId() != 0) {
                            mfm.setFieldId(aggregatModel.getRuntimeId());
                        }
                        else {
                            aggregatModel.setRuntimeId(GeneralUtils.generateRandomId());
                            mfm.setFieldId(aggregatModel.getRuntimeId());
                        }
                        mfm.setFieldName(aggregatModel.getAlias());
                        mfm.setFieldType("AGGREGATE");
                        this.availableColumnList.add(mfm);
                    }
                }

                if (reportModel.getGroupByList() != null && !reportModel.getGroupByList().isEmpty()) {
                }

                if (reportModel.getCustomFields() != null && !reportModel.getCustomFields().isEmpty()) {
                    for (String customFiled : reportModel.getCustomFields()) {
                        ModuleFieldModel mfm = new ModuleFieldModel();
                        mfm.setFieldId(GeneralUtils.generateRandomId());
                        mfm.setFieldName(customFiled);
                        mfm.setFieldType("CUSTOM");
                        this.availableColumnList.add(mfm);
                    }
                }

                this.generateColumnGroups();
                this.generateAvailableAggregateColumns();
            }
            catch (Exception ex) {
                Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void generateAvailableAggregateColumns() {
        this.availableColumnListForAggregates = new ArrayList<>();
        if (this.availableColumnList != null) {
            for (ModuleFieldModel mfm : this.availableColumnList) {
                if (mfm.getFieldType().equals("INT") || mfm.getFieldType().equals("DECIMAL") || mfm.getFieldType().equals("LONG")) {
                    this.availableColumnListForAggregates.add(new ModuleFieldModel(mfm));
                }
            }
        }
    }

    public void generateColumnsForJoin(JoinModel joinModel) {
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("modules_field_configuration.module_id", "=", joinModel.getJoinModule().getModuleId()));
        this.currentFieldsForJoin = super.getFieldSetService(fm.getClause(), null, false);
        this.availableColumnList.addAll(currentFieldsForJoin);
        this.moduleFieldMap.put(String.valueOf(joinModel.getJoinModule().getModuleId()), currentFieldsForJoin);
    }

    public void onJoinColumnChoose(JoinModel joinModel) {
        if (relatedModules != null) {
            for (ModuleModel mm : relatedModules) {
                if (mm.getModuleId() == joinModel.getOnField().getModuleId()) {
                    if (this.reportModel.getRelatedModulesList() == null) {
                        this.reportModel.setRelatedModulesList(new ArrayList<>());
                    }

                    this.reportModel.getRelatedModulesList().add(mm);
                    break;
                }
            }
        }
    }

    public String getFieldGroup(ModuleFieldModel mfm) {
        for (ModuleModel mm : this.moduleList) {
            if (mfm.getModuleId() == mm.getModuleId()) {
                return mm.getModuleName();
            }
        }
        return null;
    }

    public void generateReport() {
//        ReportGenerator gr = new ReportGenerator();
//        try {
//            RequestContext context = RequestContext.getCurrentInstance();
//            context.execute("PF('reportDialog').show()");
//        }
//        catch (Exception ex) {
//            Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
//        }
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("reportViewer.xhtml?moduleId=" + super.getModuleId()
                    + "&report=" + this.reportModel.getReportDetails().getFieldValueMap().get("id").getCurrentValue());
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String onCustomReportFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            if (event.getNewStep().equals("templateConfig")) {
                if (this.reportModel.getCustomReportConfig() == null) {
                    this.reportModel.setCustomReportConfig(new CustomReportConfig());
                }
                String customTemplate = this.reportModel.getCustomReportConfig().getHtmlContent();
                ArrayList<DocTemplateParameter> tempParamList = null;
                if (this.reportModel.getCustomReportConfig().getTemplateParameters() != null) {
                    tempParamList = new ArrayList<>(this.reportModel.getCustomReportConfig().getTemplateParameters());
                }

                this.docGenComponentManager = new DocGenComponentManager();
                ArrayList<DocTemplateParameter> refreshedParamList = this.docGenComponentManager.loadTemplateParametersList(customTemplate);
                if (refreshedParamList != null && tempParamList != null) {
                    for (DocTemplateParameter dtpR : refreshedParamList) {
                        for (DocTemplateParameter dtpT : tempParamList) {
                            if (dtpR.getKeyName().equals(dtpT.getKeyName())) {
                                dtpR.setKeyValue(dtpT.getKeyValue());
                            }
                        }
                    }
                }

                this.reportModel.getCustomReportConfig().setTemplateParameters(refreshedParamList);
            }
            return event.getNewStep();
        }
    }

    @Override
    public void save() {
        validationResponse = this.validateCustomQueries();
        if (validationResponse.getErrorCode() != 1000) {
            PrimeFaces.current().ajax().update("restrictedFunctionsWarningDialog");
            PrimeFaces.current().executeScript("PF('restrictedFunctionsWarningDialog').show()");
            return;
        }

        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                this.edit();
                break;
            case Defines.REQUEST_MODE_ADD:
                if (reportModel.getChartColorPallete() == null || reportModel.getChartColorPallete().isEmpty()) {
                    reportModel.setChartColorPallete("JUST_BLUE");
                }
//                super.populateDataFromForm();
                if (!this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue().equals("3")) {
                    if (!this.reportModel.isSkipPatternRecognition()) {
                        String patternString = "" + reportModel.getPrimaryModule().getModuleBaseTable() + "" + "\\W+(\\w+)";
                        Pattern pattern = Pattern.compile(patternString);
                        Matcher matcher = pattern.matcher(reportModel.getCustomColumnScript());
                        while (matcher.find()) {
                            for (ModuleFieldModel mfm : this.availableColumnList) {
                                if (mfm.getModuleId() == reportModel.getPrimaryModule().getModuleId()) {
                                    if (mfm.getFieldName().equals(matcher.group(1).replaceAll("\\.", ""))) {
                                        boolean exists = false;
                                        for (ModuleFieldModel mfmR : reportModel.getFieldList()) {
                                            if (mfmR.getModuleId() == mfm.getModuleId() && mfmR.getFieldName().equals(mfm.getFieldName())) {
                                                exists = true;
                                                break;
                                            }
                                        }
                                        if (!exists) {
                                            reportModel.getFieldList().add(mfm);
                                        }
                                    }
                                }
                            }
                        }

                        if (reportModel.getRelatedModulesList() != null) {
                            for (ModuleModel mm : reportModel.getRelatedModulesList()) {
                                String patternStringRelated = "" + mm.getModuleBaseTable() + "" + "\\W+(\\w+)";
                                Pattern patternRelated = Pattern.compile(patternStringRelated);
                                Matcher matcherRelated = patternRelated.matcher(reportModel.getCustomColumnScript());
                                while (matcherRelated.find()) {
                                    for (ModuleFieldModel mfm : this.availableColumnList) {
                                        if (mfm.getModuleId() == mm.getModuleId()) {
                                            if (mfm.getFieldName().equals(matcherRelated.group(1).replaceAll("\\.", ""))) {
                                                boolean exists = false;
                                                for (ModuleFieldModel mfmR : reportModel.getFieldList()) {
                                                    if (mfmR.getModuleId() == mfm.getModuleId() && mfmR.getFieldName().equals(mfm.getFieldName())) {
                                                        exists = true;
                                                        break;
                                                    }
                                                }
                                                if (!exists) {
                                                    reportModel.getFieldList().add(mfm);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                ReportRequest request = new ReportRequest();
                this.reportModel.setReportDetails(this.reportBasicRecord);
                request.setReportModel(this.reportModel);
                request.setRequestActionType("1");
                ReportHandler reportHandler = new ReportHandler();
                ReportResponse response;
                try {
                    response = reportHandler.execute(request);
                    if (response.getErrorCode() == 1000) {
                        ModuleFieldModel idFieldModel = new ModuleFieldModel();
                        idFieldModel.setFieldName("id");
                        idFieldModel.setFieldType("INT");
                        idFieldModel.setCurrentValue(String.valueOf(response.getAddReportId()));
                        this.reportModel.getReportDetails().getFieldValueMap().put("id", idFieldModel);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Report Added Susscefully."));
                        this.enableReportGeneration = true;
                    }
                    else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add report."));
                    }
                }
                catch (Exception ex) {
                    Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
    }

    @Override
    public void edit() {
        if (reportModel.getChartColorPallete() == null || reportModel.getChartColorPallete().isEmpty()) {
            reportModel.setChartColorPallete("JUST_BLUE");
        }
//        super.populateDataFromForm();
        if (!this.reportBasicRecord.getFieldValueMap().get("report_type").getCurrentValue().equals("3")) {
            if (!this.reportModel.isSkipPatternRecognition()) {
                String patternString = "" + reportModel.getPrimaryModule().getModuleBaseTable() + "" + "\\W+(\\w+)";
                Pattern pattern = Pattern.compile(patternString);
                Matcher matcher = pattern.matcher(reportModel.getCustomColumnScript());
                while (matcher.find()) {
                    for (ModuleFieldModel mfm : this.availableColumnList) {
                        if (mfm.getModuleId() == reportModel.getPrimaryModule().getModuleId()) {
                            if (mfm.getFieldName().equals(matcher.group(1).replaceAll("\\.", ""))) {
                                boolean exists = false;
                                for (ModuleFieldModel reportMfm : reportModel.getFieldList()) {
                                    if (reportMfm.getModuleId() == mfm.getModuleId()) {
                                        if (reportMfm.getFieldName().equals(mfm.getFieldName())) {
                                            exists = true;
                                            break;
                                        }
                                    }
                                }
                                if (!exists) {
//                            reportModel.getFieldList().add(mfm);
                                }
                            }
                        }
                    }
                }

                if (reportModel.getRelatedModulesList() != null) {
                    for (ModuleModel mm : reportModel.getRelatedModulesList()) {
                        String patternStringRelated = "" + mm.getModuleBaseTable() + "" + "\\W+(\\w+)";
                        Pattern patternRelated = Pattern.compile(patternStringRelated);
                        Matcher matcherRelated = patternRelated.matcher(reportModel.getCustomColumnScript());
                        while (matcherRelated.find()) {
                            for (ModuleFieldModel mfm : this.availableColumnList) {
                                if (mfm.getModuleId() == mm.getModuleId()) {
                                    if (mfm.getFieldName().equals(matcherRelated.group(1).replaceAll("\\.", ""))) {
                                        boolean exists = false;
                                        for (ModuleFieldModel reportMfm : reportModel.getFieldList()) {
                                            if (reportMfm.getModuleId() == mfm.getModuleId()) {
                                                if (reportMfm.getFieldName().equals(mfm.getFieldName())) {
                                                    exists = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!exists) {
                                            reportModel.getFieldList().add(mfm);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        ReportRequest request = new ReportRequest();
        this.reportModel.setReportDetails(this.reportBasicRecord);
        request.setReportModel(this.reportModel);
        request.setRequestActionType("3");
        ReportHandler reportHandler = new ReportHandler();
        ReportResponse response;
        try {
            response = reportHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                ModuleFieldModel idFieldModel = new ModuleFieldModel();
                idFieldModel.setFieldName("id");
                idFieldModel.setFieldType("INT");
                idFieldModel.setCurrentValue(String.valueOf(response.getAddReportId()));
//                this.reportModel.getReportDetails().getFieldValueMap().put("id", idFieldModel);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Report Added Susscefully."));
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add report."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private ResponseModel validateCustomQueries() {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setErrorCode(1000);
        responseModel.setErrorMessage("Success");
        if (reportModel.getCustomColumnScript() != null && !reportModel.getCustomColumnScript().isEmpty()) {
            if (reportModel.getCustomColumnScript().contains("create ") || reportModel.getCustomColumnScript().contains("delete ")
                    || reportModel.getCustomColumnScript().contains("update ") || reportModel.getCustomColumnScript().contains("drop ")
                    || reportModel.getCustomColumnScript().contains("alter ") || reportModel.getCustomColumnScript().contains("procedure ")) {
                responseModel.setErrorCode(2056);
                responseModel.setErrorMessage("Custom select query contains restricted functions");
            }
        }

        if (reportModel.getCustomFilterScript() != null && !reportModel.getCustomFilterScript().isEmpty()) {
            if (reportModel.getCustomFilterScript().contains("create ") || reportModel.getCustomFilterScript().contains("delete ")
                    || reportModel.getCustomFilterScript().contains("update ") || reportModel.getCustomFilterScript().contains("drop ")
                    || reportModel.getCustomFilterScript().contains("alter ") || reportModel.getCustomFilterScript().contains("procedure ")) {
                responseModel.setErrorCode(2056);
                responseModel.setErrorMessage("Custom filter query contains restricted functions");
            }
        }

        return responseModel;
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String onFlowProcess(FlowEvent event) {
        return event.getNewStep();
    }

    public ArrayList<ModuleModel> getRelatedModules() {
        return relatedModules;
    }

    public void setRelatedModules(ArrayList<ModuleModel> relatedModules) {
        this.relatedModules = relatedModules;
    }

    public ArrayList<ModuleFieldModel> getSelectedColumnList() {
        return selectedColumnList;
    }

    public void setSelectedColumnList(ArrayList<ModuleFieldModel> selectedColumnList) {
        this.selectedColumnList = selectedColumnList;
    }

    public ReportModel getReportModel() {
        return reportModel;
    }

    public void setReportModel(ReportModel reportModel) {
        this.reportModel = reportModel;
    }

    public ArrayList<ModuleModel> getModuleList() {
        return moduleList;
    }

    public void setModuleList(ArrayList<ModuleModel> moduleList) {
        this.moduleList = moduleList;
    }

    public ArrayList<ModuleFieldModel> getAvailableColumnList() {
        return availableColumnList;
    }

    public void setAvailableColumnList(ArrayList<ModuleFieldModel> availableColumnList) {
        this.availableColumnList = availableColumnList;
    }

    public String getFilterColumn() {
        return filterColumn;
    }

    public void setFilterColumn(String filterColumn) {
        this.filterColumn = filterColumn;
    }

    public List<SelectItem> getFilterColumnList() {
        return filterColumnList;
    }

    public void setFilterColumnList(List<SelectItem> filterColumnList) {
        this.filterColumnList = filterColumnList;
    }

    public StreamedContent getReportPdfContent() {
        return reportPdfContent;
    }

    public void setReportPdfContent(StreamedContent reportPdfContent) {
        this.reportPdfContent = reportPdfContent;
    }

    public boolean isEnableReportGeneration() {
        return enableReportGeneration;
    }

    public void setEnableReportGeneration(boolean enableReportGeneration) {
        this.enableReportGeneration = enableReportGeneration;
    }

    public ArrayList<ModuleFieldModel> getAvailableColumnListForAggregates() {
        return availableColumnListForAggregates;
    }

    public void setAvailableColumnListForAggregates(ArrayList<ModuleFieldModel> availableColumnListForAggregates) {
        this.availableColumnListForAggregates = availableColumnListForAggregates;
    }

    public ArrayList<String> getCustomColumns() {
        return customColumns;
    }

    public void setCustomColumns(ArrayList<String> customColumns) {
        this.customColumns = customColumns;
    }

    public ArrayList<ModuleFieldModel> getCurrentFieldsForJoin() {
        return currentFieldsForJoin;
    }

    public void setCurrentFieldsForJoin(ArrayList<ModuleFieldModel> currentFieldsForJoin) {
        this.currentFieldsForJoin = currentFieldsForJoin;
    }

    public HashMap<String, ArrayList<ModuleFieldModel>> getModuleFieldMap() {
        return moduleFieldMap;
    }

    public void setModuleFieldMap(HashMap<String, ArrayList<ModuleFieldModel>> moduleFieldMap) {
        this.moduleFieldMap = moduleFieldMap;
    }

    public boolean isSkipPattenRecognition() {
        return skipPattenRecognition;
    }

    public void setSkipPattenRecognition(boolean skipPattenRecognition) {
        this.skipPattenRecognition = skipPattenRecognition;
    }

    public EditorOptions getEditorOptions() {
        return editorOptions;
    }

    public void setEditorOptions(EditorOptions editorOptions) {
        this.editorOptions = editorOptions;
    }

    public BeezlCodeHelper getBeezlCodeHelper() {
        return beezlCodeHelper;
    }

    public void setBeezlCodeHelper(BeezlCodeHelper beezlCodeHelper) {
        this.beezlCodeHelper = beezlCodeHelper;
    }

    public ResponseModel getValidationResponse() {
        return validationResponse;
    }

    public void setValidationResponse(ResponseModel validationResponse) {
        this.validationResponse = validationResponse;
    }

    public boolean isShowChartColors() {
        return showChartColors;
    }

    public void setShowChartColors(boolean showChartColors) {
        this.showChartColors = showChartColors;
    }

    public String getSelectedChartColorPallete() {
        return selectedChartColorPallete;
    }

    public void setSelectedChartColorPallete(String selectedChartColorPallete) {
        this.selectedChartColorPallete = selectedChartColorPallete;
    }

    public HashMap<String, ArrayList<String>> getColorMap() {
        return colorMap;
    }

    public void setColorMap(HashMap<String, ArrayList<String>> colorMap) {
        this.colorMap = colorMap;
    }

    public ReportViewer getReportViewer() {
        return reportViewer;
    }

    public void setReportViewer(ReportViewer reportViewer) {
        this.reportViewer = reportViewer;
    }

    public RecordModel getReportBasicRecord() {
        return reportBasicRecord;
    }

    public void setReportBasicRecord(RecordModel reportBasicRecord) {
        this.reportBasicRecord = reportBasicRecord;
    }

    public int getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(int currentStep) {
        this.currentStep = currentStep;
    }

    public List<SelectItem> getFilterColumnSimpleList() {
        return filterColumnSimpleList;
    }

    public void setFilterColumnSimpleList(List<SelectItem> filterColumnSimpleList) {
        this.filterColumnSimpleList = filterColumnSimpleList;
    }

    public String getShowChartFiltering() {
        return showChartFiltering;
    }

    public void setShowChartFiltering(String showChartFiltering) {
        this.showChartFiltering = showChartFiltering;
    }

    public ComparitiveDatasetConfig getSelectedComparitiveDatasetConfig() {
        return selectedComparitiveDatasetConfig;
    }

    public void setSelectedComparitiveDatasetConfig(ComparitiveDatasetConfig selectedComparitiveDatasetConfig) {
        this.selectedComparitiveDatasetConfig = selectedComparitiveDatasetConfig;
    }

}
