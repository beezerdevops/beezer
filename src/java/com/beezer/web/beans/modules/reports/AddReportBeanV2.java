/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.reports;

import com.beezer.web.beans.ExpressionCodeHelper;
import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.ReportHandler;
import com.beezer.web.models.SelectionModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.reports.AggregatModel;
import com.crm.models.global.reports.CustomColumnExpression;
import com.crm.models.global.reports.JoinModel;
import com.crm.models.global.reports.OrderByConfig;
import com.crm.models.global.reports.ReportFilterModel;
import com.crm.models.global.reports.ReportModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldBlockModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.requests.ReportRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.ReportResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.responses.managers.RelationResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "addReportBeanV2")
@ViewScoped
public class AddReportBeanV2 extends BeanFramework {

    private ArrayList<ModuleModel> relatedModules;
    private ArrayList<ModuleFieldModel> selectedColumnList;
    private ArrayList<ModuleFieldModel> availableColumnList;
    private ArrayList<ModuleFieldModel> availableColumnListForAggregates;
    private ArrayList<ModuleFieldModel> currentFieldsForJoin;
    private ReportModel reportModel;
    private ArrayList<ModuleModel> moduleList;

    private String filterColumn;
    private List<SelectItem> filterColumnList;
    private StreamedContent reportPdfContent;
    private boolean enableReportGeneration;

    private ArrayList<String> customColumns;
    private HashMap<String, ArrayList<ModuleFieldModel>> moduleFieldMap;

    private boolean skip;
    private ExpressionCodeHelper expressionCodeHelper;

    @PostConstruct
    public void init() {
        this.reportModel = new ReportModel();
        this.reportModel.setANDFilterList(new ArrayList<>());
        this.reportModel.setORFilterList(new ArrayList<>());
        this.reportModel.setGroupByList(new ArrayList<>());
        this.reportModel.setOrderByList(new ArrayList<>());
        this.reportModel.setAggregatesList(new ArrayList<>());
        this.reportModel.setCustomFields(new ArrayList<>());
        this.reportModel.setJoinConfiguration(new ArrayList<>());
        this.moduleList = this.getModuleService();
        this.availableColumnList = new ArrayList<>();
        this.availableColumnListForAggregates = new ArrayList<>();
        this.relatedModules = new ArrayList<>();
        this.currentFieldsForJoin = new ArrayList<>();
        this.moduleFieldMap = new HashMap<>();
        this.expressionCodeHelper = new ExpressionCodeHelper();
        super.loadForms("12",0);
        super.setMode();
        if (super.getRequestMode().equalsIgnoreCase(Defines.REQUEST_MODE_EDIT)) {
            this.enableReportGeneration = true;
        }
    }

    @Override
    public void loadRecord() {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("id", "=", Integer.valueOf(super.getRequestParams().get("report"))));
        ReportRequest reportRequest = new ReportRequest();
        reportRequest.setRequestActionType("0");
        reportRequest.setClause(fm.getClause());
        ReportResponse reportResponse = null;
        ReportHandler reportHandler = new ReportHandler();
        try {
            reportResponse = reportHandler.execute(reportRequest);
        }
        catch (Exception ex) {
            Logger.getLogger(AddReportBeanV2.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (reportResponse != null && reportResponse.getErrorCode() == 1000) {
            reportModel = reportResponse.getReturnList().get(0);
            this.loadAvailableColumns();
            super.setRecordModel(reportModel.getReportDetails());
            if (reportModel.getJoinConfiguration() != null && !reportModel.getJoinConfiguration().isEmpty()) {
                for (JoinModel jm : reportModel.getJoinConfiguration()) {
                    this.generateColumnsForJoin(jm);
                }
            }
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    @Override
    protected void fillFormDetails() {
//        if (this.getDefaultForm() != null && this.getRecordModel() != null) {
//            for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
//                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
//                    for (Map.Entry<String, ModuleFieldModel> entry : this.getRecordModel().getFieldValueMap().entrySet()) {
//                        if (flm.getFieldName().equals(entry.getKey())) {
//                            flm.getFieldObject().setCurrentValue(entry.getValue().getCurrentValue());
//                            switch (flm.getFieldObject().getFieldType()) {
//                                case "REL":
//                                    this.loadRelationRecord(entry.getKey(), entry.getValue().getCurrentValue());
//                                    break;
//                                case "CAT":
//                                    for (CategoryModel cm : this.categoriesCache.get(entry.getKey())) {
//                                        if (cm.getEntityId() == Integer.parseInt(entry.getValue().getCurrentValue())) {
//                                            super.getCatalogsNameMap().put(entry.getKey(), cm.getName());
//                                            break;
//                                        }
//                                    }
//                                    this.selectionMap.put(entry.getKey(), new SelectionModel(entry.getValue().getCurrentValue()));
//                                    this.onSelect(entry.getKey(), null);
//                                    PrimeFaces.current().executeScript("PF('" + entry.getKey() + "').selectValue(" + entry.getValue().getCurrentValue() + ")");
//                                    break;
//                                case "CONTENT":
//                                    SelectionModel sm = new SelectionModel();
//                                    sm.setContentModel(entry.getValue().getContent());
//                                    this.selectionMap.put(entry.getKey(), sm);
//                                    break;
//                            }
//                        }
//                    }
//                }
//            }
//        }
        super.fillFormDetails();
        if (reportModel != null) {
            PrimeFaces.current().executeScript("PF('" + "primary_module" + "').selectValue(" + reportModel.getPrimaryModule().getModuleId() + ")");
        }
    }

    public void onTabChange(TabChangeEvent event) {
        switch (event.getTab().getId()) {
            case "selectFilters":
                this.generateColumnGroups();
                break;
            case "selectGroups":
                this.generateColumnGroups();
                break;
            case "selectOrders":
                this.generateColumnGroups();
                break;
            case "selectColumns":
                this.getPrimaryModuleRelations(reportModel.getPrimaryModule().getModuleId());
                this.generateColumnGroups();
                break;
        }
    }

    public void getPrimaryModuleRelations(int moduleId) {
        try {
            RelationRequest request = new RelationRequest();
//            request.setActionType("0");
//            request.setModuleId(String.valueOf(moduleId));
            request.setActionType("5");
            request.setRelationId(String.valueOf(moduleId));
            RelationHandler relationHandler = new RelationHandler();
            RelationResponse response = relationHandler.relationExecutor(request);
            if (response.getErrorCode() == 1000) {
//                this.relatedModules = new ArrayList<>();
                for (RelationManagerModel rmm : response.getRelationManagerList()) {
                    for (ModuleModel mm : this.moduleList) {
                        if (mm.getModuleId() == rmm.getModuleId()) {
                            if (!relatedModules.contains(mm)) {
                                this.relatedModules.add(mm);
                                this.getPrimaryModuleRelations(mm.getModuleId());
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReportBeanV2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onSelect(String fieldName, String selection) {
        if (selection != null) {
            switch (fieldName) {
                case "primary_module":
                    for (ModuleModel mm : this.moduleList) {
                        if (Integer.valueOf(selection) == mm.getModuleId()) {
                            this.reportModel.setPrimaryModule(mm);
                            this.selectedCache.put(fieldName, selectionMap.get(fieldName).getSelectedValue());
                            this.loadAvailableColumns();
                            break;
                        }
                    }
                    break;
                case "report_type":
                    super.onSelect(fieldName, selection);
                    if (selection.equals("2")) {
                        if (this.getDefaultForm() != null) {
                            for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
                                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                                    if (flm.getFieldObject().getFieldName().equals("graph_type")) {
                                        flm.setIsVisible(true);
                                        PrimeFaces.current().ajax().update("tabHolder:containerPanel");
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                default:
                    super.onSelect(fieldName, selection);
                    break;

            }
        }
    }

    @Override
    protected void loadLookup(String fieldName, String lookupId, String... apiKey) {
        if (fieldName.equals("primary_module")) {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response;
            try {
                response = moduleHandler.managerExecutor(request);
                if (response.getErrorCode() == 1000) {
                    super.selectedCache.put(fieldName, null);
                    TreeMap<String, String> temp = new TreeMap<>();
                    for (ModuleModel mm : response.getReturnList()) {
                        temp.put(mm.getModuleName(), String.valueOf(mm.getModuleId()));
                        super.selectionMap.put(fieldName, new SelectionModel());
                    }
                    super.lookupDictionary.put(fieldName, temp);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(AddReportBeanV2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            super.loadLookup(fieldName, lookupId);
        }
    }

    public void generateColumnGroups() {
        if (reportModel != null) {
            filterColumnList = new ArrayList<>();
            SelectItemGroup primaryColumnsGroup = null;
            if (reportModel.getPrimaryModule() != null) {
                primaryColumnsGroup = new SelectItemGroup(reportModel.getPrimaryModule().getModuleName());
                ArrayList<SelectItem> columItemList = new ArrayList<>();
                for (ModuleFieldModel mfm : this.availableColumnList) {
                    if (mfm.getModuleId() == this.reportModel.getPrimaryModule().getModuleId()) {
                        columItemList.add(new SelectItem(mfm, mfm.getFieldLabel()));
                    }
                }
                primaryColumnsGroup.setSelectItems(columItemList.toArray(new SelectItem[columItemList.size()]));
            }
            filterColumnList.add(primaryColumnsGroup);
            if (reportModel.getRelatedModulesList() != null) {
                for (ModuleModel mm : reportModel.getRelatedModulesList()) {
                    SelectItemGroup columnsGroup = new SelectItemGroup(mm.getModuleName());
                    ArrayList<SelectItem> columnItemList = new ArrayList<>();
                    for (ModuleFieldModel mfm : this.availableColumnList) {
                        if (mfm.getModuleId() == mm.getModuleId()) {
                            columnItemList.add(new SelectItem(mfm, mfm.getFieldLabel()));
                        }
                    }
                    columnsGroup.setSelectItems(columnItemList.toArray(new SelectItem[columnItemList.size()]));
                    filterColumnList.add(columnsGroup);
                }
            }

            if (reportModel.getAggregatesList() != null && !reportModel.getAggregatesList().isEmpty()) {
                SelectItemGroup columnsGroup = new SelectItemGroup("Aggreagte Fields");
                ArrayList<SelectItem> columnItemList = new ArrayList<>();
                for (ModuleFieldModel mfm : this.availableColumnList) {
                    if (mfm.getFieldType().equals("AGGREGATE")) {
                        columnItemList.add(new SelectItem(mfm, mfm.getFieldName()));
                    }
                }
                columnsGroup.setSelectItems(columnItemList.toArray(new SelectItem[columnItemList.size()]));
                filterColumnList.add(columnsGroup);
            }
        }
    }

    public void addJoinStatement() {
        if (this.reportModel.getJoinConfiguration() != null) {
            this.reportModel.getJoinConfiguration().add(new JoinModel());
        }
        else {
            this.reportModel.setJoinConfiguration(new ArrayList<>());
            this.reportModel.getJoinConfiguration().add(new JoinModel());
        }
    }

    public void removeJoinStatement(JoinModel joinModel) {
        this.reportModel.getJoinConfiguration().remove(joinModel);
    }

    public void addNewANDCondition() {
        if (this.reportModel.getANDFilterList() != null) {
            this.reportModel.getANDFilterList().add(new ReportFilterModel());
        }
        else {
            this.reportModel.setANDFilterList(new ArrayList<>());
            this.reportModel.getANDFilterList().add(new ReportFilterModel());
        }
    }

    public void addNewORCondition() {
        if (this.reportModel.getORFilterList() != null) {
            this.reportModel.getORFilterList().add(new ReportFilterModel());
        }
        else {
            this.reportModel.setORFilterList(new ArrayList<>());
            this.reportModel.getORFilterList().add(new ReportFilterModel());
        }
    }

    public void addNewAggregateFunction() {
        AggregatModel aggregatModel = new AggregatModel();
        aggregatModel.setRuntimeId(GeneralUtils.generateRandomId());
        if (this.reportModel.getAggregatesList() != null) {
            this.reportModel.getAggregatesList().add(aggregatModel);
        }
        else {
            this.reportModel.setAggregatesList(new ArrayList<>());
            this.reportModel.getAggregatesList().add(aggregatModel);
        }

        ModuleFieldModel mfm = new ModuleFieldModel();
        if (aggregatModel.getRuntimeId() != 0) {
            mfm.setFieldId(aggregatModel.getRuntimeId());
        }
        else {
            aggregatModel.setRuntimeId(GeneralUtils.generateRandomId());
            mfm.setFieldId(aggregatModel.getRuntimeId());
        }
        mfm.setFieldName(aggregatModel.getAlias());
        mfm.setFieldType("AGGREGATE");
        this.availableColumnList.add(mfm);
    }

    public void removeAggregateFunction(AggregatModel aggregatModel) {
        ModuleFieldModel aggField = new ModuleFieldModel();
        for (ModuleFieldModel mfm : this.availableColumnList) {
            if (mfm.getFieldId() == aggregatModel.getRuntimeId()) {
                aggField = mfm;
                break;
            }
        }
        this.availableColumnList.remove(aggField);
        this.reportModel.getAggregatesList().remove(aggregatModel);
    }

    public void addNewGroup() {
        if (this.reportModel.getGroupByList() != null) {
            this.reportModel.getGroupByList().add(new ModuleFieldModel());
        }
        else {
            this.reportModel.setGroupByList(new ArrayList<>());
            this.reportModel.getGroupByList().add(new ModuleFieldModel());
        }
    }

    public void addNewOrderBy() {
        if (this.reportModel.getOrderByList() != null) {
            this.reportModel.getOrderByList().add(new OrderByConfig());
        }
        else {
            this.reportModel.setOrderByList(new ArrayList<>());
            this.reportModel.getOrderByList().add(new OrderByConfig());
        }
    }

    public void removeANDCondition(ReportFilterModel rfm) {
        this.reportModel.getANDFilterList().remove(rfm);
    }

    public void removeORCondition(ReportFilterModel rfm) {
        this.reportModel.getORFilterList().remove(rfm);
    }

    public void removeGroup(ModuleFieldModel mfm) {
        this.reportModel.getGroupByList().remove(mfm);
    }

    public void removeOrderBy(OrderByConfig mfm) {
        this.reportModel.getOrderByList().remove(mfm);
    }
    
    public void addCustomExpression() {
        if (this.reportModel.getCustomColumnExpressions()!= null) {
            this.reportModel.getCustomColumnExpressions().add(new CustomColumnExpression());
        }
        else {
            this.reportModel.setCustomColumnExpressions(new ArrayList<>());
            this.reportModel.getCustomColumnExpressions().add(new CustomColumnExpression());
        }
    }

    public void removeCustomExpression(CustomColumnExpression customExpression) {
        this.reportModel.getCustomColumnExpressions().remove(customExpression);
    }

    public List<ModuleModel> completeModuleList(String query) {
//        List<ModuleModel> allModules = this.getRelatedModules();
        List<ModuleModel> filteredModules = new ArrayList<>();
        for (ModuleModel module : moduleList) {
            if (module.getModuleName().toLowerCase().startsWith(query)) {
                filteredModules.add(module);
            }
        }
        return filteredModules;
    }

    public List<ModuleFieldModel> completeFieldList(String query) {
        List<ModuleFieldModel> allFields = this.getAvailableColumnList();
        List<ModuleFieldModel> filtiredFields = new ArrayList<>();
        for (ModuleFieldModel mfm : allFields) {
            if (mfm.getFieldLabel() != null) {
                if (mfm.getFieldLabel().toLowerCase().contains(query)) {
                    filtiredFields.add(mfm);
                }
            }
            else {
                filtiredFields.add(mfm);
            }
        }
        Collections.sort(filtiredFields);
        return filtiredFields;
    }

    private ArrayList<ModuleModel> getModuleService() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response;
            response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                return response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReportBeanV2.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void onModuleSelect(SelectEvent event) {
//        if (this.reportModel.getRelatedModulesList() == null) {
//            this.reportModel.setRelatedModulesList(new ArrayList<>());
//        }
//        this.reportModel.getRelatedModulesList().add((ModuleModel) event.getObject());
        this.loadAvailableColumns();
    }
    
    public void onModuleUnselect(UnselectEvent event) {
        ModuleModel mm = (ModuleModel) event.getObject();
        this.reportModel.getRelatedModulesList().remove(mm);
        this.loadAvailableColumns();
    }
    
    public void onCustomColumnUnselect(UnselectEvent event) {
        String col = (String) event.getObject();
        this.reportModel.getCustomFields().remove(col);
    }

    public void onColumnSelect(SelectEvent event) {
//        if(this.reportModel.getFieldList() == null){
//            this.reportModel.setFieldList(new ArrayList<>());
//        }
//        this.reportModel.getFieldList().add((ModuleFieldModel) event.getObject());
//        this.getAvailableColumnList().remove((ModuleFieldModel) event.getObject());
    }

    public void onColumnUnselect(UnselectEvent event) {
        ModuleFieldModel mfm = (ModuleFieldModel) event.getObject();
        this.reportModel.getFieldList().remove(mfm);
    }

    public void loadAvailableColumns() {
        if (this.reportModel.getPrimaryModule() != null) {
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("modules_field_configuration.module_id", "=", this.reportModel.getPrimaryModule().getModuleId()));
            if (this.reportModel.getRelatedModulesList() != null) {
                for (ModuleModel mm : this.reportModel.getRelatedModulesList()) {
                    fm.setFilter("OR", new FilterType("modules_field_configuration.module_id", "=", mm.getModuleId()));
                }
            }
            try {
//                this.availableColumnList = super.getFieldSetService(fm.getClause(), String.valueOf(this.reportModel.getPrimaryModule().getModuleId()));
                this.availableColumnList = super.getFieldSetService(fm.getClause(), null, false);
                if (reportModel.getAggregatesList() != null && !reportModel.getAggregatesList().isEmpty()) {
                    for (AggregatModel aggregatModel : reportModel.getAggregatesList()) {
                        ModuleFieldModel mfm = new ModuleFieldModel();
                        if (aggregatModel.getRuntimeId() != 0) {
                            mfm.setFieldId(aggregatModel.getRuntimeId());
                        }
                        else {
                            aggregatModel.setRuntimeId(GeneralUtils.generateRandomId());
                            mfm.setFieldId(aggregatModel.getRuntimeId());
                        }
                        mfm.setFieldName(aggregatModel.getAlias());
                        mfm.setFieldType("AGGREGATE");
                        this.availableColumnList.add(mfm);
                    }
                }
                this.generateColumnGroups();
                this.generateAvailableAggregateColumns();
            }
            catch (Exception ex) {
                Logger.getLogger(AddReportBeanV2.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void generateAvailableAggregateColumns() {
        this.availableColumnListForAggregates = new ArrayList<>();
        if (this.availableColumnList != null) {
            for (ModuleFieldModel mfm : this.availableColumnList) {
                if (mfm.getFieldType().equals("INT") || mfm.getFieldType().equals("DECIMAL") || mfm.getFieldType().equals("LONG")) {
                    this.availableColumnListForAggregates.add(new ModuleFieldModel(mfm));
                }
            }
        }
    }

    public void generateColumnsForJoin(JoinModel joinModel) {
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("modules_field_configuration.module_id", "=", joinModel.getJoinModule().getModuleId()));
        this.currentFieldsForJoin = super.getFieldSetService(fm.getClause(), null, false);
        this.moduleFieldMap.put(String.valueOf(joinModel.getJoinModule().getModuleId()), currentFieldsForJoin);
    }

    public String getFieldGroup(ModuleFieldModel mfm) {
        for (ModuleModel mm : this.moduleList) {
            if (mfm.getModuleId() == mm.getModuleId()) {
                return mm.getModuleName();
            }
        }
        return null;
    }

    public void generateReport() {
//        ReportGenerator gr = new ReportGenerator();
//        try {
//            RequestContext context = RequestContext.getCurrentInstance();
//            context.execute("PF('reportDialog').show()");
//        }
//        catch (Exception ex) {
//            Logger.getLogger(AddReportBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("reportViewer.xhtml?moduleId=" + super.getModuleId()
                    + "&report=" + this.reportModel.getReportDetails().getFieldValueMap().get("id").getCurrentValue());
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddReportBeanV2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void save() {
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                this.edit();
                break;
            case Defines.REQUEST_MODE_ADD:
                super.populateDataFromForm();
                String patternString = "" + reportModel.getPrimaryModule().getModuleBaseTable() + "" + "\\W+(\\w+)";
                Pattern pattern = Pattern.compile(patternString);
                Matcher matcher = pattern.matcher(reportModel.getCustomColumnScript());
                while (matcher.find()) {
                    for (ModuleFieldModel mfm : this.availableColumnList) {
                        if (mfm.getModuleId() == reportModel.getPrimaryModule().getModuleId()) {
                            if (mfm.getFieldName().equals(matcher.group(1).replaceAll("\\.", ""))) {
                                boolean exists = false;
                                for (ModuleFieldModel mfmR : reportModel.getFieldList()) {
                                    if (mfmR.getModuleId() == mfm.getModuleId() && mfmR.getFieldName().equals(mfm.getFieldName())) {
                                        exists = true;
                                        break;
                                    }
                                }
                                if (!exists) {
                                    reportModel.getFieldList().add(mfm);
                                }
                            }
                        }
                    }
                }

                if (reportModel.getRelatedModulesList() != null) {
                    for (ModuleModel mm : reportModel.getRelatedModulesList()) {
                        String patternStringRelated = "" + mm.getModuleBaseTable() + "" + "\\W+(\\w+)";
                        Pattern patternRelated = Pattern.compile(patternStringRelated);
                        Matcher matcherRelated = patternRelated.matcher(reportModel.getCustomColumnScript());
                        while (matcherRelated.find()) {
                            for (ModuleFieldModel mfm : this.availableColumnList) {
                                if (mfm.getModuleId() == mm.getModuleId()) {
                                    if (mfm.getFieldName().equals(matcherRelated.group(1).replaceAll("\\.", ""))) {
                                        boolean exists = false;
                                        for (ModuleFieldModel mfmR : reportModel.getFieldList()) {
                                            if (mfmR.getModuleId() == mfm.getModuleId() && mfmR.getFieldName().equals(mfm.getFieldName())) {
                                                exists = true;
                                                break;
                                            }
                                        }
                                        if (!exists) {
                                            reportModel.getFieldList().add(mfm);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                ReportRequest request = new ReportRequest();
                this.reportModel.setReportDetails(super.getRecordModel());
                request.setReportModel(this.reportModel);
                request.setRequestActionType("1");
                ReportHandler reportHandler = new ReportHandler();
                ReportResponse response;
                try {
                    response = reportHandler.execute(request);
                    if (response.getErrorCode() == 1000) {
                        ModuleFieldModel idFieldModel = new ModuleFieldModel();
                        idFieldModel.setFieldName("id");
                        idFieldModel.setFieldType("INT");
                        idFieldModel.setCurrentValue(String.valueOf(response.getAddReportId()));
                        this.reportModel.getReportDetails().getFieldValueMap().put("id", idFieldModel);
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Report Added Susscefully."));
                        this.enableReportGeneration = true;
                    }
                    else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add report."));
                    }
                }
                catch (Exception ex) {
                    Logger.getLogger(AddReportBeanV2.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
    }

    @Override
    public void edit() {
        super.populateDataFromForm();
        String patternString = "" + reportModel.getPrimaryModule().getModuleBaseTable() + "" + "\\W+(\\w+)";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(reportModel.getCustomColumnScript());
        while (matcher.find()) {
            for (ModuleFieldModel mfm : this.availableColumnList) {
                if (mfm.getModuleId() == reportModel.getPrimaryModule().getModuleId()) {
                    if (mfm.getFieldName().equals(matcher.group(1).replaceAll("\\.", ""))) {
                        boolean exists = false;
                        for (ModuleFieldModel reportMfm : reportModel.getFieldList()) {
                            if (reportMfm.getModuleId() == mfm.getModuleId()) {
                                if (reportMfm.getFieldName().equals(mfm.getFieldName())) {
                                    exists = true;
                                    break;
                                }
                            }
                        }
                        if (!exists) {
//                            reportModel.getFieldList().add(mfm);
                        }
                    }
                }
            }
        }

        if (reportModel.getRelatedModulesList() != null) {
            for (ModuleModel mm : reportModel.getRelatedModulesList()) {
                String patternStringRelated = "" + mm.getModuleBaseTable() + "" + "\\W+(\\w+)";
                Pattern patternRelated = Pattern.compile(patternStringRelated);
                Matcher matcherRelated = patternRelated.matcher(reportModel.getCustomColumnScript());
                while (matcherRelated.find()) {
                    for (ModuleFieldModel mfm : this.availableColumnList) {
                        if (mfm.getModuleId() == mm.getModuleId()) {
                            if (mfm.getFieldName().equals(matcherRelated.group(1).replaceAll("\\.", ""))) {
                                boolean exists = false;
                                for (ModuleFieldModel reportMfm : reportModel.getFieldList()) {
                                    if (reportMfm.getModuleId() == mfm.getModuleId()) {
                                        if (reportMfm.getFieldName().equals(mfm.getFieldName())) {
                                            exists = true;
                                            break;
                                        }
                                    }
                                }
                                if (!exists) {
                                    reportModel.getFieldList().add(mfm);
                                }
                            }
                        }
                    }
                }
            }
        }

        ReportRequest request = new ReportRequest();
        this.reportModel.setReportDetails(super.getRecordModel());
        request.setReportModel(this.reportModel);
        request.setRequestActionType("3");
        ReportHandler reportHandler = new ReportHandler();
        ReportResponse response;
        try {
            response = reportHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                ModuleFieldModel idFieldModel = new ModuleFieldModel();
                idFieldModel.setFieldName("id");
                idFieldModel.setFieldType("INT");
                idFieldModel.setCurrentValue(String.valueOf(response.getAddReportId()));
//                this.reportModel.getReportDetails().getFieldValueMap().put("id", idFieldModel);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Report Added Susscefully."));
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add report."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddReportBeanV2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onReorder(){
        int x = 0;
    }

    public String onFlowProcess(FlowEvent event) {
        if (skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            switch (event.getNewStep()) {
                case "selectFilters":
                    this.generateColumnGroups();
                    break;
                case "selectGroups":
                    this.generateColumnGroups();
                    break;
                case "selectOrders":
                    this.generateColumnGroups();
                    break;
                case "selectColumns":
                    this.getPrimaryModuleRelations(reportModel.getPrimaryModule().getModuleId());
                    this.generateColumnGroups();
                    break;
            }
            return event.getNewStep();
        }
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<ModuleModel> getRelatedModules() {
        return relatedModules;
    }

    public void setRelatedModules(ArrayList<ModuleModel> relatedModules) {
        this.relatedModules = relatedModules;
    }

    public ArrayList<ModuleFieldModel> getSelectedColumnList() {
        return selectedColumnList;
    }

    public void setSelectedColumnList(ArrayList<ModuleFieldModel> selectedColumnList) {
        this.selectedColumnList = selectedColumnList;
    }

    public ReportModel getReportModel() {
        return reportModel;
    }

    public void setReportModel(ReportModel reportModel) {
        this.reportModel = reportModel;
    }

    public ArrayList<ModuleModel> getModuleList() {
        return moduleList;
    }

    public void setModuleList(ArrayList<ModuleModel> moduleList) {
        this.moduleList = moduleList;
    }

    public ArrayList<ModuleFieldModel> getAvailableColumnList() {
        return availableColumnList;
    }

    public void setAvailableColumnList(ArrayList<ModuleFieldModel> availableColumnList) {
        this.availableColumnList = availableColumnList;
    }

    public String getFilterColumn() {
        return filterColumn;
    }

    public void setFilterColumn(String filterColumn) {
        this.filterColumn = filterColumn;
    }

    public List<SelectItem> getFilterColumnList() {
        return filterColumnList;
    }

    public void setFilterColumnList(List<SelectItem> filterColumnList) {
        this.filterColumnList = filterColumnList;
    }

    public StreamedContent getReportPdfContent() {
        return reportPdfContent;
    }

    public void setReportPdfContent(StreamedContent reportPdfContent) {
        this.reportPdfContent = reportPdfContent;
    }

    public boolean isEnableReportGeneration() {
        return enableReportGeneration;
    }

    public void setEnableReportGeneration(boolean enableReportGeneration) {
        this.enableReportGeneration = enableReportGeneration;
    }

    public ArrayList<ModuleFieldModel> getAvailableColumnListForAggregates() {
        return availableColumnListForAggregates;
    }

    public void setAvailableColumnListForAggregates(ArrayList<ModuleFieldModel> availableColumnListForAggregates) {
        this.availableColumnListForAggregates = availableColumnListForAggregates;
    }

    public ArrayList<String> getCustomColumns() {
        return customColumns;
    }

    public void setCustomColumns(ArrayList<String> customColumns) {
        this.customColumns = customColumns;
    }

    public ArrayList<ModuleFieldModel> getCurrentFieldsForJoin() {
        return currentFieldsForJoin;
    }

    public void setCurrentFieldsForJoin(ArrayList<ModuleFieldModel> currentFieldsForJoin) {
        this.currentFieldsForJoin = currentFieldsForJoin;
    }

    public HashMap<String, ArrayList<ModuleFieldModel>> getModuleFieldMap() {
        return moduleFieldMap;
    }

    public void setModuleFieldMap(HashMap<String, ArrayList<ModuleFieldModel>> moduleFieldMap) {
        this.moduleFieldMap = moduleFieldMap;
    }

    public boolean isSkip() {
        return skip;
    }

    public void setSkip(boolean skip) {
        this.skip = skip;
    }

    public ExpressionCodeHelper getExpressionCodeHelper() {
        return expressionCodeHelper;
    }

    public void setExpressionCodeHelper(ExpressionCodeHelper expressionCodeHelper) {
        this.expressionCodeHelper = expressionCodeHelper;
    }

}
