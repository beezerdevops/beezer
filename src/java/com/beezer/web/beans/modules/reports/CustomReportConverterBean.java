/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.reports;

import com.beezer.web.beans.ApplicationBean;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.DocumentUtilsHandler;
import com.beezer.web.handler.ReportHandler;
import com.beezer.web.utils.GeneralUtils;
import com.beezl.interpreter.BEEZLInterperter;
import com.crm.models.global.reports.CustomReportConfig;
import com.crm.models.global.reports.ReportModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.docTemplates.DynamicTemplateReference;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.workflow.activities.DocTemplateParameter;
import com.crm.models.requests.ReportRequest;
import com.crm.models.requests.pdf.DocumentUtilRequest;
import com.crm.models.responses.ReportResponse;
import com.crm.models.responses.pdf.DocumentUtilResponse;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Ahmed El Badry Aug 12, 2022
 */
@ManagedBean(name = "customReportConverterBean")
@RequestScoped
public class CustomReportConverterBean {

    private StreamedContent convertedReport;
    private String idCache;

    public CustomReportConverterBean() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            convertedReport = new DefaultStreamedContent();
        }
        else {
            String id = context.getExternalContext().getRequestParameterMap().get("reportId");
            if (id != null) {
                idCache = id;
            }
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("id", "=", idCache));
            ReportRequest reportRequest = new ReportRequest();
            reportRequest.setRequestActionType("0");
            reportRequest.setClause(fm.getClause());
            ReportResponse reportResponse = null;
            ReportHandler reportHandler = new ReportHandler();
            try {
                reportResponse = reportHandler.execute(reportRequest);
            }
            catch (Exception ex) {
                Logger.getLogger(ApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            ReportModel reportModel = null;
            if (reportResponse != null && reportResponse.getErrorCode() == 1000) {
                reportModel = reportResponse.getReturnList().get(0);
            }

            if (reportModel != null) {
                if (reportModel.getCustomReportConfig() != null) {
                    DocumentUtilsHandler documentUtilsHandler = new DocumentUtilsHandler();
                    DocumentUtilRequest documentUtilRequest = new DocumentUtilRequest();
                    documentUtilRequest.setRequestActionType("0");
                    String htmlComplete = "";
                    if (reportModel.getReportDetails().getFieldValueMap().get("report_type").getCurrentValue().equals("1")
                            && reportModel.isEnableCustomReport()) {
                        htmlComplete = this.evaluateDetailedReportWithCustomization(reportModel, reportModel.getCustomReportConfig());
                    }
                    else {
                        htmlComplete = this.evaluateCustomReport(reportModel.getCustomReportConfig());
                    }

                    documentUtilRequest.setHtmlContent(htmlComplete);
                    DocumentUtilResponse documentUtilResponse = documentUtilsHandler.executeRequest(documentUtilRequest);
                    if (documentUtilResponse.getErrorCode() == 1000 && documentUtilResponse.getConvertedDocument() != null) {
                        convertedReport = DefaultStreamedContent.builder()
                                .name(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue() + ".pdf")
                                .contentType("application/pdf")
                                .stream(() -> new ByteArrayInputStream(
                                documentUtilResponse.getConvertedDocument().getContentStream()))
                                .build();
                        FacesContext.getCurrentInstance().responseComplete();
                    }
                }
            }
        }
    }

    public String evaluateCustomReport(CustomReportConfig customReportConfig) {
        if (customReportConfig != null) {
            if (customReportConfig.getHtmlContent() != null
                    && !customReportConfig.getHtmlContent().isEmpty()
                    && customReportConfig.getTemplateParameters() != null
                    && !customReportConfig.getTemplateParameters().isEmpty()) {

                BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                LinkedHashMap<String, String> bvm = null;

                Map<String, Object> configs = new HashMap<>();
                for (DocTemplateParameter dtp : customReportConfig.getTemplateParameters()) {
                    switch (dtp.getType()) {
                        case "STRING":
                            beezlInterperter.addDynamicField(dtp.getKeyName(), dtp.getKeyValue());
                            break;
                        case "LIST":
                            break;
                    }
                }

                try {
                    bvm = beezlInterperter.evaluate();
                }
                catch (Exception ex) {
                    Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                    return "Something went wrong :(  we encountered this error " + ex.getMessage();
                }

                for (DocTemplateParameter dtp : customReportConfig.getTemplateParameters()) {
                    if (bvm != null) {
                        if (!dtp.getType().equals("LIST")) {
                            configs.put(dtp.getKeyName(), bvm.get(dtp.getKeyName()));
                        }
                    }
                    else {
                        return "";
                    }
                }

                StringWriter out = new StringWriter();
                Configuration cfg = new Configuration();
                cfg.setDefaultEncoding("UTF-8");
                Template t;
                try {
                    t = new Template("name", new StringReader(customReportConfig.getHtmlContent()), cfg);
                    t.process(configs, out);
                }
                catch (Exception ex) {
                    Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                    return "Something went wrong :(  we encountered this error " + ex.getMessage();
                }
                String processedWidget = out.getBuffer().toString();

                return processedWidget;
            }
        }
        return "";
    }

    public String evaluateDetailedReportWithCustomization(ReportModel reportModel, CustomReportConfig customReportConfig) {
        if (customReportConfig != null) {
            if (customReportConfig.getHtmlContent() != null
                    && !customReportConfig.getHtmlContent().isEmpty()) {

                ArrayList<RecordModel> detailReportData = null;
                ReportRequest request = new ReportRequest();
                request.setReportModel(reportModel);
                request.setLimit(1000);
                request.setOffset(0);
                request.setRequestActionType("0");
                request.setGetData(true);
                ReportHandler reportHandler = new ReportHandler();
                ReportResponse reportResponse = reportHandler.execute(request);
                if (reportResponse.getErrorCode() == 1000 && reportResponse.getDataRecords() != null && !reportResponse.getDataRecords().isEmpty()) {
                    detailReportData = new ArrayList<>(reportResponse.getDataRecords());
                }

                BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                LinkedHashMap<String, String> bvm = null;
                LinkedHashMap<String, String> dynamicFieldsMap = new LinkedHashMap<>();

                Map<String, Object> configs = new HashMap<>();
                for (DocTemplateParameter dtp : customReportConfig.getTemplateParameters()) {
                    switch (dtp.getType()) {
                        case "STRING":
                            beezlInterperter.addDynamicField(dtp.getKeyName(), dtp.getKeyValue());
                            dynamicFieldsMap.put(dtp.getKeyName(), dtp.getKeyValue());
                            break;
                        case "LIST":
                            ArrayList<RecordModel> contextRecordsList;
                            contextRecordsList = detailReportData;
                            ArrayList<DynamicTemplateReference> dynamicObjects = new ArrayList<>();
                            HashMap<String, String> propertValueMap;
                            if (contextRecordsList != null && !contextRecordsList.isEmpty()) {
                                for (RecordModel rm : contextRecordsList) {
                                    propertValueMap = new HashMap<>();
                                    for (Map.Entry<String, ModuleFieldModel> entry : rm.getFieldValueMap().entrySet()) {
                                        String value = "";
                                        if (entry.getValue().getCurrentValue() != null) {
                                            value = entry.getValue().getCurrentValue();
                                        }
                                        propertValueMap.put(entry.getKey(), value);
                                    }
                                    dynamicObjects.add(new DynamicTemplateReference(propertValueMap));
                                }
                            }
                            configs.put(dtp.getKeyName(), dynamicObjects);
                            break;
                    }
                }

//                CubeProcessor cubeProcessor = new CubeProcessor(dynamicFieldsMap, 10);
//                bvm = cubeProcessor.execute();
                try {
                    bvm = beezlInterperter.evaluate();
                }
                catch (Exception ex) {
                    Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                    return "Something went wrong :(  we encountered this error " + ex.getMessage();
                }

                for (DocTemplateParameter dtp : customReportConfig.getTemplateParameters()) {
                    if (bvm != null) {
                        if (!dtp.getType().equals("LIST")) {
                            configs.put(dtp.getKeyName(), bvm.get(dtp.getKeyName()));
                        }
                    }
                    else {
//                        return "";
                    }
                }

                StringWriter out = new StringWriter();
                Configuration cfg = new Configuration();
                cfg.setDefaultEncoding("UTF-8");
                Template t;
                try {
                    String modifiedTemplate = customReportConfig.getHtmlContent();
                    modifiedTemplate = modifiedTemplate.replace("<!--", "<");
                    modifiedTemplate = modifiedTemplate.replace("-->", ">");
                    modifiedTemplate = modifiedTemplate.replace("&#39;", "'");
                    t = new Template("name", new StringReader(modifiedTemplate), cfg);
                    t.process(configs, out);
                }
                catch (Exception ex) {
                    Logger.getLogger(ReportViewer.class.getName()).log(Level.SEVERE, null, ex);
                }
                String processedWidget = out.getBuffer().toString();

                return processedWidget;
            }
        }
        return "";
    }

    public void generatePdf(String reportId) {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
            // So, we're rendering the HTML. Return a stub StreamedContent so that it will generate right URL.
            convertedReport = new DefaultStreamedContent();
        }
        else {
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("id", "=", reportId));
            ReportRequest reportRequest = new ReportRequest();
            reportRequest.setRequestActionType("0");
            reportRequest.setClause(fm.getClause());
            ReportResponse reportResponse = null;
            ReportHandler reportHandler = new ReportHandler();
            try {
                reportResponse = reportHandler.execute(reportRequest);
            }
            catch (Exception ex) {
                Logger.getLogger(ApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            ReportModel reportModel = null;
            if (reportResponse != null && reportResponse.getErrorCode() == 1000) {
                reportModel = reportResponse.getReturnList().get(0);
            }

            if (reportModel.getCustomReportConfig() != null) {
                DocumentUtilsHandler documentUtilsHandler = new DocumentUtilsHandler();
                DocumentUtilRequest documentUtilRequest = new DocumentUtilRequest();
                documentUtilRequest.setRequestActionType("0");
                documentUtilRequest.setHtmlContent(reportModel.getCustomReportConfig().getHtmlContent());
                DocumentUtilResponse documentUtilResponse = documentUtilsHandler.executeRequest(documentUtilRequest);
                if (documentUtilResponse.getErrorCode() == 1000 && documentUtilResponse.getConvertedDocument() != null) {
                    convertedReport = DefaultStreamedContent.builder()
                            .name(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue() + ".pdf")
                            .contentType("application/pdf")
                            .stream(() -> new ByteArrayInputStream(
                            documentUtilResponse.getConvertedDocument().getContentStream()))
                            .build();
                }
            }
        }
    }

    public boolean isAjaxRequest() {
        boolean val = FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest();
        return val;
    }

    public StreamedContent getConvertedReport() {
        return convertedReport;
    }

}
