/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.reports;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.deals.DealsBean;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.DataViewHandler;
import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.ReportHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.beezer.web.models.SelectionModel;
import com.crm.models.internal.DataTableSettings;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.dataView.DataViewSettings;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.DataViewRequest;
import com.crm.models.requests.ReportRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.responses.DataViewResponse;
import com.crm.models.responses.ReportResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "reportsBean")
@ViewScoped
public class ReportsBean extends BeanFramework {

    private HashMap<String, String> moduleMap;
    private ArrayList<RecordModel> reportList;
    private DataViewSettings dataViewSettings;

    private String naturalLanguageQuery;
    private String producedSql;

    private ReportViewer reportViewer;
    private boolean showAiReport;

    @PostConstruct
    public void init() {
        super.setModuleId("12");
        super.loadFilters("12");
        this.loadModules();
        this.loadRecordsList();
        this.loadDataViewSettings();
        super.initializeTableColumnDesigner();
    }

    public void loadRecordsList() {
        super.setLazyRecordModel(new LazyRecordModel(null, false, LazyRecordModel.REPORT));
    }

    public void loadRecordsListInstantly() {
        RequestModel request = new RequestModel();
        request.setClause(null);
        request.setRequestingModule("12");
        request.setRequestActionType("0");
        request.setSkipContent(true);
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = requestHandler.executeRequest(request);
        if (response.getErrorCode() == 1000) {
            this.reportList = response.getRecordList();
        }
    }

    public void loadDataViewSettings() {
        DataViewRequest dataViewRequest = new DataViewRequest();
        dataViewRequest.setRequestActionType("0");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("module_id", "=", super.getModuleId()));
        dataViewRequest.setClause(fm.getClause());
        DataViewHandler dataViewHandler = new DataViewHandler();
        DataViewResponse dataViewResponse = dataViewHandler.executeRequest(dataViewRequest);
        if (dataViewResponse.getErrorCode() == 1000) {
            if (dataViewResponse.getReturnList() != null && !dataViewResponse.getReturnList().isEmpty()) {
                this.dataViewSettings = dataViewResponse.getReturnList().get(0);

                if (this.dataViewSettings.getDataViewConfig().isEnableTabularView()) {
                    if (this.dataViewSettings.getDataViewConfig().getTabularViewSettings() != null) {
                        for (DataTableSettings dts : this.dataViewSettings.getDataViewConfig().getTabularViewSettings().getTableSettings()) {
                            if (dts.getColumnLabel() != null && !dts.getColumnLabel().isEmpty()) {
                                dts.setColumnLabel(dts.getColumnLabel().replace("_", " "));
                                dts.setColumnLabel(Character.toUpperCase(dts.getColumnLabel().charAt(0)) + dts.getColumnLabel().substring(1));
                            }

                            if (dts.getColumnFieldName().equals("primary_module.name")) {
                                dts.setStyling("'font-weight: bold;'");
                            }
                        }
                        super.setSettingsList(this.dataViewSettings.getDataViewConfig().getTabularViewSettings().getTableSettings());
                    }
                }

            }
            else {
                super.initializeTableColumnDesigner();
            }
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("addReport_new.xhtml?report="
                    + super.getSelectedRecords().get(0).getFieldValueMap().get("id").getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT);
        }
        catch (Exception ex) {
            Logger.getLogger(DealsBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void loadModules() {
        this.moduleMap = new HashMap<>();
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response;
            response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                for (ModuleModel mm : response.getReturnList()) {
                    this.moduleMap.put(String.valueOf(mm.getModuleId()), mm.getModuleName());
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ReportsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void quickViewReport(RecordModel model) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("reportViewer.xhtml?moduleId=" + "12"
                    + "&report=" + model.getFieldValueMap().get("id").getCurrentValue());
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(ReportsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void openNaturalLanguageDialog() {
        naturalLanguageQuery = null;
        producedSql = null;
        PrimeFaces.current().ajax().update("naturalLanguageToSqlDialog");
        PrimeFaces.current().executeScript("PF('naturalLanguageToSqlDialog').show()");
    }

    public void processNaturalLanguageQuery() {
        if (naturalLanguageQuery != null && !naturalLanguageQuery.isEmpty()) {
            showAiReport = false;
            PrimeFaces.current().ajax().update("aiReportHolder");
//            RequestModel request = new RequestModel();
//            request.setClause(naturalLanguageQuery);
//            request.setRequestActionType("99");
//            request.setSkipContent(true);
//            RequestHandler requestHandler = new RequestHandler();
//            ResponseModel response = requestHandler.executeRequest(request);
//            if (response.getErrorCode() == 1000) {
//                this.producedSql = response.getErrorMessage();
//                PrimeFaces.current().ajax().update("producedSQLPanel");
//            }

            ReportRequest reportRequest = new ReportRequest();
            reportRequest.setClause(naturalLanguageQuery);
            reportRequest.setRequestActionType("4");
            ReportHandler reportHandler = new ReportHandler();
            ReportResponse reportResponse = reportHandler.execute(reportRequest);
            if (reportResponse.getErrorCode() == 1000 && reportResponse.getReturnList() != null && !reportResponse.getReturnList().isEmpty()) {
                showAiReport = true;
                PrimeFaces.current().ajax().update("aiReportHolder");
                this.reportViewer = new ReportViewer();
                this.reportViewer.initializeForPreview(reportResponse.getReturnList().get(0));
                PrimeFaces.current().ajax().update("producedReportPanel");
            }
        }
    }

    @Override
    protected void loadLookup(String fieldName, String lookupId, String... apiKey) {
        if (fieldName.equals("primary_module")) {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response;
            try {
                response = moduleHandler.managerExecutor(request);
                if (response.getErrorCode() == 1000) {
                    super.selectedCache.put(fieldName, null);
                    TreeMap<String, String> temp = new TreeMap<>();
                    for (ModuleModel mm : response.getReturnList()) {
                        temp.put(mm.getModuleName(), String.valueOf(mm.getModuleId()));
                        super.selectionMap.put(fieldName, new SelectionModel());
                    }
                    super.lookupDictionary.put(fieldName, temp);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(AddReportBean_new.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            super.loadLookup(fieldName, lookupId);
        }
    }

    @Override
    public void search(String api) {
        super.search("GenericMasterAPI");
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        request.setBulkRecords(new ArrayList<>(super.getSelectedRecords()));
        request.setRequestActionType("2");
        request.setRequestingModule(super.getModuleId());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.getSelectedRecords().clear();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record deleted Susscefully."));
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete record."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ReportsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public HashMap<String, String> getModuleMap() {
        return moduleMap;
    }

    public void setModuleMap(HashMap<String, String> moduleMap) {
        this.moduleMap = moduleMap;
    }

    public ArrayList<RecordModel> getReportList() {
        return reportList;
    }

    public void setReportList(ArrayList<RecordModel> reportList) {
        this.reportList = reportList;
    }

    public String getNaturalLanguageQuery() {
        return naturalLanguageQuery;
    }

    public void setNaturalLanguageQuery(String naturalLanguageQuery) {
        this.naturalLanguageQuery = naturalLanguageQuery;
    }

    public String getProducedSql() {
        return producedSql;
    }

    public void setProducedSql(String producedSql) {
        this.producedSql = producedSql;
    }

    public ReportViewer getReportViewer() {
        return reportViewer;
    }

    public void setReportViewer(ReportViewer reportViewer) {
        this.reportViewer = reportViewer;
    }

    public boolean isShowAiReport() {
        return showAiReport;
    }

    public void setShowAiReport(boolean showAiReport) {
        this.showAiReport = showAiReport;
    }

}
