/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.notes;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.FaceletContext;

/**
 *
 * @author badry
 */
@ManagedBean(name = "notesBean")
@ViewScoped
public class NotesBean extends BeanFramework {

    private String requestedModuleId;
    private String requestedObjectId;

    @PostConstruct
    public void init() {
        super.setModuleId("15");
        if (requestedObjectId != null) {
            super.loadFieldSet("15", null);
            super.buildRecordModel();
            this.getRequestorIds();
            this.loadRecordsList();
        }
    }

    public void getRequestorIds() {
        if (requestedModuleId == null && requestedObjectId == null) {
            FaceletContext faceletContext = (FaceletContext) FacesContext.getCurrentInstance().getAttributes().get(FaceletContext.FACELET_CONTEXT_KEY);
            requestedModuleId = (String) faceletContext.getAttribute("noteModuleId");
            requestedObjectId = (String) faceletContext.getAttribute("noteObjectId");
        }
    }

    public void loadRecordsList() {
        if (requestedModuleId != null && requestedObjectId != null) {
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("notes_base.module_id", "=", requestedModuleId));
            fm.setFilter("AND", new FilterType("notes_base.object_id", "=", requestedObjectId));
            RequestModel request = new RequestModel();
            request.setClause(fm.getClause());
            request.setRequestingModule(super.getModuleId());
            request.setRequestActionType("0");
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response;
            try {
                response = requestHandler.executeRequest(request, "GenericMasterAPI");
                super.setRecordsList(response.getRecordList());
            }
            catch (Exception ex) {
                Logger.getLogger(NotesBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void save() {
        super.getRecordModel().getFieldValueMap().get("user_id").setCurrentValue(String.valueOf(GeneralUtils.getLoggedUser().getUserId()));
        super.getRecordModel().getFieldValueMap().get("module_id").setCurrentValue(requestedModuleId);
        super.getRecordModel().getFieldValueMap().get("object_id").setCurrentValue(requestedObjectId);
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("1");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            super.getRecordModel().getFieldValueMap().get("note_content").setCurrentValue("");
            this.loadRecordsList();
        }
        catch (Exception ex) {
            Logger.getLogger(NotesBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteNote(RecordModel recordModel) {
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            super.getRecordsList().remove(recordModel);
//            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(NotesBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getRequestedModuleId() {
        return requestedModuleId;
    }

    public void setRequestedModuleId(String requestedModuleId) {
        this.requestedModuleId = requestedModuleId;
    }

    public String getRequestedObjectId() {
        return requestedObjectId;
    }

    public void setRequestedObjectId(String requestedObjectId) {
        this.requestedObjectId = requestedObjectId;
    }

}
