/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.modules.categories;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.products.ProductsBean;
import com.beezer.web.handler.RequestHandler;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.event.ActionEvent;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.MenuActionEvent;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author badry
 */
@ManagedBean(name = "catalogBean")
@ViewScoped
public class CatalogBean extends BeanFramework {

    private String selectedCatalog;
    private ArrayList<RecordModel> categoryList;
    private RecordModel selectedCategory;
    private ArrayList<RecordModel> productsList;
    private RecordModel selectedProduct;

    private MenuModel menuModel;
    private int breadId = 0;

    @PostConstruct
    public void init() {
        super.setModuleId("8");
        this.getRequestedCategory();
        this.loadCategories();
        super.loadFieldSet(super.getModuleId(), null);
        super.buildRecordModel();
        this.menuModel = new DefaultMenuModel();
        buildBreadCrumbs();
    }

    private void getRequestedCategory() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        selectedCatalog = params.get("catalog");
    }

    private void loadCategories() {
        try {
            RequestModel requestModel = new RequestModel();
            requestModel.setRequestingModule(super.getModuleId());
            requestModel.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("parent_cat", "=", selectedCatalog != null ? selectedCatalog : -1));
            requestModel.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel responseModel = requestHandler.executeRequest(requestModel, "GenericMasterAPI");
            if (responseModel.getErrorCode() == 1000) {
                this.categoryList = responseModel.getRecordList();
                this.loadProducts();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(CatalogBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadProducts() {
        try {
            RequestModel requestModel = new RequestModel();
            requestModel.setRequestingModule("3");
            requestModel.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("products_base.category_id", "=", selectedCatalog != null ? selectedCatalog : -1));
            requestModel.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel responseModel = requestHandler.executeRequest(requestModel, "GenericMasterAPI");
            if (responseModel.getErrorCode() == 1000) {
                this.productsList = responseModel.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(CatalogBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void openCategory() {
        if (selectedCategory != null) {
            selectedCatalog = String.valueOf(selectedCategory.getFieldValueMap().get("id").getCurrentValue());
            this.loadCategories();
            super.buildRecordModel();
            buildBreadCrumbs();
        }
    }

    public void buildBreadCrumbs() {
        DefaultMenuItem item = new DefaultMenuItem();
        if (selectedCategory == null) {
            item.setValue("Categories");
            item.setId("home");
            item.setParam("key", "-1");
            item.setUpdate("@form");
            this.menuModel.getElements().add(item);

            item = new DefaultMenuItem();
            item.setValue("Catalogs");
            item.setId("0");
            item.setParam("key", "0");
            item.setCommand("#{catalogBean.navigate}");
            item.setUpdate("@form");
        }
        else {
            item = new DefaultMenuItem();
            item.setValue(selectedCategory.getFieldValueMap().get("name").getCurrentValue());
            breadId++;
            item.setId(String.valueOf(breadId));
            item.setParam("key", String.valueOf(selectedCategory.getFieldValueMap().get("id").getCurrentValue()));
            item.setCommand("#{catalogBean.navigate}");
            item.setUpdate("@form");
        }
        this.menuModel.getElements().add(item);
        this.menuModel.generateUniqueIds();
    }

    public void navigate(ActionEvent event) {
        MenuItem menuItem = ((MenuActionEvent) event).getMenuItem();
        selectedCatalog = menuItem.getParams().get("key").get(0);
        if (selectedCatalog.equals("0")) {
            selectedCatalog = null;
            breadId = 1;
        }
        boolean needsRemoval = false;
        MenuModel updatedMenu = new DefaultMenuModel();
        for (MenuElement me : this.menuModel.getElements()) {
            MenuItem mi = (MenuItem) me;
            if (!needsRemoval) {
                if (selectedCatalog == null) {
                    if (mi.getParams().get("key").get(0).equals("0")) {
                        needsRemoval = true;
                    }
                }
                else if (mi.getParams().get("key").get(0).equals(selectedCatalog)) {
                    needsRemoval = true;
                }
                updatedMenu.getElements().add(me);
            }
        }
        this.menuModel = updatedMenu;
        this.loadCategories();
        super.buildRecordModel();
    }

    public void navigate2(ActionEvent event) {
        MenuItem menuItem = ((MenuActionEvent) event).getMenuItem();
        selectedCatalog = menuItem.getParams().get("key").get(0);
        if (selectedCatalog.equals("0")) {
            selectedCatalog = null;
        }
        boolean needsRemoval = false;
        MenuModel updatedMenu = new DefaultMenuModel();
        for (MenuElement me : this.menuModel.getElements()) {
            MenuItem mi = (MenuItem) me;
            if (!needsRemoval) {
                if (selectedCatalog == null) {
                    if (mi.getParams().get("key").get(0).equals("0")) {
                        needsRemoval = true;
                    }
                }
                else if (mi.getParams().get("key").get(0).equals(selectedCatalog)) {
                    needsRemoval = true;
                }
                updatedMenu.getElements().add(me);
            }
        }
        this.menuModel = updatedMenu;
        this.loadCategories();
        super.buildRecordModel();
    }

    public void openProduct() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("productDetails.xhtml?&product="
                    + this.getSelectedProduct().getFieldValueMap().get("product_id").getCurrentValue());
        }
        catch (IOException ex) {
            Logger.getLogger(CatalogBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addProduct() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("addProduct.xhtml?faces-redirect=true&catalog="
                    + selectedCatalog);
        }
        catch (IOException ex) {
            Logger.getLogger(CatalogBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteCategory(RecordModel recordModel){
    }

    public void addCategory() {
        if (recordModel != null) {
            try {
                recordModel.getFieldValueMap().get("type").setCurrentValue("PRODUCTS");
                recordModel.getFieldValueMap().get("parent_cat").setCurrentValue(selectedCatalog != null ? selectedCatalog : "-1");
                RequestModel requestModel = new RequestModel();
                requestModel.setRequestingModule(super.getModuleId());
                requestModel.setRequestActionType("1");
                requestModel.setRecordModel(recordModel);
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel responseModel = requestHandler.executeRequest(requestModel, "GenericMasterAPI");
                if (responseModel.getErrorCode() == 1000) {
                    this.loadCategories();
                }
            }
            catch (Exception ex) {
                Logger.getLogger(CatalogBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ArrayList<RecordModel> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<RecordModel> categoryList) {
        this.categoryList = categoryList;
    }

    public RecordModel getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(RecordModel selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public ArrayList<RecordModel> getProductsList() {
        return productsList;
    }

    public void setProductsList(ArrayList<RecordModel> productsList) {
        this.productsList = productsList;
    }

    public RecordModel getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(RecordModel selectedProduct) {
        this.selectedProduct = selectedProduct;
    }

    public String getSelectedCatalog() {
        return selectedCatalog;
    }

    public void setSelectedCatalog(String selectedCatalog) {
        this.selectedCatalog = selectedCatalog;
    }

    public MenuModel getMenuModel() {
        return menuModel;
    }

    public void setMenuModel(MenuModel menuModel) {
        this.menuModel = menuModel;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        if(selectedCategory != null){
            try {
                RequestModel requestModel = new RequestModel();
                requestModel.setRequestingModule(super.getModuleId());
                requestModel.setRequestActionType("2");
                requestModel.setRecordModel(selectedCategory);
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel responseModel = requestHandler.executeRequest(requestModel, "GenericMasterAPI");
                if(responseModel.getErrorCode() == 1000){
                    this.selectedCatalog = selectedCategory.getFieldValueMap().get("id").getCurrentValue();
                    this.loadProducts();
                    ProductsBean productsBean = new ProductsBean();
                    productsBean.setSelectedRecords(this.productsList);
                    productsBean.delete();
                    this.loadCategories();
                }
            }
            catch (Exception ex) {
                Logger.getLogger(CatalogBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
