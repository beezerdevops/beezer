/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.supportCentral.SupportCentralHelper;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.AccessHandler;
import com.beezer.web.handler.ApiKeyHandler;
import com.beezer.web.handler.CompaniesHandler;
import com.beezer.web.handler.ForgotPasswordHandler;
import com.beezer.web.handler.LocaleHandler;
import com.beezer.web.handler.LookAndFeelHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.handler.TimezoneHandler;
import com.beezer.web.handler.UserHandler;
import com.beezer.web.listeners.SessionListener;
import com.beezer.web.utils.Decryptor;
import com.beezer.web.utils.Encryptor;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.CompanyModel;
import com.crm.models.global.UserModel;
import com.crm.models.internal.LookAndFeelModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.application.AppModuleConfigModel;
import com.crm.models.internal.application.ApplicationModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.localization.LocalizationHost;
import com.crm.models.internal.security.permissions.ObjectPermissionModel;
import com.crm.models.requests.CompanyRequest;
import com.crm.models.requests.LookAndFeelRequest;
import com.crm.models.requests.UserRequest;
import com.crm.models.requests.managers.APIManagerRequest;
import com.crm.models.requests.managers.AccessRequest;
import com.crm.models.requests.managers.LocaleRequest;
import com.crm.models.responses.CompanyResponse;
import com.crm.models.responses.LookAndFeelResponse;
import com.crm.models.responses.UserResponse;
import com.crm.models.responses.managers.APIManagerResponse;
import com.crm.models.responses.managers.AccessResponse;
import com.crm.models.responses.managers.LocaleResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.primefaces.PrimeFaces;

/**
 *
 * @author badry
 */
@ManagedBean(name = "accessBean")
@SessionScoped
public class AccessBean implements Serializable{

    @ManagedProperty(value = "#{userModel}")
    private UserModel userModel;

    private UserModel concurrentUserModel;

    @ManagedProperty(value = "#{companyModel}")
    private CompanyModel companyModel;

    private boolean authenticationFailed = false;
    private String email;
    private String password;
    private String oldPassword;
    private ArrayList<ApplicationModel> applicationList;
    private ApplicationModel activeApplication;
    private boolean admin;
    private boolean adminConsole;
    private LocalizationHost localizationHost;
    private ArrayList<RecordModel> notifications;
    private boolean newNotification;
    private String authenticationMessage;
    private String iconStyle;
    private String appLookAndFeel;
    private FacesContext concurrentContect;
    private boolean forceLogin;

    private BeanFramework activeView;
    private SupportCentralHelper supportCentralHelper;

    public String loginMasterStyling = "";

    private boolean validToken;
    private String token;

    private String forgotPasswordEmail;

    private String companyTimezone;
    
    private boolean sandboxEnabled = false;

    @PostConstruct
    public void init() {
        this.loadHomeRequestUrl();
//        this.checkRequestParams();
    }

    public void loadHomeRequestUrl() {
        HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String subdomain = origRequest.getServerName().split("\\.")[0];
        if (subdomain != null && !subdomain.isEmpty()) {
            if (!subdomain.equals("app")) {
                if (subdomain.equals("sandbox")) {
                    this.sandboxEnabled = true;
                }
            }
        }
    }

    public void checkRequestParams() {
//        String token = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("token");
        if (token != null && !token.isEmpty()) {
            PrimeFaces.current().executeScript("PF('autoSigninNotification').show()");
            String serializedMap = Decryptor.decrypt(token);
            ObjectMapper mapper = new ObjectMapper();
            HashMap<String, String> userInfoMap = null;
            try {
                userInfoMap = mapper.readValue(serializedMap, HashMap.class);
            }
            catch (IOException ex) {
                PrimeFaces.current().executeScript("PF('autoSigninNotification').hide()");
                PrimeFaces.current().executeScript("PF('invalidTokenNotification').show()");
                Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (userInfoMap != null) {
                validToken = false;
                try {
                    Date expiryDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(userInfoMap.get("tokenExpiry"));
                    Date currentDate = new Date();
                    if (expiryDate.after(currentDate)) {
                        validToken = true;
                    }
                }
                catch (ParseException ex) {
                    PrimeFaces.current().executeScript("PF('autoSigninNotification').hide()");
                    PrimeFaces.current().executeScript("PF('invalidTokenNotification').show()");
                    Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (validToken) {
                    this.email = userInfoMap.get("userEmail");
                    this.password = userInfoMap.get("userPassword");
                    this.signIn();
                }
                else {
                    PrimeFaces.current().executeScript("PF('autoSigninNotification').hide()");
                    PrimeFaces.current().executeScript("PF('invalidTokenNotification').show()");
                }
            }
        }
    }

    public void signIn() {
        try {
            AccessHandler accessHandler = new AccessHandler();
            AccessRequest request = new AccessRequest();
            request.setActionType(AccessRequest.ACTIONTYPE_LOGIN);
            request.setUserName(this.email);
            request.setPassword(this.password);
            request.setAccessScope("APP_SPACE");
            AccessResponse response = accessHandler.accessExcecutor(request);
            if (response.getErrorCode() == 1000) {

                boolean skipSessionRegisteration = false;
                if (Defines.LOGIN_LEDGER.get(response.getUserProfile().getUserId()) != null) {
                    HttpSession existingSession = Defines.LOGIN_LEDGER.get(response.getUserProfile().getUserId());
                    FacesContext facesContext = FacesContext.getCurrentInstance();
                    HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
                    if (existingSession.getId().equals(session.getId())) {
                        skipSessionRegisteration = true;
                    }
                    else {
                        concurrentContect = FacesContext.getCurrentInstance();
                        concurrentUserModel = response.getUserProfile();
                        PrimeFaces.current().executeScript("PF('doubleLoginWarningDialog').show()");
                        return;
                    }
                }

                if (!skipSessionRegisteration) {
                    FacesContext facesContext = FacesContext.getCurrentInstance();
                    HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);

                    SessionListener sessionListener = new SessionListener();
                    sessionListener.setPf(PrimeFaces.current());
                    sessionListener.setUserModel(response.getUserProfile());
                    session.setAttribute("user", sessionListener);
                }

                this.authenticationFailed = false;
                this.setUserModel(response.getUserProfile());
                if(this.sandboxEnabled){
                    this.getUserModel().setSchemaCache(response.getUserProfile().getSchemaCache()+"_sandbox");
                }
                ArrayList<ApplicationModel> tempApplicationList = response.getApplicationList();
                this.applicationList = new ArrayList<>();
                if (tempApplicationList != null) {
                    for (ApplicationModel am : tempApplicationList) {
                        if (am.isDefaultApp()) {
                            activeApplication = am;
                        }

                        if (am.isActive()) {
                            this.applicationList.add(am);
                        }
                    }
                }

                if (activeApplication == null) {
                    if (applicationList != null && applicationList.size() == 1) {
                        activeApplication = applicationList.get(0);
                    }

                    if (this.userModel.getPermissionSet() != null && this.userModel.getPermissionSet().getPermittedApplications() != null
                            && this.userModel.getPermissionSet().getPermittedApplications().getAppList() != null) {
                        for (ApplicationModel am : this.userModel.getPermissionSet().getPermittedApplications().getAppList()) {
                            if (am.isDefaultApp()) {
                                for (ApplicationModel appModel : this.applicationList) {
                                    if (am.getAppId() == appModel.getAppId()) {
                                        this.activeApplication = appModel;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }

//                FacesContext.getCurrentInstance().getExternalContext().redirect("dashboardB.xhtml");
                if (!forceLogin) {
                    if (activeApplication != null) {
                        FacesContext.getCurrentInstance().getExternalContext().redirect("personal-dashboard.xhtml");
                    }
                    else {
                        PrimeFaces.current().executeScript("PF('noAppsFoundDialog').show()");
                    }
                }
                this.loadAPIkey();
                this.loadLocalizationValues();
                this.loadNotifications();
                this.loadCompanyProfile();
                this.loadTimezone();
                this.iconStyle = "";
                this.loadIconStyles();
                this.loadLookAndFeel();
//                RequestContext.getCurrentInstance().update("moduleMenu");
            }
            else {
                this.authenticationFailed = true;
                this.authenticationMessage = response.getErrorMessage();
                PrimeFaces.current().ajax().update("somethingWentWrongWarningDialog");
                PrimeFaces.current().executeScript("PF('somethingWentWrongWarningDialog').show()");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void signIn(String username, String password) {
        try {
            AccessHandler accessHandler = new AccessHandler();
            AccessRequest request = new AccessRequest();
            request.setActionType(AccessRequest.ACTIONTYPE_LOGIN);
            request.setUserName(username);
            request.setPassword(password);
            request.setAccessScope("APP_SPACE");
            AccessResponse response = accessHandler.accessExcecutor(request);
            if (response.getErrorCode() == 1000) {
//                FacesContext facesContext = FacesContext.getCurrentInstance();
//                HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
//                session.setAttribute("user", response.getUserProfile());

                this.authenticationFailed = false;
                this.setUserModel(response.getUserProfile());
                if(this.sandboxEnabled){
                    this.getUserModel().setSchemaCache(response.getUserProfile().getSchemaCache()+"_sandbox");
                }
                ArrayList<ApplicationModel> tempApplicationList = response.getApplicationList();
                this.applicationList = new ArrayList<>();
                if (tempApplicationList != null) {
                    for (ApplicationModel am : tempApplicationList) {
                        if (am.isDefaultApp()) {
                            activeApplication = am;
                        }

                        if (am.isActive()) {
                            this.applicationList.add(am);
                        }
                    }
                }

//                FacesContext.getCurrentInstance().getExternalContext().redirect("dashboard.xhtml");
                this.loadLocalizationValues();
                this.loadNotifications();
                this.loadCompanyProfile();
                this.iconStyle = "";
                this.loadIconStyles();
                this.loadLookAndFeel();
//                RequestContext.getCurrentInstance().update("moduleMenu");
            }
            else {
                this.authenticationFailed = true;
                this.authenticationMessage = response.getErrorMessage();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void forgotPassword() {
        RequestModel requestModel = new RequestModel();
        requestModel.setRequestActionType("0");
        requestModel.setKeyObjectMap(new HashMap<>());
        requestModel.getKeyObjectMap().put("userEmail", this.forgotPasswordEmail);
        ForgotPasswordHandler forgotPasswordHandler = new ForgotPasswordHandler();
        ResponseModel responseModel = forgotPasswordHandler.executeForManager(requestModel);
        if (responseModel.getErrorCode() == 1000) {
            PrimeFaces.current().executeScript("PF('forgotPasswordSuccessful').show()");
        }
        else {
            PrimeFaces.current().ajax().update("somethingWentWrongWarningDialog");
            PrimeFaces.current().executeScript("PF('somethingWentWrongWarningDialog').show()");
        }
    }

    public void loadLookAndFeel() {
        this.appLookAndFeel = "";
        LookAndFeelRequest lookAndFeelRequest = new LookAndFeelRequest();
        lookAndFeelRequest.setRequestActionType("0");
        LookAndFeelHandler handler = new LookAndFeelHandler();
        LookAndFeelResponse lookAndFeelResponse = handler.executeRequest(lookAndFeelRequest);
        if (lookAndFeelResponse.getErrorCode() == 1000) {
            if (lookAndFeelResponse.getReturnList() != null && !lookAndFeelResponse.getReturnList().isEmpty()) {
                LookAndFeelModel lookAndFeelModel = lookAndFeelResponse.getReturnList().get(0);
                String sideBarBgColor = "";
                if (lookAndFeelModel.getSideBarBackgroundColor() != null && !lookAndFeelModel.getSideBarBackgroundColor().isEmpty()) {
                    sideBarBgColor = ".beezer-theme .ui-menu, .ui-menu .ui-menu-child{\n"
                            + "    background: #" + lookAndFeelModel.getSideBarBackgroundColor() + ";\n"
                            + "}\n"
                            + ".beezer-theme #sidebar-wrapper{\n"
                            + "    background: #" + lookAndFeelModel.getSideBarBackgroundColor() + ";\n"
                            + "}\n";
                    this.appLookAndFeel = this.appLookAndFeel + sideBarBgColor;
                }
                String sideBarTextColor = "";
                if (lookAndFeelModel.getSideBarTextColor() != null && !lookAndFeelModel.getSideBarTextColor().isEmpty()) {
                    sideBarTextColor = ".beezer-theme .ui-menu .ui-menuitem-link{\n"
                            + "    color: #" + lookAndFeelModel.getSideBarTextColor() + ";\n"
                            + "}\n";
                    this.appLookAndFeel = this.appLookAndFeel + sideBarTextColor;
                }
                String logoBgColor = "";
                if (lookAndFeelModel.getSideBarLogoBackgroundColor() != null && !lookAndFeelModel.getSideBarLogoBackgroundColor().isEmpty()) {
                    logoBgColor = ".beezer-theme .sidebar-nav li:first-child a{\n"
                            + "    background: #" + lookAndFeelModel.getSideBarLogoBackgroundColor() + ";\n"
                            + "}\n";
                    this.appLookAndFeel = this.appLookAndFeel + logoBgColor;
                }
                String seperatorBgColor = "";
                if (lookAndFeelModel.getSideBarMenuSeperatorBackgroundColor() != null && !lookAndFeelModel.getSideBarMenuSeperatorBackgroundColor().isEmpty()) {
                    seperatorBgColor = ".beezer-theme .ui-widget-header.side{\n"
                            + "    background: #" + lookAndFeelModel.getSideBarMenuSeperatorBackgroundColor() + ";\n"
                            + "}\n";
                    this.appLookAndFeel = this.appLookAndFeel + seperatorBgColor;
                }
                String seperatorTextColor = "";
                if (lookAndFeelModel.getSideBarMenuSeperatorTextColor() != null && !lookAndFeelModel.getSideBarMenuSeperatorTextColor().isEmpty()) {
                    seperatorTextColor = ".beezer-theme .ui-widget-header.side h3{\n"
                            + "    color: #" + lookAndFeelModel.getSideBarMenuSeperatorTextColor() + ";\n"
                            + "}\n";
                    this.appLookAndFeel = this.appLookAndFeel + seperatorTextColor;
                }
                String topNavBgColor = "";
                if (lookAndFeelModel.getTopNavBarBackgroundColor() != null && !lookAndFeelModel.getTopNavBarBackgroundColor().isEmpty()) {
                    topNavBgColor = ".beezer-theme .top-nav-container{\n"
                            + "    background: #" + lookAndFeelModel.getTopNavBarBackgroundColor() + ";\n"
                            + "}\n";
                    this.appLookAndFeel = this.appLookAndFeel + topNavBgColor;
                }
                String topNavIconBgColor = "";
                if (lookAndFeelModel.getTopNavBarIconColor() != null && !lookAndFeelModel.getTopNavBarIconColor().isEmpty()) {
                    topNavIconBgColor = ".beezer-theme .top-nav-menu-items i{\n"
                            + "    color: #" + lookAndFeelModel.getTopNavBarIconColor() + ";\n"
                            + "}\n"
                            + ".beezer-theme .hamburger.is-open .hamb-top{\n"
                            + "    background-color: #" + lookAndFeelModel.getTopNavBarIconColor() + ";\n"
                            + "}\n"
                            + "\n"
                            + ".beezer-theme .hamburger.is-open .hamb-bottom{\n"
                            + "    background-color: #" + lookAndFeelModel.getTopNavBarIconColor() + ";\n"
                            + "}\n"
                            + "\n"
                            + ".beezer-theme .hamburger.is-closed .hamb-top{\n"
                            + "    background-color: #" + lookAndFeelModel.getTopNavBarIconColor() + ";\n"
                            + "}\n"
                            + "\n"
                            + ".beezer-theme .hamburger.is-closed .hamb-middle{\n"
                            + "    background-color: #" + lookAndFeelModel.getTopNavBarIconColor() + ";\n"
                            + "}\n"
                            + "\n"
                            + ".beezer-theme .hamburger.is-closed .hamb-bottom{\n"
                            + "    background-color: #" + lookAndFeelModel.getTopNavBarIconColor() + ";\n"
                            + "}\n";
                    this.appLookAndFeel = this.appLookAndFeel + topNavIconBgColor;
                }
                String activeAppColor = "";
                if (lookAndFeelModel.getActiveAppNameColor() != null && !lookAndFeelModel.getActiveAppNameColor().isEmpty()) {
                    activeAppColor = ".beezer-theme .top-active-app-name label{\n"
                            + "    color: #" + lookAndFeelModel.getActiveAppNameColor() + ";\n"
                            + "}\n";
                    this.appLookAndFeel = this.appLookAndFeel + activeAppColor;
                }

                if (lookAndFeelModel.getCustomStyling() != null && !lookAndFeelModel.getCustomStyling().isEmpty()) {
                    this.appLookAndFeel = this.appLookAndFeel + "\n" + lookAndFeelModel.getCustomStyling();
                }
            }
        }
    }

    public void signOut() {
        this.token = null;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);

        SessionListener sessionListener = new SessionListener();
        sessionListener.setUserModel(this.userModel);
        session.removeAttribute("user");
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
        }
        catch (IOException ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @PreDestroy
    public void onDestroy() {
        this.activeView.onDestroy();
//        PrimeFaces.current().executeScript("alert('hi')");
//PrimeFaces.current().executeScript("showDoubleLoginNotification()");
//String viewId = Defines.PF_LEDGER.get(this.userModel.getUserId()).getViewRoot().getViewId();
//Map<String, Object> appMap = Defines.PF_LEDGER.get(this.userModel.getUserId()).getViewRoot().getViewMap();
//        PrimeFaces.current().ajax().update("homeBody");
//        PrimeFaces.current().executeScript("PF('doubleLoginNotificationDialog').show()");
    }

    public void forceSignIn() {
        if (this.concurrentUserModel != null) {
            HttpSession activeSession = Defines.LOGIN_LEDGER.get(this.concurrentUserModel.getUserId());
            if (activeSession != null) {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("personal-dashboard.xhtml");
//                    Defines.PF_LEDGER.get(this.concurrentUserModel.getUserId()).executeScript("PF('doubleLoginNotificationDialog').show()");
                    activeSession.invalidate();
                }
                catch (IOException ex) {
                    Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
//            Defines.LOGIN_LEDGER.remove(this.concurrentUserModel.getUserId());
            this.concurrentUserModel = null;
            forceLogin = true;

            this.signIn();
        }
    }

    public void goToDesigner() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(Defines.GENERAL_CONFIG_MAP.get("designer.url"));
        }
        catch (IOException ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    public void loadIconStyles() {
//        if (activeApplication != null && activeApplication.getModuleConfigMap() != null) {
//            for (Map.Entry<String, ArrayList<AppModuleConfigModel>> entrySet : activeApplication.getModuleConfigMap().entrySet()) {
//                ArrayList<AppModuleConfigModel> appModuleList = entrySet.getValue();
//                if (appModuleList != null) {
//                    for (AppModuleConfigModel amcm : appModuleList) {
//                        if (amcm.getDynamicIcon() != null) {
//                            String iconClass = ".nav-" + amcm.getModuleDefaultName().replaceAll("\\s+", "_").toLowerCase() + "-icon";
//                            iconClass = iconClass + "{\n";
//                            iconClass = iconClass + "background: url('" + GeneralUtils.generateDynamicResourceURL(amcm.getDynamicIcon()) + "') no-repeat !important;\n";
//                            iconClass = iconClass + "    padding: 1em;\n"
//                                    + "    margin-right: 1em;\n"
//                                    + "    background-size: contain !important;\n";
//                            iconClass = iconClass + "}\n";
//                            this.iconStyle = iconStyle + iconClass;
//                            amcm.setIcon("nav-" + amcm.getModuleDefaultName().replaceAll("\\s+", "_").toLowerCase() + "-icon");
//                        }
//                    }
//                }
//            }
//        }
//    }
    public void loadIconStyles() {
        if (activeApplication != null && activeApplication.getModuleConfigMap() != null) {
            ArrayList<AppModuleConfigModel> completeModuleList = new ArrayList<>();
            FilterManager fm = new FilterManager();
            int index = 1;
            for (Map.Entry<String, ArrayList<AppModuleConfigModel>> entrySet : activeApplication.getModuleConfigMap().entrySet()) {
                completeModuleList.addAll(entrySet.getValue());
                ArrayList<AppModuleConfigModel> appModuleList = entrySet.getValue();
                if (appModuleList != null) {
                    for (AppModuleConfigModel amcm : appModuleList) {
                        if (amcm.getDynamicIcon() != null) {
                            amcm.setIcon("nav-" + amcm.getModuleDefaultName().replaceAll("\\s+", "_").toLowerCase() + "-icon");
                            if (index == 1) {
                                fm.setFilter("AND", new FilterType("unstruct_id", "=", amcm.getDynamicIcon()));
                            }
                            else {
                                fm.setFilter("OR", new FilterType("unstruct_id", "=", amcm.getDynamicIcon()));
                            }
                            index++;
                        }
                    }
                }
            }
            HashMap<String, String> urlMap = GeneralUtils.generateBulkDynamicResourceURL(fm.getClause(), completeModuleList);
            if (urlMap != null) {
                for (Map.Entry<String, String> entry : urlMap.entrySet()) {
                    String iconClass = ".nav-" + entry.getKey() + "-icon";
                    iconClass = iconClass + "{\n";
                    iconClass = iconClass + "background: url('" + entry.getValue() + "') no-repeat !important;\n";
                    iconClass = iconClass + "    padding: 1em;\n"
                            + "    margin-right: 1em;\n"
                            + "    background-size: contain !important;\n";
                    iconClass = iconClass + "}\n";
                    this.iconStyle = iconStyle + iconClass;
                }
            }
        }

    }

    public void loadLocalizationValues() {
        try {
            LocaleRequest request = new LocaleRequest();
            request.setRequestActionType("0");
            LocaleHandler localeHandler = new LocaleHandler();
            LocaleResponse response = localeHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                this.localizationHost = response.getLocalizationHost();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String loadLocalizedValue(String key) {
        if (localizationHost != null) {
            return localizationHost.getLocalValues().get(userModel.getLang()).getKeyValueMap().get(key);
        }
        return key;
    }

    public void updateActiveApp(ApplicationModel am) {
        try {
            activeApplication = am;
//            FacesContext.getCurrentInstance().getExternalContext().redirect("dashboardB.xhtml");
            FacesContext.getCurrentInstance().getExternalContext().redirect("personal-dashboard.xhtml");
        }
        catch (IOException ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void openSupportCentral() {
        this.supportCentralHelper = new SupportCentralHelper(this);
        PrimeFaces.current().ajax().update("supportCentralDialog");
        PrimeFaces.current().executeScript("PF('supportCentralDialog').show()");
    }

    public void loadCompanyProfile() {
        try {
            CompanyRequest request = new CompanyRequest();
            request.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("company_id", "=", this.userModel.getCompanyId()));
            request.setClause(fm.getClause());
            CompaniesHandler companiesHandler = new CompaniesHandler();
            CompanyResponse response = companiesHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                this.companyModel = response.getReturnList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadTimezone() {
        RequestModel requestModel = new RequestModel();
        requestModel.setRequestActionType("0");
        requestModel.setKeyObjectMap(new HashMap<>());
        requestModel.getKeyObjectMap().put("country", this.companyModel.getCountry());
        TimezoneHandler timezoneHandler = new TimezoneHandler();
        ResponseModel responseModel = timezoneHandler.executeForManager(requestModel);
        if (responseModel.getErrorCode() == 1000 && responseModel.getRecordList() != null && !responseModel.getRecordList().isEmpty()) {
            this.companyTimezone = responseModel.getRecordList().get(0).getFieldValueMap().get("time_zone").getCurrentValue();
        }
        else {
            this.companyTimezone = "GMT+4";
        }
    }

    public void loadAPIkey() {
        APIManagerRequest apimr = new APIManagerRequest();
        apimr.setRequestActionType("0");
        apimr.setTenantId(this.userModel.getTenantId());
        ApiKeyHandler apiKeyHandler = new ApiKeyHandler();
        APIManagerResponse apiResponse = apiKeyHandler.executeRequest(apimr);
        if (apiResponse.getErrorCode() == 1000) {
            Defines.API_KEY = apiResponse.getApiKey();
        }
    }

    public void changePassword() {
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("4");
            userRequest.setOldPassword(oldPassword);
            userRequest.setUserModel(userModel);
            if (GeneralUtils.getLoggedUser().getPermissionSet().isAdmin()) {
                userRequest.setOveridePasswordVerification(true);
            }
            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
                PrimeFaces.current().executeScript("PF('changePasswordDialog').hide()");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changeEmail() {
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("5");
            userRequest.setUserModel(userModel);
            if (GeneralUtils.getLoggedUser().getPermissionSet().isAdmin()) {
                userRequest.setOveridePasswordVerification(true);
            }
            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
//                RequestContext.getCurrentInstance().execute("PF('changeEmailDialog').hide()");
                PrimeFaces.current().executeScript("PF('changeEmailDialog').hide()");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadNotifications() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule("33");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("recepient_id", "=", userModel.getUserId()));
        fm.setFilter("AND", new FilterType("is_read", "=", false));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
                notifications = response.getRecordList();
                for (RecordModel rm : notifications) {
                    if (rm.getFieldValueMap().get("is_read").getCurrentValue().equals("false")) {
                        this.newNotification = true;
                        break;
                    }
                }
            }
            PrimeFaces.current().ajax().update("notificationsList");
        }
        catch (Exception ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void notificationsRead() {
        if (notifications != null && !notifications.isEmpty()) {
            boolean update = false;
            for (RecordModel rm : notifications) {
                if (rm.getFieldValueMap().get("is_read").getCurrentValue().equals("false")) {
                    update = true;
                    break;
                }
            }
            if (update) {
                RequestModel request = new RequestModel();
                request.setRequestActionType("3");
                request.setRequestingModule("33");
                for (RecordModel rm : notifications) {
                    rm.getFieldValueMap().get("is_read").setCurrentValue("true");
                }
                request.setBulkRecords(notifications);
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel response;
                try {
                    response = requestHandler.executeRequest(request, "GenericMasterAPI");
                    if (response.getErrorCode() == 1000) {

                    }
                }
                catch (Exception ex) {
                    Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            newNotification = false;
        }
    }

    public void onNotificationSelect(RecordModel notificationModel) {
        String outcomePage;
        this.updateNotificationReadDatatus(notificationModel);
        switch (notificationModel.getFieldValueMap().get("n_module").getCurrentValue()) {
            case "7":
                outcomePage = "dealDetails.xhtml?recordId="
                        + notificationModel.getFieldValueMap().get("n_orig_obj").getCurrentValue();
                break;
            case "34":
                outcomePage = "taskDetails.xhtml?recordId="
                        + notificationModel.getFieldValueMap().get("n_orig_obj").getCurrentValue()
                        + "&" + "moduleId=" + notificationModel.getFieldValueMap().get("n_module").getCurrentValue();
                break;
            default:
                RequestModel request = new RequestModel();
                request.setRequestActionType("3");
                request.setRequestingModule("33");
                notificationModel.getFieldValueMap().get("is_read").setCurrentValue("true");
                request.setRecordModel(notificationModel);
                RequestHandler requestHandler = new RequestHandler();

                try {
                    ResponseModel response = requestHandler.executeRequest(request, "GenericMasterAPI");
                }
                catch (Exception ex) {
                    Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
                }

                outcomePage = "recordDetails.xhtml?recordId="
                        + notificationModel.getFieldValueMap().get("n_orig_obj").getCurrentValue()
                        + "&" + "moduleId=" + notificationModel.getFieldValueMap().get("n_module").getCurrentValue();
                break;
        }
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(outcomePage);
        }
        catch (IOException ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateNotificationReadDatatus(RecordModel notificationModel) {
        boolean update = false;
        if (notificationModel.getFieldValueMap().get("is_read").getCurrentValue().equals("false")) {
            update = true;
        }

        if (update) {
            RequestModel request = new RequestModel();
            request.setRequestActionType("3");
            request.setRequestingModule("33");
            notificationModel.getFieldValueMap().get("is_read").setCurrentValue("true");
            request.setRecordModel(notificationModel);
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response;
            try {
                response = requestHandler.executeRequest(request, "GenericMasterAPI");
                if (response.getErrorCode() == 1000) {

                }
            }
            catch (Exception ex) {
                Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean isPermittetedToAction(String moduleId, String action) {
        if (moduleId != null && action != null && userModel.getPermissionSet() != null && userModel.getPermissionSet().getModulePermissionList() != null) {
            for (ObjectPermissionModel opm : userModel.getPermissionSet().getModulePermissionList()) {
                if (opm.getObjectId() == Integer.valueOf(moduleId)) {
                    switch (action.toUpperCase()) {
                        case "CREATE":
                            return opm.isCreate();
                        case "DELETE":
                            return opm.isDelete();
                        case "EDIT":
                            return opm.isEdit();
                        case "VIEW":
                            return opm.isReadOnly();
                    }
                }
            }
        }
        return true;
    }

    public void quickAddRecord(String moduleId) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?moduleId=" + moduleId);
        }
        catch (IOException ex) {
            Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String openMyDesignerSpace() {
        if (this.userModel != null) {
            HashMap<String, String> userInfoMap = new HashMap<>();
            userInfoMap.put("userEmail", this.email);
            userInfoMap.put("userPassword", this.password);
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            cal.add(Calendar.DATE, 1);
            Date date = cal.getTime();
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String tokenExpiryDate = null;
            tokenExpiryDate = format1.format(date);
            userInfoMap.put("tokenExpiry", tokenExpiryDate);
            String serializedMap = GeneralUtils.serializeRequest(userInfoMap);
            String encryptedMap = Encryptor.encrypt(serializedMap);
            return Defines.GENERAL_CONFIG_MAP.get("designer.url") + "/login.xhtml?token=" + encryptedMap.replace("+", "%2B");
        }
        return "login.xhtml";
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public boolean isAuthenticationFailed() {
        return authenticationFailed;
    }

    public void setAuthenticationFailed(boolean authenticationFailed) {
        this.authenticationFailed = authenticationFailed;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<ApplicationModel> getApplicationList() {
        return applicationList;
    }

    public void setApplicationList(ArrayList<ApplicationModel> applicationList) {
        this.applicationList = applicationList;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public boolean isAdminConsole() {
        return adminConsole;
    }

    public void setAdminConsole(boolean adminConsole) {
        this.adminConsole = adminConsole;
    }

    public CompanyModel getCompanyModel() {
        return companyModel;
    }

    public void setCompanyModel(CompanyModel companyModel) {
        this.companyModel = companyModel;
    }

    public ApplicationModel getActiveApplication() {
        return activeApplication;
    }

    public void setActiveApplication(ApplicationModel activeApplication) {
        this.activeApplication = activeApplication;
    }

    public LocalizationHost getLocalizationHost() {
        return localizationHost;
    }

    public void setLocalizationHost(LocalizationHost localizationHost) {
        this.localizationHost = localizationHost;
    }

    public ArrayList<RecordModel> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<RecordModel> notifications) {
        this.notifications = notifications;
    }

    public boolean isNewNotification() {
        return newNotification;
    }

    public void setNewNotification(boolean newNotification) {
        this.newNotification = newNotification;
    }

    public String getAuthenticationMessage() {
        return authenticationMessage;
    }

    public void setAuthenticationMessage(String authenticationMessage) {
        this.authenticationMessage = authenticationMessage;
    }

    public String getIconStyle() {
        return iconStyle;
    }

    public void setIconStyle(String iconStyle) {
        this.iconStyle = iconStyle;
    }

    public String getAppLookAndFeel() {
        return appLookAndFeel;
    }

    public void setAppLookAndFeel(String appLookAndFeel) {
        this.appLookAndFeel = appLookAndFeel;
    }

    public BeanFramework getActiveView() {
        return activeView;
    }

    public void setActiveView(BeanFramework activeView) {
        this.activeView = activeView;
    }

    public SupportCentralHelper getSupportCentralHelper() {
        return supportCentralHelper;
    }

    public void setSupportCentralHelper(SupportCentralHelper supportCentralHelper) {
        this.supportCentralHelper = supportCentralHelper;
    }

    public String getLoginMasterStyling() {
        return loginMasterStyling;
    }

    public void setLoginMasterStyling(String loginMasterStyling) {
        this.loginMasterStyling = loginMasterStyling;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getForgotPasswordEmail() {
        return forgotPasswordEmail;
    }

    public void setForgotPasswordEmail(String forgotPasswordEmail) {
        this.forgotPasswordEmail = forgotPasswordEmail;
    }

    public String getCompanyTimezone() {
        return companyTimezone;
    }

    public void setCompanyTimezone(String companyTimezone) {
        this.companyTimezone = companyTimezone;
    }

    public boolean isSandboxEnabled() {
        return sandboxEnabled;
    }

    public void setSandboxEnabled(boolean sandboxEnabled) {
        this.sandboxEnabled = sandboxEnabled;
    }

}
