/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.commons.Defines;
import com.beezer.web.utils.GeneralUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author badry
 */
@ManagedBean(name = "calendarBean")
@ViewScoped
public class CalendarBean {
    
    private ScheduleModel eventModel;
    
    @PostConstruct
    public void init() {
        eventModel = new DefaultScheduleModel();
    }

    public void showUserProfile() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("userProfile.xhtml?user="
                    + GeneralUtils.getLoggedUser().getUserId() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT);
        }
        catch (Exception ex) {
            Logger.getLogger(CalendarBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ScheduleModel getEventModel() {
        return eventModel;
    }

    public void setEventModel(ScheduleModel eventModel) {
        this.eventModel = eventModel;
    }
    
    
}
