/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.templates;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.content.ContentLibraryBean;
import com.beezer.web.beans.templates.dataView.CardsViewManager;
import com.beezer.web.beans.templates.dataView.KanbanViewManager;
import com.beezer.web.beans.templates.dataView.ListViewManager;
import com.beezer.web.beans.templates.dataView.ScheduleViewManager;
import com.beezer.web.beans.templates.dataView.SpreadsheetViewManager;
import com.beezer.web.beans.templates.dataView.StructuredViewManager;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.DataViewHandler;
import com.beezer.web.handler.LabelPrinterHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.SortConfiguration;
import com.crm.models.internal.barcode.LabelConfig;
import com.crm.models.internal.barcode.LabelPrinterConfiguration;
import com.crm.models.internal.dataView.DataViewSettings;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.DataViewRequest;
import com.crm.models.requests.managers.LabelPrinterRequest;
import com.crm.models.responses.DataViewResponse;
import com.crm.models.responses.managers.LabelPrinterResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "dataBean")
@ViewScoped
public class DataBean extends BeanFramework implements Serializable {

    private String recordId;
    private DataViewSettings dataViewSettings;
    private boolean loadDefaultView;
    private KanbanViewManager kanbanViewManager;
    private ModuleModel activeModule;

    @ManagedProperty(value = "#{contentLibraryBean}")
    private ContentLibraryBean contentLibraryBean;

    private StructuredViewManager structuredViewManager;
    private ScheduleViewManager scheduleViewManager;
    private SpreadsheetViewManager spreadsheetViewManager;
    private CardsViewManager cardsViewManager;
    private ListViewManager listViewManager;

    private boolean showRecordView;
    private boolean showStructuredView;
    private boolean showBoardView;
    private boolean showScheduleView;
    private boolean showSpreadsheetView;

    private HashMap<String, String> viewOptionList;
    private String activeViewOption;

    private LabelPrinterConfiguration labelPrinterConfiguration;

    private AddDataBean addDataBean;

    public DataBean() {
    }

    @PostConstruct
    public void init() {
        super.BEAN_TYPE = "DATA_BEAN";
        if (super.getModuleId() != null) {
            this.labelPrinterConfiguration = new LabelPrinterConfiguration();
            this.labelPrinterConfiguration.setLabelConfig(new LabelConfig());
            this.loadDefaultView = true;
            super.setModuleId(super.getModuleId());
            this.activeModule = super.loadModule(super.getModuleId());
            super.setModuleBreadCrumbName(this.activeModule.getModuleName());
            this.loadDataViewSettings("DATA");

            if (this.activeModule != null && this.activeModule.isEnableBarcodeSheet()) {
                super.loadFieldSet(super.getModuleId(), null);
            }

            if (!super.relationView) {
//                super.loadFilters(super.getModuleId());
//                super.initializeTableColumnDesigner();

//                this.loadDataViewSettings("DATA");
                this.loadRecordsList();
            }
        }
    }

    public void loadDataViewSettings(String viewType) {
        DataViewRequest dataViewRequest = new DataViewRequest();
        dataViewRequest.setRequestActionType("0");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("module_id", "=", super.getModuleId()));
        dataViewRequest.setClause(fm.getClause());
        dataViewRequest.setRequestingModule(super.getModuleId());
        DataViewHandler dataViewHandler = new DataViewHandler();
        DataViewResponse dataViewResponse = dataViewHandler.executeRequest(dataViewRequest);
        if (dataViewResponse.getErrorCode() == 1000) {
            if (dataViewResponse.getReturnList() != null && !dataViewResponse.getReturnList().isEmpty()) {
                this.dataViewSettings = dataViewResponse.getReturnList().get(0);
                this.loadDefaultView = false;

                switch (viewType) {
                    case "DATA":
                        this.populateViewOptionList();
//                        if (dataViewSettings.getDataViewConfig().isKanbanDefault()) {
//                            this.kanbanViewManager = new KanbanViewManager(dataViewSettings.getDataViewConfig().getKanbanViewSettings(), this);
//                        }
//                        else {
//                            if (this.dataViewSettings.getDataViewConfig().getDataViewType().equals("KANBAN")) {
//                                this.kanbanViewManager = new KanbanViewManager(dataViewSettings.getDataViewConfig().getKanbanViewSettings(), this);
//                            }
//                            else if (this.dataViewSettings.getDataViewConfig().getDataViewType().equals("TABLE")) {
//                                if (this.dataViewSettings.getDataViewConfig().getTabularViewSettings() != null) {
//                                    super.setSettingsList(this.dataViewSettings.getDataViewConfig().getTabularViewSettings().getTableSettings());
//                                }
//                            }
//                        }
                        if (dataViewSettings.getDataViewConfig().isKanbanDefault()) {
                            this.showBoardView = true;
                            this.kanbanViewManager = new KanbanViewManager(dataViewSettings.getDataViewConfig().getKanbanViewSettings(), this);

                            if (this.dataViewSettings.getDataViewConfig().isEnableTabularView()) {
                                if (this.dataViewSettings.getDataViewConfig().getTabularViewSettings() != null) {
                                    super.setSettingsList(this.dataViewSettings.getDataViewConfig().getTabularViewSettings().getTableSettings());
                                }
                            }

                            if (this.dataViewSettings.getDataViewConfig().isEnableSpreadsheetView()) {
                                this.spreadsheetViewManager = new SpreadsheetViewManager(dataViewSettings.getDataViewConfig().getSpreadsheetViewSettings(), this);
                            }

                            if (this.dataViewSettings.getDataViewConfig().isEnableSchedulerView()) {
                                if (this.dataViewSettings.getDataViewConfig().getScheduleViewSettings() != null) {
                                    this.scheduleViewManager = new ScheduleViewManager(this);
                                    this.scheduleViewManager.initialize();
                                }
                            }
                        }
                        else {
                            if (this.dataViewSettings.getDataViewConfig().isEnableKanbanView()) {
                                this.kanbanViewManager = new KanbanViewManager(dataViewSettings.getDataViewConfig().getKanbanViewSettings(), this);
                            }

                            if (this.dataViewSettings.getDataViewConfig().isEnableTabularView()) {
                                if (this.dataViewSettings.getDataViewConfig().getTabularViewSettings() != null) {
                                    super.setSettingsList(this.dataViewSettings.getDataViewConfig().getTabularViewSettings().getTableSettings());
                                }
                            }

                            if (this.dataViewSettings.getDataViewConfig().isEnableSchedulerView()) {
                                if (this.dataViewSettings.getDataViewConfig().getScheduleViewSettings() != null) {
                                    this.scheduleViewManager = new ScheduleViewManager(this);
                                    this.scheduleViewManager.initialize();
                                }
                            }

                            if (this.dataViewSettings.getDataViewConfig().isEnableSpreadsheetView()) {
                                this.spreadsheetViewManager = new SpreadsheetViewManager(dataViewSettings.getDataViewConfig().getSpreadsheetViewSettings(), this);
                            }

                            if (this.dataViewSettings.getDataViewConfig().isEnableCardView()) {
                                this.cardsViewManager = new CardsViewManager(this);
                                this.cardsViewManager.initialize();
                            }

                            if (this.dataViewSettings.getDataViewConfig().isEnableListView()) {
                                this.listViewManager = new ListViewManager(this);
                                this.listViewManager.initialize();
                            }
                        }
                        
                        this.handleDefaultViews();
                        break;
                    case "DETAIL":
                        if (this.dataViewSettings.getDataViewConfig().getTabularViewSettings() != null) {
                            super.setSettingsList(this.dataViewSettings.getDataViewConfig().getTabularViewSettings().getTableSettings());
                        }
                        break;
                    default:
//                        if (dataViewSettings.getDataViewConfig().isKanbanDefault()) {
//                            this.kanbanViewManager = new KanbanViewManager(dataViewSettings.getDataViewConfig().getKanbanViewSettings(), this);
//                        }
//                        else {
//                            if (this.dataViewSettings.getDataViewConfig().getDataViewType().equals("KANBAN")) {
//                                this.kanbanViewManager = new KanbanViewManager(dataViewSettings.getDataViewConfig().getKanbanViewSettings(), this);
//                            }
//                            else if (this.dataViewSettings.getDataViewConfig().getDataViewType().equals("TABLE")) {
//                                if (this.dataViewSettings.getDataViewConfig().getTabularViewSettings() != null) {
//                                    super.setSettingsList(this.dataViewSettings.getDataViewConfig().getTabularViewSettings().getTableSettings());
//                                }
//                            }
//                        }
                        this.populateViewOptionList();
                        if (dataViewSettings.getDataViewConfig().isKanbanDefault()) {
                            this.showBoardView = true;
                            this.kanbanViewManager = new KanbanViewManager(dataViewSettings.getDataViewConfig().getKanbanViewSettings(), this);
                        }
                        else {
                            if (this.dataViewSettings.getDataViewConfig().isEnableKanbanView()) {
                                this.kanbanViewManager = new KanbanViewManager(dataViewSettings.getDataViewConfig().getKanbanViewSettings(), this);

                                if (this.dataViewSettings.getDataViewConfig().isEnableTabularView()) {
                                    if (this.dataViewSettings.getDataViewConfig().getTabularViewSettings() != null) {
                                        super.setSettingsList(this.dataViewSettings.getDataViewConfig().getTabularViewSettings().getTableSettings());
                                    }
                                }
                            }

                            if (this.dataViewSettings.getDataViewConfig().isEnableTabularView()) {
                                if (this.dataViewSettings.getDataViewConfig().getTabularViewSettings() != null) {
                                    super.setSettingsList(this.dataViewSettings.getDataViewConfig().getTabularViewSettings().getTableSettings());
                                }
                            }

                        }
                }

            }
            else {
//                super.initializeTableColumnDesigner();
            }
        }
    }

    private void handleDefaultViews() {
        if (dataViewSettings.getDataViewConfig().isKanbanDefault()) {
            this.activeViewOption = "KANBAN";
        }

        if (dataViewSettings.getDataViewConfig().isCardDefault()) {
            this.activeViewOption = "CARD";
        }

        if (dataViewSettings.getDataViewConfig().isListDefault()) {
            this.activeViewOption = "LIST";
        }

        if (dataViewSettings.getDataViewConfig().isSpreadsheetDefault()) {
            this.activeViewOption = "SPREADSHEET";
        }
    }

    public void populateViewOptionList() {
        this.viewOptionList = new HashMap<>();
        if (this.dataViewSettings != null && this.dataViewSettings.getDataViewConfig() != null) {
            if (this.dataViewSettings.getDataViewConfig().isEnableTabularView()) {
                this.activeViewOption = "TABLE";
                this.viewOptionList.put("Tabular", "TABLE");
            }

            if (this.dataViewSettings.getDataViewConfig().isEnableSchedulerView()) {
                this.viewOptionList.put("Schedule", "SCHEDULE");
            }

            if (this.dataViewSettings.getDataViewConfig().isEnableKanbanView()) {
                if (this.dataViewSettings.getDataViewConfig().isKanbanDefault()) {
                    this.activeViewOption = "KANBAN";
                }
                this.viewOptionList.put("Kanban Board", "KANBAN");
            }

            if (this.dataViewSettings.getDataViewConfig().isStructuredViewEnabled()) {
                this.viewOptionList.put("Structure Tree", "STRUCTURED");
            }

            if (this.dataViewSettings.getDataViewConfig().isEnableSpreadsheetView()) {
                if (this.dataViewSettings.getDataViewConfig().isSpreadsheetViewDefault()) {
                    this.activeViewOption = "SPREADSHEET";
                }
                this.viewOptionList.put("Spreadsheet", "SPREADSHEET");
            }

            if (this.dataViewSettings.getDataViewConfig().isEnableCardView()) {
                this.viewOptionList.put("Cards", "CARD");
            }

            if (this.dataViewSettings.getDataViewConfig().isEnableListView()) {
                this.viewOptionList.put("List", "LIST");
            }

            if (!viewOptionList.isEmpty() && viewOptionList.size() == 1) {
                for (Map.Entry<String, String> viewOption : viewOptionList.entrySet()) {
                    this.activeViewOption = viewOption.getValue();
                }
            }

        }
    }

    public void activateView() {
        if (this.activeViewOption != null) {
            switch (this.activeViewOption) {
                case "TABLE":
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "SCHEDULE":
                    this.scheduleViewManager = new ScheduleViewManager(this);
                    this.scheduleViewManager.initialize();
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "KANBAN":
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "SPREADSHEET":
                    this.spreadsheetViewManager.initializeSpreadsheetView();
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "CARD":
                    this.cardsViewManager.initialize();
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "LIST":
                    this.listViewManager.initialize();
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "STRUCTURED":
                    DataViewRequest dataViewRequest = new DataViewRequest();
                    dataViewRequest.setRequestActionType("4");
                    dataViewRequest.setDataStructureViewConfig(this.dataViewSettings.getDataViewConfig().getDataStructureViewConfig());
                    DataViewHandler dataViewHandler = new DataViewHandler();
                    DataViewResponse dataViewResponse = dataViewHandler.executeRequest(dataViewRequest);
//                    this.showStructuredView = true;
                    if (dataViewResponse.getErrorCode() == 1000) {
                        this.structuredViewManager = new StructuredViewManager(this, dataViewResponse.getStructuredDataModel());
                        this.structuredViewManager.initialize();
                        PrimeFaces.current().ajax().update("dataViewsPanel");
                        PrimeFaces.current().ajax().update("recordViewPanel");
//                        PrimeFaces.current().ajax().update("structuredViewButton");
//                        PrimeFaces.current().ajax().update("recordViewButton");
                    }
                    break;
            }
        }
    }

    public void activateView(String activeViewOptionIn) {
        if (activeViewOptionIn != null) {
            switch (activeViewOptionIn) {
                case "TABLE":
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "SCHEDULE":
                    this.scheduleViewManager = new ScheduleViewManager(this);
                    this.scheduleViewManager.initialize();
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "KANBAN":
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "SPREADSHEET":
                    this.spreadsheetViewManager.initializeSpreadsheetView();
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "CARD":
                    this.cardsViewManager.initialize();
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "LIST":
                    this.listViewManager.initialize();
                    PrimeFaces.current().ajax().update("dataViewsPanel");
                    break;
                case "STRUCTURED":
                    DataViewRequest dataViewRequest = new DataViewRequest();
                    dataViewRequest.setRequestActionType("4");
                    dataViewRequest.setDataStructureViewConfig(this.dataViewSettings.getDataViewConfig().getDataStructureViewConfig());
                    DataViewHandler dataViewHandler = new DataViewHandler();
                    DataViewResponse dataViewResponse = dataViewHandler.executeRequest(dataViewRequest);
//                    this.showStructuredView = true;
                    if (dataViewResponse.getErrorCode() == 1000) {
                        this.structuredViewManager = new StructuredViewManager(this, dataViewResponse.getStructuredDataModel());
                        this.structuredViewManager.initialize();
                        PrimeFaces.current().ajax().update("dataViewsPanel");
                        PrimeFaces.current().ajax().update("recordViewPanel");
//                        PrimeFaces.current().ajax().update("structuredViewButton");
//                        PrimeFaces.current().ajax().update("recordViewButton");
                    }
                    break;
            }
        }
    }

    public void loadStructuredView() {
        if (showStructuredView) {
            DataViewRequest dataViewRequest = new DataViewRequest();
            dataViewRequest.setRequestActionType("4");
            dataViewRequest.setDataStructureViewConfig(this.dataViewSettings.getDataViewConfig().getDataStructureViewConfig());
            DataViewHandler dataViewHandler = new DataViewHandler();
            DataViewResponse dataViewResponse = dataViewHandler.executeRequest(dataViewRequest);
            this.showStructuredView = true;
            if (dataViewResponse.getErrorCode() == 1000) {
                this.structuredViewManager = new StructuredViewManager(this, dataViewResponse.getStructuredDataModel());
                this.structuredViewManager.initialize();
                PrimeFaces.current().ajax().update("dataViewsPanel");
                PrimeFaces.current().ajax().update("recordViewPanel");
                PrimeFaces.current().ajax().update("structuredViewButton");
                PrimeFaces.current().ajax().update("recordViewButton");
            }
        }
        else {
            this.showRecordView();
        }

    }

    public void showRecordView() {
        this.showStructuredView = false;
        PrimeFaces.current().ajax().update("dataViewsPanel");
        PrimeFaces.current().ajax().update("recordViewPanel");
        PrimeFaces.current().ajax().update("structuredViewButton");
        PrimeFaces.current().ajax().update("recordViewButton");
    }

    public void toggleBoardView() {
        PrimeFaces.current().ajax().update("dataViewsPanel");
        PrimeFaces.current().ajax().update("recordViewPanel");
    }

    public void loadRecordsList() {
        if (this.dataViewSettings != null) {
            if (this.dataViewSettings.getDataViewConfig().isEnableTabularView()) {
                if (this.dataViewSettings.getDataViewConfig().getTabularViewSettings() != null && this.dataViewSettings.getDataViewConfig().getTabularViewSettings().isEnableCustomSort()) {
                    ArrayList<SortConfiguration> sortConfigurations = new ArrayList<>();
                    sortConfigurations.add(this.dataViewSettings.getDataViewConfig().getTabularViewSettings().getSortConfiguration());
                    super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", null, false, LazyRecordModel.RECORD, super.getModuleId(), sortConfigurations));
                }
                else {
                    super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", null, false, LazyRecordModel.RECORD, super.getModuleId()));
                }
            }
            else {
                super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", null, false, LazyRecordModel.RECORD, super.getModuleId()));
            }
        }
    }

    public void loadRelatedRecord(String keyName, String keyValue) {
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(keyName, keyValue));
        super.setLazyRecordModel(new LazyRecordModel("GenericMasterAPI", fm.getClause(), false, LazyRecordModel.RECORD, super.getModuleId()));
    }

    public void addNewRecord() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?moduleId=" + super.getModuleId());
        }
        catch (IOException ex) {
            Logger.getLogger(DataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void openBarcodeSheetConfigurator() {
        this.labelPrinterConfiguration.setLabelConfig(new LabelConfig());
        if (this.getFieldsList() != null) {
            for (ModuleFieldModel mfm : this.getFieldsList()) {
                if (mfm.getFieldType().equals("BARCODE")) {
                    this.labelPrinterConfiguration.getLabelConfig().setBarcodeTargetFiled(mfm.getFieldName());
                    this.labelPrinterConfiguration.getLabelConfig().setPaperSize("A4_SHEET");
                    this.labelPrinterConfiguration.getLabelConfig().setSheetSize("24X3");
                    break;
                }
            }
        }
        PrimeFaces.current().ajax().update("addPrinterConfigurationDialog");
        PrimeFaces.current().executeScript("PF('addPrinterConfigurationDialog').show()");
    }

    public StreamedContent generatePrintableBarcodeSheet() {
        if (super.getSelectedRecords() == null || super.getSelectedRecords().isEmpty()) {
            PrimeFaces.current().ajax().update("emptySelectionBarcodeGenerationMessage");
            PrimeFaces.current().executeScript("PF('emptySelectionBarcodeGenerationMessage').show()");
            return null;
        }

        LabelPrinterRequest labelPrinterRequest = new LabelPrinterRequest();
        labelPrinterRequest.setLabelPrinterConfiguration(labelPrinterConfiguration);
        labelPrinterRequest.setBulkRecords(new ArrayList<>(super.getSelectedRecords()));
        labelPrinterRequest.setRequestActionType("4");
        LabelPrinterHandler handler = new LabelPrinterHandler();
        LabelPrinterResponse labelPrinterResponse = handler.executeRequest(labelPrinterRequest);
        if (labelPrinterResponse.getErrorCode() == 1000 && labelPrinterResponse.getPrintableSheet() != null) {
            return DefaultStreamedContent.builder()
                    .name(labelPrinterResponse.getPrintableSheet().getFileName() + "." + labelPrinterResponse.getPrintableSheet().getFileExtenstion())
                    .contentType("application/pdf")
                    .stream(() -> new ByteArrayInputStream(labelPrinterResponse.getPrintableSheet().getContentStream()))
                    .build();
        }
        return null;
    }

    public void goToQuickEdit(RecordModel recordModel) {
        if (recordModel != null) {
            ModuleFieldModel recordKeyField = GeneralUtils.getKeyFieldFromRecord(recordModel);

            this.addDataBean = new AddDataBean();
            this.addDataBean.setQuickEdit(true);
            this.addDataBean.setRequestParams(new HashMap<>());
            this.addDataBean.getRequestParams().put(Defines.REQUEST_MODE_KEY, Defines.REQUEST_MODE_EDIT);
            this.addDataBean.getRequestParams().put("moduleId", super.getModuleId());
            this.addDataBean.getRequestParams().put("recordId", recordKeyField.getCurrentValue());
            this.addDataBean.setModuleId(super.getModuleId());
            this.addDataBean.init();

            PrimeFaces.current().ajax().update("quickEditFormHolder");
            PrimeFaces.current().executeScript("PF('quickEditRecordDialog').show()");

//            if (recordKeyField != null) {
//                try {
//                    FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?recordId="
//                            + recordKeyField.getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
//                            + Defines.REQUEST_MODE_EDIT + "&moduleId=" + super.getModuleId());
//                }
//                catch (Exception ex) {
//                    Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
        }
    }

    public void goToQuickEditForRecordManager(RecordModel recordModel) {
        if (recordModel != null) {
            ModuleFieldModel recordKeyField = GeneralUtils.getKeyFieldFromRecord(recordModel);

            if (recordKeyField != null) {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?recordId="
                            + recordKeyField.getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                            + Defines.REQUEST_MODE_EDIT + "&moduleId=" + super.getModuleId()
                            + "&masterRec=" + super.getOriginatingDetailBean().getRequestedId()
                            + "&masterModule=" + super.getOriginatingDetailBean().getModuleId());
                }
                catch (Exception ex) {
                    Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public void saveQuickEdit() {
        this.addDataBean.save();
        this.loadRecordsList();
        PrimeFaces.current().ajax().update("recordViewPanel");
        PrimeFaces.current().executeScript("PF('quickEditRecordDialog').hide()");
    }

    public void openAdvancedFilter() {
        super.loadFilters(super.getModuleId());
        PrimeFaces.current().ajax().update("advancedSearchFiltersDialog");
        PrimeFaces.current().executeScript("PF('advancedSearchFiltersDialog').show()");
    }

    @Override
    public void search(String api) {
        switch (this.activeViewOption) {
            case "KANBAN":
                this.kanbanViewManager.search("GenericMasterAPI");
                break;
            case "CARD":
                this.cardsViewManager.search("GenericMasterAPI");
                break;
            case "LIST":
                this.listViewManager.search("GenericMasterAPI");
                break;
            default:
                super.search("GenericMasterAPI");
                break;
        }
    }

    @Override
    public void delete() {
        RequestModel request = new RequestModel();
        request.setBulkRecords(new ArrayList<>(super.getSelectedRecords()));
        request.setRequestActionType("2");
        request.setRequestingModule(super.getModuleId());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            switch (response.getErrorCode()) {
                case 1000:
                    super.getSelectedRecords().clear();
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record deleted Susscefully. "));
                    break;
                case 1002:
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Success", "Record deleted Susscefully. " + response.getErrorMessage()));
                    break;
                default:
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete record due to: " + response.getErrorMessage()));
                    break;
            }
        }
        catch (Exception ex) {
            Logger.getLogger(DataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", super.getSelectedRecords().get(0));
            RecordModel selectedModel = (RecordModel) event.getObject();
            for (Map.Entry<String, ModuleFieldModel> entry : selectedModel.getFieldValueMap().entrySet()) {
                if (entry.getValue().isIsKey()) {
                    this.recordId = entry.getValue().getCurrentValue();
                    break;
                }
            }
            if (super.getModuleId().equals("7")) {
//                FacesContext.getCurrentInstance().getExternalContext().redirect("dealDetails.xhtml?recordId="
//                        + this.recordId);
                FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                        + this.recordId + "&" + "moduleId=" + super.getModuleId());
            }
            else {
                if (super.getOriginatingDetailBean() != null) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                            + this.recordId + "&" + "moduleId=" + super.getModuleId()
                            + "&masterRec=" + super.getOriginatingDetailBean().getRequestedId()
                            + "&masterModule=" + super.getOriginatingDetailBean().getModuleId());
                }
                else {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                            + this.recordId + "&" + "moduleId=" + super.getModuleId());
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(DataBean.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
            }
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(DataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public DataViewSettings getDataViewSettings() {
        return dataViewSettings;
    }

    public void setDataViewSettings(DataViewSettings dataViewSettings) {
        this.dataViewSettings = dataViewSettings;
    }

    public boolean isLoadDefaultView() {
        return loadDefaultView;
    }

    public void setLoadDefaultView(boolean loadDefaultView) {
        this.loadDefaultView = loadDefaultView;
    }

    public KanbanViewManager getKanbanViewManager() {
        return kanbanViewManager;
    }

    public void setKanbanViewManager(KanbanViewManager kanbanViewManager) {
        this.kanbanViewManager = kanbanViewManager;
    }

    public ContentLibraryBean getContentLibraryBean() {
        return contentLibraryBean;
    }

    public void setContentLibraryBean(ContentLibraryBean contentLibraryBean) {
        this.contentLibraryBean = contentLibraryBean;
    }

    public StructuredViewManager getStructuredViewManager() {
        return structuredViewManager;
    }

    public void setStructuredViewManager(StructuredViewManager structuredViewManager) {
        this.structuredViewManager = structuredViewManager;
    }

    public boolean isShowStructuredView() {
        return showStructuredView;
    }

    public void setShowStructuredView(boolean showStructuredView) {
        this.showStructuredView = showStructuredView;
    }

    public boolean isShowBoardView() {
        return showBoardView;
    }

    public void setShowBoardView(boolean showBoardView) {
        this.showBoardView = showBoardView;
    }

    public boolean isShowScheduleView() {
        return showScheduleView;
    }

    public void setShowScheduleView(boolean showScheduleView) {
        this.showScheduleView = showScheduleView;
    }

    public boolean isShowRecordView() {
        return showRecordView;
    }

    public void setShowRecordView(boolean showRecordView) {
        this.showRecordView = showRecordView;
    }

    public HashMap<String, String> getViewOptionList() {
        return viewOptionList;
    }

    public void setViewOptionList(HashMap<String, String> viewOptionList) {
        this.viewOptionList = viewOptionList;
    }

    public String getActiveViewOption() {
        return activeViewOption;
    }

    public void setActiveViewOption(String activeViewOption) {
        this.activeViewOption = activeViewOption;
    }

    public ScheduleViewManager getScheduleViewManager() {
        return scheduleViewManager;
    }

    public void setScheduleViewManager(ScheduleViewManager scheduleViewManager) {
        this.scheduleViewManager = scheduleViewManager;
    }

    public SpreadsheetViewManager getSpreadsheetViewManager() {
        return spreadsheetViewManager;
    }

    public void setSpreadsheetViewManager(SpreadsheetViewManager spreadsheetViewManager) {
        this.spreadsheetViewManager = spreadsheetViewManager;
    }

    public boolean isShowSpreadsheetView() {
        return showSpreadsheetView;
    }

    public void setShowSpreadsheetView(boolean showSpreadsheetView) {
        this.showSpreadsheetView = showSpreadsheetView;
    }

    public LabelPrinterConfiguration getLabelPrinterConfiguration() {
        return labelPrinterConfiguration;
    }

    public void setLabelPrinterConfiguration(LabelPrinterConfiguration labelPrinterConfiguration) {
        this.labelPrinterConfiguration = labelPrinterConfiguration;
    }

    public ModuleModel getActiveModule() {
        return activeModule;
    }

    public void setActiveModule(ModuleModel activeModule) {
        this.activeModule = activeModule;
    }

    public AddDataBean getAddDataBean() {
        return addDataBean;
    }

    public void setAddDataBean(AddDataBean addDataBean) {
        this.addDataBean = addDataBean;
    }

    public CardsViewManager getCardsViewManager() {
        return cardsViewManager;
    }

    public void setCardsViewManager(CardsViewManager cardsViewManager) {
        this.cardsViewManager = cardsViewManager;
    }

    public ListViewManager getListViewManager() {
        return listViewManager;
    }

    public void setListViewManager(ListViewManager listViewManager) {
        this.listViewManager = listViewManager;
    }

}
