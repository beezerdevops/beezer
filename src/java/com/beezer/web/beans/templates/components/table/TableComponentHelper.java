/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.templates.components.table;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import java.util.ArrayList;

/**
 *
 * @author Ahmed El Badry Jan 29, 2022
 */
public class TableComponentHelper {

    private final BeanFramework addDataBean;
    private ArrayList<String> pivotHeaderValues;
    private ArrayList<String> pivotRowValues;
    private ArrayList<RecordModel> pivotReportData;
    private FieldsLayoutModel targetFieldLayoutModel;
    private ArrayList<ModuleFieldModel> customValueFields;

    public TableComponentHelper(BeanFramework addDataBean, FieldsLayoutModel targetFieldLayoutModel) {
        this.addDataBean = addDataBean;
        this.targetFieldLayoutModel = targetFieldLayoutModel;
        this.pivotReportData = new ArrayList<>();
        this.pivotHeaderValues = new ArrayList<>();
        this.pivotRowValues = new ArrayList<>();
    }

    public void initializePivotData(ArrayList<RecordModel> originalRecords) {
        if (this.targetFieldLayoutModel.getTableComponentConfiguration().isPivotViewActive() && this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration() != null) {
            if (this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getHeaderColumnFieldName() != null
                    && !this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getHeaderColumnFieldName().isEmpty()) {
                this.customValueFields = new ArrayList<>();

                for (RecordModel rm : originalRecords) {
                    ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getHeaderColumnFieldName());
                    if (mfm != null) {
                        if (!this.pivotHeaderValues.contains(mfm.getCurrentValue())) {
                            this.pivotHeaderValues.add(mfm.getCurrentValue());
                            String valueFieldName = this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getValueColumn();
                            ModuleFieldModel customValueField = new ModuleFieldModel();
                            customValueField.setFieldName(valueFieldName + "#@#" + mfm.getCurrentValue());
                            customValueField.setFieldType("TEXT");
                            customValueFields.add(customValueField);
                        }
                    }
                }

                for (RecordModel rm : originalRecords) {
                    ModuleFieldModel mfmRow = GeneralUtils.getFieldFromRecord(rm, this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getRowConfiguration());
                    if (mfmRow != null) {
                        if (!this.pivotRowValues.contains(mfmRow.getCurrentValue())) {
                            this.pivotRowValues.add(mfmRow.getCurrentValue());
                        }
                    }

                    if (mfmRow != null) {
                        if (!pivotReportData.isEmpty()) {
                            if (!this.isRecordExist(pivotReportData, this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getRowConfiguration(), mfmRow.getCurrentValue())) {
                                ArrayList<ModuleFieldModel> targetCustomValueFields = this.fillCustomFieldsList(this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getRowConfiguration(), mfmRow.getCurrentValue(), originalRecords, customValueFields);
                                if (targetCustomValueFields != null) {
                                    for (ModuleFieldModel mfmCustom : targetCustomValueFields) {
                                        rm.getFieldValueMap().put(mfmCustom.getFieldName(), mfmCustom);
                                    }
                                }
                                pivotReportData.add(rm);
                            }
                        }
                        else {
                            ArrayList<ModuleFieldModel> targetCustomValueFields = this.fillCustomFieldsList(this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getRowConfiguration(), mfmRow.getCurrentValue(), originalRecords, customValueFields);
                            if (targetCustomValueFields != null) {
                                for (ModuleFieldModel mfmCustom : targetCustomValueFields) {
                                    rm.getFieldValueMap().put(mfmCustom.getFieldName(), mfmCustom);
                                }
                            }
                            pivotReportData.add(rm);
                        }
                    }
                }
            }
        }
    }

    private boolean isRecordExist(ArrayList<RecordModel> recordList, String targetField, String targetValue) {
        for (RecordModel rm : recordList) {
            ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, targetField);
            if (mfm != null) {
                if (mfm.getCurrentValue() != null && mfm.getCurrentValue().equals(targetValue)) {
                    return true;
                }
            }
        }
        return false;
    }

    private RecordModel captureRecordOnValue(ArrayList<RecordModel> recordList, String targetField, String targetValue) {
        for (RecordModel rm : recordList) {
            ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(rm, targetField);
            if (mfm != null) {
                if (mfm.getCurrentValue() != null && mfm.getCurrentValue().equals(targetValue)) {
                    return rm;
                }
            }
        }
        return null;
    }

    private ArrayList<ModuleFieldModel> fillCustomFieldsList(String targetRecordField, String targetRecordValue, ArrayList<RecordModel> originalRecords, ArrayList<ModuleFieldModel> customFields) {
        if (originalRecords != null && !originalRecords.isEmpty() && customFields != null && !customFields.isEmpty()) {
            ArrayList<ModuleFieldModel> filledFields = new ArrayList<>();
            ArrayList<RecordModel> targetRecords = new ArrayList<>();
            for (RecordModel targetRecord : originalRecords) {
                ModuleFieldModel targetField = new ModuleFieldModel(GeneralUtils.getFieldFromRecord(targetRecord, targetRecordField));
                if (targetField != null) {
                    if (targetField.getCurrentValue() != null && !targetField.getCurrentValue().isEmpty() && targetField.getCurrentValue().equals(targetRecordValue)) {
                        targetRecords.add(targetRecord);
                    }
                }
            }

            for (ModuleFieldModel customMfm : customFields) {
                String[] parts = customMfm.getFieldName().split("#@#");
                String valueFieldName = parts[0];
                String targetFieldCurrentValue = parts[1];
                RecordModel rm = this.captureRecordOnValue(targetRecords, this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getHeaderColumnFieldName(), targetFieldCurrentValue);
                if (rm != null) {
                    ModuleFieldModel valueField = GeneralUtils.getFieldFromRecord(rm, valueFieldName);
                    if (valueField != null) {
                        ModuleFieldModel finalMfm = new ModuleFieldModel(customMfm);
                        finalMfm.setCurrentValue(valueField.getCurrentValue());
                        filledFields.add(finalMfm);
                    }
                }
            }
            return filledFields;
        }
        else {
            return customFields;
        }
    }

    public ArrayList<RecordModel> buildRecordList(ArrayList<RecordModel> originalRecords) {
        if (originalRecords != null && !originalRecords.isEmpty()) {
            ArrayList<RecordModel> consalidatedRecordList = new ArrayList<>();
            
            for (RecordModel pivotRecord : this.pivotReportData) {
                ArrayList<RecordModel> targetRecords = new ArrayList<>();
                ModuleFieldModel targetPivotField = GeneralUtils.getFieldFromRecord(pivotRecord, this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getRowConfiguration());
                for (RecordModel targetRecord : originalRecords) {
                    ModuleFieldModel targetField = new ModuleFieldModel(GeneralUtils.getFieldFromRecord(targetRecord, this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getRowConfiguration()));
                    if (targetField.getCurrentValue() != null && !targetField.getCurrentValue().isEmpty() && targetField.getCurrentValue().equals(targetPivotField.getCurrentValue())) {
                        targetRecords.add(targetRecord);
                    }
                }

                for (ModuleFieldModel customMfm : this.customValueFields) {
                    String[] parts = customMfm.getFieldName().split("#@#");
                    String valueFieldName = parts[0];
                    String targetFieldCurrentValue = parts[1];
                    for (RecordModel rm : targetRecords) {
                        ModuleFieldModel mfm = new ModuleFieldModel(GeneralUtils.getFieldFromRecord(rm, this.targetFieldLayoutModel.getTableComponentConfiguration().getPivotConfiguration().getHeaderColumnFieldName()));
                        if (mfm.getCurrentValue() != null && mfm.getCurrentValue().equals(targetFieldCurrentValue)) {
                            rm.getFieldValueMap().get(valueFieldName).setCurrentValue(pivotRecord.getFieldValueMap().get(customMfm.getFieldName()).getCurrentValue());
                        }
                    }
                }
                consalidatedRecordList.addAll(targetRecords);
            }
            return consalidatedRecordList;
        }
        else {
            return new ArrayList<>();
        }
    }

    public ArrayList<String> getPivotHeaderValues() {
        return pivotHeaderValues;
    }

    public void setPivotHeaderValues(ArrayList<String> pivotHeaderValues) {
        this.pivotHeaderValues = pivotHeaderValues;
    }

    public ArrayList<String> getPivotRowValues() {
        return pivotRowValues;
    }

    public void setPivotRowValues(ArrayList<String> pivotRowValues) {
        this.pivotRowValues = pivotRowValues;
    }

    public ArrayList<RecordModel> getPivotReportData() {
        return pivotReportData;
    }

    public void setPivotReportData(ArrayList<RecordModel> pivotReportData) {
        this.pivotReportData = pivotReportData;
    }

    public FieldsLayoutModel getTargetFieldLayoutModel() {
        return targetFieldLayoutModel;
    }

    public void setTargetFieldLayoutModel(FieldsLayoutModel targetFieldLayoutModel) {
        this.targetFieldLayoutModel = targetFieldLayoutModel;
    }

}
