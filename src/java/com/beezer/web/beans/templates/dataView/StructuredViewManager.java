/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.templates.dataView;

import com.beezer.web.beans.templates.DataBean;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.dataView.StructuredDataModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.primefaces.event.organigram.OrganigramNodeSelectEvent;
import org.primefaces.model.DefaultOrganigramNode;
import org.primefaces.model.OrganigramNode;

/**
 *
 * @author Ahmed El Badry Dec 2, 2020
 */
public class StructuredViewManager {

    private DataBean dataBean;
    private StructuredDataModel structuredDataModel;

    private OrganigramNode rootNode;
    private OrganigramNode selection;
    private boolean zoom = false;
    private String style = "width: 800px";
    private int leafNodeConnectorHeight = 10;
    private boolean autoScrollToSelection = false;

    private String viewField;
    private ArrayList<String> viewFieldList;

    public StructuredViewManager(DataBean dataBean, StructuredDataModel structuredDataModel) {
        this.dataBean = dataBean;
        this.structuredDataModel = structuredDataModel;
    }

    public void initialize() {
        viewField = this.dataBean.getDataViewSettings().getDataViewConfig().getDataStructureViewConfig().getViewField();
        if (this.dataBean.getDataViewSettings().getDataViewConfig().getDataStructureViewConfig().getViewFieldList() != null) {
            this.viewFieldList = new ArrayList<>();
            for (String s : this.dataBean.getDataViewSettings().getDataViewConfig().getDataStructureViewConfig().getViewFieldList()) {
                this.viewFieldList.add(s);
            }
        }
        rootNode = new DefaultOrganigramNode("root", structuredDataModel, null);
        rootNode.setCollapsible(false);
        rootNode.setDroppable(true);
        rootNode.setSelectable(true);
        buildOrgChartDiagram(structuredDataModel, rootNode);
    }

    public void buildOrgChartDiagram(StructuredDataModel data, OrganigramNode parent) {
        if (data.getChildren() != null) {
            for (StructuredDataModel SDM : data.getChildren()) {
                OrganigramNode divisionNode = new DefaultOrganigramNode("division", SDM, parent);
                divisionNode.setDroppable(true);
                divisionNode.setDraggable(true);
                divisionNode.setSelectable(true);
                buildOrgChartDiagram(SDM, divisionNode);
            }
        }
    }

    public void nodeSelectListener(OrganigramNodeSelectEvent event) {
        StructuredDataModel sdm = (StructuredDataModel) ((OrganigramNode) event.getOrganigramNode()).getData();
        RecordModel rm = sdm.getData();
        ModuleFieldModel keyField = GeneralUtils.getKeyFieldFromRecord(rm);
        if (keyField != null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                        + keyField.getCurrentValue() + "&" + "moduleId=" + this.dataBean.getModuleId());
            }
            catch (IOException ex) {
                Logger.getLogger(StructuredViewManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public DataBean getDataBean() {
        return dataBean;
    }

    public void setDataBean(DataBean dataBean) {
        this.dataBean = dataBean;
    }

    public StructuredDataModel getStructuredDataModel() {
        return structuredDataModel;
    }

    public void setStructuredDataModel(StructuredDataModel structuredDataModel) {
        this.structuredDataModel = structuredDataModel;
    }

    public OrganigramNode getRootNode() {
        return rootNode;
    }

    public void setRootNode(OrganigramNode rootNode) {
        this.rootNode = rootNode;
    }

    public OrganigramNode getSelection() {
        return selection;
    }

    public void setSelection(OrganigramNode selection) {
        this.selection = selection;
    }

    public boolean isZoom() {
        return zoom;
    }

    public void setZoom(boolean zoom) {
        this.zoom = zoom;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public int getLeafNodeConnectorHeight() {
        return leafNodeConnectorHeight;
    }

    public void setLeafNodeConnectorHeight(int leafNodeConnectorHeight) {
        this.leafNodeConnectorHeight = leafNodeConnectorHeight;
    }

    public boolean isAutoScrollToSelection() {
        return autoScrollToSelection;
    }

    public void setAutoScrollToSelection(boolean autoScrollToSelection) {
        this.autoScrollToSelection = autoScrollToSelection;
    }

    public String getViewField() {
        return viewField;
    }

    public void setViewField(String viewField) {
        this.viewField = viewField;
    }

    public ArrayList<String> getViewFieldList() {
        return viewFieldList;
    }

    public void setViewFieldList(ArrayList<String> viewFieldList) {
        this.viewFieldList = viewFieldList;
    }

}
