/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.templates.dataView;

import com.beezer.web.beans.templates.DataBean;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.beezl.interpreter.BEEZLInterperter;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author Ahmed El Badry Jan 22, 2021
 */
public class ScheduleViewManager {

    private final DataBean dataBean;
    private ScheduleModel lazyEventModel;
    private ScheduleEvent<?> event = new DefaultScheduleEvent<>();
    private String styleClass = "";

    public ScheduleViewManager(DataBean dataBean) {
        this.dataBean = dataBean;
    }

    public void initialize() {
        lazyEventModel = new LazyScheduleModel() {

            @Override
            public void loadEvents(LocalDateTime start, LocalDateTime end) {
                PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
//                if (start.getMonth().getValue() != end.getMonth().getValue()) {
//                    end = end.minusMonths(1);
//                }
                long startDate = java.util.Date.from(start.atZone(ZoneId.systemDefault()).toInstant()).getTime();
                long endDate = java.util.Date.from(end.atZone(ZoneId.systemDefault()).toInstant()).getTime();
                GeneralUtils.getDateNoTimeFromMillisDBFormat(String.valueOf(startDate));
                if (dataBean.getDataViewSettings().getDataViewConfig().getScheduleViewSettings() != null) {
                    ModuleModel mm = dataBean.loadModule(dataBean.getModuleId());
                    String startDateIdentifier = dataBean.getDataViewSettings().getDataViewConfig().getScheduleViewSettings().getStartDateIdentifierField();
                    String endDateIdentifier = dataBean.getDataViewSettings().getDataViewConfig().getScheduleViewSettings().getEndDateIdentifierField();
                    String titleIdentifier = dataBean.getDataViewSettings().getDataViewConfig().getScheduleViewSettings().getTitleIdentifierField();
                    FilterManager fm = new FilterManager();
                    fm.setFilter("AND", new FilterType(mm.getModuleBaseTable() + "." + startDateIdentifier, "BETWEEN",
                            GeneralUtils.getDateNoTimeFromMillisDBFormat(String.valueOf(startDate)),
                            GeneralUtils.getDateNoTimeFromMillisDBFormat(String.valueOf(endDate))));
                    RequestModel request = new RequestModel();
                    request.setClause(fm.getClause());
                    request.setRequestingModule(dataBean.getModuleId());
                    request.setLimit(999);
                    request.setRequestActionType("0");
                    request.setSkipContent(true);
                    request.setRequestContext("DATAVIEW_CALENDAR");
                    RequestHandler requestHandler = new RequestHandler();
                    ResponseModel response = requestHandler.executeRequest(request);
                    if (response.getErrorCode() == 1000) {
                        if (response.getRecordList() != null && !response.getRecordList().isEmpty()) {
                            for (RecordModel rm : response.getRecordList()) {
                                String styleClassName = "";
                                if (dataBean.getDataViewSettings().getDataViewConfig().getScheduleViewSettings().getStyling() != null
                                        && !dataBean.getDataViewSettings().getDataViewConfig().getScheduleViewSettings().getStyling().isEmpty()) {
                                    styleClassName = dataBean.evaluateStylingRules(dataBean.getDataViewSettings().getDataViewConfig().getScheduleViewSettings().getStyling(), rm);
                                }

                                String titleValue = "";
                                if (GeneralUtils.getFieldFromRecord(rm, titleIdentifier).getFieldType().equals("SMARTFIELD")) {
                                    BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                                    beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                                    beezlInterperter.setRecodeModel(rm);
                                    LinkedHashMap<String, String> bvm;
                                    beezlInterperter.addDynamicField("fieldScript", GeneralUtils.getFieldFromRecord(rm, titleIdentifier).getBeezlScript());
                                    try {
                                        bvm = beezlInterperter.evaluate();
                                        titleValue = bvm.get("fieldScript");
                                    }
                                    catch (Exception ex) {
                                        Logger.getLogger(KanbanViewManager.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }
                                else {
                                    titleValue = GeneralUtils.getFieldFromRecord(rm, titleIdentifier).getCurrentValue();
                                }

                                event = DefaultScheduleEvent.builder()
                                        .title(titleValue)
                                        .startDate(GeneralUtils.getLocalDateTimeFromDateString(GeneralUtils.getFieldFromRecord(rm, startDateIdentifier).getCurrentValue()))
                                        .endDate(GeneralUtils.getLocalDateTimeFromDateString(GeneralUtils.getFieldFromRecord(rm, endDateIdentifier).getCurrentValue()))
                                        .data(rm).backgroundColor(styleClassName).borderColor(styleClassName)
                                        .build();
                                lazyEventModel.addEvent(event);
                            }
                        }
                    }
                    PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
                }
            }
        };
    }

    public void onEventSelect(SelectEvent<ScheduleEvent<?>> selectEvent) {
        ScheduleEvent<?> selectedEvent = selectEvent.getObject();
        RecordModel rm = (RecordModel) selectedEvent.getData();
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                    + GeneralUtils.getKeyFieldFromRecord(rm).getCurrentValue() + "&" + "moduleId=" + dataBean.getModuleId());
        }
        catch (IOException ex) {
            Logger.getLogger(ScheduleViewManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onViewChange(SelectEvent<String> selectEvent) {
        int x = 0;
    }

    public ScheduleModel getLazyEventModel() {
        return lazyEventModel;
    }

    public void setLazyEventModel(ScheduleModel lazyEventModel) {
        this.lazyEventModel = lazyEventModel;
    }

    public ScheduleEvent<?> getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent<?> event) {
        this.event = event;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

}
