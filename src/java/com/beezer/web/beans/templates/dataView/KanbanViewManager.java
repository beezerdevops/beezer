/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.templates.dataView;

import com.beezer.web.beans.DashboardV2Bean;
import com.beezer.web.beans.templates.DataBean;
import com.beezer.web.commons.Defines;
import com.beezer.web.comparators.KanbanCardComparator;
import com.beezer.web.handler.LookupHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.KanbanRecordModel;
import com.beezer.web.models.LazyKanbanModel;
import com.beezer.web.utils.GeneralUtils;
import com.beezl.interpreter.BEEZLInterperter;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.SortConfiguration;
import com.crm.models.internal.dataView.BoardSettings;
import com.crm.models.internal.dataView.KanbanCard;
import com.crm.models.internal.dataView.KanbanViewSettings;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.lookup.LookupModel;
import com.crm.models.internal.workflow.activities.DocTemplateParameter;
import com.crm.models.requests.managers.LookupRequest;
import com.crm.models.responses.managers.LookupResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.extensions.event.ClipboardSuccessEvent;

/**
 *
 * @author Ahmed El Badry Sep 20, 2020
 */
public class KanbanViewManager {

    private ArrayList<KanbanRecordModel> recordsGroupsList;
    private KanbanViewSettings kanbanViewSettings;
    private ModuleFieldModel boardField;
    private final DataBean dataBean;
    private String keyFieldName;

    private String customLinkStyling;
    private String customCssStyling;
    private String evaluatedCustomCard;

//    private BEEZLInterperter beezlInterperter;
    public KanbanViewManager(KanbanViewSettings kanbanViewSettings, DataBean dataBean) {
        this.kanbanViewSettings = kanbanViewSettings;
        this.dataBean = dataBean;
        this.keyFieldName = this.dataBean.getKeyField();

//        BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
//        beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
        this.initializeGroupData();
//        this.loadRecordsListForKanban();
        this.loadLazyRecordsListForKanban();
    }

    public void loadRecordsListForKanban() {
        try {
            RequestModel request = new RequestModel();
            request.setRequestingModule(this.dataBean.getModuleId());
            request.setLimit(100);
            request.setOffset(0);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            if (kanbanViewSettings.isEnableCustomSort()) {
                ArrayList<SortConfiguration> sortConfigurations = new ArrayList<>();
                sortConfigurations.add(this.kanbanViewSettings.getSortConfiguration());
                request.setSortConfigurations(sortConfigurations);
            }
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
                this.groupData(response.getRecordList());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(KanbanViewManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadLazyRecordsListForKanban() {
        for (KanbanRecordModel krm : recordsGroupsList) {
            krm.setLazyRecordModel(new LazyKanbanModel("GenericMasterAPI", null, false, LazyKanbanModel.RECORD, this.dataBean.getModuleId(), kanbanViewSettings, dataBean, krm.getTagId()));
        }
    }

    public void groupData(ArrayList<RecordModel> recordModels) {
        if (recordModels != null) {
            Collections.sort(kanbanViewSettings.getCardSettings(), new KanbanCardComparator());
            this.recordsGroupsList = new ArrayList<>();
            ArrayList<LookupModel> lookupList = null;
            if (this.dataBean.getFieldsList() != null && !this.dataBean.getFieldsList().isEmpty()) {
                String lookupTableId = null;
                for (ModuleFieldModel mfm : this.dataBean.getFieldsList()) {
                    if (kanbanViewSettings.getFieldId() == mfm.getFieldId()) {
                        lookupTableId = mfm.getFieldValues();
                        boardField = mfm;
                        break;
                    }
                }

                LookupHandler lh = new LookupHandler();
                LookupRequest request = new LookupRequest();
                request.setLookupId(lookupTableId);
                request.setActionType("0");
                LookupResponse response = lh.getLookups(request);
                if (response.getErrorCode() == 1000) {
                    lookupList = response.getReturnList();
                }

                if (lookupList != null) {
                    KanbanRecordModel krm;
                    for (BoardSettings lm : kanbanViewSettings.getBoardSettings()) {
                        krm = new KanbanRecordModel();
                        krm.setGroupTagName(lm.getLookupLabel());
                        krm.setTagId(String.valueOf(lm.getLookupFieldId()));
                        ArrayList<RecordModel> groupData = new ArrayList<>();
                        for (RecordModel rm : recordModels) {
                            if (rm.getFieldValueMap().get(boardField.getFieldName()).getCurrentValue().equals(String.valueOf(lm.getLookupFieldId()))) {
                                groupData.add(rm);
                            }
                        }
                        krm.setGroupData(groupData);
                        this.recordsGroupsList.add(krm);
                    }
                }
            }
        }
    }

    public void initializeGroupData() {
        Collections.sort(kanbanViewSettings.getCardSettings(), new KanbanCardComparator());
        this.recordsGroupsList = new ArrayList<>();
        ArrayList<LookupModel> lookupList = null;
        if (this.dataBean.getFieldsList() != null && !this.dataBean.getFieldsList().isEmpty()) {
            String lookupTableId = null;
            for (ModuleFieldModel mfm : this.dataBean.getFieldsList()) {
                if (kanbanViewSettings.getFieldId() == mfm.getFieldId()) {
                    lookupTableId = mfm.getFieldValues();
                    boardField = mfm;
                    break;
                }
            }

            LookupHandler lh = new LookupHandler();
            LookupRequest request = new LookupRequest();
            request.setLookupId(lookupTableId);
            request.setActionType("0");
            LookupResponse response = lh.getLookups(request);
            if (response.getErrorCode() == 1000) {
                lookupList = response.getReturnList();
            }

            if (lookupList != null) {
                KanbanRecordModel krm;
                for (BoardSettings lm : kanbanViewSettings.getBoardSettings()) {
                    krm = new KanbanRecordModel();
                    krm.setGroupTagName(lm.getLookupLabel());
                    krm.setTagId(String.valueOf(lm.getLookupFieldId()));
                    ArrayList<RecordModel> groupData = new ArrayList<>();

                    krm.setGroupData(groupData);
                    this.recordsGroupsList.add(krm);
                }
            }
        }
    }

    public void onCardMove() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String orderDraggedId = params.get("orderId");
        String sourceListId = params.get("sourceListId");
        String targetListId = params.get("targetListId");
        if (sourceListId.equals(targetListId)) {
            for (KanbanRecordModel krm : this.recordsGroupsList) {
                krm.getLazyRecordModel().setCardMoved(true);
            }
            return;
        }
        RecordModel modifiedRM = null;
        for (KanbanRecordModel krm : this.recordsGroupsList) {
            krm.getLazyRecordModel().setCardMoved(true);
            if (krm.getTagId().equals(sourceListId)) {
                for (RecordModel rm : krm.getLazyRecordModel().getData()) {
                    if (rm.getFieldValueMap().get(this.keyFieldName).getCurrentValue().equals(orderDraggedId)) {
                        modifiedRM = rm;
                        break;
                    }
                }
                if (modifiedRM != null) {
//                    krm.getGroupData().remove(modifiedRM);
                    krm.getLazyRecordModel().getData().remove(modifiedRM);
                    krm.getLazyRecordModel().setRowCount(krm.getLazyRecordModel().getRowCount() - 1);
                }
            }
        }
        if (modifiedRM != null) {
            for (KanbanRecordModel krm : this.recordsGroupsList) {
                krm.getLazyRecordModel().setCardMoved(true);
                if (krm.getTagId().equals(targetListId)) {
                    if (krm.getLazyRecordModel() != null) {
//                        krm.getGroupData().add(modifiedRM);
                        krm.getLazyRecordModel().getData().add(0, modifiedRM);
                        krm.getLazyRecordModel().setRowCount(krm.getLazyRecordModel().getRowCount() + 1);
                        modifiedRM.getFieldValueMap().get(boardField.getFieldName()).setCurrentValue(targetListId);
                        break;
                    }
                    else {
                        krm.setGroupData(new ArrayList<>());
                        krm.getGroupData().add(modifiedRM);
                        break;
                    }
                }
            }
            this.dataBean.setRecordModel(modifiedRM);
            this.dataBean.edit();
        }
    }

    public void showDetails(RecordModel rm) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                    + rm.getFieldValueMap().get(this.keyFieldName).getCurrentValue() + "&" + "moduleId=" + this.dataBean.getModuleId());
        }
        catch (IOException ex) {
            Logger.getLogger(KanbanViewManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String copyCardContent(RecordModel rm) {
        String content = "";
        for (KanbanCard kanbanCard : kanbanViewSettings.getCardSettings()) {
            if (!kanbanCard.isEnableCustomField()) {
                content = content + "\n" + rm.getFieldValueMap().get(kanbanCard.getCardField().getFieldName()).getCurrentValue();
            }
            else {
                if (rm.getFieldValueMap().get(kanbanCard.getCardField().getFieldName() + "_eval") != null) {
                    content = content + "\n" + rm.getFieldValueMap().get(kanbanCard.getCardField().getFieldName() + "_eval").getCurrentValue();
                }
            }
        }
        return content;
    }

    public void successListener(final ClipboardSuccessEvent successEvent) {
        final FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Content Copied");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String evaluateCustomField(RecordModel targetRecord, String fieldScript, KanbanCard kanbanCard) {
//        if (kanbanCard.getEvaluatedCustomField() != null && !kanbanCard.getEvaluatedCustomField().isEmpty()) {
//            return kanbanCard.getEvaluatedCustomField();
//        }
        BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
        beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
//        beezlInterperter.clear();
        beezlInterperter.setRecodeModel(targetRecord);
        LinkedHashMap<String, String> bvm;
        beezlInterperter.addDynamicField("fieldScript", fieldScript);
        try {
            bvm = beezlInterperter.evaluate();
        }
        catch (Exception ex) {
            Logger.getLogger(KanbanViewManager.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
        kanbanCard.setEvaluatedCustomField(bvm.get("fieldScript"));

        ModuleFieldModel evaluatedScriptField = new ModuleFieldModel();
        evaluatedScriptField.setFieldName(kanbanCard.getCardField().getFieldName() + "_eval");
        evaluatedScriptField.setCurrentValue(bvm.get("fieldScript"));
        targetRecord.getFieldValueMap().put(kanbanCard.getCardField().getFieldName() + "_eval", evaluatedScriptField);
        return bvm.get("fieldScript");
    }

    public String evaluateCustomCard(RecordModel targetRecord) {
        if (this.kanbanViewSettings.getCustomCardConfiguration() != null) {
            if (this.kanbanViewSettings.getCustomCardConfiguration().getHtmlContent() != null
                    && !this.kanbanViewSettings.getCustomCardConfiguration().getHtmlContent().isEmpty()
                    && this.kanbanViewSettings.getCustomCardConfiguration().getTemplateParameters() != null
                    && !this.kanbanViewSettings.getCustomCardConfiguration().getTemplateParameters().isEmpty()) {

                if (this.kanbanViewSettings.getCustomCardConfiguration().getLinkStyling() != null) {
                    this.customLinkStyling = this.customLinkStyling + "\n" + this.kanbanViewSettings.getCustomCardConfiguration().getLinkStyling();
                }

                if (this.kanbanViewSettings.getCustomCardConfiguration().getCssStyling() != null) {
                    this.customCssStyling = this.customCssStyling + "\n" + this.kanbanViewSettings.getCustomCardConfiguration().getCssStyling();
                }

                BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                beezlInterperter.setRecodeModel(targetRecord);
                LinkedHashMap<String, String> bvm = null;

                Map<String, Object> configs = new HashMap<>();
                for (DocTemplateParameter dtp : this.kanbanViewSettings.getCustomCardConfiguration().getTemplateParameters()) {
                    switch (dtp.getType()) {
                        case "STRING":
                            beezlInterperter.addDynamicField(dtp.getKeyName(), dtp.getKeyValue());
                            break;
                        case "LIST":
                            break;
                    }
                }

                try {
                    bvm = beezlInterperter.evaluate();
                }
                catch (Exception ex) {
                    Logger.getLogger(KanbanViewManager.class.getName()).log(Level.SEVERE, null, ex);
                }

                for (DocTemplateParameter dtp : this.kanbanViewSettings.getCustomCardConfiguration().getTemplateParameters()) {
                    if (bvm != null) {
                        if (!dtp.getType().equals("LIST")) {
                            configs.put(dtp.getKeyName(), bvm.get(dtp.getKeyName()));
                        }
                    }
                    else {
                        return "";
                    }
                }

                StringWriter out = new StringWriter();
                Configuration cfg = new Configuration();
                cfg.setDefaultEncoding("UTF-8");
                Template t;
                try {
                    t = new Template("name", new StringReader(this.kanbanViewSettings.getCustomCardConfiguration().getHtmlContent()), cfg);
                    t.process(configs, out);
                }
                catch (Exception ex) {
                    Logger.getLogger(DashboardV2Bean.class.getName()).log(Level.SEVERE, null, ex);
                    return "";
                }
                String processedWidget = out.getBuffer().toString();
                return processedWidget;
            }
        }
        return "";
    }

    public void search(String api) {
        if (this.dataBean.getFTSKeyword() != null && !this.dataBean.getFTSKeyword().isEmpty()) {
            for (KanbanRecordModel krm : recordsGroupsList) {
                krm.setLazyRecordModel(new LazyKanbanModel("GenericMasterAPI", this.dataBean.getFTSKeyword(), true, LazyKanbanModel.RECORD, this.dataBean.getModuleId(), kanbanViewSettings, dataBean, krm.getTagId()));
            }
//            this.dataBean.setFTSKeyword(null);
        }
        else {
            FilterManager fm = new FilterManager();
            this.dataBean.populateFormData();
            if (this.dataBean.isRelationView()) {
                fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + this.dataBean.getKeyName(), this.dataBean.getKeyValue()));
            }
            for (ModuleFieldModel mfm : this.dataBean.getFieldsList()) {
                if (mfm.getCurrentValue() != null && !mfm.getCurrentValue().isEmpty()) {
                    if (this.dataBean.getRelationRecordsCache().get(mfm.getFieldName()) != null) {
                        String tableName = this.dataBean.getRelationRecordsCache().get(mfm.getFieldName()).getRelationManager().getMasterTable();
//                        fm.setFilter("AND", new FilterType(tableName + "." + mfm.getFieldName(), mfm.getCurrentValue()));
                        fm.setFilter("AND", new FilterType(tableName + "." + mfm.getFieldName(), "IN", mfm.getCurrentValue()));
                    }
                    else {
                        switch (mfm.getFieldType()) {
                            case "LOOKUP":
                                fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "=", mfm.getCurrentValue()));
                                break;
                            case "BOOLEAN":
                                if (mfm.getCurrentValue() == null || mfm.getCurrentValue().isEmpty() || mfm.getCurrentValue().equals("0")) {
                                }
                                else if (mfm.getCurrentValue().equals("1")) {
                                    fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "=", true));
                                }
                                else if (mfm.getCurrentValue().equals("2")) {
                                    fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "=", false));
                                }
                                break;
                            case "INT":
                            case "DECIMAL":
                                fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "=", mfm.getCurrentValue()));
                                break;
                            case "DATE":
                                if (!mfm.isDateBetween()) {
                                    String modifiedDateValue = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getCurrentValue()));
                                    mfm.setCurrentValue(modifiedDateValue);
                                    fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "=", modifiedDateValue, "DATE", true));
                                }
                                else {
                                    String modifiedDateFrom = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateFrom()));
                                    String modifiedDateTo = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateTo()));
                                    fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "BETWEEN", modifiedDateFrom, modifiedDateTo));
                                }
                                break;
                            default:
                                fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "LIKE", mfm.getCurrentValue()));
                                break;
                        }
                    }
                }
                else if (mfm.getFieldType().equals("DATE")) {
                    if (mfm.isDateBetween()) {
                        String modifiedDateFrom = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateFrom()));
                        String modifiedDateTo = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateTo()));
                        fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "BETWEEN", modifiedDateFrom, modifiedDateTo));
                        mfm.setDateFrom(GeneralUtils.convertPFDateFormate(modifiedDateFrom));
                        mfm.setDateTo(GeneralUtils.convertPFDateFormate(modifiedDateTo));
                    }
                }
            }
            for (KanbanRecordModel krm : recordsGroupsList) {
                krm.setLazyRecordModel(new LazyKanbanModel("GenericMasterAPI", fm.getClause(), false, LazyKanbanModel.RECORD, this.dataBean.getModuleId(), kanbanViewSettings, dataBean, krm.getTagId()));
            }
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public ArrayList<KanbanRecordModel> getRecordsGroupsList() {
        return recordsGroupsList;
    }

    public void setRecordsGroupsList(ArrayList<KanbanRecordModel> recordsGroupsList) {
        this.recordsGroupsList = recordsGroupsList;
    }

    public KanbanViewSettings getKanbanViewSettings() {
        return kanbanViewSettings;
    }

    public void setKanbanViewSettings(KanbanViewSettings kanbanViewSettings) {
        this.kanbanViewSettings = kanbanViewSettings;
    }

    public String getKeyFieldName() {
        return keyFieldName;
    }

    public void setKeyFieldName(String keyFieldName) {
        this.keyFieldName = keyFieldName;
    }

    public String getCustomLinkStyling() {
        return customLinkStyling;
    }

    public void setCustomLinkStyling(String customLinkStyling) {
        this.customLinkStyling = customLinkStyling;
    }

    public String getCustomCssStyling() {
        return customCssStyling;
    }

    public void setCustomCssStyling(String customCssStyling) {
        this.customCssStyling = customCssStyling;
    }

    public String getEvaluatedCustomCard() {
        return evaluatedCustomCard;
    }

    public void setEvaluatedCustomCard(String evaluatedCustomCard) {
        this.evaluatedCustomCard = evaluatedCustomCard;
    }

}
