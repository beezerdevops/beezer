/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.templates.dataView;

import com.beezer.web.beans.templates.DataBean;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.ProxyRecordModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.DataTableSettings;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.dataView.SpreadsheetViewSettings;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.managers.RelationResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.commons.lang3.SerializationUtils;
import org.primefaces.PrimeFaces;
import org.primefaces.extensions.component.sheet.Sheet;
import org.primefaces.extensions.event.SheetEvent;
import org.primefaces.extensions.model.sheet.SheetUpdate;

/**
 *
 * @author Ahmed El Badry Nov 23, 2021
 */
public class SpreadsheetViewManager {

    private SpreadsheetViewSettings spreadsheetViewSettings;
    private DataBean dataBean;
    private ArrayList<RecordModel> recordsList;
    private ArrayList<RecordModel> filteredRecordsList;
    private ArrayList<String> dummyList;
    private HashMap<String, ArrayList<ProxyRecordModel>> relationRecordsCache;

    public SpreadsheetViewManager(SpreadsheetViewSettings spreadsheetViewSettings, DataBean dataBean) {
        this.spreadsheetViewSettings = spreadsheetViewSettings;
        this.dataBean = dataBean;
        this.relationRecordsCache = new HashMap<>();
//        this.loadRecordsListForSpreadsheet();
    }

    public void initializeSpreadsheetView() {
        this.loadRecordsListForSpreadsheet();
        this.initializeViewSettings();
        PrimeFaces.current().ajax().update("bzr_spreadsheet");
    }

    private void initializeViewSettings() {
        if (this.spreadsheetViewSettings != null && this.spreadsheetViewSettings.getTableSettings() != null) {
            for (DataTableSettings dts : this.spreadsheetViewSettings.getTableSettings()) {
                switch (dts.getColumnType()) {
                    case "LOOKUP":
                        if (!dts.getColumnFieldName().contains(".name")) {
                            dts.setColumnFieldName(dts.getColumnFieldName() + ".name");
                        }
                        break;
                    case "REL":
                        RelationManagerModel rmm = this.dataBean.getRelationRecordsCache().get(dts.getColumnFieldName()).getRelationManager();
                        this.loadRelation(dts.getColumnFieldName(), rmm);
                        break;
                }
            }
        }
    }

    public void loadRecordsListForSpreadsheet() {
        try {
            RequestModel request = new RequestModel();
            request.setRequestingModule(this.dataBean.getModuleId());
            request.setLimit(10000);
            request.setOffset(0);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
                this.recordsList = response.getRecordList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(SpreadsheetViewManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ProxyRecordModel transformToProxy(RecordModel recordModel, String fieldName) {
        RelationManagerModel rmm = this.dataBean.getRelationRecordsCache().get(fieldName).getRelationManager();
        ProxyRecordModel prm = new ProxyRecordModel();
        prm.setFieldValueMap(SerializationUtils.clone(recordModel.getFieldValueMap()));
        prm.setViewField(rmm.getViewColumn());
        return prm;
    }

    public String fetchRelationRecordViewValue(Object relValueIdObj, String fieldName) {
        String relValueId = String.valueOf(relValueIdObj);
        RelationManagerModel rmm = this.dataBean.getRelationRecordsCache().get(fieldName).getRelationManager();
        for (ProxyRecordModel prm : this.relationRecordsCache.get(fieldName)) {
            if (prm.getFieldValueMap().get(rmm.getDetailColumn()).getCurrentValue().equals(relValueId)) {
                return prm.getFieldValueMap().get(rmm.getViewColumn()).getCurrentValue();
            }
        }
        return "";
    }

    protected void loadRelation(String fieldName, RelationManagerModel relationManagerModel) {
        RelationRequest relationRequest = new RelationRequest();
        relationRequest.setRelationId(String.valueOf(relationManagerModel.getId()));
        relationRequest.setSkipRecords(false);
        RelationHandler handler = new RelationHandler();
        RelationResponse relationResponse;
        try {
            relationResponse = handler.getRelation(relationRequest);
            if (relationResponse.getErrorCode() == 1000) {
                if (relationResponse.getRecordList() != null) {
                    ArrayList<ProxyRecordModel> proxyRecordList = new ArrayList<>();
                    for (RecordModel rm : relationResponse.getRecordList()) {
                        ProxyRecordModel prm = new ProxyRecordModel();
                        prm.setFieldValueMap(SerializationUtils.clone(rm.getFieldValueMap()));
                        prm.setViewField(relationManagerModel.getViewColumn());
                        proxyRecordList.add(prm);
                    }
                    this.relationRecordsCache.put(fieldName, proxyRecordList);
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(SpreadsheetViewManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String fetchRowKey(RecordModel recordModel) {
        ModuleFieldModel mfm = GeneralUtils.getKeyFieldFromRecord(recordModel);
        if (mfm != null) {
            return mfm.getCurrentValue();
        }
        return "";
    }

    public void cellChangeEvent(final SheetEvent event) {
        final Sheet sheet = event.getSheet();
        final List<SheetUpdate> updates = sheet.getUpdates();
        // A SheetUpdate exists for each column updated, the same row may
        // appear more than once. For this reason we will track those we already
        // persisted
        final HashSet<RecordModel> processed = new HashSet<>();
        int rowUpdates = 0;
        for (final SheetUpdate update : updates) {
            final RecordModel recordModel = (RecordModel) update.getRowData();
            if (processed.contains(recordModel)) {
                continue;
            }
            this.dataBean.setRecordModel(recordModel);
            this.dataBean.edit();
            rowUpdates++;
        }
        sheet.commitUpdates();
    }
    
     public  void rowSelectEvent(final SheetEvent event) {
        final Sheet sheet = event.getSheet();
        final int row = sheet.getSelectedRow();
//        for(sheet.get)
        FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Row Selected", String.format("Row %d selected.", row)));
    }

    public SpreadsheetViewSettings getSpreadsheetViewSettings() {
        return spreadsheetViewSettings;
    }

    public void setSpreadsheetViewSettings(SpreadsheetViewSettings spreadsheetViewSettings) {
        this.spreadsheetViewSettings = spreadsheetViewSettings;
    }

    public DataBean getDataBean() {
        return dataBean;
    }

    public void setDataBean(DataBean dataBean) {
        this.dataBean = dataBean;
    }

    public ArrayList<RecordModel> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(ArrayList<RecordModel> recordsList) {
        this.recordsList = recordsList;
    }

    public ArrayList<RecordModel> getFilteredRecordsList() {
        return filteredRecordsList;
    }

    public void setFilteredRecordsList(ArrayList<RecordModel> filteredRecordsList) {
        this.filteredRecordsList = filteredRecordsList;
    }

    public ArrayList<String> getDummyList() {
        return dummyList;
    }

    public void setDummyList(ArrayList<String> dummyList) {
        this.dummyList = dummyList;
    }

    public HashMap<String, ArrayList<ProxyRecordModel>> getRelationRecordsCache() {
        return relationRecordsCache;
    }

    public void setRelationRecordsCache(HashMap<String, ArrayList<ProxyRecordModel>> relationRecordsCache) {
        this.relationRecordsCache = relationRecordsCache;
    }

}
