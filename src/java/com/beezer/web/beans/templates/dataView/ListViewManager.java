/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.templates.dataView;

import com.beezer.web.beans.templates.DataBean;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.models.LazyRecordModel;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.dataView.ListViewDataSettings;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.ByteArrayInputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Ahmed El Badry Jan 22, 2021
 */
public class ListViewManager {

    private final DataBean dataBean;
    private ListViewDataSettings listViewDataSettings;
    private LazyRecordModel lazyRecordModel;
    private RecordModel selectedRecordForDelete;
    private String moduleId;
    private String styleClass = "";

    public ListViewManager(DataBean dataBean) {
        this.dataBean = dataBean;
        this.listViewDataSettings = this.dataBean.getDataViewSettings().getDataViewConfig().getListViewDataSettings();
        this.moduleId = this.dataBean.getModuleId();
    }

    public void initialize() {
        lazyRecordModel = new LazyRecordModel("GenericMasterAPI", null, false, LazyRecordModel.RECORD, this.dataBean.getModuleId());
        PrimeFaces.current().ajax().update("allRecordsCardGrid");
    }

    public void onRowSelect(RecordModel selectedModel) {
        String recordId = "";
        try {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", selectedModel);
            for (Map.Entry<String, ModuleFieldModel> entry : selectedModel.getFieldValueMap().entrySet()) {
                if (entry.getValue().isIsKey()) {
                    recordId = entry.getValue().getCurrentValue();
                    break;
                }
            }
            if (dataBean.getModuleId().equals("7")) {
//                FacesContext.getCurrentInstance().getExternalContext().redirect("dealDetails.xhtml?recordId="
//                        + this.recordId);
                FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                        + recordId + "&" + "moduleId=" + dataBean.getModuleId());
            }
            else {
                if (dataBean.getOriginatingDetailBean() != null) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                            + recordId + "&" + "moduleId=" + dataBean.getModuleId()
                            + "&masterRec=" + dataBean.getOriginatingDetailBean().getRequestedId()
                            + "&masterModule=" + dataBean.getOriginatingDetailBean().getModuleId());
                }
                else {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                            + recordId + "&" + "moduleId=" + dataBean.getModuleId());
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ListViewManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void openDeleteConfirmation(RecordModel selectedModel) {
        this.selectedRecordForDelete = selectedModel;
        PrimeFaces.current().ajax().update("cardDeleteConfirmation");
        PrimeFaces.current().executeScript("PF('cardDeleteConfirmation').show();");
    }

    public void delete() {
        RequestModel request = new RequestModel();
        request.setRecordModel(selectedRecordForDelete);
        request.setRequestActionType("2");
        request.setRequestingModule(dataBean.getModuleId());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            switch (response.getErrorCode()) {
                case 1000:
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Record deleted Susscefully. "));
                    break;
                case 1002:
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Success", "Record deleted Susscefully. " + response.getErrorMessage()));
                    break;
                default:
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete record due to: " + response.getErrorMessage()));
                    break;
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ListViewManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void search(String api) {
        if (this.dataBean.getFTSKeyword() != null && !this.dataBean.getFTSKeyword().isEmpty()) {
            lazyRecordModel = new LazyRecordModel("GenericMasterAPI", this.dataBean.getFTSKeyword(), true, LazyRecordModel.RECORD, this.dataBean.getModuleId());
//            this.dataBean.setFTSKeyword(null);
        }
        else {
            FilterManager fm = new FilterManager();
            this.dataBean.populateFormData();
            if (this.dataBean.isRelationView()) {
                fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + this.dataBean.getKeyName(), this.dataBean.getKeyValue()));
            }
            for (ModuleFieldModel mfm : this.dataBean.getFieldsList()) {
                if (mfm.getCurrentValue() != null && !mfm.getCurrentValue().isEmpty()) {
                    if (this.dataBean.getRelationRecordsCache().get(mfm.getFieldName()) != null) {
                        String tableName = this.dataBean.getRelationRecordsCache().get(mfm.getFieldName()).getRelationManager().getMasterTable();
//                        fm.setFilter("AND", new FilterType(tableName + "." + mfm.getFieldName(), mfm.getCurrentValue()));
                        fm.setFilter("AND", new FilterType(tableName + "." + mfm.getFieldName(), "IN", mfm.getCurrentValue()));
                    }
                    else {
                        switch (mfm.getFieldType()) {
                            case "LOOKUP":
                                fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "=", mfm.getCurrentValue()));
                                break;
                            case "BOOLEAN":
                                if (mfm.getCurrentValue() == null || mfm.getCurrentValue().isEmpty() || mfm.getCurrentValue().equals("0")) {
                                }
                                else if (mfm.getCurrentValue().equals("1")) {
                                    fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "=", true));
                                }
                                else if (mfm.getCurrentValue().equals("2")) {
                                    fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "=", false));
                                }
                                break;
                            case "INT":
                            case "DECIMAL":
                                fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "=", mfm.getCurrentValue()));
                                break;
                            case "DATE":
                                if (!mfm.isDateBetween()) {
                                    String modifiedDateValue = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getCurrentValue()));
                                    mfm.setCurrentValue(modifiedDateValue);
                                    fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "=", modifiedDateValue, "DATE", true));
                                }
                                else {
                                    String modifiedDateFrom = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateFrom()));
                                    String modifiedDateTo = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateTo()));
                                    fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "BETWEEN", modifiedDateFrom, modifiedDateTo));
                                }
                                break;
                            default:
                                fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "LIKE", mfm.getCurrentValue()));
                                break;
                        }
                    }
                }
                else if (mfm.getFieldType().equals("DATE")) {
                    if (mfm.isDateBetween()) {
                        String modifiedDateFrom = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateFrom()));
                        String modifiedDateTo = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateTo()));
                        fm.setFilter("AND", new FilterType(this.dataBean.getActiveModule().getModuleBaseTable() + "." + mfm.getFieldName(), "BETWEEN", modifiedDateFrom, modifiedDateTo));
                        mfm.setDateFrom(GeneralUtils.convertPFDateFormate(modifiedDateFrom));
                        mfm.setDateTo(GeneralUtils.convertPFDateFormate(modifiedDateTo));
                    }
                }
            }
            lazyRecordModel = new LazyRecordModel("GenericMasterAPI", fm.getClause(), false, LazyRecordModel.RECORD, this.dataBean.getModuleId());
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public StreamedContent loadImageFromStream(UnstructuredModel unstructuredModel) {
        FacesContext context = FacesContext.getCurrentInstance();
        String id = context.getExternalContext().getRequestParameterMap().get("contentId");
        String imageContext = context.getExternalContext().getRequestParameterMap().get("context");
        String moduleId = context.getExternalContext().getRequestParameterMap().get("moduleId");

        return DefaultStreamedContent.builder()
                .stream(() -> new ByteArrayInputStream(
                unstructuredModel.getContentStream()))
                .build();
    }

    public LazyRecordModel getLazyRecordModel() {
        return lazyRecordModel;
    }

    public void setLazyRecordModel(LazyRecordModel lazyRecordModel) {
        this.lazyRecordModel = lazyRecordModel;
    }

    public ListViewDataSettings getListViewDataSettings() {
        return listViewDataSettings;
    }

    public void setListViewDataSettings(ListViewDataSettings listViewDataSettings) {
        this.listViewDataSettings = listViewDataSettings;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

}
