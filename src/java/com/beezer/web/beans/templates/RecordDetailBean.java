/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.templates;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.content.ContentLibraryBean;
import com.beezer.web.beans.modules.notes.NotesBean;
import com.beezer.web.beans.modules.reports.ReportViewer;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.handler.SmartViewHandler;
import com.beezer.web.handler.TaskHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.comparators.FieldLayoutOrderComparator;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.global.UserModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.TaskManagerModel;
import com.crm.models.internal.TaskMemberModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.internal.smartView.SmartViewModel;
import com.crm.models.internal.smartView.TabsConfig;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.requests.managers.SmartViewRequest;
import com.crm.models.requests.managers.TasksRequest;
import com.crm.models.responses.managers.RelationResponse;
import com.crm.models.responses.managers.SmartViewResponse;
import com.crm.models.responses.managers.TasksResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "recordDetailBean")
@ViewScoped
public class RecordDetailBean extends BeanFramework {

    private String requestedId;
    private LinkedHashMap<Integer, RelationManagerModel> relationalMap;
    private LinkedHashMap<Integer, DataBean> relationalBeanMap;
    private LinkedHashMap<Integer, String> relationalKeyMap;
    private LinkedHashMap<Integer, String> relationalTableMap;

    private SmartViewModel smartViewModel;

    @ManagedProperty(value = "#{notesBean}")
    private NotesBean notesBean;

    @ManagedProperty(value = "#{contentLibraryBean}")
    private ContentLibraryBean contentLibraryBean;

    private boolean approval;
    private TaskManagerModel originatingTask;

    private TaskManagerModel clarificationTaskRequest;

    private UnstructuredModel headerDynamicContent;

    private ModuleModel activeModule;

    @ManagedProperty(value = "#{usersBean.usersList}")
    private ArrayList<UserModel> users;
    private ArrayList<UserModel> selectedUsers;

    private boolean originatedFromSmartView;

    private String chosenRichTextField;

    private int selectedTabModule;

    private boolean customTabActive = false;
    private TabsConfig activeCustomTab;
    private ReportViewer reportViewer;

    private String originatingTaskPage = "1";

    public RecordDetailBean() {
    }

    @PostConstruct
    public void init() {
        super.BEAN_TYPE = "RECORD_DETAIL_BEAN";
        this.setSelectedRecord();

        if (super.getRequestParams().get("masterRec") != null && !super.getRequestParams().get("masterRec").isEmpty()
                && super.getRequestParams().get("masterModule") != null && !super.getRequestParams().get("masterModule").isEmpty()) {
            this.originatedFromSmartView = true;
        }

        if (super.getRequestParams().get("origin_page") != null) {
            originatingTaskPage = super.getRequestParams().get("origin_page");
        }
        else {
            originatingTaskPage = "1";
        }

//        this.activeModule = super.loadModule(super.getModuleId());
        super.loadFieldSet(super.getModuleId(), null);
        this.loadRecord();
        this.loadSmartViewConfigurations();
        this.loadRelationalModules();

        super.fillRecordDetails();
        super.fillAdvancentComponentsValuesForSmartView(smartViewModel);
        super.setMasterPageName("data.xhtml?moduleId=" + super.getModuleId());
    }

    private void setSelectedRecord() {
        requestedId = super.getRequestParams().get("recordId");
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_APPROVE:
                this.approval = true;
                this.loadOriginationTask();
                break;
        }
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        request.setApprovalTaskRequest(approval);
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        request.setRequestContext("RECORD_DETAIL");
        ModuleModel mm = super.loadModule(super.getModuleId());
        super.setModuleBreadCrumbName(mm.getModuleName());
        String moduleName = "";
        if (mm != null) {
            moduleName = mm.getModuleBaseTable() + ".";
        }
        FilterManager fm = new FilterManager();
        if (requestedId != null) {
            fm.setFilter("AND", new FilterType(moduleName + this.getKeyField(), requestedId));
            request.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response;
            try {
                response = requestHandler.executeRequest(request, "GenericMasterAPI");
                if (response.getErrorCode() == 1000) {
                    super.recordModel = response.getRecordList().get(0);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void loadOriginationTask() {
        String taskId = super.getRequestParams().get("TaskId");
        if (taskId != null && !taskId.isEmpty()) {
            TasksRequest tasksRequest = new TasksRequest();
            tasksRequest.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("tasks_manager_base.task_id", "=", taskId));
            tasksRequest.setClause(fm.getClause());
            TaskHandler taskHandler = new TaskHandler();
            TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
            if (tasksResponse.getErrorCode() == 1000) {
                if (tasksResponse.getReturnList() != null && !tasksResponse.getReturnList().isEmpty()) {
                    this.originatingTask = tasksResponse.getReturnList().get(0);
                    this.clarificationTaskRequest = new TaskManagerModel();
                }
            }
        }

    }

    public void loadRelationalModules() {
        RelationRequest relationRequest = new RelationRequest();
        relationRequest.setRelationId(super.getModuleId());
        relationRequest.setActionType("5");
        RelationHandler handler = new RelationHandler();
        RelationResponse relationResponse;
        try {
            relationResponse = handler.relationExecutor(relationRequest);
            if (relationResponse.getErrorCode() == 1000) {
                this.relationalMap = new LinkedHashMap<>();
                this.relationalBeanMap = new LinkedHashMap<>();
                this.relationalKeyMap = new LinkedHashMap<>();
                this.relationalTableMap = new LinkedHashMap<>();
                DataBean db;
                for (RelationManagerModel rmm : relationResponse.getRelationManagerList()) {
                    if (smartViewModel != null && smartViewModel.getTabConfigHolder() != null
                            && smartViewModel.getTabConfigHolder().getDefaultTabsConfig() != null) {
                        for (TabsConfig tabsConfig : smartViewModel.getTabConfigHolder().getDefaultTabsConfig()) {
                            int mId = Integer.valueOf(tabsConfig.getTabId().replaceAll("ID_", ""));
                            if (mId == rmm.getModuleId()) {
                                if (relationalMap.get(rmm.getModuleId()) != null) {
                                    if (!relationalMap.get(rmm.getModuleId()).isLinkingRelation()) {
                                        relationalMap.put(rmm.getModuleId(), rmm);
                                        db = new DataBean();
                                        db.setOriginatingDetailBean(this);
                                        db.setModuleId(String.valueOf(rmm.getModuleId()));
                                        db.setRelationView(true);
//                                        db.init();
                                        relationalBeanMap.put(rmm.getModuleId(), db);
                                        relationalKeyMap.put(rmm.getModuleId(), rmm.getMasterColumn());
                                        relationalTableMap.put(rmm.getModuleId(), rmm.getMasterTable());
                                    }
                                }
                                else {
                                    relationalMap.put(rmm.getModuleId(), rmm);
                                    db = new DataBean();
                                    db.setOriginatingDetailBean(this);
                                    db.setModuleId(String.valueOf(rmm.getModuleId()));
                                    db.setRelationView(true);
//                                    db.init();
                                    relationalBeanMap.put(rmm.getModuleId(), db);
                                    relationalKeyMap.put(rmm.getModuleId(), rmm.getMasterColumn());
                                    relationalTableMap.put(rmm.getModuleId(), rmm.getMasterTable());
                                }

                            }
                        }
                    }
                    else {

                        RelationManagerModel workingRMM = new RelationManagerModel();

                        if (relationalMap.get(rmm.getModuleId()) != null) {
                            RelationManagerModel tempRMM = relationalMap.get(rmm.getModuleId());
                            if (!tempRMM.getRelationType().equals("O2M")) {
                                if (rmm.getRelationType().equals("O2M")) {
                                    workingRMM = rmm;
                                }
                            }
                        }
                        else {
                            workingRMM = rmm;
                        }

                        relationalMap.put(workingRMM.getModuleId(), workingRMM);
                        db = new DataBean();
                        db.setModuleId(String.valueOf(workingRMM.getModuleId()));
                        db.setRelationView(true);
                        db.setOriginatingDetailBean(this);
                        db.init();
                        relationalBeanMap.put(workingRMM.getModuleId(), db);
                        relationalKeyMap.put(workingRMM.getModuleId(), workingRMM.getMasterColumn());
                        relationalTableMap.put(workingRMM.getModuleId(), workingRMM.getMasterTable());

//                        relationalMap.put(rmm.getModuleId(), rmm);
//                        db = new DataBean();
//                        db.setModuleId(String.valueOf(rmm.getModuleId()));
//                        db.setRelationView(true);
//                        db.init();
//                        relationalBeanMap.put(rmm.getModuleId(), db);
//                        relationalKeyMap.put(rmm.getModuleId(), rmm.getMasterColumn());
//                        relationalTableMap.put(rmm.getModuleId(), rmm.getMasterTable());
                    }

                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadSmartViewConfigurations() {
        if (smartViewModel == null) {
            SmartViewHandler smartViewHandler = new SmartViewHandler();
            SmartViewRequest request = new SmartViewRequest();
            request.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("module_id", "=", super.getModuleId()));
            request.setClause(fm.getClause());
            SmartViewResponse response = smartViewHandler.executeRequest(request);
            if (response.getErrorCode() == 1000) {
                if (response.getReturnList() != null && !response.getReturnList().isEmpty()) {
                    smartViewModel = response.getReturnList().get(0);
                }
            }
        }

        if (smartViewModel != null) {
            if (smartViewModel.getHeaderConfig() != null && smartViewModel.getHeaderConfig().getDisplayPicType() != null) {
                if (smartViewModel.getHeaderConfig().getDisplayPicType().equalsIgnoreCase("DYNAMIC")) {
                    for (Map.Entry<String, ModuleFieldModel> entry : this.recordModel.getFieldValueMap().entrySet()) {
                        if (entry.getValue().getFieldName().equals(smartViewModel.getHeaderConfig().getDisplayPicRef())) {
                            this.headerDynamicContent = entry.getValue().getContent();
                            if (headerDynamicContent != null) {
                                smartViewModel.getHeaderConfig().setDisplayPicRef(entry.getValue().getContent().getUnstructuredId());
                                break;
                            }
                        }
                    }
                }
            }

            if (smartViewModel.getHeaderConfig() != null && smartViewModel.getHeaderConfig().getFieldsLayoutList() != null) {
                Collections.sort(smartViewModel.getHeaderConfig().getFieldsLayoutList(), new FieldLayoutOrderComparator());
                for (FieldsLayoutModel flm : smartViewModel.getHeaderConfig().getFieldsLayoutList()) {
                    super.buildAdvancedComponentsMap(flm);
                }
            }

            if (smartViewModel.getTabConfigHolder() != null && smartViewModel.getTabConfigHolder().getTabsConfig() != null) {
                for (TabsConfig tc : smartViewModel.getTabConfigHolder().getTabsConfig()) {
                    if (tc.getFieldsLayoutList() != null) {
                        for (FieldsLayoutModel flm : tc.getFieldsLayoutList()) {
                            super.buildAdvancedComponentsMap(flm);
                        }
                    }
                }
            }
        }
    }

    public void onTabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        if (!tabId.equalsIgnoreCase("detailsTab") && !tabId.equalsIgnoreCase("notesTab") && !tabId.equalsIgnoreCase("attachmentsTab")) {
            if (!tabId.contains("smartTab_")) {
                int id = Integer.parseInt(tabId.replaceAll("id", ""));
                DataBean temp = this.relationalBeanMap.get(id);
                temp.init();
                String qualifiedColumnName = this.relationalTableMap.get(id) + "." + this.relationalKeyMap.get(id);
                String keyValue = GeneralUtils.getFieldFromRecord(recordModel, this.relationalMap.get(id).getDetailColumn()).getCurrentValue();
                temp.loadRelatedRecord(qualifiedColumnName, keyValue);
                temp.loadFilters(String.valueOf(id));
                temp.setKeyName(qualifiedColumnName);
                temp.setKeyValue(keyValue);
                temp.loadDataViewSettings("DETAIL");
                this.relationalBeanMap.put(id, temp);
//                RequestContext.getCurrentInstance().update("detailsForm:tabHolder");
                PrimeFaces.current().ajax().update("tabHolder");
            }
            else {
//                RequestContext.getCurrentInstance().update("detailsForm:tabHolder");
                PrimeFaces.current().ajax().update("tabHolder");
            }
        }
        else if (tabId.equalsIgnoreCase("notesTab")) {
            notesBean.setRequestedModuleId(super.getModuleId());
            notesBean.setRequestedObjectId(this.requestedId);
            notesBean.init();
        }
        else if (tabId.equalsIgnoreCase("attachmentsTab")) {
            contentLibraryBean.setRequestedModule(super.getModuleId());
            contentLibraryBean.setRequestedObjectId(this.requestedId);
            contentLibraryBean.init();
        }
    }

    public void onTabChangeForced(String tabId) {
        customTabActive = true;
        if (tabId.contains("smartTab_")) {
            for (TabsConfig tabsConfig : this.smartViewModel.getTabConfigHolder().getTabsConfig()) {
                String tabOriginalId = tabId.replace("smartTab_", "");
                if (tabsConfig.getTabId().equals(tabOriginalId)) {
                    this.activeCustomTab = tabsConfig;
                    break;
                }
            }

            if (this.activeCustomTab.getFieldsLayoutList() != null && !this.activeCustomTab.getFieldsLayoutList().isEmpty()) {
                for (FieldsLayoutModel flm : this.activeCustomTab.getFieldsLayoutList()) {
                    if (flm.getCustomComponentConfiguration() != null) {
                        if (flm.getCustomComponentConfiguration().getReportWidgetConfig() != null) {
                            this.reportViewer = new ReportViewer();
                            this.reportViewer.setReportId(flm.getCustomComponentConfiguration().getReportWidgetConfig().getReportId());
                            this.reportViewer.setFromSmartView(true);
                            this.reportViewer.init();
//                            this.reportViewer.initializeForPreview(reportViewer.getReportModel());
                            this.reportViewer.evaluateGlobalVariables(recordModel, flm.getCustomComponentConfiguration().getGlobalVariables());
                        }
                    }
                }
            }

            PrimeFaces.current().ajax().update("tabContentHolder");
        }
    }

    public void approveRequest() {
        TasksRequest tasksRequest = new TasksRequest();
        if (this.originatingTask.getTaskType().equals("CLARIFY")) {
            tasksRequest.setRequestActionType("9");
        }
        else {
            tasksRequest.setRequestActionType("3");
        }
        this.originatingTask.setTaskResponse("APPROVED");
        this.originatingTask.setStatus("DONE");
        tasksRequest.setTaskManagerModel(originatingTask);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            try {
//                FacesContext.getCurrentInstance().getExternalContext().redirect("taskDetails.xhtml?recordId="
//                        + originatingTask.getTaskId() + "&" + "moduleId=" + "34");
                FacesContext.getCurrentInstance().getExternalContext().redirect("tasksDashboard.xhtml?origin_page=" + originatingTaskPage);
            }
            catch (IOException ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void rejectRequest() {
        TasksRequest tasksRequest = new TasksRequest();
        if (this.originatingTask.getTaskType().equals("CLARIFY")) {
            tasksRequest.setRequestActionType("9");
        }
        else {
            tasksRequest.setRequestActionType("3");
        }
        this.originatingTask.setTaskResponse("REJECTED");
        this.originatingTask.setStatus("DONE");
        tasksRequest.setTaskManagerModel(originatingTask);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            try {
//                FacesContext.getCurrentInstance().getExternalContext().redirect("taskDetails.xhtml?recordId="
//                        + originatingTask.getTaskId() + "&" + "moduleId=" + "34");
                FacesContext.getCurrentInstance().getExternalContext().redirect("tasksDashboard.xhtml?origin_page=" + originatingTaskPage);
            }
            catch (IOException ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void clarificationRequest() {
        if (selectedUsers != null) {
            this.clarificationTaskRequest.setTaskMembers(new ArrayList<>());
            for (UserModel um : selectedUsers) {
                TaskMemberModel memberModel = new TaskMemberModel();
                memberModel.setMemberId(um.getUserId());
                memberModel.setMemberType("USER");
                this.clarificationTaskRequest.getTaskMembers().add(memberModel);
            }
        }

        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("8");
        this.clarificationTaskRequest.setRequestId(this.originatingTask.getRequestId());
        this.clarificationTaskRequest.setRequestModuleId(this.originatingTask.getRequestModuleId());
        this.clarificationTaskRequest.setTaskType("CLARIFY");
        this.clarificationTaskRequest.setStatus("NSTART");
        this.clarificationTaskRequest.setParentTask(this.originatingTask.getTaskId());
        this.clarificationTaskRequest.setPriority("HIGH");
        tasksRequest.setTaskManagerModel(clarificationTaskRequest);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            try {
//                FacesContext.getCurrentInstance().getExternalContext().redirect("taskDetails.xhtml?recordId="
//                        + originatingTask.getTaskId() + "&" + "moduleId=" + "34");
                FacesContext.getCurrentInstance().getExternalContext().redirect("tasksDashboard.xhtml?origin_page=" + originatingTaskPage);
            }
            catch (IOException ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void sendBackRequest() {
        TasksRequest tasksRequest = new TasksRequest();
        if (this.originatingTask.getTaskType().equals("CLARIFY")) {
            tasksRequest.setRequestActionType("9");
        }
        else {
            tasksRequest.setRequestActionType("3");
        }
        this.originatingTask.setTaskResponse("SEND_BACK");
        this.originatingTask.setStatus("DONE");
        tasksRequest.setTaskManagerModel(originatingTask);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            try {
//                FacesContext.getCurrentInstance().getExternalContext().redirect("taskDetails.xhtml?recordId="
//                        + originatingTask.getTaskId() + "&" + "moduleId=" + "34");
                FacesContext.getCurrentInstance().getExternalContext().redirect("tasksDashboard.xhtml?origin_page=" + originatingTaskPage);
            }
            catch (IOException ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void goToRelationField(ModuleFieldModel relationField) {
        if (relationField != null && relationField.getModuleId() != 0) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                        + relationField.getCurrentValue() + "&" + "moduleId=" + relationField.getModuleId());
            }
            catch (IOException ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void loadRecordHistory() {
    }

    public void returnToOriginalRecord() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                    + super.getRequestParams().get("masterRec") + "&" + "moduleId=" + super.getRequestParams().get("masterModule"));
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void downloadUploadTemplate(String targetModuleId) {
        DataBean db = this.relationalBeanMap.get(Integer.valueOf(targetModuleId));
        if (db.getFieldsList() == null) {
            db.loadFieldSet(targetModuleId, null);
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(baos));
        int index = 0;
        for (ModuleFieldModel mfm : db.getFieldsList()) {
            try {
                if (index == 0) {
                    if (mfm.isIsKey()) {
                        continue;
                    }
                    else if (mfm.isChronicalField()) {
                        continue;
                    }
                    else {
                        writer.append(mfm.getFieldName());
                    }
                }
                else {
                    if (mfm.isIsKey()) {
                        continue;
                    }
                    else if (mfm.isChronicalField()) {
                        continue;
                    }
                    else {
                        writer.append(",");
                        writer.append(mfm.getFieldName());
                    }
                }
            }
            catch (IOException ex) {
                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
            index++;
        }

        try {
            writer.close();
            byte[] bytes = baos.toByteArray();
            StreamedContent targetUploadTemplate = DefaultStreamedContent.builder()
                    .name("template.csv")
                    .contentType("text/csv")
                    .stream(() -> new ByteArrayInputStream(
                    bytes))
                    .build();
            super.setUploadTemplate(targetUploadTemplate);
//            this.uploadTemplate = new DefaultStreamedContent(new ByteArrayInputStream(bytes), "text/csv", "template.csv");
        }
        catch (IOException ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void showRichTextContent(String fieldName) {
        ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(recordModel, fieldName);
        if (mfm != null) {
            this.chosenRichTextField = mfm.getCurrentValue();
            PrimeFaces.current().ajax().update("richTextContentHolderDialog");
            PrimeFaces.current().executeScript("PF('richTextContentHolderDialog').show()");
        }
    }

    public void onTabSelect(int slectedTabModuleId) {
        customTabActive = false;
        this.selectedTabModule = slectedTabModuleId;
        switch (slectedTabModuleId) {
            case 0:
                break;
            case 1:
                notesBean.setRequestedModuleId(super.getModuleId());
                notesBean.setRequestedObjectId(this.requestedId);
                notesBean.init();
                break;
            case 2:
                contentLibraryBean.setRequestedModule(super.getModuleId());
                contentLibraryBean.setRequestedObjectId(this.requestedId);
                contentLibraryBean.init();
                break;
            default:
                DataBean temp = this.relationalBeanMap.get(selectedTabModule);
                temp.init();
                String qualifiedColumnName = this.relationalTableMap.get(selectedTabModule) + "." + this.relationalKeyMap.get(selectedTabModule);
                String keyValue = GeneralUtils.getFieldFromRecord(recordModel, this.relationalMap.get(selectedTabModule).getDetailColumn()).getCurrentValue();
                temp.loadRelatedRecord(qualifiedColumnName, keyValue);
//        temp.loadFilters(String.valueOf(selectedTabModule));
                temp.setKeyName(qualifiedColumnName);
                temp.setKeyValue(keyValue);
                temp.loadDataViewSettings("DETAIL");
                this.relationalBeanMap.put(selectedTabModule, temp);
        }

    }

    @Override
    public void handleFileUpload(FileUploadEvent event) {
        int relModuleId = (int) event.getComponent().getAttributes().get("relModuleId");

        UnstructuredModel cm = new UnstructuredModel();
        cm.setFileExtenstion(GeneralUtils.getFileExtenstion(event.getFile().getFileName()));
        cm.setFileName(event.getFile().getFileName());
        cm.setFileSize(event.getFile().getSize());
        cm.setContentStream(event.getFile().getContent());

        RequestModel request = new RequestModel();
        request.setRequestingModule(String.valueOf(relModuleId));
        request.setUnstructuredData(cm);
        request.setRequestActionType("5");
        request.setInternalBulkUpload(true);
        request.setInternalBulkUploadMasterKeyName(this.getKeyField());
        request.setInternalBulkUploadMasterKeyValue(this.getRequestedId());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = requestHandler.executeRequest(request);
        if (response.getErrorCode() == 1000) {
            FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", event.getFile().getFileName() + " Failed to upload");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        try {
            if (this.approval && super.getRequestParams().get("TaskId") != null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?recordId="
                        + super.getRecordModel().getFieldValueMap().get(super.getKeyField()).getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                        + Defines.REQUEST_MODE_EDIT + "&moduleId=" + super.getModuleId() + "&TaskId=" + super.getRequestParams().get("TaskId"));
            }
            else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?recordId="
                        + super.getRecordModel().getFieldValueMap().get(super.getKeyField()).getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                        + Defines.REQUEST_MODE_EDIT + "&moduleId=" + super.getModuleId());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
//        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onDynoButtonClick(int layoutId) {
        super.onDynoButtonClick(layoutId);

        FieldsLayoutModel flm = super.getAdvancedConfigMap().get(layoutId);
        if (flm != null) {
            switch (flm.getDynoButtonConfiguration().getActionType()) {
                case "RUN_WF":
                    this.loadRecord();
                    this.loadSmartViewConfigurations();
                    PrimeFaces.current().ajax().update("recordHeaderCardPanel");
                    PrimeFaces.current().ajax().update("tabHolder");
                    break;
            }
        }
    }

    public void addAnotherRecord() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?moduleId=" + super.getModuleId());
        }
        catch (IOException ex) {
            Logger.getLogger(DataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void returnToAllRecords() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("data.xhtml?moduleId=" + super.getModuleId());
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public LinkedHashMap<Integer, RelationManagerModel> getRelationalMap() {
        return relationalMap;
    }

    public void setRelationalMap(LinkedHashMap<Integer, RelationManagerModel> relationalMap) {
        this.relationalMap = relationalMap;
    }

    public LinkedHashMap<Integer, DataBean> getRelationalBeanMap() {
        return relationalBeanMap;
    }

    public void setRelationalBeanMap(LinkedHashMap<Integer, DataBean> relationalBeanMap) {
        this.relationalBeanMap = relationalBeanMap;
    }

    public LinkedHashMap<Integer, String> getRelationalKeyMap() {
        return relationalKeyMap;
    }

    public void setRelationalKeyMap(LinkedHashMap<Integer, String> relationalKeyMap) {
        this.relationalKeyMap = relationalKeyMap;
    }

    public LinkedHashMap<Integer, String> getRelationalTableMap() {
        return relationalTableMap;
    }

    public void setRelationalTableMap(LinkedHashMap<Integer, String> relationalTableMap) {
        this.relationalTableMap = relationalTableMap;
    }

    public String getRequestedId() {
        return requestedId;
    }

    public void setRequestedId(String requestedId) {
        this.requestedId = requestedId;
    }

    public NotesBean getNotesBean() {
        return notesBean;
    }

    public void setNotesBean(NotesBean notesBean) {
        this.notesBean = notesBean;
    }

    public SmartViewModel getSmartViewModel() {
        return smartViewModel;
    }

    public void setSmartViewModel(SmartViewModel smartViewModel) {
        this.smartViewModel = smartViewModel;
    }

    public ContentLibraryBean getContentLibraryBean() {
        return contentLibraryBean;
    }

    public void setContentLibraryBean(ContentLibraryBean contentLibraryBean) {
        this.contentLibraryBean = contentLibraryBean;
    }

    public boolean isApproval() {
        return approval;
    }

    public void setApproval(boolean approval) {
        this.approval = approval;
    }

    public TaskManagerModel getOriginatingTask() {
        return originatingTask;
    }

    public void setOriginatingTask(TaskManagerModel originatingTask) {
        this.originatingTask = originatingTask;
    }

    public UnstructuredModel getHeaderDynamicContent() {
        return headerDynamicContent;
    }

    public void setHeaderDynamicContent(UnstructuredModel headerDynamicContent) {
        this.headerDynamicContent = headerDynamicContent;
    }

    public ModuleModel getActiveModule() {
        return activeModule;
    }

    public void setActiveModule(ModuleModel activeModule) {
        this.activeModule = activeModule;
    }

    public TaskManagerModel getClarificationTaskRequest() {
        return clarificationTaskRequest;
    }

    public void setClarificationTaskRequest(TaskManagerModel clarificationTaskRequest) {
        this.clarificationTaskRequest = clarificationTaskRequest;
    }

    public ArrayList<UserModel> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<UserModel> users) {
        this.users = users;
    }

    public ArrayList<UserModel> getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(ArrayList<UserModel> selectedUsers) {
        this.selectedUsers = selectedUsers;
    }

    public boolean isOriginatedFromSmartView() {
        return originatedFromSmartView;
    }

    public void setOriginatedFromSmartView(boolean originatedFromSmartView) {
        this.originatedFromSmartView = originatedFromSmartView;
    }

    public String getChosenRichTextField() {
        return chosenRichTextField;
    }

    public void setChosenRichTextField(String chosenRichTextField) {
        this.chosenRichTextField = chosenRichTextField;
    }

    public int getSelectedTabModule() {
        return selectedTabModule;
    }

    public void setSelectedTabModule(int selectedTabModule) {
        this.selectedTabModule = selectedTabModule;
    }

    public boolean isCustomTabActive() {
        return customTabActive;
    }

    public void setCustomTabActive(boolean customTabActive) {
        this.customTabActive = customTabActive;
    }

    public TabsConfig getActiveCustomTab() {
        return activeCustomTab;
    }

    public void setActiveCustomTab(TabsConfig activeCustomTab) {
        this.activeCustomTab = activeCustomTab;
    }

    public ReportViewer getReportViewer() {
        return reportViewer;
    }

    public void setReportViewer(ReportViewer reportViewer) {
        this.reportViewer = reportViewer;
    }

}
