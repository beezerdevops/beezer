/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.templates;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.content.ContentLibraryBean;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.ProcessingHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.utils.GeneralUtils;
import com.beezl.interpreter.BEEZLInterperter;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.components.TableComponent;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author badry
 */
@ManagedBean(name = "addDataBean")
@ViewScoped
public class AddDataBean extends BeanFramework {

    private boolean quickEdit;
    private boolean originatedFromSmartView;
    private String saveError;
    
    @ManagedProperty(value = "#{contentLibraryBean}")
    private ContentLibraryBean contentLibraryBean;

    public AddDataBean(String dataDialogWidgetVar) {
        super(dataDialogWidgetVar, "ADD_DATA_BEAN");
    }

    public AddDataBean() {
    }

    @PostConstruct
    public void init() {
        super.BEAN_TYPE = "ADD_DATA_BEAN";

        if (super.getRequestParams().get("masterRec") != null && !super.getRequestParams().get("masterRec").isEmpty()
                && super.getRequestParams().get("masterModule") != null && !super.getRequestParams().get("masterModule").isEmpty()) {
            this.originatedFromSmartView = true;
        }

        if (super.getModuleId() != null) {
            super.setMasterPageName("data.xhtml?moduleId=" + super.getModuleId());
             if (super.getRequestParams().get(Defines.REQUEST_MODE_KEY) != null
                && super.getRequestParams().get(Defines.REQUEST_MODE_KEY).equals(Defines.REQUEST_MODE_EDIT)){
                  this.loadRecord();
             }
            super.loadForms(super.getModuleId(), 0);
            super.setMode();
            contentLibraryBean = new ContentLibraryBean();
            contentLibraryBean.setRequestedModule(super.getModuleId());
            contentLibraryBean.setRequestedObjectId(super.getRequestParams().get("recordId"));
            contentLibraryBean.init();
            ModuleModel mm = super.loadModule(super.getModuleId());
            if (mm != null) {
                super.setModuleBreadCrumbName(mm.getModuleName());
            }
        }
    }

    public boolean validateTable(TableComponent tableConfig, ArrayList<RecordModel> recList) {
        if (tableConfig != null) {
            if (tableConfig.isEnableRowValidation() && tableConfig.getValidationRule() != null && !tableConfig.getValidationRule().isEmpty() && recList != null && !recList.isEmpty()) {
                for (RecordModel rm : recList) {
                    BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                    beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                    beezlInterperter.setRecodeModel(rm);
                    beezlInterperter.addDynamicField("validation_result", tableConfig.getValidationRule());
                    try {
                        LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                        if (dynamicFieldMap.get("validation_result") != null && !dynamicFieldMap.get("validation_result").isEmpty()) {
                            if (!Boolean.valueOf(dynamicFieldMap.get("validation_result"))) {
                                return false;
                            }
                        }
                        else {
                            return false;
                        }
                    }
                    catch (Exception ex) {
                        Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
                        return false;
                    }
                }
                return true;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }

    @Override
    public void save() {
        if (!super.validateFields()) {
            return;
        }
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                this.edit();
                break;
            case Defines.REQUEST_MODE_ADD:
                super.populateDataFromForm();
                RequestModel request = new RequestModel();
                request.setRequestingModule(super.getModuleId());
                request.setRecordModel(super.recordModel);
                request.setRequestActionType("1");
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel response;
                try {
                    boolean runPreProcessForSubRecords = false;
                    ArrayList<RecordModel> recList = new ArrayList<>();
                    TableComponent tableConfig = null;
                    String subRecordsModule = "0";
                    if (super.getAdvancedConfigMap() != null && !super.getAdvancedConfigMap().isEmpty()) {
                        for (Map.Entry<Integer, FieldsLayoutModel> entrySet : super.getAdvancedConfigMap().entrySet()) {
                            FieldsLayoutModel flm = entrySet.getValue();
                            if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                                if (flm.getTableComponentConfiguration().isProcessData()) {
                                    request.setHoldProcessRun(true);
                                    runPreProcessForSubRecords = true;
                                    tableConfig = flm.getTableComponentConfiguration();
                                    recList = super.getTableRecordModelMap().get(entrySet.getKey()).getData();
                                    subRecordsModule = flm.getTableComponentConfiguration().getBindingModelId();
                                }
                            }
                        }
                    }

                    if (runPreProcessForSubRecords && recList != null && !recList.isEmpty() && subRecordsModule != null && !subRecordsModule.isEmpty() && !subRecordsModule.equals("0")) {
                        if (this.validateTable(tableConfig, recList)) {
                            RequestModel preProcessRequest = new RequestModel();
                            preProcessRequest.setRequestActionType("1");
                            preProcessRequest.setBulkRecords(recList);
                            preProcessRequest.setRequestingModule(subRecordsModule);
                            ProcessingHandler preProcessHandler = new ProcessingHandler();
                            ResponseModel preProcessResponse = preProcessHandler.executeRequest(preProcessRequest);
                            if (preProcessResponse.getErrorCode() == 1000) {
                                response = requestHandler.executeRequest(request, "GenericMasterAPI");
                                if (response.getErrorCode() == 1000) {
                                    ModuleFieldModel keyField = GeneralUtils.getKeyFieldFromRecord(super.recordModel);
                                    if (keyField != null) {
                                        super.recordModel.getFieldValueMap().get(keyField.getFieldName()).setCurrentValue(String.valueOf(response.getObjectId()));
                                    }
                                    else {
                                        keyField = GeneralUtils.getKeyFieldType(Integer.valueOf(super.getModuleId()));
                                        keyField.setCurrentValue(String.valueOf(response.getObjectId()));
                                        super.recordModel.getFieldValueMap().put(keyField.getFieldName(), keyField);
                                    }
                                    this.saveFormTableRecords(response, "1", request);
                                }
                                else {
                                    this.saveError = response.getErrorMessage();
                                    this.updateMessage(response.getErrorCode(), response.getErrorMessage());
                                }
                            }
                            else {
                                this.saveError = preProcessResponse.getErrorMessage();
                                this.updateMessage(preProcessResponse.getErrorCode(), preProcessResponse.getErrorMessage());
                            }
                        }
                        else {
                            this.saveError = tableConfig.getErrorMessage();
                            this.updateMessage(2056, tableConfig.getErrorMessage());
                        }
                    }
                    else {
                        response = requestHandler.executeRequest(request, "GenericMasterAPI");
                        if (response.getErrorCode() == 1000) {
                            ModuleFieldModel keyField = GeneralUtils.getKeyFieldFromRecord(super.recordModel);
                            if (keyField != null) {
                                super.recordModel.getFieldValueMap().get(keyField.getFieldName()).setCurrentValue(String.valueOf(response.getObjectId()));
                            }
                            else {
                                keyField = GeneralUtils.getKeyFieldType(Integer.valueOf(super.getModuleId()));
                                keyField.setCurrentValue(String.valueOf(response.getObjectId()));
                                super.recordModel.getFieldValueMap().put(keyField.getFieldName(), keyField);
                            }
                            this.saveFormTableRecords(response, "1", request);
                        }
                        else {
                            if (response.getErrorCode() == 2000) {
                                PrimeFaces.current().executeScript("PF('creditWarningDialog').show()");
                            }
                            else {
                                this.saveError = response.getErrorMessage();
                                this.updateMessage(2056, response.getErrorMessage());
                            }
                        }
                    }

                    ///////////////////////////////////////////////////////
                    //hold post process run if there are sub records
//                    if (super.getAdvancedConfigMap() != null && !super.getAdvancedConfigMap().isEmpty()) {
//                        for (Map.Entry<Integer, FieldsLayoutModel> entrySet : super.getAdvancedConfigMap().entrySet()) {
//                            FieldsLayoutModel flm = entrySet.getValue();
//                            if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
//                                if (flm.getTableComponentConfiguration().isProcessData()) {
//                                    request.setHoldProcessRun(true);
//                                }
//                            }
//                        }
//                    }
//
//                    response = requestHandler.executeRequest(request, "GenericMasterAPI");
//                    if (response.getErrorCode() == 1000) {
//                        ModuleFieldModel keyField = GeneralUtils.getKeyFieldFromRecord(super.recordModel);
//                        if (keyField != null) {
//                            super.recordModel.getFieldValueMap().get(keyField.getFieldName()).setCurrentValue(String.valueOf(response.getObjectId()));
//                        }
//                        else {
//                            keyField = GeneralUtils.getKeyFieldType(Integer.valueOf(super.getModuleId()));
//                            keyField.setCurrentValue(String.valueOf(response.getObjectId()));
//                            super.recordModel.getFieldValueMap().put(keyField.getFieldName(), keyField);
//                        }
//                        this.saveFormTableRecords(response, "1", request);
//                    }
//                    else {
//                        this.updateMessage(response.getErrorCode(), response.getErrorMessage());
//                    }
                }
                catch (Exception ex) {
                    this.saveError = ex.getMessage();
                    this.updateMessage(2056, ex.getMessage());
                    Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
    }

    public void saveFormTableRecords(ResponseModel response, String actionType, RequestModel originalRequest) {
        boolean runContinueProcessScan = false;
        if (super.getAdvancedConfigMap() != null && !super.getAdvancedConfigMap().isEmpty()) {
            for (Map.Entry<Integer, FieldsLayoutModel> entrySet : super.getAdvancedConfigMap().entrySet()) {
                FieldsLayoutModel flm = entrySet.getValue();
                if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                    if (flm.getTableComponentConfiguration().isProcessData()) {
                        runContinueProcessScan = true;
                        ArrayList<RecordModel> recordsToUpdate = new ArrayList<>();
                        ArrayList<RecordModel> recordsToAdd = new ArrayList<>();
//                        ArrayList<RecordModel> recList = super.getTableRecordListMap().get(entrySet.getKey());
                        ArrayList<RecordModel> recList;
                        /**
                         * 21/01/2022: Add Support for Pivot tables
                         */
                        if (flm.getTableComponentConfiguration().isPivotViewActive()) {
                            recList = super.getTableHelpersMap().get(flm.getLayoutId()).buildRecordList(super.getTableRecordModelMap().get(entrySet.getKey()).getData());
                        }
                        else {
                            recList = super.getTableRecordModelMap().get(entrySet.getKey()).getData();
                        }
                        /**
                         * ***************************************************************************************
                         */

                        if (recList != null && !recList.isEmpty()) {
                            for (RecordModel rm : recList) {
                                if (actionType.equals("1")) {
                                    if (rm.getFieldValueMap().get(flm.getTableComponentConfiguration().getKeyFieldName()) != null) {
                                        rm.getFieldValueMap().get(flm.getTableComponentConfiguration().getKeyFieldName()).setCurrentValue(String.valueOf(response.getObjectId()));
                                    }
                                    else {
                                        ModuleFieldModel keyField = GeneralUtils.getFieldType(flm.getTableComponentConfiguration().getKeyFieldName(), Integer.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));
                                        rm.getFieldValueMap().put(keyField.getFieldName(), keyField);
                                        rm.getFieldValueMap().get(flm.getTableComponentConfiguration().getKeyFieldName()).setCurrentValue(String.valueOf(response.getObjectId()));
                                    }
                                }
                                else if (actionType.equals("3")) {
                                    if (rm.getFieldValueMap().get(flm.getTableComponentConfiguration().getKeyFieldName()) != null) {
                                        rm.getFieldValueMap().get(flm.getTableComponentConfiguration().getKeyFieldName()).setCurrentValue(GeneralUtils.getKeyFieldFromRecord(super.getRecordModel()).getCurrentValue());
                                    }
                                    else {
                                        ModuleFieldModel keyField = GeneralUtils.getFieldType(flm.getTableComponentConfiguration().getKeyFieldName(), Integer.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));
                                        rm.getFieldValueMap().put(keyField.getFieldName(), keyField);
                                        rm.getFieldValueMap().get(flm.getTableComponentConfiguration().getKeyFieldName()).setCurrentValue(GeneralUtils.getKeyFieldFromRecord(super.getRecordModel()).getCurrentValue());
                                    }
                                }

                                ModuleFieldModel recordKeyField = GeneralUtils.getKeyFieldFromRecord(rm);
                                if (recordKeyField == null) {
                                    recordKeyField = GeneralUtils.getKeyFieldType(Integer.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));
                                }

                                if (recordKeyField.getCurrentValue() != null && !recordKeyField.getCurrentValue().isEmpty()) {
//                                        recordsToUpdate.add(rm);
                                    RequestModel requestModel = new RequestModel();
                                    requestModel.setRecordModel(rm);
                                    requestModel.setRequestActionType("3");
                                    requestModel.setRequestingModule(flm.getTableComponentConfiguration().getBindingModelId());
                                    RequestHandler handler = new RequestHandler();
                                    ResponseModel responseModel = handler.executeRequest(requestModel);
                                }
                                else {
//                                        recordsToAdd.add(rm);
                                    RequestModel requestModel = new RequestModel();
                                    requestModel.setRecordModel(rm);
                                    requestModel.setRequestActionType("1");
                                    requestModel.setRequestingModule(flm.getTableComponentConfiguration().getBindingModelId());
                                    RequestHandler handler = new RequestHandler();
                                    ResponseModel responseModel = handler.executeRequest(requestModel);
                                }
                            }

//                            RequestModel requestModel = new RequestModel();
//                            requestModel.setBulkRecords(recList);
//                            requestModel.setRequestActionType(actionType);
//                            requestModel.setRequestingModule(flm.getTableComponentConfiguration().getBindingModelId());
//                            RequestHandler handler = new RequestHandler();
//                            ResponseModel responseModel = handler.executeRequest(requestModel);
//                            this.updateMessage(responseModel.getErrorCode(), responseModel.getErrorMessage());
                        }
//                        RequestHandler handler = new RequestHandler();
//                        originalRequest.setContinueProcessScan(true);
//                        ResponseModel responseModel = handler.executeRequest(originalRequest);
                    }
                }
            }
        }
        if (runContinueProcessScan) {
            RequestHandler handler = new RequestHandler();
            originalRequest.setContinueProcessScan(true);
            ResponseModel responseModel = handler.executeRequest(originalRequest);
        }
        if (!quickEdit) {
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
    }

    public boolean isFormContainTableRecords() {
        if (super.getAdvancedConfigMap() != null && !super.getAdvancedConfigMap().isEmpty()) {
            for (Map.Entry<Integer, FieldsLayoutModel> entrySet : super.getAdvancedConfigMap().entrySet()) {
                FieldsLayoutModel flm = entrySet.getValue();
                if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                    if (flm.getTableComponentConfiguration().isProcessData()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void edit() {
        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("3");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            boolean runPreProcessForSubRecords = false;
            ArrayList<RecordModel> recList = new ArrayList<>();
            TableComponent tableConfig = null;
            String subRecordsModule = "0";
            if (super.getAdvancedConfigMap() != null && !super.getAdvancedConfigMap().isEmpty()) {
                for (Map.Entry<Integer, FieldsLayoutModel> entrySet : super.getAdvancedConfigMap().entrySet()) {
                    FieldsLayoutModel flm = entrySet.getValue();
                    if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                        if (flm.getTableComponentConfiguration().isProcessData()) {
                            request.setHoldProcessRun(true);
                            runPreProcessForSubRecords = true;
                            tableConfig = flm.getTableComponentConfiguration();
                            recList = super.getTableRecordModelMap().get(entrySet.getKey()).getData();
                            subRecordsModule = flm.getTableComponentConfiguration().getBindingModelId();
                        }
                    }
                }
            }

            if (runPreProcessForSubRecords && recList != null && !recList.isEmpty() && subRecordsModule != null && !subRecordsModule.isEmpty() && !subRecordsModule.equals("0")) {
                if (this.validateTable(tableConfig, recList)) {
                    RequestModel preProcessRequest = new RequestModel();
                    preProcessRequest.setRequestActionType("3");
                    preProcessRequest.setBulkRecords(recList);
                    preProcessRequest.setRequestingModule(subRecordsModule);
                    ProcessingHandler preProcessHandler = new ProcessingHandler();
                    ResponseModel preProcessResponse = preProcessHandler.executeRequest(preProcessRequest);
                    if (preProcessResponse.getErrorCode() == 1000) {
                        response = requestHandler.executeRequest(request, "GenericMasterAPI");
                        if (response.getErrorCode() == 1000) {
                            this.saveFormTableRecords(response, "3", request);
                        }
                    }
                    else {
                        this.updateMessage(preProcessResponse.getErrorCode(), preProcessResponse.getErrorMessage());
//                        FacesContext.getCurrentInstance().getMessageList().get(0).
                    }
                }
                else {
                    this.updateMessage(2056, tableConfig.getErrorMessage());
                }

            }
            else {
                response = requestHandler.executeRequest(request, "GenericMasterAPI");
                if (response.getErrorCode() == 1000) {
                    this.saveFormTableRecords(response, "3", request);
                }
                else {
                    this.updateMessage(2056, response.getErrorMessage());
                }
            }

//            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void cancel() {
        try {
            if (this.originatedFromSmartView) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                        + super.getRequestParams().get("masterRec") + "&" + "moduleId=" + super.getRequestParams().get("masterModule"));
            }
            else {
                FacesContext.getCurrentInstance().getExternalContext().redirect("data.xhtml?moduleId=" + super.getModuleId());
            }
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void returnToOriginalRecord() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                    + super.getRequestParams().get("masterRec") + "&" + "moduleId=" + super.getRequestParams().get("masterModule"));
            FacesContext.getCurrentInstance().responseComplete();
        }
        catch (IOException ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        request.setRequestContext("ADD_DATA");
        ModuleModel mm = super.loadModule(super.getModuleId());
        super.setModuleBreadCrumbName(mm.getModuleName());
        String moduleName = "";
        if (mm != null) {
            moduleName = mm.getModuleBaseTable() + ".";
        }
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(moduleName + this.getKeyField(), Integer.valueOf(super.getRequestParams().get("recordId"))));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.recordModel = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isQuickEdit() {
        return quickEdit;
    }

    public void setQuickEdit(boolean quickEdit) {
        this.quickEdit = quickEdit;
    }

    public boolean isOriginatedFromSmartView() {
        return originatedFromSmartView;
    }

    public void setOriginatedFromSmartView(boolean originatedFromSmartView) {
        this.originatedFromSmartView = originatedFromSmartView;
    }

    public String getSaveError() {
        return saveError;
    }

    public void setSaveError(String saveError) {
        this.saveError = saveError;
    }

    public ContentLibraryBean getContentLibraryBean() {
        return contentLibraryBean;
    }

    public void setContentLibraryBean(ContentLibraryBean contentLibraryBean) {
        this.contentLibraryBean = contentLibraryBean;
    }

}
