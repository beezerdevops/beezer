/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.handler.LookupHandler;
import com.beezer.web.handler.ModuleHandler;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.lookup.LookupManagerModel;
import com.crm.models.requests.managers.LookupRequest;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.responses.managers.LookupResponse;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.extensions.event.CompleteEvent;

/**
 *
 * @author Ahmed El Badry Dec 24, 2019
 */
public class ExpressionCodeHelper {

    private ArrayList<ModuleModel> moduleList;
    private HashMap<String, ArrayList<ModuleFieldModel>> moduleFieldMap;
    private ArrayList<LookupManagerModel> lookupManagerList;

    public ExpressionCodeHelper() {
        this.moduleFieldMap = new HashMap<>();
        this.loadModuleList();
        this.loadLookupManagers();
    }

    public List<String> complete(final CompleteEvent event) {
        final ArrayList<String> suggestions = new ArrayList<>();
        //Check if searching for modules or fields
        // if context is null then we are searching for modules
        if (event.getContext().equals("null")) {
            if (event.getToken() != null && !event.getToken().isEmpty() && !event.getToken().equals("null")) {
                for (ModuleModel mm : moduleList) {
                    if (mm.getModuleBaseTable().contains(event.getToken().replaceAll("%24", ""))) {
                        suggestions.add(mm.getModuleBaseTable() );
                    }
                }

                for (LookupManagerModel lmm : lookupManagerList) {
                    if (lmm.getLookupTable().contains(event.getToken().replaceAll("%24", ""))) {
                        suggestions.add(lmm.getLookupTable());
                    }
                }
                suggestions.add("FunctionManager");
            }
        }
        //Search for fields of context
        else {
            if (event.getContext().equals("FunctionManager")) {
                suggestions.add("loadValueByClause(\"targetFieldName\", \"targetModule\", \"Clause\");");
                suggestions.add("loadRecordsByClause(\"targetModule\", \"Clause\");");
                suggestions.add("evaluateSumFunction(\"targetFieldName\", \"targetModule\", \"Clause\");");
                suggestions.add("evaluateAverageFunction(\"targetFieldName\", \"targetModule\", \"Clause\");");
                suggestions.add("evaluateCount( \"targetModule\", \"Clause\");");
                suggestions.add("getSubListFromContext(\"targetModule\",  \"listField\")");
                suggestions.add("resolveLookupValue(\"lookupTable\",  \"value\")");
                suggestions.add("getListFromContext(\"listName\")");
                suggestions.add("representNumberAsText(\"numberIn\")");
                suggestions.add("convertToDateFormat(\"dateIn\")");
                suggestions.add("representDecimalAsTextWithCurrency(\"numberIn\",\"currency1\",\"currency2\")");
                suggestions.add("convertToDateFormat(\"dateIn\")");
            }
            else {
                for (ModuleModel mm : moduleList) {
                    if (mm.getModuleBaseTable().equals(event.getContext().replaceAll("%24", ""))) {
                        if (moduleFieldMap.get(String.valueOf(mm.getModuleId())) != null) {
                            //If field list is empty load into cache
                            if (moduleFieldMap.get(String.valueOf(mm.getModuleId())).isEmpty()) {
                                this.loadFieldSet(String.valueOf(mm.getModuleId()));
                            }
                            for (ModuleFieldModel mfm : moduleFieldMap.get(String.valueOf(mm.getModuleId()))) {
                                suggestions.add(mfm.getFieldName() );
                            }
                        }
                    }
                }
            }

        }

        return suggestions;
    }

    private void loadModuleList() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.moduleList = response.getReturnList();
                for (ModuleModel mm : moduleList) {
                    this.moduleFieldMap.put(String.valueOf(mm.getModuleId()), new ArrayList<>());
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ExpressionCodeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadFieldSet(String moduleId) {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(moduleId);
            request.setActionType("0");
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                if (this.moduleFieldMap.get(moduleId) != null) {
                    this.moduleFieldMap.get(moduleId).addAll(response.getReturnList());
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ExpressionCodeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadLookupManagers() {
        try {
            LookupHandler lh = new LookupHandler();
            LookupRequest request = new LookupRequest();
            request.setActionType("0");
            LookupResponse response = lh.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.lookupManagerList = response.getManagerList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ExpressionCodeHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addModule(ModuleModel moduleModel) {
        if (moduleModel.getModuleBaseTable() == null || moduleModel.getModuleBaseTable().isEmpty()) {
            for (ModuleModel mm : moduleList) {
                if (mm.getModuleId() == moduleModel.getModuleId()) {
                    moduleModel.setModuleBaseTable(mm.getModuleBaseTable());
                    break;
                }
            }
        }
        this.moduleList.add(moduleModel);
        this.moduleFieldMap.put(String.valueOf(moduleModel.getModuleId()), new ArrayList<>());
    }

    public ArrayList<ModuleModel> getModuleList() {
        return moduleList;
    }

    public void setModuleList(ArrayList<ModuleModel> moduleList) {
        this.moduleList = moduleList;
    }

}
