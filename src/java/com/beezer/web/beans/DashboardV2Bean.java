/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.reports.ReportViewer;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.DashboardManagerHandler;
import com.beezer.web.utils.GeneralUtils;
import com.beezl.interpreter.BEEZLInterperter;
import com.crm.models.internal.dashboard.v2.DashboardModel;
import com.crm.models.internal.dashboard.v2.DashboardWidget;
import com.crm.models.internal.workflow.activities.DocTemplateParameter;
import com.crm.models.requests.managers.DashboardManagerRequest;
import com.crm.models.responses.managers.DashboardManagerResponse;
import freemarker.template.Configuration;
import freemarker.template.Template;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.primefaces.PrimeFaces;
//import org.primefaces.model.DashboardColumn;
//import org.primefaces.model.DefaultDashboardColumn;
//import org.primefaces.model.DefaultDashboardModel;

/**
 *
 * @author badry
 */
@ManagedBean(name = "dashboardV2Bean")
@ViewScoped
public class DashboardV2Bean extends BeanFramework {

//    private org.primefaces.model.DashboardModel dashboardModel;
    private ArrayList<DashboardModel> dashboardWidgetsList;

    @ManagedProperty(value = "#{reportViewer}")
    private ReportViewer reportsViewer;

    private LinkedHashMap<Integer, ReportViewer> reportViewerMap;
    private LinkedHashMap<Integer, String> customViewMap;

    private String customLinkStyling;
    private String customCssStyling;

    public DashboardV2Bean() {
    }

    @PostConstruct
    public void init() {
        this.customLinkStyling = "";
        this.customCssStyling = "";
        GeneralUtils.refreshMenuIconStyles();
        if (GeneralUtils.getActiveApplication() != null) {
            reportViewerMap = new LinkedHashMap<>();
            customViewMap = new LinkedHashMap<>();
            this.loadDashboardWidgets();
        }
    }

    public void initializeDashboardView() {
//        dashboardModel = new DefaultDashboardModel();
//        DashboardColumn column1 = new DefaultDashboardColumn();
//        if (dashboardWidgetsList != null) {
//            for (DashboardModel dm : dashboardWidgetsList) {
//                if (dm.getDashboardConfiguration() != null && dm.getDashboardConfiguration().getWidgetList() != null) {
//                    for (DashboardWidget dw : dm.getDashboardConfiguration().getWidgetList()) {
//                        column1.addWidget("id_" + dw.getWidgetId());
//                    }
//                }
//            }
//            dashboardModel.addColumn(column1);
//        }
    }

    public void loadDashboardWidgets() {
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        DashboardManagerRequest dashboardRequest = new DashboardManagerRequest();
        dashboardRequest.setRequestActionType("5");
        dashboardRequest.setAppId(GeneralUtils.getActiveApplication().getAppId());
        DashboardManagerHandler dashboardHandler = new DashboardManagerHandler();
        DashboardManagerResponse dashboardResponse = dashboardHandler.executeRequest(dashboardRequest);
        if (dashboardResponse.getErrorCode() == 1000) {
            this.dashboardWidgetsList = dashboardResponse.getReturnList();
        }
    }

    public void initializeWidgets() {
        if (dashboardWidgetsList != null) {
            for (DashboardModel dm : dashboardWidgetsList) {
                if (dm.getDashboardConfiguration() != null && dm.getDashboardConfiguration().getWidgetList() != null) {
                    for (DashboardWidget dw : dm.getDashboardConfiguration().getWidgetList()) {
                        switch (dw.getWidgetType()) {
                            case "REPORT":
                                ReportViewer reportViewer = new ReportViewer();
                                reportViewer.setReportId(dw.getReportWidgetConfig().getReportId());
                                reportViewer.init();
                                this.reportViewerMap.put(dw.getWidgetId(), reportViewer);
                                break;
                            case "CUSTOM":
                                this.evaluateCustomWidgets(dw);
                                break;
                        }
                    }
                }
            }
            this.initializeDashboardView();
        }
    }

    public void evaluateCustomWidgets(DashboardWidget dashboardWidget) {
        if (dashboardWidget.getCustomWidgetConfig() != null) {
            if (dashboardWidget.getCustomWidgetConfig().getHtmlContent() != null
                    && !dashboardWidget.getCustomWidgetConfig().getHtmlContent().isEmpty()
                    && dashboardWidget.getCustomWidgetConfig().getTemplateParameters() != null
                    && !dashboardWidget.getCustomWidgetConfig().getTemplateParameters().isEmpty()) {
                
                if(dashboardWidget.getCustomWidgetConfig().getLinkStyling() != null){
                    this.customLinkStyling = this.customLinkStyling + "\n" + dashboardWidget.getCustomWidgetConfig().getLinkStyling();
                }
                
                 if(dashboardWidget.getCustomWidgetConfig().getCssStyling()!= null){
                    this.customCssStyling = this.customCssStyling + "\n" + dashboardWidget.getCustomWidgetConfig().getCssStyling();
                }
                
                BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser()); 
                LinkedHashMap<String, String> bvm = null;

                Map<String, Object> configs = new HashMap<>();
                for (DocTemplateParameter dtp : dashboardWidget.getCustomWidgetConfig().getTemplateParameters()) {
                    switch (dtp.getType()) {
                        case "STRING":
                            beezlInterperter.addDynamicField(dtp.getKeyName(), dtp.getKeyValue());
                            break;
                        case "LIST":
                            break;
                    }
                }

                try {
                    bvm = beezlInterperter.evaluate();
                }
                catch (Exception ex) {
                    Logger.getLogger(DashboardV2Bean.class.getName()).log(Level.SEVERE, null, ex);
                }

                for (DocTemplateParameter dtp : dashboardWidget.getCustomWidgetConfig().getTemplateParameters()) {
                    if (bvm != null) {
                        if (!dtp.getType().equals("LIST")) {
                            configs.put(dtp.getKeyName(), bvm.get(dtp.getKeyName()));
                        }
                    }
                    else {
                        return;
                    }
                }

                StringWriter out = new StringWriter();
                Configuration cfg = new Configuration();
                cfg.setDefaultEncoding("UTF-8");
                Template t;
                try {
                    t = new Template("name", new StringReader(dashboardWidget.getCustomWidgetConfig().getHtmlContent()), cfg);
                    t.process(configs, out);
                }
                catch (Exception ex) {
                    Logger.getLogger(DashboardV2Bean.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
                String processedWidget = out.getBuffer().toString();

                this.customViewMap.put(dashboardWidget.getWidgetId(), processedWidget);
            }
        }
    }

//    public org.primefaces.model.DashboardModel getDashboardModel() {
//        return dashboardModel;
//    }
//
//    public void setDashboardModel(org.primefaces.model.DashboardModel dashboardModel) {
//        this.dashboardModel = dashboardModel;
//    }

    public ArrayList<DashboardModel> getDashboardWidgetsList() {
        return dashboardWidgetsList;
    }

    public void setDashboardWidgetsList(ArrayList<DashboardModel> dashboardWidgetsList) {
        this.dashboardWidgetsList = dashboardWidgetsList;
    }

    public LinkedHashMap<Integer, ReportViewer> getReportViewerMap() {
        return reportViewerMap;
    }

    public void setReportViewerMap(LinkedHashMap<Integer, ReportViewer> reportViewerMap) {
        this.reportViewerMap = reportViewerMap;
    }

    public ReportViewer getReportsViewer() {
        return reportsViewer;
    }

    public void setReportsViewer(ReportViewer reportsViewer) {
        this.reportsViewer = reportsViewer;
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public LinkedHashMap<Integer, String> getCustomViewMap() {
        return customViewMap;
    }

    public void setCustomViewMap(LinkedHashMap<Integer, String> customViewMap) {
        this.customViewMap = customViewMap;
    }

    public String getCustomLinkStyling() {
        return customLinkStyling;
    }

    public void setCustomLinkStyling(String customLinkStyling) {
        this.customLinkStyling = customLinkStyling;
    }

    public String getCustomCssStyling() {
        return customCssStyling;
    }

    public void setCustomCssStyling(String customCssStyling) {
        this.customCssStyling = customCssStyling;
    }

}
