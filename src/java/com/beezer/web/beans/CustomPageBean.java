/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.application.AppModuleConfigModel;
import com.crm.models.internal.application.ApplicationModel;
import java.util.ArrayList;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author badry
 */
@ManagedBean(name = "customPageBean")
@ViewScoped
public class CustomPageBean {

    private String appId;
    private String moduleId;
    private String targetUrl;

    @PostConstruct
    public void init() {
        this.getRequestParams();
        this.constructTargetUrl();
    }

    public void getRequestParams() {
        Map<String, String> requestParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        appId = requestParams.get("appId");
        moduleId = requestParams.get("moduleId");
    }

    public void constructTargetUrl() {
        if (appId != null && !appId.isEmpty() && moduleId != null && !moduleId.isEmpty()) {
            ApplicationModel activeApp = GeneralUtils.getActiveApplication();
            boolean killSearch = false;
            for (ArrayList<AppModuleConfigModel> value : activeApp.getModuleConfigMap().values()) {
                for (AppModuleConfigModel amcm : value) {
                    if (amcm.getAppId() == Integer.valueOf(appId) && amcm.getModuleId() == Integer.valueOf(moduleId)) {
                        targetUrl = amcm.getOutcome() + "?auid=" + GeneralUtils.getLoggedUser().getUserId();
                        killSearch = true;
                        break;
                    }
                }
                if (killSearch) {
                    break;
                }
            }
        }
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

}
