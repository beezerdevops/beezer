/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.published;

import com.beezer.web.beans.AccessBean;
import com.beezer.web.beans.IntegrationAccessBean;
import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.ApiKeyHandler;
import com.beezer.web.handler.PublishedComponentsHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.handler.UnstructuredHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.forms.FieldBlockModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.publishedComponents.PublishedRegistry;
import com.crm.models.requests.UnstructuredRequest;
import com.crm.models.requests.managers.APIManagerRequest;
import com.crm.models.requests.managers.PublishedRegistryRequest;
import com.crm.models.responses.UnstructuredResponse;
import com.crm.models.responses.managers.APIManagerResponse;
import com.crm.models.responses.managers.PublishedRegistryResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Ahmed El Badry Nov 13, 2021
 */
@ManagedBean(name = "publishedFormBean")
@ViewScoped
public class PublishedFormBean extends BeanFramework {

    private String formKey;
    private PublishedRegistry publishedRegistry;
    private String apiKey;
    private boolean notPublic = false;
    private String headerStyling = "";

    private String headerBackgroundImage = "";

    private boolean missingFormId = false;

    private boolean formSubmittedSuccessfully;

    @PostConstruct
    public void init() {
        this.getUrlParams();
        if (!missingFormId) {
            //        this.authenticateApiKey();
            this.loadRegisteredComponent();
            if (!this.notPublic) {
                super.loadForms(null, this.publishedRegistry.getComponentId(), this.apiKey);
                if (super.getDefaultForm() != null) {
                    super.setModuleId(String.valueOf(super.getDefaultForm().getModuleId()));

                    this.enforcePublicFormVisibility();

                    if (super.getDefaultForm().getPublicFormDesignModel() != null) {
                        if (super.getDefaultForm().getPublicFormDesignModel().isEnableHeaderMedia()
                                && super.getDefaultForm().getPublicFormDesignModel().getHeaderBackgroundMediaLink() != null
                                && !super.getDefaultForm().getPublicFormDesignModel().getHeaderBackgroundMediaLink().isEmpty()) {

                            UnstructuredRequest request = new UnstructuredRequest();
                            request.setRequestActionType("0");
                            request.setUnstructuredId(super.getDefaultForm().getPublicFormDesignModel().getHeaderBackgroundMediaLink());
                            request.setApiKey(apiKey);
                            UnstructuredHandler handler = new UnstructuredHandler();
                            UnstructuredResponse response = handler.executeForManager(request);
                            String link = "";
                            if (response.getErrorCode() == 1000) {
                                link = response.getReturnList().get(0).getLink();
                                headerBackgroundImage = link;
                            }

                            headerStyling = "background-image: " + "url(" + link + ");";
                            headerStyling = headerStyling + "background-size: 100% 100%;";
                        }
                        else {
                            headerStyling = "background: " + "#" + super.getDefaultForm().getPublicFormDesignModel().getHeaderColor();
                        }
                    }
                }
            }
        }

    }

    private void enforcePublicFormVisibility() {
        for (FieldBlockModel fbm : super.getDefaultForm().getBlockList()) {
            for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                if (flm.getPublicFormFieldVisibility() != null && !flm.getPublicFormFieldVisibility().isEmpty()) {
                    if (flm.getPublicFormFieldVisibility().equals("HIDE")) {
                        flm.setIsVisible(false);
                    }

                    if (flm.getPublicFormFieldVisibility().equals("DISABLE")) {
                        flm.setReadOnly(true);
                    }
                }
            }
        }
    }

    private void getUrlParams() {
        Map<String, String> params = new HashMap<>(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        if (params.get("FORM_KEY") != null) {
            this.formKey = params.get("FORM_KEY");
        }

        if (formKey == null || formKey.isEmpty()) {
            this.missingFormId = true;
        }

//        if (params.get("AAK") != null) {
//            this.apiKey = params.get("AAK");
//        }
    }

    public void loadRegisteredComponent() {
        if (!missingFormId) {
            PublishedRegistryRequest publishedRegistryRequest = new PublishedRegistryRequest();
            publishedRegistryRequest.setRequestActionType("0");
            publishedRegistryRequest.setPublishedId(formKey);
            PublishedComponentsHandler componentsHandler = new PublishedComponentsHandler();
            PublishedRegistryResponse registryResponse = componentsHandler.executeForManager(publishedRegistryRequest);
            if (registryResponse.getErrorCode() == 1000 && registryResponse.getReturnList() != null && !registryResponse.getReturnList().isEmpty()) {
                this.publishedRegistry = registryResponse.getReturnList().get(0);
                if (publishedRegistry.isPublished()) {
                    this.apiKey = publishedRegistry.getApiKey();
                    this.notPublic = false;
                    this.authenticateApiKey();
                }
                else {
                    this.notPublic = true;
                }
            }
        }
    }

    public void authenticateApiKey() {
        APIManagerRequest apimr = new APIManagerRequest();
        apimr.setRequestActionType("4");
        apimr.setApiKey(this.apiKey);
        ApiKeyHandler apiKeyHandler = new ApiKeyHandler();
        APIManagerResponse apiResponse = apiKeyHandler.executeRequest(apimr);
        if (apiResponse.getErrorCode() == 1000) {
            Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
            AccessBean accessBean = (AccessBean) sessionMap.get("accessBean");
            if (accessBean != null) {
                accessBean.setUserModel(apiResponse.getAccessProfile());
                sessionMap.put("accessBean", accessBean);
            }
            else {
                accessBean = new AccessBean();
                accessBean.setUserModel(apiResponse.getAccessProfile());
                sessionMap.put("accessBean", accessBean);
            }

//            IntegrationAccessBean integrationAccessBean = new IntegrationAccessBean();
//            integrationAccessBean.setUserModel(apiResponse.getAccessProfile());
//            sessionMap.put("integrationAccessBean", integrationAccessBean);
        }
    }

    @Override
    public void save() {
        if (!super.validateFields()) {
            return;
        }
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_EDIT:
                this.edit();
                break;
            case Defines.REQUEST_MODE_ADD:
                super.populateDataFromForm();
                RequestModel request = new RequestModel();
                request.setRequestingModule(super.getModuleId());
                request.setRecordModel(super.recordModel);
                request.setRequestActionType("1");
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel response;
                try {
                    response = requestHandler.executeRequest(request, "GenericMasterAPI");
                    if (response.getErrorCode() == 1000) {
                        ModuleFieldModel keyField = GeneralUtils.getKeyFieldFromRecord(super.recordModel);
                        if (keyField != null) {
                            super.recordModel.getFieldValueMap().get(keyField.getFieldName()).setCurrentValue(String.valueOf(response.getObjectId()));
                        }
                        else {
                            keyField = GeneralUtils.getKeyFieldType(Integer.valueOf(super.getModuleId()));
                            keyField.setCurrentValue(String.valueOf(response.getObjectId()));
                            super.recordModel.getFieldValueMap().put(keyField.getFieldName(), keyField);
                        }
                        this.saveFormTableRecords(response, "1", request);

//                        PrimeFaces.current().executeScript("PF('requestCompletedDialog').show()");
                        this.formSubmittedSuccessfully = true;
                    }
                    else {
//                        this.updateMessage(response.getErrorCode(), response.getErrorMessage());
                    }
                }
                catch (Exception ex) {
                    Logger.getLogger(PublishedFormBean.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
        }
    }

    public void saveFormTableRecords(ResponseModel response, String actionType, RequestModel originalRequest) {
        if (super.getAdvancedConfigMap() != null && !super.getAdvancedConfigMap().isEmpty()) {
            for (Map.Entry<Integer, FieldsLayoutModel> entrySet : super.getAdvancedConfigMap().entrySet()) {
                FieldsLayoutModel flm = entrySet.getValue();
                if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                    if (flm.getTableComponentConfiguration().isProcessData()) {
                        ArrayList<RecordModel> recordsToUpdate = new ArrayList<>();
                        ArrayList<RecordModel> recordsToAdd = new ArrayList<>();
//                        ArrayList<RecordModel> recList = super.getTableRecordListMap().get(entrySet.getKey());
                        ArrayList<RecordModel> recList = super.getTableRecordModelMap().get(entrySet.getKey()).getData();
                        if (recList != null && !recList.isEmpty()) {
                            for (RecordModel rm : recList) {
                                if (actionType.equals("1")) {
                                    rm.getFieldValueMap().get(flm.getTableComponentConfiguration().getKeyFieldName()).setCurrentValue(String.valueOf(response.getObjectId()));
                                }
                                else if (actionType.equals("3")) {
                                    rm.getFieldValueMap().get(flm.getTableComponentConfiguration().getKeyFieldName()).setCurrentValue(GeneralUtils.getKeyFieldFromRecord(super.getRecordModel()).getCurrentValue());
                                }

                                ModuleFieldModel recordKeyField = GeneralUtils.getKeyFieldFromRecord(rm);
                                if (recordKeyField != null) {
                                    if (recordKeyField.getCurrentValue() != null && !recordKeyField.getCurrentValue().isEmpty()) {
//                                        recordsToUpdate.add(rm);
                                        RequestModel requestModel = new RequestModel();
                                        requestModel.setRecordModel(rm);
                                        requestModel.setRequestActionType("3");
                                        requestModel.setRequestingModule(flm.getTableComponentConfiguration().getBindingModelId());
                                        RequestHandler handler = new RequestHandler();
                                        ResponseModel responseModel = handler.executeRequest(requestModel);
                                    }
                                    else {
//                                        recordsToAdd.add(rm);
                                        RequestModel requestModel = new RequestModel();
                                        requestModel.setRecordModel(rm);
                                        requestModel.setRequestActionType("1");
                                        requestModel.setRequestingModule(flm.getTableComponentConfiguration().getBindingModelId());
                                        RequestHandler handler = new RequestHandler();
                                        ResponseModel responseModel = handler.executeRequest(requestModel);
                                    }
                                }
                            }

                            RequestHandler handler = new RequestHandler();
                            originalRequest.setContinueProcessScan(true);
                            ResponseModel responseModel = handler.executeRequest(originalRequest);

//                            RequestModel requestModel = new RequestModel();
//                            requestModel.setBulkRecords(recList);
//                            requestModel.setRequestActionType(actionType);
//                            requestModel.setRequestingModule(flm.getTableComponentConfiguration().getBindingModelId());
//                            RequestHandler handler = new RequestHandler();
//                            ResponseModel responseModel = handler.executeRequest(requestModel);
//                            this.updateMessage(responseModel.getErrorCode(), responseModel.getErrorMessage());
                        }
                    }
                }
            }
        }
//        this.updateMessage(response.getErrorCode(), response.getErrorMessage());
    }

    public boolean isFormContainTableRecords() {
        if (super.getAdvancedConfigMap() != null && !super.getAdvancedConfigMap().isEmpty()) {
            for (Map.Entry<Integer, FieldsLayoutModel> entrySet : super.getAdvancedConfigMap().entrySet()) {
                FieldsLayoutModel flm = entrySet.getValue();
                if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                    if (flm.getTableComponentConfiguration().isProcessData()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean isNotPublic() {
        return notPublic;
    }

    public void setNotPublic(boolean notPublic) {
        this.notPublic = notPublic;
    }

    public String getHeaderStyling() {
        return headerStyling;
    }

    public void setHeaderStyling(String headerStyling) {
        this.headerStyling = headerStyling;
    }

    public boolean isMissingFormId() {
        return missingFormId;
    }

    public void setMissingFormId(boolean missingFormId) {
        this.missingFormId = missingFormId;
    }

    public boolean isFormSubmittedSuccessfully() {
        return formSubmittedSuccessfully;
    }

    public void setFormSubmittedSuccessfully(boolean formSubmittedSuccessfully) {
        this.formSubmittedSuccessfully = formSubmittedSuccessfully;
    }

    public String getHeaderBackgroundImage() {
        return headerBackgroundImage;
    }

    public void setHeaderBackgroundImage(String headerBackgroundImage) {
        this.headerBackgroundImage = headerBackgroundImage;
    }

}
