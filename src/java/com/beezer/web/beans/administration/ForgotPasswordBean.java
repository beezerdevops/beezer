/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.administration;

import com.beezer.web.handler.ForgotPasswordHandler;
import com.beezer.web.models.ActivationLinkModel;
import com.beezer.web.utils.Decryptor;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Ahmed El Badry Feb 17, 2023
 */
@ManagedBean(name = "forgotPasswordBean")
@ViewScoped
public class ForgotPasswordBean {

    private String forgotPasswordToken;
    private boolean tokenMissing;
    private boolean invalidToken;
    private ActivationLinkModel forgotPasswordTokenModel;
    private String userPassword;

    @PostConstruct
    public void init() {
        this.getUrlParams();
        this.decryptToken();
    }

    private void getUrlParams() {
        Map<String, String> params = new HashMap<>(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        if (params.get("access_key") != null) {
            this.forgotPasswordToken = params.get("access_key");
        }

        if (forgotPasswordToken == null || forgotPasswordToken.isEmpty()) {
            this.tokenMissing = true;
        }
    }

    private void decryptToken() {
        String serializedModel = Decryptor.decrypt(forgotPasswordToken);
        ObjectMapper mapper = new ObjectMapper();
        forgotPasswordTokenModel = null;
        try {
            forgotPasswordTokenModel = mapper.readValue(serializedModel, ActivationLinkModel.class);
        }
        catch (IOException ex) {
            Logger.getLogger(ForgotPasswordBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (forgotPasswordTokenModel == null) {
            this.invalidToken = false;
        }
    }

    public void submitNewPassword() {
        RequestModel requestModel = new RequestModel();
        requestModel.setRequestActionType("1");
        requestModel.setKeyObjectMap(new HashMap<>());
        requestModel.getKeyObjectMap().put("userId", this.forgotPasswordTokenModel.getUserId());
        requestModel.getKeyObjectMap().put("newPassword", userPassword);
        ForgotPasswordHandler forgotPasswordHandler = new ForgotPasswordHandler();
        ResponseModel responseModel = forgotPasswordHandler.executeForManager(requestModel);
        if (responseModel.getErrorCode() == 1000) {
            PrimeFaces.current().executeScript("PF('changeCompletedDialog').show()");
        }
        else {
            PrimeFaces.current().ajax().update("somethingWentWrongWarningDialog");
            PrimeFaces.current().executeScript("PF('somethingWentWrongWarningDialog').show()");
        }
    }

    public String getForgotPasswordToken() {
        return forgotPasswordToken;
    }

    public void setForgotPasswordToken(String forgotPasswordToken) {
        this.forgotPasswordToken = forgotPasswordToken;
    }

    public boolean isTokenMissing() {
        return tokenMissing;
    }

    public void setTokenMissing(boolean tokenMissing) {
        this.tokenMissing = tokenMissing;
    }

    public boolean isInvalidToken() {
        return invalidToken;
    }

    public void setInvalidToken(boolean invalidToken) {
        this.invalidToken = invalidToken;
    }

    public ActivationLinkModel getForgotPasswordTokenModel() {
        return forgotPasswordTokenModel;
    }

    public void setForgotPasswordTokenModel(ActivationLinkModel forgotPasswordTokenModel) {
        this.forgotPasswordTokenModel = forgotPasswordTokenModel;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

}
