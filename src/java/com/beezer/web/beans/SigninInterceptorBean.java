/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.handler.AccessHandler;
import com.beezer.web.handler.CompaniesHandler;
import com.beezer.web.handler.LocaleHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.handler.UserHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.CompanyModel;
import com.crm.models.global.UserModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.application.AppModuleConfigModel;
import com.crm.models.internal.application.ApplicationModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.localization.LocalizationHost;
import com.crm.models.internal.security.permissions.ObjectPermissionModel;
import com.crm.models.requests.CompanyRequest;
import com.crm.models.requests.UserRequest;
import com.crm.models.requests.managers.AccessRequest;
import com.crm.models.requests.managers.LocaleRequest;
import com.crm.models.responses.CompanyResponse;
import com.crm.models.responses.UserResponse;
import com.crm.models.responses.managers.AccessResponse;
import com.crm.models.responses.managers.LocaleResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author badry
 */
@ManagedBean(name = "signinInterceptorBean")
@SessionScoped
public class SigninInterceptorBean {

    private String dummyLablel;

//    @ManagedProperty(value = "#{accessBean}")
//    private AccessBean accessBean;

    @PostConstruct
    public void init() {
        HashMap<String, String> requestParams = new HashMap<>(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
         Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        AccessBean accessBean = (AccessBean) sessionMap.get("accessBean");
        
        accessBean.signIn(requestParams.get("username"), requestParams.get("password"));
    }

    public String getDummyLablel() {
        return dummyLablel;
    }

    public void setDummyLablel(String dummyLablel) {
        this.dummyLablel = dummyLablel;
    }

}
