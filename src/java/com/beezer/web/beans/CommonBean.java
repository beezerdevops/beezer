/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.commons.Defines;
import com.beezer.web.utils.GeneralUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author badry
 */
@ManagedBean(name = "commonBean")
@ViewScoped
public class CommonBean {

    public void showUserProfile() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("userProfile.xhtml?user="
                    + GeneralUtils.getLoggedUser().getUserId() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT);
        }
        catch (Exception ex) {
            Logger.getLogger(CommonBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getModuleIcon(String resource) {
        return GeneralUtils.generateDynamicResourceURL(resource);
    }
}
