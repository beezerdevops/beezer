/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.beans.modules.reports.ReportViewer;
import com.beezer.web.beans.modules.reports.ReportsBean;
import com.beezer.web.handler.DashboardHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.dashboard.DashboardConfig;
import com.crm.models.internal.dashboard.DashboardWidgetModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.managers.DashboardRequest;
import com.crm.models.responses.managers.DashboardResponse;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
//import org.primefaces.model.DashboardColumn;
//import org.primefaces.model.DefaultDashboardColumn;
//import org.primefaces.model.DefaultDashboardModel;

/**
 *
 * @author badry
 */
@ManagedBean(name = "dashboardBean")
@ViewScoped
public class DashboardBean extends BeanFramework {

    private Calendar calendar;

    private DashboardWidgetModel dashboardWidget;
//    private org.primefaces.model.DashboardModel dashboardModel;
    private ArrayList<DashboardWidgetModel> dashboardWidgetsList;

    @ManagedProperty(value = "#{reportsBean}")
    private ReportsBean reportsBean;

    @ManagedProperty(value = "#{reportViewer}")
    private ReportViewer reportsViewer;

    private LinkedHashMap<Integer, ReportViewer> reportViewerMap;

    public DashboardBean() {
    }

    @PostConstruct
    public void init() {
        GeneralUtils.refreshMenuIconStyles();
        calendar = Calendar.getInstance();
        this.dashboardWidget = new DashboardWidgetModel();
        if (GeneralUtils.getActiveApplication() != null) {
            this.dashboardWidget.setAppId(GeneralUtils.getActiveApplication().getAppId());
            this.dashboardWidget.setDashboardConfig(new DashboardConfig());
            reportViewerMap = new LinkedHashMap<>();
            this.loadDashboardWidgets();
        }
    }

    public void initializeDashboardView() {
//        dashboardModel = new DefaultDashboardModel();
//        DashboardColumn column1 = new DefaultDashboardColumn();
//        if (dashboardWidgetsList != null) {
//            for (DashboardWidgetModel dwm : dashboardWidgetsList) {
//                column1.addWidget("id_" + dwm.getId());
//            }
//            dashboardModel.addColumn(column1);
//        }
    }

    public void loadDashboardWidgets() {
//        RequestContext.getCurrentInstance().execute("PF('waitingDlgName').show()");
        PrimeFaces.current().executeScript("PF('waitingDlgName').show()");
        DashboardRequest dashboardRequest = new DashboardRequest();
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("user_id", "=", GeneralUtils.getLoggedUser().getUserId()));
        fm.setFilter("AND", new FilterType("app_id", "=", GeneralUtils.getActiveApplication().getAppId()));
        dashboardRequest.setClause(fm.getClause());
        dashboardRequest.setRequestActionType("0");
        DashboardHandler dashboardHandler = new DashboardHandler();
        DashboardResponse dashboardResponse = dashboardHandler.executeRequest(dashboardRequest);
        if (dashboardResponse.getErrorCode() == 1000) {
            this.dashboardWidgetsList = dashboardResponse.getDashboardList();
        }
    }

    public void initializeWidgets() {
        if (dashboardWidgetsList != null) {
            for (DashboardWidgetModel dwm : dashboardWidgetsList) {
                if (dwm.getWidgetType().equals("REPORT")) {
                    ReportViewer reportViewer = new ReportViewer();
                    reportViewer.setReportId(dwm.getDashboardConfig().getReportId());
                    reportViewer.init();
                    this.reportViewerMap.put(dwm.getId(), reportViewer);
                }
            }
            this.initializeDashboardView();
        }
    }

    public void onReportWidgetSelect() {
        dashboardWidget = new DashboardWidgetModel();
        dashboardWidget.setDashboardConfig(new DashboardConfig());
        dashboardWidget.setWidgetType("REPORT");
        reportsBean.loadRecordsListInstantly();
//        RequestContext.getCurrentInstance().update("addWidgetDialog");
//        RequestContext.getCurrentInstance().execute("PF('addWidgetDialog').show()");
        PrimeFaces.current().ajax().update("addWidgetDialog");
        PrimeFaces.current().executeScript("PF('addWidgetDialog').show()");
    }

    public void onReportSelect(String selectedReport) {
        for (RecordModel rm : this.reportsBean.getReportList()) {
            if (rm.getFieldValueMap().get("id").getCurrentValue().equals(selectedReport)) {
                dashboardWidget.getDashboardConfig().setReportType(rm.getFieldValueMap().get("report_type").getCurrentValue());
                dashboardWidget.getDashboardConfig().setReportGraphType(rm.getFieldValueMap().get("graph_type").getCurrentValue());
                break;
            }
        }
    }

    public void deleteWidget(DashboardWidgetModel widget) {
        DashboardRequest dashboardRequest = new DashboardRequest();
        dashboardRequest.setRequestActionType("2");
        dashboardRequest.setDashboardModel(widget);
        DashboardHandler dashboardHandler = new DashboardHandler();
        DashboardResponse dashboardResponse = dashboardHandler.executeRequest(dashboardRequest);
        if (dashboardResponse.getErrorCode() == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Dashboard Deleted"));
            dashboardWidgetsList.remove(widget);
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Dashboard Not Deleted"));
        }
    }

    public void handleTitleUpdate(DashboardWidgetModel widget) {
        DashboardRequest dashboardRequest = new DashboardRequest();
        dashboardRequest.setRequestActionType("3");
        dashboardRequest.setDashboardModel(widget);
        DashboardHandler dashboardHandler = new DashboardHandler();
        DashboardResponse dashboardResponse = dashboardHandler.executeRequest(dashboardRequest);
        if (dashboardResponse.getErrorCode() == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Dashboard updated"));
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Dashboard Not updated"));
        }
    }

    @Override
    public void save() {
        DashboardRequest dashboardRequest = new DashboardRequest();
        dashboardRequest.setRequestActionType("1");
        dashboardWidget.setAppId(GeneralUtils.getActiveApplication().getAppId());
        dashboardRequest.setDashboardModel(dashboardWidget);
        DashboardHandler dashboardHandler = new DashboardHandler();
        DashboardResponse dashboardResponse = dashboardHandler.executeRequest(dashboardRequest);
        if (dashboardResponse.getErrorCode() == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Dashboard Added"));
            this.dashboardWidgetsList.add(dashboardWidget);
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Dashboard Not Added"));
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public DashboardWidgetModel getDashboardWidget() {
        return dashboardWidget;
    }

    public void setDashboardWidget(DashboardWidgetModel dashboardWidget) {
        this.dashboardWidget = dashboardWidget;
    }

    public ReportsBean getReportsBean() {
        return reportsBean;
    }

    public void setReportsBean(ReportsBean reportsBean) {
        this.reportsBean = reportsBean;
    }

//    public org.primefaces.model.DashboardModel getDashboardModel() {
//        return dashboardModel;
//    }
//
//    public void setDashboardModel(org.primefaces.model.DashboardModel dashboardModel) {
//        this.dashboardModel = dashboardModel;
//    }

    public ArrayList<DashboardWidgetModel> getDashboardWidgetsList() {
        return dashboardWidgetsList;
    }

    public void setDashboardWidgetsList(ArrayList<DashboardWidgetModel> dashboardWidgetsList) {
        this.dashboardWidgetsList = dashboardWidgetsList;
    }

    public LinkedHashMap<Integer, ReportViewer> getReportViewerMap() {
        return reportViewerMap;
    }

    public void setReportViewerMap(LinkedHashMap<Integer, ReportViewer> reportViewerMap) {
        this.reportViewerMap = reportViewerMap;
    }

    public ReportViewer getReportsViewer() {
        return reportsViewer;
    }

    public void setReportsViewer(ReportViewer reportsViewer) {
        this.reportsViewer = reportsViewer;
    }

}
