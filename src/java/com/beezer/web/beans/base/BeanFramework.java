/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.base;

import com.beezer.web.beans.admin.TableViewDesignerBean;
import com.beezer.web.beans.modules.deals.AddDealBean;
import com.beezer.web.beans.admin.appBuilder.fields.FieldsManagerBean;
import com.beezer.web.beans.templates.AddDataBean;
import com.beezer.web.beans.templates.RecordDetailBean;
import com.beezer.web.beans.templates.components.table.TableComponentHelper;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.CategoriesHandler;
import com.beezer.web.handler.DataViewHandler;
import com.beezer.web.handler.FormsHandler;
import com.beezer.web.handler.LookupHandler;
import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.handler.TableSettingsHandler;
import com.beezer.web.handler.UnstructuredHandler;
import com.beezer.web.handler.UserHandler;
import com.beezer.web.models.LazyCustomTableRecord;
import com.beezer.web.models.LazyRecordModel;
import com.beezer.web.models.SelectionModel;
import com.beezer.web.utils.GeneralUtils;
import com.beezer.web.utils.RecordAggregateFunctions;
import com.beezl.interpreter.BEEZLInterperter;
import com.crm.models.comparators.BlockOrderComparator;
import com.crm.models.comparators.FieldLayoutOrderComparator;
import com.crm.models.comparators.FormFieldOrderComparator;
import com.crm.models.comparators.TableSettingsOrderComparator;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.global.UserModel;
import com.crm.models.internal.CategoryModel;
import com.crm.models.internal.DataTableSettings;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.application.AppModuleConfigModel;
import com.crm.models.internal.components.ParameterConfiguaration;
import com.crm.models.internal.components.TableFilterConfig;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.filter.dynamic.FilterConfig;
import com.crm.models.internal.forms.ConditionalAction;
import com.crm.models.internal.forms.EventSubscriber;
import com.crm.models.internal.forms.FieldBlockModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.forms.FormModel;
import com.crm.models.internal.forms.RelationDataMap;
import com.crm.models.internal.lookup.LookupModel;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.internal.security.permissions.ObjectPermissionModel;
import com.crm.models.internal.smartView.SmartViewModel;
import com.crm.models.internal.smartView.TabsConfig;
import com.crm.models.internal.workflow.WFConditionModel;
import com.crm.models.requests.DataTableRequest;
import com.crm.models.requests.DataViewRequest;
import com.crm.models.requests.UnstructuredRequest;
import com.crm.models.requests.UserRequest;
import com.crm.models.requests.managers.CategoryRequest;
import com.crm.models.requests.managers.FormRequest;
import com.crm.models.requests.managers.LookupRequest;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.DataTableResponse;
import com.crm.models.responses.DataViewResponse;
import com.crm.models.responses.UnstructuredResponse;
import com.crm.models.responses.UserResponse;
import com.crm.models.responses.managers.CategoryResponse;
import com.crm.models.responses.managers.FormResponse;
import com.crm.models.responses.managers.LookupResponse;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.responses.managers.RelationResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import org.apache.commons.codec.binary.Base64;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FilesUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.extensions.model.codescanner.Code;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;

/**
 *
 * @author badry
 */
public abstract class BeanFramework extends BaseBean {

    protected String BEAN_TYPE;
    protected String masterPageName;
    protected RecordModel recordModel;

    protected String selection;

    protected HashMap<String, TreeMap<String, String>> lookupDictionary;
    protected HashMap<String, HashMap<String, String>> relationDictionary;
    protected HashMap<String, HashMap<String, String>> categoryDictionary;
    protected HashMap<String, TreeMap<String, String>> userDictionary;

    protected HashMap<String, String> selectedCache;
    protected HashMap<String, ArrayList<LookupModel>> lookupCache;
    protected HashMap<String, ArrayList<String>> simpleLookupCache;
    protected HashMap<String, RelationResponse> relationRecordsCache;
    protected HashMap<String, AddDataBean> relationBeanCache;

    protected HashMap<String, SelectionModel> selectionMap;
    protected HashMap<String, RecordModel> selectedRecordsMap;
    protected HashMap<String, SelectionModel> selectedMultiRelationMap;

    private RecordModel selectedRelationRecord;
    private ArrayList<FormModel> formList;
    private FormModel defaultForm;
    private HashMap<Integer, ArrayList<FieldsLayoutModel>> layoutFieldsMap;

    protected HashMap<String, ArrayList<CategoryModel>> categoriesCache;
    private HashMap<String, String> catalogsNameMap;

    private HashMap<UnstructuredModel, UploadedFile> contentCache;

    private Map<String, String> requestParams;
    private String requestMode;
    private String FTSKeyword;
    private String relationFTSKeyword;
    private LazyRecordModel lazyRecordModel;
    private LazyRecordModel relationLazyRecordModel;
    private ArrayList<DataTableSettings> relationTableSettings;
    private boolean quickAdd;
    private String moduleId;
    private String relationModuleId;

    private AddDataBean addDataBean;
    private String dataDialogWidgetVar;

    private String tableViewDesignerBeanSelectedModule;

    @ManagedProperty(value = "#{tableViewDesignerBean}")
    private TableViewDesignerBean tableViewDesignerBean;

    private String recordCount;

    private LinkedHashMap<Integer, FieldsLayoutModel> advancedConfigMap;
    private HashMap<String, ArrayList<RecordModel>> eventResultListMap;
    private HashMap<Integer, ArrayList<RecordModel>> tableRecordListMap;
    private HashMap<Integer, LazyCustomTableRecord> tableRecordModelMap;
    private HashMap<Integer, TableComponentHelper> tableHelpersMap;
    private HashMap<Integer, String> tableNameMap;
//    private HashMap<Integer, LazyRecordModel> advancedTableLazyRecordMap;
    private LazyRecordModel advCompLazyRecordModel;
    private String activeEvent;
    private int activeComponentId;
    private HashMap<String, ArrayList<EventSubscriber>> eventSubscriberMap;

    protected boolean relationView = false;
    private String keyName;
    private String keyValue;

    private StreamedContent uploadTemplate;
    private UploadedFile uploadedFile;

    private boolean permitCreate;
    private String moduleBreadCrumbName;

    private boolean autoRedirectForm;

    private RecordDetailBean originatingDetailBean;

    private HashMap<String, ArrayList<RecordModel>> autoCompleteListMap;
    private HashMap<String, String> autoCompleteValueMap;

    private int dynoGetDataNumberOfRecords = 10;

    private RecordModel scannedRecord;
    protected HashMap<String, SelectionModel> advancedTableRelationSelectionMap;

    private String bulkUploadErrorMessage;

    private HashMap<String, Date> formDateFieldsCache;
    private HashMap<Integer, Boolean> blockVisibilityMap;
    private HashMap<Integer, Boolean> blockDisabilityMap;

    public BeanFramework() {
        GeneralUtils.setActiveViewBean(this);
        this.dataDialogWidgetVar = "dataDialog";
        this.initialize();
        this.getUrlParams();
        if (this.requestParams.get("moduleId") != null && !this.requestParams.get("moduleId").isEmpty()) {
            this.moduleId = this.requestParams.get("moduleId");
            this.tableViewDesignerBeanSelectedModule = this.moduleId;
//            this.permitCreate = this.isPermittetedToCreate(moduleId);
        }
    }

    public BeanFramework(String dataDialogWidgetVar, String beanType) {
        this.BEAN_TYPE = beanType;
        this.dataDialogWidgetVar = dataDialogWidgetVar;
        this.initialize();
        this.getUrlParams();
    }

    private void getUrlParams() {
        this.requestParams = new HashMap<>(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        if (this.requestParams.get(Defines.REQUEST_MODE_KEY) != null) {
            this.requestMode = this.requestParams.get(Defines.REQUEST_MODE_KEY);
        }
        else {
            this.requestMode = Defines.REQUEST_MODE_ADD;
        }
    }

    public void loadRecordCount(String keyName, String keyValue) {
        try {
            RequestModel request = new RequestModel();
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType(keyName, keyValue));
            request.setClause(fm.getClause());
            request.setRequestingModule(moduleId);
            request.setRequestActionType("4");
            request.setSkipContent(true);
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                this.recordCount = String.valueOf(response.getTotalRecordCount());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isAjaxRequest() {
        boolean val = FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest();
        return val;
    }

    public String getKeyField() {
        String keyField;
        if (super.getFieldsList() != null) {
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                if (mfm.isIsKey()) {
                    keyField = mfm.getFieldName();
                    return keyField;
                }
            }
        }
        else {
            this.loadFieldSet(this.getModuleId(), null);
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                if (mfm.isIsKey()) {
                    keyField = mfm.getFieldName();
                    return keyField;
                }
            }
        }
        return null;
    }

    public ModuleModel loadModule(String moduleId) {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            FilterManager fm = new FilterManager();
            if (moduleId == null || moduleId.isEmpty()) {
                moduleId = this.getModuleId();
            }
            fm.setFilter("AND", new FilterType("module_id", "=", moduleId));
            request.setClause(fm.getClause());
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                return response.getReturnList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    private void initialize() {
        this.lookupCache = new HashMap<>();
        this.simpleLookupCache = new HashMap<>();
        this.selectedCache = new HashMap<>();
        this.relationRecordsCache = new HashMap<>();
        this.lookupDictionary = new HashMap<>();
        this.relationDictionary = new HashMap<>();
        this.categoryDictionary = new HashMap<>();
        this.userDictionary = new HashMap<>();
        this.selectionMap = new HashMap<>();
        this.layoutFieldsMap = new HashMap<>();
        this.categoriesCache = new HashMap<>();
        this.selectedRecordsMap = new HashMap<>();
        this.catalogsNameMap = new HashMap<>();
        this.contentCache = new HashMap<>();
        this.relationBeanCache = new HashMap<>();
        this.selectedMultiRelationMap = new HashMap<>();
        this.advancedConfigMap = new LinkedHashMap<>();
        this.eventResultListMap = new HashMap<>();
        this.eventSubscriberMap = new HashMap<>();
        this.tableRecordListMap = new HashMap<>();
        this.tableRecordModelMap = new HashMap<>();
        this.tableNameMap = new HashMap<>();
        this.autoCompleteListMap = new HashMap<>();
        this.autoCompleteValueMap = new HashMap<>();
        this.tableHelpersMap = new HashMap<>();
        this.advancedTableRelationSelectionMap = new HashMap<>();
        this.formDateFieldsCache = new HashMap<>();
        this.blockVisibilityMap = new HashMap<>();
        this.blockDisabilityMap = new HashMap<>();
        this.clearMultiViewState();
        recordCount = "0";
    }

    public void onSelect(String fieldName, String selection) {
        this.selectedCache.put(fieldName, selectionMap.get(fieldName).getSelectedValue());

        /*
        The Next part is to handle conditional actions on the form fields as per configuration.
        This code snippet applies for dropdowns only.
         */
        if (defaultForm != null) {
            if (defaultForm.getFormConfiguration() != null
                    && defaultForm.getFormConfiguration().getFieldCriteria() != null
                    && defaultForm.getFormConfiguration().getFieldCriteria().getCriteriaGroup() != null) {
                for (WFConditionModel conditionModel : defaultForm.getFormConfiguration().getFieldCriteria().getCriteriaGroup()) {
                    if (conditionModel.getFieldModel().getFieldName().equals(fieldName)) {
                        switch (conditionModel.getOperator()) {
                            case "EQUAL":
                                if (conditionModel.getValue().equals(selection)) {
                                    if (conditionModel.getActionList() != null) {
                                        for (ConditionalAction conditionalAction : conditionModel.getActionList()) {
                                            this.attachConditionalAction(conditionModel, conditionalAction);
                                        }
                                    }
                                    else {
                                        this.attachConditionalAction(conditionModel, null);
                                    }

                                }
                                else {
                                    /*In case another selection is occured on the same field that does not meet the 
                                condition, the conditional action must be reset to have the default behavior.*/
                                    this.attachConditionalAction(conditionModel, new ConditionalAction());
                                }
                        }
                    }
                }
            }

            this.applyConditionalRuleOnBlock(fieldName, selection);
            this.applyDisabilityConditionalRuleOnBlock(fieldName, selection);
        }
//        PrimeFaces.current().ajax().update("formBuilderMasterPanel");
    }

    public void applyConditionalRuleOnBlock(String fieldName, String selection) {
        //13-02-2024: Same Conditional but for blocks
        boolean orOutput = true;
        boolean firstRun = true;
        for (FieldBlockModel blockModel : defaultForm.getBlockList()) {
            if (blockModel.getBlockConfiguration() != null
                    && blockModel.getBlockConfiguration().getFieldCriteria() != null
                    && blockModel.getBlockConfiguration().getFieldCriteria().getCriteriaGroup() != null) {
                orOutput = true;
                firstRun = true;
                for (WFConditionModel conditionModel : blockModel.getBlockConfiguration().getFieldCriteria().getCriteriaGroup()) {
                    if (conditionModel.getFieldModel().getFieldName().equals(fieldName)) {
                        switch (conditionModel.getOperator()) {
                            case "EQUAL":
                                if (conditionModel.getValue().equals(selection)) {
                                    if (firstRun) {
                                        orOutput = orOutput && true;
                                    }
                                    else {
                                        orOutput = orOutput || true;
                                    }
//                                    blockModel.setVisible(true);
                                }
                                else {
                                    /*In case another selection is occured on the same field that does not meet the 
                                condition, the conditional action must be reset to have the default behavior.*/
                                    if (firstRun) {
                                        orOutput = orOutput && false;
                                    }
                                    else {
                                        orOutput = orOutput || false;
                                    }
//                                    blockModel.setVisible(false);
//                                    this.blockVisibilityMap.put(blockModel.getBlockId(), false);
                                }
                                break;
                            case "NOT_EQUAL":
                                if (!conditionModel.getValue().equals(selection)) {
                                    if (firstRun) {
                                        orOutput = orOutput && true;
                                    }
                                    else {
                                        orOutput = orOutput || true;
                                    }
//                                    blockModel.setVisible(true);
                                }
                                else {
                                    /*In case another selection is occured on the same field that does not meet the 
                                condition, the conditional action must be reset to have the default behavior.*/
                                    if (firstRun) {
                                        orOutput = orOutput && false;
                                    }
                                    else {
                                        orOutput = orOutput || false;
                                    }
//                                    blockModel.setVisible(false);
//                                    this.blockVisibilityMap.put(blockModel.getBlockId(), false);
                                }
                                break;
                        }
                        blockModel.setVisible(orOutput);
                        this.blockVisibilityMap.put(blockModel.getBlockId(), orOutput);
                    }
                    firstRun = false;
                }
//                blockModel.setVisible(orOutput);
//                this.blockVisibilityMap.put(blockModel.getBlockId(), orOutput);
            }
            else {
                blockModel.setVisible(true);
            }
        }
    }

    public void applyDisabilityConditionalRuleOnBlock(String fieldName, String selection) {
        //13-02-2024: Same Conditional but for blocks
        boolean orOutput = true;
        boolean firstRun = true;
        for (FieldBlockModel blockModel : defaultForm.getBlockList()) {
            if (blockModel.getBlockDisability() != null
                    && blockModel.getBlockDisability().getFieldCriteria() != null
                    && blockModel.getBlockDisability().getFieldCriteria().getCriteriaGroup() != null) {

                if (blockModel.getBlockDisability().isDisableIfNotCreator()) {
                    if (this.requestMode.equals(Defines.REQUEST_MODE_EDIT) && this.recordModel != null) {
                        if (this.recordModel.getFieldValueMap().get("sys_creation_user") != null
                                && this.recordModel.getFieldValueMap().get("sys_creation_user").getCurrentValue() != null
                                && !this.recordModel.getFieldValueMap().get("sys_creation_user").getCurrentValue().isEmpty()) {
                            String loggedUserId = String.valueOf(GeneralUtils.getLoggedUser().getUserId());
                            if (!this.recordModel.getFieldValueMap().get("sys_creation_user").getCurrentValue().equals(loggedUserId)) {
                                for (FieldsLayoutModel flm : blockModel.getFieldsLayoutList()) {
                                    if (flm.getComponentType().equals("NORM")) {
                                        flm.setReadOnly(true);
                                    }
                                }
                                this.blockDisabilityMap.put(blockModel.getBlockId(), orOutput);
                                continue;
                            }
                        }
                    }
                }

                orOutput = true;
                firstRun = true;
                for (WFConditionModel conditionModel : blockModel.getBlockDisability().getFieldCriteria().getCriteriaGroup()) {
                    if (conditionModel.getFieldModel().getFieldName().equals(fieldName)) {
                        switch (conditionModel.getOperator()) {
                            case "EQUAL":
                                if (conditionModel.getValue().equals(selection)) {
                                    if (firstRun) {
                                        orOutput = orOutput && true;
                                    }
                                    else {
                                        orOutput = orOutput || true;
                                    }
//                                    blockModel.setVisible(true);
                                }
                                else {
                                    /*In case another selection is occured on the same field that does not meet the 
                                condition, the conditional action must be reset to have the default behavior.*/
                                    if (firstRun) {
                                        orOutput = orOutput && false;
                                    }
                                    else {
                                        orOutput = orOutput || false;
                                    }
//                                    blockModel.setVisible(false);
//                                    this.blockVisibilityMap.put(blockModel.getBlockId(), false);
                                }
                                break;
                            case "NOT_EQUAL":
                                if (!conditionModel.getValue().equals(selection)) {
                                    if (firstRun) {
                                        orOutput = orOutput && true;
                                    }
                                    else {
                                        orOutput = orOutput || true;
                                    }
//                                    blockModel.setVisible(true);
                                }
                                else {
                                    /*In case another selection is occured on the same field that does not meet the 
                                condition, the conditional action must be reset to have the default behavior.*/
                                    if (firstRun) {
                                        orOutput = orOutput && false;
                                    }
                                    else {
                                        orOutput = orOutput || false;
                                    }
//                                    blockModel.setVisible(false);
//                                    this.blockVisibilityMap.put(blockModel.getBlockId(), false);
                                }
                                break;
                        }
                        blockModel.setDisabled(orOutput);
                        for (FieldsLayoutModel flm : blockModel.getFieldsLayoutList()) {
                            if (flm.getComponentType().equals("NORM")) {
                                flm.setReadOnly(orOutput);
                            }
                        }
                        this.blockDisabilityMap.put(blockModel.getBlockId(), orOutput);
                    }
                    firstRun = false;
                }
//                blockModel.setVisible(orOutput);
//                this.blockVisibilityMap.put(blockModel.getBlockId(), orOutput);
            }
            else {
                blockModel.setDisabled(false);
            }
        }
    }

    public void onBooleanCheck(String fieldName, boolean value) {
        /*
        The Next part is to handle conditional actions on the form fields as per configuration.
        This code snippet applies for booleans only.
         */
        if (defaultForm != null) {
            if (defaultForm.getFormConfiguration() != null
                    && defaultForm.getFormConfiguration().getFieldCriteria() != null
                    && defaultForm.getFormConfiguration().getFieldCriteria().getCriteriaGroup() != null) {
                for (WFConditionModel conditionModel : defaultForm.getFormConfiguration().getFieldCriteria().getCriteriaGroup()) {
                    if (conditionModel.getFieldModel().getFieldName().equals(fieldName)) {
                        switch (conditionModel.getOperator()) {
                            case "EQUAL":
                                if (conditionModel.getValue().equals(Boolean.toString(value))) {
                                    this.attachConditionalAction(conditionModel, null);
                                }
                                else {
                                    /*In case another selection is occured on the same field that does not meet the 
                                condition, the conditional action must be reset to have the default behavior.*/
                                    this.attachConditionalAction(conditionModel, new ConditionalAction());
                                }
                        }
                    }
                }
            }
        }
    }

    public boolean isPermittetedToCreate(String moduleId) {
        if (moduleId != null && GeneralUtils.getLoggedUser().getPermissionSet() != null && GeneralUtils.getLoggedUser().getPermissionSet().getModulePermissionList() != null) {
            for (ObjectPermissionModel opm : GeneralUtils.getLoggedUser().getPermissionSet().getModulePermissionList()) {
                if (opm.getObjectId() == Integer.valueOf(moduleId)) {
                    return opm.isCreate();
                }
            }
        }
        return true;
    }

    private void attachConditionalAction(WFConditionModel source, ConditionalAction target) {
        if (source.getActionList() != null) {
            for (ConditionalAction conditionalAction : source.getActionList()) {
                for (FieldBlockModel fbm : this.defaultForm.getBlockList()) {
                    for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                        if (flm.getComponentType().equals("NORM")) {
                            if (flm.getFieldObject().getFieldName().equals(conditionalAction.getFieldModel().getFieldName())) {
                                if (target != null) {
                                    flm.setConditionalAction(target);
//                                PrimeFaces.current().ajax().update(flm.getFieldName());
//                                PrimeFaces.current().ajax().update("formBuilderMasterPanel");
                                }
                                else {
                                    flm.setConditionalAction(conditionalAction);
//                                PrimeFaces.current().ajax().update(flm.getFieldName());
//                                PrimeFaces.current().ajax().update("formBuilderMasterPanel");
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public void onMultiSelect(String fieldName, String selection) {
        if (selectionMap.get(fieldName) != null) {
            this.selectedCache.put(fieldName, String.join(",", selectionMap.get(fieldName).getMultiSelectedValues()));
        }
        else {
            this.selectedCache.put(fieldName, null);
        }
    }

    public float renederNumberValues(String num) {
        if (num == null || num.isEmpty()) {
            return 0;
        }
        else {
            return Float.valueOf(num);
        }
    }

    public String byteToBase64(byte[] byteArray) {
        String imageString = new String(Base64.encodeBase64(byteArray));
        return imageString;
    }

    public void onContentSelect(FileUploadEvent event) {
        try {
            SelectionModel sm = new SelectionModel();
            UnstructuredModel cm = new UnstructuredModel();
            cm.setFileExtenstion(GeneralUtils.getFileExtenstion(event.getFile().getFileName()));
            cm.setFileName(event.getFile().getFileName());
            cm.setFileSize(event.getFile().getSize());
            cm.setContentStream(event.getFile().getContent());
            sm.setContentModel(cm);
            this.selectionMap.put((String) event.getComponent().getAttributes().get("FieldName"), sm);
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onGalleryContentSelect(FileUploadEvent event) {
        try {
            SelectionModel sm = new SelectionModel();
            UnstructuredModel cm = new UnstructuredModel();
            cm.setFileExtenstion(GeneralUtils.getFileExtenstion(event.getFile().getFileName()));
            cm.setFileName(event.getFile().getFileName());
            cm.setFileSize(event.getFile().getSize());
            cm.setContentStream(event.getFile().getContent());
            String fieldName = (String) event.getComponent().getAttributes().get("FieldName");
            if (this.selectionMap.get(fieldName) != null) {
                if (this.selectionMap.get(fieldName).getContentList() == null) {
                    this.selectionMap.get(fieldName).setContentList(new ArrayList<>());
                }
                this.selectionMap.get(fieldName).getContentList().add(cm);
            }
            else {
                ArrayList<UnstructuredModel> cmList = new ArrayList<>();
                cmList.add(cm);
                sm.setContentList(cmList);
                this.selectionMap.put(fieldName, sm);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onMultiContentSelect(FilesUploadEvent event) {
        try {
            SelectionModel sm = new SelectionModel();
            ArrayList<UnstructuredModel> cmList = new ArrayList<>();
            for (UploadedFile f : event.getFiles().getFiles()) {
                UnstructuredModel cm = new UnstructuredModel();
                cm.setFileExtenstion(GeneralUtils.getFileExtenstion(f.getFileName()));
                cm.setFileName(f.getFileName());
                cm.setFileSize(f.getSize());
                cm.setContentStream(f.getContent());
                cmList.add(cm);
            }

            String fieldName = (String) event.getComponent().getAttributes().get("FieldName");
            if (this.selectionMap.get(fieldName) != null) {
                if (this.selectionMap.get(fieldName).getContentList() == null) {
                    this.selectionMap.get(fieldName).setContentList(new ArrayList<>());
                }
                this.selectionMap.get(fieldName).getContentList().addAll(cmList);
            }
            else {
                sm.setContentList(cmList);
                this.selectionMap.put(fieldName, sm);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteContent(UnstructuredModel unstructuredModel, String fieldName) {
        try {
            UnstructuredRequest request = new UnstructuredRequest();
            request.setRequestActionType("2");
            request.setUnstructuredModel(unstructuredModel);
            UnstructuredHandler handler = new UnstructuredHandler();
            UnstructuredResponse response = handler.executeForManager(request);
            if (response.getErrorCode() == 1000) {
                this.selectionMap.get(fieldName).getContentList().remove(unstructuredModel);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteFormAttachment(String fieldName, String fileName) {
        if (this.selectionMap.get(fieldName) != null) {
            this.selectionMap.get(fieldName).getContentList().removeIf(x -> x.getFileName().equals(fileName));
        }
    }

    public void deleteFormSingleAttachment(String fieldName, String fileName) {
        if (this.selectionMap.get(fieldName) != null) {
            this.selectionMap.get(fieldName).setContentModel(null);
        }
    }

    public void save() {
        this.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRecordModel(this.recordModel);
        request.setRequestActionType("1");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, GeneralUtils.getApiName(this.getModuleId()));
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public abstract void edit();

    @Override
    public abstract void delete();

    public abstract void cancel();

    public void search(String api) {
        if (this.getFTSKeyword() != null && !this.getFTSKeyword().isEmpty()) {
            lazyRecordModel = new LazyRecordModel(api, this.getFTSKeyword(), true, LazyRecordModel.RECORD, moduleId);
            this.setFTSKeyword(null);
        }
        else {
            FilterManager fm = new FilterManager();
            this.populateFormData();
            if (relationView) {
                fm.setFilter("AND", new FilterType(keyName, keyValue));
            }
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                if (mfm.getCurrentValue() != null && !mfm.getCurrentValue().isEmpty()) {
                    if (this.relationRecordsCache.get(mfm.getFieldName()) != null) {
                        String tableName = this.relationRecordsCache.get(mfm.getFieldName()).getRelationManager().getMasterTable();
//                        fm.setFilter("AND", new FilterType(tableName + "." + mfm.getFieldName(), mfm.getCurrentValue()));
                        fm.setFilter("AND", new FilterType(tableName + "." + mfm.getFieldName(), "IN", mfm.getCurrentValue()));
                    }
                    else {
                        switch (mfm.getFieldType()) {
                            case "LOOKUP":
                                fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", mfm.getCurrentValue()));
                                break;
                            case "BOOLEAN":
                                if (mfm.getCurrentValue() == null || mfm.getCurrentValue().isEmpty() || mfm.getCurrentValue().equals("0")) {
                                }
                                else if (mfm.getCurrentValue().equals("1")) {
                                    fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", true));
                                }
                                else if (mfm.getCurrentValue().equals("2")) {
                                    fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", false));
                                }
                                break;
                            case "INT":
                            case "DECIMAL":
                                fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", mfm.getCurrentValue()));
                                break;
                            case "DATE":
                                if (!mfm.isDateBetween()) {
                                    String modifiedDateValue = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getCurrentValue()));
                                    mfm.setCurrentValue(modifiedDateValue);
                                    fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", modifiedDateValue, "DATE", true));
                                }
                                else {
                                    String modifiedDateFrom = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateFrom()));
                                    String modifiedDateTo = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateTo()));
                                    fm.setFilter("AND", new FilterType(mfm.getFieldName(), "BETWEEN", modifiedDateFrom, modifiedDateTo));
                                }
                                break;
                            default:
                                fm.setFilter("AND", new FilterType(mfm.getFieldName(), "LIKE", mfm.getCurrentValue()));
                                break;
                        }
                    }
                }
                else if (mfm.getFieldType().equals("DATE")) {
                    if (mfm.isDateBetween()) {
                        String modifiedDateFrom = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateFrom()));
                        String modifiedDateTo = GeneralUtils.getDateNoTimeFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getDateTo()));
                        fm.setFilter("AND", new FilterType(mfm.getTableName() + "." + mfm.getFieldName(), "BETWEEN", modifiedDateFrom, modifiedDateTo));
                        mfm.setDateFrom(GeneralUtils.convertPFDateFormate(modifiedDateFrom));
                        mfm.setDateTo(GeneralUtils.convertPFDateFormate(modifiedDateTo));
                    }
                }
            }
            lazyRecordModel = new LazyRecordModel(api, fm.getClause(), false, LazyRecordModel.RECORD, moduleId);
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public void search(String api, String staticClause) {
        if (this.getFTSKeyword() != null && !this.getFTSKeyword().isEmpty()) {
            lazyRecordModel = new LazyRecordModel(api, this.getFTSKeyword(), true, LazyRecordModel.RECORD, moduleId);
            lazyRecordModel.setExtraClause(staticClause);
            this.setFTSKeyword(null);
        }
        else {
            FilterManager fm = new FilterManager();
            this.populateFormData();
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                if (mfm.getCurrentValue() != null && !mfm.getCurrentValue().isEmpty()) {
                    if (this.relationRecordsCache.get(mfm.getFieldName()) != null) {
                        String tableName = this.relationRecordsCache.get(mfm.getFieldName()).getRelationManager().getMasterTable();
//                        fm.setFilter("AND", new FilterType(tableName + "." + mfm.getFieldName(), mfm.getCurrentValue()));
                        fm.setFilter("AND", new FilterType(tableName + "." + mfm.getFieldName(), "IN", mfm.getCurrentValue()));
                    }
                    else {
                        switch (mfm.getFieldType()) {
                            case "LOOKUP":
                                fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", mfm.getCurrentValue()));
                                break;
                            case "BOOLEAN":
                                if (mfm.getCurrentValue() == null || mfm.getCurrentValue().isEmpty() || mfm.getCurrentValue().equals("0")) {
                                }
                                else if (mfm.getCurrentValue().equals("1")) {
                                    fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", true));
                                }
                                else if (mfm.getCurrentValue().equals("2")) {
                                    fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", false));
                                }
                                break;
                            case "INT":
                            case "DECIMAL":
                                fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", mfm.getCurrentValue()));
                                break;
                            case "DATE":
                                String modifiedDateValue = GeneralUtils.getDateFromMillisDBFormat(GeneralUtils.getMillisFromDate(mfm.getCurrentValue()));
                                mfm.setCurrentValue(modifiedDateValue);
                                fm.setFilter("AND", new FilterType(mfm.getFieldName(), "=", modifiedDateValue, "DATE", true));
                                break;
                            default:
                                fm.setFilter("AND", new FilterType(mfm.getFieldName(), "LIKE", mfm.getCurrentValue()));
                                break;
                        }
                    }
                }
            }
            lazyRecordModel = new LazyRecordModel(api, fm.getClause() + staticClause, false, LazyRecordModel.RECORD, moduleId);
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public void searchByRelation(String keyField) {
        if (keyField != null) {
//            String api = GeneralUtils.getApiName(String.valueOf(this.relationRecordsCache.get(keyField).getRelationManager().getDetailModuleId()));
            String api = "GenericMasterAPI";
            if (api != null) {
                if (this.getRelationFTSKeyword() != null && !this.getRelationFTSKeyword().isEmpty()) {
                    relationLazyRecordModel = new LazyRecordModel(api, this.getRelationFTSKeyword(), true, LazyRecordModel.RECORD, String.valueOf(this.relationRecordsCache.get(keyField).getRelationManager().getDetailModuleId()));
                    this.setRelationFTSKeyword(null);
                }
                else {
                    FilterManager fm = new FilterManager();
//                    for (ModuleFieldModel mfm : super.getFieldsList()) {
//                        if (mfm.getCurrentValue() != null && !mfm.getCurrentValue().isEmpty()) {
//                            fm.setFilter("AND", new FilterType(mfm.getFieldName(), "LIKE", mfm.getCurrentValue()));
//                        }
//                    }
//this.loadLazyRelationDataRecords(selection);
                    relationLazyRecordModel = new LazyRecordModel(api, fm.getClause(), false, LazyRecordModel.RECORD, String.valueOf(this.relationRecordsCache.get(keyField).getRelationManager().getDetailModuleId()));
                }
            }
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public void searchForAdvancedTable(String keyField) {
        if (keyField != null) {
            if (this.getRelationFTSKeyword() != null) {
                String targetModuleId = null;
                //For table with data kind of thing
                FieldsLayoutModel sourceComponent = null;
                if (keyField.contains("table_")) {
                    targetModuleId = this.advancedConfigMap.get(Integer.valueOf(keyField.replaceAll("table_", ""))).getTableComponentConfiguration().getBindingModelId();
                }
                //For Get_DATA search event
                else if (keyField.contains("dynamic_data")) {
                    sourceComponent = this.advancedConfigMap.get(Integer.valueOf(keyField.replaceAll("dynamic_data", "")));
                    targetModuleId = String.valueOf(this.relationRecordsCache.get(keyField).getRelationManager().getDetailModuleId());
                }

                if (sourceComponent != null && sourceComponent.getDynoButtonConfiguration() != null) {
                    advCompLazyRecordModel = new LazyRecordModel("GenericMasterAPI", this.getRelationFTSKeyword(), true, LazyRecordModel.RECORD, targetModuleId, sourceComponent.getDynoButtonConfiguration().getSortConfigurations());
                }
                else {
                    advCompLazyRecordModel = new LazyRecordModel("GenericMasterAPI", this.getRelationFTSKeyword(), true, LazyRecordModel.RECORD, targetModuleId);
                }
                this.setRelationFTSKeyword(null);
            }
            else {
                FilterManager fm = new FilterManager();
                for (ModuleFieldModel mfm : super.getFieldsList()) {
                    if (mfm.getCurrentValue() != null && !mfm.getCurrentValue().isEmpty()) {
                        fm.setFilter("AND", new FilterType(mfm.getFieldName(), "LIKE", mfm.getCurrentValue()));
                    }
                }
                advCompLazyRecordModel = new LazyRecordModel("GenericMasterAPI", fm.getClause(), false, LazyRecordModel.RECORD, moduleId);
            }
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public List<RecordModel> onAutocompleteSearch(String query) {
        if (query != null && !query.isEmpty()) {
            FacesContext context = FacesContext.getCurrentInstance();
            String relationVar = (String) UIComponent.getCurrentComponent(context).getAttributes().get("targetField");
            RelationResponse relResponse = this.relationRecordsCache.get(relationVar);

            this.relationModuleId = String.valueOf(relResponse.getRelationManager().getDetailModuleId());
            this.setDataTableKey(relResponse.getRelationManager().getDetailColumn());
            super.setRelationFieldName(relationVar);

            RequestModel request = new RequestModel();
            request.setClause(query);
            request.setRequestingModule(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
            request.setFullTextSearch(true);
            request.setLimit(999);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            request.setRequestContext("DATAVIEW");
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel autoCompleteResponse = requestHandler.executeRequest(request);
            if (autoCompleteResponse.getErrorCode() == 1000) {
                this.autoCompleteListMap.put(relationVar, autoCompleteResponse.getRecordList());
                return autoCompleteResponse.getRecordList();
            }
        }
        return new ArrayList<>();
    }

    public List<RecordModel> onAutocompleteSearchForTable(String query) {
        if (query != null && !query.isEmpty()) {
            FacesContext context = FacesContext.getCurrentInstance();
            String relationVar = (String) UIComponent.getCurrentComponent(context).getAttributes().get("targetField");
            RelationResponse relResponse = this.relationRecordsCache.get(relationVar);

//            this.relationModuleId = String.valueOf(relResponse.getRelationManager().getDetailModuleId());
//            this.setDataTableKey(relResponse.getRelationManager().getDetailColumn());
//            super.setRelationFieldName(relationVar);
            RequestModel request = new RequestModel();
            request.setClause(query);
            request.setRequestingModule(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
            request.setFullTextSearch(true);
            request.setLimit(999);
            request.setRequestActionType("0");
            request.setSkipContent(true);
            request.setRequestContext("DATAVIEW");
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel autoCompleteResponse = requestHandler.executeRequest(request);
            if (autoCompleteResponse.getErrorCode() == 1000) {
                this.autoCompleteListMap.put(relationVar, autoCompleteResponse.getRecordList());
                return autoCompleteResponse.getRecordList();
            }
        }
        return new ArrayList<>();
    }

    public void onAutoCompleteItemSelect(SelectEvent<String> event) {
        //Handle if master and detail columns dont have the same name.
        String selectedValue = (String) event.getObject();
        if (this.getDataTableKey().equals(super.getRelationFieldName())) {
            this.selectedRecordsMap.put(this.getDataTableKey(), selectedRelationRecord);
            this.selectionMap.put(this.getDataTableKey(), new SelectionModel(selectedValue));
        }
        else {
            this.selectedRecordsMap.put(super.getRelationFieldName(), selectedRelationRecord);
            this.selectionMap.put(super.getRelationFieldName(), new SelectionModel(selectedValue));
        }

        if (this.getDataTableKey().equals(super.getRelationFieldName())) {
            this.selectedCache.put(this.getDataTableKey(), selectedValue);
        }
        else {
            this.selectedCache.put(super.getRelationFieldName(), selectedValue);
        }

        if (super.getFieldsList() != null) {
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                if (this.getDataTableKey().equals(super.getRelationFieldName())) {
                    if (mfm.getFieldName().equals(this.getDataTableKey())) {
                        mfm.setCurrentValue(selectedValue);
                    }
                }
                else {
                    if (mfm.getFieldName().equals(super.getRelationFieldName())) {
                        mfm.setCurrentValue(selectedValue);
                    }
                }

            }
        }
    }

    public void onAutoCompleteItemSelectForTable(SelectEvent<String> event) {
        //Handle if master and detail columns dont have the same name.
        String selectedValue = (String) event.getObject();
        FacesContext context = FacesContext.getCurrentInstance();
        RecordModel targetRecord = (RecordModel) UIComponent.getCurrentComponent(context).getAttributes().get("targetRecord");
        String relationVar = (String) UIComponent.getCurrentComponent(context).getAttributes().get("targetField");
        targetRecord.getFieldValueMap().get(relationVar).setCurrentValue(selectedValue);
//        if (this.getDataTableKey().equals(super.getRelationFieldName())) {
//            this.selectedRecordsMap.put(this.getDataTableKey(), selectedRelationRecord);
//            this.selectionMap.put(this.getDataTableKey(), new SelectionModel(selectedValue));
//        }
//        else {
//            this.selectedRecordsMap.put(super.getRelationFieldName(), selectedRelationRecord);
//            this.selectionMap.put(super.getRelationFieldName(), new SelectionModel(selectedValue));
//        }
//
//        if (this.getDataTableKey().equals(super.getRelationFieldName())) {
//            this.selectedCache.put(this.getDataTableKey(), selectedValue);
//        }
//        else {
//            this.selectedCache.put(super.getRelationFieldName(), selectedValue);
//        }
//
//        if (super.getFieldsList() != null) {
//            for (ModuleFieldModel mfm : super.getFieldsList()) {
//                if (this.getDataTableKey().equals(super.getRelationFieldName())) {
//                    if (mfm.getFieldName().equals(this.getDataTableKey())) {
//                        mfm.setCurrentValue(selectedValue);
//                    }
//                }
//                else {
//                    if (mfm.getFieldName().equals(super.getRelationFieldName())) {
//                        mfm.setCurrentValue(selectedValue);
//                    }
//                }
//
//            }
//        }
    }

    public void resetFilters() {
        for (ModuleFieldModel mfm : super.getFilterFieldsList()) {
            mfm.setCurrentValue(null);
            if (mfm.getFieldType().equals("DATE")) {
                if (mfm.isDateBetween()) {
                    mfm.setDateFrom(null);
                    mfm.setDateTo(null);
                }
            }
        }
    }

    public abstract void loadRecord();

    public void loadForms(String moduleId, int formId, String... apiKey) {
        try {
            FormsHandler formsHandler = new FormsHandler();
            FormRequest request = new FormRequest();
            request.setActionType("0");
            if (formId == 0) {
                request.setModuleId(Integer.parseInt(moduleId));
            }
            else {
                request.setFormId(formId);
            }
            if (apiKey.length > 0) {
                if (apiKey[0] != null) {
                    request.setApiKey(apiKey[0]);
                }
            }
            FormResponse response = formsHandler.formExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.formList = response.getReturnList();
                if (this.formList != null) {
                    for (FormModel fm : this.formList) {
                        if (fm.isIsDefault()) {
                            this.defaultForm = fm;
                            break;
                        }
                    }
                    Collections.sort(defaultForm.getBlockList(), new BlockOrderComparator());
                    /**
                     * ******
                     * Check if in form configuration a parameter is set,
                     * evaluate parameter as this is designed to be the default
                     * value of the form.
                     */
                    if (defaultForm.getFormConfiguration() != null && defaultForm.getFormConfiguration().getParams() != null) {
                        try {
                            BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                            beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                            beezlInterperter.setRecodeModel(this.recordModel);
                            for (ParameterConfiguaration pc : defaultForm.getFormConfiguration().getParams()) {
                                beezlInterperter.addDynamicField(pc.getKey(), pc.getValue());
                            }
                            LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                            HashMap<String, String> temp = new HashMap<>();
                            for (ParameterConfiguaration pc : defaultForm.getFormConfiguration().getParams()) {
                                temp.put(pc.getKey(), dynamicFieldMap.get(pc.getKey()));
                            }
                            this.requestParams.putAll(temp);
                        }
                        catch (Exception ex) {
                            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                    for (FieldBlockModel fbm : this.defaultForm.getBlockList()) {
                        this.blockVisibilityMap.put(fbm.getBlockId(), true);
                    }

                    for (FieldBlockModel fbm : this.defaultForm.getBlockList()) {
                        Collections.sort(fbm.getFieldsLayoutList(), new FieldLayoutOrderComparator());
                        this.layoutFieldsMap.put(fbm.getBlockId(), fbm.getFieldsLayoutList());
                        for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                            /*Next line is to initialize conditional model, for now this is a test*/
//                            flm.setConditionalAction(new ConditionalAction());
                            /**/
                            if (flm.getComponentType().equals("NORM")) {
                                if (this.requestParams.get(Defines.REQUEST_MODE_KEY) == null
                                        || this.requestParams.get(Defines.REQUEST_MODE_KEY).equals(Defines.REQUEST_MODE_ADD)) {
                                    if (requestParams.get(flm.getFieldObject().getFieldName()) != null) {
                                        flm.getFieldObject().setCurrentValue(requestParams.get(flm.getFieldObject().getFieldName()));
                                    }
                                }
                                switch (flm.getFieldObject().getFieldType()) {
                                    case "MULTIREL":
                                    case "REL":
                                        if (flm.getFieldObject().getCurrentValue() == null) {
                                            if (flm.getFieldObject().getDefaultValue() != null && !flm.getFieldObject().getDefaultValue().isEmpty()) {
//                                                flm.getFieldObject().setCurrentValue(flm.getFieldObject().getDefaultValue());
                                                BEEZLInterperter bEEZLInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                                                bEEZLInterperter.setRecodeModel(recordModel);
                                                bEEZLInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                                                bEEZLInterperter.addDynamicField(flm.getFieldObject().getFieldName(), flm.getFieldObject().getDefaultValue());
                                                LinkedHashMap<String, String> dynamicFieldMap = bEEZLInterperter.evaluate();
                                                flm.getFieldObject().setCurrentValue(dynamicFieldMap.get(flm.getFieldObject().getFieldName()));
                                            }
                                        }

                                        this.loadRelation(flm.getFieldName(), flm.getFieldObject().getFieldValues());
                                        this.selectionMap.put(flm.getFieldName(), new SelectionModel());
                                        this.autoCompleteValueMap.put(flm.getFieldName(), null);
                                        if (flm.getFieldObject().getCurrentValue() != null) {
                                            this.loadRelationRecord(flm.getFieldName(), flm.getFieldObject().getCurrentValue());

                                            //27-01-2023: For public forms, auto map fields with parameterized urls
                                            if (apiKey.length > 0) {
                                                if (apiKey[0] != null) {
                                                    this.updateRelationDataMap(flm.getFieldName(), this.selectedRecordsMap.get(flm.getFieldName()));
                                                }
                                            }

                                        }
                                        break;
                                    case "MSLOOKUP":
                                    case "LOOKUP":
                                        if (flm.getFieldObject().getCurrentValue() == null) {
                                            if (flm.getFieldObject().getDefaultValue() != null) {
                                                flm.getFieldObject().setCurrentValue(flm.getFieldObject().getDefaultValue());
                                            }
                                        }
                                        if (apiKey.length > 0) {
                                            if (apiKey[0] != null) {
                                                this.loadLookup(flm.getFieldName(), flm.getFieldObject().getFieldValues(), apiKey[0]);
                                            }
                                            else {
                                                this.loadLookup(flm.getFieldName(), flm.getFieldObject().getFieldValues());
                                            }
                                        }
                                        else {
                                            this.loadLookup(flm.getFieldName(), flm.getFieldObject().getFieldValues());
                                        }

                                        this.selectionMap.put(flm.getFieldName(), new SelectionModel());
                                        if (flm.getFieldObject().getCurrentValue() != null && !flm.getFieldObject().getCurrentValue().isEmpty()) {
                                            this.selectionMap.put(flm.getFieldName(), new SelectionModel(flm.getFieldObject().getCurrentValue()));
                                            this.onSelect(flm.getFieldName(), flm.getFieldObject().getCurrentValue());
                                            if (this.isQuickAdd()) {
//                                                PrimeFaces.current().executeScript("PF('" + this.dataDialogWidgetVar+flm.getFieldName() + "').selectValue(" + flm.getFieldObject().getCurrentValue() + ")");
                                            }
                                            else {
                                                PrimeFaces.current().executeScript("PF('" + flm.getFieldName() + "').selectValue(" + flm.getFieldObject().getCurrentValue() + ")");
                                            }
                                        }
                                        else {
                                            this.onSelect(flm.getFieldName(), flm.getFieldObject().getCurrentValue());
                                        }
                                        break;
                                    case "USER":
                                        if (flm.getFieldObject().getCurrentValue() == null) {
                                            if (flm.getFieldObject().getDefaultValue() != null) {
                                                flm.getFieldObject().setCurrentValue(flm.getFieldObject().getDefaultValue());
                                            }
                                        }
                                        this.loadUsers(flm.getFieldName());

                                        this.selectionMap.put(flm.getFieldName(), new SelectionModel());
                                        if (flm.getFieldObject().getCurrentValue() != null && !flm.getFieldObject().getCurrentValue().isEmpty()) {
                                            this.selectionMap.put(flm.getFieldName(), new SelectionModel(flm.getFieldObject().getCurrentValue()));
                                            this.onSelect(flm.getFieldName(), flm.getFieldObject().getCurrentValue());
                                            if (this.isQuickAdd()) {
//                                                PrimeFaces.current().executeScript("PF('" + this.dataDialogWidgetVar+flm.getFieldName() + "').selectValue(" + flm.getFieldObject().getCurrentValue() + ")");
                                            }
                                            else {
                                                PrimeFaces.current().executeScript("PF('" + flm.getFieldName() + "').selectValue(" + flm.getFieldObject().getCurrentValue() + ")");
                                            }
                                        }
                                        break;
                                    case "MULTI_CONTENT":
                                    case "MEDIA_IMG_GALLERY":
                                        if (flm.getFieldObject().getCurrentValue() == null) {
                                            if (flm.getFieldObject().getDefaultValue() != null) {
                                                flm.getFieldObject().setCurrentValue(flm.getFieldObject().getDefaultValue());
                                            }
                                        }
                                        this.selectionMap.put(flm.getFieldName(), new SelectionModel());
                                        break;
                                    case "CAT":
                                        if (flm.getFieldObject().getCurrentValue() == null) {
                                            if (flm.getFieldObject().getDefaultValue() != null) {
                                                flm.getFieldObject().setCurrentValue(flm.getFieldObject().getDefaultValue());
                                            }
                                        }
                                        this.loadCatalogs(flm.getFieldName());
                                        this.selectionMap.put(flm.getFieldName(), new SelectionModel());
                                        break;
                                    case "CONTENT":
                                        if (flm.getFieldObject().getDefaultValue() != null) {
                                            flm.getFieldObject().setCurrentValue(flm.getFieldObject().getDefaultValue());
                                        }
                                        this.selectionMap.put(flm.getFieldName(), new SelectionModel());
                                        break;
                                    case "SIGNATURE":
                                        if (flm.getFieldObject().getDefaultValue() != null) {
                                            flm.getFieldObject().setCurrentValue(flm.getFieldObject().getDefaultValue());
                                        }
                                        this.selectionMap.put(flm.getFieldName(), new SelectionModel());
                                        break;
                                    case "DATE_TIME":
                                        if (flm.getFieldObject().getCurrentValue() != null && !flm.getFieldObject().getCurrentValue().isEmpty()) {
                                            Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(flm.getFieldObject().getCurrentValue());
                                            this.formDateFieldsCache.put(flm.getFieldName(), date1);
                                        }
                                        else {
                                            this.formDateFieldsCache.put(flm.getFieldName(), null);
                                        }
                                        break;
                                    case "INT":
                                    case "DECIMAL":
                                    case "LONG":
                                        if (flm.getFieldObject().getCurrentValue() == null) {
                                            if (flm.getFieldObject().getDefaultValue() != null) {
                                                BEEZLInterperter bEEZLInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                                                bEEZLInterperter.setRecodeModel(recordModel);
                                                bEEZLInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                                                bEEZLInterperter.addDynamicField(flm.getFieldObject().getFieldName(), flm.getFieldObject().getDefaultValue());
                                                LinkedHashMap<String, String> dynamicFieldMap = bEEZLInterperter.evaluate();
                                                flm.getFieldObject().setCurrentValue(dynamicFieldMap.get(flm.getFieldObject().getFieldName()));
//                                                flm.getFieldObject().setCurrentValue(flm.getFieldObject().getDefaultValue());
                                            }
                                            else {
                                                flm.getFieldObject().setCurrentValue("0");
                                            }
                                        }
                                        break;
                                    case "BOOLEAN":
                                        if (flm.getFieldObject().getCurrentValue() == null) {
                                            if (flm.getFieldObject().getDefaultValue() != null) {
                                                flm.getFieldObject().setCurrentValue(flm.getFieldObject().getDefaultValue());
                                            }
                                            else {
                                                flm.getFieldObject().setCurrentValue("false");
                                            }
                                        }
                                        else {
                                            this.onBooleanCheck(flm.getFieldObject().getFieldName(), Boolean.valueOf(flm.getFieldObject().getCurrentValue()));
                                        }
                                        break;
                                }
                            }
                            else {
                                this.buildAdvancedComponentsMap(flm);
                            }
                        }
                    }

                    for (FieldBlockModel fbm : this.defaultForm.getBlockList()) {
                        fbm.setVisible(this.blockVisibilityMap.get(fbm.getBlockId()));
                    }

                    for (FieldBlockModel fbm : this.defaultForm.getBlockList()) {
                        for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                            if (flm.getComponentType().equals("NORM")) {
                                flm.setReadOnly(this.blockDisabilityMap.get(fbm.getBlockId()));
                            }
                        }
                    }

                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void buildAdvancedComponentsMap(FieldsLayoutModel flm) {
        if (flm.getAdvancedComponent().getType() == null) {
            return;
        }

        this.advancedConfigMap.put(flm.getLayoutId(), flm);
        switch (flm.getAdvancedComponent().getType()) {
            case "DYNO_BUTTON":
                if (flm.getDynoButtonConfiguration().getDisabledIfScript() != null
                        && !flm.getDynoButtonConfiguration().getDisabledIfScript().isEmpty()) {
                    BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                    beezlInterperter.setRecodeModel(this.recordModel);
                    beezlInterperter.addDynamicField("disabled", flm.getDynoButtonConfiguration().getDisabledIfScript());
                    LinkedHashMap<String, String> dynamicFieldMap;
                    try {
                        dynamicFieldMap = beezlInterperter.evaluate();
                        if (dynamicFieldMap.get("disabled") != null && !dynamicFieldMap.get("disabled").isEmpty()) {
                            flm.getDynoButtonConfiguration().setDisabled(Boolean.valueOf(dynamicFieldMap.get("disabled")));
                        }
                        else {
                            flm.getDynoButtonConfiguration().setDisabled(false);
                        }
                    }
                    catch (Exception ex) {
                        Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else {
                    flm.getDynoButtonConfiguration().setDisabled(false);
                }

                if (flm.getDynoButtonConfiguration().getEventName() != null) {
                    this.eventResultListMap.put(flm.getDynoButtonConfiguration().getEventName(), null);
                    if (eventSubscriberMap.get(flm.getDynoButtonConfiguration().getEventName()) == null) {
                        this.eventSubscriberMap.put(flm.getDynoButtonConfiguration().getEventName(), new ArrayList<>());
                    }
                }
                break;
            case "TABLE_COMP":
                this.tableRecordListMap.put(flm.getLayoutId(), new ArrayList<>());
                this.tableRecordModelMap.put(flm.getLayoutId(), new LazyCustomTableRecord(new ArrayList<>()));

                this.tableNameMap.put(flm.getLayoutId(), "table_" + flm.getLayoutId());
                if (eventSubscriberMap.get(flm.getTableComponentConfiguration().getComponentName()) == null) {
                    this.eventSubscriberMap.put(flm.getTableComponentConfiguration().getComponentName(), new ArrayList<>());
                }
                Collections.sort(flm.getTableComponentConfiguration().getColumnSettings(), new TableSettingsOrderComparator());
                if (flm.getTableComponentConfiguration().getSummaryColumnsSettings() != null) {
                    Collections.sort(flm.getTableComponentConfiguration().getSummaryColumnsSettings(), new TableSettingsOrderComparator());
                }

                if (flm.getTableComponentConfiguration().getColumnSettings() != null) {
                    for (DataTableSettings dts : flm.getTableComponentConfiguration().getColumnSettings()) {
                        ModuleFieldModel tableFieldModel;
                        if (flm.getTableComponentConfiguration().getBindingModelId() != null && !flm.getTableComponentConfiguration().getBindingModelId().isEmpty()) {
                            tableFieldModel = GeneralUtils.getFieldType(dts.getColumnFieldName(), Integer.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));
                            if (tableFieldModel != null) {
                                dts.setColumnType(tableFieldModel.getFieldType());
                            }
                        }

                        if (dts.isEditable() && dts.getComponentType().equals("REL") && flm.getTableComponentConfiguration().getBindingModelId() != null
                                && !flm.getTableComponentConfiguration().getBindingModelId().isEmpty()) {
                            tableFieldModel = GeneralUtils.getFieldType(dts.getColumnFieldName(), Integer.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));
                            if (tableFieldModel != null) {
                                this.loadRelation(tableFieldModel.getFieldName(), tableFieldModel.getFieldValues());
                            }
                        }
                        else if (dts.isEditable() && dts.getComponentType().equals("LOOKUP")
                                && dts.getTargetLookup() != null
                                && !dts.getTargetLookup().isEmpty()) {
                            this.loadLookup(dts.getColumnFieldName(), dts.getTargetLookup());
                        }
                    }
                }

                if (flm.getTableComponentConfiguration().getEventList() != null) {
                    for (EventSubscriber es : flm.getTableComponentConfiguration().getEventList()) {
                        switch (es.getType()) {
                            case "SELECT_EVENT":
                                if (this.eventSubscriberMap.get(es.getEventName()) != null) {
                                    this.eventSubscriberMap.get(es.getEventName()).add(new EventSubscriber("comp_" + flm.getLayoutId(), es.getType(), flm));
                                }
                                else {
                                    this.eventSubscriberMap.put(es.getEventName(), new ArrayList<>());
                                    this.eventSubscriberMap.get(es.getEventName()).add(new EventSubscriber("comp_" + flm.getLayoutId(), es.getType(), flm));
                                }
                                break;
                            case "INHERIT_TABLE_DATA":
                                if (this.eventSubscriberMap.get(es.getEventName()) != null) {
                                    this.eventSubscriberMap.get(es.getEventName()).add(new EventSubscriber("comp_" + flm.getLayoutId(), es.getType(), flm));
                                }
                                else {
                                    this.eventSubscriberMap.put(es.getEventName(), new ArrayList<>());
                                    this.eventSubscriberMap.get(es.getEventName()).add(new EventSubscriber("comp_" + flm.getLayoutId(), es.getType(), flm));
                                }
                                break;
                            case "REFRESH_DATA":
                                if (this.eventSubscriberMap.get(es.getEventName()) != null) {
                                    this.eventSubscriberMap.get(es.getEventName()).add(new EventSubscriber("comp_" + flm.getLayoutId(), es.getType(), flm));
                                }
                                else {
                                    this.eventSubscriberMap.put(es.getEventName(), new ArrayList<>());
                                    this.eventSubscriberMap.get(es.getEventName()).add(new EventSubscriber("comp_" + flm.getLayoutId(), es.getType(), flm));
                                }
                                break;
                            case "DATA_ENTRY":
                                if (this.eventSubscriberMap.get(es.getEventName()) != null) {
                                    this.eventSubscriberMap.get(es.getEventName()).add(new EventSubscriber("comp_" + flm.getLayoutId(), es.getType(), flm));
                                }
                                else {
                                    this.eventSubscriberMap.put(es.getEventName(), new ArrayList<>());
                                    this.eventSubscriberMap.get(es.getEventName()).add(new EventSubscriber("comp_" + flm.getLayoutId(), es.getType(), flm));
                                }
                                break;
                            case "SCANNER":
                                if (this.eventSubscriberMap.get(es.getEventName()) != null) {
                                    this.eventSubscriberMap.get(es.getEventName()).add(new EventSubscriber("comp_" + flm.getLayoutId(), es.getType(), flm));
                                }
                                else {
                                    this.eventSubscriberMap.put(es.getEventName(), new ArrayList<>());
                                    this.eventSubscriberMap.get(es.getEventName()).add(new EventSubscriber("comp_" + flm.getLayoutId(), es.getType(), flm));
                                }
                                break;
                        }
                    }
                }
                break;
            case "LAYOUT":
                if (flm.getLayoutComponentConfiguration() != null && flm.getLayoutComponentConfiguration().getComponentsList() != null) {
                    Collections.sort(flm.getLayoutComponentConfiguration().getComponentsList(), new FieldLayoutOrderComparator());
                    for (FieldsLayoutModel component : flm.getLayoutComponentConfiguration().getComponentsList()) {
                        this.buildAdvancedComponentsMap(component);
                    }
                }
                break;
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
        selectedRelationRecord = (RecordModel) event.getObject();
        this.selectedRecordsMap.put(this.getDataTableKey(), selectedRelationRecord);
        this.selectionMap.put(this.getDataTableKey(), new SelectionModel(selectedRelationRecord.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue()));
        this.selectedCache.put(this.getDataTableKey(), selectedRelationRecord.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue());
        PrimeFaces.current().executeScript("PF('" + dataDialogWidgetVar + "').hide()");
//        PrimeFaces.current().executeScript("PF('"+this.getDataTableKey()+"').jq.val("+ "'" +selected.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue()+"'"+")");
    }

    public void onFilterRowSelect(SelectEvent event) {
        selectedRelationRecord = (RecordModel) event.getObject();

        this.updateRelationDataMap(this.getDataTableKey(), selectedRelationRecord);
        //Handle if master and detail columns dont have the same name.
        if (this.getDataTableKey().equals(super.getRelationFieldName())) {
            this.selectedRecordsMap.put(this.getDataTableKey(), selectedRelationRecord);
            this.selectionMap.put(this.getDataTableKey(), new SelectionModel(selectedRelationRecord.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue()));
        }
        else {
            this.selectedRecordsMap.put(super.getRelationFieldName(), selectedRelationRecord);
            this.selectionMap.put(super.getRelationFieldName(), new SelectionModel(selectedRelationRecord.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue()));
        }

        if (this.getDataTableKey().equals(super.getRelationFieldName())) {
            this.selectedCache.put(this.getDataTableKey(), selectedRelationRecord.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue());
        }
        else {
            this.selectedCache.put(super.getRelationFieldName(), selectedRelationRecord.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue());
        }

        if (super.getFieldsList() != null) {
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                if (this.getDataTableKey().equals(super.getRelationFieldName())) {
                    if (mfm.getFieldName().equals(this.getDataTableKey())) {
                        mfm.setCurrentValue(selectedRelationRecord.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue());
                    }
                }
                else {
                    if (mfm.getFieldName().equals(super.getRelationFieldName())) {
                        mfm.setCurrentValue(selectedRelationRecord.getFieldValueMap().get(super.getRelationFieldName()).getCurrentValue());
                    }
                }

            }
        }
        PrimeFaces.current().executeScript("PF('" + dataDialogWidgetVar + "').hide()");
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
//        PrimeFaces.current().ajax().update(this.getDataTableKey());
//        PrimeFaces.current().executeScript("PF('"+this.getDataTableKey()+"').jq.val("+ "'" +selected.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue()+"'"+")");
    }

    public void updateRelationDataMap(String relationField, RecordModel relRecord) {
        ArrayList<RelationDataMap> dataMap = new ArrayList<>();
        for (FieldBlockModel fbm : defaultForm.getBlockList()) {
            for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                if (flm.getComponentType().equals("NORM")) {
                    if (flm.getFieldName().equals(relationField)) {
                        if (flm.getDataMapHolder() != null && flm.getDataMapHolder().getRelationDataMap() != null) {
                            dataMap = flm.getDataMapHolder().getRelationDataMap();
                            break;
                        }
                    }
                }
            }
        }

        for (RelationDataMap rdm : dataMap) {
            for (FieldBlockModel fbm : defaultForm.getBlockList()) {
                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                    if (flm.getComponentType().equals("NORM")) {
                        if (rdm.getSourceField().equals(flm.getFieldName())) {
                            if (relRecord.getFieldValueMap().get(rdm.getTargetField()) != null) {
                                if (flm.getFieldObject().getFieldType().equals("REL")) {
//                                    String dataKey = GeneralUtils.getKeyFieldFromRecord(relRecord).getFieldName();
                                    this.onFilterRowSelectInternal(relRecord, flm.getFieldObject().getFieldName());
                                }
                                else if (flm.getFieldObject().getFieldType().equals("LOOKUP")) {
                                    this.selectionMap.put(flm.getFieldObject().getFieldName(), new SelectionModel(relRecord.getFieldValueMap().get(rdm.getTargetField()).getCurrentValue()));
                                    this.onSelect(flm.getFieldObject().getFieldName(), relRecord.getFieldValueMap().get(rdm.getTargetField()).getCurrentValue());
                                }
                                else {
                                    flm.getFieldObject().setCurrentValue(relRecord.getFieldValueMap().get(rdm.getTargetField()).getCurrentValue());
                                }
                                PrimeFaces.current().ajax().update(flm.getFieldObject().getFieldName());
                            }
                        }
                    }
                }
            }
        }

        PrimeFaces.current().executeScript("updateForm();");
//        PrimeFaces.current().ajax().update("formBuilderMasterPanel");
//        PrimeFaces.current().executeScript("$(document).ready(function () {\n"
//                + "            $('.ui-panelgrid-cell > .advanced-non-layout').parent().css('flex-grow', '1');\n"
//                + "            $('.ui-panelgrid-cell > .advanced-layout').parent().css('width', '100%');\n"
//                + "        });");
        PrimeFaces.current().ajax().update("quickAddForm");
    }

    public void onMultiRelationRecordSelect(String tableKey) {
        if (tableKey != null) {
            if (super.getSelectedRecords() != null) {
                RelationResponse relationConfig = null;
                if (super.getRelationFieldName() != null) {
                    relationConfig = this.relationRecordsCache.get(super.getRelationFieldName());
                }
                else {
                    relationConfig = this.relationRecordsCache.get(tableKey);
                }

                if (relationConfig != null) {
                    ArrayList<String> selectedValues = new ArrayList<>();
                    ArrayList<String> selectedValueViews = new ArrayList<>();
                    for (RecordModel rm : super.getSelectedRecords()) {
                        ModuleFieldModel mfm = rm.getFieldValueMap().get(relationConfig.getRelationManager().getDetailColumn());
                        selectedValues.add(mfm.getCurrentValue());
                        mfm = rm.getFieldValueMap().get(relationConfig.getRelationManager().getViewColumn());
                        selectedValueViews.add(mfm.getCurrentValue());
                    }
                    if (super.getRelationFieldName() != null) {
                        this.selectedMultiRelationMap.put(super.getRelationFieldName(), new SelectionModel(selectedValues, selectedValueViews));
                    }
                    else {
                        this.selectedMultiRelationMap.put(tableKey, new SelectionModel(selectedValues, selectedValueViews));
                    }
                    PrimeFaces.current().executeScript("PF('" + "multi" + dataDialogWidgetVar + "').hide()");
                }
            }
        }
    }

    public void onFilterRowSelectInternal(RecordModel event, String dataKey) {
        selectedRelationRecord = (RecordModel) event;
//        this.updateRelationDataMap(this.getDataTableKey(), selectedRelationRecord);
        this.selectedRecordsMap.put(dataKey, selectedRelationRecord);
        this.selectionMap.put(dataKey, new SelectionModel(selectedRelationRecord.getFieldValueMap().get(dataKey).getCurrentValue()));
        this.selectedCache.put(dataKey, selectedRelationRecord.getFieldValueMap().get(dataKey).getCurrentValue());
        if (super.getFieldsList() != null) {
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                if (mfm.getFieldName().equals(dataKey)) {
                    mfm.setCurrentValue(selectedRelationRecord.getFieldValueMap().get(dataKey).getCurrentValue());
                }
            }
        }
        PrimeFaces.current().executeScript("updateForm()");
//        PrimeFaces.current().executeScript("PF('quickAddDialog').hide()"); 
//        PrimeFaces.current().executeScript("PF('" + dataDialogWidgetVar + "').hide()");
//        PrimeFaces.current().executeScript("PF('"+this.getDataTableKey()+"').jq.val("+ "'" +selected.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue()+"'"+")");
    }

    public void onFilterRowSelectInternal(RecordModel event) {
        selectedRelationRecord = (RecordModel) event;
//        this.updateRelationDataMap(this.getDataTableKey(), selectedRelationRecord);
        this.selectedRecordsMap.put(this.getDataTableKey(), selectedRelationRecord);
        this.selectionMap.put(this.getDataTableKey(), new SelectionModel(selectedRelationRecord.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue()));
        this.selectedCache.put(this.getDataTableKey(), selectedRelationRecord.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue());
        if (super.getFieldsList() != null) {
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                if (mfm.getFieldName().equals(this.getDataTableKey())) {
                    mfm.setCurrentValue(selectedRelationRecord.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue());
                }
            }
        }
        PrimeFaces.current().executeScript("updateForm()");
//        PrimeFaces.current().executeScript("PF('quickAddDialog').hide()"); 
//        PrimeFaces.current().executeScript("PF('" + dataDialogWidgetVar + "').hide()");
//        PrimeFaces.current().executeScript("PF('"+this.getDataTableKey()+"').jq.val("+ "'" +selected.getFieldValueMap().get(this.getDataTableKey()).getCurrentValue()+"'"+")");
    }

    public void loadFieldSet(String moduleId, String layoutId) {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(moduleId);
            request.setLayoutId(layoutId);
            RequestHandler requestHandler = new RequestHandler();
            ModuleFieldResponse response = requestHandler.getFieldSet(request);
            if (response.getErrorCode() == 1000) {
                super.setFieldsList(response.getReturnList());
                Collections.sort(super.getFieldsList(), new FormFieldOrderComparator());
                for (ModuleFieldModel mfm : super.getFieldsList()) {
                    switch (mfm.getFieldType()) {
                        case "MULTIREL":
                        case "REL":
                            this.loadRelation(mfm.getFieldName(), mfm.getFieldValues());
                            this.selectionMap.put(mfm.getFieldName(), new SelectionModel());
                            break;
                        case "MSLOOKUP":
                        case "LOOKUP":
                            this.loadLookup(mfm.getFieldName(), mfm.getFieldValues());
                            this.selectionMap.put(mfm.getFieldName(), new SelectionModel());
                            break;
                        case "USER":
                            this.loadUsers(mfm.getFieldName());
                            this.selectionMap.put(mfm.getFieldName(), new SelectionModel());
                            break;
                        case "CAT":
                            this.loadCatalogs(mfm.getFieldName());
                            this.selectionMap.put(mfm.getFieldName(), new SelectionModel());
                            break;
                        case "CONTENT":
                            this.selectionMap.put(mfm.getFieldName(), new SelectionModel());
                            break;
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<ModuleFieldModel> getFieldSetService(String clause, String moduleId, boolean includeRelations) {
        try {
            ArrayList<ModuleFieldModel> al = new ArrayList<>();
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setClause(clause);
            request.setModuleId(moduleId);
            request.setRequestingModule(moduleId);
            RequestHandler requestHandler = new RequestHandler();
            ModuleFieldResponse response = requestHandler.getFieldSet(request);
            if (response.getErrorCode() == 1000) {
                al = response.getReturnList();
                if (includeRelations) {
                    RelationRequest relationRequest = new RelationRequest();
                    relationRequest.setModuleId(moduleId);
                    relationRequest.setActionType("4");
                    RelationHandler relationHandler = new RelationHandler();
                    RelationResponse relationResponse = relationHandler.relationExecutor(relationRequest);
                    if (relationResponse.getErrorCode() == 1000) {
                        if (relationResponse.getRelationManagerList() != null) {
                            for (RelationManagerModel rmm : relationResponse.getRelationManagerList()) {
                                ModuleFieldModel mfm = GeneralUtils.getFieldType(rmm.getViewColumn(), rmm.getDetailModuleId());
                                al.add(mfm);
                            }
                        }
                    }
                }
                return al;
            }
            return null;
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    protected ArrayList<ModuleFieldModel> getFieldSetService(String clause) throws Exception {
        ModuleFieldRequest request = new ModuleFieldRequest();
        request.setClause(clause);
        request.setModuleId(moduleId);
        RequestHandler requestHandler = new RequestHandler();
        ModuleFieldResponse response = requestHandler.getFieldSet(request);
        if (response.getErrorCode() == 1000) {
            return response.getReturnList();
        }
        return null;
    }

    private void buildSimpleLookupCache(String fieldName, ArrayList<LookupModel> lookupList) {
        if (lookupList != null) {
            ArrayList<String> simpleLookupValues = new ArrayList<>();
            for (LookupModel lm : lookupList) {
                simpleLookupValues.add(lm.getName());
            }
            this.simpleLookupCache.put(fieldName + ".name", simpleLookupValues);
        }
    }

    protected void loadLookup(String fieldName, String lookupId, String... apiKey) {
        LookupRequest lookupRequest = new LookupRequest();
        lookupRequest.setLookupId(lookupId);
        if (apiKey.length > 0) {
            if (apiKey[0] != null) {
                lookupRequest.setApiKey(apiKey[0]);
            }
        }
        LookupHandler handler = new LookupHandler();
        LookupResponse lookupResponse;
        try {
            lookupResponse = handler.getLookups(lookupRequest);
            if (lookupResponse.getErrorCode() == 1000) {
                this.lookupCache.put(fieldName, lookupResponse.getReturnList());
                this.buildSimpleLookupCache(fieldName, lookupResponse.getReturnList());
                this.selectedCache.put(fieldName, null);
                TreeMap<String, String> temp = new TreeMap<>();
                for (LookupModel lm : lookupResponse.getReturnList()) {
                    switch (lookupResponse.getLookupManager().getKey()) {
                        case "ID":
                            temp.put(lm.getName(), String.valueOf(lm.getId()));
                            this.selectionMap.put(fieldName, new SelectionModel());
                            break;
                        case "NAME":
                            temp.put(lm.getName(), lm.getName());
                            this.selectionMap.put(fieldName, new SelectionModel());
                            break;
                    }
                }
                this.lookupDictionary.put(fieldName, temp);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void loadUsers(String fieldName) {
        UserRequest userRequest = new UserRequest();
        userRequest.setActionType("0");
        UserHandler handler = new UserHandler();
        UserResponse userResponse;
        try {
            userResponse = handler.execute(userRequest);
            if (userResponse.getErrorCode() == 1000) {
                this.selectedCache.put(fieldName, null);
                TreeMap<String, String> temp = new TreeMap<>();
                for (UserModel userModel : userResponse.getReturnList()) {
                    temp.put(userModel.getName(), String.valueOf(userModel.getUserId()));
                    this.selectionMap.put(fieldName, new SelectionModel());
                }
                this.userDictionary.put(fieldName, temp);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void loadRelation(String fieldName, String relationId) {
        RelationRequest relationRequest = new RelationRequest();
        relationRequest.setRelationId(relationId);
        relationRequest.setSkipRecords(true);
        RelationHandler handler = new RelationHandler();
        RelationResponse relationResponse;
        try {
            relationResponse = handler.getRelation(relationRequest);
            if (relationResponse.getErrorCode() == 1000) {
                this.relationRecordsCache.put(fieldName, relationResponse);

                if (this.selectedCache.get(fieldName) == null || this.selectedCache.get(fieldName).isEmpty()) {
                    this.selectedCache.put(fieldName, null);
                }
                if (BEAN_TYPE != null && !BEAN_TYPE.equals("DATA_BEAN") && !BEAN_TYPE.equals("RECORD_DETAIL_BEAN")) {
                    if (BEAN_TYPE.equals("ADD_DATA_BEAN") && !quickAdd) {
                        this.initializeQuickAddBean(fieldName);
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void loadCatalogs(String fieldName) {
        try {
            CategoryRequest request = new CategoryRequest();
            request.setActionType("0");
            CategoriesHandler categoriesHandler = new CategoriesHandler();
            CategoryResponse response = categoriesHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                this.selectedCache.put(fieldName, null);
                this.categoriesCache.put(fieldName, response.getReturnList());
                HashMap<String, String> temp = new HashMap<>();
                for (CategoryModel cm : response.getReturnList()) {
                    temp.put(cm.getName(), String.valueOf(cm.getEntityId()));
                }
                this.selectionMap.put(fieldName, new SelectionModel());
                this.categoryDictionary.put(fieldName, temp);
            }
//        CategoryRequest request = new CategoryRequest();
//        ObjectMapper mapper = new ObjectMapper();
//        String requestJson = "";
//        try {
//            requestJson = mapper.writeValueAsString(request);
//        }
//        catch (IOException ex) {
//            Logger.getLogger(OrganizationsBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        GetCategoryWS categoryWS = new GetCategoryWS();
//        String responseJson = categoryWS.execute(requestJson);
//        CategoryResponse response;
//        try {
//            response = mapper.readValue(responseJson, CategoryResponse.class);
//            if (response.getErrorCode() == 1000) {
//                this.selectedCache.put(fieldName, null);
//                this.categoriesCache.put(fieldName, response.getReturnList());
//                HashMap<String, String> temp = new HashMap<>();
//                for (CategoryModel cm : response.getReturnList()) {
//                    temp.put(cm.getName(), String.valueOf(cm.getEntityId()));
//                }
//                this.selectionMap.put(fieldName, new SelectionModel());
//                this.categoryDictionary.put(fieldName, temp);
//            }
//        }
//        catch (IOException ex) {
//            Logger.getLogger(OrganizationsBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onSearchChipDeselect(UnselectEvent evt) {
        String relationVar = (String) evt.getComponent().getAttributes().get("searchChipAttrivute");
        String obj = (String) evt.getObject();
        if (selectedMultiRelationMap.get(relationVar) != null) {
            selectedMultiRelationMap.remove(relationVar);
        }
    }

    public void populateFormData() {
        if (recordModel == null) {
            recordModel = new RecordModel();
            recordModel.setFieldValueMap(new LinkedHashMap<>());
        }
        LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
        if (super.getFieldsList() == null) {
            this.loadFieldSet(moduleId, null);
        }
        for (ModuleFieldModel mfm : super.getFieldsList()) {
            if (selectedCache.get(mfm.getFieldName()) != null) {
                mfm.setCurrentValue(selectedCache.get(mfm.getFieldName()));
                fieldValueMap.put(mfm.getFieldName(), mfm);
            }
            else if (selectedMultiRelationMap.get(mfm.getFieldName()) != null && selectedMultiRelationMap.get(mfm.getFieldName()).getMultiRelationSelectedValues() != null) {
                mfm.setCurrentValue(String.join(",", selectedMultiRelationMap.get(mfm.getFieldName()).getMultiRelationSelectedValues()));
                fieldValueMap.put(mfm.getFieldName(), mfm);
            }
            else {
                switch (this.getRequestMode()) {
                    case Defines.REQUEST_MODE_EDIT:
                        fieldValueMap.put(mfm.getFieldName(), recordModel.getFieldValueMap().get(mfm.getFieldName()));
                        break;
                    case Defines.REQUEST_MODE_ADD:
                        fieldValueMap.put(mfm.getFieldName(), mfm);
                        break;
                }
            }
        }
        recordModel.setFieldValueMap(fieldValueMap);
    }

    public void populateDataFromForm() {
        if (recordModel == null) {
            recordModel = new RecordModel();
        }
        LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
        for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
            for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                if (flm.getComponentType().equals("NORM")) {
                    if (selectedCache != null && selectedCache.get(flm.getFieldObject().getFieldName()) != null) {
                        flm.getFieldObject().setCurrentValue(selectedCache.get(flm.getFieldObject().getFieldName()));
                    }
                    else {
                        if (this.selectionMap.get(flm.getFieldObject().getFieldName()) != null && this.selectionMap.get(flm.getFieldObject().getFieldName()).getSelectedValue() != null) {
                            flm.getFieldObject().setCurrentValue(this.selectionMap.get(flm.getFieldObject().getFieldName()).getSelectedValue());
                        }
                    }
                    switch (flm.getFieldObject().getFieldType()) {
                        case "CONTENT":
                            UnstructuredModel um = new UnstructuredModel();
//                        um.setFieldName(flm.getFieldObject().getFieldName());
//                        um.setContentStream(this.selectionMap.get(flm.getFieldObject().getFieldName()).getContentModel().getContentStream());
//                        um.setFileExtenstion(this.selectionMap.get(flm.getFieldObject().getFieldName()).getContentModel().getFileExtenstion());
//                        um.setFileName(this.selectionMap.get(flm.getFieldObject().getFieldName()).getContentModel().getFileName());
//                        um.setFileSize(this.selectionMap.get(flm.getFieldObject().getFieldName()).getContentModel().getFileSize());
                            flm.getFieldObject().setContent(this.selectionMap.get(flm.getFieldObject().getFieldName()).getContentModel());
                            this.contentCache.put(um, this.selectionMap.get(flm.getFieldObject().getFieldName()).getSelectedContent());
                            break;
                        case "MULTI_CONTENT":
                        case "MEDIA_IMG_GALLERY":
                            flm.getFieldObject().setContentList(this.selectionMap.get(flm.getFieldObject().getFieldName()).getContentList());
                            break;
                        case "MULTIREL":
                            flm.getFieldObject().setDetailRecordIdList(this.selectedMultiRelationMap.get(flm.getFieldObject().getFieldName()).getMultiRelationSelectedValues());
                            break;
//                        case "DATE_TIME":
//                            String pattern = "dd/MM/yyyy HH:mm:ss";
//
//// Create an instance of SimpleDateFormat used for formatting 
//// the string representation of date according to the chosen pattern
//                            DateFormat df = new SimpleDateFormat(pattern);
//// Using DateFormat format method we can create a string 
//// representation of a date with the defined format.
//                            String dateAsString = df.format(this.formDateFieldsCache.get(flm.getFieldName()));
//                            flm.getFieldObject().setCurrentValue(dateAsString);
//                            break;
                    }
                    fieldValueMap.put(flm.getFieldObject().getFieldName(), flm.getFieldObject());
                }
            }
        }

        /**
         * ******************************************************
         * Summary fields field mapping. When "Link to Form" option is selected
         * in a summary column, this section will iterate over the summary
         * columns and map the summary columns to their mapped fields.
         * *******************************************************
         */
        ArrayList<FieldsLayoutModel> componentsToSkip = new ArrayList<>();
        for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
            if (!fbm.isVisible()) {
                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                    if (!flm.getComponentType().equals("NORM")) {
                        if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                            componentsToSkip.add(flm);
                        }
                    }
                }
            }

        }

        if (this.getAdvancedConfigMap() != null && !this.getAdvancedConfigMap().isEmpty()) {
            for (Map.Entry<Integer, FieldsLayoutModel> entrySet : this.getAdvancedConfigMap().entrySet()) {
                FieldsLayoutModel flm = entrySet.getValue();
                FieldsLayoutModel skippableComponent = componentsToSkip.stream().filter(x -> x.getLayoutId() == flm.getLayoutId()).findFirst().orElse(null);
                if (skippableComponent != null) {
                    continue;
                }
                if (flm.getAdvancedComponent().getType().equals("TABLE_COMP") && flm.getTableComponentConfiguration() != null
                        && flm.getTableComponentConfiguration().getSummaryColumnsSettings() != null) {
                    for (DataTableSettings dts : flm.getTableComponentConfiguration().getSummaryColumnsSettings()) {
                        if (dts.isLinkToForm()) {
                            FilterManager fm = new FilterManager();
                            fm.setFilter("AND", new FilterType("module_configuration.module_id", "=", this.moduleId));
                            fm.setFilter("AND", new FilterType("field_name", "=",
                                    dts.getColumnFieldName() != null && !dts.getColumnFieldName().isEmpty() ? dts.getColumnFieldName() : dts.getMappingField()));
                            ModuleFieldModel dtsField = this.getFieldSetService(fm.getClause(), null, false).get(0);
                            if (dtsField != null) {
                                dtsField.setCurrentValue(dts.getValueHolder());
                                fieldValueMap.put(dtsField.getFieldName(), dtsField);
                            }
                        }
                    }
                }
            }
        }

        //        recordModel.setFieldValueMap(fieldValueMap);
        if (recordModel.getFieldValueMap() != null) {
            recordModel.getFieldValueMap().putAll(fieldValueMap);
        }
        else {
//            for (Map.Entry<String, ModuleFieldModel> entry : this.getRecordModel().getFieldValueMap().entrySet()) {
//                if (entry.getValue().isIsKey()) {
//                    fieldValueMap.put(entry.getKey(), entry.getValue());
//                    break;
//                }
//            }
            recordModel.setFieldValueMap(fieldValueMap);
        }
    }

    protected void populateFromAllFields() {
        if (recordModel == null) {
            recordModel = new RecordModel();
        }
        this.populateFormData();
        for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
            for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                if (flm.getComponentType().equals("NORM")) {
                    if (selectedCache != null && selectedCache.get(flm.getFieldObject().getFieldName()) != null) {
                        flm.getFieldObject().setCurrentValue(selectedCache.get(flm.getFieldObject().getFieldName()));
                    }
                    recordModel.getFieldValueMap().put(flm.getFieldObject().getFieldName(), flm.getFieldObject());
                }

            }
        }
    }

    protected boolean validateFields() {
        for (FieldBlockModel fbm : this.defaultForm.getBlockList()) {
            for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                if (flm.getComponentType().equals("NORM")) {
                    switch (flm.getFieldObject().getFieldType()) {
                        case "REL":
                            if (flm.isIsMandatory()) {
//                            for (ModuleFieldModel mfm : super.getFieldsList()) {
//                                if (mfm.getFieldName().equals(flm.getFieldObject().getFieldName())) {
//                                    if (selectedCache.get(mfm.getFieldName()) == null || selectedCache.get(mfm.getFieldName()).isEmpty()) {
//                                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", mfm.getFieldLabel() + " is mandatory"));
//                                        return false;
//                                    }
//                                }
//                            }

                                if (selectedCache.get(flm.getFieldObject().getFieldName()) == null
                                        || selectedCache.get(flm.getFieldObject().getFieldName()).isEmpty()) {
                                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", flm.getLabel() + " is mandatory"));
                                    return false;
                                }
                            }
                            break;
                    }
                }
            }
        }
        return true;
    }

    protected void buildRecordModel() {
        recordModel = new RecordModel();
        LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
        for (ModuleFieldModel mfm : super.getFieldsList()) {
            fieldValueMap.put(mfm.getFieldName(), mfm);
        }
        recordModel.setFieldValueMap(fieldValueMap);
    }

    protected void fillRecordDetails() {
        if (this.getFieldsList() != null && this.getRecordModel() != null) {
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                for (Map.Entry<String, ModuleFieldModel> entry : this.getRecordModel().getFieldValueMap().entrySet()) {
                    if (mfm.getFieldName().equals(entry.getKey())) {
                        mfm.setCurrentValue(entry.getValue().getCurrentValue());
                        switch (mfm.getFieldType()) {
                            case "MULTIREL":
                                this.loadMultiRelationRecord(entry.getKey(), entry.getValue().getDetailRecordIdList());
                                break;
                            case "REL":
                                this.loadRelationRecord(entry.getKey(), entry.getValue().getCurrentValue());
                                break;
                            case "MSLOOKUP":
                                ArrayList<String> tempValues = new ArrayList<>();
                                String[] selectedLookupValues = mfm.getCurrentValue().split(",");
                                for (Map.Entry<String, String> multiEntry : this.lookupDictionary.get(mfm.getFieldName()).entrySet()) {
                                    for (String s : selectedLookupValues) {
                                        if (s.equals(multiEntry.getValue())) {
                                            tempValues.add(multiEntry.getKey());
                                            break;
                                        }
                                    }
                                }
                                this.selectionMap.put(entry.getKey(), new SelectionModel(tempValues.toArray(new String[0])));
                                break;
                            case "LOOKUP":
                                if (this.getRecordModel().getFieldValueMap().get(entry.getKey() + ".name") != null) {
                                    this.selectionMap.put(entry.getKey() + ".name", new SelectionModel(this.getRecordModel().getFieldValueMap().get(entry.getKey() + ".name").getCurrentValue()));
                                }
                                break;
                            case "USER":
                                if (this.getRecordModel().getFieldValueMap().get(entry.getKey() + ".name") != null) {
                                    this.selectionMap.put(entry.getKey() + ".name", new SelectionModel(this.getRecordModel().getFieldValueMap().get(entry.getKey() + ".name").getCurrentValue()));
                                }
                                break;
                            case "LOOKUP_NAME":
                                this.selectionMap.put(entry.getKey(), new SelectionModel(entry.getValue().getCurrentValue()));
                                break;
                            case "USER_NAME":
                                this.selectionMap.put(entry.getKey(), new SelectionModel(entry.getValue().getCurrentValue()));
                                break;
                            case "MULTI_CONTENT":
                            case "MEDIA_IMG_GALLERY":
                                SelectionModel sm = new SelectionModel();
                                ArrayList<String> linkList = new ArrayList<>();
                                if (entry.getValue().getContentList() != null) {
                                    for (UnstructuredModel um : entry.getValue().getContentList()) {
                                        linkList.add(um.getLink());
                                    }
                                }
                                sm.setContentUrlList(linkList);
                                sm.setContentList(entry.getValue().getContentList());
                                this.selectionMap.put(entry.getKey(), sm);
                                break;
                            case "CAT":
                                for (CategoryModel cm : this.categoriesCache.get(entry.getKey())) {
                                    if (cm.getEntityId() == Integer.parseInt(entry.getValue().getCurrentValue())) {
                                        this.catalogsNameMap.put(entry.getKey(), cm.getName());
                                        break;
                                    }
                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    protected void fillFormDetails() {
        if (this.getDefaultForm() != null && this.getRecordModel() != null) {
            for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
                if (fbm.getBlockDisability() != null
                        && fbm.getBlockDisability().getFieldCriteria() != null
                        && fbm.getBlockDisability().getFieldCriteria().getCriteriaGroup() != null) {

                    if (fbm.getBlockDisability().isDisableIfNotCreator()) {
                        if (this.requestMode.equals(Defines.REQUEST_MODE_EDIT) && this.recordModel != null) {
                            if (this.recordModel.getFieldValueMap().get("sys_creation_user") != null
                                    && this.recordModel.getFieldValueMap().get("sys_creation_user").getCurrentValue() != null
                                    && !this.recordModel.getFieldValueMap().get("sys_creation_user").getCurrentValue().isEmpty()) {
                                String loggedUserId = String.valueOf(GeneralUtils.getLoggedUser().getUserId());
                                if (!this.recordModel.getFieldValueMap().get("sys_creation_user").getCurrentValue().equals(loggedUserId)) {
                                    for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                                        if (flm.getComponentType().equals("NORM")) {
                                            flm.setReadOnly(true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                    for (Map.Entry<String, ModuleFieldModel> entry : this.getRecordModel().getFieldValueMap().entrySet()) {
                        if (flm.getComponentType().equals("NORM")) {
                            if (flm.getFieldName().equals(entry.getKey())) {
                                flm.getFieldObject().setCurrentValue(entry.getValue().getCurrentValue());
                                switch (flm.getFieldObject().getFieldType()) {
                                    case "MULTIREL":
                                        this.loadMultiRelationRecord(entry.getKey(), entry.getValue().getDetailRecordIdList());
                                        break;
                                    case "REL":
                                        this.loadRelationRecord(entry.getKey(), entry.getValue().getCurrentValue());
                                        break;
                                    case "MSLOOKUP":
                                        if (entry.getValue().getCurrentValue() != null) {
                                            this.selectionMap.put(entry.getKey(), new SelectionModel(entry.getValue().getCurrentValue().split(",")));
                                        }
                                        else {
                                            this.selectionMap.put(entry.getKey(), null);
                                        }
                                        this.onMultiSelect(entry.getKey(), null);
                                        break;
                                    case "LOOKUP":
                                        if (entry.getValue().getCurrentValue() != null && !entry.getValue().getCurrentValue().isEmpty()) {
                                            this.selectionMap.put(entry.getKey(), new SelectionModel(entry.getValue().getCurrentValue()));
                                            this.onSelect(entry.getKey(), entry.getValue().getCurrentValue());
                                            PrimeFaces.current().executeScript("PF('" + entry.getKey() + "').selectValue(" + entry.getValue().getCurrentValue() + ")");
                                        }
                                        break;
                                    case "BOOLEAN":
                                        this.onBooleanCheck(entry.getKey(), Boolean.valueOf(entry.getValue().getCurrentValue()));
                                        break;
                                    case "CAT":
                                        for (CategoryModel cm : this.categoriesCache.get(entry.getKey())) {
                                            if (cm.getEntityId() == Integer.parseInt(entry.getValue().getCurrentValue())) {
                                                this.catalogsNameMap.put(entry.getKey(), cm.getName());
                                                break;
                                            }
                                        }
                                        this.selectionMap.put(entry.getKey(), new SelectionModel(entry.getValue().getCurrentValue()));
                                        this.onSelect(entry.getKey(), null);
                                        PrimeFaces.current().executeScript("PF('" + entry.getKey() + "').selectValue(" + entry.getValue().getCurrentValue() + ")");
                                        break;
                                    case "CONTENT":
                                        SelectionModel sm = new SelectionModel();
                                        sm.setContentModel(entry.getValue().getContent());
                                        this.selectionMap.put(entry.getKey(), sm);
                                        break;
                                    case "MULTI_CONTENT":
                                    case "MEDIA_IMG_GALLERY":
                                        SelectionModel smImgGal = new SelectionModel();
                                        smImgGal.setContentList(entry.getValue().getContentList());
                                        this.selectionMap.put(entry.getKey(), smImgGal);
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void fillAdvancentComponentsValuesForSmartView(SmartViewModel smartViewModel) {
        if (smartViewModel != null && this.getRecordModel() != null) {
            if (smartViewModel.getTabConfigHolder() != null && smartViewModel.getTabConfigHolder().getTabsConfig() != null) {
                for (TabsConfig tc : smartViewModel.getTabConfigHolder().getTabsConfig()) {
                    if (tc.getFieldsLayoutList() != null) {
                        for (FieldsLayoutModel flm : tc.getFieldsLayoutList()) {
                            if (!flm.getComponentType().equals("NORM")) {
                                if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                                    this.fillFieldsLayoutForSmartView(flm);
                                }
                                else if (flm.getAdvancedComponent().getType().equals("LAYOUT")) {
                                    for (FieldsLayoutModel layoutFlm : flm.getLayoutComponentConfiguration().getComponentsList()) {
                                        this.fillFieldsLayoutForSmartView(layoutFlm);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void fillFieldsLayoutForSmartView(FieldsLayoutModel flm) {
        if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
            if (flm.getTableComponentConfiguration().getBindingModelId() != null) {
                if (flm.getTableComponentConfiguration().isProcessData()) {
                    ArrayList<RecordModel> tableRecords = new ArrayList<>();
                    RequestModel request = new RequestModel();
                    request.setRequestActionType("0");
                    request.setLimit(999);
                    request.setRequestingModule(flm.getTableComponentConfiguration().getBindingModelId());
                    if (flm.getTableComponentConfiguration().getOrderByField() != null
                            && !flm.getTableComponentConfiguration().getOrderByField().isEmpty()) {
                        request.setOrderByFieldName(flm.getTableComponentConfiguration().getOrderByField());
                        request.setOrderType(flm.getTableComponentConfiguration().getOrderByType());
                    }
                    ModuleModel mm = this.loadModule(flm.getTableComponentConfiguration().getBindingModelId());
                    FilterManager fm = new FilterManager();
                    fm.setFilter("AND", new FilterType(mm.getModuleBaseTable() + "." + flm.getTableComponentConfiguration().getKeyFieldName(), Integer.valueOf(this.getRequestParams().get("recordId"))));

                    //29-06-2022: Filter configuration handling
                    if (flm.getTableComponentConfiguration().isEnableFilters()) {
                        if (flm.getTableComponentConfiguration().getTableFilterSettings() != null && !flm.getTableComponentConfiguration().getTableFilterSettings().isEmpty()) {
                            for (TableFilterConfig tfc : flm.getTableComponentConfiguration().getTableFilterSettings()) {
                                fm.setFilter("AND", new FilterType(mm.getModuleBaseTable() + "." + tfc.getFieldName(), tfc.getOperator(), tfc.getValue()));
                            }
                        }
                    }

                    ////////////////////////////////////////////
                    request.setClause(fm.getClause());
                    RequestHandler requestHandler = new RequestHandler();
                    ResponseModel response = requestHandler.executeRequest(request);
                    if (response.getErrorCode() == 1000) {
                        tableRecords = response.getRecordList();
                    }
                    ArrayList<ModuleFieldModel> tableFieldSet = getFieldSetService(null, flm.getTableComponentConfiguration().getBindingModelId(), true);
                    for (RecordModel rm : tableRecords) {
                        this.buildTableComponentRecord(flm, rm, tableFieldSet);
                    }
                }
                else {
                    this.activeEvent = flm.getTableComponentConfiguration().getComponentName();
                    ModuleModel mm = this.loadModule(flm.getTableComponentConfiguration().getBindingModelId());
                    FilterManager fm = new FilterManager();
                    fm.setFilter("AND", new FilterType(mm.getModuleBaseTable() + "." + flm.getTableComponentConfiguration().getKeyFieldName(), Integer.valueOf(this.getRequestParams().get("recordId"))));
                    advCompLazyRecordModel = new LazyRecordModel("GenericMasterAPI", fm.getClause(), false, LazyRecordModel.RECORD, String.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));
                    this.loadRelationTableSettings(String.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));
                }
            }

        }
    }

    public void fillAdvancentComponentsValues() {
        if (this.getDefaultForm() != null && this.getRecordModel() != null) {
            for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                    if (!flm.getComponentType().equals("NORM")) {
                        if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                            if (flm.getTableComponentConfiguration().getBindingModelId() != null) {
//                                if (flm.getTableComponentConfiguration().isProcessData()) {
//                                    
//                                }
                                ArrayList<RecordModel> tableRecords = new ArrayList<>();
                                RequestModel request = new RequestModel();
                                request.setRequestActionType("0");
                                request.setLimit(999);
                                request.setRequestingModule(flm.getTableComponentConfiguration().getBindingModelId());
                                if (flm.getTableComponentConfiguration().getOrderByField() != null
                                        && !flm.getTableComponentConfiguration().getOrderByField().isEmpty()) {
                                    request.setOrderByFieldName(flm.getTableComponentConfiguration().getOrderByField());
                                    request.setOrderType(flm.getTableComponentConfiguration().getOrderByType());
                                }
                                ModuleModel mm = this.loadModule(flm.getTableComponentConfiguration().getBindingModelId());
                                FilterManager fm = new FilterManager();
                                fm.setFilter("AND", new FilterType(mm.getModuleBaseTable() + "." + flm.getTableComponentConfiguration().getKeyFieldName(), Integer.valueOf(this.getRequestParams().get("recordId"))));

                                //29-06-2022: Filter configuration handling
                                if (flm.getTableComponentConfiguration().isEnableFilters()) {
                                    if (flm.getTableComponentConfiguration().getTableFilterSettings() != null && !flm.getTableComponentConfiguration().getTableFilterSettings().isEmpty()) {
                                        for (TableFilterConfig tfc : flm.getTableComponentConfiguration().getTableFilterSettings()) {
                                            fm.setFilter("AND", new FilterType(mm.getModuleBaseTable() + "." + tfc.getFieldName(), tfc.getOperator(), tfc.getValue()));
                                        }
                                    }
                                }

                                ////////////////////////////////////////////
                                request.setClause(fm.getClause());
                                RequestHandler requestHandler = new RequestHandler();
                                ResponseModel response = requestHandler.executeRequest(request);;
                                if (response.getErrorCode() == 1000) {
                                    tableRecords = response.getRecordList();
                                }
                                ArrayList<ModuleFieldModel> tableFieldSet = getFieldSetService(null, flm.getTableComponentConfiguration().getBindingModelId(), true);

                                if (tableRecords != null) {
                                    for (RecordModel rm : tableRecords) {
                                        this.buildTableComponentRecord(flm, rm, tableFieldSet);
                                    }
                                }

                                /**
                                 * 29/01/2022: Add pivot table functionality
                                 */
                                if (flm.getTableComponentConfiguration().isPivotViewActive()) {
                                    TableComponentHelper tableComponentHelper = new TableComponentHelper(this, flm);
                                    tableComponentHelper.initializePivotData(this.tableRecordModelMap.get(flm.getLayoutId()).getData());
                                    this.tableHelpersMap.put(flm.getLayoutId(), tableComponentHelper);
                                }
                            }

                            /**
                             * 08/09/2020: Load data into summary fields fix. **
                             */
                            for (DataTableSettings dts : flm.getTableComponentConfiguration().getSummaryColumnsSettings()) {
                                if (dts.isLinkToForm()) {
                                    ModuleFieldModel mfm = GeneralUtils.getFieldFromRecord(recordModel, (dts.getColumnFieldName() != null && !dts.getColumnFieldName().isEmpty() ? dts.getColumnFieldName() : dts.getMappingField()));
                                    if (mfm != null) {
                                        dts.setValueHolder(mfm.getCurrentValue());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void clearMultiViewState() {
        FacesContext context = FacesContext.getCurrentInstance();
        String viewId = context.getViewRoot().getViewId();
        PrimeFaces.current().multiViewState().clearAll(viewId, true, null);
    }

    public void buildTableComponentRecord(FieldsLayoutModel flm, RecordModel rm, ArrayList<ModuleFieldModel> tableFieldSet) {
        ArrayList<RecordModel> targetRecordList = new ArrayList<>();
        if (flm.getTableComponentConfiguration().getBindingModelId() != null && !flm.getTableComponentConfiguration().getBindingModelId().isEmpty()) {
//            ArrayList<ModuleFieldModel> tableFieldSet = getFieldSetService(null, flm.getTableComponentConfiguration().getBindingModelId(), true);

            //filter field list to match configured columns
//            ArrayList<ModuleFieldModel> filteredList = new ArrayList<>();
//            if (tableFieldSet != null) {
//                ModuleFieldModel keyFieldModel = null;
//                for (ModuleFieldModel mfm : tableFieldSet) {
//                    if (mfm.isIsKey() && mfm.getFieldName().equals(flm.getTableComponentConfiguration().getKeyFieldName())) {
//                        keyFieldModel = new ModuleFieldModel(mfm);
//                    }
//
//                    for (DataTableSettings dts : flm.getTableComponentConfiguration().getColumnSettings()) {
//                        if (dts.getColumnFieldName().equals(mfm.getFieldName())) {
//                            filteredList.add(mfm);
//                            break;
//                        }
//                    }
//                }
//                tableFieldSet = filteredList;
//                tableFieldSet.add(keyFieldModel);
//            }
            if (tableFieldSet != null) {
//                RecordModel tableRecordModel = new RecordModel();
//                LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
//                for (ModuleFieldModel mfm : tableFieldSet) {
//                    fieldValueMap.put(mfm.getFieldName(), new ModuleFieldModel(mfm));
//
//                    //Make sure key field value is loaded from the record
//                    if (mfm.isIsKey()) {
//                        fieldValueMap.get(mfm.getFieldName()).setCurrentValue(rm.getFieldValueMap().get(mfm.getFieldName()).getCurrentValue());
//                    }
//
//                    for (DataTableSettings dts : flm.getTableComponentConfiguration().getColumnSettings()) {
//                        if (dts.getColumnFieldName().equals(mfm.getFieldName())) {
//                            fieldValueMap.get(mfm.getFieldName()).setValueStyling(this.evaluateStylingRules(dts.getStyling(), rm));
//                            if (!dts.isEditable()) {
//                                if (mfm.getFieldType().equals("LOOKUP")) {
//                                    fieldValueMap.get(mfm.getFieldName()).setCurrentValue(rm.getFieldValueMap().get(mfm.getFieldName() + ".name").getCurrentValue());
//                                }
//                                else {
//                                    fieldValueMap.get(mfm.getFieldName()).setCurrentValue(rm.getFieldValueMap().get(mfm.getFieldName()).getCurrentValue());
//                                }
//                            }
//
//                            if (dts.isEditable()) {
//                                switch (dts.getComponentType()) {
//                                    case "SPINNER":
//                                        fieldValueMap.get(mfm.getFieldName()).setCurrentValue(rm.getFieldValueMap().get(mfm.getFieldName()).getCurrentValue());
//                                        break;
//                                    case "BOOLEAN":
//                                        fieldValueMap.get(mfm.getFieldName()).setCurrentValue(rm.getFieldValueMap().get(mfm.getFieldName()).getCurrentValue());
//                                        break;
//                                }
//                            }
//                            break;
//                        }
//
//                    }
//                }
//                tableRecordModel.setFieldValueMap(fieldValueMap);
                targetRecordList.add(rm);
                for (DataTableSettings dts : flm.getTableComponentConfiguration().getColumnSettings()) {
                    String dynamiKeyName = "";
                    if (dts.isDynamicColumn()) {
                        if (dts.getColumnFieldName() != null && !dts.getColumnFieldName().isEmpty()) {
                            dynamiKeyName = dts.getColumnFieldName();
                        }
                        else {
                            dynamiKeyName = dts.getColumnLabel();
                        }
                        String script = dts.getDynamicScript();
                        BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                        beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                        beezlInterperter.addDynamicField(dynamiKeyName, script);
                        beezlInterperter.setRecodeModel(rm);
                        try {
                            LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();

                            if (rm.getFieldValueMap().get(dynamiKeyName) != null) {
                                rm.getFieldValueMap().get(dynamiKeyName).setCurrentValue(dynamicFieldMap.get(dynamiKeyName));
                            }
                            else {
                                ModuleFieldModel mfm = new ModuleFieldModel();
                                mfm.setFieldName(dynamiKeyName);
                                mfm.setFieldType("TEXT");
                                mfm.setCurrentValue(dynamicFieldMap.get(dynamiKeyName));
                                rm.getFieldValueMap().put(dynamiKeyName, mfm);
                            }
                            String styling = this.evaluateStylingRules(dts.getStyling(), rm);
                            rm.getFieldValueMap().get(dynamiKeyName).setValueStyling(styling);
                        }
                        catch (Exception ex) {
                            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }

//                ArrayList<RecordModel> sourceRecordList = this.tableRecordListMap.get(flm.getLayoutId());
                ArrayList<RecordModel> sourceRecordList = this.tableRecordModelMap.get(flm.getLayoutId()).getData();
                if (sourceRecordList != null && !sourceRecordList.isEmpty()) {
                    targetRecordList.addAll(sourceRecordList);
                }
//                this.tableRecordListMap.put(flm.getLayoutId(), targetRecordList);
                this.tableRecordModelMap.put(flm.getLayoutId(), new LazyCustomTableRecord(targetRecordList));
//                PrimeFaces.current().ajax().update("comp_" + flm.getLayoutId());
                if (flm.getTableComponentConfiguration().getComponentName() != null && !flm.getTableComponentConfiguration().getComponentName().isEmpty()) {
                    this.eventResultListMap.put(flm.getTableComponentConfiguration().getComponentName(), targetRecordList);
                    this.notifyEventSubscriberData(flm.getTableComponentConfiguration().getComponentName());
                }
            }
        }
    }

    protected void updateMessage(int errorCode, String errorMessage) {
        if (errorCode == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Operation successful."));
            try {
                if (!quickAdd) {
                    for (Map.Entry<String, ArrayList<AppModuleConfigModel>> entry : GeneralUtils.getActiveApplication().getModuleConfigMap().entrySet()) {
                        for (AppModuleConfigModel amcm : entry.getValue()) {
                            if (amcm.getModuleId() == Integer.valueOf(this.getModuleId())) {
                                if (amcm.getOutcome() != null && !amcm.getOutcome().isEmpty()) {
                                    this.masterPageName = amcm.getOutcome() + ".xhtml";
                                    break;
                                }
                            }
                        }
                    }
                    if (this.masterPageName != null && !this.masterPageName.isEmpty()) {
                        if (this.BEAN_TYPE.equals("RECORD_DETAIL_BEAN")) {
                            FacesContext.getCurrentInstance().getExternalContext().redirect(this.masterPageName);
                        }
                        else {
                            String redirectPage;
                            if (this.getRequestParams().get("TaskId") != null) {
                                redirectPage = "recordDetails.xhtml?recordId="
                                        + GeneralUtils.getKeyFieldFromRecord(this.recordModel).getCurrentValue()
                                        + "&" + Defines.REQUEST_MODE_KEY + "=" + Defines.REQUEST_MODE_APPROVE
                                        + "&moduleId=" + this.getModuleId()
                                        + "&TaskId=" + this.getRequestParams().get("TaskId");
                            }
                            else {
                                if (this.getRequestParams().get("masterRec") != null && !this.getRequestParams().get("masterRec").isEmpty()
                                        && this.getRequestParams().get("masterModule") != null && !this.getRequestParams().get("masterModule").isEmpty()) {
                                    redirectPage = "recordDetails.xhtml?recordId="
                                            + this.getRequestParams().get("masterRec") + "&" + "moduleId=" + this.getRequestParams().get("masterModule");
                                }
                                else {
                                    redirectPage = "recordDetails.xhtml?recordId="
                                            + GeneralUtils.getKeyFieldFromRecord(this.recordModel).getCurrentValue()
                                            + "&moduleId=" + this.getModuleId();
                                }
                            }
                            FacesContext.getCurrentInstance().getExternalContext().redirect(redirectPage);
                        }
                    }
                }
                else {
                    if (this.autoRedirectForm) {
                        String redirectPage = "recordDetails.xhtml?recordId="
                                + GeneralUtils.getKeyFieldFromRecord(this.recordModel).getCurrentValue()
                                + "&moduleId=" + this.getModuleId();
                        FacesContext.getCurrentInstance().getExternalContext().redirect(redirectPage);
                    }
                }
            }
            catch (IOException ex) {
                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
//            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Reason: " + errorMessage));
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Operation Failed", errorMessage);
//            PrimeFaces.current().dialog().showMessageDynamic(message);
            PrimeFaces.current().ajax().update("somethingWrongHappenedDialog");
            PrimeFaces.current().executeScript("PF('somethingWrongHappenedDialog').show();");
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide();");
        PrimeFaces.current().executeScript("window.scrollTo(0,0);");
    }

    protected void updateMessage(int errorCode, String errorMessage, String outcomePage) {
        if (errorCode == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Operation successful."));
            try {
                if (!quickAdd) {
                    if (outcomePage != null && !outcomePage.isEmpty()) {
                        FacesContext.getCurrentInstance().getExternalContext().redirect(outcomePage);
                    }
                }
            }
            catch (IOException ex) {
                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Reason: " + errorMessage));
            PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
        }
        PrimeFaces.current().executeScript("window.scrollTo(0,0);");
    }

    public void loadRelationDataRecords(String relationVar) {
        try {
//            super.setSettingsList(null);
            super.setRecordsList(null);
            RequestModel request = new RequestModel();
            RelationResponse relResponse = this.relationRecordsCache.get(relationVar);
            if (relResponse != null) {
                request.setRequestingModule(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
                RequestHandler requestHandler = new RequestHandler();
                ResponseModel response = requestHandler.executeGetRequest(request);
                if (response.getErrorCode() == 1000) {
                    this.loadTableSettings(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
                    super.setRecordsList(response.getRecordList());
                    this.setDataTableKey(relationVar);
//                    RequestContext context = RequestContext.getCurrentInstance();
                    PrimeFaces.current().executeScript("PF('" + dataDialogWidgetVar + "').show()");
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public FieldsLayoutModel getFieldLayoutModelFromForm(String fieldName) {
        for (FieldBlockModel fbm : this.defaultForm.getBlockList()) {
            for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                if (flm.getComponentType().equals("NORM")) {
                    if (flm.getFieldName().equals(fieldName)) {
                        return flm;
                    }
                }
            }
        }
        return null;
    }

    public void loadLazyRelationDataRecords(String relationVar) {
        try {
//            super.setSettingsList(null);
            super.setRecordsList(null);
            RequestModel request = new RequestModel();
            RelationResponse relResponse = this.relationRecordsCache.get(relationVar);
            if (relResponse != null) {
                String api = GeneralUtils.getApiName(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));

                //load conditioanl filtering from form configuration
                String conditionalClause = null;
                FieldsLayoutModel flm = this.getFieldLayoutModelFromForm(relationVar);
                if (flm != null && flm.getFiltering() != null) {
                    if (flm.getFiltering().getANDFilterList() != null && !flm.getFiltering().getANDFilterList().isEmpty()) {
                        for (FilterConfig andConfig : flm.getFiltering().getANDFilterList()) {
//                            if (andConfig.getFieldModel().getFieldType().equals("REL")) {
//                                String evaluated = GeneralUtils.getFieldValueFromForm(defaultForm, andConfig.getValue(), selectionMap);
//                                andConfig.getFieldModel().setCurrentValue(evaluated);
//                            }
                            if (andConfig.getFieldModel().getFieldType().equals("LOOKUP")) {
                                if (andConfig.getValue().contains("$")) {
                                    String evaluated = GeneralUtils.getFieldValueFromForm(defaultForm, andConfig.getValue().replace("$", ""), selectionMap);
                                    andConfig.getFieldModel().setCurrentValue(evaluated);
                                }
                                else {
                                    andConfig.getFieldModel().setCurrentValue(andConfig.getValue());
                                }
                            }
                            else {
                                String evaluated = GeneralUtils.getFieldValueFromForm(defaultForm, andConfig.getValue().replace("$", ""), selectionMap);
                                andConfig.getFieldModel().setCurrentValue(evaluated);
                            }
                        }
                    }

                    if (flm.getFiltering().getORFilterList() != null && !flm.getFiltering().getORFilterList().isEmpty()) {
                        for (FilterConfig orConfig : flm.getFiltering().getORFilterList()) {
                            if (orConfig.getFieldModel().getFieldType().equals("REL")) {
                                String evaluated = GeneralUtils.getFieldValueFromForm(defaultForm, orConfig.getValue(), selectionMap);
//                                orConfig.setValue(evaluated);
                                orConfig.getFieldModel().setCurrentValue(evaluated);
                            }
                        }
                    }

//                    conditionalClause = FilterConverter.convertHolderToClause(flm.getFiltering());
                    conditionalClause = GeneralUtils.convertHolderToClause(flm.getFiltering());
                }

                relationLazyRecordModel = new LazyRecordModel(api, conditionalClause, false, LazyRecordModel.RECORD, String.valueOf(relResponse.getRelationManager().getDetailModuleId()));

                if (flm.getRelationDataViewHolder() == null) {
                    this.loadRelationTableSettings(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
                }
                else {
                    this.setRelationTableSettings(flm.getRelationDataViewHolder().getRelationTableSettings());
                }
                this.relationModuleId = String.valueOf(relResponse.getRelationManager().getDetailModuleId());
//                this.setDataTableKey(relationVar);
                this.setDataTableKey(relResponse.getRelationManager().getDetailColumn());
                super.setRelationFieldName(relationVar);
//                RequestContext context = RequestContext.getCurrentInstance();
                if (quickAdd) {
                    PrimeFaces.current().ajax().update("form" + dataDialogWidgetVar);
                }
                else {
                    PrimeFaces.current().ajax().update("dialogFormMaster");
                }
                PrimeFaces.current().executeScript("PF('" + dataDialogWidgetVar + "').show()");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
        PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
    }

    public void loadLazyMultiRelationDataRecords(String relationVar) {
        try {
//            super.setSettingsList(null);
            super.setRecordsList(null);
            RequestModel request = new RequestModel();
            RelationResponse relResponse = this.relationRecordsCache.get(relationVar);
            if (relResponse != null) {
                String api = GeneralUtils.getApiName(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
                relationLazyRecordModel = new LazyRecordModel(api, this.getFTSKeyword(), false, LazyRecordModel.RECORD, String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
                this.loadRelationTableSettings(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
                this.relationModuleId = String.valueOf(relResponse.getRelationManager().getDetailModuleId());
//                this.setDataTableKey(relationVar);
                this.loadRelationTableSettings(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
                this.setDataTableKey(relResponse.getRelationManager().getDetailColumn());
                super.setRelationFieldName(relationVar);
//                RequestContext context = RequestContext.getCurrentInstance();
                PrimeFaces.current().executeScript("PF('" + "multi" + dataDialogWidgetVar + "').show()");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadQuickAddForm(String relationVar) {
        RelationResponse relResponse = this.relationRecordsCache.get(relationVar);
        if (relResponse != null) {
//                this.moduleId = String.valueOf(relResponse.getRelationManager().getDetailModuleId());
            this.setDataTableKey(relationVar);
//            this.addDataBean = new AddDataBean("dialog" + String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
//            this.addDataBean.setModuleId(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
//            this.addDataBean.init();
//            RequestContext context = RequestContext.getCurrentInstance();
            PrimeFaces.current().executeScript("PF('quickAddDialog').show()");
        }
    }

    public void initializeQuickAddBean(String relationVar) {
        RelationResponse relResponse = this.relationRecordsCache.get(relationVar);
        if (relResponse != null) {
            this.addDataBean = new AddDataBean("dialog" + String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
            this.addDataBean.setModuleId(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
            this.addDataBean.setQuickAdd(true);
            this.addDataBean.init();
            this.relationBeanCache.put(relationVar, addDataBean);
        }
    }

    public void loadRelationRecord(String relationVar, String varValue) {
        try {
            RequestModel request = new RequestModel();
            request.setRequestActionType("0");
            request.setRequestingModule(this.getModuleId());
            RelationResponse relResponse = this.relationRecordsCache.get(relationVar);
            if (relResponse != null) {
                if (varValue != null && !varValue.isEmpty()) {
                    request.setRequestingModule(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
                    FilterManager fm = new FilterManager();
                    fm.setFilter("AND", new FilterType(relResponse.getRelationManager().getDetailTable() + "." + relResponse.getRelationManager().getDetailColumn(), varValue));
                    request.setClause(fm.getClause());

                    /*Moved this section here*/
                    RequestHandler requestHandler = new RequestHandler();
                    ResponseModel response = requestHandler.executeGetRequest(request);
                    if (response.getErrorCode() == 1000) {
                        if (response.getRecordList() != null && !response.getRecordList().isEmpty()) {
                            this.selectedRecordsMap.put(relationVar, response.getRecordList().get(0));
                            /*Reference to the commented part below, i changed the relationValue -> relResponse.getRelationManager().getDetailColumn()*/
                            this.selectionMap.put(relationVar, new SelectionModel(response.getRecordList().get(0).getFieldValueMap().get(relResponse.getRelationManager().getDetailColumn()).getCurrentValue()));
                            this.selectedCache.put(relationVar, response.getRecordList().get(0).getFieldValueMap().get(relResponse.getRelationManager().getDetailColumn()).getCurrentValue());
                        }
                    }
                }
            }

//            RequestHandler requestHandler = new RequestHandler();
//            ResponseModel response = requestHandler.executeGetRequest(request);
//            if (response.getErrorCode() == 1000) {
//                if (response.getRecordList() != null && !response.getRecordList().isEmpty()) {
//                    this.selectedRecordsMap.put(relationVar, response.getRecordList().get(0));
//                    this.selectionMap.put(relationVar, new SelectionModel(response.getRecordList().get(0).getFieldValueMap().get(relationVar).getCurrentValue()));
//                    this.selectedCache.put(relationVar, response.getRecordList().get(0).getFieldValueMap().get(relationVar).getCurrentValue());
//                }
//            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadMultiRelationRecord(String relationVar, ArrayList<String> varValue) {
        try {
            RequestModel request = new RequestModel();
            request.setRequestActionType("0");
            request.setRequestingModule(this.getModuleId());
            RelationResponse relResponse = this.relationRecordsCache.get(relationVar);
            if (relResponse != null) {
                request.setRequestingModule(String.valueOf(relResponse.getRelationManager().getDetailModuleId()));
                FilterManager fm = new FilterManager();
                int index = 0;
                for (String s : varValue) {
                    if (index == 0) {
                        fm.setFilter("AND", new FilterType(relResponse.getRelationManager().getDetailColumn(), s));
                    }
                    else {
                        fm.setFilter("OR", new FilterType(relResponse.getRelationManager().getDetailColumn(), s));
                    }
                    index++;
                }
                request.setClause(fm.getClause());
            }

            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeGetRequest(request);
            if (response.getErrorCode() == 1000) {
                SelectionModel selectionModel = new SelectionModel();
                selectionModel.setSelectedRecords(response.getRecordList());
                ArrayList<String> selectedValues = new ArrayList<>();
                ArrayList<String> selectedValueViews = new ArrayList<>();
                for (RecordModel rm : response.getRecordList()) {
                    selectedValues.add(rm.getFieldValueMap().get(relResponse.getRelationManager().getDetailColumn()).getCurrentValue());
                    selectedValueViews.add(rm.getFieldValueMap().get(relResponse.getRelationManager().getViewColumn()).getCurrentValue());
                }
                selectionModel.setMultiRelationSelectedValues(selectedValues);
                selectionModel.setMultiRelationSelectedValueViews(selectedValueViews);

//                this.selectedRecordsMap.put(relationVar, response.getRecordList().get(0));
                this.selectionMap.put(relationVar, selectionModel);
                this.selectedMultiRelationMap.put(relationVar, selectionModel);
//                this.selectedCache.put(relationVar, response.getRecordList().get(0).getFieldValueMap().get(relationVar).getCurrentValue());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setMode() {
        if (this.requestParams.get(Defines.REQUEST_MODE_KEY) != null
                && this.requestParams.get(Defines.REQUEST_MODE_KEY).equals(Defines.REQUEST_MODE_EDIT)) {
            this.requestMode = Defines.REQUEST_MODE_EDIT;
            if (this.recordModel == null) {
                this.loadRecord();
            }
            this.fillFormDetails();
            this.fillAdvancentComponentsValues();
        }
    }

//    public void initializeRecordWithParameter(){
//        if (this.getDefaultForm() != null) {
//            for (FieldBlockModel fbm : this.getDefaultForm().getBlockList()) {
//                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
//                    ModuleFieldModel mfm;
//                    switch (flm.getFieldName()) {
//                        case "cust_id":
//                            mfm = GeneralUtils.getFieldFromRecord(customerModel, "cust_id");
//                            flm.getFieldObject().setCurrentValue(mfm.getCurrentValue());
//                            this.loadRelationRecord("cust_id", mfm.getCurrentValue());
//                            break;
//                        case "deal_id":
//                            mfm = GeneralUtils.getFieldFromRecord(dealModel.getDealDetails(), "deal_id");
//                            flm.getFieldObject().setCurrentValue(mfm.getCurrentValue());
//                            this.loadRelationRecord("deal_id", mfm.getCurrentValue());
//                            break;
//                    }
//                }
//            }
//        }
//    }
    public void loadTableSettings(String moduleId) {
        try {
            DataTableRequest request = new DataTableRequest();
            request.setModuleId(moduleId);
            request.setActionType("0");
            TableSettingsHandler settingsHandler = new TableSettingsHandler();
            DataTableResponse response = settingsHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                super.setSettingsList(response.getReturnList());
                Collections.sort(super.getSettingsList(), new TableSettingsOrderComparator());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadRelationTableSettings(String moduleId) {
        try {
            DataTableRequest request = new DataTableRequest();
            request.setModuleId(moduleId);
            request.setActionType("0");
            TableSettingsHandler settingsHandler = new TableSettingsHandler();
            DataTableResponse response = settingsHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                if (response.getReturnList() != null && !response.getReturnList().isEmpty()) {
                    this.setRelationTableSettings(response.getReturnList());
                    Collections.sort(this.getRelationTableSettings(), new TableSettingsOrderComparator());
                }
                else {
                    this.setRelationTableSettings(this.loadDataViewSettingsForModule(moduleId));
                    Collections.sort(this.getRelationTableSettings(), new TableSettingsOrderComparator());
                }

            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<DataTableSettings> loadDataViewSettingsForModule(String moduleId) {
        DataViewRequest dataViewRequest = new DataViewRequest();
        dataViewRequest.setRequestActionType("0");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("module_id", "=", moduleId));
        dataViewRequest.setClause(fm.getClause());
        DataViewHandler dataViewHandler = new DataViewHandler();
        DataViewResponse dataViewResponse = dataViewHandler.executeRequest(dataViewRequest);
        if (dataViewResponse.getErrorCode() == 1000
                && dataViewResponse.getReturnList() != null && !dataViewResponse.getReturnList().isEmpty()) {
            if (dataViewResponse.getReturnList().get(0).getDataViewConfig().isEnableTabularView()) {
                for (DataTableSettings dts : dataViewResponse.getReturnList().get(0).getDataViewConfig().getTabularViewSettings().getTableSettings()) {
                    dts.setVisible(true);
                }
                return dataViewResponse.getReturnList().get(0).getDataViewConfig().getTabularViewSettings().getTableSettings();
            }
            else {
                return null;
            }
        }
        return new ArrayList<>();
    }

    public void loadFilters(String moduleId) {
        if (super.getFieldsList() == null) {
            this.loadFieldSet(moduleId, null);
        }

        if (super.getFilterFieldsList() == null) {
            this.loadFilterFieldSet();
        }

//        if (super.getSettingsList() == null) {
//            this.loadTableSettings(moduleId);
//        }
    }

    public void loadFilterFieldSet() {
        if (super.getFieldsList() != null) {
            super.setFilterFieldsList(new ArrayList<>());
            for (ModuleFieldModel mfm : super.getFieldsList()) {
                if (!mfm.isChronicalField()) {
                    if (mfm.getFieldType().equalsIgnoreCase("REL")) {
                        this.getSelectedMultiRelationMap().put(mfm.getFieldName(), new SelectionModel());
                    }
                    super.getFilterFieldsList().add(mfm);
                }
            }
        }
    }

    public void loadTableColumnDesigner() {
//        this.tableViewDesignerBean = new TableViewDesignerBean();
//        if (this.getModuleId() != null) {
//            tableViewDesignerBean.setSelectModuleId(this.getModuleId());
//            tableViewDesignerBean.init();
//            Map<String, Object> appMap = FacesContext.getCurrentInstance().getViewRoot().getViewMap();
//            appMap.put("tableViewDesignerBean", tableViewDesignerBean);
//            PrimeFaces.current().executeScript("PF('tableViewManagerDialog').show()");
//        }
        PrimeFaces.current().executeScript("PF('tableViewManagerDialog').show()");
    }

    public final void initializeTableColumnDesigner() {
        if (this.getModuleId() != null) {
            tableViewDesignerBean.setSelectModuleId(this.getModuleId());
            tableViewDesignerBean.init();
            Map<String, Object> appMap = FacesContext.getCurrentInstance().getViewRoot().getViewMap();
            appMap.put("tableViewDesignerBean", tableViewDesignerBean);
//            PrimeFaces.current().executeScript("PF('tableViewManagerDialog').show()");
        }
    }

    public void quickSave(BeanFramework bf) {
        this.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(this.getModuleId());
        request.setRecordModel(this.recordModel);
        request.setRequestActionType("1");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                ModuleFieldModel mfm = new ModuleFieldModel();
                mfm.setFieldName(this.getKeyField());
                mfm.setCurrentValue(String.valueOf(response.getObjectId()));
                mfm.setIsKey(true);
                mfm.setFieldType("INT");
                this.recordModel.getFieldValueMap().put(mfm.getFieldName(), mfm);
                bf.onFilterRowSelectInternal(this.recordModel);
                if (bf.getAddDataBean().getActiveEvent() != null && !bf.getAddDataBean().getActiveEvent().isEmpty()) {
                    bf.setRecordModel(this.recordModel);
                    bf.notifyEventSubscriberData(bf.getAddDataBean().getActiveEvent());
                }
                PrimeFaces.current().executeScript("PF('quickAddDialog').hide()");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void quickSaveSmartView(AddDataBean adb) {
        adb.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(adb.getModuleId());
        request.setRecordModel(adb.recordModel);
        if (adb.getRequestMode().equals(Defines.REQUEST_MODE_ADD)) {
            request.setRequestActionType("1");
        }
        else if (adb.getRequestMode().equals(Defines.REQUEST_MODE_EDIT)) {
            request.setRequestActionType("3");
        }

        if (adb.isFormContainTableRecords()) {
            request.setHoldProcessRun(true);
        }

        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Operation successful."));

                if (adb.getRequestMode().equals(Defines.REQUEST_MODE_EDIT)) {
                    ModuleFieldModel keyField = GeneralUtils.getKeyFieldFromRecord(adb.recordModel);
                    response.setObjectId(Integer.valueOf(keyField.getCurrentValue()));
                    adb.saveFormTableRecords(response, "3", request);
                }
                else {
                    ModuleFieldModel mfm = new ModuleFieldModel();
                    mfm.setFieldName(adb.getKeyField());
                    mfm.setCurrentValue(String.valueOf(response.getObjectId()));
                    adb.recordModel.getFieldValueMap().put(mfm.getFieldName(), mfm);
                    ModuleFieldModel keyFieldChecker = GeneralUtils.getKeyFieldFromRecord(adb.recordModel);
                    if (keyFieldChecker != null) {
                        adb.recordModel.getFieldValueMap().get(keyFieldChecker.getFieldName()).setCurrentValue(String.valueOf(response.getObjectId()));
                    }
                    else {
                        keyFieldChecker = GeneralUtils.getKeyFieldType(Integer.valueOf(adb.getModuleId()));
                        keyFieldChecker.setCurrentValue(String.valueOf(response.getObjectId()));
                        adb.recordModel.getFieldValueMap().put(keyFieldChecker.getFieldName(), keyFieldChecker);
                    }
                    adb.saveFormTableRecords(response, "1", request);
                }

//                bf.onFilterRowSelectInternal(this.recordModel);
                if (adb.getActiveEvent() != null && !adb.getActiveEvent().isEmpty()) {
                    this.notifyEventSubscriberData(adb.getActiveEvent());
                }

                if (adb.isAutoRedirectForm()) {
                    ModuleFieldModel keyField = GeneralUtils.getKeyFieldFromRecord(adb.recordModel);
                    if (keyField != null) {
                        adb.recordModel.getFieldValueMap().get(keyField.getFieldName()).setCurrentValue(String.valueOf(response.getObjectId()));
                    }
                    else {
                        keyField = GeneralUtils.getKeyFieldType(Integer.valueOf(adb.getModuleId()));
                        keyField.setCurrentValue(String.valueOf(response.getObjectId()));
                        adb.recordModel.getFieldValueMap().put(keyField.getFieldName(), keyField);
                    }

                    String redirectPage = "recordDetails.xhtml?recordId="
                            + GeneralUtils.getKeyFieldFromRecord(adb.recordModel).getCurrentValue()
                            + "&moduleId=" + adb.getModuleId();
                    FacesContext.getCurrentInstance().getExternalContext().redirect(redirectPage);
                }
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Operation unsuccessful."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onCodeScanned(final SelectEvent<Code> event) {
        final Code code = event.getObject();
        FieldsLayoutModel flm = this.advancedConfigMap.get(this.activeComponentId);
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule(flm.getDynoButtonConfiguration().getModuleId());
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(flm.getDynoButtonConfiguration().getScannerTargetFieldName(), "=", code.getValue()));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = requestHandler.executeRequest(request);
        if (response.getErrorCode() == 1000 && response.getRecordList() != null && !response.getRecordList().isEmpty()) {
            this.scannedRecord = response.getRecordList().get(0);
            PrimeFaces.current().executeScript("PF('scannerDialog').hide()");
            PrimeFaces.current().resetInputs("scannerOutputDialog");
            PrimeFaces.current().ajax().update("scannerOutputDialog");
            PrimeFaces.current().executeScript("PF('scannerOutputDialog').show()");
        }

        PrimeFaces.current().executeScript("PF('scannerDialog').hide()");
    }

    public void onConfirmScannedRecord() {
        ArrayList<RecordModel> sel = new ArrayList<>();
        sel.add(this.scannedRecord);
        this.eventResultListMap.put(activeEvent, sel);
        this.notifyEventSubscriberData(activeEvent);
        PrimeFaces.current().executeScript("PF('scannerOutputDialog').hide()");
    }

    public void onAdvancedComponentTableSelectRow(SelectEvent event) {
        RecordModel row = (RecordModel) event.getObject();
        if (super.getSelectedRecordsSaved() != null && !super.getSelectedRecordsSaved().isEmpty()) {
            boolean keyFound = false;
            ModuleFieldModel mfmNewKey = GeneralUtils.getKeyFieldFromRecord(row);
            for (RecordModel rm : super.getSelectedRecordsSaved()) {
                ModuleFieldModel mfmKey = GeneralUtils.getKeyFieldFromRecord(rm);
                if (mfmNewKey.getCurrentValue().equals(mfmKey.getCurrentValue())) {
                    keyFound = true;
                    break;
                }
            }

            if (!keyFound) {
                super.getSelectedRecordsSaved().add(row);
            }
        }
        else {
            super.getSelectedRecordsSaved().add(row);
        }
    }

    public void onAdvancedComponentTableUnselectRow(UnselectEvent event) {
        RecordModel row = (RecordModel) event.getObject();
        super.getSelectedRecordsSaved().remove(row);
    }

    public void onAdvancedComponentTablePageChange() {
        super.setSelectedRecords(super.getSelectedRecordsSaved());
    }

    public void onDynoButtonClick(int layoutId) {
        FieldsLayoutModel flm = this.advancedConfigMap.get(layoutId);
        if (flm != null) {
            switch (flm.getDynoButtonConfiguration().getActionType()) {
                case "GET_DATA":
                    try {
                        String evaluatedClause = "";
                        if (flm.getDynoButtonConfiguration().getDataFilterClause() != null && !flm.getDynoButtonConfiguration().getDataFilterClause().isEmpty()) {
                            BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                            beezlInterperter.addDynamicField("button_clause", flm.getDynoButtonConfiguration().getDataFilterClause());
                            this.populateFromAllFields();
                            beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                            beezlInterperter.setRecodeModel(recordModel);
                            LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                            evaluatedClause = dynamicFieldMap.get("button_clause");
                        }

                        this.activeEvent = flm.getDynoButtonConfiguration().getEventName();
                        advCompLazyRecordModel = new LazyRecordModel("GenericMasterAPI", evaluatedClause, false, LazyRecordModel.RECORD, String.valueOf(flm.getDynoButtonConfiguration().getModuleId()), flm.getDynoButtonConfiguration().getSortConfigurations());
                        this.loadRelationTableSettings(String.valueOf(flm.getDynoButtonConfiguration().getModuleId()));

                        this.dynoGetDataNumberOfRecords = flm.getDynoButtonConfiguration().getNumberOfRecordsPerPage();
                        //This is to enable the search feature
                        this.setDataTableKey("dynamic_data" + flm.getLayoutId());
                        RelationManagerModel relationManagerModel = new RelationManagerModel();
                        relationManagerModel.setDetailModuleId(Integer.valueOf(flm.getDynoButtonConfiguration().getModuleId()));
                        RelationResponse relationResponse = new RelationResponse();
                        relationResponse.setRelationManager(relationManagerModel);
                        this.relationRecordsCache.put("dynamic_data" + flm.getLayoutId(), relationResponse);

                        super.setSelectedRecordsSaved(new ArrayList<>());
//                        
//                        super.setRelationFieldName(relationVar);
//                        RequestContext context = RequestContext.getCurrentInstance();
                        if (!quickAdd) {
                            PrimeFaces.current().executeScript("PF('advancedDataDialog').show()");
                        }
                        else {
                            PrimeFaces.current().executeScript("PF('advancedDataDialogRec').show()");
                        }
                    }
                    catch (Exception ex) {
                        Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case "OPEN_PAGE":
                    try {
                        String url = "add.xhtml?moduleId=" + flm.getDynoButtonConfiguration().getModuleId();
                        if (flm.getDynoButtonConfiguration().getParams() != null) {
                            BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                            beezlInterperter.setRecodeModel(this.recordModel);
                            beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                            for (ParameterConfiguaration pc : flm.getDynoButtonConfiguration().getParams()) {
                                beezlInterperter.addDynamicField(pc.getKey(), pc.getValue());
                            }
                            LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                            for (ParameterConfiguaration pc : flm.getDynoButtonConfiguration().getParams()) {
                                url = url + "&" + pc.getKey() + "=" + dynamicFieldMap.get(pc.getKey());
                            }
                        }
                        FacesContext.getCurrentInstance().getExternalContext().redirect(url);
                    }
                    catch (Exception ex) {
                        Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case "OPEN_FORM":
                    this.addDataBean = new AddDataBean("dialog" + flm.getDynoButtonConfiguration().getModuleId());
                    this.addDataBean.setModuleId(flm.getDynoButtonConfiguration().getModuleId());
                    this.addDataBean.setQuickAdd(true);
                    this.addDataBean.setActiveEvent(flm.getDynoButtonConfiguration().getEventName());
                    this.addDataBean.setAutoRedirectForm(flm.getDynoButtonConfiguration().isAutoFormRedirect());
                    if (flm.getDynoButtonConfiguration().getParams() != null) {
                        try {
                            BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                            beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                            beezlInterperter.setRecodeModel(this.recordModel);
                            for (ParameterConfiguaration pc : flm.getDynoButtonConfiguration().getParams()) {
                                beezlInterperter.addDynamicField(pc.getKey(), pc.getValue());
                            }
                            LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                            HashMap<String, String> temp = new HashMap<>();
                            for (ParameterConfiguaration pc : flm.getDynoButtonConfiguration().getParams()) {
                                temp.put(pc.getKey(), dynamicFieldMap.get(pc.getKey()));
                            }
                            this.addDataBean.setRequestParams(new HashMap<>());
                            this.addDataBean.getRequestParams().putAll(temp);
                        }
                        catch (Exception ex) {
                            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    this.addDataBean.init();
                    this.relationBeanCache.put("dynamic_form" + flm.getDynoButtonConfiguration().getModuleId(), addDataBean);
                    this.setDataTableKey("dynamic_form" + flm.getDynoButtonConfiguration().getModuleId());

//                    RequestContext context = RequestContext.getCurrentInstance();
                    PrimeFaces.current().resetInputs("quickAddDialog");
                    PrimeFaces.current().ajax().update("quickAddDialog");
                    PrimeFaces.current().executeScript("PF('quickAddDialog').show()");
                    break;
                case "RUN_WF":
                    RequestModel request = new RequestModel();
                    request.setRequestingModule(this.getModuleId());
                    request.setRecordModel(this.recordModel);
                    request.setAdhocWorkflowId(Integer.valueOf(flm.getDynoButtonConfiguration().getWorkflowId()));
                    request.setRequestActionType("6");
                    RequestHandler requestHandler = new RequestHandler();
                    ResponseModel response;
                    try {
                        response = requestHandler.executeRequest(request, "GenericMasterAPI");
                        if (response.getErrorCode() == 1000) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Operation done successfully"));
                        }
                        else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Reason: " + response.getErrorMessage()));
                        }
//                        this.updateMessage(response.getErrorCode(), response.getErrorMessage());
                    }
                    catch (Exception ex) {
                        Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case "DATA_ENTRY":
                    this.activeEvent = flm.getDynoButtonConfiguration().getEventName();
                    this.notifyEventSubscriberData(activeEvent);
                    break;
                case "SCANNER":
                    this.activeComponentId = layoutId;
                    this.activeEvent = flm.getDynoButtonConfiguration().getEventName();
                    PrimeFaces.current().executeScript("PF('scannerDialog').show()");
                    break;
            }
            PrimeFaces.current().executeScript("PF('waitingDlgName').hide()");
        }
    }

    public void onGetDataDoneClick() {
        ArrayList<RecordModel> sel = new ArrayList<>();
        if (super.getSelectedRecords() != null) {
            if (super.getSelectedRecordsSaved() != null && !super.getSelectedRecordsSaved().isEmpty()) {
                super.setSelectedRecords(super.getSelectedRecordsSaved());
            }
            else {
                super.setSelectedRecordsSaved(new ArrayList<>());
                super.getSelectedRecordsSaved().addAll(super.getSelectedRecords());
            }
            for (RecordModel rm : super.getSelectedRecords()) {
                sel.add(rm);
            }
            super.getSelectedRecords().clear();
        }
        this.eventResultListMap.put(activeEvent, sel);
        this.notifyEventSubscriberData(activeEvent);
//        PrimeFaces.current().executeScript("PF('advancedDialogFormMasterRec" + this.dataDialogWidgetVar + "').hide()");
        PrimeFaces.current().executeScript("PF('advancedDataDialogRec').hide()");
    }

    public void onCustomTableRowSelect(SelectEvent event) {
        try {
            String recordId = null;
            int tableModuleId = 0;
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("record", super.getSelectedRecords().get(0));
            RecordModel selectedModel = (RecordModel) event.getObject();
            for (Map.Entry<String, ModuleFieldModel> entry : selectedModel.getFieldValueMap().entrySet()) {
                if (entry.getValue().isIsKey()) {
                    recordId = entry.getValue().getCurrentValue();
                    tableModuleId = entry.getValue().getModuleId();
                    break;
                }
            }
            FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                    + recordId + "&" + "moduleId=" + tableModuleId);
        }
        catch (Exception ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void notifyEventSubscriberData(String eventName) {
        ArrayList<EventSubscriber> eventSubscribers = eventSubscriberMap.get(eventName);
        if (eventSubscribers != null) {
            for (EventSubscriber es : eventSubscribers) {
                FieldsLayoutModel flm = es.getFieldsLayoutModel();
                if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                    switch (es.getType()) {
                        case "SELECT_EVENT":
                            this.notifyTableSelectEvent(eventName, es);
                            break;
                        case "INHERIT_TABLE_DATA":
                            this.notifyTableInheritEvent(eventName, es);
                            break;
                        case "DATA_ENTRY":
                            this.notifyTableDataEntryEvent(eventName, es);
                            break;
                        case "REFRESH_DATA":
                            if (flm.getAdvancedComponent().getType().equals("TABLE_COMP")) {
                                if (flm.getTableComponentConfiguration().getBindingModelId() != null) {
                                    if (flm.getTableComponentConfiguration().isProcessData()) {
                                        ModuleModel mm = this.loadModule(flm.getTableComponentConfiguration().getBindingModelId());

                                        FilterManager fm = new FilterManager();
                                        fm.setFilter("AND", new FilterType(mm.getModuleBaseTable() + "." + flm.getTableComponentConfiguration().getKeyFieldName(), this.recordModel.getFieldValueMap().get(GeneralUtils.getKeyFieldFromRecord(recordModel).getFieldName()).getCurrentValue()));
                                        RequestModel request = new RequestModel();
                                        request.setClause(fm.getClause());
                                        request.setRequestingModule(String.valueOf(mm.getModuleId()));
                                        request.setRequestActionType("0");
                                        RequestHandler requestHandler = new RequestHandler();
                                        ResponseModel response = requestHandler.executeRequest(request);
                                        if (response.getErrorCode() == 1000) {
//                                            this.tableRecordListMap.put(flm.getLayoutId(), response.getRecordList());
                                            this.tableRecordModelMap.put(flm.getLayoutId(), new LazyCustomTableRecord(response.getRecordList()));
                                        }

//                                        this.activeEvent = flm.getTableComponentConfiguration().getComponentName();
//                                        ModuleModel mm = this.loadModule(flm.getTableComponentConfiguration().getBindingModelId());
//                                        FilterManager fm = new FilterManager();
//                                        fm.setFilter("AND", new FilterType(mm.getModuleBaseTable() + "." + flm.getTableComponentConfiguration().getKeyFieldName(), this.recordModel.getFieldValueMap().get(flm.getTableComponentConfiguration().getKeyFieldName()).getCurrentValue()));
//                                        advCompLazyRecordModel = new LazyRecordModel("GenericMasterAPI", fm.getClause(), false, LazyRecordModel.RECORD, String.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));
//                                        this.loadRelationTableSettings(String.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));
                                    }
                                }

                            }
//                            PrimeFaces.current().ajax().update(es.getComponentId());
                            PrimeFaces.current().ajax().update("comp_" + flm.getLayoutId());
                            break;
                    }
                }
            }
        }
    }

    public void notifyTableSelectEvent(String eventName, EventSubscriber es) {
        FieldsLayoutModel flm = es.getFieldsLayoutModel();
        ModuleFieldModel eventKeyField = null;
        ArrayList<RecordModel> targetRecordList = new ArrayList<>();
        if (flm.getTableComponentConfiguration().getBindingModelId() != null && !flm.getTableComponentConfiguration().getBindingModelId().isEmpty()) {
            ArrayList<ModuleFieldModel> tableFieldSet = getFieldSetService(null, flm.getTableComponentConfiguration().getBindingModelId(), true);
            if (tableFieldSet != null) {
                for (RecordModel rm : eventResultListMap.get(eventName)) {
                    eventKeyField = GeneralUtils.getKeyFieldFromRecord(rm);
                    RecordModel tableRecordModel = new RecordModel();
                    LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
                    for (ModuleFieldModel mfm : tableFieldSet) {
                        if (mfm != null) {
                            fieldValueMap.put(mfm.getFieldName(), new ModuleFieldModel(mfm));
                            for (DataTableSettings dts : flm.getTableComponentConfiguration().getColumnSettings()) {
                                if (dts.getColumnFieldName() == null || dts.getColumnFieldName().isEmpty()) {
                                    if (dts.getDataMap() != null && dts.getDataMap().get(eventName) != null
                                            && dts.getDataMap().get(eventName).getTargetCol() != null && !dts.getDataMap().get(eventName).getTargetCol().isEmpty()) {
                                        if (dts.getColumnFieldName() == null || dts.getColumnFieldName().isEmpty()) {
                                            dts.setMappingField(dts.getDataMap().get(eventName).getTargetCol());
                                        }

                                        ModuleFieldModel mfmx = new ModuleFieldModel();
                                        mfmx.setFieldName(dts.getDataMap().get(eventName).getTargetCol());
                                        if (dts.isDynamicColumn()) {
                                            try {
                                                String dynamiKeyName = dts.getDataMap().get(eventName).getTargetCol();
                                                String script = dts.getDynamicScript();
                                                if (dts.getDynamicScript().contains("$this$.event")) {
                                                    script = dts.getDynamicScript().replaceAll("\\$(this)\\$.event", "'" + eventName + "'");
                                                    dynamiKeyName = "this_event";
                                                }
                                                BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                                                beezlInterperter.addDynamicField(dynamiKeyName, script);
                                                beezlInterperter.setRecodeModel(rm);
                                                this.populateFromAllFields();
                                                beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                                                LinkedHashMap<String, ArrayList<RecordModel>> moduleRecordMap = new LinkedHashMap<>();
                                                ArrayList<RecordModel> parentFormRecord = new ArrayList<>();
                                                parentFormRecord.add(recordModel);
                                                moduleRecordMap.put(this.moduleId, parentFormRecord);
                                                beezlInterperter.setModuleRecordMap(moduleRecordMap);
                                                beezlInterperter.setModuleManagersList(new ArrayList<>());
                                                beezlInterperter.getModuleManagersList().add(this.loadModule(moduleId));
                                                LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                                                mfmx.setCurrentValue(dynamicFieldMap.get(dynamiKeyName));
                                            }
                                            catch (Exception ex) {
                                                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                        }
                                        else {
                                            mfmx.setCurrentValue(rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()).getCurrentValue());
                                        }
                                        mfmx.setValueStyling(this.evaluateStylingRules(dts.getStyling(), rm));
                                        fieldValueMap.put(dts.getDataMap().get(eventName).getTargetCol(), mfmx);
                                    }
                                }
                                else if (dts.getColumnFieldName().equals(mfm.getFieldName())) {
                                    if (dts.getDataMap() != null && dts.getDataMap().get(eventName) != null) {
                                        if (dts.isDynamicColumn()) {
                                            try {
                                                String dynamiKeyName = dts.getDataMap().get(eventName).getTargetCol();
                                                String script = dts.getDynamicScript();
                                                if (dts.getDynamicScript().contains("$this$.event")) {
                                                    script = script.replaceAll("\\$(this)\\$.event", "'" + eventName + "'");
                                                    dynamiKeyName = "this_event";
                                                }
                                                BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                                                beezlInterperter.addDynamicField(dynamiKeyName, script);
                                                beezlInterperter.setRecodeModel(rm);
                                                this.populateFromAllFields();
                                                beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                                                LinkedHashMap<String, ArrayList<RecordModel>> moduleRecordMap = new LinkedHashMap<>();
                                                ArrayList<RecordModel> parentFormRecord = new ArrayList<>();
                                                parentFormRecord.add(recordModel);
                                                moduleRecordMap.put(this.moduleId, parentFormRecord);
                                                beezlInterperter.setModuleRecordMap(moduleRecordMap);
                                                beezlInterperter.setModuleManagersList(new ArrayList<>());
                                                beezlInterperter.getModuleManagersList().add(this.loadModule(moduleId));
                                                LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                                                fieldValueMap.get(mfm.getFieldName()).setCurrentValue(dynamicFieldMap.get(dynamiKeyName));
                                                fieldValueMap.get(mfm.getFieldName()).setValueStyling(this.evaluateStylingRules(dts.getStyling(), rm));
                                            }
                                            catch (Exception ex) {
                                                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                        }
                                        else {
                                            fieldValueMap.get(mfm.getFieldName()).setValueStyling(this.evaluateStylingRules(dts.getStyling(), rm));
                                            if (!dts.isEditable()) {
                                                fieldValueMap.get(mfm.getFieldName()).setCurrentValue(rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()).getCurrentValue());
                                                if (mfm.getFieldType().equals("LOOKUP")) {
                                                    fieldValueMap.put(mfm.getFieldName() + ".name", rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol() + ".name"));
                                                }
                                            }

                                            if (dts.isEditable()) {
                                                switch (dts.getComponentType()) {
                                                    case "SPINNER":
                                                        if (dts.getDataMap().get(eventName) != null && dts.getDataMap().get(eventName).getTargetCol() != null && !dts.getDataMap().get(eventName).getTargetCol().isEmpty()) {
                                                            if (rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()) != null
                                                                    && rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()).getCurrentValue() != null
                                                                    && !rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()).getCurrentValue().isEmpty()) {
                                                                fieldValueMap.get(mfm.getFieldName()).setCurrentValue(rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()).getCurrentValue());
                                                            }
                                                            else {
                                                                fieldValueMap.get(mfm.getFieldName()).setCurrentValue("0");
                                                            }
                                                        }
                                                        else {
                                                            fieldValueMap.get(mfm.getFieldName()).setCurrentValue("0");
                                                        }
                                                        break;
                                                    case "TEXT_FIELD":
                                                        if (dts.getDataMap().get(eventName) != null && dts.getDataMap().get(eventName).getTargetCol() != null && !dts.getDataMap().get(eventName).getTargetCol().isEmpty()) {
                                                            if (rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()) != null
                                                                    && rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()).getCurrentValue() != null
                                                                    && !rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()).getCurrentValue().isEmpty()) {
                                                                fieldValueMap.get(mfm.getFieldName()).setCurrentValue(rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()).getCurrentValue());
                                                            }
                                                            else {
                                                                fieldValueMap.get(mfm.getFieldName()).setCurrentValue("");
                                                            }
                                                        }
                                                        else {
                                                            fieldValueMap.get(mfm.getFieldName()).setCurrentValue("");
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }

                            }
                        }
                    }
                    tableRecordModel.setFieldValueMap(fieldValueMap);
                    targetRecordList.add(tableRecordModel);
                }
            }
        }
//        ArrayList<RecordModel> sourceRecordList = this.tableRecordListMap.get(flm.getLayoutId());
/*
         */
        ArrayList<RecordModel> sourceRecordList = this.tableRecordModelMap.get(flm.getLayoutId()).getData();
        if (flm.getTableComponentConfiguration().isEnableDuplicateCheck()) {
            if (sourceRecordList != null && !sourceRecordList.isEmpty()) {
                if (eventKeyField != null && !targetRecordList.isEmpty()) {
                    ArrayList<RecordModel> cleanRecordList = new ArrayList<>();
                    for (RecordModel targetRecord : targetRecordList) {
                        boolean keyFound = false;
                        for (RecordModel sourceRecord : sourceRecordList) {
                            if (GeneralUtils.getFieldFromRecord(targetRecord, eventKeyField.getFieldName()).getCurrentValue().equals(GeneralUtils.getFieldFromRecord(sourceRecord, eventKeyField.getFieldName()).getCurrentValue())) {
                                keyFound = true;
                                break;
                            }
                        }
                        if (!keyFound) {
                            cleanRecordList.add(targetRecord);
                        }
                    }
                    targetRecordList.clear();
                    targetRecordList.addAll(cleanRecordList);
                    targetRecordList.addAll(sourceRecordList);
                }
                else {
                    targetRecordList.addAll(sourceRecordList);
                }
            }
        }
        else {
            if (sourceRecordList != null && !sourceRecordList.isEmpty()) {
                targetRecordList.addAll(sourceRecordList);
            }
        }

        //11/08/2020: Make sure that dynamic columns are evaluated in the target table records
        for (RecordModel rm : targetRecordList) {
            for (DataTableSettings dts : flm.getTableComponentConfiguration().getColumnSettings()) {
                String dynamiKeyName = "";
                if (dts.isDynamicColumn()) {
                    if (dts.getDataMap().get(eventName).getTargetCol() != null && !dts.getDataMap().get(eventName).getTargetCol().isEmpty()) {
                        dynamiKeyName = dts.getDataMap().get(eventName).getTargetCol();
                    }
                    else {
                        dynamiKeyName = dts.getColumnFieldName();
                    }
                    String script = dts.getDynamicScript();
                    BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                    if (dts.getDynamicScript().contains("$this$.event")) {
                        dts.setDynamicScript(dts.getDynamicScript().replaceAll("\\$(this)\\$.event", "'" + eventName + "'"));
                        script = dts.getDynamicScript();
                    }
                    beezlInterperter.addDynamicField(dynamiKeyName, script);
                    beezlInterperter.setRecodeModel(rm);
                    this.populateFromAllFields();
                    beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                    LinkedHashMap<String, ArrayList<RecordModel>> moduleRecordMap = new LinkedHashMap<>();
                    ArrayList<RecordModel> parentFormRecord = new ArrayList<>();
                    parentFormRecord.add(recordModel);
                    moduleRecordMap.put(this.moduleId, parentFormRecord);
                    beezlInterperter.setModuleRecordMap(moduleRecordMap);
                    beezlInterperter.setModuleManagersList(new ArrayList<>());
                    beezlInterperter.getModuleManagersList().add(this.loadModule(moduleId));
                    try {
                        LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                        rm.getFieldValueMap().get(dynamiKeyName).setCurrentValue(dynamicFieldMap.get(dynamiKeyName));
                    }
                    catch (Exception ex) {
                        Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

//        this.tableRecordListMap.put(flm.getLayoutId(), targetRecordList);
        this.tableRecordModelMap.put(flm.getLayoutId(), new LazyCustomTableRecord(targetRecordList));
        PrimeFaces.current().ajax().update(es.getComponentId());
        if (flm.getTableComponentConfiguration().getComponentName() != null && !flm.getTableComponentConfiguration().getComponentName().isEmpty()) {
            this.eventResultListMap.put(flm.getTableComponentConfiguration().getComponentName(), targetRecordList);
            this.notifyEventSubscriberData(flm.getTableComponentConfiguration().getComponentName());
        }
    }

    public void notifyTableInheritEvent(String eventName, EventSubscriber es) {
        FieldsLayoutModel flm = es.getFieldsLayoutModel();
        ArrayList<RecordModel> targetRecordList = new ArrayList<>();
        for (RecordModel rm : eventResultListMap.get(eventName)) {
            RecordModel tableRecordModel = new RecordModel();
            LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
            for (DataTableSettings dts : flm.getTableComponentConfiguration().getColumnSettings()) {
                if (dts.getDataMap() != null && dts.getDataMap().get(eventName) != null) {
                    if (dts.getColumnFieldName() == null || dts.getColumnFieldName().isEmpty()) {
                        dts.setMappingField(dts.getDataMap().get(eventName).getTargetCol());
                    }

                    ModuleFieldModel mfmx = new ModuleFieldModel();
                    mfmx.setFieldName(dts.getDataMap().get(eventName).getTargetCol());
                    if (dts.isDynamicColumn()) {
                        try {
                            BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                            beezlInterperter.addDynamicField(dts.getMappingField(), dts.getDynamicScript());
                            beezlInterperter.setRecodeModel(rm);
                            this.populateFromAllFields();
                            beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                            LinkedHashMap<String, ArrayList<RecordModel>> moduleRecordMap = new LinkedHashMap<>();
                            ArrayList<RecordModel> parentFormRecord = new ArrayList<>();
                            parentFormRecord.add(recordModel);
                            moduleRecordMap.put(this.moduleId, parentFormRecord);
                            beezlInterperter.setModuleRecordMap(moduleRecordMap);
                            beezlInterperter.setModuleManagersList(new ArrayList<>());
                            beezlInterperter.getModuleManagersList().add(this.loadModule(moduleId));
                            LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                            mfmx.setCurrentValue(dynamicFieldMap.get(dts.getDataMap().get(eventName).getTargetCol()));
                        }
                        catch (Exception ex) {
                            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else {
                        if (rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()) != null) {
                            mfmx.setCurrentValue(rm.getFieldValueMap().get(dts.getDataMap().get(eventName).getTargetCol()).getCurrentValue());
                        }
                    }
                    fieldValueMap.put(dts.getDataMap().get(eventName).getTargetCol(), mfmx);
                }
            }
            tableRecordModel.setFieldValueMap(fieldValueMap);
            targetRecordList.add(tableRecordModel);
        }

        if (flm.getTableComponentConfiguration().getSummaryColumnsSettings() != null) {
            RecordModel summaryRM = new RecordModel();
            LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
            for (DataTableSettings dts : flm.getTableComponentConfiguration().getSummaryColumnsSettings()) {
                if (dts.isAggregateColumn()) {
                    dts.setValueHolder(this.evaluateAggregate(Integer.valueOf(es.getComponentId().replaceAll("comp_", "")), dts.getColumnFieldName()));
                }
            }
        }

//        this.tableRecordListMap.put(flm.getLayoutId(), targetRecordList);
        this.tableRecordModelMap.put(flm.getLayoutId(), new LazyCustomTableRecord(targetRecordList));
        PrimeFaces.current().ajax().update(es.getComponentId());
        if (flm.getTableComponentConfiguration().getComponentName() != null && !flm.getTableComponentConfiguration().getComponentName().isEmpty()) {
            this.eventResultListMap.put(flm.getTableComponentConfiguration().getComponentName(), targetRecordList);
            this.notifyEventSubscriberData(flm.getTableComponentConfiguration().getComponentName());
        }
    }

    public void notifyTableDataEntryEvent(String eventName, EventSubscriber es) {
        FieldsLayoutModel flm = es.getFieldsLayoutModel();
        ArrayList<RecordModel> targetRecordList = new ArrayList<>();
        if (eventResultListMap.get(flm.getTableComponentConfiguration().getComponentName()) != null && !eventResultListMap.get(flm.getTableComponentConfiguration().getComponentName()).isEmpty()) {
            for (RecordModel rm : eventResultListMap.get(flm.getTableComponentConfiguration().getComponentName())) {

                targetRecordList.add(rm);
//                eventResultListMap.get(flm.getTableComponentConfiguration().getComponentName()).add(tableRecordModel);
            }
            RecordModel tableRecordModel = new RecordModel();
            LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
            for (DataTableSettings dts : flm.getTableComponentConfiguration().getColumnSettings()) {
                if (dts.getDataMap() != null && dts.getDataMap().get(eventName) != null) {
                    if (dts.getColumnFieldName() == null || dts.getColumnFieldName().isEmpty()) {
                        dts.setMappingField(dts.getDataMap().get(eventName).getTargetCol());
                    }

//                    ModuleFieldModel mfmx = new ModuleFieldModel();
                    ModuleFieldModel mfmx = GeneralUtils.getFieldType(dts.getColumnFieldName(), Integer.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));
//                    mfmx.setFieldName(dts.getDataMap().get(eventName).getTargetCol());
                    if (dts.isDynamicColumn()) {
                        try {
                            BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                            beezlInterperter.addDynamicField(dts.getMappingField(), dts.getDynamicScript());
                            LinkedHashMap<String, ArrayList<RecordModel>> moduleRecordMap = new LinkedHashMap<>();
                            moduleRecordMap.put(flm.getTableComponentConfiguration().getBindingModelId(), eventResultListMap.get(flm.getTableComponentConfiguration().getComponentName()));
                            beezlInterperter.setModuleRecordMap(moduleRecordMap);
                            LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                            mfmx.setCurrentValue(dynamicFieldMap.get(dts.getDataMap().get(eventName).getTargetCol()));
                        }
                        catch (Exception ex) {
                            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else {
                        mfmx.setCurrentValue(null);
                    }
                    fieldValueMap.put(mfmx.getFieldName(), mfmx);
                }
            }
            tableRecordModel.setFieldValueMap(fieldValueMap);
            targetRecordList.add(tableRecordModel);
        }
        else {
            RecordModel tableRecordModel = new RecordModel();
            LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
            for (DataTableSettings dts : flm.getTableComponentConfiguration().getColumnSettings()) {
                if (dts.getDataMap() != null && dts.getDataMap().get(eventName) != null) {
                    if (dts.getColumnFieldName() == null || dts.getColumnFieldName().isEmpty()) {
                        dts.setMappingField(dts.getDataMap().get(eventName).getTargetCol());
                    }

//                    ModuleFieldModel mfmx = new ModuleFieldModel();
                    ModuleFieldModel mfmx = GeneralUtils.getFieldType(dts.getColumnFieldName(), Integer.valueOf(flm.getTableComponentConfiguration().getBindingModelId()));

//                    mfmx.setFieldName(dts.getDataMap().get(eventName).getTargetCol());
                    fieldValueMap.put(mfmx.getFieldName(), mfmx);
                }
            }
            tableRecordModel.setFieldValueMap(fieldValueMap);
            targetRecordList.add(tableRecordModel);
        }

        if (flm.getTableComponentConfiguration().getSummaryColumnsSettings() != null) {
            RecordModel summaryRM = new RecordModel();
            LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
            for (DataTableSettings dts : flm.getTableComponentConfiguration().getSummaryColumnsSettings()) {
                if (dts.isAggregateColumn()) {
                    dts.setValueHolder(this.evaluateAggregate(Integer.valueOf(es.getComponentId().replaceAll("comp_", "")), dts.getColumnFieldName()));
                }
            }
        }

//        this.tableRecordListMap.put(flm.getLayoutId(), targetRecordList);
        this.tableRecordModelMap.put(flm.getLayoutId(), new LazyCustomTableRecord(targetRecordList));
        PrimeFaces.current().ajax().update(es.getComponentId());
        if (flm.getTableComponentConfiguration().getComponentName() != null && !flm.getTableComponentConfiguration().getComponentName().isEmpty()) {
//            if (eventResultListMap.get(flm.getTableComponentConfiguration().getComponentName()) != null && !eventResultListMap.get(flm.getTableComponentConfiguration().getComponentName()).isEmpty()) {
//                eventResultListMap.get(flm.getTableComponentConfiguration().getComponentName()).addAll(targetRecordList);
//            }
//            else {
//                this.eventResultListMap.put(flm.getTableComponentConfiguration().getComponentName(), targetRecordList);
//            }
            this.eventResultListMap.put(flm.getTableComponentConfiguration().getComponentName(), targetRecordList);
            this.notifyEventSubscriberData(flm.getTableComponentConfiguration().getComponentName());
        }
    }

    public String evaluateStylingRules(String stylingScript, RecordModel rm) {
        if (stylingScript != null && !stylingScript.isEmpty()) {
            try {
                BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                beezlInterperter.addDynamicField("style", stylingScript);
                beezlInterperter.setRecodeModel(rm);
                LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                stylingScript = dynamicFieldMap.get("style");
            }
            catch (Exception ex) {
                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return stylingScript;
    }

    public String evaluateAggregate(int layoutId, String colName) {
        for (DataTableSettings dts : this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getSummaryColumnsSettings()) {
            if (dts.getColumnFieldName() != null && !dts.getColumnFieldName().isEmpty()) {
                if (dts.getColumnFieldName().equals(colName)) {
                    if (dts.getAggregateFunction().equals("SUM")) {
//                    dts.setValueHolder(String.valueOf(RecordAggregateFunctions.sumOfField(this.tableRecordListMap.get(layoutId), dts.getMappingField())));
//                    return String.valueOf(RecordAggregateFunctions.sumOfField(this.tableRecordListMap.get(layoutId), dts.getMappingField()));
                        dts.setValueHolder(String.valueOf(RecordAggregateFunctions.sumOfField(this.tableRecordModelMap.get(layoutId).getData(), dts.getMappingField())));
                        return String.valueOf(RecordAggregateFunctions.sumOfField(this.tableRecordModelMap.get(layoutId).getData(), dts.getMappingField()));
                    }
                }
            }
            else {
                if (dts.getMappingField().equals(colName)) {
                    if (dts.getAggregateFunction().equals("SUM")) {
//                    dts.setValueHolder(String.valueOf(RecordAggregateFunctions.sumOfField(this.tableRecordListMap.get(layoutId), dts.getMappingField())));
//                    return String.valueOf(RecordAggregateFunctions.sumOfField(this.tableRecordListMap.get(layoutId), dts.getMappingField()));
                        dts.setValueHolder(String.valueOf(RecordAggregateFunctions.sumOfField(this.tableRecordModelMap.get(layoutId).getData(), dts.getMappingField())));
                        return String.valueOf(RecordAggregateFunctions.sumOfField(this.tableRecordModelMap.get(layoutId).getData(), dts.getMappingField()));
                    }
                }
            }
        }
        return "0";
    }

    public String evaluateDynamicField(int layoutId, String colLabel) {
        RecordModel summaryrm = new RecordModel();
        LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
        for (DataTableSettings dts : this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getSummaryColumnsSettings()) {
            if (dts.isAggregateColumn()) {
//                dts.setValueHolder(String.valueOf(RecordAggregateFunctions.sumOfField(this.tableRecordListMap.get(layoutId), dts.getMappingField())));
                dts.setValueHolder(String.valueOf(RecordAggregateFunctions.sumOfField(this.tableRecordModelMap.get(layoutId).getData(), dts.getMappingField())));
                ModuleFieldModel mfm = new ModuleFieldModel();
                mfm.setFieldName(dts.getColumnFieldName() != null && !dts.getColumnFieldName().isEmpty() ? dts.getColumnFieldName() : dts.getMappingField());
                mfm.setCurrentValue(dts.getValueHolder());
                mfm.setFieldType("TEXT");
                fieldValueMap.put(dts.getColumnFieldName() != null && !dts.getColumnFieldName().isEmpty() ? dts.getColumnFieldName() : dts.getMappingField(), mfm);
            }
            else {
                ModuleFieldModel mfm = new ModuleFieldModel();
                mfm.setFieldName(dts.getColumnFieldName());
                mfm.setCurrentValue(dts.getValueHolder());
                mfm.setFieldType("TEXT");
                fieldValueMap.put(dts.getColumnFieldName(), mfm);
            }
        }

        summaryrm.setFieldValueMap(fieldValueMap);
        for (DataTableSettings dts : this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getSummaryColumnsSettings()) {
            if (dts.getColumnFieldName().equals(colLabel)) {
                if (dts.getDynamicScript() != null && !dts.getDynamicScript().isEmpty()) {
                    try {
                        BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                        beezlInterperter.addDynamicField(dts.getColumnFieldName(), dts.getDynamicScript());
                        beezlInterperter.setRecodeModel(summaryrm);
                        beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                        LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                        dts.setValueHolder(dynamicFieldMap.get(dts.getColumnFieldName()));
                        return dynamicFieldMap.get(dts.getColumnFieldName());
                    }
                    catch (Exception ex) {
                        Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        return "0";
    }

    public void evaluateDynamicFieldForColumns(int layoutId, String colLabel) {
//        RecordModel tableRm = new RecordModel();
//        LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
//for (RecordModel tableRm : tableRecordListMap.get(layoutId))
        for (RecordModel tableRm : tableRecordModelMap.get(layoutId).getData()) {
            for (DataTableSettings dts : this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getColumnSettings()) {
                if (dts.getDynamicScript() != null && !dts.getDynamicScript().isEmpty()) {
                    try {
                        BEEZLInterperter beezlInterperter = new BEEZLInterperter(Defines.GENERAL_CONFIG_MAP.get("services.server.path"));
                        beezlInterperter.addDynamicField(dts.getColumnFieldName(), dts.getDynamicScript());
                        beezlInterperter.setRequestingUser(GeneralUtils.getLoggedUser());
                        beezlInterperter.setRecodeModel(tableRm);
                        LinkedHashMap<String, String> dynamicFieldMap = beezlInterperter.evaluate();
                        dts.setValueHolder(dynamicFieldMap.get(dts.getColumnFieldName()));
                        tableRm.getFieldValueMap().get(dts.getColumnFieldName()).setCurrentValue(dynamicFieldMap.get(dts.getColumnFieldName()));
                        String styling = this.evaluateStylingRules(dts.getStyling(), tableRm);
                        tableRm.getFieldValueMap().get(dts.getColumnFieldName()).setValueStyling(styling);
                    }
                    catch (Exception ex) {
                        Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else {
                    if (dts.getStyling() != null && !dts.getStyling().isEmpty()) {
                        String styling = this.evaluateStylingRules(dts.getStyling(), tableRm);
                        tableRm.getFieldValueMap().get(dts.getColumnFieldName()).setValueStyling(styling);
                    }
                }

            }
        }
    }

    public void onTableRecordDelete(int layoutId, RecordModel selRec) {
        RequestModel request = new RequestModel();
        ArrayList<RecordModel> bulkTemp = new ArrayList<>();
        bulkTemp.add(selRec);
        request.setBulkRecords(bulkTemp);
        request.setRequestActionType("2");
        request.setRequestingModule(this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getBindingModelId());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = requestHandler.executeRequest(request);
        if (response.getErrorCode() == 1000) {
//            this.tableRecordListMap.get(layoutId).remove(selRec);
            this.tableRecordModelMap.get(layoutId).getData().remove(selRec);
            PrimeFaces.current().ajax().update("comp_" + layoutId);
            this.notifyEventSubscriberData(this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getComponentName());
        }
    }

    public void onTableEditEvent(int layoutId) {
//        PrimeFaces.current().ajax().update("comp_" + layoutId);
//        PrimeFaces.current().executeScript("comp_" + layoutId + "()");
        this.notifyEventSubscriberData(this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getComponentName());
        this.evaluateDynamicFieldForColumns(layoutId, null);
        if (this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getSummaryColumnsSettings() != null) {
            RecordModel summaryRM = new RecordModel();
            LinkedHashMap<String, ModuleFieldModel> fieldValueMap = new LinkedHashMap<>();
            for (DataTableSettings dts : this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getSummaryColumnsSettings()) {
                if (dts.isAggregateColumn()) {
                    if (dts.getColumnFieldName() != null && !dts.getColumnFieldName().isEmpty()) {
                        dts.setValueHolder(this.evaluateAggregate(layoutId, dts.getColumnFieldName()));
                    }
                    else {
                        if (dts.getMappingField() != null && !dts.getMappingField().isEmpty()) {
                            dts.setValueHolder(this.evaluateAggregate(layoutId, dts.getMappingField()));
                        }
                    }
                }
            }
        }

        if (!this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().isProcessData()) {
        }
    }

    public void onTableEditEventForBoolean(int layoutId, RecordModel record) {
        if (this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().isProcessData()
                && this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getBindingModelId() != null
                && !this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getBindingModelId().isEmpty()) {

            RequestModel request = new RequestModel();
            request.setRequestingModule(this.advancedConfigMap.get(layoutId).getTableComponentConfiguration().getBindingModelId());
            request.setRecordModel(record);
            request.setRequestActionType("3");
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response = requestHandler.executeRequest(request);
        }
    }

    public void downloadUploadTemplate() {
        if (super.getFieldsList() == null) {
            this.loadFieldSet(this.getModuleId(), null);
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(baos));
        int index = 0;
        for (ModuleFieldModel mfm : super.getFieldsList()) {
            try {
                if (index == 0) {
                    if (mfm.isIsKey()) {
                        continue;
                    }
                    else if (mfm.isChronicalField()) {
                        continue;
                    }
                    else {
                        writer.append(mfm.getFieldName());
                    }
                }
                else {
                    if (mfm.isIsKey()) {
                        continue;
                    }
                    else if (mfm.isChronicalField()) {
                        continue;
                    }
                    else {
                        writer.append(",");
                        writer.append(mfm.getFieldName());
                    }
                }
            }
            catch (IOException ex) {
                Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
            }
            index++;
        }

        try {
            writer.close();
            byte[] bytes = baos.toByteArray();
            this.uploadTemplate = DefaultStreamedContent.builder()
                    .name("template.csv")
                    .contentType("text/csv")
                    .stream(() -> new ByteArrayInputStream(
                    bytes))
                    .build();
//            this.uploadTemplate = new DefaultStreamedContent(new ByteArrayInputStream(bytes), "text/csv", "template.csv");
        }
        catch (IOException ex) {
            Logger.getLogger(BeanFramework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void handleFileUpload(FileUploadEvent event) {
        UnstructuredModel cm = new UnstructuredModel();
        cm.setFileExtenstion(GeneralUtils.getFileExtenstion(event.getFile().getFileName()));
        cm.setFileName(event.getFile().getFileName());
        cm.setFileSize(event.getFile().getSize());
        cm.setContentStream(event.getFile().getContent());

        RequestModel request = new RequestModel();
        request.setRequestingModule(this.getModuleId());
        request.setUnstructuredData(cm);
        request.setRequestActionType("5");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response = requestHandler.executeRequest(request);
        if (response.getErrorCode() == 1000) {
            FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        else {
            this.bulkUploadErrorMessage = response.getErrorMessage();
            PrimeFaces.current().ajax().update("bulkUploadValidationResultDialog");
            PrimeFaces.current().executeScript("PF('bulkUploadValidationResultDialog').show()");
//            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", event.getFile().getFileName() + " Failed to upload");
//            FacesContext.getCurrentInstance().addMessage(null, message);
        }
    }

    public String concatLookupNameHandle(String in) {
        return in + ".name";
    }

    public String evaluateMappedColumnForLookup(DataTableSettings dts) {
        if (dts != null) {
            if (this.activeEvent != null) {
                if (dts.getDataMap() != null && !dts.getDataMap().isEmpty() && dts.getDataMap().get(activeEvent) != null) {
                    return dts.getDataMap().get(activeEvent).getTargetCol();
                }
            }
        }
        return "";
    }

    /**
     * *******************************************************
     *
     * Start of Getter & Setters.
     *
     * ********************************************************
     * @return
     */
    public RecordModel getRecordModel() {
        return recordModel;
    }

    public void setRecordModel(RecordModel recordModel) {
        this.recordModel = recordModel;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public HashMap<String, TreeMap<String, String>> getLookupDictionary() {
        return lookupDictionary;
    }

    public void setLookupDictionary(HashMap<String, TreeMap<String, String>> lookupDictionary) {
        this.lookupDictionary = lookupDictionary;
    }

    public HashMap<String, HashMap<String, String>> getRelationDictionary() {
        return relationDictionary;
    }

    public void setRelationDictionary(HashMap<String, HashMap<String, String>> relationDictionary) {
        this.relationDictionary = relationDictionary;
    }

    public HashMap<String, String> getSelectedCache() {
        return selectedCache;
    }

    public void setSelectedCache(HashMap<String, String> selectedCache) {
        this.selectedCache = selectedCache;
    }

    public HashMap<String, ArrayList<LookupModel>> getLookupCache() {
        return lookupCache;
    }

    public void setLookupCache(HashMap<String, ArrayList<LookupModel>> lookupCache) {
        this.lookupCache = lookupCache;
    }

    public HashMap<String, RelationResponse> getRelationRecordsCache() {
        return relationRecordsCache;
    }

    public void setRelationRecordsCache(HashMap<String, RelationResponse> relationRecordsCache) {
        this.relationRecordsCache = relationRecordsCache;
    }

    public HashMap<String, SelectionModel> getSelectionMap() {
        return selectionMap;
    }

    public void setSelectionMap(HashMap<String, SelectionModel> selectionMap) {
        this.selectionMap = selectionMap;
    }

    public RecordModel getSelectedRelationRecord() {
        return selectedRelationRecord;
    }

    public void setSelectedRelationRecord(RecordModel selectedRelationRecord) {
        this.selectedRelationRecord = selectedRelationRecord;
    }

    public ArrayList<FormModel> getFormList() {
        return formList;
    }

    public void setFormList(ArrayList<FormModel> formList) {
        this.formList = formList;
    }

    public FormModel getDefaultForm() {
        return defaultForm;
    }

    public void setDefaultForm(FormModel defaultForm) {
        this.defaultForm = defaultForm;
    }

    public HashMap<Integer, ArrayList<FieldsLayoutModel>> getLayoutFieldsMap() {
        return layoutFieldsMap;
    }

    public void setLayoutFieldsMap(HashMap<Integer, ArrayList<FieldsLayoutModel>> layoutFieldsMap) {
        this.layoutFieldsMap = layoutFieldsMap;
    }

    public HashMap<String, ArrayList<CategoryModel>> getCategoriesCache() {
        return categoriesCache;
    }

    public void setCategoriesCache(HashMap<String, ArrayList<CategoryModel>> categoriesCache) {
        this.categoriesCache = categoriesCache;
    }

    public HashMap<String, HashMap<String, String>> getCategoryDictionary() {
        return categoryDictionary;
    }

    public void setCategoryDictionary(HashMap<String, HashMap<String, String>> categoryDictionary) {
        this.categoryDictionary = categoryDictionary;
    }

    public HashMap<String, RecordModel> getSelectedRecordsMap() {
        return selectedRecordsMap;
    }

    public void setSelectedRecordsMap(HashMap<String, RecordModel> selectedRecordsMap) {
        this.selectedRecordsMap = selectedRecordsMap;
    }

    public HashMap<String, String> getCatalogsNameMap() {
        return catalogsNameMap;
    }

    public void setCatalogsNameMap(HashMap<String, String> catalogsNameMap) {
        this.catalogsNameMap = catalogsNameMap;
    }

    public Map<String, String> getRequestParams() {
        return requestParams;
    }

    public void setRequestParams(Map<String, String> requestParams) {
        this.requestParams = requestParams;
    }

    public String getRequestMode() {
        return requestMode;
    }

    public void setRequestMode(String requestMode) {
        this.requestMode = requestMode;
    }

    public HashMap<UnstructuredModel, UploadedFile> getContentCache() {
        return contentCache;
    }

    public void setContentCache(HashMap<UnstructuredModel, UploadedFile> contentCache) {
        this.contentCache = contentCache;
    }

    public String getFTSKeyword() {
        return FTSKeyword;
    }

    public void setFTSKeyword(String FTSKeyword) {
        this.FTSKeyword = FTSKeyword;
    }

    public LazyRecordModel getLazyRecordModel() {
        return lazyRecordModel;
    }

    public void setLazyRecordModel(LazyRecordModel lazyRecordModel) {
        this.lazyRecordModel = lazyRecordModel;
    }

    public LazyRecordModel getRelationLazyRecordModel() {
        return relationLazyRecordModel;
    }

    public void setRelationLazyRecordModel(LazyRecordModel relationLazyRecordModel) {
        this.relationLazyRecordModel = relationLazyRecordModel;
    }

    public ArrayList<DataTableSettings> getRelationTableSettings() {
        return relationTableSettings;
    }

    public void setRelationTableSettings(ArrayList<DataTableSettings> relationTableSettings) {
        this.relationTableSettings = relationTableSettings;
    }

    public String getRelationFTSKeyword() {
        return relationFTSKeyword;
    }

    public void setRelationFTSKeyword(String relationFTSKeyword) {
        this.relationFTSKeyword = relationFTSKeyword;
    }

    public String getMasterPageName() {
        return masterPageName;
    }

    public void setMasterPageName(String masterPageName) {
        this.masterPageName = masterPageName;
    }

    public boolean isQuickAdd() {
        return quickAdd;
    }

    public void setQuickAdd(boolean quickAdd) {
        this.quickAdd = quickAdd;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getRelationModuleId() {
        return relationModuleId;
    }

    public void setRelationModuleId(String relationModuleId) {
        this.relationModuleId = relationModuleId;
    }

    public void setAddDataBean(AddDataBean addDataBean) {
        this.addDataBean = addDataBean;
    }

    public AddDataBean getAddDataBean() {
        return addDataBean;
    }

    public String getDataDialogWidgetVar() {
        return dataDialogWidgetVar;
    }

    public void setDataDialogWidgetVar(String dataDialogWidgetVar) {
        this.dataDialogWidgetVar = dataDialogWidgetVar;
    }

    public HashMap<String, AddDataBean> getRelationBeanCache() {
        return relationBeanCache;
    }

    public void setRelationBeanCache(HashMap<String, AddDataBean> relationBeanCache) {
        this.relationBeanCache = relationBeanCache;
    }

    public HashMap<String, SelectionModel> getSelectedMultiRelationMap() {
        return selectedMultiRelationMap;
    }

    public void setSelectedMultiRelationMap(HashMap<String, SelectionModel> selectedMultiRelationMap) {
        this.selectedMultiRelationMap = selectedMultiRelationMap;
    }

    public TableViewDesignerBean getTableViewDesignerBean() {
        return tableViewDesignerBean;
    }

    public void setTableViewDesignerBean(TableViewDesignerBean tableViewDesignerBean) {
        this.tableViewDesignerBean = tableViewDesignerBean;
    }

    public String getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(String recordCount) {
        this.recordCount = recordCount;
    }

    public LazyRecordModel getAdvCompLazyRecordModel() {
        return advCompLazyRecordModel;
    }

    public void setAdvCompLazyRecordModel(LazyRecordModel advCompLazyRecordModel) {
        this.advCompLazyRecordModel = advCompLazyRecordModel;
    }

    public HashMap<String, ArrayList<RecordModel>> getEventResultListMap() {
        return eventResultListMap;
    }

    public void setEventResultListMap(HashMap<String, ArrayList<RecordModel>> eventResultListMap) {
        this.eventResultListMap = eventResultListMap;
    }

    public String getActiveEvent() {
        return activeEvent;
    }

    public void setActiveEvent(String activeEvent) {
        this.activeEvent = activeEvent;
    }

    public LinkedHashMap<Integer, FieldsLayoutModel> getAdvancedConfigMap() {
        return advancedConfigMap;
    }

    public void setAdvancedConfigMap(LinkedHashMap<Integer, FieldsLayoutModel> advancedConfigMap) {
        this.advancedConfigMap = advancedConfigMap;
    }

    public HashMap<Integer, ArrayList<RecordModel>> getTableRecordListMap() {
        return tableRecordListMap;
    }

    public void setTableRecordListMap(HashMap<Integer, ArrayList<RecordModel>> tableRecordListMap) {
        this.tableRecordListMap = tableRecordListMap;
    }

    public HashMap<Integer, LazyCustomTableRecord> getTableRecordModelMap() {
        return tableRecordModelMap;
    }

    public void setTableRecordModelMap(HashMap<Integer, LazyCustomTableRecord> tableRecordModelMap) {
        this.tableRecordModelMap = tableRecordModelMap;
    }

    public HashMap<String, ArrayList<EventSubscriber>> getEventSubscriberMap() {
        return eventSubscriberMap;
    }

    public void setEventSubscriberMap(HashMap<String, ArrayList<EventSubscriber>> eventSubscriberMap) {
        this.eventSubscriberMap = eventSubscriberMap;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyValue() {
        return keyValue;
    }

    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }

    public boolean isRelationView() {
        return relationView;
    }

    public void setRelationView(boolean relationView) {
        this.relationView = relationView;
    }

    public StreamedContent getUploadTemplate() {
        return uploadTemplate;
    }

    public void setUploadTemplate(StreamedContent uploadTemplate) {
        this.uploadTemplate = uploadTemplate;
    }

    public UploadedFile getUploadedFile() {
        return uploadedFile;
    }

    public void setUploadedFile(UploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public String getTableViewDesignerBeanSelectedModule() {
        return tableViewDesignerBeanSelectedModule;
    }

    public void setTableViewDesignerBeanSelectedModule(String tableViewDesignerBeanSelectedModule) {
        this.tableViewDesignerBeanSelectedModule = tableViewDesignerBeanSelectedModule;
    }

    public boolean isPermitCreate() {
        return permitCreate;
    }

    public void setPermitCreate(boolean permitCreate) {
        this.permitCreate = permitCreate;
    }

    public HashMap<Integer, String> getTableNameMap() {
        return tableNameMap;
    }

    public void setTableNameMap(HashMap<Integer, String> tableNameMap) {
        this.tableNameMap = tableNameMap;
    }

    public String getModuleBreadCrumbName() {
        return moduleBreadCrumbName;
    }

    public void setModuleBreadCrumbName(String moduleBreadCrumbName) {
        this.moduleBreadCrumbName = moduleBreadCrumbName;
    }

    public boolean isAutoRedirectForm() {
        return autoRedirectForm;
    }

    public void setAutoRedirectForm(boolean autoRedirectForm) {
        this.autoRedirectForm = autoRedirectForm;
    }

    public RecordDetailBean getOriginatingDetailBean() {
        return originatingDetailBean;
    }

    public void setOriginatingDetailBean(RecordDetailBean originatingDetailBean) {
        this.originatingDetailBean = originatingDetailBean;
    }

    public HashMap<String, ArrayList<RecordModel>> getAutoCompleteListMap() {
        return autoCompleteListMap;
    }

    public void setAutoCompleteListMap(HashMap<String, ArrayList<RecordModel>> autoCompleteListMap) {
        this.autoCompleteListMap = autoCompleteListMap;
    }

    public HashMap<String, String> getAutoCompleteValueMap() {
        return autoCompleteValueMap;
    }

    public void setAutoCompleteValueMap(HashMap<String, String> autoCompleteValueMap) {
        this.autoCompleteValueMap = autoCompleteValueMap;
    }

    public int getDynoGetDataNumberOfRecords() {
        return dynoGetDataNumberOfRecords;
    }

    public void setDynoGetDataNumberOfRecords(int dynoGetDataNumberOfRecords) {
        this.dynoGetDataNumberOfRecords = dynoGetDataNumberOfRecords;
    }

    public RecordModel getScannedRecord() {
        return scannedRecord;
    }

    public void setScannedRecord(RecordModel scannedRecord) {
        this.scannedRecord = scannedRecord;
    }

    public int getActiveComponentId() {
        return activeComponentId;
    }

    public void setActiveComponentId(int activeComponentId) {
        this.activeComponentId = activeComponentId;
    }

    public HashMap<Integer, TableComponentHelper> getTableHelpersMap() {
        return tableHelpersMap;
    }

    public void setTableHelpersMap(HashMap<Integer, TableComponentHelper> tableHelpersMap) {
        this.tableHelpersMap = tableHelpersMap;
    }

    public HashMap<String, ArrayList<String>> getSimpleLookupCache() {
        return simpleLookupCache;
    }

    public void setSimpleLookupCache(HashMap<String, ArrayList<String>> simpleLookupCache) {
        this.simpleLookupCache = simpleLookupCache;
    }

    public HashMap<String, SelectionModel> getAdvancedTableRelationSelectionMap() {
        return advancedTableRelationSelectionMap;
    }

    public void setAdvancedTableRelationSelectionMap(HashMap<String, SelectionModel> advancedTableRelationSelectionMap) {
        this.advancedTableRelationSelectionMap = advancedTableRelationSelectionMap;
    }

    public String getBulkUploadErrorMessage() {
        return bulkUploadErrorMessage;
    }

    public void setBulkUploadErrorMessage(String bulkUploadErrorMessage) {
        this.bulkUploadErrorMessage = bulkUploadErrorMessage;
    }

    public HashMap<String, Date> getFormDateFieldsCache() {
        return formDateFieldsCache;
    }

    public void setFormDateFieldsCache(HashMap<String, Date> formDateFieldsCache) {
        this.formDateFieldsCache = formDateFieldsCache;
    }

    public HashMap<String, TreeMap<String, String>> getUserDictionary() {
        return userDictionary;
    }

    public void setUserDictionary(HashMap<String, TreeMap<String, String>> userDictionary) {
        this.userDictionary = userDictionary;
    }

}
