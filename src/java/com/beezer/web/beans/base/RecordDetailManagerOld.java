/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.base;

import com.beezer.web.beans.modules.notes.NotesBean;
import com.beezer.web.beans.templates.*;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.RequestHandler;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.managers.RelationResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author badry
 */
public abstract class RecordDetailManagerOld extends BeanFramework {

    protected String requestedId;
    private LinkedHashMap<Integer, RelationManagerModel> relationalMap;
    private LinkedHashMap<Integer, DataBean> relationalBeanMap;
    private LinkedHashMap<Integer, String> relationalKeyMap;
    private LinkedHashMap<Integer, String> relationalTableMap;

    @ManagedProperty(value = "#{notesBean}")
    private NotesBean notesBean;

    public RecordDetailManagerOld() {
    }

    public void setSelectedRecord() {
        requestedId = super.getRequestParams().get("recordId");
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        ModuleModel mm = super.loadModule(super.getModuleId());
        String moduleName = "";
        if (mm != null) {
            moduleName = mm.getModuleBaseTable() + ".";
        }
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType(moduleName + this.getKeyField(), Integer.valueOf(requestedId)));
        request.setClause(fm.getClause());
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            if (response.getErrorCode() == 1000) {
                super.recordModel = response.getRecordList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RecordDetailManagerOld.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadRelationalModules() {
        RelationRequest relationRequest = new RelationRequest();
        relationRequest.setRelationId(super.getModuleId());
        relationRequest.setActionType("5");
        RelationHandler handler = new RelationHandler();
        RelationResponse relationResponse;
        try {
            relationResponse = handler.relationExecutor(relationRequest);
            if (relationResponse.getErrorCode() == 1000) {
                this.relationalMap = new LinkedHashMap<>();
                this.relationalBeanMap = new LinkedHashMap<>();
                this.relationalKeyMap = new LinkedHashMap<>();
                this.relationalTableMap = new LinkedHashMap<>();
                DataBean db;
                for (RelationManagerModel rmm : relationResponse.getRelationManagerList()) {
                    relationalMap.put(rmm.getModuleId(), rmm);
                    db = new DataBean();
                    db.setModuleId(String.valueOf(rmm.getModuleId()));
                    db.setRelationView(true);
                    db.init();
//                    String qualifiedColumnName = rmm.getMasterTable() + "." + rmm.getMasterColumn();
//                    db.loadRecordCount(qualifiedColumnName, requestedId);
                    relationalBeanMap.put(rmm.getModuleId(), db);
                    relationalKeyMap.put(rmm.getModuleId(), rmm.getMasterColumn());
                    relationalTableMap.put(rmm.getModuleId(), rmm.getMasterTable());
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RecordDetailManagerOld.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onTabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        if (!tabId.equalsIgnoreCase("detailsTab") && !tabId.equalsIgnoreCase("notesTab") && !tabId.equalsIgnoreCase("galleryTab")) {
            int id = Integer.parseInt(tabId.replaceAll("id", ""));
            DataBean temp = this.relationalBeanMap.get(id);
            String qualifiedColumnName = this.relationalTableMap.get(id) + "." + this.relationalKeyMap.get(id);
            temp.loadFilters(String.valueOf(id));
            temp.loadRelatedRecord(qualifiedColumnName, this.requestedId);
            this.relationalBeanMap.put(id, temp);
            PrimeFaces.current().ajax().update("detailsForm:tabHolder");
        }
        else if (tabId.equalsIgnoreCase("notesTab")) {
            notesBean.setRequestedModuleId(super.getModuleId());
            notesBean.setRequestedObjectId(this.requestedId);
            notesBean.init();
        }
    }

    public void goToRelationField(ModuleFieldModel relationField) {
        if (relationField != null && relationField.getModuleId() != 11) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                        + relationField.getCurrentValue() + "&" + "moduleId=" + relationField.getModuleId());
            }
            catch (IOException ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void onRowSelect(SelectEvent event) {
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?recordId="
                    + super.getRecordModel().getFieldValueMap().get(super.getKeyField()).getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT + "&moduleId=" + super.getModuleId());
        }
        catch (Exception ex) {
            Logger.getLogger(RecordDetailManagerOld.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public LinkedHashMap<Integer, RelationManagerModel> getRelationalMap() {
        return relationalMap;
    }

    public void setRelationalMap(LinkedHashMap<Integer, RelationManagerModel> relationalMap) {
        this.relationalMap = relationalMap;
    }

    public LinkedHashMap<Integer, DataBean> getRelationalBeanMap() {
        return relationalBeanMap;
    }

    public void setRelationalBeanMap(LinkedHashMap<Integer, DataBean> relationalBeanMap) {
        this.relationalBeanMap = relationalBeanMap;
    }

    public LinkedHashMap<Integer, String> getRelationalKeyMap() {
        return relationalKeyMap;
    }

    public void setRelationalKeyMap(LinkedHashMap<Integer, String> relationalKeyMap) {
        this.relationalKeyMap = relationalKeyMap;
    }

    public LinkedHashMap<Integer, String> getRelationalTableMap() {
        return relationalTableMap;
    }

    public void setRelationalTableMap(LinkedHashMap<Integer, String> relationalTableMap) {
        this.relationalTableMap = relationalTableMap;
    }

    public String getRequestedId() {
        return requestedId;
    }

    public void setRequestedId(String requestedId) {
        this.requestedId = requestedId;
    }

    public NotesBean getNotesBean() {
        return notesBean;
    }

    public void setNotesBean(NotesBean notesBean) {
        this.notesBean = notesBean;
    }

}
