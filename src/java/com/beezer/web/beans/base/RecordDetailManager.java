/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.base;

import com.beezer.web.beans.modules.content.ContentLibraryBean;
import com.beezer.web.beans.modules.notes.NotesBean;
import com.beezer.web.beans.templates.AddDataBean;
import com.beezer.web.beans.templates.DataBean;
import com.beezer.web.beans.templates.RecordDetailBean;
import com.beezer.web.commons.Defines;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.RequestHandler;
import com.beezer.web.handler.SmartViewHandler;
import com.beezer.web.handler.TaskHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.TaskManagerModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.internal.smartView.SmartViewModel;
import com.crm.models.internal.smartView.TabsConfig;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.requests.managers.SmartViewRequest;
import com.crm.models.requests.managers.TasksRequest;
import com.crm.models.responses.managers.RelationResponse;
import com.crm.models.responses.managers.SmartViewResponse;
import com.crm.models.responses.managers.TasksResponse;
import com.crm.models.serviceModels.RequestModel;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

/**
 *
 * @author badry
 */
public abstract class RecordDetailManager extends BeanFramework {

    protected String requestedId;
    private LinkedHashMap<Integer, RelationManagerModel> relationalMap;
    private LinkedHashMap<Integer, DataBean> relationalBeanMap;
    private LinkedHashMap<Integer, String> relationalKeyMap;
    private LinkedHashMap<Integer, String> relationalTableMap;

    private SmartViewModel smartViewModel;

    @ManagedProperty(value = "#{notesBean}")
    private NotesBean notesBean;

    @ManagedProperty(value = "#{contentLibraryBean}")
    private ContentLibraryBean contentLibraryBean;

    private boolean approval;
    private TaskManagerModel originatingTask;

    public RecordDetailManager() {
    }

    public void initialize() {
        super.BEAN_TYPE = "RECORD_DETAIL_BEAN";
        this.setSelectedRecord();
        super.loadFieldSet(super.getModuleId(), null);
        this.loadRecord();
        this.loadSmartViewConfigurations();
        this.loadRelationalModules();
        super.fillRecordDetails();
        super.fillAdvancentComponentsValuesForSmartView(smartViewModel);
        super.setMasterPageName("data.xhtml?moduleId=" + super.getModuleId());
    }

    public void setSelectedRecord() {
        requestedId = super.getRequestParams().get("recordId");
        switch (super.getRequestMode()) {
            case Defines.REQUEST_MODE_APPROVE:
                this.approval = true;
                this.loadOriginationTask();
                break;
        }
    }

    @Override
    public void loadRecord() {
        RequestModel request = new RequestModel();
        request.setRequestActionType("0");
        request.setRequestingModule(super.getModuleId());
        ModuleModel mm = super.loadModule(super.getModuleId());
        String moduleName = "";
        if (mm != null) {
            moduleName = mm.getModuleBaseTable() + ".";
        }
        FilterManager fm = new FilterManager();
        if (requestedId != null) {
            fm.setFilter("AND", new FilterType(moduleName + this.getKeyField(), requestedId));
            request.setClause(fm.getClause());
            RequestHandler requestHandler = new RequestHandler();
            ResponseModel response;
            try {
                response = requestHandler.executeRequest(request, "GenericMasterAPI");
                if (response.getErrorCode() == 1000) {
                    super.recordModel = response.getRecordList().get(0);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void loadOriginationTask() {
        String taskId = super.getRequestParams().get("TaskId");
        if (taskId != null && !taskId.isEmpty()) {
            TasksRequest tasksRequest = new TasksRequest();
            tasksRequest.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("task_id", "=", taskId));
            tasksRequest.setClause(fm.getClause());
            TaskHandler taskHandler = new TaskHandler();
            TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
            if (tasksResponse.getErrorCode() == 1000) {
                if (tasksResponse.getReturnList() != null && !tasksResponse.getReturnList().isEmpty()) {
                    this.originatingTask = tasksResponse.getReturnList().get(0);
                }
            }
        }

    }

    public void loadRelationalModules() {
        RelationRequest relationRequest = new RelationRequest();
        relationRequest.setRelationId(super.getModuleId());
        relationRequest.setActionType("5");
        RelationHandler handler = new RelationHandler();
        RelationResponse relationResponse;
        try {
            relationResponse = handler.relationExecutor(relationRequest);
            if (relationResponse.getErrorCode() == 1000) {
                this.relationalMap = new LinkedHashMap<>();
                this.relationalBeanMap = new LinkedHashMap<>();
                this.relationalKeyMap = new LinkedHashMap<>();
                this.relationalTableMap = new LinkedHashMap<>();
                DataBean db;
                for (RelationManagerModel rmm : relationResponse.getRelationManagerList()) {
                    if (smartViewModel != null && smartViewModel.getTabConfigHolder() != null
                            && smartViewModel.getTabConfigHolder().getDefaultTabsConfig() != null) {
                        for (TabsConfig tabsConfig : smartViewModel.getTabConfigHolder().getDefaultTabsConfig()) {
                            int mId = Integer.valueOf(tabsConfig.getTabId().replaceAll("ID_", ""));
                            if (mId == rmm.getModuleId()) {
                                relationalMap.put(rmm.getModuleId(), rmm);
                                db = new DataBean();
                                db.setModuleId(String.valueOf(rmm.getModuleId()));
                                db.setRelationView(true);
                                db.init();
                                relationalBeanMap.put(rmm.getModuleId(), db);
                                relationalKeyMap.put(rmm.getModuleId(), rmm.getMasterColumn());
                                relationalTableMap.put(rmm.getModuleId(), rmm.getMasterTable());
                            }
                        }
                    }
                    else {
                        relationalMap.put(rmm.getModuleId(), rmm);
                        db = new DataBean();
                        db.setModuleId(String.valueOf(rmm.getModuleId()));
                        db.setRelationView(true);
                        db.init();
                        relationalBeanMap.put(rmm.getModuleId(), db);
                        relationalKeyMap.put(rmm.getModuleId(), rmm.getMasterColumn());
                        relationalTableMap.put(rmm.getModuleId(), rmm.getMasterTable());
                    }

                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadSmartViewConfigurations() {
        SmartViewHandler smartViewHandler = new SmartViewHandler();
        SmartViewRequest request = new SmartViewRequest();
        request.setRequestActionType("0");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("module_id", "=", super.getModuleId()));
        request.setClause(fm.getClause());
        SmartViewResponse response = smartViewHandler.executeRequest(request);
        if (response.getErrorCode() == 1000) {
            if (response.getReturnList() != null && !response.getReturnList().isEmpty()) {
                smartViewModel = response.getReturnList().get(0);
                if (smartViewModel != null) {
                    if (smartViewModel.getHeaderConfig() != null && smartViewModel.getHeaderConfig().getFieldsLayoutList() != null) {
                        for (FieldsLayoutModel flm : smartViewModel.getHeaderConfig().getFieldsLayoutList()) {
                            super.buildAdvancedComponentsMap(flm);
                        }
                    }

                    if (smartViewModel.getTabConfigHolder() != null && smartViewModel.getTabConfigHolder().getTabsConfig() != null) {
                        for (TabsConfig tc : smartViewModel.getTabConfigHolder().getTabsConfig()) {
                            if (tc.getFieldsLayoutList() != null) {
                                for (FieldsLayoutModel flm : tc.getFieldsLayoutList()) {
                                    super.buildAdvancedComponentsMap(flm);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void onTabChange(TabChangeEvent event) {
        String tabId = event.getTab().getId();
        if (!tabId.equalsIgnoreCase("detailsTab") && !tabId.equalsIgnoreCase("notesTab")
                && !tabId.equalsIgnoreCase("attachmentsTab") && !tabId.equalsIgnoreCase("galleryTab")) {
            if (!tabId.contains("smartTab_")) {
                int id = Integer.parseInt(tabId.replaceAll("id", ""));
                DataBean temp = this.relationalBeanMap.get(id);
                String qualifiedColumnName = this.relationalTableMap.get(id) + "." + this.relationalKeyMap.get(id);
                String keyValue = GeneralUtils.getFieldFromRecord(recordModel, this.relationalMap.get(id).getDetailColumn()).getCurrentValue();
                temp.loadRelatedRecord(qualifiedColumnName, keyValue);
                temp.loadFilters(String.valueOf(id));
                temp.setKeyName(qualifiedColumnName);
                temp.setKeyValue(keyValue);
                this.relationalBeanMap.put(id, temp);
//                RequestContext.getCurrentInstance().update("detailsForm:tabHolder");
                PrimeFaces.current().ajax().update("detailsForm:tabHolder");
            }
            else {
//                RequestContext.getCurrentInstance().update("detailsForm:tabHolder");
                PrimeFaces.current().ajax().update("detailsForm:tabHolder");
            }
        }
        else if (tabId.equalsIgnoreCase("notesTab")) {
            notesBean.setRequestedModuleId(super.getModuleId());
            notesBean.setRequestedObjectId(this.requestedId);
            notesBean.init();
        }
        else if (tabId.equalsIgnoreCase("attachmentsTab")) {
            contentLibraryBean.setRequestedModule(super.getModuleId());
            contentLibraryBean.setRequestedObjectId(this.requestedId);
            contentLibraryBean.init();
        }
        else if (tabId.equalsIgnoreCase("galleryTab")) {
        }
    }

    public void approveRequest() {
        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("3");
        this.originatingTask.setTaskResponse("APPROVED");
        this.originatingTask.setStatus("DONE");
        tasksRequest.setTaskManagerModel(originatingTask);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("taskDetails.xhtml?recordId="
                        + originatingTask.getTaskId() + "&" + "moduleId=" + "34");
            }
            catch (IOException ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void rejectRequest() {
        TasksRequest tasksRequest = new TasksRequest();
        tasksRequest.setRequestActionType("3");
        this.originatingTask.setTaskResponse("REJECTED");
        this.originatingTask.setStatus("DONE");
        tasksRequest.setTaskManagerModel(originatingTask);
        TaskHandler taskHandler = new TaskHandler();
        TasksResponse tasksResponse = taskHandler.execute(tasksRequest);
        if (tasksResponse.getErrorCode() == 1000) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("taskDetails.xhtml?recordId="
                        + originatingTask.getTaskId() + "&" + "moduleId=" + "34");
            }
            catch (IOException ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void goToRelationField(ModuleFieldModel relationField) {
        if (relationField != null && relationField.getModuleId() != 0) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("recordDetails.xhtml?recordId="
                        + relationField.getCurrentValue() + "&" + "moduleId=" + relationField.getModuleId());
            }
            catch (IOException ex) {
                Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void loadRecordHistory() {
    }

    @Override
    public void onRowSelect(SelectEvent event) {
    }

    @Override
    public void save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("add.xhtml?recordId="
                    + super.getRecordModel().getFieldValueMap().get(super.getKeyField()).getCurrentValue() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT + "&moduleId=" + super.getModuleId());
        }
        catch (Exception ex) {
            Logger.getLogger(RecordDetailBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void delete() {
//        super.populateDataFromForm();
        RequestModel request = new RequestModel();
        request.setRequestingModule(super.getModuleId());
        request.setRecordModel(super.recordModel);
        request.setRequestActionType("2");
        RequestHandler requestHandler = new RequestHandler();
        ResponseModel response;
        try {
            response = requestHandler.executeRequest(request, "GenericMasterAPI");
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(AddDataBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public LinkedHashMap<Integer, RelationManagerModel> getRelationalMap() {
        return relationalMap;
    }

    public void setRelationalMap(LinkedHashMap<Integer, RelationManagerModel> relationalMap) {
        this.relationalMap = relationalMap;
    }

    public LinkedHashMap<Integer, DataBean> getRelationalBeanMap() {
        return relationalBeanMap;
    }

    public void setRelationalBeanMap(LinkedHashMap<Integer, DataBean> relationalBeanMap) {
        this.relationalBeanMap = relationalBeanMap;
    }

    public LinkedHashMap<Integer, String> getRelationalKeyMap() {
        return relationalKeyMap;
    }

    public void setRelationalKeyMap(LinkedHashMap<Integer, String> relationalKeyMap) {
        this.relationalKeyMap = relationalKeyMap;
    }

    public LinkedHashMap<Integer, String> getRelationalTableMap() {
        return relationalTableMap;
    }

    public void setRelationalTableMap(LinkedHashMap<Integer, String> relationalTableMap) {
        this.relationalTableMap = relationalTableMap;
    }

    public String getRequestedId() {
        return requestedId;
    }

    public void setRequestedId(String requestedId) {
        this.requestedId = requestedId;
    }

    public NotesBean getNotesBean() {
        return notesBean;
    }

    public void setNotesBean(NotesBean notesBean) {
        this.notesBean = notesBean;
    }

    public SmartViewModel getSmartViewModel() {
        return smartViewModel;
    }

    public void setSmartViewModel(SmartViewModel smartViewModel) {
        this.smartViewModel = smartViewModel;
    }

    public ContentLibraryBean getContentLibraryBean() {
        return contentLibraryBean;
    }

    public void setContentLibraryBean(ContentLibraryBean contentLibraryBean) {
        this.contentLibraryBean = contentLibraryBean;
    }

    public boolean isApproval() {
        return approval;
    }

    public void setApproval(boolean approval) {
        this.approval = approval;
    }

    public TaskManagerModel getOriginatingTask() {
        return originatingTask;
    }

    public void setOriginatingTask(TaskManagerModel originatingTask) {
        this.originatingTask = originatingTask;
    }

}
