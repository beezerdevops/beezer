/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.base;

import com.crm.models.internal.DataTableSettings;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
public abstract class BaseBean {

    private ArrayList<ModuleFieldModel> fieldsList;
    private ArrayList<ModuleFieldModel> filterFieldsList;
    private ArrayList<DataTableSettings> settingsList;
    private ArrayList<RecordModel> recordsList;
    private ArrayList<RecordModel> filteredRecordsList;
    private List<RecordModel> selectedRecords;
    private List<RecordModel> selectedRecordsSaved;
    private String dataTableKey;
    private String relationFieldName;
    private ArrayList<RecordModel> filteredRecords;

    public abstract void onRowSelect(SelectEvent event);

    public abstract void delete();
    
    public  String convertToMoneyFormat(Object numberIn) {
        NumberFormat nf = NumberFormat.getInstance(new Locale("en_US"));
        nf.setMinimumFractionDigits(2);
        nf.setMaximumFractionDigits(2);
        if(numberIn instanceof String){
            float f = Float.valueOf((String) numberIn);
            return nf.format(f);
        }
        return nf.format(numberIn);
    }
    
    public void onDestroy() {
//        PrimeFaces.current().executeScript("alert('hi')");
//PrimeFaces.current().executeScript("showDoubleLoginNotification()");
//String viewId = Defines.PF_LEDGER.get(this.userModel.getUserId()).getViewRoot().getViewId();
//Map<String, Object> appMap = Defines.PF_LEDGER.get(this.userModel.getUserId()).getViewRoot().getViewMap();
//        PrimeFaces.current().ajax().update("homeBody");
//PrimeFaces.current().dialog().showMessageDynamic(new FacesMessage("Hi Fucker"));
        PrimeFaces.current().executeScript("PF('doubleLoginNotificationDialog').show()");
    }

    public ArrayList<ModuleFieldModel> getFieldsList() {
        return fieldsList;
    }

    public void setFieldsList(ArrayList<ModuleFieldModel> fieldsList) {
        this.fieldsList = fieldsList;
    }

    public ArrayList<DataTableSettings> getSettingsList() {
        return settingsList;
    }

    public void setSettingsList(ArrayList<DataTableSettings> settingsList) {
        this.settingsList = settingsList;
    }

    public ArrayList<RecordModel> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(ArrayList<RecordModel> recordsList) {
        this.recordsList = recordsList;
    }

    public List<RecordModel> getSelectedRecords() {
        return selectedRecords;
    }

    public void setSelectedRecords(List<RecordModel> selectedRecords) {
        this.selectedRecords = selectedRecords;
    }

    public String getDataTableKey() {
        return dataTableKey;
    }

    public void setDataTableKey(String dataTableKey) {
        this.dataTableKey = dataTableKey;
    }

    public ArrayList<RecordModel> getFilteredRecordsList() {
        return filteredRecordsList;
    }

    public void setFilteredRecordsList(ArrayList<RecordModel> filteredRecordsList) {
        this.filteredRecordsList = filteredRecordsList;
    }

    public String getRelationFieldName() {
        return relationFieldName;
    }

    public void setRelationFieldName(String relationFieldName) {
        this.relationFieldName = relationFieldName;
    }

    public ArrayList<ModuleFieldModel> getFilterFieldsList() {
        return filterFieldsList;
    }

    public void setFilterFieldsList(ArrayList<ModuleFieldModel> filterFieldsList) {
        this.filterFieldsList = filterFieldsList;
    }

    public ArrayList<RecordModel> getFilteredRecords() {
        return filteredRecords;
    }

    public void setFilteredRecords(ArrayList<RecordModel> filteredRecords) {
        this.filteredRecords = filteredRecords;
    }

    public List<RecordModel> getSelectedRecordsSaved() {
        return selectedRecordsSaved;
    }

    public void setSelectedRecordsSaved(List<RecordModel> selectedRecordsSaved) {
        this.selectedRecordsSaved = selectedRecordsSaved;
    }

}
