/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.users;

import com.beezer.web.commons.Defines;
import com.beezer.web.handler.UserHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UserModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.UserRequest;
import com.crm.models.responses.UserResponse;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author badry
 */
@ManagedBean(name = "usersBean")
@ViewScoped
public class UsersBean {

    @ManagedProperty(value = "#{usersList}")
    private ArrayList<UserModel> usersList;
    private UserModel newUser;

    @PostConstruct
    public void init() {
        this.newUser = new UserModel();
        this.loadUsers();
    }

    public void loadUsers() {
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("users_base.tenant_id", "=", GeneralUtils.getLoggedUser().getTenantId()));
            userRequest.setClause(fm.getClause());
            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
                this.usersList = response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UsersBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void showUserProfile(UserModel user) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("userProfile.xhtml?user="
                    + user.getUserId() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_EDIT + "&" + "ADMIN=true");
        }
        catch (Exception ex) {
            Logger.getLogger(UsersBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addUser(){
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("1");
            userRequest.setUserModel(newUser);
            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("userProfile.xhtml?user="
                    + response.getReturnList().get(0).getUserId() + "&" + Defines.REQUEST_MODE_KEY + "="
                    + Defines.REQUEST_MODE_ADD + "&" + "ADMIN=true");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UsersBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<UserModel> getUsersList() {
        return usersList;
    }

    public void setUsersList(ArrayList<UserModel> usersList) {
        this.usersList = usersList;
    }

    public UserModel getNewUser() {
        return newUser;
    }

    public void setNewUser(UserModel newUser) {
        this.newUser = newUser;
    }
    
    
}
