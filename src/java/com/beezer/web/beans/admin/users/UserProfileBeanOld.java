/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.users;

import com.beezer.web.handler.AccessHandler;
import com.beezer.web.handler.UserHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UserModel;
import com.crm.models.internal.application.ApplicationModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.UserRequest;
import com.crm.models.requests.managers.AccessRequest;
import com.crm.models.responses.UserResponse;
import com.crm.models.responses.managers.AccessResponse;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author badry
 */
@ManagedBean(name = "userProfileBeanxxxx")
@ViewScoped
public class UserProfileBeanOld {

    private UserModel userModel;
    private String requestedUserId;
    private boolean authenticationFailed = false;
    private String email;
    private String password;
    private String oldPassword;
    private ArrayList<ApplicationModel> applicationList;
    private boolean admin;
    private boolean adminConsole;

    @PostConstruct
    public void init() {
        this.setSelectedRecord();
        this.loadUser();
    }

    private void setSelectedRecord() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        requestedUserId = params.get("user");
    }

    public void loadUser() {
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("user_id", "=", requestedUserId));
            userRequest.setClause(fm.getClause());
            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
                this.userModel = response.getReturnList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UserProfileBeanOld.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void signIn() {
        try {
            AccessHandler accessHandler = new AccessHandler();
            AccessRequest request = new AccessRequest();
            request.setActionType(AccessRequest.ACTIONTYPE_LOGIN);
            request.setUserName(this.email);
            request.setPassword(this.password);
            AccessResponse response = accessHandler.accessExcecutor(request);
            if (response.getErrorCode() == 1000) {
                this.authenticationFailed = false;
                this.userModel = response.getUserProfile();
                this.applicationList = response.getApplicationList();
                FacesContext.getCurrentInstance().getExternalContext().redirect("dashboard.xhtml");
            }
            else {
                this.authenticationFailed = true;
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UserProfileBeanOld.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changePassword() {
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("4");
            userRequest.setOldPassword(oldPassword);
            userRequest.setUserModel(userModel);
            if (GeneralUtils.getLoggedUser().getPermissionSet().isAdmin()) {
                userRequest.setOveridePasswordVerification(true);
            }
            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
                PrimeFaces.current().executeScript("PF('changePasswordDialog').hide()");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UserProfileBeanOld.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changeEmail() {
        try {
            UserRequest userRequest = new UserRequest();
            userRequest.setActionType("5");
            userRequest.setUserModel(userModel);
            if (GeneralUtils.getLoggedUser().getPermissionSet().isAdmin()) {
                userRequest.setOveridePasswordVerification(true);
            }
            UserHandler handler = new UserHandler();
            UserResponse response = handler.execute(userRequest);
            if (response.getErrorCode() == 1000) {
                PrimeFaces.current().executeScript("PF('changeEmailDialog').hide()");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(UserProfileBeanOld.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public boolean isAuthenticationFailed() {
        return authenticationFailed;
    }

    public void setAuthenticationFailed(boolean authenticationFailed) {
        this.authenticationFailed = authenticationFailed;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<ApplicationModel> getApplicationList() {
        return applicationList;
    }

    public void setApplicationList(ArrayList<ApplicationModel> applicationList) {
        this.applicationList = applicationList;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public boolean isAdminConsole() {
        return adminConsole;
    }

    public void setAdminConsole(boolean adminConsole) {
        this.adminConsole = adminConsole;
    }

}
