/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.workflow;

import com.beezer.web.beans.base.BeanFramework;
import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.RequestHandler;
import com.process.handler.WorkflowHandler;
import com.beezer.web.models.SelectionModel;
import com.beezer.web.utils.RandomUtils;
import com.crm.models.global.email.MailEntity;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.internal.workflow.BeezerWorkflow;
import com.crm.models.internal.workflow.FieldValueMapper;
import com.crm.models.internal.workflow.LogicalSequenceModel;
import com.crm.models.internal.workflow.WFActivityModel;
import com.crm.models.internal.workflow.WFConditionModel;
import com.crm.models.internal.workflow.WFCriteriaModel;
import com.crm.models.internal.workflow.activities.EmailConfiguration;
import com.crm.models.internal.workflow.activities.ListConfiguration;
import com.crm.models.internal.workflow.activities.LoopConfiguration;
import com.crm.models.internal.workflow.activities.NotificationConfiguration;
import com.crm.models.internal.workflow.activities.RecordFieldConfig;
import com.crm.models.internal.workflow.activities.RecordManagerConfiguration;
import com.crm.models.internal.workflow.activities.TerminateConfiguration;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.requests.managers.WorkflowRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.responses.managers.RelationResponse;
import com.crm.models.responses.managers.WorkflowResponse;
import com.crm.models.serviceModels.ResponseModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.primefaces.PrimeFaces;
import org.primefaces.event.diagram.ConnectEvent;
import org.primefaces.event.diagram.ConnectionChangeEvent;
import org.primefaces.event.diagram.DisconnectEvent;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.FlowChartConnector;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.endpoint.RectangleEndPoint;
import org.primefaces.model.diagram.overlay.ArrowOverlay;

/**
 *
 * @author badry
 */
@ManagedBean(name = "workflowBean")
@ViewScoped
public class WorkflowBean extends BeanFramework {

    private DefaultDiagramModel diagramModel;
    private DefaultDiagramModel diagramModel2;
    private LogicalSequenceModel logicalSequenceModel;
    private String requestedWorkflowId;
    private BeezerWorkflow workflowManager;
    private LogicalSequenceModel currentLSM;
    private LogicalSequenceModel selectedLSM;
    private LinkedHashMap<String, LogicalSequenceModel> componentStore;
    private ArrayList<ModuleFieldModel> availableColumnList;
    private ArrayList<ModuleModel> availableModuleList;
    private ArrayList<ModuleModel> fullModuleList;
    private ArrayList<RelationManagerModel> relationsList;
    private RecordFieldConfig activeRecordConfig;
    private LinkedHashMap<Integer, ArrayList<ModuleFieldModel>> contextParamMap;
    private String mailHolder;
    private ArrayList<ModuleFieldModel> mailTargetFieldSet;
    private ModuleModel selectedMailModule;
    private ModuleFieldModel selectedMailField;
    private ModuleModel selectedCCMailModule;
    private ModuleFieldModel selectedCCMailField;
    private ModuleModel selectedBCCMailModule;
    private ModuleFieldModel selectedBCCMailField;
    private HashMap<String, String> moduleMap;

    private boolean updateProcess;

    @ManagedProperty(value = "#{mailEntitiesListBean.entitiesList}")
    private ArrayList<MailEntity> entitiesList;

    @PostConstruct
    public void init() {
        this.moduleMap = new HashMap<>();
        this.componentStore = new LinkedHashMap<>();
        this.availableColumnList = new ArrayList<>();
        this.relationsList = new ArrayList<>();
        this.availableModuleList = new ArrayList<>();
        this.contextParamMap = new LinkedHashMap<>();
        this.diagramModel = new DefaultDiagramModel();
        this.diagramModel.setMaxConnections(-1);
        this.mailTargetFieldSet = new ArrayList<>();
        this.fullModuleList = new ArrayList<>();
//        FlowChartConnector connector = new FlowChartConnector();
        diagramModel.getDefaultConnectionOverlays().add(new ArrowOverlay(13, 13, 1, 1));
        FlowChartConnector connector = new FlowChartConnector();
        connector.setPaintStyle("{strokeStyle:'#828282',lineWidth:1}");
        this.diagramModel.setDefaultConnector(connector);
        this.getRequestedParameter();
        this.loadWorkflowProperties();
        this.loadWorkflowProcessConfiguration();
        this.loadContextParameters();
        this.loadAllModuleManagers();
    }

    public void getRequestedParameter() {
        this.requestedWorkflowId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("wf");
    }

    public void loadWorkflowProperties() {
        try {
            WorkflowRequest workflowRequest = new WorkflowRequest();
            workflowRequest.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("wf_id", "=", requestedWorkflowId));
            workflowRequest.setClause(fm.getClause());
            WorkflowHandler handler = new WorkflowHandler();
            WorkflowResponse response = handler.executeForManager(workflowRequest);
            if (response.getErrorCode() == 1000) {
                this.workflowManager = response.getBeezerWorkflowList().get(0);
                if (workflowManager != null) {
                    super.setModuleId(String.valueOf(workflowManager.getTargetModuleId()));
                    super.loadFieldSet(super.getModuleId(), null);
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onSelect(String fieldName, String selection) {
        super.onSelect(fieldName, selection);
        this.activeRecordConfig.getValueConfiguration().setValue(selection);
        Collections.replaceAll(selectedLSM.getActivity().getRecordManagerConfiguration().getFieldsConfig(), activeRecordConfig, activeRecordConfig);
    }

    public void loadWorkflowProcessConfiguration() {
        try {
            WorkflowRequest workflowRequest = new WorkflowRequest();
            workflowRequest.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("wf_id", "=", requestedWorkflowId));
            workflowRequest.setWorkflowId(Integer.parseInt(requestedWorkflowId));
            WorkflowHandler handler = new WorkflowHandler();
            WorkflowResponse response = handler.executeForConfiguration(workflowRequest);
            if (response.getErrorCode() == 1000) {
                this.currentLSM = response.getLogicalSequenceModel();
                if (this.currentLSM == null) {
                    this.currentLSM = new LogicalSequenceModel();
                }
                else {
                    updateProcess = true;
                    this.buildDiagramAligned(null, null, null, currentLSM, null, null);
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadContextParameters() {
        this.loadRelationList(workflowManager.getTargetModuleId(), relationsList);
        this.filterRelationList();
        this.loadModuleManagers();
        for (ModuleModel mm : this.availableModuleList) {
            this.loadModuleFieldSet(String.valueOf(mm.getModuleId()));
        }
    }

    public void loadContextParameters(int targetModuleId) {
        this.loadRelationList(targetModuleId, relationsList);
        this.filterRelationList();
        this.loadModuleManagers();
        for (ModuleModel mm : this.availableModuleList) {
            this.loadModuleFieldSet(String.valueOf(mm.getModuleId()));
        }
    }

    public void loadModuleManagers() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            FilterManager fm = new FilterManager();
            int index = 0;

            for (RelationManagerModel rmm : this.relationsList) {
                if (index == 0) {
                    fm.setFilter("AND", new FilterType("module_id", "=", rmm.getDetailModuleId()));
                }
                else {
                    fm.setFilter("OR", new FilterType("module_id", "=", rmm.getDetailModuleId()));
                }
                index++;
            }

            fm.setFilter("OR", new FilterType("module_id", "=", super.getModuleId()));
            if (this.relationsList.isEmpty()) {
                fm.setFilter("AND", new FilterType("module_id", "=", 0));
            }

            request.setClause(fm.getClause());
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.availableModuleList = response.getReturnList();
                for (ModuleModel mm : availableModuleList) {
                    this.moduleMap.put(mm.getModuleName(), String.valueOf(mm.getModuleId()));
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadAllModuleManagers() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.fullModuleList = response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadRelationList(int moduleId) {
        try {
            RelationRequest request = new RelationRequest();
            request.setModuleId(String.valueOf(moduleId));
            request.setActionType("4");
            RelationHandler relationHandler = new RelationHandler();
            RelationResponse response = relationHandler.relationExecutor(request);
            if (response.getErrorCode() == 1000) {
                if (response.getRelationManagerList() != null) {
                    if (relationsList != null && !relationsList.isEmpty()) {
                        boolean found = false;
                        ArrayList<RelationManagerModel> tempRL = relationsList;
                        for (RelationManagerModel rmm : relationsList) {
                            for (RelationManagerModel rmmR : response.getRelationManagerList()) {
                                if (rmm.getDetailModuleId() == rmmR.getDetailModuleId()) {
                                    found = true;
                                    break;
                                }

                                if (!found) {
                                    tempRL.add(rmmR);
                                    this.loadRelationList(rmmR.getDetailModuleId());
                                }
                            }
                        }
                        relationsList.clear();
                        relationsList.addAll(tempRL);
                    }
                    else {
                        this.relationsList.addAll(response.getRelationManagerList());
                        for (RelationManagerModel rmm : response.getRelationManagerList()) {
                            this.loadRelationList(rmm.getDetailModuleId());
                        }
                    }

//                    for (RelationManagerModel rmm : relationsList) {
//                        if (rmm.getDetailModuleId() != 911 && rmm.getDetailModuleId() != workflowManager.getTargetModuleId() && rmm.getRelationType().equals("O2O")) {
//                            this.loadRelationList(rmm.getDetailModuleId());
//                        }
//                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadRelationList(int moduleId, ArrayList<RelationManagerModel> relList) {
        try {
            RelationRequest request = new RelationRequest();
            request.setModuleId(String.valueOf(moduleId));
            request.setActionType("4");
            RelationHandler relationHandler = new RelationHandler();
            RelationResponse response = relationHandler.relationExecutor(request);
            if (response.getErrorCode() == 1000) {
                if (response.getRelationManagerList() != null) {
                    if (relList != null && !relList.isEmpty()) {
                        for (RelationManagerModel rmm : response.getRelationManagerList()) {
                            boolean found = false;
                            for (RelationManagerModel rmmR : relList) {
                                if (rmm.getDetailModuleId() == rmmR.getDetailModuleId()) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                relationsList.add(rmm);
                                relList.add(rmm);
                                this.loadRelationList(rmm.getDetailModuleId(), relList);
                            }
                        }
//                        relationsList.clear();
//                        relationsList.addAll(tempRL);
                    }
                    else {
                        relList = new ArrayList<>();
                        relList.addAll(response.getRelationManagerList());
                        relationsList.addAll(response.getRelationManagerList());
                        for (RelationManagerModel rmm : response.getRelationManagerList()) {
                            this.loadRelationList(rmm.getDetailModuleId(), relList);
                        }
                    }

//                    for (RelationManagerModel rmm : relationsList) {
//                        if (rmm.getDetailModuleId() != 911 && rmm.getDetailModuleId() != workflowManager.getTargetModuleId() && rmm.getRelationType().equals("O2O")) {
//                            this.loadRelationList(rmm.getDetailModuleId());
//                        }
//                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void filterRelationList() {
        ArrayList<RelationManagerModel> filteredRelationsList = new ArrayList<>();
        if (!relationsList.isEmpty()) {
            filteredRelationsList.add(relationsList.get(0));
            for (RelationManagerModel rmm : relationsList) {
                boolean found = false;
                for (RelationManagerModel rmmf : filteredRelationsList) {
                    if (rmmf.getDetailModuleId() == rmm.getDetailModuleId()) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    filteredRelationsList.add(rmm);
                }
            }
            relationsList.clear();
            relationsList.addAll(filteredRelationsList);
        }
    }

    public void configureRecordManagerTargetModuleList() {
        if (selectedLSM != null) {
//            this.loadRelationList(workflowManager.getTargetModuleId());
//            this.loadModuleManagers();
        }
    }

    public void onRecordManagerModuleSelect() {
        super.loadFieldSet(String.valueOf(selectedLSM.getActivity().getRecordManagerConfiguration().getTargetModuleId()), null);
    }

    private void loadModuleFieldSet(String moduleId) {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(moduleId);
            RequestHandler requestHandler = new RequestHandler();
            ModuleFieldResponse response = requestHandler.getFieldSet(request);
            if (response.getErrorCode() == 1000) {
                for (RelationManagerModel rmm : relationsList) {
                    if (rmm.getModuleId() == Integer.parseInt(moduleId)) {
                        ModuleFieldModel mfm = new ModuleFieldModel();
                        mfm.setFieldName(rmm.getViewAlias() == null ? rmm.getViewColumn() : rmm.getViewAlias());
                        mfm.setFieldLabel(rmm.getViewAlias() == null ? rmm.getViewColumn() : rmm.getViewAlias());
                        mfm.setFieldId(Integer.parseInt(RandomUtils.randomNumeric(4)));
                        response.getReturnList().add(mfm);
                    }
                }
                contextParamMap.put(Integer.parseInt(moduleId), response.getReturnList());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addNewFieldToRecordManager() {
        if (selectedLSM != null) {
            selectedLSM.getActivity().getRecordManagerConfiguration().getFieldsConfig().add(new RecordFieldConfig());
        }
    }

    public void onRecordManagerFieldSelect(RecordFieldConfig selectedField) {
        this.activeRecordConfig = selectedField;
        if (activeRecordConfig.getValueConfiguration() == null) {
            this.activeRecordConfig.setValueConfiguration(new FieldValueMapper());
        }
        else {
            if (activeRecordConfig.getFieldModel().getFieldType().equals("LOOKUP")) {
                PrimeFaces.current().executeScript("PF('" + activeRecordConfig.getFieldModel().getFieldName() + "').selectValue(" + activeRecordConfig.getValueConfiguration().getValue() + ")");
            }
        }
    }

    public void removeFieldFromRecordManager(RecordFieldConfig selectedField) {
        if (selectedLSM != null) {
            activeRecordConfig = null;
            selectedLSM.getActivity().getRecordManagerConfiguration().getFieldsConfig().remove(selectedField);
        }
    }

    public void fillMailTargetFieldSet(String type) {
        switch (type) {
            case "TO":
                this.mailHolder = "$" + selectedMailModule.getModuleBaseTable() + "$" + ".";
                this.mailTargetFieldSet = contextParamMap.get(selectedMailModule.getModuleId());
                break;
            case "CC":
                this.mailHolder = "$" + selectedCCMailModule.getModuleBaseTable() + "$" + ".";
                this.mailTargetFieldSet = contextParamMap.get(selectedCCMailModule.getModuleId());
                break;
            case "BCC":
                this.mailHolder = "$" + selectedBCCMailModule.getModuleBaseTable() + "$" + ".";
                this.mailTargetFieldSet = contextParamMap.get(selectedBCCMailModule.getModuleId());
                break;
        }

    }

    public void fillMailTargetFieldSelect(String type) {
        switch (type) {
            case "TO":
                this.mailHolder = this.mailHolder + "$" + selectedMailField.getFieldName() + "$";
                if (this.selectedLSM.getActivity().getMailConfiguration().getToMailList() != null
                        && !this.selectedLSM.getActivity().getMailConfiguration().getToMailList().isEmpty()) {
                    this.selectedLSM.getActivity().getMailConfiguration().setToMailList(this.selectedLSM.getActivity().getMailConfiguration().getToMailList() + "," + this.mailHolder);
                }
                else {
                    this.selectedLSM.getActivity().getMailConfiguration().setToMailList(this.mailHolder);
                }
                this.selectedMailField = new ModuleFieldModel();
                this.selectedMailModule = new ModuleModel();
                break;
            case "CC":
                this.mailHolder = this.mailHolder + "$" + selectedCCMailField.getFieldName() + "$";
                if (this.selectedLSM.getActivity().getMailConfiguration().getCcMailList() != null
                        && !this.selectedLSM.getActivity().getMailConfiguration().getCcMailList().isEmpty()) {
                    this.selectedLSM.getActivity().getMailConfiguration().setCcMailList(this.selectedLSM.getActivity().getMailConfiguration().getCcMailList() + "," + this.mailHolder);
                }
                else {
                    this.selectedLSM.getActivity().getMailConfiguration().setCcMailList(this.mailHolder);
                }
                this.selectedCCMailField = new ModuleFieldModel();
                this.selectedCCMailModule = new ModuleModel();
                break;
            case "BCC":
                this.mailHolder = this.mailHolder + "$" + selectedBCCMailField.getFieldName() + "$";
                if (this.selectedLSM.getActivity().getMailConfiguration().getBccMailList() != null
                        && !this.selectedLSM.getActivity().getMailConfiguration().getBccMailList().isEmpty()) {
                    this.selectedLSM.getActivity().getMailConfiguration().setBccMailList(this.selectedLSM.getActivity().getMailConfiguration().getBccMailList() + "," + this.mailHolder);
                }
                else {
                    this.selectedLSM.getActivity().getMailConfiguration().setBccMailList(this.mailHolder);
                }
                this.selectedBCCMailField = new ModuleFieldModel();
                this.selectedBCCMailModule = new ModuleModel();
                break;
        }

    }

    public void componentToDesigner() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        Element component = new Element();
        component.setX((Float.valueOf(params.get("leftPos")) + 10) + "px");
        component.setY((Float.valueOf(params.get("topPos")) + 10) + "px");
        EndPoint endPointTarget;
        EndPoint endPointSource;
        switch (params.get("property")) {
            case "intiatorPanel":
                LogicalSequenceModel initiator = new LogicalSequenceModel();
                initiator.setBlockType("INITIATOR");
                String id = RandomUtils.randomNumeric(8);
                initiator.setSequenceId(id);
                initiator.setDecisionCriteria(new WFCriteriaModel());
                initiator.getDecisionCriteria().setANDConditionList(new ArrayList<>());
                initiator.getDecisionCriteria().setORConditionList(new ArrayList<>());
                initiator.setX(params.get("leftPos") + "px");
                initiator.setY(params.get("topPos") + "px");
                initiator.setBlockLabel("Start Service");
                componentStore.put(id, initiator);
                selectedLSM = initiator;
                currentLSM = initiator;

                component.setData(initiator);
                component.setStyleClass("wf-component-base initiator-comp");
                EndPoint endPoint = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPoint.setSource(true);
                component.addEndPoint(endPoint);
                break;
            case "decisionPanel":
                LogicalSequenceModel decisionComponent = new LogicalSequenceModel();
                decisionComponent.setBlockType("DECISION");
                id = RandomUtils.randomNumeric(8);
                decisionComponent.setSequenceId(id);
                decisionComponent.setDecisionCriteria(new WFCriteriaModel());
                decisionComponent.getDecisionCriteria().setANDConditionList(new ArrayList<>());
                decisionComponent.getDecisionCriteria().setORConditionList(new ArrayList<>());
                decisionComponent.setJunction(new LinkedHashMap<>());
                decisionComponent.setX(params.get("leftPos") + "px");
                decisionComponent.setY(params.get("topPos") + "px");
                componentStore.put(id, decisionComponent);
                selectedLSM = decisionComponent;

                component.setData(decisionComponent);
                endPointTarget = createDotEndPoint(EndPointAnchor.TOP);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base decision-comp");
                component.addEndPoint(endPointTarget);//f44242
                endPointSource = createRectangleEndPoint(EndPointAnchor.LEFT, "#f44242", "FALSE", id);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                endPointSource = createRectangleEndPoint(EndPointAnchor.RIGHT, "#45f74b", "TRUE", id);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "recordManagerPanel":
                LogicalSequenceModel recordManagerComponent = new LogicalSequenceModel();
                recordManagerComponent.setBlockType("RECORD_MANAGER");
                recordManagerComponent.setActivity(new WFActivityModel());
                recordManagerComponent.getActivity().setRecordManagerConfiguration(new RecordManagerConfiguration());
                recordManagerComponent.getActivity().getRecordManagerConfiguration().setFieldsConfig(new ArrayList<>());
                id = RandomUtils.randomNumeric(8);
                recordManagerComponent.setSequenceId(id);
                recordManagerComponent.setX(params.get("leftPos") + "px");
                recordManagerComponent.setY(params.get("topPos") + "px");
                componentStore.put(id, recordManagerComponent);
                selectedLSM = recordManagerComponent;

                component.setData(recordManagerComponent);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base record-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "createTaskPanel":
                LogicalSequenceModel taskManagerComponent = new LogicalSequenceModel();
                taskManagerComponent.setBlockType("ACTIVITY");
                id = RandomUtils.randomNumeric(8);
                taskManagerComponent.setSequenceId(id);
                taskManagerComponent.setX(params.get("leftPos") + "px");
                taskManagerComponent.setY(params.get("topPos") + "px");
                componentStore.put(id, taskManagerComponent);
                selectedLSM = taskManagerComponent;

                component.setData(taskManagerComponent);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base task-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "sendMailPanel":
                LogicalSequenceModel sendMailComponent = new LogicalSequenceModel();
                sendMailComponent.setBlockType("EMAIL");
                sendMailComponent.setActivity(new WFActivityModel());
                sendMailComponent.getActivity().setMailConfiguration(new EmailConfiguration());
                id = RandomUtils.randomNumeric(8);
                sendMailComponent.setSequenceId(id);
                sendMailComponent.setX(params.get("leftPos") + "px");
                sendMailComponent.setY(params.get("topPos") + "px");
                componentStore.put(id, sendMailComponent);
                selectedLSM = sendMailComponent;

                component.setData(sendMailComponent);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base mail-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "loopComponent":
                LogicalSequenceModel loopComponent = new LogicalSequenceModel();
                loopComponent.setBlockType("LOOP");
                loopComponent.setActivity(new WFActivityModel());
                loopComponent.getActivity().setLoopConfiguration(new LoopConfiguration());
                id = RandomUtils.randomNumeric(8);
                loopComponent.setSequenceId(id);
                loopComponent.setX(params.get("leftPos") + "px");
                loopComponent.setY(params.get("topPos") + "px");
                componentStore.put(id, loopComponent);
                selectedLSM = loopComponent;

                component.setTitle("LOOP");
                component.setData(loopComponent);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base loop-comp");
                component.addEndPoint(endPointTarget);//f44242
                endPointSource = createRectangleEndPoint(EndPointAnchor.RIGHT, "#4286f4", "CONTEXT", id);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM, "#98AFC7", "POSTLOOP", id);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;

            case "listComponent":
                LogicalSequenceModel listComponent = new LogicalSequenceModel();
                listComponent.setBlockType("LIST");
                listComponent.setActivity(new WFActivityModel());
                listComponent.getActivity().setListConfiguration(new ListConfiguration());
                id = RandomUtils.randomNumeric(8);
                listComponent.setSequenceId(id);
                listComponent.setX(params.get("leftPos") + "px");
                listComponent.setY(params.get("topPos") + "px");
                componentStore.put(id, listComponent);
                selectedLSM = listComponent;

                component.setData(listComponent);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base list-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;

            case "notificationComponent":
                LogicalSequenceModel notificationComponent = new LogicalSequenceModel();
                notificationComponent.setBlockType("NOTIFICATION");
                notificationComponent.setActivity(new WFActivityModel());
                notificationComponent.getActivity().setNotificationConfiguration(new NotificationConfiguration());
                notificationComponent.getActivity().getNotificationConfiguration().setNotificationModel(new RecordModel());
                notificationComponent.getActivity().getNotificationConfiguration().getNotificationModel().setFieldValueMap(new LinkedHashMap<>());
                ModuleFieldRequest request = new ModuleFieldRequest();
                request.setModuleId("33");
                request.setLayoutId(null);
                RequestHandler requestHandler = new RequestHandler();
                try {
                    ModuleFieldResponse response = requestHandler.getFieldSet(request);
                    if (response.getErrorCode() == 1000 && response.getReturnList() != null) {
                        for (ModuleFieldModel mfm : response.getReturnList()) {
                            notificationComponent.getActivity().getNotificationConfiguration().getNotificationModel().getFieldValueMap().put(mfm.getFieldName(), new ModuleFieldModel(mfm));
                        }
                    }
                }
                catch (Exception ex) {
                    Logger.getLogger(WorkflowBean.class.getName()).log(Level.SEVERE, null, ex);
                }

                id = RandomUtils.randomNumeric(8);
                notificationComponent.setSequenceId(id);
                notificationComponent.setX(params.get("leftPos") + "px");
                notificationComponent.setY(params.get("topPos") + "px");
                componentStore.put(id, notificationComponent);
                selectedLSM = notificationComponent;

                component.setData(notificationComponent);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base notification-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;

            case "terminateComponent":
                LogicalSequenceModel terminateComponent = new LogicalSequenceModel();
                terminateComponent.setBlockType("TERMINATE");
                terminateComponent.setActivity(new WFActivityModel());
                terminateComponent.getActivity().setTerminateConfiguration(new TerminateConfiguration());
                terminateComponent.getActivity().getTerminateConfiguration().setTerminationResponse(new ResponseModel());
                id = RandomUtils.randomNumeric(8);
                terminateComponent.setSequenceId(id);
                terminateComponent.setX(params.get("leftPos") + "px");
                terminateComponent.setY(params.get("topPos") + "px");
                componentStore.put(id, terminateComponent);
                selectedLSM = terminateComponent;

                component.setData(terminateComponent);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base terminate-comp");
                component.addEndPoint(endPointTarget);
                break;
        }
        diagramModel.addElement(component);
    }

    public void clearActiveConfig() {
        activeRecordConfig = null;
    }

    public void updateDiagramPosition() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String componentId = params.get("elementId");
        String leftPos = params.get("leftPos");
        String topPos = params.get("topPos");
        for (Element el : diagramModel.getElements()) {
            LogicalSequenceModel lsm = (LogicalSequenceModel) el.getData();
            if (lsm.getSequenceId().equals(componentId)) {
                el.setX(leftPos + "px");
                el.setY(topPos + "px");
                break;
            }
        }
    }

    public void removeComponent() {
        String componentId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("elementId");
        if (componentStore.get(componentId) != null) {
            componentStore.remove(componentId);
        }
//        this.diagramModel = new DefaultDiagramModel();
        this.diagramModel.clearElements();
        this.buildDiagram(null, null, null, currentLSM);
    }

    public void openConfigurationDialog(String s) {
        String componentId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("elementId");
        if (componentStore.get(componentId) != null) {
            selectedLSM = componentStore.get(componentId);
            switch (selectedLSM.getBlockType()) {
                case "INITIATOR":
                    PrimeFaces.current().executeScript("PF('initiatorConfigDialog').show()");
                    break;
                case "DECISION":
                    this.availableColumnList = this.contextParamMap.get(workflowManager.getTargetModuleId());
                    PrimeFaces.current().ajax().update("decisionConfigDialog");
                    PrimeFaces.current().executeScript("PF('decisionConfigDialog').show()");
                    break;
                case "RECORD_MANAGER":
                    this.relationsList = new ArrayList<>();
                    onRecordManagerModuleSelect();
                    this.configureRecordManagerTargetModuleList();
                    PrimeFaces.current().ajax().update("recordManagerConfigDialog");
                    PrimeFaces.current().executeScript("PF('recordManagerConfigDialog').show()");
                    break;
                case "EMAIL":
                    PrimeFaces.current().ajax().update("mailConfigDialog");
                    PrimeFaces.current().executeScript("PF('mailConfigDialog').show()");
                    break;
                case "LOOP":
                    PrimeFaces.current().ajax().update("loopConfigDialog");
                    PrimeFaces.current().executeScript("PF('loopConfigDialog').show()");
                    break;
                case "LIST":
                    PrimeFaces.current().ajax().update("listConfigDialog");
                    PrimeFaces.current().executeScript("PF('listConfigDialog').show()");
                    break;
                case "NOTIFICATION":
                    PrimeFaces.current().ajax().update("notificationConfigDialog");
                    PrimeFaces.current().executeScript("PF('notificationConfigDialog').show()");
                    break;
                case "TERMINATE":
                    PrimeFaces.current().ajax().update("terminateConfigDialog");
                    PrimeFaces.current().executeScript("PF('terminateConfigDialog').show()");
                    break;
            }
        }
    }

    public void addNewAndCondition() {
        if (selectedLSM != null) {
            selectedLSM.getDecisionCriteria().getANDConditionList().add(new WFConditionModel());
        }
    }

    public void addNewOrCondition() {
        if (selectedLSM != null) {
            selectedLSM.getDecisionCriteria().getORConditionList().add(new WFConditionModel());
        }
    }

    public void removeAndCondition(WFConditionModel conditionModel) {
        if (selectedLSM != null) {
            selectedLSM.getDecisionCriteria().getANDConditionList().remove(conditionModel);
        }
    }

    public void removeOrCondition(WFConditionModel conditionModel) {
        if (selectedLSM != null) {
            selectedLSM.getDecisionCriteria().getORConditionList().remove(conditionModel);
        }
    }

    public void saveConfiguration() {
        if (selectedLSM != null) {
            activeRecordConfig = null;
            this.componentStore.put(selectedLSM.getSequenceId(), selectedLSM);
        }
    }

    public LogicalSequenceModel traverseLSM(LogicalSequenceModel source, String componentId, LogicalSequenceModel target) {
        if (source != null) {
            switch (source.getBlockType()) {
                case "DECISION":
                    traverseLSM(source.getJunction().get("TRUE"), componentId, target);
                    traverseLSM(source.getJunction().get("FALSE"), componentId, target);
                    break;
                case "LOOP":
                    traverseLSM(source.getActivity().getLoopConfiguration().getLoopContext(), componentId, target);
                    traverseLSM(source.getNextActivity(), componentId, target);
                    break;
                default:
                    if (source.getSequenceId().equals(componentId)) {
                        source.setNextActivity(target);
                        return null;
                    }
                    else {
                        traverseLSM(source.getNextActivity(), componentId, target);
                    }
                    break;
            }

        }
        return null;
    }

    public LogicalSequenceModel traverseLSMWithDecision(LogicalSequenceModel source, String componentId, LogicalSequenceModel target, String decision) {
        if (source != null) {
            if (source.getSequenceId().equals(componentId)) {
                if (source.getJunction() != null) {
                    source.getJunction().put(decision, target);
                }
                else {
                    source.setJunction(new LinkedHashMap<>());
                    source.getJunction().put(decision, target);
                }
                return null;
            }
            else {
                switch (source.getBlockType()) {
                    case "LOOP":
                        traverseLSMWithDecision(source.getActivity().getLoopConfiguration().getLoopContext(), componentId, target, decision);
                        traverseLSMWithDecision(source.getNextActivity(), componentId, target, decision);
                        break;
                    case "DECISION":
                        traverseLSMWithDecision(source.getJunction().get("TRUE"), componentId, target, decision);
                        traverseLSMWithDecision(source.getJunction().get("FALSE"), componentId, target, decision);
                        break;
                    default:
                        traverseLSMWithDecision(source.getNextActivity(), componentId, target, decision);
                        break;
                }
            }
        }
        return null;
    }

    public LogicalSequenceModel traverseLoopLSM(LogicalSequenceModel source, String componentId, LogicalSequenceModel target, String decision) {
        if (source != null) {
            if (source.getSequenceId().equals(componentId)) {
                if (decision.equalsIgnoreCase("CONTEXT")) {
                    if (source.getActivity() != null) {
                        source.getActivity().getLoopConfiguration().setLoopContext(target);
                    }
                    else {
                        source.getActivity().setLoopConfiguration(new LoopConfiguration());
                        source.getActivity().getLoopConfiguration().setLoopContext(target);
                    }
                }
                else {
                    source.setNextActivity(target);
                }
                return null;
            }
            else {
                if (source.getNextActivity().getBlockType().equals("DECISION")) {
                    traverseLoopLSM(source.getNextActivity().getJunction().get("TRUE"), componentId, target, decision);
                    traverseLoopLSM(source.getNextActivity().getJunction().get("FALSE"), componentId, target, decision);
                }
                else {
                    traverseLoopLSM(source.getNextActivity(), componentId, target, decision);
                }
            }
        }
        return null;
    }

    public void buildDiagram(Element parent, String parentType, String decision, LogicalSequenceModel lsm) {
        Element component = new Element();
        component.setX(lsm.getX());
        component.setY(lsm.getY());
        EndPoint endPointTarget;
        EndPoint endPointSource;
        switch (lsm.getBlockType()) {
            case "INITIATOR":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                component.setStyleClass("wf-component-base initiator-comp");
                EndPoint endPoint = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPoint.setSource(true);
                component.addEndPoint(endPoint);
                break;
            case "DECISION":
                componentStore.put(lsm.getSequenceId(), lsm);
                for (WFConditionModel wfcm : lsm.getDecisionCriteria().getANDConditionList()) {
                    if (!wfcm.isFreeForm() && wfcm.getModuleId() == 0) {
                        wfcm.setModuleId(workflowManager.getTargetModuleId());
                    }
                }
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.TOP);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base decision-comp");
                component.addEndPoint(endPointTarget);//f44242
                endPointSource = createRectangleEndPoint(EndPointAnchor.LEFT, "#f44242", "FALSE", lsm.getSequenceId());
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                endPointSource = createRectangleEndPoint(EndPointAnchor.RIGHT, "#45f74b", "TRUE", lsm.getSequenceId());
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "LOOP":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setTitle("LOOP");
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base loop-comp");
                component.addEndPoint(endPointTarget);//f44242
                endPointSource = createRectangleEndPoint(EndPointAnchor.RIGHT, "#4286f4", "CONTEXT", lsm.getSequenceId());
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM, "#98AFC7", "POSTLOOP", lsm.getSequenceId());
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "RECORD_MANAGER":
                componentStore.put(lsm.getSequenceId(), lsm);
                if (lsm.getActivity() != null && lsm.getActivity().getRecordManagerConfiguration() != null && lsm.getActivity().getRecordManagerConfiguration().getFieldsConfig() != null) {
                    for (RecordFieldConfig rfc : lsm.getActivity().getRecordManagerConfiguration().getFieldsConfig()) {
                        if (rfc.getFieldModel().getFieldType().equals("LOOKUP")) {
                            super.selectionMap.put(rfc.getFieldModel().getFieldName(), new SelectionModel(rfc.getValueConfiguration().getValue()));
                            PrimeFaces.current().executeScript("PF('" + rfc.getFieldModel().getFieldName() + "').selectValue(" + rfc.getValueConfiguration().getValue() + ")");
                        }
                    }
                }

                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base record-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "ACTIVITY":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base task-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "EMAIL":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base mail-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "LIST":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base list-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "NOTIFICATION":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base notification-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "TERMINATE":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base terminate-comp");
                component.addEndPoint(endPointTarget);
                break;
        }
        diagramModel.addElement(component);
        if (parent != null) {
            Connection conn;
            switch (parentType) {
                case "DECISION":
                    if (decision.equals("TRUE")) {
                        conn = new Connection(parent.getEndPoints().get(2), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    }
                    else {
                        conn = new Connection(parent.getEndPoints().get(1), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    }
                    break;
                case "LOOP":
                    if (decision.equals("CONTEXT")) {
                        conn = new Connection(parent.getEndPoints().get(1), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    }
                    else {
                        conn = new Connection(parent.getEndPoints().get(2), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    }
                    break;
                case "INITIATOR":
                    conn = new Connection(parent.getEndPoints().get(0), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    break;
                default:
                    conn = new Connection(parent.getEndPoints().get(1), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    break;
            }
            diagramModel.connect(conn);
        }
        switch (lsm.getBlockType()) {
            case "DECISION":
                if (lsm.getJunction() != null) {
                    if (lsm.getJunction().get("TRUE") != null) {
                        buildDiagram(component, lsm.getBlockType(), "TRUE", lsm.getJunction().get("TRUE"));
                    }

                    if (lsm.getJunction().get("FALSE") != null) {
                        buildDiagram(component, lsm.getBlockType(), "FALSE", lsm.getJunction().get("FALSE"));
                    }
                }
                break;
            case "LOOP":
                if (lsm.getActivity() != null) {
                    if (lsm.getActivity().getLoopConfiguration().getLoopContext() != null) {
                        buildDiagram(component, lsm.getBlockType(), "CONTEXT", lsm.getActivity().getLoopConfiguration().getLoopContext());
                    }
                    else {
                        break;
                    }
                }

                if (lsm.getNextActivity() != null) {
                    buildDiagram(component, lsm.getBlockType(), "POSTLOOP", lsm.getNextActivity());
                }
                break;
            default:
                if (lsm.getNextActivity() != null) {
                    buildDiagram(component, lsm.getBlockType(), null, lsm.getNextActivity());
                }
                break;
        }

    }

    public void buildDiagramAligned(Element parent, String parentType, String decision, LogicalSequenceModel lsm, String x, String y) {
        Element component = new Element();
        if (x == null) {
            component.setX(lsm.getX());
            x = lsm.getX();
        }
        else {
//            x = x.replaceAll("px", "");
//            x = String.valueOf(Float.valueOf(x) + 10) + "px";
            component.setX(x);
        }
        if (y == null) {
            component.setY(lsm.getY());
            y = lsm.getY();
        }
        else {
            y = y.replaceAll("px", "");
            y = String.valueOf(Float.valueOf(y) + 130) + "px";
            component.setY(y);
//            component.setY(lsm.getY());
        }
        EndPoint endPointTarget;
        EndPoint endPointSource;
        switch (lsm.getBlockType()) {
            case "INITIATOR":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                component.setStyleClass("wf-component-base initiator-comp");
                EndPoint endPoint = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPoint.setSource(true);
                component.addEndPoint(endPoint);
                break;
            case "DECISION":
                componentStore.put(lsm.getSequenceId(), lsm);
                for (WFConditionModel wfcm : lsm.getDecisionCriteria().getANDConditionList()) {
                    if (!wfcm.isFreeForm() && wfcm.getModuleId() == 0) {
                        wfcm.setModuleId(workflowManager.getTargetModuleId());
                    }
                }
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.TOP);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base decision-comp");
                component.addEndPoint(endPointTarget);//f44242
                endPointSource = createRectangleEndPoint(EndPointAnchor.LEFT, "#f44242", "FALSE", lsm.getSequenceId());
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                endPointSource = createRectangleEndPoint(EndPointAnchor.RIGHT, "#45f74b", "TRUE", lsm.getSequenceId());
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "LOOP":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setTitle("LOOP");
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base loop-comp");
                component.addEndPoint(endPointTarget);//f44242
                endPointSource = createRectangleEndPoint(EndPointAnchor.RIGHT, "#4286f4", "CONTEXT", lsm.getSequenceId());
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM, "#98AFC7", "POSTLOOP", lsm.getSequenceId());
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "RECORD_MANAGER":
                componentStore.put(lsm.getSequenceId(), lsm);
                if (lsm.getActivity() != null && lsm.getActivity().getRecordManagerConfiguration() != null && lsm.getActivity().getRecordManagerConfiguration().getFieldsConfig() != null) {
                    for (RecordFieldConfig rfc : lsm.getActivity().getRecordManagerConfiguration().getFieldsConfig()) {
                        if (rfc.getFieldModel().getFieldType().equals("LOOKUP")) {
                            super.selectionMap.put(rfc.getFieldModel().getFieldName(), new SelectionModel(rfc.getValueConfiguration().getValue()));
                            PrimeFaces.current().executeScript("PF('" + rfc.getFieldModel().getFieldName() + "').selectValue(" + rfc.getValueConfiguration().getValue() + ")");
                        }
                    }
                }

                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base record-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "ACTIVITY":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base task-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "EMAIL":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base mail-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "LIST":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base list-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "NOTIFICATION":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base notification-comp");
                component.addEndPoint(endPointTarget);
                endPointSource = createRectangleEndPoint(EndPointAnchor.BOTTOM);
                endPointSource.setSource(true);
                component.addEndPoint(endPointSource);
                break;
            case "TERMINATE":
                componentStore.put(lsm.getSequenceId(), lsm);
                component.setData(lsm);
                endPointTarget = createDotEndPoint(EndPointAnchor.AUTO_DEFAULT);
                endPointTarget.setTarget(true);
                component.setStyleClass("wf-component-base terminate-comp");
                component.addEndPoint(endPointTarget);
                break;
        }
        diagramModel.addElement(component);
        if (parent != null) {
            Connection conn;
            switch (parentType) {
                case "DECISION":
                    if (decision.equals("TRUE")) {
                        conn = new Connection(parent.getEndPoints().get(2), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    }
                    else {
                        conn = new Connection(parent.getEndPoints().get(1), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    }
                    break;
                case "LOOP":
                    if (decision.equals("CONTEXT")) {
                        conn = new Connection(parent.getEndPoints().get(1), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    }
                    else {
                        conn = new Connection(parent.getEndPoints().get(2), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    }
                    break;
                case "INITIATOR":
                    conn = new Connection(parent.getEndPoints().get(0), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    break;
                default:
                    conn = new Connection(parent.getEndPoints().get(1), component.getEndPoints().get(0), diagramModel.getDefaultConnector());
                    break;
            }
            diagramModel.connect(conn);
        }
        switch (lsm.getBlockType()) {
            case "DECISION":
                if (lsm.getJunction() != null) {
                    if (lsm.getJunction().get("TRUE") != null) {
                        String dtx = x.replaceAll("px", "");
                        dtx = String.valueOf(Float.valueOf(dtx) + 120) + "px";
                        buildDiagramAligned(component, lsm.getBlockType(), "TRUE", lsm.getJunction().get("TRUE"), dtx, y);
                    }

                    if (lsm.getJunction().get("FALSE") != null) {
                        String dfx = x.replaceAll("px", "");
                        dfx = String.valueOf(Float.valueOf(dfx) - 120) + "px";
                        buildDiagramAligned(component, lsm.getBlockType(), "FALSE", lsm.getJunction().get("FALSE"), dfx, y);
                    }
                }
                break;
            case "LOOP":
                if (lsm.getActivity() != null) {
                    if (lsm.getActivity().getLoopConfiguration().getLoopContext() != null) {
                        String dlx = x.replaceAll("px", "");
                        dlx = String.valueOf(Float.valueOf(dlx) + 120) + "px";
                        buildDiagramAligned(component, lsm.getBlockType(), "CONTEXT", lsm.getActivity().getLoopConfiguration().getLoopContext(), dlx, y);
                    }
                    else {
                        break;
                    }
                }

                if (lsm.getNextActivity() != null) {
                    buildDiagramAligned(component, lsm.getBlockType(), "POSTLOOP", lsm.getNextActivity(), x, y);
                }
                break;
            default:
                if (lsm.getNextActivity() != null) {
                    buildDiagramAligned(component, lsm.getBlockType(), null, lsm.getNextActivity(), x, y);
                }
                break;
        }

    }

    public static LogicalSequenceModel deepCopyLSM(LogicalSequenceModel lsm) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String str = mapper.writeValueAsString(lsm);
            LogicalSequenceModel rm = mapper.readValue(str, LogicalSequenceModel.class);
            return rm;
        }
        catch (IOException ex) {
            return null;
        }
    }

    public void onComponentConnect(ConnectEvent event) {
        if (currentLSM != null) {
            LogicalSequenceModel sourceLSM = (LogicalSequenceModel) event.getSourceElement().getData();
            LogicalSequenceModel destinationLSM = (LogicalSequenceModel) event.getTargetElement().getData();
            switch (sourceLSM.getBlockType()) {
                case "DECISION":
                    traverseLSMWithDecision(currentLSM, sourceLSM.getSequenceId(), destinationLSM, event.getSourceEndPoint().getId().replace("-" + sourceLSM.getSequenceId(), ""));
                    break;
                case "LOOP":
                    traverseLoopLSM(currentLSM, sourceLSM.getSequenceId(), destinationLSM, event.getSourceEndPoint().getId().replace("-" + sourceLSM.getSequenceId(), ""));
                    break;
                default:
                    traverseLSM(currentLSM, sourceLSM.getSequenceId(), destinationLSM);
                    break;
            }
        }
    }

    public void onComponentDisconect(DisconnectEvent event) {
        if (currentLSM != null) {
            LogicalSequenceModel sourceLSM = (LogicalSequenceModel) event.getSourceElement().getData();
            LogicalSequenceModel destinationLSM = (LogicalSequenceModel) event.getTargetElement().getData();
            switch (sourceLSM.getBlockType()) {
                case "DECISION":
                    traverseLSMWithDecision(currentLSM, sourceLSM.getSequenceId(), null, event.getSourceEndPoint().getId().replace("-" + sourceLSM.getSequenceId(), ""));
                    break;
                case "LOOP":
                    traverseLoopLSM(currentLSM, sourceLSM.getSequenceId(), null, event.getSourceEndPoint().getId().replace("-" + sourceLSM.getSequenceId(), ""));
                    break;
                default:
                    traverseLSM(currentLSM, sourceLSM.getSequenceId(), null);
                    break;
            }
        }
    }

    public void onComponentConnectionChange(ConnectionChangeEvent event) {
    }

    public void buildSequenceModel(LogicalSequenceModel lsm, String parentSequenceId) {
//        currentLSM
    }

    private EndPoint createDotEndPoint(EndPointAnchor anchor) {
        DotEndPoint endPoint = new DotEndPoint(anchor);
        endPoint.setScope("network");
        endPoint.setTarget(true);
        endPoint.setStyle("{fillStyle:'#98AFC7',cx:4,cy:4,r:4}");
        endPoint.setRadius(4);
        endPoint.setStyleClass("target-flow");
        endPoint.setHoverStyle("{fillStyle:'#5C738B'}");

        return endPoint;
    }

    private EndPoint createRectangleEndPoint(EndPointAnchor anchor) {
        RectangleEndPoint endPoint = new RectangleEndPoint(anchor);
        endPoint.setScope("network");
        endPoint.setSource(true);
        endPoint.setStyleClass("source-flow");
        endPoint.setStyle("{fillStyle:'#98AFC7',width:7,height:7}");
        endPoint.setHoverStyle("{fillStyle:'#5C738B'}");

        return endPoint;
    }

    private EndPoint createRectangleEndPoint(EndPointAnchor anchor, String color, String id, String seqId) {
        RectangleEndPoint endPoint = new RectangleEndPoint(anchor);
        endPoint.setScope("network");
        endPoint.setSource(true);
        endPoint.setId(id + "-" + seqId);
        endPoint.setStyleClass("source-flow");
        endPoint.setStyle("{fillStyle:'" + color + "',stroke:'" + color + "',width:7,height:7}");
        endPoint.setHoverStyle("{fillStyle:'" + color + "'}");
        return endPoint;
    }

    public DefaultDiagramModel getDiagramModel() {
        return diagramModel;
    }

    public void setDiagramModel(DefaultDiagramModel diagramModel) {
        this.diagramModel = diagramModel;
    }

    public LogicalSequenceModel getLogicalSequenceModel() {
        return logicalSequenceModel;
    }

    public void setLogicalSequenceModel(LogicalSequenceModel logicalSequenceModel) {
        this.logicalSequenceModel = logicalSequenceModel;
    }

    public LogicalSequenceModel getSelectedLSM() {
        return selectedLSM;
    }

    public void setSelectedLSM(LogicalSequenceModel selectedLSM) {
        this.selectedLSM = selectedLSM;
    }

    public ArrayList<ModuleFieldModel> getAvailableColumnList() {
        return availableColumnList;
    }

    public void setAvailableColumnList(ArrayList<ModuleFieldModel> availableColumnList) {
        this.availableColumnList = availableColumnList;
    }

    public ArrayList<ModuleModel> getAvailableModuleList() {
        return availableModuleList;
    }

    public void setAvailableModuleList(ArrayList<ModuleModel> availableModuleList) {
        this.availableModuleList = availableModuleList;
    }

    public RecordFieldConfig getActiveRecordConfig() {
        return activeRecordConfig;
    }

    public void setActiveRecordConfig(RecordFieldConfig activeRecordConfig) {
        this.activeRecordConfig = activeRecordConfig;
    }

    public ArrayList<MailEntity> getEntitiesList() {
        return entitiesList;
    }

    public void setEntitiesList(ArrayList<MailEntity> entitiesList) {
        this.entitiesList = entitiesList;
    }

    public String getMailHolder() {
        return mailHolder;
    }

    public void setMailHolder(String mailHolder) {
        this.mailHolder = mailHolder;
    }

    public ArrayList<ModuleFieldModel> getMailTargetFieldSet() {
        return mailTargetFieldSet;
    }

    public void setMailTargetFieldSet(ArrayList<ModuleFieldModel> mailTargetFieldSet) {
        this.mailTargetFieldSet = mailTargetFieldSet;
    }

    public ModuleModel getSelectedMailModule() {
        return selectedMailModule;
    }

    public void setSelectedMailModule(ModuleModel selectedMailModule) {
        this.selectedMailModule = selectedMailModule;
    }

    public ModuleFieldModel getSelectedMailField() {
        return selectedMailField;
    }

    public void setSelectedMailField(ModuleFieldModel selectedMailField) {
        this.selectedMailField = selectedMailField;
    }

    public ModuleModel getSelectedCCMailModule() {
        return selectedCCMailModule;
    }

    public void setSelectedCCMailModule(ModuleModel selectedCCMailModule) {
        this.selectedCCMailModule = selectedCCMailModule;
    }

    public ModuleFieldModel getSelectedCCMailField() {
        return selectedCCMailField;
    }

    public void setSelectedCCMailField(ModuleFieldModel selectedCCMailField) {
        this.selectedCCMailField = selectedCCMailField;
    }

    public ModuleModel getSelectedBCCMailModule() {
        return selectedBCCMailModule;
    }

    public void setSelectedBCCMailModule(ModuleModel selectedBCCMailModule) {
        this.selectedBCCMailModule = selectedBCCMailModule;
    }

    public ModuleFieldModel getSelectedBCCMailField() {
        return selectedBCCMailField;
    }

    public void setSelectedBCCMailField(ModuleFieldModel selectedBCCMailField) {
        this.selectedBCCMailField = selectedBCCMailField;
    }

    public BeezerWorkflow getWorkflowManager() {
        return workflowManager;
    }

    public void setWorkflowManager(BeezerWorkflow workflowManager) {
        this.workflowManager = workflowManager;
    }

    @Override
    public void save() {
        try {
            WorkflowRequest request = new WorkflowRequest();
            if (updateProcess) {
                request.setRequestActionType("3");
            }
            else {
                request.setRequestActionType("1");
            }
            request.setLogicalSequenceModel(currentLSM);
            request.setWorkflowId(Integer.parseInt(requestedWorkflowId));
            WorkflowHandler handler = new WorkflowHandler();
            WorkflowResponse response = handler.executeForConfiguration(request);
            if (response.getErrorCode() == 1000) {
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void edit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("workflowManager.xhtml");
        }
        catch (IOException ex) {
            Logger.getLogger(WorkflowBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void loadRecord() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public DefaultDiagramModel getDiagramModel2() {
        return diagramModel2;
    }

    public void setDiagramModel2(DefaultDiagramModel diagramModel2) {
        this.diagramModel2 = diagramModel2;
    }

    public HashMap<String, String> getModuleMap() {
        return moduleMap;
    }

    public void setModuleMap(HashMap<String, String> moduleMap) {
        this.moduleMap = moduleMap;
    }

    public ArrayList<ModuleModel> getFullModuleList() {
        return fullModuleList;
    }

    public void setFullModuleList(ArrayList<ModuleModel> fullModuleList) {
        this.fullModuleList = fullModuleList;
    }

    public LinkedHashMap<Integer, ArrayList<ModuleFieldModel>> getContextParamMap() {
        return contextParamMap;
    }

    public void setContextParamMap(LinkedHashMap<Integer, ArrayList<ModuleFieldModel>> contextParamMap) {
        this.contextParamMap = contextParamMap;
    }

}
