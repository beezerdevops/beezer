/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.workflow;

import com.beezer.web.beans.admin.permissions.PermissionManagerBean;
import com.beezer.web.handler.ModuleHandler;
import com.process.handler.WorkflowHandler;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.workflow.BeezerWorkflow;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.requests.managers.WorkflowRequest;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.responses.managers.WorkflowResponse;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author badry
 */
@ManagedBean(name = "workflowManagerBean")
@ViewScoped
public class WorkflowManagerBean {

    private ArrayList<BeezerWorkflow> workflowList;
    private BeezerWorkflow newWorkflow;
    private ArrayList<ModuleModel> moduleList;
    private boolean editMode;

    @PostConstruct
    public void init() {
        this.newWorkflow = new BeezerWorkflow();
        this.loadWorkflows();
        this.loadModuleList();
    }

    public void loadWorkflows() {
        try {
            WorkflowRequest workflowRequest = new WorkflowRequest();
            workflowRequest.setRequestActionType("0");
            WorkflowHandler handler = new WorkflowHandler();
            WorkflowResponse response = handler.executeForManager(workflowRequest);
            if (response.getErrorCode() == 1000) {
                this.workflowList = response.getBeezerWorkflowList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadModuleList() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.moduleList = response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(PermissionManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void editWorkflow(BeezerWorkflow beezerWorkflow) {
        this.editMode = true;
        this.newWorkflow = beezerWorkflow;
        PrimeFaces.current().executeScript("PF('addWFDialog').show()");
    }

    public void openDesigner(BeezerWorkflow beezerWorkflow) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("workflowDesigner.xhtml?wf="
                    + beezerWorkflow.getWorkflowId());
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteWorkflow(BeezerWorkflow beezerWorkflow) {
        try {
            WorkflowRequest workflowRequest = new WorkflowRequest();
            workflowRequest.setRequestActionType("2");
            workflowRequest.setBeezerWorkflow(beezerWorkflow);
            WorkflowHandler handler = new WorkflowHandler();
            WorkflowResponse response = handler.executeForManager(workflowRequest);
            if (response.getErrorCode() == 1000) {
                this.workflowList.remove(beezerWorkflow);
//                this.workflowList = response.getBeezerWorkflowList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void showAddWFForm() {
        this.editMode = false;
        this.newWorkflow = new BeezerWorkflow();
        PrimeFaces.current().executeScript("PF('addWFDialog').show()");
    }

    public void addWorkflow() {
        if (this.editMode) {
            this.updateWorkflow();
        }
        else {
            try {
                WorkflowRequest workflowRequest = new WorkflowRequest();
                workflowRequest.setRequestActionType("1");
                workflowRequest.setBeezerWorkflow(newWorkflow);
                WorkflowHandler handler = new WorkflowHandler();
                WorkflowResponse response = handler.executeForManager(workflowRequest);
                if (response.getErrorCode() == 1000) {
                    this.loadWorkflows();
                }
            }
            catch (Exception ex) {
                Logger.getLogger(WorkflowManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void updateWorkflow() {
        try {
            WorkflowRequest workflowRequest = new WorkflowRequest();
            workflowRequest.setRequestActionType("3");
            workflowRequest.setBeezerWorkflow(newWorkflow);
            WorkflowHandler handler = new WorkflowHandler();
            WorkflowResponse response = handler.executeForManager(workflowRequest);
            if (response.getErrorCode() == 1000) {
                this.loadWorkflows();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(WorkflowManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<BeezerWorkflow> getWorkflowList() {
        return workflowList;
    }

    public void setWorkflowList(ArrayList<BeezerWorkflow> workflowList) {
        this.workflowList = workflowList;
    }

    public BeezerWorkflow getNewWorkflow() {
        return newWorkflow;
    }

    public void setNewWorkflow(BeezerWorkflow newWorkflow) {
        this.newWorkflow = newWorkflow;
    }

    public ArrayList<ModuleModel> getModuleList() {
        return moduleList;
    }

    public void setModuleList(ArrayList<ModuleModel> moduleList) {
        this.moduleList = moduleList;
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

}
