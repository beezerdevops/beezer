/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.permissions;

import com.beezer.web.handler.PermissionHandler;
import com.crm.models.internal.security.permissions.PermissionModel;
import com.crm.models.requests.managers.PermissionRequest;
import com.crm.models.responses.managers.PermissionsResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "permissionListBean")
@ViewScoped
public class PermissionListBean {
    
    @ManagedProperty(value = "#{permissionsList}")
    private ArrayList<PermissionModel> permissionsList;
    private PermissionModel selectedPermissionModel;
    
    @PostConstruct
    public void init() {
        this.loadPermissionsList();
    }
    
    public void loadPermissionsList(){
        try {
            PermissionRequest permissionRequest = new PermissionRequest();
            permissionRequest.setRequestActionType("0");
            PermissionHandler permissionHandler = new PermissionHandler();
            PermissionsResponse permissionsResponse = permissionHandler.execute(permissionRequest);
            if(permissionsResponse.getErrorCode() == 1000){
                this.permissionsList = permissionsResponse.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(PermissionListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deletePermission(PermissionModel permission){
        try {
            PermissionRequest permissionRequest = new PermissionRequest();
            permissionRequest.setRequestActionType("2");
            permissionRequest.setPermissionModel(permission);
            PermissionHandler permissionHandler = new PermissionHandler();
            PermissionsResponse permissionsResponse = permissionHandler.execute(permissionRequest);
            if(permissionsResponse.getErrorCode() == 1000){
                this.loadPermissionsList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(PermissionListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onRowSelect(SelectEvent event){
        try {
            PermissionModel selectedPermission = (PermissionModel) event.getObject();
            FacesContext.getCurrentInstance().getExternalContext().redirect("permissionManager.xhtml?permission="
                    + selectedPermission.getPermissionId());
        }
        catch (IOException ex) {
            Logger.getLogger(PermissionListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<PermissionModel> getPermissionsList() {
        return permissionsList;
    }

    public void setPermissionsList(ArrayList<PermissionModel> permissionsList) {
        this.permissionsList = permissionsList;
    }

    public PermissionModel getSelectedPermissionModel() {
        return selectedPermissionModel;
    }

    public void setSelectedPermissionModel(PermissionModel selectedPermissionModel) {
        this.selectedPermissionModel = selectedPermissionModel;
    }
    
    
}
