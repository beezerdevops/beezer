/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.permissions;

import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.PermissionHandler;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.filter.dynamic.FilterConfig;
import com.crm.models.internal.filter.dynamic.FilterHolder;
import com.crm.models.internal.security.permissions.ObjectPermissionModel;
import com.crm.models.internal.security.permissions.PermissionModel;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.requests.managers.PermissionRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.responses.managers.PermissionsResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author badry
 */
@ManagedBean(name = "permissionManagerBean")
@ViewScoped
public class PermissionManagerBean {

    private PermissionModel permissionModel;
    private ArrayList<ModuleModel> moduleList;
    private ArrayList<ModuleFieldModel> fieldList;
    private String requestedPermission;
    private ObjectPermissionModel selectedPermissionModel;
    private ArrayList<ModuleFieldModel> moduleFieldSet;

    @PostConstruct
    public void init() {
        this.permissionModel = new PermissionModel();
        this.getRequestParams();
        this.initializePermissionModel();
    }

    public void getRequestParams() {
        Map<String, String> requestParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        requestedPermission = requestParams.get("permission");
    }

    public void initializePermissionModel() {
        if (this.requestedPermission != null && !this.requestedPermission.isEmpty()) {
            try {
                PermissionRequest permissionRequest = new PermissionRequest();
                permissionRequest.setRequestActionType("0");
                FilterManager fm = new FilterManager();
                fm.setFilter("AND", new FilterType("permission_id", "=", this.requestedPermission));
                permissionRequest.setClause(fm.getClause());
                PermissionHandler handler = new PermissionHandler();
                PermissionsResponse response = handler.execute(permissionRequest);
                if (response.getErrorCode() == 1000 && response.getReturnList() != null && !response.getReturnList().isEmpty()) {
                    this.permissionModel = response.getReturnList().get(0);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(PermissionManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            try {
                ModuleHandler moduleHandler = new ModuleHandler();
                ModuleManagerRequest request = new ModuleManagerRequest();
                request.setActionType("0");
                ModuleManagerResponse response = moduleHandler.managerExecutor(request);
                if (response.getErrorCode() == 1000) {
                    this.moduleList = response.getReturnList();
                    ArrayList<ObjectPermissionModel> modulePermissionList = new ArrayList<>();
                    for (ModuleModel mm : moduleList) {
                        ObjectPermissionModel obm = new ObjectPermissionModel();
                        obm.setObjectId(mm.getModuleId());
                        obm.setPermissionId(0);
                        obm.setObjectName(mm.getModuleName());
                        obm.setVisible(true);
                        obm.setFieldPermissionList(this.convertToPermissionModel(this.loadFieldList(String.valueOf(obm.getObjectId()))));
                        modulePermissionList.add(obm);
                    }
                    this.permissionModel.setModulePermissionList(modulePermissionList);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(PermissionManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ArrayList<ModuleFieldModel> loadFieldList(String moduleId) {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(moduleId);
            request.setActionType("0");
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.moduleFieldSet = response.getReturnList();
                return response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(PermissionManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private ArrayList<ObjectPermissionModel> convertToPermissionModel(ArrayList<ModuleFieldModel> fieldList) {
        if (fieldList != null) {
            ArrayList<ObjectPermissionModel> fieldPermissionList = new ArrayList<>();
            for (ModuleFieldModel mfm : fieldList) {
                ObjectPermissionModel obm = new ObjectPermissionModel();
                obm.setObjectId(mfm.getFieldId());
                obm.setObjectName(mfm.getFieldLabel());
                obm.setVisible(true);
                fieldPermissionList.add(obm);
            }
            return fieldPermissionList;
        }
        return null;
    }

    public void save() {
        try {
            PermissionRequest permissionRequest = new PermissionRequest();
            if (requestedPermission != null && !requestedPermission.isEmpty()) {
                permissionRequest.setRequestActionType("3");
            }
            else {
                permissionRequest.setRequestActionType("1");
            }
            permissionRequest.setPermissionModel(permissionModel);
            PermissionHandler permissionHandler = new PermissionHandler();
            PermissionsResponse response = permissionHandler.execute(permissionRequest);
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("permissionList.xhtml");
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add permission set."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(PermissionManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("permissionList.xhtml");
        }
        catch (IOException ex) {
            Logger.getLogger(PermissionManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addNewANDCondition() {
        selectedPermissionModel.getFiltering().getANDFilterList().add(new FilterConfig());
    }

    public void addNewORCondition() {
        selectedPermissionModel.getFiltering().getORFilterList().add(new FilterConfig());
    }

    public void initializeSelectedPermission(ObjectPermissionModel obm) {
        this.selectedPermissionModel = obm;
    }

    public void initializeSelectedPermissionFiltering(ObjectPermissionModel obm) {
        this.loadFieldList(String.valueOf(obm.getObjectId()));
        this.selectedPermissionModel = obm;
        if (selectedPermissionModel.getFiltering() == null) {
            selectedPermissionModel.setFiltering(new FilterHolder());
            this.selectedPermissionModel.getFiltering().setANDFilterList(new ArrayList<>());
            this.selectedPermissionModel.getFiltering().setORFilterList(new ArrayList<>());
            this.selectedPermissionModel.getFiltering().setGroupByList(new ArrayList<>());
            this.selectedPermissionModel.getFiltering().setOrderByList(new ArrayList<>());
        }
    }
    
    public void addNewGroup() {
        this.selectedPermissionModel.getFiltering().getGroupByList().add(new ModuleFieldModel());
    }
    
    public void addNewOrderBy() {
        this.selectedPermissionModel.getFiltering().getOrderByList().add(new ModuleFieldModel());
    }
    
    public void removeANDCondition(FilterConfig rfm) {
        this.selectedPermissionModel.getFiltering().getANDFilterList().remove(rfm);
    }
    
    public void removeORCondition(FilterConfig rfm) {
        this.selectedPermissionModel.getFiltering().getORFilterList().remove(rfm);
    }
    
    public void removeGroup(ModuleFieldModel mfm) {
        this.selectedPermissionModel.getFiltering().getGroupByList().remove(mfm);
    }
    
    public void removeOrderBy(ModuleFieldModel mfm) {
        this.selectedPermissionModel.getFiltering().getOrderByList().remove(mfm);
    }

    public void saveFieldConfig() {
        PermissionModel temp = new PermissionModel();
        temp.setModulePermissionList(new ArrayList<>());
        for (ObjectPermissionModel obm : permissionModel.getModulePermissionList()) {
            if (obm.getObjectId() == selectedPermissionModel.getObjectId()) {
                temp.getModulePermissionList().add(selectedPermissionModel);
            }
            else {
                temp.getModulePermissionList().add(obm);
            }
        }
        this.permissionModel.setModulePermissionList(temp.getModulePermissionList());
        PrimeFaces.current().executeScript("PF('fieldsDialog').hide()");
    }
    
    public void saveFilteringQuery(){
        PermissionModel temp = new PermissionModel();
        temp.setModulePermissionList(new ArrayList<>());
        for (ObjectPermissionModel obm : permissionModel.getModulePermissionList()) {
            if (obm.getObjectId() == selectedPermissionModel.getObjectId()) {
                temp.getModulePermissionList().add(selectedPermissionModel);
            }
            else {
                temp.getModulePermissionList().add(obm);
            }
        }
        this.permissionModel.setModulePermissionList(temp.getModulePermissionList());
        PrimeFaces.current().executeScript("PF('filtersDialog').hide()");
    }

    public PermissionModel getPermissionModel() {
        return permissionModel;
    }

    public void setPermissionModel(PermissionModel permissionModel) {
        this.permissionModel = permissionModel;
    }

    public ArrayList<ModuleModel> getModuleList() {
        return moduleList;
    }

    public void setModuleList(ArrayList<ModuleModel> moduleList) {
        this.moduleList = moduleList;
    }

    public ArrayList<ModuleFieldModel> getFieldList() {
        return fieldList;
    }

    public void setFieldList(ArrayList<ModuleFieldModel> fieldList) {
        this.fieldList = fieldList;
    }

    public ObjectPermissionModel getSelectedPermissionModel() {
        return selectedPermissionModel;
    }

    public void setSelectedPermissionModel(ObjectPermissionModel selectedPermissionModel) {
        this.selectedPermissionModel = selectedPermissionModel;
    }

    public ArrayList<ModuleFieldModel> getModuleFieldSet() {
        return moduleFieldSet;
    }

    public void setModuleFieldSet(ArrayList<ModuleFieldModel> moduleFieldSet) {
        this.moduleFieldSet = moduleFieldSet;
    }

}
