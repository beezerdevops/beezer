/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.roles;

import com.beezer.web.handler.PermissionHandler;
import com.beezer.web.handler.RoleHandler;
import com.crm.models.global.UserModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.security.RoleModel;
import com.crm.models.internal.security.permissions.PermissionModel;
import com.crm.models.requests.managers.PermissionRequest;
import com.crm.models.requests.managers.RolesRequest;
import com.crm.models.responses.managers.PermissionsResponse;
import com.crm.models.responses.managers.RoleResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.DualListModel;

/**
 *
 * @author badry
 */
@ManagedBean(name = "roleManagerBean")
@ViewScoped
public class RoleManagerBean {

    private RoleModel roleModel;
    private String requestedRole;
    
    @ManagedProperty(value = "#{permissionListBean.permissionsList}")
    private ArrayList<PermissionModel> permissionsList;
    private DualListModel<UserModel> userList;

    @ManagedProperty(value = "#{usersBean.usersList}")
    private ArrayList<UserModel> users;

    @PostConstruct
    public void init() {
        this.roleModel = new RoleModel();
        this.getRequestParams();
        this.loadRequestedRole();
    }

    public void getRequestParams() {
        Map<String, String> requestParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        requestedRole = requestParams.get("role");
    }

    public void loadRequestedRole() {
        if (requestedRole != null && !requestedRole.isEmpty()) {
            try {
                RolesRequest rolesRequest = new RolesRequest();
                rolesRequest.setRequestActionType("0");
                FilterManager fm = new FilterManager();
                fm.setFilter("AND", new FilterType("role_id", "=", requestedRole));
                rolesRequest.setClause(fm.getClause());
                RoleHandler roleHandler = new RoleHandler();
                RoleResponse roleResponse = roleHandler.execute(rolesRequest);
                if (roleResponse.getErrorCode() == 1000) {
                    roleModel = roleResponse.getRolesList().get(0);
//                    RequestContext.getCurrentInstance().execute("PF('rolePermission').selectValue(" + roleModel.getPermissionSet() + ")");

                    rolesRequest = new RolesRequest();
                    rolesRequest.setRequestActionType("4");
                    rolesRequest.setRoleModel(roleModel);
                    roleHandler = new RoleHandler();
                    roleResponse = roleHandler.execute(rolesRequest);
                    if (roleResponse.getErrorCode() == 1000) {
                        this.initializeUserList(roleResponse.getRoleMembersList());
                    }
                }
            }
            catch (Exception ex) {
                Logger.getLogger(RoleManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            this.initializeUserList(null);
        }
    }

    public void loadPermissionsList() {
        try {
            PermissionRequest permissionRequest = new PermissionRequest();
            permissionRequest.setRequestActionType("0");
            PermissionHandler permissionHandler = new PermissionHandler();
            PermissionsResponse permissionsResponse = permissionHandler.execute(permissionRequest);
            if (permissionsResponse.getErrorCode() == 1000) {
                this.permissionsList = permissionsResponse.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RoleManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initializeUserList(ArrayList<UserModel> subscribers) {
        if (subscribers != null && !subscribers.isEmpty()) {
            if (users != null) {
                List<UserModel> nonSubscribers = new ArrayList<>();
                for (UserModel um1 : users) {
                    boolean found = false;
                    for (UserModel um2 : subscribers) {
                        if (um1.getUserId() == um2.getUserId()) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        nonSubscribers.add(um1);
                    }
                }
                List<UserModel> userSourceList = nonSubscribers;
                List<UserModel> userTargetList = subscribers;
                userList = new DualListModel<>(userSourceList, userTargetList);
            }
        }
        else {
            List<UserModel> userSourceList = users;
            List<UserModel> userTargetList = new ArrayList<>();
            userList = new DualListModel<>(userSourceList, userTargetList);
        }
    }

    public void save() {
        try {
            RolesRequest rolesRequest = new RolesRequest();
            if (requestedRole != null && !requestedRole.isEmpty()) {
                rolesRequest.setRequestActionType("3");
            }
            else {
                rolesRequest.setRequestActionType("1");
            }
            rolesRequest.setRoleModel(roleModel);
            RoleHandler roleHandler = new RoleHandler();
            RoleResponse roleResponse = roleHandler.execute(rolesRequest);
            if (roleResponse.getErrorCode() == 1000) {
                rolesRequest.setMemberList((ArrayList<UserModel>) userList.getTarget());
                roleModel.setRoleId(roleResponse.getObjectId());
                rolesRequest.setRoleModel(roleModel);
                rolesRequest.setRequestActionType("5");
                roleResponse = roleHandler.execute(rolesRequest);
                if (roleResponse.getErrorCode() == 1000) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("roles.xhtml");
                }
                else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add role users."));
                }
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add role."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RoleManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("roles.xhtml");
        }
        catch (IOException ex) {
            Logger.getLogger(RoleManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public RoleModel getRoleModel() {
        return roleModel;
    }

    public void setRoleModel(RoleModel roleModel) {
        this.roleModel = roleModel;
    }

    public ArrayList<PermissionModel> getPermissionsList() {
        return permissionsList;
    }

    public void setPermissionsList(ArrayList<PermissionModel> permissionsList) {
        this.permissionsList = permissionsList;
    }

    public DualListModel<UserModel> getUserList() {
        return userList;
    }

    public void setUserList(DualListModel<UserModel> userList) {
        this.userList = userList;
    }

    public ArrayList<UserModel> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<UserModel> users) {
        this.users = users;
    }

}
