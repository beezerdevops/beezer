/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.roles;

import com.beezer.web.beans.admin.permissions.PermissionListBean;
import com.beezer.web.handler.RoleHandler;
import com.crm.models.global.UserModel;
import com.crm.models.internal.security.RoleModel;
import com.crm.models.internal.security.permissions.PermissionModel;
import com.crm.models.requests.managers.RolesRequest;
import com.crm.models.responses.managers.RoleResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "rolesBean")
@ViewScoped
public class RolesBean {

    private ArrayList<RoleModel> rolesList;
    private ArrayList<UserModel> subscriberList;
    private RoleModel roleModel;
    private RoleModel selectedRole;

    @PostConstruct
    public void init() {
        this.roleModel = new RoleModel();
        this.loadRoleList();
    }

    public void loadRoleList() {
        try {
            RolesRequest rolesRequest = new RolesRequest();
            rolesRequest.setRequestActionType("0");
            RoleHandler roleHandler = new RoleHandler();
            RoleResponse roleResponse = roleHandler.execute(rolesRequest);
            if (roleResponse.getErrorCode() == 1000) {
                this.rolesList = roleResponse.getRolesList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RolesBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteRole(RoleModel roleModel) {
        try {
            RolesRequest rolesRequest = new RolesRequest();
            rolesRequest.setRequestActionType("2");
            rolesRequest.setRoleModel(roleModel);
            RoleHandler roleHandler = new RoleHandler();
            RoleResponse roleResponse = roleHandler.execute(rolesRequest);
            if (roleResponse.getErrorCode() == 1000) {
                this.loadRoleList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RolesBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowSelect(SelectEvent event) {
        try {
            RoleModel selectedRoleModel = (RoleModel) event.getObject();
            FacesContext.getCurrentInstance().getExternalContext().redirect("roleManager.xhtml?role="
                    + selectedRoleModel.getRoleId());
        }
        catch (IOException ex) {
            Logger.getLogger(PermissionListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<RoleModel> getRolesList() {
        return rolesList;
    }

    public void setRolesList(ArrayList<RoleModel> rolesList) {
        this.rolesList = rolesList;
    }

    public ArrayList<UserModel> getSubscriberList() {
        return subscriberList;
    }

    public void setSubscriberList(ArrayList<UserModel> subscriberList) {
        this.subscriberList = subscriberList;
    }

    public RoleModel getRoleModel() {
        return roleModel;
    }

    public void setRoleModel(RoleModel roleModel) {
        this.roleModel = roleModel;
    }

    public RoleModel getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(RoleModel selectedRole) {
        this.selectedRole = selectedRole;
    }

}
