/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin;

import com.beezer.web.beans.admin.appBuilder.fields.FieldsManagerBean;
import com.beezer.web.beans.modules.deals.AddDealBean;
import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.RelationHandler;
import com.beezer.web.handler.TableSettingsHandler;
import com.crm.models.comparators.FormFieldOrderComparator;
import com.crm.models.comparators.TableSettingsOrderComparator;
import com.crm.models.internal.DataTableSettings;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.requests.DataTableRequest;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.DataTableResponse;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.RelationResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.DualListModel;
import org.primefaces.model.TreeNode;

/**
 *
 * @author badry
 */
@ManagedBean(name = "tableViewDesignerBean")
@ViewScoped
public class TableViewDesignerBean {

    private ArrayList<ModuleFieldModel> fieldsList;
    private String selectModuleId;
    private TreeNode availableFields;
    private ArrayList<DataTableSettings> existingTableSettings;
    private ArrayList<DataTableSettings> availableTableSettings;
    private ArrayList<String> dummyList;
    private ArrayList<DataTableSettings> jointTableSettings;
    private DualListModel<DataTableSettings> columnSettingsDualList;

    @PostConstruct
    public void init() {
        this.initialize();
    }

    public void initialize() {
//        FaceletContext faceletContext = (FaceletContext) FacesContext.getCurrentInstance().getAttributes().get(FaceletContext.FACELET_CONTEXT_KEY);
//        this.selectModuleId = (String) faceletContext.getAttribute("moduleId");
//        selectModuleId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("selectModuleId");
        if (selectModuleId != null) {
            this.loadExistingTableSettings(selectModuleId);
            this.loadFieldSet();
            this.loadRelationList();
            if (fieldsList != null) {
                this.generateAvailableFieldsTree();
                jointTableSettings = new ArrayList<>(availableTableSettings);
                jointTableSettings.addAll(existingTableSettings);
                columnSettingsDualList = new DualListModel<>(availableTableSettings, existingTableSettings);
            }

        }
    }

    public void loadFieldSet() {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(selectModuleId);
            request.setActionType("0");
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.fieldsList = response.getReturnList();
                Collections.sort(fieldsList, new FormFieldOrderComparator());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadRelationList() {
        try {
            RelationRequest request = new RelationRequest();
            request.setModuleId(selectModuleId);
            request.setActionType("4");
            RelationHandler relationHandler = new RelationHandler();
            RelationResponse response = relationHandler.relationExecutor(request);
            if (response.getErrorCode() == 1000) {
                ArrayList<RelationManagerModel> relationsList = response.getRelationManagerList();
                if (relationsList != null && !relationsList.isEmpty()) {
                    for (RelationManagerModel rmm : relationsList) {
                        ModuleFieldModel mfm = new ModuleFieldModel();
                        mfm.setFieldLabel(rmm.getViewColumn());
                        mfm.setFieldName(rmm.getViewColumn());
                        mfm.setTableName(rmm.getDetailModuleName());
                        mfm.setFieldType("TEXT");
                        fieldsList.add(mfm);
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(TableViewDesignerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected RelationManagerModel loadRelation(String fieldName, String relationId) {
        RelationRequest relationRequest = new RelationRequest();
        relationRequest.setRelationId(relationId);
        RelationHandler handler = new RelationHandler();
        RelationResponse relationResponse;
        try {
            relationResponse = handler.getRelation(relationRequest);
            if (relationResponse.getErrorCode() == 1000) {
                return relationResponse.getRelationManager();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(AddDealBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void loadExistingTableSettings(String moduleId) {
        if (moduleId != null) {
            try {
                DataTableRequest request = new DataTableRequest();
                request.setModuleId(moduleId);
                request.setActionType("0");
                TableSettingsHandler settingsHandler = new TableSettingsHandler();
                DataTableResponse response = settingsHandler.execute(request);
                if (response.getErrorCode() == 1000) {
                    this.setExistingTableSettings(response.getReturnList());
                    Collections.sort(this.getExistingTableSettings(), new TableSettingsOrderComparator());
                }
            }
            catch (Exception ex) {
                Logger.getLogger(TableViewDesignerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            this.existingTableSettings = new ArrayList<>();
            this.existingTableSettings.add(new DataTableSettings());
        }
    }

    private void generateAvailableFieldsTree() {
        availableFields = new DefaultTreeNode("Root", null);
        TreeNode root = new DefaultTreeNode("Fields", availableFields);
        root.setExpanded(true);
        ArrayList<ModuleFieldModel> tempFields = new ArrayList<>(fieldsList);
        for (DataTableSettings dts : existingTableSettings) {
            for (ModuleFieldModel mfm : fieldsList) {
                if (dts.getColumnFieldName().equals(mfm.getFieldName())) {
                    dts.setExtra(mfm.getTableName());
                    tempFields.remove(mfm);
                }
            }
        }

        this.availableTableSettings = new ArrayList<>();
        int order = 1;
        for (ModuleFieldModel mfm : tempFields) {
            DataTableSettings dts = new DataTableSettings();
            dts.setModuleId(selectModuleId);
            dts.setFilterMode("contains");
            dts.setColumnType(mfm.getFieldType());
            dts.setColumnFieldName(mfm.getFieldName());
            dts.setColumnLabel(mfm.getFieldLabel());
            dts.setIsFilterable(true);
            dts.setIsSortable(true);
            dts.setOrder(order);
            dts.setExtra(mfm.getTableName());
            order++;
            this.availableTableSettings.add(dts);
//            TreeNode model = new DefaultTreeNode("field", mfm, root);
        }
    }

    public void orderColumnList() {
        int order = 1;
        if (this.existingTableSettings != null) {
            for (DataTableSettings dts : existingTableSettings) {
                dts.setOrder(order);
                order++;
            }
        }
    }

    public void onTransfer(TransferEvent event) {
        this.existingTableSettings = (ArrayList<DataTableSettings>) this.columnSettingsDualList.getTarget();
        this.orderColumnList();
    }

    public void onSelect(SelectEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Selected", event.getObject().toString()));
    }

    public void onUnselect(UnselectEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Item Unselected", event.getObject().toString()));
    }

    public void onReorder() {
        this.existingTableSettings = (ArrayList<DataTableSettings>) this.columnSettingsDualList.getTarget();
        this.orderColumnList();
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "List Reordered", null));
    }

    public void save() {
        try {
            DataTableRequest request = new DataTableRequest();
            for (DataTableSettings dts : existingTableSettings) {
                if (dts.getColumnType() != null) {
                    switch (dts.getColumnType()) {
                        case "LOOKUP":
                            dts.setColumnFieldName(dts.getColumnFieldName() + ".name");
                            break;
                        case "REL":
                            String relId = null;
                            for (ModuleFieldModel mfm : fieldsList) {
                                if (mfm.getFieldName().equals(dts.getColumnFieldName())) {
                                    relId = mfm.getFieldValues();
                                    break;
                                }
                            }
                            if (relId != null && !relId.isEmpty()) {
                                RelationManagerModel rmm = this.loadRelation(dts.getColumnFieldName(), relId);
                                if (rmm != null) {
                                    dts.setColumnFieldName(rmm.getViewColumn());
                                }
                            }

                            break;
                    }
                }
            }
            request.setModuleId(selectModuleId);
            request.setColumnSettings(existingTableSettings);
            request.setActionType("3");
            TableSettingsHandler settingsHandler = new TableSettingsHandler();
            DataTableResponse response = settingsHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                this.loadExistingTableSettings(selectModuleId);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(TableViewDesignerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected void updateMessage(int errorCode, String errorMessage) {
        if (errorCode == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Added Susscefully."));
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add due to ." + errorMessage));
        }
    }

    public ArrayList<ModuleFieldModel> getFieldsList() {
        return fieldsList;
    }

    public void setFieldsList(ArrayList<ModuleFieldModel> fieldsList) {
        this.fieldsList = fieldsList;
    }

    public String getSelectModuleId() {
        return selectModuleId;
    }

    public void setSelectModuleId(String selectModuleId) {
        this.selectModuleId = selectModuleId;
    }

    public TreeNode getAvailableFields() {
        return availableFields;
    }

    public void setAvailableFields(TreeNode availableFields) {
        this.availableFields = availableFields;
    }

    public ArrayList<DataTableSettings> getExistingTableSettings() {
        return existingTableSettings;
    }

    public void setExistingTableSettings(ArrayList<DataTableSettings> existingTableSettings) {
        this.existingTableSettings = existingTableSettings;
    }

    public ArrayList<String> getDummyList() {
        return dummyList;
    }

    public void setDummyList(ArrayList<String> dummyList) {
        this.dummyList = dummyList;
    }

    public ArrayList<DataTableSettings> getAvailableTableSettings() {
        return availableTableSettings;
    }

    public void setAvailableTableSettings(ArrayList<DataTableSettings> availableTableSettings) {
        this.availableTableSettings = availableTableSettings;
    }

    public DualListModel<DataTableSettings> getColumnSettingsDualList() {
        return columnSettingsDualList;
    }

    public void setColumnSettingsDualList(DualListModel<DataTableSettings> columnSettingsDualList) {
        this.columnSettingsDualList = columnSettingsDualList;
    }

    public ArrayList<DataTableSettings> getJointTableSettings() {
        return jointTableSettings;
    }

    public void setJointTableSettings(ArrayList<DataTableSettings> jointTableSettings) {
        this.jointTableSettings = jointTableSettings;
    }

}
