/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.mail.entity;

import com.beezer.web.handler.MailHandler;
import com.crm.models.global.email.MailEntity;
import com.crm.models.requests.managers.MailRequest;
import com.crm.models.responses.managers.MailResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "mailEntitiesListBean")
@ViewScoped
public class MailEntitiesListBean {

    @ManagedProperty(value = "#{entitiesList}")
    private ArrayList<MailEntity> entitiesList;
    private MailEntity selectedEntity;

    @PostConstruct
    public void init() {
        this.loadEntitiesList();
    }

    public void loadEntitiesList() {
        try {
            MailRequest entityRequest = new MailRequest();
            entityRequest.setMailStore(true);
            entityRequest.setRequestActionType("0");
            MailHandler emMailHandler = new MailHandler();
            MailResponse mailResponse = emMailHandler.execute(entityRequest);
            if (mailResponse.getErrorCode() == 1000) {
                this.entitiesList = mailResponse.getEmailEntities();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(MailEntitiesListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteMailEntity(MailEntity mailEntity) {
        try {
            MailRequest mailRequest = new MailRequest();
            mailRequest.setMailStore(true);
            mailRequest.setRequestActionType("2");
            mailRequest.setEmailEntity(mailEntity);
            MailHandler handler = new MailHandler();
            MailResponse mailResponse = handler.execute(mailRequest);
            if (mailResponse.getErrorCode() == 1000) {
                this.loadEntitiesList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(MailEntitiesListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowSelect(SelectEvent event) {
        try {
            MailEntity mailEntity = (MailEntity) event.getObject();
            FacesContext.getCurrentInstance().getExternalContext().redirect("mailEntityManager.xhtml?entity="
                    + mailEntity.getId());
        }
        catch (IOException ex) {
            Logger.getLogger(MailEntitiesListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<MailEntity> getEntitiesList() {
        return entitiesList;
    }

    public void setEntitiesList(ArrayList<MailEntity> entitiesList) {
        this.entitiesList = entitiesList;
    }

    public MailEntity getSelectedEntity() {
        return selectedEntity;
    }

    public void setSelectedEntity(MailEntity selectedEntity) {
        this.selectedEntity = selectedEntity;
    }

}
