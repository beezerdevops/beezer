/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.mail.entity;

import com.beezer.web.handler.MailHandler;
import com.beezer.web.handler.MailServerHandler;
import com.crm.models.global.email.MailEntity;
import com.crm.models.global.email.MailServerConfigurationModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.managers.MailRequest;
import com.crm.models.requests.managers.MailServerRequest;
import com.crm.models.responses.managers.MailResponse;
import com.crm.models.responses.managers.MailServerResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author badry
 */
@ManagedBean(name = "mailEntityManagerBean")
@ViewScoped
public class MailEntityManagerBean {

    private MailEntity mailEntity;
    private String requestedConfiguration;
    private ArrayList<MailServerConfigurationModel> serversList;

    @PostConstruct
    public void init() {
        this.mailEntity = new MailEntity();
        this.getRequestParams();
        this.loadServerList();
        this.initalizeEntityModel();
    }

    public void getRequestParams() {
        Map<String, String> requestParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        requestedConfiguration = requestParams.get("entity");
    }
    
    public void loadServerList() {
        try {
            MailServerRequest serverRequest = new MailServerRequest();
            serverRequest.setRequestActionType("0");
            MailServerHandler serverHandler = new MailServerHandler();
            MailServerResponse serverResponse = serverHandler.execute(serverRequest);
            if (serverResponse.getErrorCode() == 1000) {
                this.serversList = serverResponse.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(MailEntityManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initalizeEntityModel() {
        if (this.requestedConfiguration != null && !this.requestedConfiguration.isEmpty()) {
            try {
                MailRequest mailRequest = new MailRequest();
                mailRequest.setMailStore(true);
                mailRequest.setRequestActionType("0");
                FilterManager fm = new FilterManager();
                fm.setFilter("AND", new FilterType("id", "=", this.requestedConfiguration));
                mailRequest.setClause(fm.getClause());
                MailHandler handler = new MailHandler();
                MailResponse response = handler.execute(mailRequest);
                if (response.getErrorCode() == 1000 && response.getReturnList() != null && !response.getReturnList().isEmpty()) {
                    this.mailEntity = response.getEmailEntities().get(0);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(MailEntityManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void save() {
        try {
            MailRequest mailRequest = new MailRequest();
            mailRequest.setMailStore(true);
            if (requestedConfiguration != null && !requestedConfiguration.isEmpty()) {
                mailRequest.setRequestActionType("3");
            }
            else {
                mailRequest.setRequestActionType("1");
            }
            mailRequest.setEmailEntity(mailEntity);
            MailHandler mailHandler = new MailHandler();
            MailResponse response = mailHandler.execute(mailRequest);
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("mailEntityList.xhtml");
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add mail."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(MailEntityManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("mailEntityList.xhtml");
        }
        catch (IOException ex) {
            Logger.getLogger(MailEntityManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public MailEntity getMailEntity() {
        return mailEntity;
    }

    public void setMailEntity(MailEntity mailEntity) {
        this.mailEntity = mailEntity;
    }

    public ArrayList<MailServerConfigurationModel> getServersList() {
        return serversList;
    }

    public void setServersList(ArrayList<MailServerConfigurationModel> serversList) {
        this.serversList = serversList;
    }

}
