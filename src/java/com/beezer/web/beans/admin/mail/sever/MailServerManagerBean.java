/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.mail.sever;

import com.beezer.web.handler.MailServerHandler;
import com.crm.models.global.email.MailServerConfigurationModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.managers.MailServerRequest;
import com.crm.models.responses.managers.MailServerResponse;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author badry
 */
@ManagedBean(name = "mailServerManagerBean")
@ViewScoped
public class MailServerManagerBean {

    private MailServerConfigurationModel serverConfigModel;
    private String requestedConfiguration;

    @PostConstruct
    public void init() {
        this.serverConfigModel = new MailServerConfigurationModel();
        this.getRequestParams();
        this.initializePermissionModel();
    }

    public void getRequestParams() {
        Map<String, String> requestParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        requestedConfiguration = requestParams.get("server");
    }

    public void initializePermissionModel() {
        if (this.requestedConfiguration != null && !this.requestedConfiguration.isEmpty()) {
            try {
                MailServerRequest mailServerRequest = new MailServerRequest();
                mailServerRequest.setRequestActionType("0");
                FilterManager fm = new FilterManager();
                fm.setFilter("AND", new FilterType("id", "=", this.requestedConfiguration));
                mailServerRequest.setClause(fm.getClause());
                MailServerHandler handler = new MailServerHandler();
                MailServerResponse response = handler.execute(mailServerRequest);
                if (response.getErrorCode() == 1000 && response.getReturnList() != null && !response.getReturnList().isEmpty()) {
                    this.serverConfigModel = response.getReturnList().get(0);
                }
            }
            catch (Exception ex) {
                Logger.getLogger(MailServerManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void save() {
        try {
            MailServerRequest mailServerRequest = new MailServerRequest();
            if (requestedConfiguration != null && !requestedConfiguration.isEmpty()) {
                mailServerRequest.setRequestActionType("3");
            }
            else {
                mailServerRequest.setRequestActionType("1");
            }
            mailServerRequest.setServerConfigModel(serverConfigModel);
            MailServerHandler mailServerHandler = new MailServerHandler();
            MailServerResponse response = mailServerHandler.execute(mailServerRequest);
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("mailServerList.xhtml");
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add server."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(MailServerManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("mailServerList.xhtml");
        }
        catch (IOException ex) {
            Logger.getLogger(MailServerManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public MailServerConfigurationModel getServerConfigModel() {
        return serverConfigModel;
    }

    public void setServerConfigModel(MailServerConfigurationModel serverConfigModel) {
        this.serverConfigModel = serverConfigModel;
    }

}
