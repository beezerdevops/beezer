/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.mail.sever;

import com.beezer.web.handler.MailServerHandler;
import com.crm.models.global.email.MailServerConfigurationModel;
import com.crm.models.requests.managers.MailServerRequest;
import com.crm.models.responses.managers.MailServerResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "mailServerListBean")
@ViewScoped
public class MailServerListBean {

    @ManagedProperty(value = "#{serversList}")
    private ArrayList<MailServerConfigurationModel> serversList;
    private MailServerConfigurationModel selectedServer;

    @PostConstruct
    public void init() {
        this.loadServerList();
    }

    public void loadServerList() {
        try {
            MailServerRequest serverRequest = new MailServerRequest();
            serverRequest.setRequestActionType("0");
            MailServerHandler serverHandler = new MailServerHandler();
            MailServerResponse serverResponse = serverHandler.execute(serverRequest);
            if (serverResponse.getErrorCode() == 1000) {
                this.serversList = serverResponse.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(MailServerListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteServerConfig(MailServerConfigurationModel serverModel) {
        try {
            MailServerRequest mailServerRequest = new MailServerRequest();
            mailServerRequest.setRequestActionType("2");
            mailServerRequest.setServerConfigModel(serverModel);
            MailServerHandler serverHandler = new MailServerHandler();
            MailServerResponse mailServerResponse = serverHandler.execute(mailServerRequest);
            if (mailServerResponse.getErrorCode() == 1000) {
                this.loadServerList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(MailServerListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowSelect(SelectEvent event) {
        try {
            MailServerConfigurationModel mailServerConfigurationModel = (MailServerConfigurationModel) event.getObject();
            FacesContext.getCurrentInstance().getExternalContext().redirect("mailServerManager.xhtml?server="
                    + mailServerConfigurationModel.getConfigurationId());
        }
        catch (IOException ex) {
            Logger.getLogger(MailServerListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<MailServerConfigurationModel> getServersList() {
        return serversList;
    }

    public void setServersList(ArrayList<MailServerConfigurationModel> serversList) {
        this.serversList = serversList;
    }

    public MailServerConfigurationModel getSelectedServer() {
        return selectedServer;
    }

    public void setSelectedServer(MailServerConfigurationModel selectedServer) {
        this.selectedServer = selectedServer;
    }

}
