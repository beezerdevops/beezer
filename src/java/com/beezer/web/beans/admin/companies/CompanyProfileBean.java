/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.companies;

import com.beezer.web.handler.CompaniesHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.CompanyModel;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.CompanyRequest;
import com.crm.models.responses.CompanyResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "companyProfileBean")
@ViewScoped
public class CompanyProfileBean {

    private CompanyModel companyModel;
    private UnstructuredModel unstructuredModel;

    @PostConstruct
    public void init() {
        this.loadCompanyProfile();
    }
    
    public void loadCompanyProfile(){
        try {
            CompanyRequest request = new CompanyRequest();
            request.setRequestActionType("0");
            FilterManager fm = new FilterManager();
            fm.setFilter("AND", new FilterType("company_id", "=", GeneralUtils.getLoggedUser().getCompanyId()));
            request.setClause(fm.getClause());
            CompaniesHandler companiesHandler = new CompaniesHandler();
            CompanyResponse response = companiesHandler.execute(request);
            if(response.getErrorCode() == 1000){
                this.companyModel = response.getReturnList().get(0);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(CompanyProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void save(){
        try {
            CompanyRequest request = new CompanyRequest();
            request.setRequestActionType("3");
            request.setUnstructuredData(unstructuredModel);
            request.setCompanyModel(companyModel);
            CompaniesHandler companiesHandler = new CompaniesHandler();
            CompanyResponse response = companiesHandler.execute(request);
            if(response.getErrorCode() == 1000){
                //this.companyModel = response.getReturnList().get(0);
                this.loadCompanyProfile();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(CompanyProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void updateCompanyLogo(){
        try {
            CompanyRequest request = new CompanyRequest();
            request.setRequestActionType("4");
            request.setUnstructuredData(unstructuredModel);
            request.setCompanyModel(companyModel);
            CompaniesHandler companiesHandler = new CompaniesHandler();
            CompanyResponse response = companiesHandler.execute(request);
            if(response.getErrorCode() == 1000){
                //this.companyModel = response.getReturnList().get(0);
                this.loadCompanyProfile();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(CompanyProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void onContentSelect(FileUploadEvent event) {
        try {
            this.unstructuredModel = new UnstructuredModel();
            this.unstructuredModel.setFileExtenstion(GeneralUtils.getFileExtenstion(event.getFile().getFileName()));
            this.unstructuredModel.setFileName(event.getFile().getFileName());
            this.unstructuredModel.setFileSize(event.getFile().getSize());
            this.unstructuredModel.setContentStream(event.getFile().getContent());
            this.updateCompanyLogo();
        }
        catch (Exception ex) {
            Logger.getLogger(CompanyProfileBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public CompanyModel getCompanyModel() {
        return companyModel;
    }

    public void setCompanyModel(CompanyModel companyModel) {
        this.companyModel = companyModel;
    }

    public UnstructuredModel getUnstructuredModel() {
        return unstructuredModel;
    }

    public void setUnstructuredModel(UnstructuredModel unstructuredModel) {
        this.unstructuredModel = unstructuredModel;
    }
    
    
}
