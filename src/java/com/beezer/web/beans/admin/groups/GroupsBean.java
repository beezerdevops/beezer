/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.groups;

import com.beezer.web.beans.admin.permissions.PermissionListBean;
import com.beezer.web.handler.GroupHandler;
import com.crm.models.global.UserModel;
import com.crm.models.internal.security.GroupModel;
import com.crm.models.internal.security.permissions.PermissionModel;
import com.crm.models.requests.managers.GroupRequest;
import com.crm.models.responses.managers.GroupResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "groupsBean")
@ViewScoped
public class GroupsBean {

    private ArrayList<GroupModel> groupList;
    private ArrayList<UserModel> subscriberList;
    private GroupModel selectedGroup;

    @PostConstruct
    public void init() {
        this.loadGroupList();
    }

    public void loadGroupList() {
        try {
            GroupRequest groupRequest = new GroupRequest();
            groupRequest.setRequestActionType("0");
            GroupHandler groupHandler = new GroupHandler();
            GroupResponse groupResponse = groupHandler.execute(groupRequest);
            if (groupResponse.getErrorCode() == 1000) {
                this.groupList = groupResponse.getGroupsList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(GroupsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteGroup(GroupModel groupModel) {
        try {
            GroupRequest groupRequest = new GroupRequest();
            groupRequest.setRequestActionType("2");
            groupRequest.setGroupModel(groupModel);
            GroupHandler groupHandler = new GroupHandler();
            GroupResponse groupResponse = groupHandler.execute(groupRequest);
            if (groupResponse.getErrorCode() == 1000) {
                this.loadGroupList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(GroupsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowSelect(SelectEvent event) {
        try {
            GroupModel selectedGroupModel = (GroupModel) event.getObject();
            FacesContext.getCurrentInstance().getExternalContext().redirect("groupManager.xhtml?group="
                    + selectedGroupModel.getGroupId());
        }
        catch (IOException ex) {
            Logger.getLogger(PermissionListBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<UserModel> getSubscriberList() {
        return subscriberList;
    }

    public void setSubscriberList(ArrayList<UserModel> subscriberList) {
        this.subscriberList = subscriberList;
    }

    public ArrayList<GroupModel> getGroupList() {
        return groupList;
    }

    public void setGroupList(ArrayList<GroupModel> groupList) {
        this.groupList = groupList;
    }

    public GroupModel getSelectedGroup() {
        return selectedGroup;
    }

    public void setSelectedGroup(GroupModel selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

}
