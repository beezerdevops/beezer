/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.groups;

import com.beezer.web.beans.admin.permissions.PermissionManagerBean;
import com.beezer.web.handler.GroupHandler;
import com.beezer.web.handler.PermissionHandler;
import com.crm.models.global.UserModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.security.GroupModel;
import com.crm.models.internal.security.permissions.PermissionModel;
import com.crm.models.requests.managers.GroupRequest;
import com.crm.models.requests.managers.PermissionRequest;
import com.crm.models.responses.managers.GroupResponse;
import com.crm.models.responses.managers.PermissionsResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DualListModel;

/**
 *
 * @author badry
 */
@ManagedBean(name = "groupManagerBean")
@ViewScoped
public class GroupManagerBean {

    private GroupModel groupModel;
    private String requestedGroup;
    
    @ManagedProperty(value = "#{permissionListBean.permissionsList}")
    private ArrayList<PermissionModel> permissionsList;
    private DualListModel<UserModel> userList;

    @ManagedProperty(value = "#{usersBean.usersList}")
    private ArrayList<UserModel> users;

    @PostConstruct
    public void init() {
        this.groupModel = new GroupModel();
        this.getRequestParams();
        this.loadRequestedGroup();
    }

    public void getRequestParams() {
        Map<String, String> requestParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        requestedGroup = requestParams.get("group");
    }

    public void loadRequestedGroup() {
        if (requestedGroup != null && !requestedGroup.isEmpty()) {
            try {
                GroupRequest groupRequest = new GroupRequest();
                groupRequest.setRequestActionType("0");
                FilterManager fm = new FilterManager();
                fm.setFilter("AND", new FilterType("group_id", "=", requestedGroup));
                groupRequest.setClause(fm.getClause());
                GroupHandler groupHandler = new GroupHandler();
                GroupResponse groupResponse = groupHandler.execute(groupRequest);
                if (groupResponse.getErrorCode() == 1000) {
                    groupModel = groupResponse.getGroupsList().get(0);
                    PrimeFaces.current().executeScript("PF('groupPermission').selectValue(" + groupModel.getPermissionSet() + ")");

                    groupRequest = new GroupRequest();
                    groupRequest.setRequestActionType("4");
                    groupRequest.setGroupModel(groupModel);
                    groupHandler = new GroupHandler();
                    groupResponse = groupHandler.execute(groupRequest);
                    if (groupResponse.getErrorCode() == 1000) {
                        this.initializeUserList(groupResponse.getGroupMembersList());
                    }
                }
            }
            catch (Exception ex) {
                Logger.getLogger(GroupManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else {
            this.initializeUserList(null);
        }
    }

    public void loadPermissionsList() {
        try {
            PermissionRequest permissionRequest = new PermissionRequest();
            permissionRequest.setRequestActionType("0");
            PermissionHandler permissionHandler = new PermissionHandler();
            PermissionsResponse permissionsResponse = permissionHandler.execute(permissionRequest);
            if (permissionsResponse.getErrorCode() == 1000) {
                this.permissionsList = permissionsResponse.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(GroupManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void initializeUserList(ArrayList<UserModel> subscribers) {
        if (subscribers != null && !subscribers.isEmpty()) {
            if (users != null) {
                List<UserModel> nonSubscribers = new ArrayList<>();
                for (UserModel um1 : users) {
                    boolean found = false;
                    for (UserModel um2 : subscribers) {
                        if (um1.getUserId() == um2.getUserId()) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        nonSubscribers.add(um1);
                    }
                }
                List<UserModel> userSourceList = nonSubscribers;
                List<UserModel> userTargetList = subscribers;
                userList = new DualListModel<>(userSourceList, userTargetList);
            }
        }
        else {
            List<UserModel> userSourceList = users;
            List<UserModel> userTargetList = new ArrayList<>();
            userList = new DualListModel<>(userSourceList, userTargetList);
        }
    }

    public void save() {
        try {
            GroupRequest groupRequest = new GroupRequest();
            if (requestedGroup != null && !requestedGroup.isEmpty()) {
                groupRequest.setRequestActionType("3");
            }
            else {
                groupRequest.setRequestActionType("1");
            }
            groupRequest.setGroupModel(groupModel);
            GroupHandler groupHandler = new GroupHandler();
            GroupResponse groupResponse = groupHandler.execute(groupRequest);
            if (groupResponse.getErrorCode() == 1000) {
                groupRequest.setMemberList((ArrayList<UserModel>) userList.getTarget());
                groupModel.setGroupId(groupResponse.getObjectId());
                groupRequest.setGroupModel(groupModel);
                groupRequest.setRequestActionType("5");
                groupResponse = groupHandler.execute(groupRequest);
                if (groupResponse.getErrorCode() == 1000) {
                    FacesContext.getCurrentInstance().getExternalContext().redirect("groups.xhtml");
                }
                else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add group users."));
                }
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add group."));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(GroupManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cancel() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("groups.xhtml");
        }
        catch (IOException ex) {
            Logger.getLogger(PermissionManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public GroupModel getGroupModel() {
        return groupModel;
    }

    public void setGroupModel(GroupModel groupModel) {
        this.groupModel = groupModel;
    }

    public ArrayList<PermissionModel> getPermissionsList() {
        return permissionsList;
    }

    public void setPermissionsList(ArrayList<PermissionModel> permissionsList) {
        this.permissionsList = permissionsList;
    }

    public DualListModel<UserModel> getUserList() {
        return userList;
    }

    public void setUserList(DualListModel<UserModel> userList) {
        this.userList = userList;
    }

    public ArrayList<UserModel> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<UserModel> users) {
        this.users = users;
    }

}
