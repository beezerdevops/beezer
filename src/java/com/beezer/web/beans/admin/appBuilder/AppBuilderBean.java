/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder;

import com.beezer.web.beans.admin.appBuilder.modules.ModuleManagerBean;
import com.beezer.web.handler.apps.ApplicationsHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.application.AppModuleConfigModel;
import com.crm.models.internal.application.ApplicationModel;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.requests.managers.ApplicationRequest;
import com.crm.models.responses.managers.ApplicationResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.TabCloseEvent;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DualListModel;

/**
 *
 * @author badry
 */
@ManagedBean(name = "appBuilderBean")
@ViewScoped
public class AppBuilderBean {

    private ApplicationModel applicationModel;
    private UnstructuredModel unstructuredModel;
    private String groupName;
    private String activeGroup;
    private ArrayList<ModuleModel> moduleList;
    private ArrayList<ModuleModel> selectedModuleList;
    private String requestedAppId;
    private boolean editMode;
//    private DualListModel<ModuleModel> dualModuleList;

    private LinkedHashMap<String, DualListModel<ModuleModel>> groupDualListMap;

    @ManagedProperty("#{moduleManagerBean}")
    private ModuleManagerBean moduleManagerService;

    @PostConstruct
    public void init() {
        this.applicationModel = new ApplicationModel();
        this.applicationModel.setModuleConfigMap(new LinkedHashMap<>());
        this.moduleList = moduleManagerService.getModuleList();
        this.groupDualListMap = new LinkedHashMap<>();
        this.getRequestedParameter();
        if (editMode) {
            this.loadApp();
            this.initializeBuilder();
        }
    }

    public void getRequestedParameter() {
        this.requestedAppId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("app");
        editMode = requestedAppId != null;
    }

    public void loadApp() {
        ApplicationsHandler applicationsHandler = new ApplicationsHandler();
        ApplicationRequest applicationRequest = new ApplicationRequest();
        applicationRequest.setRequestActionType("0");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("app_id", "=", requestedAppId));
        applicationRequest.setClause(fm.getClause());
        ApplicationResponse response = applicationsHandler.execute(applicationRequest);
        if (response.getErrorCode() == 1000) {
            this.applicationModel = response.getApplicationList().get(0);
        }
    }

    public void initializeBuilder() {
        if (applicationModel != null) {
            if (applicationModel.getModuleConfigMap() != null) {
                for (Map.Entry<String, ArrayList<AppModuleConfigModel>> entrySet : this.applicationModel.getModuleConfigMap().entrySet()) {
                    ModuleModel moduleModel;
                    ArrayList<ModuleModel> tempModuleList = new ArrayList<>();
                    for (AppModuleConfigModel amcm : entrySet.getValue()) {
                        moduleModel = new ModuleModel();
                        moduleModel.setModuleId(amcm.getModuleId());
                        moduleModel.setModuleName(amcm.getModuleName());
                        tempModuleList.add(moduleModel);
                    }
                    ArrayList<ModuleModel> clonedModuleList = cloneModuleList();
                    clonedModuleList.removeAll(tempModuleList);
                    DualListModel<ModuleModel> dualModuleList = new DualListModel<>(clonedModuleList, tempModuleList);
                    this.groupDualListMap.put(entrySet.getKey(), dualModuleList);
                }
            }
        }
    }

    public void onContentSelect(FileUploadEvent event) {
        try {
            this.unstructuredModel = new UnstructuredModel();
            this.unstructuredModel.setFileExtenstion(GeneralUtils.getFileExtenstion(event.getFile().getFileName()));
            this.unstructuredModel.setFileName(event.getFile().getFileName());
            this.unstructuredModel.setFileSize(event.getFile().getSize());
            this.unstructuredModel.setContentStream(event.getFile().getContent());
        }
        catch (Exception ex) {
            Logger.getLogger(AppBuilderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void openAddGroupDialog() {
        this.groupName = "";
        PrimeFaces.current().ajax().update("addModuleGroupDialog");
        PrimeFaces.current().executeScript("PF('addModuleGroupDialog').show()");
    }

    public void addNewGroup() {
        if (groupName != null && !groupName.isEmpty()) {
            if (applicationModel.getModuleConfigMap().get(groupName) != null) {
                PrimeFaces.current().ajax().update("duplicateGroupMsgDialog");
                PrimeFaces.current().executeScript("PF('duplicateGroupMsgDialog').show()");
            }
            else {
                activeGroup = groupName;
                DualListModel<ModuleModel> dualModuleList = new DualListModel<>(cloneModuleList(), new ArrayList<>());
                this.groupDualListMap.put(groupName, dualModuleList);
                this.applicationModel.getModuleConfigMap().put(groupName, new ArrayList<>());
                PrimeFaces.current().ajax().update("moduleAccordionList");
            }
        }
    }

    public void saveModule() {
        this.moduleManagerService.addModule();
        this.moduleList = moduleManagerService.getModuleList();
        DualListModel<ModuleModel> dualModuleList = new DualListModel<>(cloneModuleList(), new ArrayList<>());
        this.groupDualListMap.put(activeGroup, dualModuleList);
        PrimeFaces.current().ajax().update("moduleAccordionList");
        PrimeFaces.current().executeScript("PF('moduleSideBar').hide()");
    }

    public void saveApp() {
        int groupOrder = 0;
        for (Map.Entry<String, ArrayList<AppModuleConfigModel>> entrySet : this.applicationModel.getModuleConfigMap().entrySet()) {
            groupOrder++;
            String key = entrySet.getKey();
            ArrayList<ModuleModel> selectedGroupModules = (ArrayList<ModuleModel>) this.groupDualListMap.get(key).getTarget();
            ArrayList<AppModuleConfigModel> appModuleConfigModels = new ArrayList<>();
            int moduleOrder = 0;
            for (ModuleModel mm : selectedGroupModules) {
                moduleOrder++;
                AppModuleConfigModel amcm = new AppModuleConfigModel();
                amcm.setGroupOrder(groupOrder);
                amcm.setModuleGroup(key);
                amcm.setModuleId(mm.getModuleId());
                amcm.setModuleName(mm.getModuleName());
                amcm.setModuleOrder(moduleOrder);
                appModuleConfigModels.add(amcm);
            }
            entrySet.setValue(appModuleConfigModels);
        }

        ApplicationsHandler applicationsHandler = new ApplicationsHandler();
        ApplicationRequest applicationRequest = new ApplicationRequest();
        if (!editMode) {
            applicationRequest.setRequestActionType("1");
        }
        else {
            applicationRequest.setRequestActionType("3");
        }
        applicationRequest.setApplicationModel(applicationModel);
        applicationRequest.setUnstructuredData(unstructuredModel);
        ApplicationResponse response = applicationsHandler.execute(applicationRequest);
        if (response.getErrorCode() == 1000) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Your app has been created");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        else {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Oops", "Something went wrong");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }

    private ArrayList<ModuleModel> cloneModuleList() {
        ArrayList<ModuleModel> clone = new ArrayList<>();
        for (ModuleModel mm : moduleList) {
            clone.add(mm);
        }
        return clone;
    }

    public void onTabChange(TabChangeEvent event) {
        activeGroup = event.getTab().getTitle();
    }

    public void onTabClose(TabCloseEvent event) {
        int x = 0;
    }

    public void onTransfer(TransferEvent event) {
        int x = 0;
    }

    public String onFlowProcess(FlowEvent event) {
        return event.getNewStep();
    }

    public ApplicationModel getApplicationModel() {
        return applicationModel;
    }

    public void setApplicationModel(ApplicationModel applicationModel) {
        this.applicationModel = applicationModel;
    }

    public UnstructuredModel getUnstructuredModel() {
        return unstructuredModel;
    }

    public void setUnstructuredModel(UnstructuredModel unstructuredModel) {
        this.unstructuredModel = unstructuredModel;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public ArrayList<ModuleModel> getModuleList() {
        return moduleList;
    }

    public void setModuleList(ArrayList<ModuleModel> moduleList) {
        this.moduleList = moduleList;
    }

    public ModuleManagerBean getModuleManagerService() {
        return moduleManagerService;
    }

    public void setModuleManagerService(ModuleManagerBean moduleManagerService) {
        this.moduleManagerService = moduleManagerService;
    }

    public LinkedHashMap<String, DualListModel<ModuleModel>> getGroupDualListMap() {
        return groupDualListMap;
    }

    public void setGroupDualListMap(LinkedHashMap<String, DualListModel<ModuleModel>> groupDualListMap) {
        this.groupDualListMap = groupDualListMap;
    }

}
