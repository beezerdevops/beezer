/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.forms.components;

import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.RelationHandler;
import com.crm.models.internal.DataTableSettings;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.internal.forms.EventMappingField;
import com.crm.models.internal.forms.EventSubscriber;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.RelationResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author badry
 */
public class TableComponentManager extends AdvancedComponentBase {

    private ArrayList<ModuleFieldModel> tableBindingModuleFieldsList;
    private ArrayList<FieldsLayoutModel> aggregatedFormFields;
    private ArrayList<RecordModel> dummyList;

    public TableComponentManager() {
        this.dummyList = new ArrayList<>();
    }

    public void onBindingModelSelect() {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(selectedField.getTableComponentConfiguration().getBindingModelId());
            request.setActionType("0");
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.tableBindingModuleFieldsList = response.getReturnList();

                RelationRequest relationRequest = new RelationRequest();
                relationRequest.setModuleId(selectedField.getTableComponentConfiguration().getBindingModelId());
                relationRequest.setActionType("4");
                RelationHandler relationHandler = new RelationHandler();
                RelationResponse relationResponse = relationHandler.relationExecutor(relationRequest);
                if (relationResponse.getErrorCode() == 1000) {
                    if (relationResponse.getRelationManagerList() != null) {
                        for (RelationManagerModel rmm : relationResponse.getRelationManagerList()) {
                            ModuleFieldModel mfm = new ModuleFieldModel();
                            mfm.setFieldName(rmm.getViewColumn());
                            this.tableBindingModuleFieldsList.add(mfm);
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(TableComponentManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addEventListenerToTable() {
        if (this.selectedField.getTableComponentConfiguration().getEventList() == null) {
            this.selectedField.getTableComponentConfiguration().setEventList(new ArrayList<>());
        }
        this.selectedField.getTableComponentConfiguration().getEventList().add(new EventSubscriber());
    }

    public void updateDataMapList() {
        for (DataTableSettings dtss : this.selectedField.getTableComponentConfiguration().getColumnSettings()) {
            if (dtss.getDataMap() == null) {
                dtss.setDataMap(new HashMap<>());
            }
            for (EventSubscriber es : this.selectedField.getTableComponentConfiguration().getEventList()) {
                if (dtss.getDataMap().get(es.getEventName()) == null) {
                    dtss.getDataMap().put(es.getEventName(), new EventMappingField());
                }
            }
        }
    }

    public void addColumnToTableComponent() {
        DataTableSettings dts = new DataTableSettings();
        dts.setVisible(true);
        this.selectedField.getTableComponentConfiguration().getColumnSettings().add(dts);
        for (DataTableSettings dtss : this.selectedField.getTableComponentConfiguration().getColumnSettings()) {
            if (dtss.getDataMap() == null) {
                dtss.setDataMap(new HashMap<>());
            }
            if (this.selectedField.getTableComponentConfiguration().getEventList() != null) {
                for (EventSubscriber es : this.selectedField.getTableComponentConfiguration().getEventList()) {
                    if (dtss.getDataMap().get(es.getEventName()) == null) {
                        dtss.getDataMap().put(es.getEventName(), new EventMappingField());
                    }
                }
            }
        }
    }

    public void addSummaryColumnToTableComponent() {
        if (this.selectedField.getTableComponentConfiguration().getSummaryColumnsSettings() == null) {
            this.selectedField.getTableComponentConfiguration().setSummaryColumnsSettings(new ArrayList<>());
        }
        this.selectedField.getTableComponentConfiguration().getSummaryColumnsSettings().add(new DataTableSettings());
    }

    public void aggregateFormFields() {
        this.aggregatedFormFields = new ArrayList<>();
        if (super.fieldsMap != null) {
            for (Map.Entry<Integer, ArrayList<FieldsLayoutModel>> entrySet : super.fieldsMap.entrySet()) {
                for (FieldsLayoutModel flm : entrySet.getValue()) {
                    if (flm.getComponentType().equals("NORM")) {
                        aggregatedFormFields.add(flm);
                    }
                }
            }
        }
    }

    public ArrayList<ModuleFieldModel> getTableBindingModuleFieldsList() {
        return tableBindingModuleFieldsList;
    }

    public void setTableBindingModuleFieldsList(ArrayList<ModuleFieldModel> tableBindingModuleFieldsList) {
        this.tableBindingModuleFieldsList = tableBindingModuleFieldsList;
    }

    public ArrayList<FieldsLayoutModel> getAggregatedFormFields() {
        return aggregatedFormFields;
    }

    public void setAggregatedFormFields(ArrayList<FieldsLayoutModel> aggregatedFormFields) {
        this.aggregatedFormFields = aggregatedFormFields;
    }

    public ArrayList<RecordModel> getDummyList() {
        return dummyList;
    }

    public void setDummyList(ArrayList<RecordModel> dummyList) {
        this.dummyList = dummyList;
    }

}
