/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.forms.components;

import com.crm.models.internal.components.ParameterConfiguaration;
import java.util.ArrayList;

/**
 *
 * @author badry
 */
public class DynoButtonManager extends AdvancedComponentBase {

    public void addNewParam() {
        if (super.selectedField.getDynoButtonConfiguration().getParams() == null) {
            super.selectedField.getDynoButtonConfiguration().setParams(new ArrayList<>());
        }
        super.selectedField.getDynoButtonConfiguration().getParams().add(new ParameterConfiguaration());
    }
}
