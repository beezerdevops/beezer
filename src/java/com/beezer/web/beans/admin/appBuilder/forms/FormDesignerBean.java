/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.forms;

import com.beezer.web.beans.admin.appBuilder.fields.FieldsManagerBean;
import com.beezer.web.beans.admin.appBuilder.forms.components.AdvancedComponentsManager;
import com.beezer.web.handler.FormsHandler;
import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.utils.GeneralUtils;
import com.beezer.web.utils.RandomUtils;
import com.crm.models.comparators.BlockOrderComparator;
import com.crm.models.comparators.FieldLayoutOrderComparator;
import com.crm.models.comparators.FormFieldOrderComparator;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.components.AdvancedComponent;
import com.crm.models.internal.components.DynoButton;
import com.crm.models.internal.components.LayoutComponent;
import com.crm.models.internal.components.TableComponent;
import com.crm.models.internal.forms.FieldBlockModel;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.forms.FormModel;
import com.crm.models.requests.managers.FormRequest;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.responses.managers.FormResponse;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author badry
 */
@ManagedBean(name = "formDesignerBean")
@ViewScoped
public class FormDesignerBean {

    private HashMap<String, String> moduleManagerMap;
    private ArrayList<ModuleFieldModel> fieldsList;
    private String selectModuleId;

    private ArrayList<FormModel> formList;
    private FormModel newForm;
    private FormModel selectedForm;
    private TreeNode availableFields;
    private FieldsLayoutModel selectedField;

    private TreeNode advancedComponentsTree;

    private ArrayList<FieldBlockModel> blockList;
    private HashMap<Integer, ArrayList<FieldsLayoutModel>> blockFieldsMap;
    private int tempBlockIdCounter = 0;

    private AdvancedComponentsManager advancedComponentsManager;

    @PostConstruct
    public void init() {
        this.loadModuleManagers();
        this.blockFieldsMap = new HashMap<>();
        this.selectedForm = new FormModel();
        this.selectedField = new FieldsLayoutModel();
        this.advancedComponentsManager = new AdvancedComponentsManager();
        this.advancedComponentsTree = this.advancedComponentsManager.buildAdvancedComponentsTree();
    }

    private void loadModuleManagers() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.moduleManagerMap = new HashMap<>();
                for (ModuleModel mm : response.getReturnList()) {
                    this.moduleManagerMap.put(mm.getModuleName(), String.valueOf(mm.getModuleId()));
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onModuleSelect() {
        this.loadFieldSet();
        this.loadForms();
        this.generateAvailableFieldsTree();
    }

    public void loadForms() {
        try {
            FormsHandler formsHandler = new FormsHandler();
            FormRequest request = new FormRequest();
            request.setActionType("0");
            request.setModuleId(Integer.parseInt(selectModuleId));
            FormResponse response = formsHandler.formExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.formList = response.getReturnList();
                if (this.formList != null) {
                    for (FormModel fm : this.formList) {
                        if (fm.isIsDefault()) {
                            newForm = fm;
                            selectedForm = fm;
                            Collections.sort(fm.getBlockList(), new BlockOrderComparator());
                            for (FieldBlockModel fbm : fm.getBlockList()) {
                                Collections.sort(fbm.getFieldsLayoutList(), new FieldLayoutOrderComparator());
                                for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
//                                    if (flm.getFieldObject().getFieldType().equals("ADV-EDITABLE")) {
//                                        this.loadAdvancedComponentConf(flm.getFieldObject().getFieldValues());
//                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadFieldSet() {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(selectModuleId);
            request.setActionType("0");
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.fieldsList = response.getReturnList();
                Collections.sort(fieldsList, new FormFieldOrderComparator());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addNewForm() {
        this.newForm = new FormModel();
        this.blockList = new ArrayList<>();
        this.addBlock();
        this.generateAvailableFieldsTree();
    }

    public boolean deleteForm() {
        try {
            FormsHandler formsHandler = new FormsHandler();
            FormRequest request = new FormRequest();
            request.setActionType("2");
            request.setModuleId(Integer.parseInt(selectModuleId));
            request.setFormModel(selectedForm);
            FormResponse response = formsHandler.formExecutor(request);
            return response.getErrorCode() == 1000;
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public void addBlock() {
        FieldBlockModel initialBlock = new FieldBlockModel();
        tempBlockIdCounter++;
        initialBlock.setBlockId(tempBlockIdCounter);
        this.blockList.add(initialBlock);
        this.blockFieldsMap.put(tempBlockIdCounter, new ArrayList<>());
    }

    public void saveForm() {
        this.newForm.setModuleId(Integer.parseInt(selectModuleId));
        this.newForm.setIsDefault(true);
        ArrayList<FieldBlockModel> finalBlockList = new ArrayList<>();
        for (FieldBlockModel fbm : this.blockList) {
            if (this.blockFieldsMap.get(fbm.getBlockId()) != null) {
                fbm.setFieldsLayoutList(this.blockFieldsMap.get(fbm.getBlockId()));
                finalBlockList.add(fbm);
            }
        }
        this.newForm.setBlockList(finalBlockList);

        try {
            FormsHandler formsHandler = new FormsHandler();
            FormRequest request = new FormRequest();
            if (newForm.getFormId() != 0) {
                this.deleteForm();
                request.setActionType("1");
            }
            else {
                request.setActionType("1");
            }
            request.setModuleId(Integer.parseInt(selectModuleId));
            request.setFormModel(newForm);
            FormResponse response = formsHandler.formExecutor(request);
            if (response.getErrorCode() == 1000) {

            }
            this.updateMessage(response.getErrorCode(), response.getErrorMessage());
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteBlock(FieldBlockModel block) {
        ArrayList<FieldsLayoutModel> cList = this.blockFieldsMap.get(block.getBlockId());
        for (ModuleFieldModel mfm : this.fieldsList) {
            for (FieldsLayoutModel flm : cList) {
                if (mfm.getFieldName().equals(flm.getFieldName())) {
                    TreeNode property = new DefaultTreeNode("field", mfm, (TreeNode) availableFields.getChildren().get(0));
                    break;
                }
            }
        }

        this.blockFieldsMap.remove(block.getBlockId());
        this.blockList.remove(block);
    }

    private void generateAvailableFieldsTree() {
        availableFields = new DefaultTreeNode("Root", null);
        TreeNode root = new DefaultTreeNode("Fields", availableFields);
        root.setExpanded(true);
        if (newForm != null) {
            if (newForm.getBlockList() != null) {
                this.blockList = newForm.getBlockList();
                ArrayList<ModuleFieldModel> tempFields = new ArrayList<>(fieldsList);
                for (FieldBlockModel fbm : this.blockList) {
                    this.blockFieldsMap.put(fbm.getBlockId(), fbm.getFieldsLayoutList());
                    for (FieldsLayoutModel flm : fbm.getFieldsLayoutList()) {
                        if (flm.getComponentType().equals("NORM")) {
                            for (ModuleFieldModel mfm : this.fieldsList) {
                                if (flm.getFieldObject().getFieldName().equals(mfm.getFieldName())) {
                                    tempFields.remove(mfm);
                                }
                            }
                        }
                    }
                }
                for (ModuleFieldModel mfm : tempFields) {
                    TreeNode model = new DefaultTreeNode("field", mfm, root);
                }
                return;
            }
        }

        for (ModuleFieldModel mfm : this.fieldsList) {
            TreeNode model = new DefaultTreeNode("field", mfm, root);
        }
    }

    public void treeToTable() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String compType = params.get("compType");
        String property = params.get("property");
        String droppedColumnId = params.get("droppedColumnId");
        String dropPos = params.get("dropPos");
        String dropLayout = params.get("dropLayout");
        HashMap<Integer, ArrayList<FieldsLayoutModel>> tempo = new HashMap<>();
        if (!compType.equals("ADV-COMP")) {
            for (ModuleFieldModel mfm : this.fieldsList) {
                if (mfm.getFieldName().equals(property)) {
                    ArrayList<FieldsLayoutModel> cList = this.blockFieldsMap.get(Integer.parseInt(droppedColumnId));
                    FieldsLayoutModel flm = new FieldsLayoutModel();
                    flm.setBlockId(Integer.parseInt(droppedColumnId));
                    flm.setLabel(mfm.getFieldLabel());
                    flm.setFieldId(mfm.getFieldId());
                    flm.setFieldName(mfm.getFieldName());
                    flm.setOrder(cList.size() + 1);
                    flm.setFieldObject(new ModuleFieldModel(mfm));
                    cList.add(flm);
                    this.blockFieldsMap.put(Integer.parseInt(droppedColumnId), cList);
                    break;
                }
            }

            //remove from nodes
            TreeNode root = (TreeNode) availableFields.getChildren().get(0);
            ArrayList<TreeNode> treeList = (ArrayList<TreeNode>) root.getChildren();
            for (TreeNode node : treeList) {
                ModuleFieldModel model = (ModuleFieldModel) node.getData();
                if (model.getFieldName().equals(property)) {
                    root.getChildren().remove(node);
                    break;
                }
            }
        }
        else {
            ArrayList<FieldsLayoutModel> cList = this.blockFieldsMap.get(Integer.parseInt(droppedColumnId));
            FieldsLayoutModel flm = new FieldsLayoutModel();
            flm.setEntityId(GeneralUtils.generateRandomId());
            flm.setComponentType("ADV-COMP");
            flm.setOrder(cList.size() + 1);
            AdvancedComponent advancedComponent = new AdvancedComponent();
            advancedComponent.setName(property);
            switch (property) {
                case "Button":
                    advancedComponent.setType("DYNO_BUTTON");
                    flm.setDynoButtonConfiguration(new DynoButton());
                    break;
                case "Table":
                    advancedComponent.setType("TABLE_COMP");
                    flm.setTableComponentConfiguration(new TableComponent());
                    flm.getTableComponentConfiguration().setColumnSettings(new ArrayList<>());
                    flm.getTableComponentConfiguration().setSummaryColumnsSettings(new ArrayList<>());
                    break;
                case "Layout":
                    advancedComponent.setType("LAYOUT");
                    flm.setLayoutComponentConfiguration(new LayoutComponent());
                    flm.getLayoutComponentConfiguration().setComponentsList(new ArrayList<>());
                    break;
            }

            flm.setAdvancedComponent(advancedComponent);
            cList.add(flm);
            this.blockFieldsMap.put(Integer.parseInt(droppedColumnId), cList);
        }
    }

    public void treeToTableLayout() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String compType = params.get("compType");
        String property = params.get("property");
        String droppedColumnId = params.get("droppedColumnId");
        String dropPos = params.get("dropPos");
        String dropLayout = params.get("dropLayout");
        if (compType.equals("ADV-COMP")) {
            if (dropLayout != null && !dropLayout.isEmpty()) {
                ArrayList<FieldsLayoutModel> cList = this.blockFieldsMap.get(Integer.parseInt(droppedColumnId));
                FieldsLayoutModel layoutHoldingModel = null;
                for (FieldsLayoutModel layoutModel : cList) {
                    if (layoutModel.getEntityId() == Integer.parseInt(dropLayout)) {
                        layoutHoldingModel = layoutModel;
                        break;
                    }
                }

                if (layoutHoldingModel != null) {
                    ArrayList<FieldsLayoutModel> layoutComponentList = layoutHoldingModel.getLayoutComponentConfiguration().getComponentsList();
                    FieldsLayoutModel flm = new FieldsLayoutModel();
                    flm.setEntityId(GeneralUtils.generateRandomId());
                    flm.setComponentType("ADV-COMP");
                    flm.setOrder(layoutComponentList.size() + 1);
                    AdvancedComponent advancedComponent = new AdvancedComponent();
                    advancedComponent.setName(property);
                    switch (property) {
                        case "Button":
                            advancedComponent.setType("DYNO_BUTTON");
                            flm.setDynoButtonConfiguration(new DynoButton());
                            break;
                        case "Table":
                            advancedComponent.setType("TABLE_COMP");
                            flm.setTableComponentConfiguration(new TableComponent());
                            flm.getTableComponentConfiguration().setColumnSettings(new ArrayList<>());
                            flm.getTableComponentConfiguration().setSummaryColumnsSettings(new ArrayList<>());
                            break;
                        case "Layout":
                            advancedComponent.setType("LAYOUT");
                            flm.setLayoutComponentConfiguration(new LayoutComponent());
                            flm.getLayoutComponentConfiguration().setComponentsList(new ArrayList<>());
                            break;
                    }

                    flm.setAdvancedComponent(advancedComponent);
                    layoutHoldingModel.getLayoutComponentConfiguration().getComponentsList().add(flm);
                    this.blockFieldsMap.put(Integer.parseInt(droppedColumnId), cList);
                }

            }
        }
    }
    
    public void updatePosition(){
        int x = 0;
    }

    public void tableToTree() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String fieldName = params.get("colIndex");
        String backFromBlockId = params.get("backFromBlock");

        //remove from table
        ArrayList<FieldsLayoutModel> cList = this.blockFieldsMap.get(Integer.parseInt(backFromBlockId));
        for (FieldsLayoutModel lfm : cList) {
            if (lfm.getFieldName().equals(fieldName)) {
                cList.remove(lfm);
                this.blockFieldsMap.put(Integer.parseInt(backFromBlockId), cList);
                break;
            }
        }

        for (ModuleFieldModel mfm : this.fieldsList) {
            if (mfm.getFieldName().equals(fieldName)) {
                TreeNode property = new DefaultTreeNode("field", mfm, (TreeNode) availableFields.getChildren().get(0));
                break;
            }
        }
    }

    public void openConfigurationManager(FieldsLayoutModel fieldsLayoutModel, String type) {
        this.advancedComponentsManager.setModuleManagerMap(moduleManagerMap);
        this.advancedComponentsManager.setFieldsMap(blockFieldsMap);
        this.advancedComponentsManager.openConfigurationManager(fieldsLayoutModel, type);
    }
    
    public void deleteTest(FieldsLayoutModel fieldsLayoutModel, FieldsLayoutModel components){
        int x = 0;
    }

    public void applyAdvancedConfigurations() {
        this.selectedField = this.advancedComponentsManager.getSelectedField();
    }

    protected void updateMessage(int errorCode, String errorMessage) {
        if (errorCode == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Added Susscefully."));
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add due to ." + errorMessage));
        }
    }

    public HashMap<String, String> getModuleManagerMap() {
        return moduleManagerMap;
    }

    public void setModuleManagerMap(HashMap<String, String> moduleManagerMap) {
        this.moduleManagerMap = moduleManagerMap;
    }

    public ArrayList<ModuleFieldModel> getFieldsList() {
        return fieldsList;
    }

    public void setFieldsList(ArrayList<ModuleFieldModel> fieldsList) {
        this.fieldsList = fieldsList;
    }

    public String getSelectModuleId() {
        return selectModuleId;
    }

    public void setSelectModuleId(String selectModuleId) {
        this.selectModuleId = selectModuleId;
    }

    public ArrayList<FormModel> getFormList() {
        return formList;
    }

    public void setFormList(ArrayList<FormModel> formList) {
        this.formList = formList;
    }

    public FormModel getNewForm() {
        return newForm;
    }

    public void setNewForm(FormModel newForm) {
        this.newForm = newForm;
    }

    public TreeNode getAvailableFields() {
        return availableFields;
    }

    public void setAvailableFields(TreeNode availableFields) {
        this.availableFields = availableFields;
    }

    public ArrayList<FieldBlockModel> getBlockList() {
        return blockList;
    }

    public void setBlockList(ArrayList<FieldBlockModel> blockList) {
        this.blockList = blockList;
    }

    public HashMap<Integer, ArrayList<FieldsLayoutModel>> getBlockFieldsMap() {
        return blockFieldsMap;
    }

    public void setBlockFieldsMap(HashMap<Integer, ArrayList<FieldsLayoutModel>> blockFieldsMap) {
        this.blockFieldsMap = blockFieldsMap;
    }

    public FieldsLayoutModel getSelectedField() {
        return selectedField;
    }

    public void setSelectedField(FieldsLayoutModel selectedField) {
        this.selectedField = selectedField;
    }

    public TreeNode getAdvancedComponentsTree() {
        return advancedComponentsTree;
    }

    public void setAdvancedComponentsTree(TreeNode advancedComponentsTree) {
        this.advancedComponentsTree = advancedComponentsTree;
    }

    public AdvancedComponentsManager getAdvancedComponentsManager() {
        return advancedComponentsManager;
    }

    public void setAdvancedComponentsManager(AdvancedComponentsManager advancedComponentsManager) {
        this.advancedComponentsManager = advancedComponentsManager;
    }

}
