/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.relations;

import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.RelationHandler;
import com.crm.models.comparators.FormFieldOrderComparator;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.responses.managers.RelationResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author badry
 */
@ManagedBean(name = "relationManagerBean")
@ViewScoped
public class RelationsManagerBean {

    private ArrayList<ModuleModel> moduleManagersList;
    private ArrayList<ModuleFieldModel> masterFieldSet;
    private ArrayList<ModuleFieldModel> detailFieldSet;
    private ArrayList<RelationManagerModel> relationsList;
    private ModuleModel selectedMasterModule;
    private ModuleModel selectedDetailModule;

    private RelationManagerModel relationModel;
    private boolean edit;

    @PostConstruct
    public void init() {
        this.loadModuleManagers();
        this.relationModel = new RelationManagerModel();
    }

    private void loadModuleManagers() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.moduleManagersList = response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RelationsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadMasterFieldSet() {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(String.valueOf(this.selectedMasterModule.getModuleId()));
            request.setActionType("0");
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.masterFieldSet = response.getReturnList();
                Collections.sort(masterFieldSet, new FormFieldOrderComparator());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RelationsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onMasterModuleSelection() {
        this.loadMasterFieldSet();
        this.loadRelationList();
    }

    public void onDetailModuleSelection() {
        for (ModuleModel mm : moduleManagersList) {
            if (mm.getModuleId() == this.relationModel.getDetailModuleId()) {
                this.relationModel.setDetailTable(mm.getModuleBaseTable());
                break;
            }
        }
        this.loadDetailFieldSet();
    }

    public void loadDetailFieldSet() {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(String.valueOf(this.relationModel.getDetailModuleId()));
            request.setActionType("0");
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.detailFieldSet = response.getReturnList();
                Collections.sort(detailFieldSet, new FormFieldOrderComparator());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RelationsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadRelationList() {
        try {
            RelationRequest request = new RelationRequest();
            request.setModuleId(String.valueOf(this.selectedMasterModule.getModuleId()));
            request.setActionType("4");
            RelationHandler relationHandler = new RelationHandler();
            RelationResponse response = relationHandler.relationExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.relationsList = response.getRelationManagerList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RelationsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void saveChanges() {
        try {
            RelationRequest request = new RelationRequest();
            request.setModuleId(String.valueOf(this.selectedMasterModule.getModuleId()));
            request.setRelationModel(relationModel);
            if (edit) {
                request.setActionType("3");
            }
            else {
                request.setActionType("1");
            }
            RelationHandler relationHandler = new RelationHandler();
            RelationResponse response = relationHandler.relationExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.relationsList.add(relationModel);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RelationsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addNewRelation() {
//        RelationManagerModel rel = new RelationManagerModel();
//        this.relationsList.add(rel);
        edit = false;
        this.relationModel = new RelationManagerModel();
        relationModel.setModuleId(selectedMasterModule.getModuleId());
        relationModel.setMasterTable(selectedMasterModule.getModuleBaseTable());
        PrimeFaces.current().ajax().update("relationSideBar");
        PrimeFaces.current().executeScript("PF('relationSideBar').show()");
    }

    public void onEdit(RelationManagerModel rmm) {
        edit = true;
        this.relationModel = rmm;
        this.loadDetailFieldSet();
        PrimeFaces.current().ajax().update("relationSideBar");
        PrimeFaces.current().executeScript("PF('relationSideBar').show()");
    }

    public void deleteRelation(RelationManagerModel rmm) {
        try {
            RelationHandler relationHandler = new RelationHandler();
            RelationRequest request = new RelationRequest();
            request.setRelationModel(rmm);
            request.setActionType("2");
            RelationResponse response = relationHandler.relationExecutor(request);
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "deleted Susscefully."));
                this.loadRelationList();
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete."));
                this.loadRelationList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(RelationsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<ModuleModel> getModuleManagersList() {
        return moduleManagersList;
    }

    public void setModuleManagersList(ArrayList<ModuleModel> moduleManagersList) {
        this.moduleManagersList = moduleManagersList;
    }

    public ArrayList<RelationManagerModel> getRelationsList() {
        return relationsList;
    }

    public void setRelationsList(ArrayList<RelationManagerModel> relationsList) {
        this.relationsList = relationsList;
    }

    public ArrayList<ModuleFieldModel> getMasterFieldSet() {
        return masterFieldSet;
    }

    public void setMasterFieldSet(ArrayList<ModuleFieldModel> masterFieldSet) {
        this.masterFieldSet = masterFieldSet;
    }

    public ArrayList<ModuleFieldModel> getDetailFieldSet() {
        return detailFieldSet;
    }

    public void setDetailFieldSet(ArrayList<ModuleFieldModel> detailFieldSet) {
        this.detailFieldSet = detailFieldSet;
    }

    public ModuleModel getSelectedMasterModule() {
        return selectedMasterModule;
    }

    public void setSelectedMasterModule(ModuleModel selectedMasterModule) {
        this.selectedMasterModule = selectedMasterModule;
    }

    public ModuleModel getSelectedDetailModule() {
        return selectedDetailModule;
    }

    public void setSelectedDetailModule(ModuleModel selectedDetailModule) {
        this.selectedDetailModule = selectedDetailModule;
    }

    public RelationManagerModel getRelationModel() {
        return relationModel;
    }

    public void setRelationModel(RelationManagerModel relationModel) {
        this.relationModel = relationModel;
    }

}
