/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.forms.components;

import com.crm.models.internal.components.AdvancedComponent;
import com.crm.models.internal.components.DynoButton;
import com.crm.models.internal.components.LayoutComponent;
import com.crm.models.internal.components.TableComponent;
import com.crm.models.internal.forms.FieldsLayoutModel;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author badry
 */
public class AdvancedComponentsManager extends AdvancedComponentBase {

    private TableComponentManager tableComponentManager;
    private DynoButtonManager dynoButtonManager;
    private LayoutComponentManager layoutComponentManager;

    public void openConfigurationManager(FieldsLayoutModel fieldsLayoutModel, String type) {
        super.selectedField = fieldsLayoutModel;
        switch (type) {
            case "DYNO_BUTTON":
                this.dynoButtonManager = new DynoButtonManager();
                this.dynoButtonManager.setSelectedField(super.selectedField);
                PrimeFaces.current().ajax().update("advancedConfigManager");
                PrimeFaces.current().executeScript("PF('advancedConfigManager').show()");
                break;
            case "TABLE_COMP":
                this.tableComponentManager = new TableComponentManager();
                this.tableComponentManager.setFieldsMap(super.fieldsMap);
                this.tableComponentManager.setSelectedField(super.selectedField);
                this.tableComponentManager.onBindingModelSelect();
                PrimeFaces.current().ajax().update("advancedConfigManager");
                PrimeFaces.current().executeScript("PF('advancedConfigManager').show()");
                break;
            case "LAYOUT":
                this.layoutComponentManager = new LayoutComponentManager();
                this.layoutComponentManager.setSelectedField(super.selectedField);
                PrimeFaces.current().ajax().update("advancedConfigManager");
                PrimeFaces.current().executeScript("PF('advancedConfigManager').show()");
                break;
        }
    }

    public TreeNode buildAdvancedComponentsTree() {
        TreeNode advancedComponentsTree = new DefaultTreeNode("Root", null);
        TreeNode root = new DefaultTreeNode("Advanced Components", advancedComponentsTree);
        AdvancedComponent advComp = new DynoButton();
        advComp.setName("Button");
        TreeNode model = new DefaultTreeNode("advancedComponent", advComp, root);
        advComp = new TableComponent();
        advComp.setName("Table");
        model = new DefaultTreeNode("advancedComponent", advComp, root);
        advComp = new LayoutComponent();
        advComp.setName("Layout");
        model = new DefaultTreeNode("advancedComponent", advComp, root);
        return advancedComponentsTree;
    }

    public TableComponentManager getTableComponentManager() {
        return tableComponentManager;
    }

    public void setTableComponentManager(TableComponentManager tableComponentManager) {
        this.tableComponentManager = tableComponentManager;
    }

    public DynoButtonManager getDynoButtonManager() {
        return dynoButtonManager;
    }

    public void setDynoButtonManager(DynoButtonManager dynoButtonManager) {
        this.dynoButtonManager = dynoButtonManager;
    }

    public LayoutComponentManager getLayoutComponentManager() {
        return layoutComponentManager;
    }

    public void setLayoutComponentManager(LayoutComponentManager layoutComponentManager) {
        this.layoutComponentManager = layoutComponentManager;
    }

}
