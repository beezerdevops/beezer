/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.fields;

import com.beezer.web.handler.LookupHandler;
import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.RelationHandler;
import com.crm.models.comparators.FormFieldOrderComparator;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.lookup.LookupManagerModel;
import com.crm.models.internal.relation.RelationManagerModel;
import com.crm.models.requests.managers.LookupRequest;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.requests.managers.RelationRequest;
import com.crm.models.responses.managers.LookupResponse;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.responses.managers.RelationResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "fieldManagerBean")
@ViewScoped
public class FieldsManagerBean {

    private HashMap<String, String> moduleManagerMap;
    protected ArrayList<ModuleFieldModel> fieldsList;
    private String selectModuleId;

    private ArrayList<ModuleFieldModel> fieldsToUpdate;
    private ArrayList<ModuleFieldModel> fieldsToAdd;

    private ModuleFieldModel fieldModel;
    private ArrayList<RelationManagerModel> relationsList;
    private RelationManagerModel selectedRelation;
    private ArrayList<LookupManagerModel> lookupManagerList;

    private boolean edit;

    @PostConstruct
    public void init() {
        this.loadModuleManagers();
        this.loadLookups();
        fieldModel = new ModuleFieldModel();
        fieldsToAdd = new ArrayList<>();
        fieldsToUpdate = new ArrayList<>();
    }

    private void loadModuleManagers() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.moduleManagerMap = new HashMap<>();
                for (ModuleModel mm : response.getReturnList()) {
                    this.moduleManagerMap.put(mm.getModuleName(), String.valueOf(mm.getModuleId()));
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadRelationList() {
        try {
            RelationRequest request = new RelationRequest();
            request.setModuleId(this.selectModuleId);
            request.setActionType("4");
            RelationHandler relationHandler = new RelationHandler();
            RelationResponse response = relationHandler.relationExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.relationsList = response.getRelationManagerList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadLookups() {
        try {
            LookupHandler lh = new LookupHandler();
            LookupRequest request = new LookupRequest();
            request.setActionType("0");
            LookupResponse response = lh.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.lookupManagerList = response.getManagerList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadFieldSet() {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(selectModuleId);
            request.setActionType("0");
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.fieldsList = response.getReturnList();
                Collections.sort(fieldsList, new FormFieldOrderComparator());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onTypeChoose() {
        if (fieldModel.getFieldType().equals("REL")) {
            this.loadRelationList();
            PrimeFaces.current().ajax().update("relationDialog");
//            PrimeFaces.current().executeScript("PF('relationDialog').show()");
        }
    }

    public void onRelationChoose() {
        this.fieldModel.setFieldValues(String.valueOf(selectedRelation.getId()));
        PrimeFaces.current().executeScript("PF('relationDialog').hide()");
    }

    public void saveChanges() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldRequest request = new ModuleFieldRequest();
            fieldsToAdd = new ArrayList<>();
            fieldsToAdd.add(fieldModel);
            request.setFileds(fieldsToAdd);
            request.setModuleId(selectModuleId);
            if (edit) {
                request.setActionType("3");
            }
            else {
                request.setActionType("1");
            }
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Added Susscefully."));
                this.loadFieldSet();
                fieldsToAdd.clear();
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add."));
                this.loadFieldSet();
                fieldsToAdd.clear();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            fieldsToAdd.clear();
        }
    }

    public void addNewField() {
//        ModuleFieldModel mfm = new ModuleFieldModel();
//        mfm.setModuleId(Integer.valueOf(selectModuleId));
//        this.fieldsList.add(mfm);
        edit = false;
        this.fieldModel = new ModuleFieldModel();
        fieldModel.setModuleId(Integer.valueOf(selectModuleId));
        selectedRelation = new RelationManagerModel();
        this.relationsList = new ArrayList<>();
        PrimeFaces.current().ajax().update("fieldSideBar");
        PrimeFaces.current().executeScript("PF('fieldSideBar').show()");
    }

    public void onEdit(ModuleFieldModel mfm) {
        edit = true;
        fieldModel = mfm;
        if(fieldModel.getFieldType().equals("REL")){
            this.loadRelationList();
        }
        PrimeFaces.current().ajax().update("fieldSideBar");
        PrimeFaces.current().executeScript("PF('fieldSideBar').show()");
    }

    public void onRowCancel(RowEditEvent event) {
    }

    public void deleteField(ModuleFieldModel mfm) {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldRequest request = new ModuleFieldRequest();
            ArrayList<ModuleFieldModel> reqList = new ArrayList<>();
            reqList.add(mfm);
            request.setFileds(reqList);
            request.setActionType("2");
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "deleted Susscefully."));
                this.loadFieldSet();
            }
            else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to delete."));
                this.loadFieldSet();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public HashMap<String, String> getModuleManagerMap() {
        return moduleManagerMap;
    }

    public void setModuleManagerMap(HashMap<String, String> moduleManagerMap) {
        this.moduleManagerMap = moduleManagerMap;
    }

    public String getSelectModuleId() {
        return selectModuleId;
    }

    public void setSelectModuleId(String selectModuleId) {
        this.selectModuleId = selectModuleId;
    }

    public ArrayList<ModuleFieldModel> getFieldsList() {
        return fieldsList;
    }

    public void setFieldsList(ArrayList<ModuleFieldModel> fieldsList) {
        this.fieldsList = fieldsList;
    }

    public ModuleFieldModel getFieldModel() {
        return fieldModel;
    }

    public void setFieldModel(ModuleFieldModel fieldModel) {
        this.fieldModel = fieldModel;
    }

    public ArrayList<RelationManagerModel> getRelationsList() {
        return relationsList;
    }

    public void setRelationsList(ArrayList<RelationManagerModel> relationsList) {
        this.relationsList = relationsList;
    }

    public ArrayList<LookupManagerModel> getLookupManagerList() {
        return lookupManagerList;
    }

    public void setLookupManagerList(ArrayList<LookupManagerModel> lookupManagerList) {
        this.lookupManagerList = lookupManagerList;
    }

    public RelationManagerModel getSelectedRelation() {
        return selectedRelation;
    }

    public void setSelectedRelation(RelationManagerModel selectedRelation) {
        this.selectedRelation = selectedRelation;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

}
