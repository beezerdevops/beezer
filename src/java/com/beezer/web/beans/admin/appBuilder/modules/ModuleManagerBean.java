/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.modules;

import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.responses.managers.ModuleManagerResponse;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "moduleManagerBean")
@ViewScoped
public class ModuleManagerBean {

    private ModuleModel moduleModel;
    private ArrayList<ModuleModel> moduleList;
    private ArrayList<ModuleModel> filteredModuleList;
    private UnstructuredModel unstructuredModel;
    private boolean edit;

    @PostConstruct
    public void init() {
        this.moduleModel = new ModuleModel();
        this.loadModuleList();
    }

    public void loadModuleList() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.moduleList = response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ModuleManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void openAddModuleSideBar() {
        this.moduleModel = new ModuleModel();
        PrimeFaces.current().ajax().update("moduleSideBar");
        PrimeFaces.current().executeScript("PF('moduleSideBar').show()");
    }

    public void addModule() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            if (!edit) {
                request.setActionType("1");
            }
            else {
                request.setActionType("3");
            }
            request.setModel(moduleModel);
            request.setUnstructuredData(unstructuredModel);
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.moduleList.add(moduleModel);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ModuleManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteModule(ModuleModel moduleModel) {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("2");
            request.setModel(moduleModel);
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.moduleList.remove(moduleModel);
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ModuleManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onContentSelect(FileUploadEvent event) {
        try {
            this.unstructuredModel = new UnstructuredModel();
            this.unstructuredModel.setFileExtenstion(GeneralUtils.getFileExtenstion(event.getFile().getFileName()));
            this.unstructuredModel.setFileName(event.getFile().getFileName());
            this.unstructuredModel.setFileSize(event.getFile().getSize());
            this.unstructuredModel.setContentStream(event.getFile().getContent());
        }
        catch (Exception ex) {
            Logger.getLogger(ModuleManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onEdit(ModuleModel moduleModel) {
        edit = true;
        this.moduleModel = moduleModel;
        PrimeFaces.current().ajax().update("moduleSideBar");
        PrimeFaces.current().executeScript("PF('moduleSideBar').show()");
    }

    public ModuleModel getModuleModel() {
        return moduleModel;
    }

    public void setModuleModel(ModuleModel moduleModel) {
        this.moduleModel = moduleModel;
    }

    public ArrayList<ModuleModel> getModuleList() {
        return moduleList;
    }

    public void setModuleList(ArrayList<ModuleModel> moduleList) {
        this.moduleList = moduleList;
    }

    public ArrayList<ModuleModel> getFilteredModuleList() {
        return filteredModuleList;
    }

    public void setFilteredModuleList(ArrayList<ModuleModel> filteredModuleList) {
        this.filteredModuleList = filteredModuleList;
    }

    public UnstructuredModel getUnstructuredModel() {
        return unstructuredModel;
    }

    public void setUnstructuredModel(UnstructuredModel unstructuredModel) {
        this.unstructuredModel = unstructuredModel;
    }

}
