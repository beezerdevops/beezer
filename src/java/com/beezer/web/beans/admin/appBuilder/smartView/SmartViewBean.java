/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.smartView;

import com.beezer.web.beans.admin.appBuilder.fields.FieldsManagerBean;
import com.beezer.web.beans.admin.appBuilder.forms.components.AdvancedComponentsManager;
import com.beezer.web.handler.ModuleHandler;
import com.beezer.web.handler.SmartViewHandler;
import com.beezer.web.utils.GeneralUtils;
import com.crm.models.comparators.FormFieldOrderComparator;
import com.crm.models.global.UnstructuredModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.ModuleModel;
import com.crm.models.internal.components.AdvancedComponent;
import com.crm.models.internal.components.DynoButton;
import com.crm.models.internal.components.LayoutComponent;
import com.crm.models.internal.components.TableComponent;
import com.crm.models.internal.filter.FilterManager;
import com.crm.models.internal.filter.FilterType;
import com.crm.models.internal.forms.FieldsLayoutModel;
import com.crm.models.internal.smartView.HeaderConfig;
import com.crm.models.internal.smartView.SmartViewModel;
import com.crm.models.internal.smartView.TabConfigHolder;
import com.crm.models.internal.smartView.TabsConfig;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.requests.managers.ModuleManagerRequest;
import com.crm.models.requests.managers.SmartViewRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import com.crm.models.responses.managers.ModuleManagerResponse;
import com.crm.models.responses.managers.SmartViewResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author badry
 */
@ManagedBean(name = "smartViewBean")
@ViewScoped
public class SmartViewBean {

    private HashMap<String, String> moduleManagerMap;
    private ArrayList<ModuleFieldModel> fieldsList;
    private String selectModuleId;

    private TreeNode availableFields;
    private FieldsLayoutModel selectedField;

    private SmartViewModel smartViewModel;
    private TabsConfig tabConfig;

    private TreeNode advancedComponentsTree;

    //Tab map
    private HashMap<String, TabsConfig> blockFieldsMap;

    private String activeTab;
    private AdvancedComponentsManager advancedComponentsManager;

    private boolean update = false;
    private UnstructuredModel unstructuredModel;

    @PostConstruct
    public void init() {
        this.loadModuleManagers();
        this.loadFieldSet();
        this.blockFieldsMap = new HashMap<>();
        this.selectedField = new FieldsLayoutModel();
        this.smartViewModel = new SmartViewModel();
        this.smartViewModel.setHeaderConfig(new HeaderConfig());
        this.smartViewModel.setTabConfigHolder(new TabConfigHolder());
        this.advancedComponentsManager = new AdvancedComponentsManager();
        this.advancedComponentsTree = this.advancedComponentsManager.buildAdvancedComponentsTree();
    }

    private void loadModuleManagers() {
        try {
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleManagerRequest request = new ModuleManagerRequest();
            request.setActionType("0");
            ModuleManagerResponse response = moduleHandler.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.moduleManagerMap = new HashMap<>();
                for (ModuleModel mm : response.getReturnList()) {
                    this.moduleManagerMap.put(mm.getModuleName(), String.valueOf(mm.getModuleId()));
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void buildAdvancedComponentsTree() {
        advancedComponentsTree = new DefaultTreeNode("Root", null);
        TreeNode root = new DefaultTreeNode("Advanced Components", advancedComponentsTree);
        AdvancedComponent advComp = new DynoButton();
        advComp.setName("Button");
        TreeNode model = new DefaultTreeNode("advancedComponent", advComp, root);
        advComp = new DynoButton();
        advComp.setName("Table");
        model = new DefaultTreeNode("advancedComponent", advComp, root);
    }

    public void onModuleSelect() {
        this.loadFieldSet();
        this.loadSmartView();
//        this.generateAvailableFieldsTree();
    }

    public void loadSmartView() {
        SmartViewHandler smartViewHandler = new SmartViewHandler();
        SmartViewRequest request = new SmartViewRequest();
        request.setRequestActionType("0");
        FilterManager fm = new FilterManager();
        fm.setFilter("AND", new FilterType("module_id", "=", selectModuleId));
        request.setClause(fm.getClause());
        SmartViewResponse response = smartViewHandler.executeRequest(request);
        if (response.getErrorCode() == 1000) {
            if (response.getReturnList() != null && !response.getReturnList().isEmpty()) {
                smartViewModel = response.getReturnList().get(0);
                update = true;
                this.populateTabMap();
            }
            else {
                smartViewModel = new SmartViewModel();
                smartViewModel.setHeaderConfig(new HeaderConfig());
                smartViewModel.setTabConfigHolder(new TabConfigHolder());
                smartViewModel.setModuleId(Integer.valueOf(selectModuleId));
            }
        }
    }

    public void loadFieldSet() {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(selectModuleId);
            request.setActionType("0");
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.fieldsList = response.getReturnList();
                Collections.sort(fieldsList, new FormFieldOrderComparator());
            }
        }
        catch (Exception ex) {
            Logger.getLogger(FieldsManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void populateTabMap() {
        if (smartViewModel.getTabConfigHolder().getTabsConfig() != null) {
            int index = 0;
            for (TabsConfig tc : smartViewModel.getTabConfigHolder().getTabsConfig()) {
                if (index == 0) {
                    this.activeTab = tc.getTabId();
                }
                this.blockFieldsMap.put(tc.getTabId(), tc);
                index++;
            }
        }
    }

    public void addNewSmartView() {
        this.smartViewModel = new SmartViewModel();
    }

    public void saveSmartView() {
        if (!blockFieldsMap.isEmpty()) {
            smartViewModel.getTabConfigHolder().setTabsConfig(new ArrayList<>());
            for (Map.Entry<String, TabsConfig> entrySet : blockFieldsMap.entrySet()) {
                smartViewModel.getTabConfigHolder().getTabsConfig().add(entrySet.getValue());
            }
        }

        SmartViewHandler smartViewHandler = new SmartViewHandler();
        SmartViewRequest request = new SmartViewRequest();
        if (!update) {
            request.setRequestActionType("1");
        }
        else {
            request.setRequestActionType("3");
        }
        request.setSmartViewModel(smartViewModel);
        request.setUnstructuredData(unstructuredModel);
        SmartViewResponse response = smartViewHandler.executeRequest(request);
        if (response.getErrorCode() == 1000) {
        }
        this.updateMessage(response.getErrorCode(), response.getErrorMessage());
    }

    public void onTabChange(TabChangeEvent event) {
        activeTab = event.getTab().getId();
    }

    public void openDisplayImageConfig() {
        PrimeFaces.current().ajax().update("displayPicSidebar");
        PrimeFaces.current().executeScript("PF('displayPicSidebar').show()");
    }

    public void onContentSelect(FileUploadEvent event) {
        try {
            this.unstructuredModel = new UnstructuredModel();
            this.unstructuredModel.setFileExtenstion(GeneralUtils.getFileExtenstion(event.getFile().getFileName()));
            this.unstructuredModel.setFileName(event.getFile().getFileName());
            this.unstructuredModel.setFileSize(event.getFile().getSize());
            this.unstructuredModel.setContentStream(event.getFile().getContent());
        }
        catch (Exception ex) {
            Logger.getLogger(SmartViewBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void removeDisplayPic() {
        this.smartViewModel.getHeaderConfig().setDisplayPicRef(null);
        this.unstructuredModel = null;
    }

    public void openTabConfiguration(TabsConfig config) {
        if (config != null) {
            this.tabConfig = config;
        }
        else {
            this.tabConfig = new TabsConfig();
        }
        PrimeFaces.current().ajax().update("tabSideBar");
        PrimeFaces.current().executeScript("PF('tabSideBar').show()");
    }

    public void addTab() {
        if (smartViewModel.getTabConfigHolder().getTabsConfig() != null) {
            smartViewModel.getTabConfigHolder().getTabsConfig().add(tabConfig);
        }
        else {
            smartViewModel.getTabConfigHolder().setTabsConfig(new ArrayList<>());
            smartViewModel.getTabConfigHolder().getTabsConfig().add(tabConfig);
        }
        this.activeTab = tabConfig.getTabId();
        tabConfig.setFieldsLayoutList(new ArrayList<>());
        blockFieldsMap.put(activeTab, tabConfig);
    }

    public void removeTab(TabsConfig tabsConfig) {
        smartViewModel.getTabConfigHolder().getTabsConfig().remove(tabsConfig);
        blockFieldsMap.remove(tabsConfig.getTabId());
    }

    public void treeToTable() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String compType = params.get("compType");
        String property = params.get("property");
        String droppedColumnId = params.get("droppedColumnId");
        String dropPos = params.get("dropPos");

        if (droppedColumnId.equals("TAB_LAYOUT")) {
            ArrayList<FieldsLayoutModel> cList = this.blockFieldsMap.get(activeTab).getFieldsLayoutList();
            FieldsLayoutModel flm = new FieldsLayoutModel();
            flm.setComponentType("ADV-COMP");
            flm.setOrder(cList.size() + 1);
            AdvancedComponent advancedComponent = new AdvancedComponent();
            advancedComponent.setName(property);
            switch (property) {
                case "Button":
                    advancedComponent.setType("DYNO_BUTTON");
                    flm.setDynoButtonConfiguration(new DynoButton());
                    break;
                case "Table":
                    advancedComponent.setType("TABLE_COMP");
                    flm.setTableComponentConfiguration(new TableComponent());
                    flm.getTableComponentConfiguration().setColumnSettings(new ArrayList<>());
                    flm.getTableComponentConfiguration().setSummaryColumnsSettings(new ArrayList<>());
                    break;
                case "Layout":
                    advancedComponent.setType("LAYOUT");
                    flm.setLayoutComponentConfiguration(new LayoutComponent());
                    flm.getLayoutComponentConfiguration().setComponentsList(new ArrayList<>());
                    break;
            }

            flm.setAdvancedComponent(advancedComponent);
            cList.add(flm);
            this.blockFieldsMap.get(activeTab).setFieldsLayoutList(cList);
//            this.blockFieldsMap.put(activeTab, cList);
        }
        else {
            if (smartViewModel.getHeaderConfig().getFieldsLayoutList() == null) {
                smartViewModel.getHeaderConfig().setFieldsLayoutList(new ArrayList<>());
            }
            FieldsLayoutModel flm = new FieldsLayoutModel();
            flm.setComponentType("ADV-COMP");
            flm.setOrder(smartViewModel.getHeaderConfig().getFieldsLayoutList().size() + 1);
            AdvancedComponent advancedComponent = new AdvancedComponent();
            advancedComponent.setName(property);
            switch (property) {
                case "Button":
                    advancedComponent.setType("DYNO_BUTTON");
                    flm.setDynoButtonConfiguration(new DynoButton());
                    break;
                case "Table":
                    break;
                case "Layout":
                    advancedComponent.setType("LAYOUT");
                    flm.setLayoutComponentConfiguration(new LayoutComponent());
                    flm.getLayoutComponentConfiguration().setComponentsList(new ArrayList<>());
                    break;
            }

            flm.setAdvancedComponent(advancedComponent);
            smartViewModel.getHeaderConfig().getFieldsLayoutList().add(flm);
        }

    }

    public void treeToTableLayout() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String compType = params.get("compType");
        String property = params.get("property");
        String droppedColumnId = params.get("droppedColumnId");
        String dropPos = params.get("dropPos");
        String dropLayout = params.get("dropLayout");
        if (droppedColumnId.equals("TAB_LAYOUT")) {
            if (compType.equals("ADV-COMP")) {
                if (dropLayout != null && !dropLayout.isEmpty()) {
                    ArrayList<FieldsLayoutModel> cList = this.blockFieldsMap.get(activeTab).getFieldsLayoutList();
                    FieldsLayoutModel layoutHoldingModel = null;
                    for (FieldsLayoutModel layoutModel : cList) {
                        if (layoutModel.getEntityId() == Integer.parseInt(dropLayout)) {
                            layoutHoldingModel = layoutModel;
                            break;
                        }
                    }

                    if (layoutHoldingModel != null) {
                        ArrayList<FieldsLayoutModel> layoutComponentList = layoutHoldingModel.getLayoutComponentConfiguration().getComponentsList();
                        FieldsLayoutModel flm = new FieldsLayoutModel();
                        flm.setEntityId(GeneralUtils.generateRandomId());
                        flm.setComponentType("ADV-COMP");
                        flm.setOrder(layoutComponentList.size() + 1);
                        AdvancedComponent advancedComponent = new AdvancedComponent();
                        advancedComponent.setName(property);
                        switch (property) {
                            case "Button":
                                advancedComponent.setType("DYNO_BUTTON");
                                flm.setDynoButtonConfiguration(new DynoButton());
                                break;
                            case "Table":
                                advancedComponent.setType("TABLE_COMP");
                                flm.setTableComponentConfiguration(new TableComponent());
                                flm.getTableComponentConfiguration().setColumnSettings(new ArrayList<>());
                                flm.getTableComponentConfiguration().setSummaryColumnsSettings(new ArrayList<>());
                                break;
                            case "Layout":
                                advancedComponent.setType("LAYOUT");
                                flm.setLayoutComponentConfiguration(new LayoutComponent());
                                flm.getLayoutComponentConfiguration().setComponentsList(new ArrayList<>());
                                break;
                        }

                        flm.setAdvancedComponent(advancedComponent);
                        layoutHoldingModel.getLayoutComponentConfiguration().getComponentsList().add(flm);
                        this.blockFieldsMap.get(activeTab).setFieldsLayoutList(cList);
                    }

                }
            }
        }
        else {
            if (smartViewModel.getHeaderConfig().getFieldsLayoutList() == null) {
                smartViewModel.getHeaderConfig().setFieldsLayoutList(new ArrayList<>());
            }
            FieldsLayoutModel layoutHoldingModel = null;
            for (FieldsLayoutModel layoutModel : smartViewModel.getHeaderConfig().getFieldsLayoutList()) {
                if (layoutModel.getEntityId() == Integer.parseInt(dropLayout)) {
                    layoutHoldingModel = layoutModel;
                    break;
                }
            }
            if (layoutHoldingModel != null) {

                FieldsLayoutModel flm = new FieldsLayoutModel();
                flm.setComponentType("ADV-COMP");
                flm.setOrder(smartViewModel.getHeaderConfig().getFieldsLayoutList().size() + 1);
                AdvancedComponent advancedComponent = new AdvancedComponent();
                advancedComponent.setName(property);
                switch (property) {
                    case "Button":
                        advancedComponent.setType("DYNO_BUTTON");
                        flm.setDynoButtonConfiguration(new DynoButton());
                        break;
                    case "Table":
                        break;
                }

                flm.setAdvancedComponent(advancedComponent);
                layoutHoldingModel.getLayoutComponentConfiguration().getComponentsList().add(flm);
//            smartViewModel.getHeaderConfig().getFieldsLayoutList().add(flm);
            }
        }

    }

    public void tableToTree() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String fieldName = params.get("colIndex");
        String backFromBlockId = params.get("backFromBlock");

        //remove from table
        ArrayList<FieldsLayoutModel> cList = this.blockFieldsMap.get(activeTab).getFieldsLayoutList();
        for (FieldsLayoutModel lfm : cList) {
            if (lfm.getFieldName().equals(fieldName)) {
                cList.remove(lfm);
                this.blockFieldsMap.get(activeTab).setFieldsLayoutList(cList);
//                this.blockFieldsMap.put(backFromBlockId, cList);
                break;
            }
        }

        for (ModuleFieldModel mfm : this.fieldsList) {
            if (mfm.getFieldName().equals(fieldName)) {
                TreeNode property = new DefaultTreeNode("field", mfm, (TreeNode) availableFields.getChildren().get(0));
                break;
            }
        }
    }

    public void openConfigurationManager(FieldsLayoutModel fieldsLayoutModel, String type) {
        this.advancedComponentsManager.setModuleManagerMap(moduleManagerMap);
        this.advancedComponentsManager.openConfigurationManager(fieldsLayoutModel, type);
    }

    public void applyAdvancedConfigurations() {
        this.selectedField = this.advancedComponentsManager.getSelectedField();
    }

    protected void updateMessage(int errorCode, String errorMessage) {
        if (errorCode == 1000) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success", "Added Susscefully."));
        }
        else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Failed", "Failed to add due to ." + errorMessage));
        }
    }

    public HashMap<String, String> getModuleManagerMap() {
        return moduleManagerMap;
    }

    public void setModuleManagerMap(HashMap<String, String> moduleManagerMap) {
        this.moduleManagerMap = moduleManagerMap;
    }

    public ArrayList<ModuleFieldModel> getFieldsList() {
        return fieldsList;
    }

    public void setFieldsList(ArrayList<ModuleFieldModel> fieldsList) {
        this.fieldsList = fieldsList;
    }

    public String getSelectModuleId() {
        return selectModuleId;
    }

    public void setSelectModuleId(String selectModuleId) {
        this.selectModuleId = selectModuleId;
    }

    public TreeNode getAvailableFields() {
        return availableFields;
    }

    public void setAvailableFields(TreeNode availableFields) {
        this.availableFields = availableFields;
    }

    public HashMap<String, TabsConfig> getBlockFieldsMap() {
        return blockFieldsMap;
    }

    public void setBlockFieldsMap(HashMap<String, TabsConfig> blockFieldsMap) {
        this.blockFieldsMap = blockFieldsMap;
    }

    public FieldsLayoutModel getSelectedField() {
        return selectedField;
    }

    public void setSelectedField(FieldsLayoutModel selectedField) {
        this.selectedField = selectedField;
    }

    public TreeNode getAdvancedComponentsTree() {
        return advancedComponentsTree;
    }

    public void setAdvancedComponentsTree(TreeNode advancedComponentsTree) {
        this.advancedComponentsTree = advancedComponentsTree;
    }

    public SmartViewModel getSmartViewModel() {
        return smartViewModel;
    }

    public void setSmartViewModel(SmartViewModel smartViewModel) {
        this.smartViewModel = smartViewModel;
    }

    public String getActiveTab() {
        return activeTab;
    }

    public void setActiveTab(String activeTab) {
        this.activeTab = activeTab;
    }

    public TabsConfig getTabConfig() {
        return tabConfig;
    }

    public void setTabConfig(TabsConfig tabConfig) {
        this.tabConfig = tabConfig;
    }

    public AdvancedComponentsManager getAdvancedComponentsManager() {
        return advancedComponentsManager;
    }

    public void setAdvancedComponentsManager(AdvancedComponentsManager advancedComponentsManager) {
        this.advancedComponentsManager = advancedComponentsManager;
    }

}
