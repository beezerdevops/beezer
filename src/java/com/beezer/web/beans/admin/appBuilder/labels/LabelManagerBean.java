/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.labels;

import com.beezer.web.handler.LocaleHandler;
import com.crm.models.internal.localization.LocalValueHolder;
import com.crm.models.internal.localization.LocalizationHost;
import com.crm.models.requests.managers.LocaleRequest;
import com.crm.models.responses.managers.LocaleResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.PrimeFaces;

/**
 *
 * @author badry
 */
@ManagedBean(name = "labelManagerBean")
@ViewScoped
public class LabelManagerBean {

    private LocalizationHost localizationHost;
    private LocalizationHost volatileLocalizationHost;
    private String selectedLang;

    private String labelId;
    private String labelEn;
    private String labelAr;

    private boolean edit;
    private boolean showDuplicateMessage;
    private ArrayList<String> filteredValues;

    @PostConstruct
    public void init() {
        volatileLocalizationHost = new LocalizationHost();
        volatileLocalizationHost.setLocalValues(new LinkedHashMap<>());
        volatileLocalizationHost.getLocalValues().put("en", new LocalValueHolder());
        volatileLocalizationHost.getLocalValues().put("ar", new LocalValueHolder());
        volatileLocalizationHost.getLocalValues().get("en").setKeyValueMap(new LinkedHashMap<>());
        volatileLocalizationHost.getLocalValues().get("ar").setKeyValueMap(new LinkedHashMap<>());
        this.loadLocalizationHost();
    }

    private void loadLocalizationHost() {
        try {
            LocaleRequest request = new LocaleRequest();
            request.setRequestActionType("0");
            LocaleHandler localeHandler = new LocaleHandler();
            LocaleResponse response = localeHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                this.localizationHost = response.getLocalizationHost();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LabelManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void clearFilters() {
        filteredValues = null;
        PrimeFaces.current().executeScript("PF('labelsTable').clearFilters()");
    }

    public void addNewLabel() {
        labelAr = null;
        labelEn = null;
        labelId = null;
        showDuplicateMessage = false;
        edit = false;
        PrimeFaces.current().ajax().update("addLabelDialog");
        PrimeFaces.current().executeScript("PF('addLabelDialog').show()");
    }

    public void editLabel(Map.Entry<String, String> keyValueMap) {
        showDuplicateMessage = false;
        edit = true;
        labelId = keyValueMap.getKey();
        labelEn = localizationHost.getLocalValues().get("en").getKeyValueMap().get(labelId);
        labelAr = localizationHost.getLocalValues().get("ar").getKeyValueMap().get(labelId);
        PrimeFaces.current().ajax().update("addLabelDialog");
        PrimeFaces.current().executeScript("PF('addLabelDialog').show()");
    }

    public void saveLabel() {
        if (!edit) {
            showDuplicateMessage = localizationHost.getLocalValues().get("en").getKeyValueMap().get(labelId) != null;
            if (showDuplicateMessage) {
                return;
            }
        }

        volatileLocalizationHost = new LocalizationHost();
        volatileLocalizationHost.setLocalValues(new LinkedHashMap<>());
        volatileLocalizationHost.getLocalValues().put("en", new LocalValueHolder());
        volatileLocalizationHost.getLocalValues().put("ar", new LocalValueHolder());
        volatileLocalizationHost.getLocalValues().get("en").setKeyValueMap(new LinkedHashMap<>());
        volatileLocalizationHost.getLocalValues().get("ar").setKeyValueMap(new LinkedHashMap<>());

        volatileLocalizationHost.getLocalValues().get("en").getKeyValueMap().put(labelId, labelEn);
        volatileLocalizationHost.getLocalValues().get("ar").getKeyValueMap().put(labelId, labelAr);

        try {
            LocaleRequest request = new LocaleRequest();
            if (edit) {
                request.setRequestActionType("3");
            }
            else {
                request.setRequestActionType("1");
            }
            request.setLocalizationHost(volatileLocalizationHost);
            LocaleHandler localeHandler = new LocaleHandler();
            LocaleResponse response = localeHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                localizationHost.getLocalValues().get("en").getKeyValueMap().put(labelId, labelEn);
                localizationHost.getLocalValues().get("ar").getKeyValueMap().put(labelId, labelAr);
                PrimeFaces.current().executeScript("PF('addLabelDialog').hide()");
                PrimeFaces.current().ajax().update("@form");
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LabelManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        labelAr = null;
        labelEn = null;
        labelId = null;
    }

    public LocalizationHost getLocalizationHost() {
        return localizationHost;
    }

    public void setLocalizationHost(LocalizationHost localizationHost) {
        this.localizationHost = localizationHost;
    }

    public LocalizationHost getVolatileLocalizationHost() {
        return volatileLocalizationHost;
    }

    public void setVolatileLocalizationHost(LocalizationHost volatileLocalizationHost) {
        this.volatileLocalizationHost = volatileLocalizationHost;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }

    public String getLabelEn() {
        return labelEn;
    }

    public void setLabelEn(String labelEn) {
        this.labelEn = labelEn;
    }

    public String getLabelAr() {
        return labelAr;
    }

    public void setLabelAr(String labelAr) {
        this.labelAr = labelAr;
    }

    public String getSelectedLang() {
        return selectedLang;
    }

    public void setSelectedLang(String selectedLang) {
        this.selectedLang = selectedLang;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public boolean isShowDuplicateMessage() {
        return showDuplicateMessage;
    }

    public void setShowDuplicateMessage(boolean showDuplicateMessage) {
        this.showDuplicateMessage = showDuplicateMessage;
    }

    public ArrayList<String> getFilteredValues() {
        return filteredValues;
    }

    public void setFilteredValues(ArrayList<String> filteredValues) {
        this.filteredValues = filteredValues;
    }

}
