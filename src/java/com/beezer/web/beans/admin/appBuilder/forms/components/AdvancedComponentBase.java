/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.forms.components;

import com.crm.models.internal.forms.FieldsLayoutModel;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author badry
 */
public class AdvancedComponentBase {

    protected FieldsLayoutModel selectedField;
    protected HashMap<String, String> moduleManagerMap;
    protected HashMap<Integer, ArrayList<FieldsLayoutModel>> fieldsMap;

    public AdvancedComponentBase() {
    }

    public FieldsLayoutModel getSelectedField() {
        return selectedField;
    }

    public void setSelectedField(FieldsLayoutModel selectedField) {
        this.selectedField = selectedField;
    }

    public HashMap<String, String> getModuleManagerMap() {
        return moduleManagerMap;
    }

    public void setModuleManagerMap(HashMap<String, String> moduleManagerMap) {
        this.moduleManagerMap = moduleManagerMap;
    }

    public HashMap<Integer, ArrayList<FieldsLayoutModel>> getFieldsMap() {
        return fieldsMap;
    }

    public void setFieldsMap(HashMap<Integer, ArrayList<FieldsLayoutModel>> fieldsMap) {
        this.fieldsMap = fieldsMap;
    }

}
