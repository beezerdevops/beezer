/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder.lookups;

import com.beezer.web.handler.LookupHandler;
import com.crm.models.internal.lookup.LookupManagerModel;
import com.crm.models.internal.lookup.LookupModel;
import com.crm.models.requests.managers.LookupRequest;
import com.crm.models.responses.managers.LookupResponse;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author badry
 */
@ManagedBean(name = "lookupManagerBean")
@ViewScoped
public class LookupManagerBean {

    private ArrayList<LookupManagerModel> lookupManagerList;
    private LookupManagerModel selectedLookup;
    private ArrayList<LookupModel> lookupList;
    private LookupManagerModel newLookupTable;

    @PostConstruct
    public void init() {
        this.loadManagers();
        newLookupTable = new LookupManagerModel();
    }

    private void loadManagers() {
        try {
            LookupHandler lh = new LookupHandler();
            LookupRequest request = new LookupRequest();
            request.setActionType("0");
            LookupResponse response = lh.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.lookupManagerList = response.getManagerList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LookupManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowSelect(SelectEvent event) {
        this.loadLookupValues(String.valueOf(selectedLookup.getId()));
    }
    
    private void loadLookupValues(String tableId){
        try {
            LookupHandler lh = new LookupHandler();
            LookupRequest request = new LookupRequest();
            request.setLookupId(tableId);
            LookupResponse response = lh.getLookups(request);
            if (response.getErrorCode() == 1000) {
                this.lookupList = response.getReturnList();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LookupManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addLookupValue() {
        this.lookupList.add(new LookupModel());
    }

    public void addNewLookupTable() {
        try {
            LookupHandler lh = new LookupHandler();
            LookupRequest request = new LookupRequest();
            request.setLookupName(newLookupTable.getLookupName());
            request.setLookupTable(newLookupTable.getLookupTable());
            request.setKey(newLookupTable.getKey());
            request.setActionType("1");
            LookupResponse response = lh.managerExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.lookupManagerList = response.getManagerList();
                FacesMessage msg = new FacesMessage("Added");
                FacesContext.getCurrentInstance().addMessage(null, msg);
                this.loadManagers();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LookupManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowEdit(RowEditEvent event) {
        try {
            LookupHandler lh = new LookupHandler();
            LookupRequest request = new LookupRequest();
            ArrayList<LookupModel> reqList = new ArrayList<>();
            LookupModel temp = (LookupModel) event.getObject();
            reqList.add(temp);
            request.setLookups(reqList);
            if (temp.getId() == 0) {
                request.setActionType("1");
            }
            else {
                request.setActionType("3");
            }
            request.setLookupId(String.valueOf(selectedLookup.getId()));
            LookupResponse response = lh.valueExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.lookupManagerList = response.getManagerList();
                FacesMessage msg = new FacesMessage("Edited", String.valueOf(((LookupModel) event.getObject()).getId()));
                FacesContext.getCurrentInstance().addMessage(null, msg);
                this.loadManagers();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LookupManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void onRowCancel(RowEditEvent event) {
        FacesMessage msg = new FacesMessage("Edit Cancelled", String.valueOf(((LookupModel) event.getObject()).getId()));
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void deleteTable(LookupManagerModel managerModel) {
        if (managerModel != null) {
            try {
                LookupHandler lh = new LookupHandler();
                LookupRequest request = new LookupRequest();
                request.setLookupName(managerModel.getLookupName());
                request.setLookupTable(managerModel.getLookupTable());
                request.setLookupId(String.valueOf(managerModel.getId()));
                request.setKey(managerModel.getKey());
                request.setActionType("2");
                LookupResponse response = lh.managerExecutor(request);
                if (response.getErrorCode() == 1000) {
                    this.lookupManagerList = response.getManagerList();
                    FacesMessage msg = new FacesMessage("Deleted Successfully");
                    FacesContext.getCurrentInstance().addMessage(null, msg);
                    this.loadManagers();
                }
            }
            catch (Exception ex) {
                Logger.getLogger(LookupManagerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void deleteLookupValue(LookupModel lookupModel) {
        try {
            LookupHandler lh = new LookupHandler();
            LookupRequest request = new LookupRequest();
            ArrayList<LookupModel> reqList = new ArrayList<>();
            reqList.add(lookupModel);
            request.setLookups(reqList);
            request.setActionType("2");
            request.setLookupId(String.valueOf(selectedLookup.getId()));
            LookupResponse response = lh.valueExecutor(request);
            if (response.getErrorCode() == 1000) {
                this.lookupManagerList = response.getManagerList();
                FacesMessage msg = new FacesMessage("Deleted", String.valueOf(lookupModel.getId()));
                FacesContext.getCurrentInstance().addMessage(null, msg);
                this.loadManagers();
                this.loadLookupValues(String.valueOf(selectedLookup.getId()));
            }
        }
        catch (Exception ex) {
            Logger.getLogger(LookupManagerBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<LookupManagerModel> getLookupManagerList() {
        return lookupManagerList;
    }

    public void setLookupManagerList(ArrayList<LookupManagerModel> lookupManagerList) {
        this.lookupManagerList = lookupManagerList;
    }

    public LookupManagerModel getSelectedLookup() {
        return selectedLookup;
    }

    public void setSelectedLookup(LookupManagerModel selectedLookup) {
        this.selectedLookup = selectedLookup;
    }

    public ArrayList<LookupModel> getLookupList() {
        return lookupList;
    }

    public void setLookupList(ArrayList<LookupModel> lookupList) {
        this.lookupList = lookupList;
    }

    public LookupManagerModel getNewLookupTable() {
        return newLookupTable;
    }

    public void setNewLookupTable(LookupManagerModel newLookupTable) {
        this.newLookupTable = newLookupTable;
    }

}
