/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans.admin.appBuilder;

import com.beezer.web.handler.apps.ApplicationsHandler;
import com.crm.models.internal.application.ApplicationModel;
import com.crm.models.requests.managers.ApplicationRequest;
import com.crm.models.responses.managers.ApplicationResponse;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author badry
 */
@ManagedBean(name = "myAppsBean")
@ViewScoped
public class MyAppsBean {

    private ArrayList<ApplicationModel> appList;
    private ApplicationModel selectedApp;

    @PostConstruct
    public void init() {
        this.loadApplicationList();
    }

    public void loadApplicationList() {
        ApplicationsHandler applicationsHandler = new ApplicationsHandler();
        ApplicationRequest applicationRequest = new ApplicationRequest();
        applicationRequest.setRequestActionType("0");
        ApplicationResponse response = applicationsHandler.execute(applicationRequest);
        if (response.getErrorCode() == 1000) {
            this.appList = response.getApplicationList();
        }
    }

    public void openAppBuilder(ApplicationModel applicationModel) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect("appBuilder.xhtml?app="
                    + applicationModel.getAppId());
        }
        catch (Exception ex) {
            Logger.getLogger(MyAppsBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void openDeleteAppConfirmation(ApplicationModel applicationModel) {
        this.selectedApp = applicationModel;
        PrimeFaces.current().executeScript("PF('deleteAppConfirmDialog').show()");
    }

    public void deleteApp() {
        ApplicationsHandler applicationsHandler = new ApplicationsHandler();
        ApplicationRequest applicationRequest = new ApplicationRequest();
        applicationRequest.setRequestActionType("2");
        applicationRequest.setApplicationModel(selectedApp);
        ApplicationResponse response = applicationsHandler.execute(applicationRequest);
        if (response.getErrorCode() == 1000) {
            this.appList.remove(selectedApp);
            PrimeFaces.current().ajax().update("appsGrid");
        }
    }

    public ArrayList<ApplicationModel> getAppList() {
        return appList;
    }

    public void setAppList(ArrayList<ApplicationModel> appList) {
        this.appList = appList;
    }

}
