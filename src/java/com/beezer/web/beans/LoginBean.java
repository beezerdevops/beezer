/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.beezer.web.commons.Defines;
import com.beezer.web.utils.Decryptor;
import com.beezer.web.utils.Encryptor;
import com.beezer.web.utils.GeneralUtils;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.codehaus.jackson.map.ObjectMapper;
import org.primefaces.PrimeFaces;

/**
 *
 * @author Ahmed El Badry Jan 8, 2023
 */
@ManagedBean(name = "loginBean")
@ViewScoped
public class LoginBean {

    private AccessBean accessBean;
    private boolean validToken;

    @PostConstruct
    public void init() {
        this.loadAccessBean();
//        this.checkRequestParams();
    }

    private void loadAccessBean() {
        Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        AccessBean currentAccessBean = (AccessBean) sessionMap.get("accessBean");
        if (currentAccessBean != null) {
            this.accessBean = currentAccessBean;
        }
        else {
            AccessBean ab = new AccessBean();
            sessionMap.put("accessBean", ab);
            this.accessBean = ab;
        }
    }

    public void checkRequestParams() {
        String token = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("token");
        if (token != null && !token.isEmpty()) {
            PrimeFaces.current().executeScript("PF('autoSigninNotification').show()");
            String serializedMap = Decryptor.decrypt(token);
            ObjectMapper mapper = new ObjectMapper();
            HashMap<String, String> userInfoMap = null;
            try {
                userInfoMap = mapper.readValue(serializedMap, HashMap.class);
            }
            catch (IOException ex) {
                PrimeFaces.current().executeScript("PF('autoSigninNotification').hide()");
                PrimeFaces.current().executeScript("PF('invalidTokenNotification').show()");
                Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
            }

            if (userInfoMap != null) {
                validToken = false;
                try {
                    Date expiryDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(userInfoMap.get("tokenExpiry"));
                    Date currentDate = new Date();
                    if (expiryDate.after(currentDate)) {
                        validToken = true;
                    }
                }
                catch (ParseException ex) {
                    PrimeFaces.current().executeScript("PF('autoSigninNotification').hide()");
                    PrimeFaces.current().executeScript("PF('invalidTokenNotification').show()");
                    Logger.getLogger(AccessBean.class.getName()).log(Level.SEVERE, null, ex);
                }

                if (validToken) {
                    this.accessBean.setEmail(userInfoMap.get("userEmail"));
                    this.accessBean.setPassword(userInfoMap.get("userPassword"));
                    this.accessBean.signIn();
                }
                else {
                    PrimeFaces.current().executeScript("PF('autoSigninNotification').hide()");
                    PrimeFaces.current().executeScript("PF('invalidTokenNotification').show()");
                }
            }
        }
    }
    
    public void openMyDesignerSpace() {
        if (this.accessBean.getUserModel() != null) {
            HashMap<String, String> userInfoMap = new HashMap<>();
            userInfoMap.put("userEmail", this.accessBean.getEmail());
            userInfoMap.put("userPassword", this.accessBean.getPassword());
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            cal.add(Calendar.DATE, 1);
            Date date = cal.getTime();
            SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String tokenExpiryDate = null;
            tokenExpiryDate = format1.format(date);
            userInfoMap.put("tokenExpiry", tokenExpiryDate);
            String serializedMap = GeneralUtils.serializeRequest(userInfoMap);
            String encryptedMap = Encryptor.encrypt(serializedMap);
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(Defines.GENERAL_CONFIG_MAP.get("designer.url") + "/login.xhtml?token=" + encryptedMap.replace("+", "%2B"));
            }
            catch (IOException ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
