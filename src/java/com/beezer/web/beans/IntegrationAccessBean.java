/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.beans;

import com.crm.models.global.UserModel;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Ahmed El Badry Nov 13, 2021
 */
@ManagedBean(name = "integrationAccessBean")
@SessionScoped
public class IntegrationAccessBean {

    private UserModel userModel;
    
    @PostConstruct
    public void init() {
        int x = 0;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

}
