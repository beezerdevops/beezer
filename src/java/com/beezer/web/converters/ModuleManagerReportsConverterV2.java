/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.converters;

import com.beezer.web.beans.modules.reports.AddReportBean_new;
import com.beezer.web.beans.modules.reports.AddReportBeanV2;
import com.crm.models.internal.ModuleModel;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author badry
 */
@FacesConverter(forClass = com.crm.models.internal.ModuleModel.class, value = "moduleReportsConverterV2")
public class ModuleManagerReportsConverterV2 implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        try {
            if (value != null && value.trim().length() > 0) {
                return getModel(Integer.valueOf(value));
            }
        }
        catch (Exception e) {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid module."));
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o != null) {
            return String.valueOf(((ModuleModel) o).getModuleId());
        }
        else {
            return null;
        }
    }

    private ModuleModel getModel(int moduleId) {
        Map<String, Object> appMap = FacesContext.getCurrentInstance().getViewRoot().getViewMap();
        AddReportBeanV2 viewScopedBean = (AddReportBeanV2) appMap.get("addReportBeanV2");
        for (ModuleModel mm : viewScopedBean.getModuleList()) {
            if (mm.getModuleId() == moduleId) {
                return mm;
            }
        }
        return null;
    }
}
