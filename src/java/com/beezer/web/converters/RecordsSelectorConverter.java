/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.converters;

import com.beezer.web.beans.templates.AddDataBean;
import com.crm.models.internal.forms.FormModel;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author badry
 */
@FacesConverter(forClass = com.crm.models.internal.RecordModel.class, value = "recordsConverter")
public class RecordsSelectorConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        try {
            if (value != null && value.trim().length() > 0) {
                return getModel(Integer.valueOf(value));
            }
        }
        catch (Exception e) {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid form."));
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o != null) {
            return String.valueOf(((FormModel) o).getFormId());
        }
        else {
            return null;
        }
    }

    private FormModel getModel(int formid) {
        Map<String, Object> appMap = FacesContext.getCurrentInstance().getViewRoot().getViewMap();
        AddDataBean viewScopedBean = (AddDataBean) appMap.get("addDataBean");
        for (FormModel fm : viewScopedBean.getFormList()) {
            if (fm.getFormId()== formid) {
                return fm;
            }
        }
        return null;
    }
}
