/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.converters;

import com.beezer.web.beans.modules.sales.insights.SalesInsightsBean;
import com.beezer.web.models.apps.sales.InsightsOptionsModel;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author badry
 */
@FacesConverter(forClass = com.beezer.web.models.apps.sales.InsightsOptionsModel.class, value = "insightOptionsConverter")
public class InsightOptionsConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        try {
            if (value != null && value.trim().length() > 0) {
                return getModel(Integer.parseInt(value));
            }
        }
        catch (Exception e) {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid module."));
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o != null) {
            if (o instanceof String) {
                int x = 0;
            }
            return String.valueOf(((InsightsOptionsModel) o).getId());
        }
        else {
            return null;
        }
    }

    private InsightsOptionsModel getModel(int optionId) {
        Map<String, Object> appMap = FacesContext.getCurrentInstance().getViewRoot().getViewMap();
        SalesInsightsBean viewScopedBean = (SalesInsightsBean) appMap.get("salesInsightsBean");
        for (InsightsOptionsModel mfm : viewScopedBean.getInsightsOptionsModelList()) {
            if (mfm.getId() == optionId) {
                return mfm;
            }
        }
        return null;
    }

//    private ModuleFieldModel getModel(int fieldId) {
//        Map<String, Object> appMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
//        AddReportBean viewScopedBean = (AddReportBean) appMap.get("addReportBean");
//        for (ModuleFieldModel mfm : viewScopedBean.getAvailableColumnList()) {
//            if (mfm.getFieldId() == fieldId) {
//                return mfm;
//            }
//        }
//        return null;
//    }
}
