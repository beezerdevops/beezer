/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.converters;

import com.beezer.web.beans.admin.permissions.PermissionManagerBean;
import com.crm.models.internal.ModuleFieldModel;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author badry
 */
@FacesConverter(forClass = com.crm.models.internal.ModuleFieldModel.class, value = "permissionsModuleFieldsConverter")
public class PermissionsModuleFieldsConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        try {
            if (value != null && value.trim().length() > 0) {
                return getModel(Integer.valueOf(value));
            }
        }
        catch (Exception e) {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid module."));
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o != null) {
            if (o instanceof String) {
                int x = 0;
            }
            return String.valueOf(((ModuleFieldModel) o).getFieldId());
        }
        else {
            return null;
        }
    }

    private ModuleFieldModel getModel(int fieldId) {
        Map<String, Object> appMap = FacesContext.getCurrentInstance().getViewRoot().getViewMap();
        PermissionManagerBean viewScopedBean = (PermissionManagerBean) appMap.get("permissionManagerBean");
        for (ModuleFieldModel mfm : viewScopedBean.getModuleFieldSet()) {
            if (mfm.getFieldId() == fieldId) {
                return mfm;
            }
        }
        return null;
    }
}
