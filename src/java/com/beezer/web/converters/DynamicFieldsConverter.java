/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.converters;

import com.beezer.web.handler.ModuleHandler;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.requests.managers.ModuleFieldRequest;
import com.crm.models.responses.managers.ModuleFieldResponse;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author badry
 */
@FacesConverter(forClass = com.crm.models.internal.ModuleFieldModel.class, value = "dynamicFieldsConverter")
public class DynamicFieldsConverter implements Converter {

    private ArrayList<ModuleFieldModel> fieldsList;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        try {
            if (value != null && value.trim().length() > 0) {
                Map<String, Object> attributes = uic.getAttributes();
                String moduleId = (String) attributes.get("moduleId");
                return getModel(Integer.valueOf(value), moduleId);
            }
        }
        catch (Exception e) {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid field."));
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o != null) {
            if (o instanceof String) {
                int x = 0;
            }
            return String.valueOf(((ModuleFieldModel) o).getFieldId());
        }
        else {
            return null;
        }
    }

    private ModuleFieldModel getModel(int fieldId, String moduleId) {
        try {
            ModuleFieldRequest request = new ModuleFieldRequest();
            request.setModuleId(moduleId);
            request.setActionType("0");
            ModuleHandler moduleHandler = new ModuleHandler();
            ModuleFieldResponse response = moduleHandler.fieldExecutor(request);
            if (response.getErrorCode() == 1000) {
                if (response.getReturnList() != null && !response.getReturnList().isEmpty()) {
                    this.fieldsList = response.getReturnList();
                    for (ModuleFieldModel mfm : response.getReturnList()) {
                        if (mfm.getFieldId() == fieldId) {
                            return mfm;
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
            Logger.getLogger(DynamicFieldsConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void loadFieldSet(String moduleId) {
    }

//    private ModuleFieldModel getModel(int fieldId) {
//        Map<String, Object> appMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
//        AddReportBean viewScopedBean = (AddReportBean) appMap.get("addReportBean");
//        for (ModuleFieldModel mfm : viewScopedBean.getAvailableColumnList()) {
//            if (mfm.getFieldId() == fieldId) {
//                return mfm;
//            }
//        }
//        return null;
//    }
}
