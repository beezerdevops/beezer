/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.converters;

import com.beezer.web.beans.modules.reports.AddReportBean;
import com.beezer.web.beans.modules.reports.AddReportBean_new;
import com.crm.models.internal.ModuleFieldModel;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author badry
 */
@FacesConverter(forClass = com.crm.models.internal.ModuleFieldModel.class, value = "moduleFieldsJoinConverterOld")
public class ModuleFieldsJoinConverterOld implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        try {
            if (value != null && value.trim().length() > 0) {
                Map<String, Object> attributes = uic.getAttributes();
                String moduleId = (String) attributes.get("moduleId");
                return getModel(Integer.valueOf(value), moduleId);
            }
        }
        catch (Exception e) {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid module."));
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o != null) {
            if (o instanceof String) {
                int x = 0;
            }
            return String.valueOf(((ModuleFieldModel) o).getFieldId());
        }
        else {
            return null;
        }
    }

    private ModuleFieldModel getModel(int fieldId, String moduleId) {
        Map<String, Object> appMap = FacesContext.getCurrentInstance().getViewRoot().getViewMap();
        AddReportBean viewScopedBean = (AddReportBean) appMap.get("addReportBean");
        for (ModuleFieldModel mfm : viewScopedBean.getModuleFieldMap().get(moduleId)) {
            if (mfm.getFieldId() == fieldId) {
                return mfm;
            }
        }
        return null;
    }

//    private ModuleFieldModel getModel(int fieldId) {
//        Map<String, Object> appMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
//        AddReportBean_new viewScopedBean = (AddReportBean_new) appMap.get("addReportBean");
//        for (ModuleFieldModel mfm : viewScopedBean.getAvailableColumnList()) {
//            if (mfm.getFieldId() == fieldId) {
//                return mfm;
//            }
//        }
//        return null;
//    }
}
