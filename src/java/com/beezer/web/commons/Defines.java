/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.commons;

import com.beezer.web.beans.AccessBean;
import java.util.HashMap;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 *
 * @author badry
 */
public class Defines {

    public static final String REQUEST_MODE_KEY = "RequestMode";
    public static final String REQUEST_MODE_ADD = "NEW";
    public static final String REQUEST_MODE_EDIT = "EDIT";
    public static final String REQUEST_MODE_APPROVE = "APPROVE";

    public static final String CENTRAL_API_KEY = "c8246de6de5940b2994cde8defce0f0d";

    public static Logger ERROR_LOGGER = Logger.getLogger("errorLogger");
    public static Logger DEBUG_LOGGER = Logger.getLogger("debugLogger");
    public static String APP_ROOT = "";
    public static String API_KEY = "";
    public static HashMap<String, String> GENERAL_CONFIG_MAP = new HashMap<>();

    public static HashMap<Long, HttpSession> LOGIN_LEDGER = new HashMap<>();
    public static HashMap<Long, FacesContext> PF_LEDGER = new HashMap<>();
    public static HashMap<Long, AccessBean> ACCESS_LEDGER = new HashMap<>();

    public static final String KEY = "cGoJaIlYsrfb8ZkugjByNVFSsc3eoSSO";
    public static final String SALT = "5CyEJbjBWHrEC1UCQ2NUYhkMSgsAtSSO";

    public static final String allBlue = "#1789fc";
    public static final String retro = "#ea5545, #f46a9b, #ef9b20, #edbf33, #ede15b, #bdcf32, #87bc45, #27aeef, #b33dc6";
    public static final String blueYellow = "#115f9a, #1984c5, #22a7f0, #48b5c4, #76c68f, #a6d75b, #c9e52f, #d0ee11, #d0f400";
    public static final String orangePurple = "#ffb400, #d2980d, #a57c1b, #786028, #363445, #48446e, #5e569b, #776bcd, #9080ff";
    public static final String aqua = "#e27c7c, #a86464, #6d4b4b, #503f3f, #333333, #3c4e4b, #466964, #599e94, #6cd4c5";
    public static final String blackPink = "#2e2b28, #3b3734, #474440, #54504c, #6b506b, #ab3da9, #de25da, #eb44e8, #ff80ff";
}
