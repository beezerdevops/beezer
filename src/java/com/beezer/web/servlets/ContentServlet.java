/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.servlets;

import com.beezer.web.commons.Defines;
import com.beezer.web.handler.UnstructuredHandler;
import com.crm.models.requests.UnstructuredRequest;
import com.crm.models.responses.UnstructuredResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ahmed El Badry Dec 12, 2019
 */
public class ContentServlet extends HttpServlet {

    // Constants ----------------------------------------------------------------------------------
    private static final int DEFAULT_BUFFER_SIZE = 10240; // 10KB.

    // Properties ---------------------------------------------------------------------------------
    private String filePath;

    // Actions ------------------------------------------------------------------------------------
    @Override
    public void init() throws ServletException {

        // Define base path somehow. You can define it as init-param of the servlet.
        this.filePath = "/files";

        // In a Windows environment with the Applicationserver running on the
        // c: volume, the above path is exactly the same as "c:\files".
        // In UNIX, it is just straightforward "/files".
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Get requested file by path info.
        String requestedFile = request.getPathInfo();

        // Check if file is actually supplied to the request URI.
        if (requestedFile == null) {
            // Do your thing if the file is not supplied to the request URI.
            // Throw an exception, or send 404, or show default/warning page, or just ignore it.
            response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
            return;
        }

        UnstructuredRequest unstructuredRequest = new UnstructuredRequest();
//        unstructuredRequest.setRequestorUserModel(GeneralUtils.getLoggedUser());
        unstructuredRequest.setApiKey(Defines.API_KEY);
        unstructuredRequest.setUnstructuredId(requestedFile.substring(1));
        unstructuredRequest.setContext("CONFIG");
        unstructuredRequest.setObjectId(0);
        UnstructuredHandler unstructuredHandler = new UnstructuredHandler();
        UnstructuredResponse unstructuredResponse = unstructuredHandler.getImage(unstructuredRequest);
        if (unstructuredResponse.getErrorCode() != 1000 || unstructuredResponse.getReturnList() == null
                || unstructuredResponse.getReturnList().isEmpty()) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
            return;
        }

        // Get content type by filename.
        String contentType = getServletContext().getMimeType(unstructuredResponse.getReturnList().get(0).getFileName() + unstructuredResponse.getReturnList().get(0).getFileExtenstion());

        // If content type is unknown, then set the default value.
        // For all content types, see: http://www.w3schools.com/media/media_mimeref.asp
        // To add new content types, add new mime-mapping entry in web.xml.
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        File file = new File(unstructuredResponse.getReturnList().get(0).getFileName() + unstructuredResponse.getReturnList().get(0).getFileExtenstion());
        OutputStream os = new FileOutputStream(file);
        os.write(unstructuredResponse.getReturnList().get(0).getContentStream());
        os.close();

        // Init servlet response.
        response.reset();
        response.setBufferSize(DEFAULT_BUFFER_SIZE);
        response.setContentType(contentType);
        response.setHeader("Content-Length", String.valueOf(file.length()));
        response.setHeader("Content-Disposition", "attachment; filename=\"" + unstructuredResponse.getReturnList().get(0).getFileName() + "\"");

        // Prepare streams.
        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        try {
            // Open streams.
            input = new BufferedInputStream(new FileInputStream(file), DEFAULT_BUFFER_SIZE);
            output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

            // Write file contents to response.
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        }
        finally {
            // Gently close streams.
            close(output);
            close(input);
        }
    }

    // Helpers (can be refactored to public utility class) ----------------------------------------
    private static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            }
            catch (IOException e) {
                // Do your thing with the exception. Print it, log it or mail it.
                e.getMessage();
            }
        }
    }

}
