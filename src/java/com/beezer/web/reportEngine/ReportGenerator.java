/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.reportEngine;

import ar.com.fdvs.dj.core.DynamicJasperHelper;
import ar.com.fdvs.dj.core.layout.ClassicLayoutManager;
import ar.com.fdvs.dj.domain.DynamicReport;
import ar.com.fdvs.dj.domain.Style;
import ar.com.fdvs.dj.domain.builders.ColumnBuilder;
import ar.com.fdvs.dj.domain.builders.DynamicReportBuilder;
import ar.com.fdvs.dj.domain.constants.Border;
import ar.com.fdvs.dj.domain.constants.Font;
import ar.com.fdvs.dj.domain.constants.HorizontalAlign;
import ar.com.fdvs.dj.domain.constants.Transparency;
import ar.com.fdvs.dj.domain.entities.columns.AbstractColumn;
import com.beezer.web.handler.ReportHandler;
import com.crm.models.global.reports.ReportModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.requests.ReportRequest;
import com.crm.models.responses.ReportResponse;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

/**
 *
 * @author badry
 */
public class ReportGenerator {

    public byte[] generate(ReportModel reportModel) {
        Style headerStyle = new Style();

        headerStyle.setBackgroundColor(new Color(230, 230, 230));
        headerStyle.setBorderBottom(Border.THIN());
        headerStyle.setBorderColor(Color.black);
        headerStyle.setHorizontalAlign(HorizontalAlign.LEFT);
        headerStyle.setTransparency(Transparency.OPAQUE);

        /**
         * "titleStyle" exists in the template .jrxml file The title should be
         * seen in a big font size, violet foreground and light green background
         */
        Style titleStyle = new Style("titleStyle");

        /**
         * "subtitleStyleParent" is meant to be used as a parent style, while
         * "subtitleStyle" is the child.
         */
        Style subtitleStyleParent = new Style("subtitleParent");
        subtitleStyleParent.setBackgroundColor(Color.CYAN);
        subtitleStyleParent.setTransparency(Transparency.OPAQUE);

        Style subtitleStyle = Style.createBlankStyle("subtitleStyle", "subtitleParent");
        subtitleStyle.setFont(Font.GEORGIA_SMALL_BOLD);

        DynamicReportBuilder drb = new DynamicReportBuilder();
        drb.setTitle(reportModel.getReportDetails().getFieldValueMap().get("report_name").getCurrentValue()) //defines the title of the report
                .setSubtitle("This report was generated on " + new Date())
                .setDetailHeight(15) //defines the height for each record of the report
                .setMargins(30, 20, 30, 15) //define the margin space for each side (top, bottom, left and right)
                .setDefaultStyles(titleStyle, subtitleStyle, headerStyle, null)
                .addStyle(subtitleStyleParent); //register the parent style

        if (reportModel != null) {
            for (ModuleFieldModel mfm : reportModel.getFieldList()) {
                AbstractColumn col = ColumnBuilder.getNew() //creates a new instance of a ColumnBuilder
                        .setColumnProperty(mfm.getFieldName(), String.class.getName()) //defines the field of the data source that this column will show, also its type
                        .setTitle(mfm.getFieldLabel()) //the title for the column
                        .setWidth(85) //the width of the column
                        .build();
                drb.addColumn(col);
            }
            drb.setUseFullPageWidth(true);
            DynamicReport dr = drb.build();
            JasperPrint jp;
            try {
                jp = DynamicJasperHelper.generateJasperPrint(dr, new ClassicLayoutManager(), this.createDataSource(reportModel));
                return JasperExportManager.exportReportToPdf(jp);
//                JasperExportManager.exportReportToPdfFile(jp, "C:\\Users\\badry\\Desktop\\Hybris\\sample.pdf");
            }
            catch (JRException ex) {
                Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public JRDataSource createDataSource(ReportModel reportModel) {
        ArrayList<RecordModel> dataList = this.getRecordData(reportModel);
        ArrayList<String> columnNames = new ArrayList<>();
        ArrayList<Object> dataValues;
        for (ModuleFieldModel mfm : reportModel.getFieldList()) {
            columnNames.add(mfm.getFieldName());
        }
        DRDataSource dataSource = new DRDataSource(columnNames.toArray(new String[columnNames.size()]));
        for (RecordModel rm : dataList) {
            dataValues = new ArrayList<>();
            for (ModuleFieldModel mfm : reportModel.getFieldList()) {
                if (rm.getFieldValueMap().get(mfm.getFieldName()) != null) {
                    if (rm.getFieldValueMap().get(mfm.getFieldName()).getFieldType().equals("LOOKUP")) {
                        dataValues.add(rm.getFieldValueMap().get(mfm.getFieldName() + ".name").getCurrentValue());
                    }
                    else {
                        dataValues.add(rm.getFieldValueMap().get(mfm.getFieldName()).getCurrentValue());
                    }
                }
            }
            dataSource.add(dataValues.toArray(new Object[dataValues.size()]));
        }
        return dataSource;
    }

//    private ArrayList<RecordModel> getRecordData(ReportModel reportModel) {
//        try {
//            RequestModel request = new RequestModel();
//            request.setLimit(1000);
//            request.setOffset(0);
//            request.setRequestActionType("0");
//            request.setRequestingModule(String.valueOf(reportModel.getPrimaryModule().getModuleId()));
//            RequestHandler requestHandler = new RequestHandler();
//            ResponseModel response = requestHandler.executeGetRequest(request);
//            if (response.getErrorCode() == 1000) {
//                return response.getRecordList();
//            }
//        }
//        catch (Exception ex) {
//            Logger.getLogger(OrganizationDetailBean.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }
    public ArrayList<RecordModel> getRecordData(ReportModel reportModel) {
        try {
            ReportRequest request = new ReportRequest();
            request.setReportModel(reportModel);
            request.setLimit(1000);
            request.setOffset(0);
            request.setRequestActionType("0");
            request.setGetData(true);
            ReportHandler reportHandler = new ReportHandler();
            ReportResponse response = reportHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                return response.getDataRecords();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
