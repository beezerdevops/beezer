/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beezer.web.reportEngine;

import com.beezer.web.handler.ReportHandler;
import com.crm.models.global.reports.ReportModel;
import com.crm.models.internal.ModuleFieldModel;
import com.crm.models.internal.RecordModel;
import com.crm.models.requests.ReportRequest;
import com.crm.models.responses.ReportResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import static net.sf.dynamicreports.report.builder.DynamicReports.cht;
import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.grp;
import static net.sf.dynamicreports.report.builder.DynamicReports.sbt;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;
import net.sf.dynamicreports.report.builder.chart.Bar3DChartBuilder;
import net.sf.dynamicreports.report.builder.chart.PieChartBuilder;
import net.sf.dynamicreports.report.builder.chart.TimeSeriesChartBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.group.ColumnGroupBuilder;
import net.sf.dynamicreports.report.builder.subtotal.AggregationSubtotalBuilder;
import net.sf.dynamicreports.report.constant.Orientation;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;

/**
 *
 * @author badry
 */
public class SalesReport {
    //init styles

    public byte[] generate(ReportModel reportModel) {
        JasperReportBuilder report = new JasperReportBuilder();
        net.sf.dynamicreports.report.builder.style.FontBuilder boldFont = stl.fontArialBold();

        //init columns
        TextColumnBuilder<String> channelColumn = col.column("Channel", "lead_source.name", type.stringType());

        TextColumnBuilder<String> itemColumn = col.column("Product", "product_name", type.stringType()).setPrintRepeatedDetailValues(true);

        TextColumnBuilder<Date> orderDateColumn = col.column("Order date", "added_date", type.dateType());

        TextColumnBuilder<Integer> quantityColumn = col.column("Quantity", "quantity", type.integerType());

        TextColumnBuilder<BigDecimal> unitPriceColumn = col.column("Unit price", "selling_price", Templates.currencyType);

        //price = unitPrice * quantity
        TextColumnBuilder<BigDecimal> priceColumn = unitPriceColumn.multiply(quantityColumn).setTitle("Price")
                .setDataType(Templates.currencyType);

        //init groups
        ColumnGroupBuilder stateGroup = grp.group(channelColumn).keepTogether();
        ColumnGroupBuilder productGroup = grp.group(itemColumn);

        AggregationSubtotalBuilder<BigDecimal> unitPriceSum = sbt.sum(unitPriceColumn)
                .setLabel("Total:")
                .setLabelStyle(Templates.boldStyle);

        AggregationSubtotalBuilder<BigDecimal> priceSum = sbt.sum(priceColumn)
                .setLabel("")
                .setLabelStyle(Templates.boldStyle);

        //init charts
        Bar3DChartBuilder itemChart = cht.bar3DChart()
                .setTitle("Sales by item")
                .setTitleFont(boldFont)
                .setCategory(itemColumn)
                .addSerie(
                        cht.serie(priceColumn).setSeries(channelColumn));

        TimeSeriesChartBuilder dateChart = cht.timeSeriesChart()
                .setTitle("Sales by date")
                .setTitleFont(boldFont)
                .setFixedHeight(150)
                .setTimePeriod(orderDateColumn)
                .addSerie(
                        cht.serie(priceColumn), cht.serie(priceColumn));

        PieChartBuilder stateChart = cht.pieChart()
                .setTitle("Sales by Channel")
                .setTitleFont(boldFont)
                .setFixedHeight(100)
                .setShowLegend(false)
                .setKey(channelColumn)
                .addSerie(
                        cht.serie(quantityColumn).setSeries(channelColumn));

        //configure report
        report.addProperty("net.sf.jasperreports.chart.pie.ignore.duplicated.key", "true");
        report.setTemplate(Templates.reportTemplate) //columns

                .columns(
                        channelColumn, itemColumn, orderDateColumn, quantityColumn, unitPriceColumn, priceColumn)
                .groupBy(stateGroup)
                .subtotalsAtFirstGroupFooter(
                        sbt.sum(unitPriceColumn), sbt.sum(priceColumn))
                .subtotalsAtSummary(
                        unitPriceSum, priceSum)
                .title(
                        Templates.createTitleComponent("Sales"),
                        cmp.verticalList(
                                itemChart, dateChart, stateChart),
                        cmp.verticalGap(10))
                .pageFooter(
                        Templates.footerComponent)
                .setDataSource(this.createDataSource(reportModel));

        report.build();
        try {
            return JasperExportManager.exportReportToPdf(report.toJasperPrint());
        }
        catch (Exception ex) {
            Logger.getLogger(SalesReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public JRDataSource createDataSource(ReportModel reportModel) {
        ArrayList<RecordModel> dataList = this.getRecordData(reportModel);
        ArrayList<String> columnNames = new ArrayList<>();
        ArrayList<Object> dataValues;
        for (ModuleFieldModel mfm : reportModel.getFieldList()) {
            if (mfm.getFieldType().equals("LOOKUP")) {
                columnNames.add(mfm.getFieldName() + ".name");
            }
            else {
                columnNames.add(mfm.getFieldName());
            }
        }
        DRDataSource dataSource = new DRDataSource(columnNames.toArray(new String[columnNames.size()]));
        for (RecordModel rm : dataList) {
            dataValues = new ArrayList<>();
            for (ModuleFieldModel mfm : reportModel.getFieldList()) {
                if (rm.getFieldValueMap().get(mfm.getFieldName()) != null) {
                    if (rm.getFieldValueMap().get(mfm.getFieldName()).getFieldName().equals("quantity")) {
                        String z = rm.getFieldValueMap().get(mfm.getFieldName()).getCurrentValue();
                        String s = !z.contains(".") ? z : z.replaceAll("0*$", "").replaceAll("\\.$", "");
                        dataValues.add(Integer.parseInt(s));
                    }
                    else if (rm.getFieldValueMap().get(mfm.getFieldName()).getFieldName().equals("selling_price")) {
                        dataValues.add(new BigDecimal(rm.getFieldValueMap().get(mfm.getFieldName()).getCurrentValue()));
                    }
                    else if (rm.getFieldValueMap().get(mfm.getFieldName()).getFieldName().equals("added_date")) {
//                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
//                        Date startDate = null;
//                        try {
//                            startDate = df.parse(rm.getFieldValueMap().get(mfm.getFieldName()).getCurrentValue());
//                            String newDateString = df.format(startDate);
//                            System.out.println(newDateString);
//                        }
//                        catch (ParseException e) {
//                            e.printStackTrace();
//                        }
                        dataValues.add(new Date(Long.valueOf(rm.getFieldValueMap().get(mfm.getFieldName()).getCurrentValue())));
                    }
                    else if (rm.getFieldValueMap().get(mfm.getFieldName()).getFieldType().equals("LOOKUP")) {
                        dataValues.add(rm.getFieldValueMap().get(mfm.getFieldName() + ".name").getCurrentValue());
                    }
                    else {
                        dataValues.add(rm.getFieldValueMap().get(mfm.getFieldName()).getCurrentValue());
                    }

                }
            }
            dataSource.add(dataValues.toArray(new Object[dataValues.size()]));
        }
        return dataSource;
    }

    public ArrayList<RecordModel> getRecordData(ReportModel reportModel) {
        try {
            ReportRequest request = new ReportRequest();
            request.setReportModel(reportModel);
            request.setLimit(1000);
            request.setOffset(0);
            request.setRequestActionType("0");
            request.setGetData(true);
            ReportHandler reportHandler = new ReportHandler();
            ReportResponse response = reportHandler.execute(request);
            if (response.getErrorCode() == 1000) {
                return response.getDataRecords();
            }
        }
        catch (Exception ex) {
            Logger.getLogger(SalesReport.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
